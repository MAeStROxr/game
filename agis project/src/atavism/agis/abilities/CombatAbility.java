package atavism.agis.abilities;

import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisAbilityState;
import atavism.agis.core.AgisEffect;
import atavism.agis.plugins.CombatPlugin;
import atavism.server.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Old Ability class that is no longer used. Replaced by CombatMeleeAbility etc.
 * Do not use.
 * @author
 *
 */
public class CombatAbility extends AgisAbility {
    public CombatAbility(String name) {
        super(name);
    }

    public Map resolveHit(AgisAbilityState state) {
	return new HashMap();
    }

    public AgisEffect getActivationEffect() { return activationEffect; }
    public void setActivationEffect(AgisEffect effect) { this.activationEffect = effect; }
    protected AgisEffect activationEffect = null;

    public void completeActivation(AgisAbilityState state) {
        super.completeActivation(state);

        //Add attacker to target's list of attackers
        CombatPlugin.addAttacker(state.getTargetOid(), state.getSourceOid());
        state.getSource().setCombatState(true);        
        
	Map params = resolveHit(state);
	Log.debug("CombatAbility.completeActivation: params=" + params);
        AgisEffect.applyEffect(activationEffect, state.getSource(), state.getTarget(), getID(), params);
    }
}