package atavism.agis.abilities;

import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisAbilityState;
import atavism.agis.core.AgisEffect;
import atavism.agis.effects.ParryEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.engine.OID;
import atavism.server.util.Log;

import java.util.*;

/**
 * Runs the checks and calculations for Melee Combat Abilities and applies the effects. 
 * @author Andrew Harrison
 *
 */
public class CombatMeleeAbility extends AgisAbility {
    static Random random = new Random();

    public CombatMeleeAbility(String name) {
        super(name);
    }

    /**
     * [16:25] Andrew: basically the idea behind that is the hit chance calculations 
     * should only be run when the ability is about to be successfully completed
	 * [16:25] Andrew: this function is run like 5 times
	 * [16:25] Andrew: upon using an ability
  	 * [16:25] Andrew: each time the activationState is changed to the next stage 
     * 
     * @return
     */
    protected AbilityResult checkAbility(CombatInfo obj, CombatInfo target,
                    ActivationState state) {

        AbilityResult result = super.checkAbility(obj, target, state);
        // Check basic results
        if (result != AbilityResult.SUCCESS) {
        	Log.debug("ANDREW - ability failed. Reason: " + result);
        	ExtendedCombatMessages.sendAbilityFailMessage(obj, result);
            return result;
        }
        
        // First we have a few checks of our own
        result = checkPosition(obj, target, state);
    	if (result != AbilityResult.SUCCESS) {
    		ExtendedCombatMessages.sendAbilityFailMessage(obj, result);
    	    return result;
    	}
    	
    	// Now check for the ability missing/dodged/crit etc.
    	if (state == ActivationState.ACTIVATING) {
    		//effectParamsA = new HashMap<String, Integer>();
    		//effectParamsT = new HashMap<String, Integer>();
    		params = new HashMap<String, Integer>();
    		changeCoordinatedEffect("success");
    		
    		/* How this works:
    		 * We start with a base chance to hit (95%) and a chance to miss (5%)
    		 * Now from the hit % we take away the chance to be dodged, parried,
    		 * blocked. Then we take away the crit chance from the remaining hit chance.
    		 * If the crit chance is higher than the remaining hit chance then
    		 * the crit chance will only be as big as the hit chance.
    		 */
    		
    		// Check if the target has an appropriate weapon equipped and defensive skill
    		String defensiveType = "dodged";
    		String weapType = target.getStringProperty("weaponType");
    		//int defensiveSkillLevel = target.statGetCurrentValue("level");
    		
    		// Does the player have a parry effect for this weapon type
    		boolean hasParry = false;
    		int skillType = -1;
    		for (AgisEffect.EffectState eState: target.getCurrentEffects()) {
    			AgisEffect e = eState.getEffect();
    			if (e instanceof ParryEffect) {
    				ParryEffect pEffect = (ParryEffect) e;
    				String weapon = pEffect.getWeapon();
    				if (weapType.contains(weapon)) {
    					hasParry = true;
    					skillType = pEffect.GetEffectSkillType();
    				}
    			}
    		}
    		if (hasParry == true) {
    			// Figure out which skill it is from and get its current level
    			if (skillType != -1) {
    	            if (!target.getCurrentSkillInfo().getSkills().containsKey(skillType))
    	    	        Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
    	            else {
    	        	    //defensiveSkillLevel = target.getCurrentSkillInfo().getSkills().get(skillType).getSkillLevel();
    	        	    defensiveType = "parried";
    	            }
    			}
    		}
    		
    		double hitChance = CombatHelper.CalcPhysicalHitChance(obj, target, skillType);
    		
    		//int critChance = CombatHelper.CalcCritChance(obj, target) + blockChance; 
    		// Get some calc for this
    		// Teelo note: No need to reduce critChance to 100% when the random won't land higher than 100% anyway
    		
    		// Lets see where the random value falls
    		//int rand = random.nextInt(100);
    		double rand = random.nextDouble();
    		Log.debug("COMBATMELEE: random value = " + rand + "; hitChance = " + hitChance);
    		hitRoll = (int)(rand * 100);
    		if (target.getState().equals(CombatInfo.COMBAT_STATE_IMMUNE)) {
    			attackerResult = AgisAbility.RESULT_IMMUNE;
    		} else if (target.getState().equals(CombatInfo.COMBAT_STATE_EVADE)) {
    			attackerResult = AgisAbility.RESULT_EVADED;
    		} else if (rand < 0.05) {
    			attackerResult = AgisAbility.RESULT_MISSED;
    			changeCoordinatedEffect("missed");
    			casterResultEffect = getResultVal("missed", true);
    			targetResultEffect = getResultVal("missed", false);
    			if (casterResultEffect != null && targetResultEffect != null) {
    			    Log.debug("RESULT: set result to missed with caster effect: " + casterResultEffect.getName() 
    					+ " and target effect: " + targetResultEffect.getName());
    			}
    			//effectParamsA.put("effect1", 53);
    			//effectParamsT.put("effect1", 63);
    		} else if (rand < (1-hitChance)) {
    			changeCoordinatedEffect(defensiveType);
    			if (defensiveType.equals("dodged")){
    				attackerResult = AgisAbility.RESULT_DODGED;
    				changeCoordinatedEffect("dodged");
    				casterResultEffect = getResultVal("dodged", true);
    				targetResultEffect = getResultVal("dodged", false);
    				if (casterResultEffect != null && targetResultEffect != null) {
        			    Log.debug("RESULT: set result to dodged with caster effect: " + casterResultEffect.getName() 
        					+ " and target effect: " + targetResultEffect.getName());
        			}
    			    //effectParamsA.put("effect1", 55);
    			    //effectParamsT.put("effect1", 65);
    			} else if (defensiveType.equals("parried")){
    				attackerResult = AgisAbility.RESULT_PARRIED;
    				casterResultEffect = getResultVal("parried", true);
    				targetResultEffect = getResultVal("parried", false);
    				if (casterResultEffect != null && targetResultEffect != null) {
        			    Log.debug("RESULT: set result to parried with caster effect: " + casterResultEffect.getName() 
        					+ " and target effect: " + targetResultEffect.getName());
        			}
    			    //effectParamsA.put("effect1", 54);
    			    //effectParamsT.put("effect1", 64);
    			}
    		}/* else if (rand < parryChance) {
    			attackerResult = 4;
    			changeCoordinatedEffect("parried");
    			effectParamsA.put("effect1", 54);
    			effectParamsT.put("effect1", 64);
    		} else if (rand < blockChance) {
    			attackerResult = 6;
    			changeCoordinatedEffect("blocked");
    			effectParamsA.put("effect1", 56);
    			effectParamsT.put("effect1", 66);
    		} else if (rand < critChance) {
    			attackerResult = 2;
    			changeCoordinatedEffect("critical");
    			effectParamsA.put("effect1", 52);
    			effectParamsT.put("effect1", 62);
    		}*/ else {
    			attackerResult = AgisAbility.RESULT_HIT;
    		}
    		
    		// Only send skill up chance when hit roll is over 80
    		Log.debug("SKILL: hit roll is: " + hitRoll);
    		if (hitRoll < 60) {
    			sendSkillUpChance = false;
    		}
    	}

        return AbilityResult.SUCCESS;
    }
    
    /**
     * Not currently used - needs to be implemented in the core AgisAbility class in the near future.
     * @author Andrew
     *
     */
    private class PlayerAngle {
    	
    	/**
    	 * Must be between 0 and 360
    	 */
    	private float facing;
    	
    	protected PlayerAngle(float angle) {
    		while (angle < 0)
    			angle += 360;
    		while (angle >= 360)
    			angle -= 360;
    		this.facing = angle;
    	}
    	
    	protected boolean is_within(float min, float max, boolean anticlockwise) {
    		if (min < max) {
	    		if (anticlockwise) {
	    			return (this.facing < min || this.facing > max);
	    		} else {
	    			return (this.facing > min && this.facing < max);
	    		}
    		} else {
    			if (anticlockwise) {
	    			return (this.facing < min && this.facing > max);
	    		} else {
	    			return (this.facing > min || this.facing < max);
	    		}
    		}
    	}
    }
    
    private Map<String, Integer> params = null;
    private AgisEffect casterResultEffect = null;
    private AgisEffect targetResultEffect = null;
    //private Map<String, Integer> effectParamsA = null;
    //private Map<String, Integer> effectParamsT = null;
    private int attackerResult = 0;
    private int hitRoll = 0;
    
    // Does this ability require any special positionals?
    // 0 = none; 1 = front; 2 = sides; 3 = back
    public void setPositional(int positional) {
    	position = positional;
    }
    public int getPositional() {
        return position;
    }
    public int position = 0;
    
    // Does this ability require any special positions?
    public void setShieldReq(int shieldNeeded) {
    	shieldReq = shieldNeeded;
    }
    public int getShieldReq() {
        return shieldReq;
    }
    public int shieldReq = 0;

    // Copied from EffectAbility
    public LinkedList<AgisEffect> getActivationEffect() {
        return activationEffects;
    }
    public void addActivationEffect(AgisEffect effect) {
    	if (activationEffects == null) {
    		activationEffects = new LinkedList<AgisEffect>();
    	}
    	activationEffects.add(effect);
    }
    protected LinkedList<AgisEffect> activationEffects = null;
    
    public void addEffectTarget(String target) {
    	if (effectTarget == null) {
    		effectTarget = new LinkedList<String>();
    	}
    	effectTarget.add(target);
    }
    public LinkedList<String> GetEffectTarget() {
        return effectTarget;
    }
    public LinkedList<String> effectTarget = null;
    
    
    public AgisEffect getChannelEffect() {
        return channelEffect;
    }
    public void setChannelEffect(AgisEffect effect) {
        this.channelEffect = effect;
    }
    protected AgisEffect channelEffect = null;

    
    public AgisEffect getActiveEffect() {
        return activeEffect;
    }
    public void setActiveEffect(AgisEffect effect) {
        this.activeEffect = effect;
    }
    protected AgisEffect activeEffect = null;


    public void completeActivation(AgisAbilityState state) {
        super.completeActivation(state);
        
        // If this ability doesn't require a target, and the player doesn't have a target (they have 
        // targeted themselves), don't apply any effects
        if (!reqTarget && state.getSourceOid().equals(state.getTargetOid())) {
        	return;
        }
        
		state.getTarget().setCombatState(true);
		ArrayList<CombatInfo> targets = new ArrayList<CombatInfo>();
		if (targetType == TargetType.AREA_ENEMY) {
			targets = getAoETargets(state.getSource());
			Log.error("COMBAT: got aoe targets: " + targets);
		}
		// Here we need to add in the effectsList to the parameters
		for (int i = 0; i < activationEffects.size(); i++) {
			params.put("result", attackerResult);
			//params.put("effectVal", effectVals.get(i));
			params.put("skillType", skillType);
			params.put("hitRoll", hitRoll);
			//activationEffects.get(i).setDuelEffect(getDuelID());
			String target = effectTarget.get(i);
			if (targetType == TargetType.AREA_ENEMY && !target.equals("player")) {
				for (CombatInfo targetInfo : targets) {
					//Log.error("RESULT: adding param result with value: " + attackerResult + "; params is now: " + params + " and effect: " + activationEffects.get(i));
					AgisEffect.EffectState eState = AgisEffect.applyEffect(activationEffects.get(i), state.getSource(), 
							targetInfo, getID(), params, state.getItem());
				}
			} else {
				CombatInfo targetInfo = state.getTarget();
				//Log.debug("TARGET: got target for effect: " + target + " for ability: " + getName() + " and activtionEffectNum: " + i 
				//		+ " and effect: " + activationEffects.get(i));
				if (target.equals("caster"))
					targetInfo = state.getSource();
				
				//Log.error("RESULT: adding param result with value: " + attackerResult + "; params is now: " + params + " and effect: " + activationEffects.get(i));
				AgisEffect.EffectState eState = AgisEffect.applyEffect(activationEffects.get(i), state.getSource(), 
						targetInfo, getID(), params, state.getItem());
			}
            params.clear();
		} //end for
        
		// Result Effects - to be re-added
		/*if (casterResultEffect != null) {
			AgisEffect.EffectState eState = AgisEffect.applyEffect(casterResultEffect, state.getSource(), state.getSource(), params);
	        Log.debug("RESULT: added caster result effect: " + casterResultEffect.getName() + " and id: " + eState.getEffect().getID());
		}
		if (targetResultEffect != null) {
			AgisEffect.EffectState eState = AgisEffect.applyEffect(targetResultEffect, state.getSource(), state.getTarget(), params);
	        Log.debug("RESULT: added target result effect: " + targetResultEffect.getName() + " and id: " + eState.getEffect().getID());
		}*/
		
		// Item Used message is not sent if the target is being hit by their dueling partner
		Integer duelID = (Integer)state.getTarget().getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
    	if (duelID == null || duelID < 0 || !state.getSource().isUser())
    		AgisInventoryClient.equippedItemUsed(state.getTargetOid(), "Defend");
        
        // Finally, lets try auto-attack this mob
        CombatInfo info = state.getSource();
        CombatInfo target = state.getTarget();
    	OID oid = target.getOwnerOid();
        info.setAutoAttack(oid);
    }

    public void pulseChannelling(AgisAbilityState state) {
        super.pulseChannelling(state);
        AgisEffect.applyEffect(channelEffect, state.getSource(), state.getTarget(), getID());
    }

    public void pulseActivated(AgisAbilityState state) {
        super.pulseActivated(state);
        AgisEffect.applyEffect(activeEffect, state.getSource(), state.getTarget(), getID());
    }

}
