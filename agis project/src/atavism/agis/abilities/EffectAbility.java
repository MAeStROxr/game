package atavism.agis.abilities;

import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisAbilityState;
import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.engine.OID;
import atavism.server.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

/**
 * This class is for abilities that do not cause some form of instant damage or require the player to physically
 * "hit" the player, but still will place an effect on an enemy target. As such to-hit calculations do need to 
 * be done. If you want a certain effect to always be applied regardless, use FriendlyEffectAbility.
 * @author Andrew Harrison
 *
 */
public class EffectAbility extends AgisAbility {
	static Random random = new Random();
	protected LinkedList<AgisEffect> activationEffects;
	public LinkedList<String> effectTarget;
	protected AgisEffect channelEffect;
	protected AgisEffect activeEffect;
	
    public EffectAbility(String name) {
        super(name);
        this.params = new HashMap<String, Integer>();
        this.activationEffects = new LinkedList<AgisEffect>();
        this.effectTarget = new LinkedList<String>();
        this.activeEffect = null;
        this.channelEffect = null;
    }
    
    protected AbilityResult checkAbility(CombatInfo obj, CombatInfo target,
			ActivationState state) {

        AbilityResult result = super.checkAbility(obj, target, state);
        Log.debug("draive checkabililty effect activated");
        if (result != AbilityResult.SUCCESS) {
        	ExtendedCombatMessages.sendAbilityFailMessage(obj, result);
            return result;
        }
        this.params = new HashMap<String, Integer>();
        
        // Now check for the ability missing/dodged/crit etc.
    	if (state == ActivationState.ACTIVATING) {
    		//effectParamsA = new HashMap<String, Integer>();
    		//effectParamsT = new HashMap<String, Integer>();
    		params = new HashMap<String, Integer>();
    		
    		params.put("hitRoll", 100);
    		if (target.getState().equals(CombatInfo.COMBAT_STATE_IMMUNE)) {
    			attackerResult = AgisAbility.RESULT_IMMUNE;
    		} else if (target.getState().equals(CombatInfo.COMBAT_STATE_EVADE)) {
    			attackerResult = AgisAbility.RESULT_EVADED;
    		} else {
    			attackerResult = AgisAbility.RESULT_HIT;
    		}
    	}
        return AbilityResult.SUCCESS;
    }
    
    private Map<String, Integer> params = null;
    private AgisEffect casterResultEffect = null;
    private AgisEffect targetResultEffect = null;
    //private Map<String, Integer> effectParamsA = null;
    //private Map<String, Integer> effectParamsT = null;
    private int attackerResult = 0;
    
    // Copied from EffectAbility
    public LinkedList<AgisEffect> getActivationEffect() {
        return this.activationEffects;
    }
    public void addActivationEffect(AgisEffect effect) {
    	this.activationEffects.add(effect);
    }
    
    public void addEffectTarget(String target) {
    	effectTarget.add(target);
    }
    public LinkedList<String> getEffectTarget() {
        return effectTarget;
    }

    public AgisEffect getChannelEffect() { return this.channelEffect; }
    public void setChannelEffect(AgisEffect effect) { this.channelEffect = effect; }

    public AgisEffect getActiveEffect() { return this.activeEffect; }
    public void setActiveEffect(AgisEffect effect) { this.activeEffect = effect; }

    public void completeActivation(AgisAbilityState state) {
        super.completeActivation(state);
        
        // Here we need to add in the effectsList to the parameters
		for (int i = 0; i < activationEffects.size(); i++) {
			CombatInfo targetInfo = state.getTarget();
			if (this.effectTarget.get(i).equals("caster"))
				targetInfo = state.getSource();
			//params.put("effectVal", effectVals.get(i));
			params.put("result", attackerResult);
			//params.put("effectVal", effectVals.get(i));
			params.put("skillType", skillType);
			AgisEffect.EffectState eState = AgisEffect.applyEffect(activationEffects.get(i), state.getSource(), 
					targetInfo, getID(), params, state.getItem());
            
            params.clear();
		}
		
		// Now lets apply the result effects - to be re-added
		/*if (casterResultEffect != null) {
			AgisEffect.EffectState eState = AgisEffect.applyEffect(casterResultEffect, state.getSource(), state.getSource(), params);
	        Log.debug("RESULT: added caster result effect: " + casterResultEffect.getName() + " and id: " + eState.getEffect().getID());
		}
		if (targetResultEffect != null) {
			AgisEffect.EffectState eState = AgisEffect.applyEffect(targetResultEffect, state.getSource(), state.getTarget(), params);
	        Log.debug("RESULT: added target result effect: " + targetResultEffect.getName() + " and id: " + eState.getEffect().getID());
		}*/
		
		// Finally, lets try auto-attack this mob if it is not the caster themselves
        CombatInfo info = state.getSource();
        CombatInfo target = state.getTarget();
        if (!info.getOwnerOid().equals(target.getOwnerOid())) {
        	OID oid = target.getOwnerOid();
        	info.setAutoAttack(oid);
        }
    }

    public void pulseChannelling(AgisAbilityState state) {
        super.pulseChannelling(state);
        AgisEffect.applyEffect(channelEffect, state.getSource(), state.getTarget(), getID());
    }

    public void pulseActivated(AgisAbilityState state) {
        super.pulseActivated(state);
        AgisEffect.applyEffect(activeEffect, state.getSource(), state.getTarget(), getID());
    }
}