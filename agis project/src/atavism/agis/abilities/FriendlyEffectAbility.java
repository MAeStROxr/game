package atavism.agis.abilities;

import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisAbilityState;
import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Ability child class that applies effects to a friendly target.
 * It skips all the to-hit calculations and just straight up applies the effect if 
 * it meets all the basic requirements (enough energy/ close enough etc.).
 * 
 * @author: Andrew Harrison
 */
public class FriendlyEffectAbility extends AgisAbility {
	
	private Map<String, Integer> params;
	public LinkedList<Integer> effectVals;
	protected LinkedList<AgisEffect> activationEffects;
	public LinkedList<String> effectTarget;
	protected AgisEffect channelEffect;
	protected AgisEffect activeEffect;
	
    public FriendlyEffectAbility(String name) {
        super(name);
        this.params = new HashMap<String, Integer>();
        this.effectVals = new LinkedList<Integer>();
        this.activationEffects = new LinkedList<AgisEffect>();
        this.effectTarget = new LinkedList<String>();
        this.activeEffect = null;
        this.channelEffect = null;
    }
    
    protected AbilityResult checkAbility(CombatInfo obj, CombatInfo target,
			ActivationState state) {

        AbilityResult result = super.checkAbility(obj, target, state);
        Log.debug("draive checkabililty effect activated");
        if (result != AbilityResult.SUCCESS) {
        	ExtendedCombatMessages.sendAbilityFailMessage(obj, result);
            return result;
        }
        this.params = new HashMap<String, Integer>();
        return AbilityResult.SUCCESS;
    }

    // List of effect values
    public void addEffectVal(int effect) {
    	effectVals.add(effect);
    }
    public LinkedList<Integer> GetEffectVal() {
        return effectVals;
    }
    
    // Copied from EffectAbility
    public LinkedList<AgisEffect> getActivationEffect() {
        return this.activationEffects;
    }
    public void addActivationEffect(AgisEffect effect) {
    	this.activationEffects.add(effect);
    }
    
    public void addEffectTarget(String target) {
    	effectTarget.add(target);
    }
    public LinkedList<String> getEffectTarget() {
        return effectTarget;
    }

    public AgisEffect getChannelEffect() { return this.channelEffect; }
    public void setChannelEffect(AgisEffect effect) { this.channelEffect = effect; }

    public AgisEffect getActiveEffect() { return this.activeEffect; }
    public void setActiveEffect(AgisEffect effect) { this.activeEffect = effect; }

    public void completeActivation(AgisAbilityState state) {
        super.completeActivation(state);
        
        // Here we need to add in the effectsList to the parameters
		for (int i = 0; i < activationEffects.size(); i++) {
			CombatInfo targetInfo = state.getTarget();
			if (this.effectTarget.get(i).equals("caster"))
				targetInfo = state.getSource();
			//this.params.put("effectVal", this.effectVals.get(i));
			params.put("skillType", skillType);
			params.put("hitRoll", 0);
			Log.debug("Friendly Effect Ability: effect=" + activationEffects.get(i) + ", source=" + state.getSource() + ", target=" + targetInfo);
			AgisEffect.EffectState eState = AgisEffect.applyEffect(activationEffects.get(i), state.getSource(), 
					targetInfo, getID(), params, state.getItem());
            
            params.clear();
		}
    }

    public void pulseChannelling(AgisAbilityState state) {
        super.pulseChannelling(state);
        AgisEffect.applyEffect(channelEffect, state.getSource(), state.getTarget(), getID());
    }

    public void pulseActivated(AgisAbilityState state) {
        super.pulseActivated(state);
        AgisEffect.applyEffect(activeEffect, state.getSource(), state.getTarget(), getID());
    }
}