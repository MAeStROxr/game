package atavism.agis.abilities;

import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisAbilityState;
import atavism.agis.core.AgisEffect;
import atavism.agis.objects.AgisEquipSlot;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.InventoryClient;
import atavism.server.util.Log;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

/**
 * Ability child class that is used for Magical Combat Attacks such as a fireball.
 * Runs some checks on the ability to ensure it can be activated then applies the activation effects.
 * @author Andrew Harrison
 *
 */
public class MagicalAttackAbility extends AgisAbility {
    static Random random = new Random();

    public MagicalAttackAbility(String name) {
        super(name);
    }

    /**
     * [16:25] Andrew: basically the idea behind that is the hit chance calculations 
     * should only be run when the ability is about to be successfully completed
	 * [16:25] Andrew: this function is run like 5 times upon using an ability
  	 * [16:25] Andrew: each time the activationState is changed to the next stage 
     * 
     * @return
     */
    protected AbilityResult checkAbility(CombatInfo obj, CombatInfo target,
                    ActivationState state) {

        AbilityResult result = super.checkAbility(obj, target, state);
        // Check basic results
        if (result != AbilityResult.SUCCESS) {
        	Log.debug("ANDREW - ability failed. Reason: " + result);
        	ExtendedCombatMessages.sendAbilityFailMessage(obj, result);
            return result;
        }
        
        // First we have a few checks of our own
        result = checkPosition(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;
    	
    	result = checkEquip(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;
       
    	
    	// Now check for the ability missing/dodged/crit etc.
    	if (state == ActivationState.ACTIVATING) {
    		//effectParamsA = new HashMap<String, Integer>();
    		//effectParamsT = new HashMap<String, Integer>();
    		params = new HashMap<String, Integer>();
    		changeCoordinatedEffect("success");
    		
    		double hitChance = CombatHelper.CalcMagicalHitChance(obj, target, skillType);
    		
    		// Lets see where the random value falls
    		double rand = random.nextDouble();
    		Log.debug("MAGICALATTACK: random value = " + rand + "; hitChance = " + hitChance);
    		hitRoll = (int)(rand * 100);
    		if (target.getState().equals(CombatInfo.COMBAT_STATE_IMMUNE)) {
    			attackerResult = AgisAbility.RESULT_IMMUNE;
    		} else if (target.getState().equals(CombatInfo.COMBAT_STATE_EVADE)) {
    			attackerResult = AgisAbility.RESULT_EVADED;
    		} else if (rand > 0.95) {
    			attackerResult = AgisAbility.RESULT_MISSED;
    			changeCoordinatedEffect("missed");
    			casterResultEffect = getResultVal("missed", true);
    			targetResultEffect = getResultVal("missed", false);
    			if (casterResultEffect != null && targetResultEffect != null) {
    			    Log.debug("RESULT: set result to missed with caster effect: " + casterResultEffect.getName() 
    					+ " and target effect: " + targetResultEffect.getName());
    			}
    			//effectParamsA.put("effect1", 53);
    			//effectParamsT.put("effect1", 63);
    		} else if (rand > hitChance) {
    			attackerResult = AgisAbility.RESULT_DODGED;
    			changeCoordinatedEffect("dodged");
    			casterResultEffect = getResultVal("dodged", true);
				targetResultEffect = getResultVal("dodged", false);
				if (casterResultEffect != null && targetResultEffect != null) {
    			    Log.debug("RESULT: set result to dodged with caster effect: " + casterResultEffect.getName() 
    					+ " and target effect: " + targetResultEffect.getName());
    			}
			    //effectParamsA.put("effect1", 55);
			    //effectParamsT.put("effect1", 65);
    		} else {
    			attackerResult = AgisAbility.RESULT_HIT;
    		}
    	}
        return AbilityResult.SUCCESS;
    }
    
    /**
     * Needs to be moved to the AgisAbility class.
     * @author Andrew
     *
     */
    private class PlayerAngle {
    	/**
    	 * Must be between 0 and 360
    	 */
    	private float facing;
    	
    	protected PlayerAngle(float angle) {
    		while (angle < 0)
    			angle += 360;
    		while (angle >= 360)
    			angle -= 360;
    		this.facing = angle;
    	}
    	
    	protected boolean is_within(float min, float max, boolean anticlockwise) {
    		if (min < max) {
	    		if (anticlockwise) {
	    			return (this.facing < min || this.facing > max);
	    		} else {
	    			return (this.facing > min && this.facing < max);
	    		}
    		} else {
    			if (anticlockwise) {
	    			return (this.facing < min && this.facing > max);
	    		} else {
	    			return (this.facing > min || this.facing < max);
	    		}
    		}
    	}
    }
    
    /*
     * This method checks to see if the ability requires the player to be at a certain position
     * relative to the target.
     */
    protected AbilityResult checkPosition(CombatInfo obj, CombatInfo target, ActivationState state) {
    	PlayerAngle angle = new PlayerAngle(CombatHelper.calculateValue(obj, target));
    	switch (position) {
	    	case 1:
	    		// Attacker must be in front of the target
	    		if (!angle.is_within(315, 45, false))
	    			return AbilityResult.NOT_IN_FRONT;
	    		break;
	    	case 2:
	    		// Attacker must be to the side of the target
	    		if (!(angle.is_within(45, 135, false) || angle.is_within(225, 315, false)))
	    			return AbilityResult.NOT_BESIDE;
	    		break;
	    	case 3:
	    		// Attacker must be behind the target
	    		if (!(angle.is_within(135, 225, false)))
	    			return AbilityResult.NOT_BEHIND;
	    		break;
    	}
    	return AbilityResult.SUCCESS;
    }
    
    /*
     * This method checks to see if there are any equipment requirements such as having a sword or shield.
     * Needs updated.
     */
    protected AbilityResult checkEquip(CombatInfo obj, CombatInfo target, ActivationState state) {
    	if (!(weaponReq.equals(""))) {
    		OID ioid = (OID)AgisInventoryClient.findItem(obj.getOid(), AgisEquipSlot.PRIMARYWEAPON);
            Log.error("primary weapon ioid = " + ioid);
            String weap = (String)EnginePlugin.getObjectProperty(ioid, InventoryClient.ITEM_NAMESPACE, "type");
    	}
    	
        return AbilityResult.SUCCESS;
    }
    
    private Map<String, Integer> params = null;
    private AgisEffect casterResultEffect = null;
    private AgisEffect targetResultEffect = null;
    //private Map<String, Integer> effectParamsA = null;
    //private Map<String, Integer> effectParamsT = null;
    private int attackerResult = 0;
    private int hitRoll = 0;
    
    // Does this ability require any special positionals?
    // 0 = none; 1 = front; 2 = sides; 3 = back
    public void SetPositional(int positional) {
    	position = positional;
    }
    public int GetPositional() {
        return position;
    }
    public int position = 0;
    
    // Does this ability require any special positions?
    public void SetShieldReq(int shieldNeeded) {
    	shieldReq = shieldNeeded;
    }
    public int GetShieldReq() {
        return shieldReq;
    }
    public int shieldReq = 0;

    // Copied from EffectAbility
    public LinkedList<AgisEffect> getActivationEffect() {
        return activationEffects;
    }
    public void addActivationEffect(AgisEffect effect) {
    	if (activationEffects == null) {
    		activationEffects = new LinkedList<AgisEffect>();
    	}
    	activationEffects.add(effect);
    }
    protected LinkedList<AgisEffect> activationEffects = null;
    
    public void addEffectTarget(String target) {
    	if (effectTarget == null) {
    		effectTarget = new LinkedList<String>();
    	}
    	effectTarget.add(target);
    }
    public LinkedList<String> GetEffectTarget() {
        return effectTarget;
    }
    public LinkedList<String> effectTarget = null;
    
    
    public AgisEffect getChannelEffect() {
        return channelEffect;
    }
    public void setChannelEffect(AgisEffect effect) {
        this.channelEffect = effect;
    }
    protected AgisEffect channelEffect = null;

    
    public AgisEffect getActiveEffect() {
        return activeEffect;
    }
    public void setActiveEffect(AgisEffect effect) {
        this.activeEffect = effect;
    }
    protected AgisEffect activeEffect = null;


    public void completeActivation(AgisAbilityState state) {
        super.completeActivation(state);
		state.getSource().setCombatState(true);
		// Here we need to add in the effectsList to the parameters
		for (int i = 0; i < activationEffects.size(); i++) {
			CombatInfo targetInfo = state.getTarget();
			String target = effectTarget.get(i);
			if (target.equals("caster"))
				targetInfo = state.getSource();
			params.put("result", attackerResult);
			params.put("skillType", skillType);
			params.put("hitRoll", hitRoll);
			AgisEffect.EffectState eState = AgisEffect.applyEffect(activationEffects.get(i), state.getSource(), 
					targetInfo, getID(), params, state.getItem());
            
            params.clear();
		} //end for
        
        // Now lets apply the result effects - These need to be re-added
		/*if (casterResultEffect != null) {
			AgisEffect.EffectState eState = AgisEffect.applyEffect(casterResultEffect, state.getSource(), state.getSource(), params);
	        Log.debug("RESULT: added caster result effect: " + casterResultEffect.getName() + " and id: " + eState.getEffect().getID());
		}
		if (targetResultEffect != null) {
			AgisEffect.EffectState eState = AgisEffect.applyEffect(targetResultEffect, state.getSource(), state.getTarget(), params);
	        Log.debug("RESULT: added target result effect: " + targetResultEffect.getName() + " and id: " + eState.getEffect().getID());
		}*/
        
        // Finally, lets try auto-attack this mob
        CombatInfo info = state.getSource();
        CombatInfo target = state.getTarget();
    	OID oid = target.getOwnerOid();
        info.setAutoAttack(oid);
    }

    public void pulseChannelling(AgisAbilityState state) {
        super.pulseChannelling(state);
        AgisEffect.applyEffect(channelEffect, state.getSource(), state.getTarget(), getID());
    }

    public void pulseActivated(AgisAbilityState state) {
        super.pulseActivated(state);
        AgisEffect.applyEffect(activeEffect, state.getSource(), state.getTarget(), getID());
    }

}
