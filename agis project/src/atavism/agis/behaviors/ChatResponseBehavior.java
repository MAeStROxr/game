package atavism.agis.behaviors;

import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.MessageTypeFilter;
import atavism.server.engine.Behavior;
import atavism.server.engine.Engine;
import atavism.server.plugins.WorldManagerClient;

import java.util.HashMap;
import java.util.Map;

public class ChatResponseBehavior extends Behavior implements MessageCallback {
    public void initialize() {
	MessageTypeFilter filter = new MessageTypeFilter();
	filter.addType(WorldManagerClient.MSG_TYPE_COM);
	eventSub = Engine.getAgent().createSubscription(filter, this);
    }
    
    public void activate() {
    }

    public void deactivate() {
	if (eventSub != null) {
	    Engine.getAgent().removeSubscription(eventSub);
	    eventSub = null;
	}
        //return true;
    }

    public void handleMessage(Message msg, int flags) {
        if (msg instanceof WorldManagerClient.ComMessage) {
            WorldManagerClient.ComMessage comMsg = (WorldManagerClient.ComMessage)msg;
            String response = responses.get(comMsg.getString());
            if (response != null && MessageAgent.responseExpected(flags)) {
                WorldManagerClient.sendChatMsg(obj.getOid(), "", 1, response);
            }
        }
    }
        
    public void addChatResponse(String trigger, String response) {
	responses.put(trigger, response);
    }

    Map<String, String> responses = new HashMap<String, String>();
    Long eventSub = null;

    private static final long serialVersionUID = 1L;
}