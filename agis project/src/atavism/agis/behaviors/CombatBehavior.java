package atavism.agis.behaviors;

import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.plugins.*;
import atavism.agis.plugins.GroupClient.GroupInfo;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.math.Point;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.EntityHandle;
import atavism.server.objects.ObjectTracker;
import atavism.server.objects.SpawnData;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedPropertyMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class CombatBehavior extends Behavior implements MessageCallback, Runnable {
    public CombatBehavior() {
    	super();
    }

    public CombatBehavior(SpawnData data) {
    	super(data);
    	String value = (String)data.getProperty("combat.reactionRadius");
    	if (value != null) {
    		setReactionRadius(Integer.valueOf(value));
    	}
    	value = (String)data.getProperty("combat.movementSpeed");
    	if (value != null) {
    		setMovementSpeed(Integer.valueOf(value));
    	}
    }

    public void initialize() {
        SubjectFilter filter = new SubjectFilter(obj.getOid());
        filter.addType(CombatClient.MSG_TYPE_DAMAGE);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter.addType(CombatClient.MSG_TYPE_COMBAT_LOGOUT);
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_AGGRO_RADIUS);
        filter.addType(CombatClient.MSG_TYPE_AUTO_ATTACK_COMPLETED);
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        filter.addType(CombatClient.MSG_TYPE_ALTER_THREAT);
        //filter.addType(AgisMobClient.MSG_TYPE_INVALID_PATH);
        //filter.addType(WorldManagerClient.MSG_TYPE_UPDATE_OBJECT);
        eventSub = Engine.getAgent().createSubscription(filter, this);

    }
    
    /**
     * Called when a mob respawns, clear all behaviour variables here or they will carry over to new spawns
     */
    public void activate() {
	    activated = true;
	    Log.debug("CombatBehavior.activate: adding reaction radius");
	    MobManagerPlugin.getTracker(obj.getInstanceOid()).addReactionRadius(obj.getOid(), reactionRadius);
	    threatMap.clear();
	    tagOwner = null;
	    hitBoxRange = (Integer)EnginePlugin.getObjectProperty(obj.getOid(), CombatClient.NAMESPACE, CombatPlugin.PROP_HITBOX);
    }
    
    public void deactivate() {
    	lock.lock();
    	try {
    		activated = false;
    		if (eventSub != null) {
    			Engine.getAgent().removeSubscription(eventSub);
    			eventSub = null;
    		}
    		if (eventSub2 != null) {
    			Engine.getAgent().removeSubscription(eventSub2);
    			eventSub2 = null;
    		}
    	}
    	finally {
    		lock.unlock();
    	}
    }

    public void handleMessage(Message msg, int flags) {
	    lock.lock();
	    try {
	    if (activated == false)
		    return;
	    if (msg instanceof CombatClient.DamageMessage) {
		    CombatClient.DamageMessage dmgMsg = (CombatClient.DamageMessage)msg;
		    //attackTarget(dmgMsg.getAttackerOid());
		    addTargetToThreatMap(dmgMsg.getAttackerOid(), dmgMsg.getDmg());
		    if (tagOwner == null && dmgMsg.getAttackerOid() != null) {
		    	Log.debug("TAG: setting tagOwner to: " + dmgMsg.getAttackerOid());
		    	tagOwner = dmgMsg.getAttackerOid();
		    }
		    
		    if (CombatPlugin.EXP_BASED_ON_DAMAGE_DEALT)
		    	addDamageToMap(dmgMsg.getAttackerOid(), dmgMsg.getDmg());
	    }
	    if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_AGGRO_RADIUS) {
	    	ObjectTracker.NotifyAggroRadiusMessage nMsg = (ObjectTracker.NotifyAggroRadiusMessage)msg;
            Log.debug("CombatBehavior.onMessage: got in aggro range message=" + nMsg);
            OID subjectOid = nMsg.getSubject();
            if (inCombat || evade) {
            	Log.debug("CombatBehavior.onMessage: mob is in combat=" + inCombat + " or evade=" + evade);
            	return;
            }
            // Check if target is dead
            if (!CombatPlugin.isPlayerAlive(subjectOid)) {
            	return;
            }
            // check if target is Immune or a Spirit
            String state = (String)EnginePlugin.getObjectProperty(subjectOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_STATE);
            if (state != null && state.equals(CombatInfo.COMBAT_STATE_IMMUNE) || state.equals(CombatInfo.COMBAT_STATE_SPIRIT)) {
            	return;
            }
            //attackTarget(subjectOid);
            if (!threatMap.containsKey(subjectOid)) {
            	addTargetToThreatMap(subjectOid, 0);
	    	} else if (threatMap.get(subjectOid) < 0) {
	    		setTargetThreat(subjectOid, 0);
	    	}
            // Play Aggro coord effect
	        CoordinatedEffect cE = new CoordinatedEffect("AggroEffect");
	    	cE.sendSourceOid(true);
	    	cE.sendTargetOid(true);
	    	cE.invoke(obj.getOid(), subjectOid);
	    }
	    if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS) {
    	    ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage)msg;
     	    Log.debug("CombatBehavior.onMessage: myOid=" + obj.getOid() + " subject=" + nMsg.getSubject()
     		      + " inRadius=" + nMsg.getInRadius());
     	    // Remove target from threat map if they are no longer in radius
    	    if (!nMsg.getInRadius() && threatMap.containsKey(nMsg.getSubject())) {
    	    	removeTargetFromThreatMap(nMsg.getSubject()); 
    	    }
	    }
	    if (msg instanceof PropertyMessage) {
	    	PropertyMessage propMsg = (PropertyMessage) msg;
		    OID subject = propMsg.getSubject();
		    Boolean dead = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);
		    if (dead != null && dead) {
                Log.debug("CombatBehavior.onMessage: obj=" + obj + " got death=" + propMsg.getSubject() + " currentTarget=" + currentTarget);
		        if (subject.equals(obj.getOid())) {
		        	handleDeath(null);
			        Log.debug("CombatBehavior.onMessage: mob died, deactivating all behaviors");
			        for(Behavior behav : obj.getBehaviors()) {
			            behav.deactivate();
			            obj.removeBehavior(behav);
			        }
			        EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), WorldManagerClient.NAMESPACE, "facing", null);
		        }
		        else if (currentTarget != null && currentTarget.equals(subject)) {
		    		currentTarget = null;
		    		Log.debug("FACING: set current target: " + subject + " to null");
		    		EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), WorldManagerClient.NAMESPACE, "facing", null);
		    		attackTarget(null);
		    	}
		    	if (threatMap.containsKey(subject)) {
		    		//removeTargetFromThreatMap(subject); 
		    		setTargetThreat(subject, -1000);
		    		TargetedPropertyMessage newPropMsg = new TargetedPropertyMessage(subject, obj.getOid());
		    		newPropMsg.setProperty("aggressive", false);
		            Engine.getAgent().sendBroadcast(newPropMsg);
		    		Log.debug("FACING: removed: " + subject + " from threatmap");
		    	}
		    	//damageMap.remove(subject); // Not sure whether this should happen?
		    } else if (dead != null && !dead && !subject.equals(obj.getOid())) {
		    	Log.debug("REVIVE: obj=" + obj + " got life=" + propMsg.getSubject() + " currentTarget=" + currentTarget);
		    	// Someone just came alive, add to thread map?
		    	if (threatMap.containsKey(subject)) {
		    		// Do a distance check to see if they are within aggro range
		    		InterpolatedWorldNode wnode = obj.getWorldNode();
		    		BasicWorldNode targetWNode = WorldManagerClient.getWorldNode(subject);
		    		Log.debug("AGGRO: getting distance from wnode: " + wnode.getLoc() + " to target: " + targetWNode.getLoc()
		    				+ " and aggro radius: " + FactionPlugin.AGGRO_RADIUS);
		    		if (Point.distanceTo(wnode.getLoc(), targetWNode.getLoc()) < FactionPlugin.AGGRO_RADIUS)
		    			setTargetThreat(subject, 0);
		    	}
		    }
		    
		    // Handle movement speed property change
		    Integer movementSpeed = (Integer)propMsg.getProperty(AgisWorldManagerPlugin.PROP_MOVEMENT_SPEED);
		    if (movementSpeed != null) {
		    	speed = movementSpeed;
		    	Log.debug("SPEED: set mob speed to: " + speed);
		    }
		    String combatstate = (String)propMsg.getProperty(CombatInfo.COMBAT_PROP_STATE);
		    if (combatstate != null && combatstate.equals("")) {
		    	if (currentTarget != null) {
		    		attackTarget(currentTarget);
		    	}
		    	Log.debug("STATE: got mob state set back to null");
		    }
	    } 
	    if (msg.getMsgType() == WorldManagerClient.MSG_TYPE_DESPAWNED) {
	    	WorldManagerClient.DespawnedMessage despawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
			OID objOid = despawnedMsg.getSubject();
			Log.debug("DESPAWNED: got target despawned: " + objOid);
			if (threatMap.containsKey(objOid)) {
				removeTargetFromThreatMap(objOid); 
				damageMap.remove(objOid);
    	    }
	    }
	    if (msg.getMsgType() == CombatClient.MSG_TYPE_COMBAT_LOGOUT) {
	    	CombatClient.CombatLogoutMessage clMsg = (CombatClient.CombatLogoutMessage) msg;
	    	OID subjectOid = clMsg.getSubject();
	    	OID playerOid = clMsg.getPlayerOid();
	    	Log.debug("Logout reaction. Obj: " + obj.getOid() + "; target: " + playerOid + "; subject: " + subjectOid);
	    	targetsInRange.remove(playerOid);
	    	removeTargetFromThreatMap(playerOid);
	    	damageMap.remove(playerOid);
	    	attackTarget(null);
	    	//CombatClient.updateRemoveAttitude(obj.getOid(), playerOid);
	    }
	    if (msg.getMsgType() == AgisMobClient.MSG_TYPE_INVALID_PATH) {
	    	//TODO
	    	attackTarget(null);
    		Engine.getAgent().sendBroadcast(new BaseBehavior.GotoCommandMessage(obj, centerLoc, speed));
		    // Set evade to true for 10 seconds
    		Log.debug("Evade set to true 1");
		    evade = true;
		    inCombat = false;
		    Engine.getExecutor().schedule(cdt, 10000, TimeUnit.MILLISECONDS);
		    Log.debug("Evade set to true 2");
		    EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), WorldManagerClient.NAMESPACE, "facing", null);
		    CombatClient.setCombatInfoState(obj.getOid(), CombatInfo.COMBAT_STATE_EVADE);
	    }
	    if (msg instanceof CombatClient.AutoAttackCompletedMessage) {
		    Log.debug("AUTO: got auto attack completed");
		    Engine.getAgent().sendBroadcast(new BaseBehavior.StopCommandMessage(obj));
		    Engine.getExecutor().schedule(this, 1000, TimeUnit.MILLISECONDS);
	    }
	    if (msg.getMsgType() == CombatClient.MSG_TYPE_ALTER_THREAT) {
	    	CombatClient.AlterThreatMessage clMsg = (CombatClient.AlterThreatMessage) msg;
	    	addTargetToThreatMap(clMsg.getAttackerOid(), clMsg.getThreatChange());
	    }
	    //return true;
	    } finally {
	        lock.unlock();
	    }
    }

    protected void attackTarget(OID targetOid) {
        if (Log.loggingDebug)
            Log.debug("CombatBehavior.attackTarget: obj=" + obj + " targetOid=" + targetOid);
        /*if (targetSub != null) {
        	Engine.getAgent().removeSubscription(targetSub);
        }*/
        
        currentTarget = targetOid;
        
        if (currentTarget != null) {
	        /*SubjectFilter filter = new SubjectFilter(targetOid);
	        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
	        targetSub = Engine.getAgent().createSubscription(filter, this);*/

	        Engine.getAgent().sendBroadcast(new BaseBehavior.FollowCommandMessage(obj, new EntityHandle(currentTarget), speed, hitBoxRange));
	        CombatClient.autoAttack(obj.getOid(), currentTarget, true);
	        inCombat = true;
	        Engine.getExecutor().schedule(cdt, 1000, TimeUnit.MILLISECONDS);
	        // Get mob to face target
	        EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), WorldManagerClient.NAMESPACE, "facing", currentTarget);
	    } else {
	        CombatClient.autoAttack(obj.getOid(), null, false);
	        //TODO: If the mob is not at their original position, they need to return to it?
	        Point loc = obj.getWorldNode().getLoc();
	        if (Point.distanceTo(loc, centerLoc) > 0.5) {
	        	Engine.getAgent().sendBroadcast(new BaseBehavior.GotoCommandMessage(obj, centerLoc, speed));
	        }
	        Engine.getAgent().sendBroadcast(new BaseBehavior.ArrivedEventMessage(obj));
	        inCombat = false;
	        Log.debug("FACING: set current target: " + currentTarget + " to null");
    		EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), WorldManagerClient.NAMESPACE, "facing", null);
	    }
    }
    
    protected void handleDeath(OID killer) {
    	Log.debug("DEATH: got handleDeath with killer: " + killer);
    	inCombat = false;
    	//CombatInfo info = CombatPlugin.getCombatInfo(obj.getOid());
        //OID tagOwnerOid = (OID) EnginePlugin.getObjectProperty(obj.getOid(), CombatClient.NAMESPACE, CombatInfo.COMBAT_TAG_OWNER);
        Log.debug("TAG: got tagOwner: " + tagOwner);
        
    	// Send mob killed message
        ExtensionMessage killedMsg = new ExtensionMessage(AgisMobClient.MSG_TYPE_MOB_KILLED, null, obj.getOid());
        killedMsg.setProperty("killer", killer);
        killedMsg.setProperty("mobType", 0);
        //killedMsg.setProperty("scriptID", scriptID);
        Engine.getAgent().sendBroadcast(killedMsg);
        
        // Set mob not attackable
        EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_ATTACKABLE, false);
        
        // Send alter exp amount message
        if (AgisMobPlugin.MOB_DEATH_EXP) {
        	if (CombatPlugin.EXP_BASED_ON_DAMAGE_DEALT) {
        		// Work out if any members are part of the same group
        		HashMap<OID, OID> groupMapping = new HashMap<OID, OID>();
        		Set<OID> attackers = damageMap.keySet();
        		for (OID attacker : attackers) {
        			GroupInfo gInfo = GroupClient.GetGroupMemberOIDs(attacker);
        			if (gInfo.groupOid != null) {
        				if (groupMapping.containsKey(gInfo.groupOid)) {
        					int totalGroupDamage = damageMap.get(groupMapping.get(gInfo.groupOid)) + damageMap.get(attacker);
        					damageMap.put(groupMapping.get(gInfo.groupOid), totalGroupDamage);
        					damageMap.remove(attacker);
        				} else {
        					groupMapping.put(gInfo.groupOid, attacker);
        				}
        			}
        		}
        		ClassAbilityClient.rewardExpForKill(obj.getOid(), damageMap);
        	} else if (tagOwner != null) {
        		HashMap<OID, Integer> attackers = new HashMap<OID, Integer>();
        		attackers.put(tagOwner, 1);
        		ClassAbilityClient.rewardExpForKill(obj.getOid(), attackers);
        	}
        }
        
    	if (AgisMobPlugin.lootObjectTmpl == -1) {
    		Log.debug("DEATH: sending generateLoot property");
    		// If there is no loot object prefab, just send the generateLoot message
    		if (AgisMobPlugin.MOB_DEATH_EXP) {
    			//TODO: work out how to get group who did most damage to then become the looter
    			OID lootTarget = tagOwner;
    			
    			TargetedPropertyMessage propMsg = new TargetedPropertyMessage(lootTarget, obj.getOid());
                propMsg.setProperty("lootable", true);
                Engine.getAgent().sendBroadcast(propMsg);
        		AgisInventoryClient.generateLoot(obj.getOid());
    		} else {
    			TargetedPropertyMessage propMsg = new TargetedPropertyMessage(tagOwner, obj.getOid());
                propMsg.setProperty("lootable", true);
                Engine.getAgent().sendBroadcast(propMsg);
        		AgisInventoryClient.generateLoot(obj.getOid());
    		}
    		
    		// TEMP: Send create skinning message:
    		Integer skinningLootTable = (Integer) EnginePlugin.getObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, "skinningLootTable");
    		if (skinningLootTable != null && skinningLootTable > 0) {
    			Integer skinningLevelReq = (Integer) EnginePlugin.getObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, "skinningLevelReq");
    			CraftingClient.sendCreateResourceNodeFromMobMessage(obj.getOid(), skinningLootTable, skinningLevelReq, 2);
    		}
            
    		damageMap.clear();
    		return;
    	}
    	damageMap.clear();
    	
        // Send message to spawn loot mobs for the targets to pick up
        SpawnData spawnData = new SpawnData();
        spawnData.setProperty("id", 1);
        spawnData.setTemplateID(AgisMobPlugin.lootObjectTmpl);
        spawnData.setInstanceOid(obj.getInstanceOid());
        BasicWorldNode node = WorldManagerClient.getWorldNode(obj.getOid());
        Point spawnLoc = node.getLoc();
        spawnLoc.add(0, 1, 0);
        spawnData.setLoc(spawnLoc);
        spawnData.setOrientation(node.getOrientation());
        spawnData.setNumSpawns(1);
        spawnData.setSpawnRadius(0);
        spawnData.setRespawnTime(-1); // Don't respawn;
        spawnData.setCorpseDespawnTime(500);
        spawnData.setProperty("lootTables", lootTables);
        
        HashMap<String, Serializable> spawnProps = new HashMap<String, Serializable>();
        ArrayList<OID> acceptableTargets = new ArrayList<OID>();
        acceptableTargets.add(tagOwner);
        /*OID highestThreatOid = null;
        int highestThreat = -1;
        for (OID playerOid : threatMap.keySet()) {
        	Log.debug("Comparing threat: " + threatMap.get(playerOid) + " against highest: " + highestThreat);
        	if (threatMap.get(playerOid) > highestThreat) {
        		highestThreatOid = playerOid;
        		highestThreat = threatMap.get(playerOid);
        	}
        }
        acceptableTargets.add(highestThreatOid);
        Log.error("Acceptable targets: " + acceptableTargets);*/
        spawnProps.put("acceptableTargets", acceptableTargets);
        //spawnProps.put("duration", 30000); // exist for up to 30 seconds
        spawnData.setProperty("props", spawnProps);
        ExtensionMessage spawnMsg = new ExtensionMessage();
        spawnMsg.setMsgType(AgisMobClient.MSG_TYPE_SPAWN_DOME_MOB);
        spawnMsg.setProperty("spawnData", spawnData);
        spawnMsg.setProperty("spawnType", -4 /*Dome.MOBTYPE_LOOT*/);
        spawnMsg.setProperty("roamRadius", 0);
        Engine.getAgent().sendBroadcast(spawnMsg);
        
    }
    
    public void run() {
    	if (activated == false) {
    	    return;
    	}
    	
    	if (currentTarget != null) {
    		Engine.getAgent().sendBroadcast(new BaseBehavior.FollowCommandMessage(obj, new EntityHandle(currentTarget), speed, hitBoxRange));
    	}
    }
    
    /**
     * A runnable class to check if the mob has chased a target too far from its original location.
     * @author Andrew
     *
     */
    class CheckDistanceTravelled implements Runnable {
    	public void run() {
    		checkDistance();
    	}
    	
    	private void checkDistance() {
        	if (centerLoc == null) {
        		return;
        	}
        	InterpolatedWorldNode wnode = obj.getWorldNode();
        	if (wnode == null) {
        		Log.error("AGGRO: got null wnode during distance check for oid: " + obj.getOid());
        		return;
        	}
        	Point loc = wnode.getCurrentLoc();
        	float distance = Point.distanceTo(loc, centerLoc);
        	//Log.debug("COMBAT: distance from centerLoc: " + distance);
        	if ((distance > chaseDistance) && (evade == false)) {
        		Log.debug("COMBAT: mob has exceeded max distance: " + chaseDistance);
        		// Add in some call to stop attacking
        		//CombatClient.autoAttack(obj.getOid(), null, false);
        		attackTarget(null);
        		Engine.getAgent().sendBroadcast(new BaseBehavior.GotoCommandMessage(obj, centerLoc, speed));
    		    // Set evade to true for 10 seconds
        		Log.debug("Evade set to true 1");
    		    evade = true;
    		    inCombat = false;
    		    threatMap.clear();
    		    Engine.getExecutor().schedule(this, 10000, TimeUnit.MILLISECONDS);
    		    Log.debug("Evade set to true 2");
    		    EnginePlugin.setObjectPropertyNoResponse(obj.getOid(), WorldManagerClient.NAMESPACE, "facing", null);
    		    CombatClient.setCombatInfoState(obj.getOid(), CombatInfo.COMBAT_STATE_EVADE);
        	} else if (evade == true) {
        	    evade = false;
        	    CombatClient.setCombatInfoState(obj.getOid(), "");
        	    Log.debug("Evade set to false");
        	} else {
        		// Also check if the mob is too far from the target, if so - get it to follow the target again
        		/*if (currentTarget != null) {
        			Engine.getAgent().sendBroadcast(new BaseBehavior.FollowCommandMessage(obj, new EntityHandle(currentTarget), speed));
        		}*/
        		Engine.getExecutor().schedule(this, 1000, TimeUnit.MILLISECONDS);
        	}
        }
    }
    
    public void setMovementSpeed(int speed) { this.speed = new Integer(speed); }
    public int getMovementSpeed() { return speed.intValue(); }
    protected Integer speed = new Integer(6);

    public void setReactionRadius(int radius) { reactionRadius = radius; }
    public int getReactionRadius() { return reactionRadius.intValue(); }
    protected Integer reactionRadius = new Integer(70);
    
    public void setCenterLoc(Point loc) {centerLoc = loc; }
    public Point getCenterLoc() { return centerLoc; }
    protected Point centerLoc = null;
    
    public void setchaseDistance(int distance) { chaseDistance = distance; }
    public int getchaseDistance() { return chaseDistance; }
    int chaseDistance = 60; // How far the mob will chase a player in meters
    
    CheckDistanceTravelled cdt = new CheckDistanceTravelled();
    LinkedList<OID> targetsInRange = new LinkedList<OID>();
    
    public void setAggroRange(int radius) { aggroRange = radius; }
    public int getAggroRange() { return aggroRange; }
    int aggroRange = 5;
    
    public void setHitBoxRange(float radius) { hitBoxRange = radius; }
    public float getHitBoxRange() { return hitBoxRange; }
    float hitBoxRange = 2;
    
    /**
	 * Add the amount of threat for the given oid to the threatMap. If the oid already
	 * has an entry then we just add to it.
	 * @param oid
	 * @param threat
	 */
    protected void addTargetToThreatMap(OID targetOid, int threatAmount) {
    	Log.debug("THREAT: adding " + targetOid + " to threat map with threatAmount: " + threatAmount);
    	if (threatMap.containsKey(targetOid)) {
    		threatMap.put(targetOid, threatMap.get(targetOid) + threatAmount);
    	} else {
    		threatMap.put(targetOid, threatAmount);
    		TargetedPropertyMessage propMsg = new TargetedPropertyMessage(targetOid, obj.getOid());
            propMsg.setProperty("aggressive", true);
            Engine.getAgent().sendBroadcast(propMsg);
            SubjectFilter filter = new SubjectFilter(targetOid);
	        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
	        filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
	        targetSubs.put(targetOid, Engine.getAgent().createSubscription(filter, this));
    	}
    	threatUpdated();
    }
    
    protected void removeTargetFromThreatMap(OID targetOid) {
    	threatMap.remove(targetOid);
    	Engine.getAgent().removeSubscription(targetSubs.get(targetOid));
    	targetSubs.remove(targetOid);
    	threatUpdated();
    	if (tagOwner != null && tagOwner.equals(targetOid)) {
    		tagOwner = null;
    		Log.debug("TAG: tagOwner removed: " + targetOid);
    	}
    }
    
	public void decreaseTargetThreat(OID targetOid, int amount) {
		if (threatMap.containsKey(targetOid)) {
			threatMap.put(targetOid, threatMap.get(targetOid) - amount);
		}
		threatUpdated();
	}
	
	/**
	 * Use when a player has died to set threat to -1.
	 * @param targetOid
	 * @param amount
	 */
	public void setTargetThreat(OID targetOid, int amount) {
		threatMap.put(targetOid, amount);
		threatUpdated();
	}
	
	void threatUpdated() {
		// Update the current target to the person with the highest threat
    	OID highestThreatOid = null;
        int highestThreat = -1;
        for (OID playerOid : threatMap.keySet()) {
        	Log.debug("THREAT: Comparing threat: " + threatMap.get(playerOid) + " against highest: " + highestThreat);
        	if (threatMap.get(playerOid) > highestThreat) {
        		highestThreatOid = playerOid;
        		highestThreat = threatMap.get(playerOid);
        	}
        }
        Log.debug("THREAT: highest threat: " + highestThreatOid + " against currentTarget: " + currentTarget);
    	if (highestThreatOid != currentTarget) {
    		currentTarget = highestThreatOid;
	    	attackTarget(currentTarget);
    	}
	}
    
	public void addDamageToMap(OID targetOid, int amount) {
		if (damageMap.containsKey(targetOid)) {
			damageMap.put(targetOid, damageMap.get(targetOid) + amount);
		} else {
			damageMap.put(targetOid, amount);
		}
	}
	
	public void setThreatMap(HashMap<OID, Integer> threatMap) {
		this.threatMap = threatMap;
	}
	public HashMap<OID, Integer> getThreatMap() {
		return threatMap;
	}
	protected HashMap<OID, Integer> threatMap = new HashMap<OID, Integer>();
	
	public void setDamageMap(HashMap<OID, Integer> damageMap) {
		this.damageMap = damageMap;
	}
	public HashMap<OID, Integer> getDamageMap() {
		return damageMap;
	}
	protected HashMap<OID, Integer> damageMap = new HashMap<OID, Integer>();
	
	public void setLootTables(HashMap<Integer, Integer> tables) 
	{ 
		lootTables = tables; 
	}
	
	protected HashMap<Integer, Integer> lootTables = new HashMap<Integer, Integer>();
    
    Long eventSub = null;
    HashMap<OID, Long> targetSubs = new HashMap<OID, Long>();
    Long eventSub2 = null;
    boolean evade = false;
    boolean inCombat = false;
    protected OID currentTarget = null;
    protected OID tagOwner = null;
    protected boolean activated = false;
    private static final long serialVersionUID = 1L;
}
