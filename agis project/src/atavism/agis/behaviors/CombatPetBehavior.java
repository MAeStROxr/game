package atavism.agis.behaviors;

import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.AgisMobClient;
import atavism.agis.plugins.CombatClient;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.math.Point;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.EntityHandle;
import atavism.server.objects.SpawnData;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class CombatPetBehavior extends Behavior implements MessageCallback, Runnable {
    public CombatPetBehavior() {
	super();
    }

    public CombatPetBehavior(SpawnData data) {
    	super(data);
    	String value = (String)data.getProperty("combat.reactionRadius");
    	if (value != null) {
    		setReactionRadius(Integer.valueOf(value));
    	}
    	value = (String)data.getProperty("combat.movementSpeed");
    	if (value != null) {
    		setMovementSpeed(Integer.valueOf(value));
    	}
    }

    public void initialize() {
        SubjectFilter filter = new SubjectFilter(obj.getOid());
        filter.addType(CombatClient.MSG_TYPE_DAMAGE);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter.addType(CombatClient.MSG_TYPE_COMBAT_LOGOUT);
        filter.addType(AgisMobClient.MSG_TYPE_PET_COMMAND_UPDATE);
        //filter.addType(WorldManagerClient.MSG_TYPE_UPDATE_OBJECT);
        eventSub = Engine.getAgent().createSubscription(filter, this);
        
        // Another filter to pick up when a players faction rep changes
        MessageTypeFilter filter2 = new MessageTypeFilter();
        filter2.addType(CombatClient.MSG_TYPE_FACTION_UPDATE);
        eventSub2 = Engine.getAgent().createSubscription(filter2, this);
    }
    public void activate() {
	    activated = true;
	    Log.debug("CombatBehavior.activate: adding reaction radius");
	    MobManagerPlugin.getTracker(obj.getInstanceOid()).addReactionRadius(obj.getOid(), reactionRadius);
	    Engine.getExecutor().scheduleAtFixedRate(this, 10, 1, TimeUnit.SECONDS);
    }
    public void deactivate() {
	lock.lock();
	try {
	    activated = false;
	    if (eventSub != null) {
	        Engine.getAgent().removeSubscription(eventSub);
		    eventSub = null;
	    }
	    if (eventSub2 != null) {
	        Engine.getAgent().removeSubscription(eventSub2);
		    eventSub2 = null;
	    }
	}
	finally {
	    lock.unlock();
	}
    }

    public void handleMessage(Message msg, int flags) {
	    lock.lock();
	    try {
	    if (activated == false)
		    return;
	    // Command handlers
	    if (msg.getMsgType() == AgisMobClient.MSG_TYPE_PET_COMMAND_UPDATE) {
	    	AgisMobClient.petCommandUpdateMessage pcuMsg = (AgisMobClient.petCommandUpdateMessage) msg;
	    	int commandVal = pcuMsg.getCommand();
	    	if (commandVal == -3) {
	    		attackTarget(pcuMsg.getTarget());
	    		currentCommand = -3;
	    	} else if (commandVal == -2) {
	    		Engine.getAgent().sendBroadcast(new BaseBehavior.FollowCommandMessage(obj, new EntityHandle(ownerOid), speed, hitBoxRange));
	    		currentCommand = -2;
	    	} else if (commandVal == -1) {
	    		Engine.getAgent().sendBroadcast(new BaseBehavior.StopCommandMessage(obj));
	    		currentCommand = -1;
	    	} else if (commandVal == 1) {
	    		CombatClient.autoAttack(obj.getOid(), null, false);
	    		attitude = 1;
	    	} else if (commandVal == 2) {
	    		attitude = 2;
	    	} else if (commandVal == 3) {
	    		attitude = 3;
	    	} 
	    }
	    if (msg instanceof CombatClient.DamageMessage) {
		    CombatClient.DamageMessage dmgMsg = (CombatClient.DamageMessage)msg;
		    OID attackerOid = dmgMsg.getAttackerOid();
		    if (attitude != 1 && !attackerOid.equals(obj.getOid()))
		        attackTarget(attackerOid);
	    }
	    if (msg instanceof PropertyMessage) {
		    PropertyMessage propMsg = (PropertyMessage) msg;
		    Boolean dead = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);
		    if (dead != null && dead) {
                if (Log.loggingDebug)
                    Log.debug("CombatBehavior.onMessage: obj=" + obj + " got death=" + propMsg.getSubject() + " currentTarget=" + currentTarget);
		        if (propMsg.getSubject() == obj.getOid()) {
			        Log.debug("CombatBehavior.onMessage: mob died, deactivating all behaviors");
			        for(Behavior behav : obj.getBehaviors()) {
			            behav.deactivate();
			            obj.removeBehavior(behav);
			        }
		        }
		        else if (propMsg.getSubject() == currentTarget) {
			        attackTarget(null);
		        }
		    }
	    } 
	    if (msg.getMsgType() == CombatClient.MSG_TYPE_COMBAT_LOGOUT) {
	    	CombatClient.CombatLogoutMessage clMsg = (CombatClient.CombatLogoutMessage) msg;
	    	OID subjectOid = clMsg.getSubject();
	    	OID playerOid = clMsg.getPlayerOid();
	    	Log.debug("Logout reaction. Obj: " + obj.getOid() + "; target: " + playerOid + "; subject: " + subjectOid);
	    	targetsInRange.remove(playerOid);
	    	//CombatClient.updateRemoveAttitude(obj.getOid(), playerOid);
	    }
	    //return true;
	    } finally {
	        lock.unlock();
	    }
    }

    protected void attackTarget(OID targetOid) {
        if (Log.loggingDebug)
            Log.debug("CombatBehavior.attackTarget: obj=" + obj + " targetOid=" + targetOid);
        if (targetSub != null) {
        	Engine.getAgent().removeSubscription(targetSub);
        }
        currentTarget = targetOid;
        
        if (currentTarget != null) {
	        SubjectFilter filter = new SubjectFilter(targetOid);
	        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
	        targetSub = Engine.getAgent().createSubscription(filter, this);

	        Engine.getAgent().sendBroadcast(new BaseBehavior.FollowCommandMessage(obj, new EntityHandle(currentTarget), speed, hitBoxRange));
	        CombatClient.autoAttack(obj.getOid(), currentTarget, true);
	        inCombat = true;
	        Engine.getExecutor().schedule(cdt, 1000, TimeUnit.MILLISECONDS);
	        //TODO: Update attitude to hated
		    
		    // Update facing
		    /*Quaternion newOrientation;
		    AOVector newDir = new AOVector(destLoc);
		    newDir.sub(myLoc);
		    if (!newDir.isZero()){
		       newDir.normalize();
		       newOrientation = Quaternion.fromVectorRotation(AOVector.UnitZ, newDir);
		       newDir.multiply(mobSpeed);
		       InterpolatedWorldNode curWnode = obj.getWorldNode();           
		       BasicWorldNode updateNode = new BasicWorldNode(curWnode);
		       updateNode.setDir(newDir);
		       updateNode.setOrientation(newOrientation);
		       WorldManagerClient.UpdateWorldNodeMessage upMsg = new WorldManagerClient.UpdateWorldNodeMessage(obj.getOid(), updateNode);
		               
		       Engine.getAgent().sendBroadcast(upMsg); 
		    }*/
	    } else {
	        CombatClient.autoAttack(obj.getOid(), null, false);
	        Engine.getAgent().sendBroadcast(new BaseBehavior.ArrivedEventMessage(obj));
	        inCombat = false;
	    }
    }
    
    public void run() {
    	if (activated == false) {
    	    return;
    	}
    }
    
    /**
     * A runnable class to check if the mob has chased a target too far from its original location.
     * @author Andrew
     *
     */
    class CheckDistanceTravelled implements Runnable, Serializable {
    	public void run() {
    		checkDistance();
    	}
    	
    	private void checkDistance() {
        	if (centerLoc == null) {
        		return;
        	}
        	InterpolatedWorldNode wnode = obj.getWorldNode();
        	if (wnode == null) {
        		Log.error("AGGRO: got null wnode during distance check for oid: " + obj.getOid());
        		return;
        	}
        	Point loc = wnode.getCurrentLoc();
        	float distance = Point.distanceTo(loc, centerLoc);
        	//Log.debug("COMBAT: distance from centerLoc: " + distance);
        	if ((distance > chaseDistance) && (evade == false)) {
        		Log.debug("COMBAT: mob has exceeded max distance: " + chaseDistance);
        		// Add in some call to stop attacking
        		CombatClient.autoAttack(obj.getOid(), null, false);
        		Engine.getAgent().sendBroadcast(new BaseBehavior.GotoCommandMessage(obj, centerLoc, speed));
    		    // Set evade to true for 10 seconds
        		Log.debug("Evade set to true 1");
    		    evade = true;
    		    inCombat = false;
    		    Engine.getExecutor().schedule(this, 15000, TimeUnit.MILLISECONDS);
    		    Log.debug("Evade set to true 2");
        	} else if (evade == true) {
        	    evade = false;
        	    Log.debug("Evade set to false");
        	} else {
        		Engine.getExecutor().schedule(this, 1000, TimeUnit.MILLISECONDS);
        	}
        }
    	private static final long serialVersionUID = 1L;
    }
    
    public void setMovementSpeed(int speed) { this.speed = new Integer(speed); }
    public int getMovementSpeed() { return speed.intValue(); }
    protected Integer speed = new Integer(6);

    public void setReactionRadius(int radius) { reactionRadius = radius; }
    public int getReactionRadius() { return reactionRadius.intValue(); }
    protected Integer reactionRadius = new Integer(100);
    
    public void setCenterLoc(Point loc) {centerLoc = loc; }
    public Point getCenterLoc() { return centerLoc; }
    protected Point centerLoc = null;
    
    public void setchaseDistance(int distance) { chaseDistance = distance; }
    public int getchaseDistance() { return chaseDistance; }
    int chaseDistance = 60; // How far the mob will chase a player in millimeters
    
    public void setHitBoxRange(float radius) { hitBoxRange = radius; }
    public float getHitBoxRange() { return hitBoxRange; }
    float hitBoxRange = 5;
    
    public void setOwnerOid(OID ownerOid) { this.ownerOid = ownerOid; }
    public OID getOwnerOid() { return ownerOid; }

    OID ownerOid = null;
    CheckDistanceTravelled cdt = new CheckDistanceTravelled();
    LinkedList<OID> targetsInRange = new LinkedList<OID>();
    int attitude = 2; // 1: Passive, 2: Defensive, 3: Aggressive
    int currentCommand = -2; // -1: Stay, -2: Follow, -3: Attack
    int aggroRange = 15;
    Long eventSub = null;
    Long targetSub = null;
    Long eventSub2 = null;
    boolean evade = false;
    boolean inCombat = false;
    protected OID currentTarget = null;
    protected boolean activated = false;
    private static final long serialVersionUID = 1L;
}
