package atavism.agis.behaviors;

import atavism.agis.plugins.ArenaClient;
import atavism.agis.plugins.CombatClient;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.Behavior;
import atavism.server.engine.Engine;
import atavism.server.math.Point;
import atavism.server.objects.ObjectTracker;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.util.Log;

public class DotBehavior extends Behavior implements MessageCallback {
    public DotBehavior() {
    	super();
    }

    public void initialize() {
        SubjectFilter filter = new SubjectFilter(obj.getOid());
	filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        eventSub = Engine.getAgent().createSubscription(filter, this);
    }

    public void activate() {
	activated = true;
	MobManagerPlugin.getTracker(obj.getInstanceOid()).addReactionRadius(obj.getOid(), radius);
    }

    public void deactivate() {
	lock.lock();
	try {
	    activated = false;
	    if (eventSub != null) {
                Engine.getAgent().removeSubscription(eventSub);
		eventSub = null;
	    }
	}
	finally {
	    lock.unlock();
	}
    }

    public void handleMessage(Message msg, int flags) {
	if (activated == false) {
	    return;
	}
	if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS) {
	    ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage)msg;
	    if (nMsg.getInRadius()) {
	    	reaction(nMsg);
	    }
	}
    }

    public void reaction(ObjectTracker.NotifyReactionRadiusMessage nMsg) {
    	Log.debug("DOT: got reaction hit");
    	ArenaClient.dotScore(nMsg.getSubject());
    	CombatClient.startAbility(-500, nMsg.getTarget(), nMsg.getTarget(), null);
    	Log.debug("DOT: reaction hit finished");
    }

    public void setRadius(int radius) {
    	this.radius = radius;
    }
    public int getRadius() {
    	return radius;
    }

    protected int radius = 0;
    protected Point destination;
    protected boolean activated = false;
    Long eventSub = null;
    private static final long serialVersionUID = 1L;
}
