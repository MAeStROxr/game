package atavism.agis.behaviors;

import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.CombatClient;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.BaseBehavior;
import atavism.server.engine.Behavior;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.EntityHandle;
import atavism.server.objects.SpawnData;
import atavism.server.util.Log;

public class NonCombatPetBehavior extends Behavior implements MessageCallback {
    public NonCombatPetBehavior() {
    	super();
    }

    public NonCombatPetBehavior(SpawnData data) {
    	super(data);
    	String value = (String)data.getProperty("combat.movementSpeed");
    	if (value != null) {
    		setMovementSpeed(Integer.valueOf(value));
    	}
    }

    public void initialize() {
        SubjectFilter filter = new SubjectFilter(obj.getOid());
        filter.addType(CombatClient.MSG_TYPE_DAMAGE);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter.addType(CombatClient.MSG_TYPE_COMBAT_LOGOUT);
        //filter.addType(WorldManagerClient.MSG_TYPE_UPDATE_OBJECT);
        eventSub = Engine.getAgent().createSubscription(filter, this);
        
        // Another filter to pick up when a players faction rep changes
        MessageTypeFilter filter2 = new MessageTypeFilter();
        filter2.addType(CombatClient.MSG_TYPE_FACTION_UPDATE);
        eventSub2 = Engine.getAgent().createSubscription(filter2, this);
    }
    public void activate() {
	    activated = true;
	    Log.debug("CombatBehavior.activate: adding reaction radius");
	    Engine.getAgent().sendBroadcast(new BaseBehavior.FollowCommandMessage(obj, new EntityHandle(ownerOid), speed, hitBoxRange));
    }
    
    public void deactivate() {
	lock.lock();
	try {
	    activated = false;
	    if (eventSub != null) {
	        Engine.getAgent().removeSubscription(eventSub);
		    eventSub = null;
	    }
	    if (eventSub2 != null) {
	        Engine.getAgent().removeSubscription(eventSub2);
		    eventSub2 = null;
	    }
	}
	finally {
	    lock.unlock();
	}
    }

    public void handleMessage(Message msg, int flags) {
	    lock.lock();
	    try {
	    if (activated == false)
		    return;
	    if (msg instanceof PropertyMessage) {
		    PropertyMessage propMsg = (PropertyMessage) msg;
		    Boolean dead = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);
		    if (dead != null && dead) {
                if (Log.loggingDebug)
                    Log.debug("CombatBehavior.onMessage: obj=" + obj + " got death=" + propMsg.getSubject());
		        if (propMsg.getSubject() == obj.getOid()) {
			        Log.debug("CombatBehavior.onMessage: mob died, deactivating all behaviors");
			        for(Behavior behav : obj.getBehaviors()) {
			            behav.deactivate();
			            obj.removeBehavior(behav);
			        }
		        }
		    }
	    }
	    //return true;
	    } finally {
	        lock.unlock();
	    }
    }
    
    public void run() {
    	if (activated == false) {
    	    return;
    	}
    }
    
    public void setMovementSpeed(int speed) { this.speed = new Integer(speed); }
    public int getMovementSpeed() { return speed.intValue(); }
    protected Integer speed = new Integer(6);
    
    public void setHitBoxRange(float radius) { hitBoxRange = radius; }
    public float getHitBoxRange() { return hitBoxRange; }
    float hitBoxRange = 5;
    
    public void setOwnerOid(OID ownerOid) { this.ownerOid = ownerOid; }
    public OID getOwnerOid() { return ownerOid; }

    OID ownerOid = null;
    Long eventSub = null;
    Long targetSub = null;
    Long eventSub2 = null;
    protected boolean activated = false;
    private static final long serialVersionUID = 1L;
}
