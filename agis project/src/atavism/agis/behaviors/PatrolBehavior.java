package atavism.agis.behaviors;

import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.BaseBehavior;
import atavism.server.engine.Behavior;
import atavism.server.engine.Engine;
import atavism.server.math.Point;
import atavism.server.objects.SpawnData;
import atavism.server.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PatrolBehavior extends Behavior implements MessageCallback, Runnable {
    public PatrolBehavior() {
	super();
    }

    public PatrolBehavior(SpawnData data) {
    	super();
    }

    public void initialize() {
        SubjectFilter filter = new SubjectFilter(obj.getOid());
        filter.addType(Behavior.MSG_TYPE_EVENT);
        eventSub = Engine.getAgent().createSubscription(filter, this);
    }
    public void activate() {
        startPatrol();
    }
    public void deactivate() {
        if (eventSub != null) {
	    Engine.getAgent().removeSubscription(eventSub);
            eventSub = null;
        }
    }

    public void handleMessage(Message msg, int flags) {
        if (msg.getMsgType() == Behavior.MSG_TYPE_EVENT) {
	        String event = ((Behavior.EventMessage)msg).getEvent();
	        if (event.equals(BaseBehavior.MSG_EVENT_TYPE_ARRIVED)) {
		        Engine.getExecutor().schedule(this, (long)(getLingerTimes().get(nextWaypoint) * 1000), TimeUnit.MILLISECONDS);
            }
        }
        //return true;
    }

    public void addWaypoint(Point wp) {
        waypoints.add(wp);
    }
    protected List<Point> waypoints = new ArrayList<Point>();

    public void addLingerTime(float time) {
    	lingerTimes.add(time);
    }
    public List<Float> getLingerTimes() {
        return lingerTimes;
    }
    protected List<Float> lingerTimes = new ArrayList<Float>();

    public void setMovementSpeed(float speed) {
        this.speed = speed;
    }
    public float getMovementSpeed() {
        return speed;
    }
    protected float speed = 3;

    /**
     * Calculate the current position of the mob and work out where along the chain they are
     */
    protected void startPatrol() {
    	Point currentLoc = obj.getWorldNode().getLoc();
    	float minDistance = Point.distanceTo(currentLoc, waypoints.get(0));
    	nextWaypoint = 0;
    	for (int i = 1; i < waypoints.size(); i++) {
    		float distanceFromMarker = Point.distanceTo(currentLoc, waypoints.get(i));
    		if (distanceFromMarker < minDistance) {
    			minDistance = distanceFromMarker;
    			nextWaypoint = i;
    			Log.debug("PATROL: first waypoint is now: " + nextWaypoint);
    		}
    	}
        nextPatrol();
    }

    protected void sendMessage(Point waypoint, float speed) {
    	Log.debug("PATH: sending patrol point: " + waypoint + " with next: " + nextWaypoint);
        Engine.getAgent().sendBroadcast(new BaseBehavior.GotoCommandMessage(obj, waypoint, speed));
    }

    protected void nextPatrol() {
    	sendMessage(waypoints.get(nextWaypoint), getMovementSpeed());
    }

    public void run() {
    	nextWaypoint++;
        if (nextWaypoint == waypoints.size()) {
            nextWaypoint = 0;
        }
        nextPatrol();
    }

    int nextWaypoint = 0;
    Long eventSub = null;
    private static final long serialVersionUID = 1L;

}
