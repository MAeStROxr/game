package atavism.agis.behaviors;

import atavism.agis.objects.CombatInfo;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.BaseBehavior;
import atavism.server.engine.Behavior;
import atavism.server.engine.Engine;
import atavism.server.math.Point;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.SpawnData;
import atavism.server.util.Log;
import atavism.server.util.Points;

import java.util.concurrent.TimeUnit;

public class RadiusRoamBehavior extends Behavior implements MessageCallback, Runnable {
    public RadiusRoamBehavior() {
    	super();
    }

    public RadiusRoamBehavior(SpawnData data) {
    	super(data);
    	setCenterLoc(data.getLoc());
    	setRadius(data.getSpawnRadius());
    }

    public void initialize() {
        SubjectFilter filter = new SubjectFilter(obj.getOid());
        filter.addType(Behavior.MSG_TYPE_EVENT);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        eventSub = Engine.getAgent().createSubscription(filter, this);
    }

    public void activate() {
    	activated = true;
        startRoam();
    }

    public void deactivate() {
    	lock.lock();
		try {
			activated = false;
	    	if (eventSub != null) {
                Engine.getAgent().removeSubscription(eventSub);
                eventSub = null;
	    	}
	    	inCombat = false;
		}
		finally {
			lock.unlock();
		}
    }

    public void handleMessage(Message msg, int flags) {
	    if (activated == false) {
	        return; //return true;
	    }
	    
        if (msg.getMsgType() == Behavior.MSG_TYPE_EVENT) {
	        String event = ((Behavior.EventMessage)msg).getEvent();
	        if (event.equals(BaseBehavior.MSG_EVENT_TYPE_ARRIVED)) {
	        	if (!inCombat)
		            Engine.getExecutor().schedule(this, lingerTime, TimeUnit.MILLISECONDS);
	        }
        } else if (msg instanceof PropertyMessage) {
        	PropertyMessage propMsg = (PropertyMessage) msg;
		    Boolean combat = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_COMBATSTATE);
		    if (combat != null) {
                Log.debug("RadiusRoamBehavior.onMessage: obj=" + obj + " got combat=" + propMsg.getSubject());
		        if (propMsg.getSubject().equals(obj.getOid())) {
		        	if (combat) {
			            Log.debug("RadiusRoamBehavior.onMessage: mob is in combat");
			            inCombat = true;
		        	} else {
		        		Log.debug("RadiusRoamBehavior.onMessage: mob is not in combat");
		        		inCombat = false;
		        		Engine.getExecutor().schedule(this, lingerTime, TimeUnit.MILLISECONDS);
		        	}
		        }
		    }
        }
        
        //return true;
    }

    public void setCenterLoc(Point loc) {
	centerLoc = loc;
    }
    public Point getCenterLoc() {
	return centerLoc;
    }
    protected Point centerLoc = null;

    public void setRadius(int radius) {
	this.radius = radius;
    }
    public int getRadius() {
	return radius;
    }
    protected int radius = 0;

    public void setLingerTime(long time) {
        lingerTime = time;
    }
    public long getLingerTime() {
        return lingerTime;
    }
    protected long lingerTime = 5000;

    public void setMovementSpeed(float speed) {
        this.speed = speed;
    }
    public float getMovementSpeed() {
        return speed;
    }
    protected float speed = 2.2f;

    protected void startRoam() {
        nextRoam();
    }

    protected void nextRoam() {
        Point roamPoint = Points.findNearby(centerLoc, radius);
        Log.warn("Got next roam Point: " + roamPoint);
        Engine.getAgent().sendBroadcast(new BaseBehavior.GotoCommandMessage(obj, roamPoint, speed));
    }

    public void run() {
	    if (activated == false) {
	        return;
	    }
	    if (!inCombat) {
            nextRoam();
	    }
    }

    boolean inCombat = false;
    Long eventSub = null;

    protected boolean activated = false;

    private static final long serialVersionUID = 1L;
}
