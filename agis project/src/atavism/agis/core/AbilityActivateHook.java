package atavism.agis.core;

import atavism.agis.objects.AgisItem;
import atavism.agis.plugins.CombatClient;
import atavism.server.engine.OID;
import atavism.server.util.Log;

/**
 * an activate hook for items that trigger abilities
 * when the item is activated, the mob uses the ability
 */
public class AbilityActivateHook implements ActivateHook {
    public AbilityActivateHook() {
    	super();
    }

    public AbilityActivateHook(AgisAbility ability) {
    	super();
    	Log.debug("AJ: creating abilityactivatehook with ability: " + ability.getID());
        setAbilityID(ability.getID());
    }

    public AbilityActivateHook(int abilityID) {
    	super();
        setAbilityID(abilityID);
    }

    public void setAbilityID(int abilityID) {
        if (abilityID == -1) {
            throw new RuntimeException("AbilityActivateHook.setAbility: bad ability");
        }
        Log.debug("AJ: setting abilityID to: " + abilityID);
        this.abilityID = abilityID;
    }
    public int getAbilityID() {
    	return abilityID;
    }
    protected int abilityID;

    public AgisAbility getAbility() {
    	if (abilityID == -1)
    		return null;
    	return Agis.AbilityManager.get(abilityID);
    }

    public boolean activate(OID activatorOid, AgisItem item, OID targetOid) {
        if (Log.loggingDebug)
            Log.debug("AbilityActivateHook.activate: activator=" + activatorOid + " item=" + item + " ability=" + abilityID + " target=" + targetOid);
        //CombatClient.startAbility(abilityName, activatorOid, targetOid, item.getOid());
        CombatClient.startAbility(abilityID, activatorOid, targetOid, item, null);
        return true;
    }
    
    public String toString() {
    	return "AbilityActivateHook:ability=" + abilityID;
    }

    private static final long serialVersionUID = 1L;
}
