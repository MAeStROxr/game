package atavism.agis.core;

import atavism.agis.objects.*;
import atavism.server.engine.Manager;

public class Agis {
    public static Manager<AgisAbility> AbilityManager =
	new Manager<AgisAbility>("AbilityManager");

    public static Manager<AgisEffect> EffectManager =
	new Manager<AgisEffect>("EffectManager");

    public static Manager<SkillTemplate> SkillManager = new Manager<SkillTemplate>("SkillManager");
    
    public static Manager<Faction> FactionManager = new Manager<Faction>("FactionManager");
    
    public static Manager<Currency> CurrencyManager = new Manager<Currency>("CurrencyManager");
    
    public static Manager<LootTable> LootTableManager = new Manager<LootTable>("LootTableManager");
    
    public static Manager<AgisQuest> QuestManager = new Manager<AgisQuest>("QuestManager");
    
    public static int getDefaultCorpseTimeout() {
	return defaultCorpseTimeout;
    }
    public static void setDefaultCorpseTimeout(int timeout) {
	defaultCorpseTimeout = timeout;
    }
    private static int defaultCorpseTimeout = 60000;
}