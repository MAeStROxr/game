package atavism.agis.core;

import atavism.agis.core.AgisEffect.EffectState;
import atavism.agis.objects.AgisItem;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.objects.LevelingMap;
import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.AgisWorldManagerPlugin;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CombatPlugin;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.BasicWorldNode;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.LockFactory;
import atavism.server.util.Log;

import java.util.*;
import java.util.concurrent.locks.Lock;

/**
 * The AgisAbility object describes an action that a mob can perform.
 * <p>
 * When an ability is triggered, a AgisAbility.State object is
 * generated that represents the current state of that instance of the
 * ability. It progresses through a sequence of states, dependent on
 * the configuration of the AgisAbility object.
 * <p>
 * As each state is entered or exited, a method is called that can be
 * overridden to create different types of abilities.
 */
public class AgisAbility {
    public AgisAbility(String name) {
        setName(name);
    }

    /**
     * Returns the string describing this ability, useful for logging.
     * 
     * @return string describing ability
     */
    public String toString() {
        return "[AgisAbility: " + getName() + "]";
    }

    /**
     * Returns if two objects are the same - tested by comparing the ability name.
     * null objects are never equal to any other object including other null objects.
     *
     * @return true if abilities match
     */
    public boolean equals(Object other) {
        AgisAbility otherAbility = (AgisAbility) other;
        boolean val = getName().equals(otherAbility.getName());
        return val;
    }

    /**
     * Returns a hash of the ability name
     *
     * @return hash value of the object's ability name
     */
    public int hashCode() {
        int hash = getName().hashCode();
        return hash;
    }

    /**
     * AgisAbility lock
     */
    transient protected Lock lock = LockFactory.makeLock("AgisAbilityLock");

    /**
     * Sets the name of the ability. This is used to identify the
     * ability, so it should be unique.
     *
     * @param name name for this ability.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of the ability.
     *
     * @return name for this ability.
     */
    public String getName() {
        return name;
    }
    String name = null;
    
    public void setID(int id) {
    	this.id = id;
    }
    public int getID() {
    	return id;
    }
    int id = -1;
    
    
    public void setSkillType(int skillType) {
    	this.skillType = skillType;
    }
    public int getSkillType() {
    	return skillType;
    }
    protected int skillType = -1;
    
    /*
     * 1: Active
     * 2: Passive
     * 3: Profession
     */
    public void setAbilityType(int abilityType) {
    	this.abilityType = abilityType;
    }
    public int getAbilityType() {
    	return abilityType;
    }
    int abilityType = -1;
    
    public void setDuelID(int duelID) {
    	this.duelID = duelID;
    }
    public int getDuelID() {
    	return duelID;
    }
    int duelID = -1;

    public enum TargetType {
	UNINIT,
	NONE,
	ENEMY,
	FRIENDNOTSELF,
	FRIEND,
	GROUP,
	SELF,
	ANY,
	AREA_ENEMY,
	AREA_FRIENDLY,
	OTHER
    }
    
    public enum TargetSpecies {
    	UNINIT,
    	ANY,
    	BEAST,
    	HUMANOID,
    	ELEMENTAL,
    	UNDEAD,
    	PLAYER,
    	NONPLAYER
    }

    /**
     * Returns the time the ability takes to activate.
     *
     * @return time in ms to activate the ability.
     */
    public long getActivationTime() { return activationTime; }

    /**
     * Sets the time the ability takes to activate.
     *
     * @param time time in ms that the ability takes to activate.
     */
    public void setActivationTime(long time) { activationTime = time; }

    /**
     * Returns if the ability has 0 activation time.
     *
     * @return true if activate time is 0.
     */
    public boolean isInstant() { return activationTime == 0; }
    protected long activationTime = 0;

    /**
     * Returns the stat cost for successfully activating the ability.
     *
     * @return stat cost for activating the ability.
     */
    public int getActivationCost() { return activationCost; }

    /**
     * Sets the stat cost for successfully activating the ability.
     *
     * @param cost stat cost for activating the ability.
     */
    public void setActivationCost(int cost) { activationCost = cost; }
    protected int activationCost = 0;

    /**
     * Returns the name of the property that stat costs are deducted from.
     *
     * @return name of the property that stat costs are deducted from.
     */
    public String getCostProperty() { return costProp; }

    /**
     * Sets the name of the property that stat costs are deducted from.
     *
     * @param name name of the property that stat costs are deducted from.
     */
    public void setCostProperty(String name) { costProp = name; }
    protected String costProp = null;

    /**
     * Returns the time in ms for each pulse of a channelled ability.
     *
     * @return time in ms for each pulse of a channelled ability.
     */
    public long getChannelPulseTime() { return channelPulseTime; }

    /**
     * Sets the time in ms for each pulse of a channelled ability.
     *
     * @param time time in ms for each pulse of a channelled ability.
     */
    public void setChannelPulseTime(long time) { channelPulseTime = time; }
    protected long channelPulseTime = 0;

    /**
     * Returns the number of pulses during the channelled phase.
     *
     * @return number of pulses during the channelled phase for the ability.
     */
    public int getChannelPulses() { return channelPulses; }

    /**
     * Sets the number of pulses during the channelled phase.
     *
     * @param pulses number of pulses during the channelled phase for the ability.
     */
    public void setChannelPulses(int pulses) { channelPulses = pulses; }
    protected int channelPulses = 0;

    /**
     * Returns the stat cost charged for each channelling pulse.
     *
     * @return stat cost charged for each channelling pulse.
     */
    public int getChannelCost() { return channelCost; }

    /**
     * Sets the stat cost charged for each channelling pulse.
     *
     * @param cost stat cost charged for each channelling pulse.
     */
    public void setChannelCost(int cost) { channelCost = cost; }
    protected int channelCost = 0;

    /**
     * Returns the time in ms for each pulse of the active phase.
     *
     * @return time in ms for each pulse of the active phase.
     */
    public long getActivePulseTime() { return activePulseTime; }

    /**
     * Set the time in ms for each pulse of the active phase.
     *
     * @param time time in ms for each pulse of the active phase.
     */
    public void setActivePulseTime(long time) { activePulseTime = time; }
    protected long activePulseTime = 0;

    /**
     * Returns the stat cost charged for each pulse of the active phase.
     *
     * @return stat cost charged for each pulse of the active phase.
     */
    public int getActiveCost() { return activePulseCost; }
    public int getActivePulseCost() { return activePulseCost; }

    /**
     * Sets the stat cost charged for each pulse of the active phase.
     *
     * @param cost stat cost charged for each pulse of hte active phase.
     */
    public void setActiveCost(int cost) { activePulseCost = cost; }
    public void setActivePulseCost(int cost) { activePulseCost = cost; }
    protected int activePulseCost = 0;

    /**
     * Returns the icon name for this ability.
     *
     * @return icon name for this ability.
     */
    public String getIcon() { return icon; }

    /**
     * Sets the icon name for this ability.
     *
     * @param icon icon name for this ability.
     */
    public void setIcon(String icon) { this.icon = icon; }
    protected String icon = null;
    
    public String getDisplayName() { return displayName; }
    public void setDisplayName(String displayName) { this.displayName = displayName; }
    protected String displayName = null;
    
    public String getTooltip() { return tooltip; }
    public void setTooltip(String tooltip) { this.tooltip = tooltip; }
    protected String tooltip = null;

    /**
     * Returns the minimum range in mm for this ability.
     *
     * @return minimum range in mm for this ability.
     */
    public int getMinRange() { return minRange; }

    /**
     * Sets the minimum range in mm for this ability.
     *
     * @param range minimum range in mm for this ability.
     */
    public void setMinRange(int range) { minRange = range; }
    protected int minRange = 0;

    /**
     * Returns the maximum range in metres for this ability.
     *
     * @return maximum range in metres for this ability.
     */
    public int getMaxRange() { return maxRange; }

    /**
     * Sets the maximum range in metres for this ability.
     *
     * @param range Maximum range in metres for this ability.
     */
    public void setMaxRange(int range) { maxRange = range; }
    protected int maxRange = 0;
    
    /**
     * If the areaOfEffectRadius is greater than zero the system will check for all possible targets
     * in the region and try apply the effects to them.
     * @return
     */
    public int getAreaOfEffectRadius() { return areaOfEffectRadius; }
    public void setAreaOfEffectRadius(int radius) { areaOfEffectRadius = radius; }
    protected int areaOfEffectRadius = 0;

    /**
     * Adds a cooldown to this ability. If any of the ability's cooldowns are activate on
     * the mob attempting to activate the ability, it will not be able to activate.
     *
     * @param cd Cooldown to add to this ability.
     */
    public void addCooldown(Cooldown cd) {
	try {
	    lock.lock();
	    cooldownMap.put(cd.getID(), cd);
	}
	finally {
	    lock.unlock();
	}
    }

    /**
     * Removes a cooldown from this ability.
     *
     * @param id id of the cooldown to remove.
     */
    public void removeCooldown(String id) {
	try {
	    lock.lock();
	    cooldownMap.remove(id);
	}
	finally {
	    lock.unlock();
	}
    }

    public Map<String, Cooldown> getCooldownMap() {
	try {
	    lock.lock();
	    return new HashMap<String, Cooldown>(cooldownMap);
	}
	finally {
	    lock.unlock();
	}
    }
    public void setCooldownMap(Map<String, Cooldown> cooldownMap) {
	try {
	    lock.lock();
	    this.cooldownMap = new HashMap<String, Cooldown>(cooldownMap);
	}
	finally {
	    lock.unlock();
	}
    }
    protected Map<String, Cooldown> cooldownMap = new HashMap<String, Cooldown>();
    
    // Does this ability require a certain weapon?
    public void setWeaponReq(String weaponType) {
    	weaponReq = weaponType;
    }
    public String getWeaponReq() {
        return weaponReq;
    }
    public String weaponReq = "";
    
    public boolean getDecrementWeaponUses() {
    	return decrementWeaponUses;
    }
    public void setDecrementWeaponUses(boolean decrement) {
    	decrementWeaponUses = decrement;
    }
    protected boolean decrementWeaponUses = false;

    /**
     * Adds a reagent requirement to this ability. Reagents are items that are required
     * to be present in inventory, and are consumed when the ability completes the
     * ACTIVATING phase.
     *
     * @param reagent name of the template the reagent was created from.
     */
    public void addReagent(Integer reagent) {
	try {
	    lock.lock();
	    reagentList.add(reagent);
	}
	finally {
	    lock.unlock();
	}
    }
    public ArrayList<Integer> getReagentList() {
	try {
	    lock.lock();
	    return new ArrayList<Integer>(reagentList);
	}
	finally {
	    lock.unlock();
	}
    }
    public void setReagentList(ArrayList<Integer> reagentList) {
	try {
	    lock.lock();
	    this.reagentList = new ArrayList<Integer>(reagentList);
	}
	finally {
	    lock.unlock();
	}
    }
    protected ArrayList<Integer> reagentList = new ArrayList<Integer>();
    
    public boolean getConsumeReagents() {
    	return consumeReagents;
    }
    public void setConsumeReagents(boolean consume) {
    	consumeReagents = consume;
    }
    protected boolean consumeReagents = true;

    /**
     * Adds a tool requirement to this ability. Tools are items that are required
     * to be present in inventory. They are not consumed.
     *
     * @param tool name of the template the tool was created from.
     */
    public void addTool(int tool) {
	try {
	    lock.lock();
	    toolList.add(tool);
	}
	finally {
	    lock.unlock();
	}
    }
    public ArrayList<Integer> getToolList() {
	try {
	    lock.lock();
	    return new ArrayList<Integer>(toolList);
	}
	finally {
	    lock.unlock();
	}
    }
    public void setToolList(ArrayList<Integer> toolList) {
	try {
	    lock.lock();
	    this.toolList = new ArrayList<Integer>(toolList);
	}
	finally {
	    lock.unlock();
	}
    }
    protected ArrayList<Integer> toolList = new ArrayList<Integer>();
    
    public int getAmmoReq() { return ammoReq; }
    public void setAmmoReq(int ammoReq) { this.ammoReq = ammoReq; }
    protected int ammoReq = 0;
    
    /**
     * Returns the vigor amount added or required for the ability.
     *
     * @return vigor amount.
     */
    public int getVigor() { return vigor; }
    
    /**
     * Sets the amount of vigor to be added or required to activate the ability
     * 
     * @param value the amount of vigor to be added or subtracted
     */
    public void setVigor(int value) { vigor = value; }
    protected int vigor = 0;
    
    /**
     * Returns the vigor amount added or required for the ability.
     *
     * @return vigor amount.
     */
    public String getStance() { return stanceReq; }
    
    /**
     * Sets the amount of vigor to be added or required to activate the ability
     * 
     * @param value the amount of vigor to be added or subtracted
     */
    public void setStance(String stance) { stanceReq = stance; }
    protected String stanceReq = "";
    

    /**
     * The casting Anim is for abilities that have a cast, it determines which animation
     * should be used.
     */
    public String getCastingAnim() { return castingAnim; }
    public void setCastingAnim(String anim) { castingAnim = anim; }
    protected String castingAnim = "";
    
    /**
     * The casting Anim is for abilities that have a cast, it determines which animation
     * should be used.
     */
    public String getCastingAffinity() { return castingAffinity; }
    public void setCastingAffinity(String affinity) { castingAffinity = affinity; }
    protected String castingAffinity = "";
    
    /**
     * Returns whether a target is required for this ability to activate
     *
     * @return target type for this ability.
     */
    public boolean getReqTarget() { return reqTarget; }

    /**
     * Sets whether a target is required for this ability to activate
     *
     * @param type target type for this ability.
     */
    public void setReqTarget(boolean req) { reqTarget = req; }
    protected boolean reqTarget = true;

    /**
     * Returns the target type for this ability.
     *
     * @return target type for this ability.
     */
    public TargetType getTargetType() { return targetType; }

    /**
     * Sets the target type for this ability.
     *
     * @param type target type for this ability.
     */
    public void setTargetType(TargetType type) { targetType = type; }
    protected TargetType targetType = TargetType.UNINIT;
    
    /**
     * Returns the list of acceptable specific targets for this ability.
     *
     * @return a list of specific targets for this ability.
     */
    public LinkedList<String> getSpecificTargets() { return specificTargets; }

    /**
     * Sets the list of acceptable specific targets for this ability.
     *
     * @param targets: a list of acceptable specific targets.
     */
    public void setSpecificTargets(LinkedList<String> targets) { specificTargets = targets; }
    public void addSpecificTarget(String name) {
    	if (specificTargets == null) {
    		specificTargets = new LinkedList<String>();
    	}
    	specificTargets.add(name);
    }
    protected LinkedList<String> specificTargets = null;
    
    /**
     * Returns the list of acceptable specific targets for this ability.
     *
     * @return a list of specific targets for this ability.
     */
    public LinkedList<TargetSpecies> getTargetableSpecies() { return targetableSpecies; }

    /**
     * Sets the list of acceptable specific targets for this ability.
     *
     * @param targets: a list of acceptable specific targets.
     */
    public void setTargetableSpecies(LinkedList<TargetSpecies> targets) { targetableSpecies = targets; }
    public void addTargetableSpecies(TargetSpecies species) {
    	targetableSpecies.add(species);
    }
    protected LinkedList<TargetSpecies> targetableSpecies = new LinkedList<TargetSpecies>();
    
    /**
     * Returns the target type for this ability.
     *
     * @return target type for this ability.
     */
    public int getTargetDeath() { return targetDeath; }

    /**
     * Sets the target type for this ability.
     *
     * @param type target type for this ability.
     */
    public void setTargetDeath(int type) { targetDeath = type; }
    protected int targetDeath = 1; // 0 for dead, 1 for alive, 2 for doesn't matter
    
    // Does this ability require any effects on the attacker?
    public void addAttackerEffectReq(int effectReq) {
    	attackerEffectReqs.add(effectReq);
    }
    public LinkedList<Integer> GetAttackerEffectReqs() {
        return attackerEffectReqs;
    }
    public LinkedList<Integer> attackerEffectReqs = new LinkedList<Integer>();
    
    // Does this ability require any effects on the target?
    public void addTargetEffectReq(int effectReq) {
    	targetEffectReqs.add(effectReq);
    }
    public LinkedList<Integer> GetTargetEffectReqs() {
        return targetEffectReqs;
    }
    public LinkedList<Integer> targetEffectReqs = new LinkedList<Integer>();
    
    // Does this ability eat up any effects on the player?
    public void addAttackerEffectConsumption(int effectReq) {
    	attackerEffectConsumption.add(effectReq);
    }
    public LinkedList<Integer> GetAttackerEffectConsumption() {
        return attackerEffectConsumption;
    }
    public LinkedList<Integer> attackerEffectConsumption = new LinkedList<Integer>();
    
    // Does this ability eat up any effects on the target?
    public void addTargetEffectConsumption(int effectReq) {
    	targetEffectConsumption.add(effectReq);
    }
    public LinkedList<Integer> GetTargetEffectConsumption() {
        return targetEffectConsumption;
    }
    public LinkedList<Integer> targetEffectConsumption = new LinkedList<Integer>();

    public boolean getUseGlobalCooldown() { return useGlobalCooldown; }
    public void setUseGlobalCooldown(boolean val) { useGlobalCooldown = val; }
    protected boolean useGlobalCooldown = true;

    public boolean getStationary() { return stationary; }
    public void setStationary(boolean val) { stationary = val; }
    protected boolean stationary = false;

    public boolean getChannelled() { return channelled; }
    public void setChannelled(boolean val) { channelled = val; }
    protected boolean channelled = false;

    public boolean getPersistent() { return persistent; }
    public void setPersistent(boolean val) { persistent = val; }
    protected boolean persistent = false;

    public boolean addCoordEffect(ActivationState state, CoordinatedEffect effect) {
	Set<CoordinatedEffect> effectSet = coordEffectMap.get(state);
	if (effectSet == null) {
	    effectSet = new HashSet<CoordinatedEffect>();
	    coordEffectMap.put(state, effectSet);
	}
	return effectSet.add(effect);
    }
    public boolean removeCoordEffect(ActivationState state, CoordinatedEffect effect) {
	Set<CoordinatedEffect> effectSet = coordEffectMap.get(state);
	if (effectSet == null) {
	    return false;
	}
	return effectSet.remove(effect);
    }
    public Collection<CoordinatedEffect> getCoordEffects(ActivationState state) {
	Set<CoordinatedEffect> effectSet = coordEffectMap.get(state);
	if (effectSet == null) {
	    effectSet = new HashSet<CoordinatedEffect>();
	    coordEffectMap.put(state, effectSet);
	}
	return effectSet;
    }
    protected Map<ActivationState, Set<CoordinatedEffect>> coordEffectMap =
	new HashMap<ActivationState, Set<CoordinatedEffect>>();
    
    /*
     * This method changes the Coordinated effect result parameter. This 
     * should be called when abilities still hit but the result is a bit
     * different (i.e. block, partial resist)
     */
    protected void changeCoordinatedEffect(String result) {
    	Set<CoordinatedEffect> effectSet = coordEffectMap.get(ActivationState.COMPLETED);
    	if (effectSet == null)
    		return;
	    Iterator<CoordinatedEffect> iter = effectSet.iterator();
	    while (iter.hasNext()) {
	    	CoordinatedEffect effect = iter.next();
	    	String argument = (String) effect.getArgument("result");
	    	if (argument != null)
	    		effect.putArgument("result", result);
	    }
    }

    public String getCompleteAnimation() { return completeAnimation; }
    public void setCompleteAnimation(String anim) { completeAnimation = anim; }
    protected String completeAnimation;

    public String getCompleteSound() { return completeSound; }
    public void setCompleteSound(String sound) { completeSound = sound; }
    protected String completeSound;
    
    protected boolean sendSkillUpChance = true;

    /**
     * This method gets the list of combat info objects we should lock.
     * The determination of this list must not lock any of the objects.
     * @param state
     * @return
     */
    public List<CombatInfo> getPotentialTargets(AgisAbilityState state) {
        List<CombatInfo> targets = new LinkedList<CombatInfo>();
        targets.add(state.getTarget());
        return targets;
    }
    
    public ArrayList<CombatInfo> getAoETargets(CombatInfo caster) {
    	ArrayList<CombatInfo> targetsInArea = new ArrayList<CombatInfo>();
    	if (targetType == TargetType.AREA_ENEMY) {
    		for (OID target : caster.getAttackableTargets().keySet()) {
    			BasicWorldNode node = WorldManagerClient.getWorldNode(caster.getOwnerOid());
    			Point loc = node.getLoc();
    			BasicWorldNode targetNode = WorldManagerClient.getWorldNode(target);
    			if (targetNode == null) {
    				Log.error("ABILITY: target node was null");
    			}
    			Point targetLoc = targetNode.getLoc();
    			if (Point.distanceToXZ(loc, targetLoc) <= areaOfEffectRadius) {
    				targetsInArea.add(CombatPlugin.getCombatInfo(target));
    				Log.error("Added aoe target: " + target);
    			} else {
    				Log.error("Target: " + target + " was too far away");
    			}
    		}
    	} else if (targetType == TargetType.AREA_FRIENDLY) {
    		for (OID target : caster.getFriendlyTargets().keySet()) {
    			BasicWorldNode node = WorldManagerClient.getWorldNode(caster.getOwnerOid());
    			Point loc = node.getLoc();
    			BasicWorldNode targetNode = WorldManagerClient.getWorldNode(target);
    			if (targetNode == null) {
    				Log.error("ABILITY: target node was null");
    			}
    			Point targetLoc = targetNode.getLoc();
    			if (Point.distanceToXZ(loc, targetLoc) <= areaOfEffectRadius) {
    				targetsInArea.add(CombatPlugin.getCombatInfo(target));
    			}
    		}
    	}
    	return targetsInArea;
    }
    
    // begin activating the ability
    public void beginActivation(AgisAbilityState state) {
        Log.debug("AgisAbility.beginActivation:");
    }

    public void completeActivation(AgisAbilityState state) {
        Log.debug("AgisAbility.completeActivation:");
        CombatInfo combatInfo = state.getSource();
        CombatInfo target = state.getTarget();
        
        if (!reagentList.isEmpty() && consumeReagents) {
        	for (int reagent: reagentList) 
                AgisInventoryClient.removeGenericItem(combatInfo.getOwnerOid(), reagent, false, 1);
        }
        
        if (costProp != null) {
            combatInfo.statModifyBaseValue(costProp, -activationCost);
            combatInfo.sendStatusUpdate();
        }
        
        /*Log.debug("ANDREW - vigor value: " + vigor);
        if (vigor != 0) {
        	combatInfo.statModifyBaseValue("vigor", vigor);
            combatInfo.sendStatusUpdate();
        }*/
        
        // Here we remove any effects that are eaten up by the ability
        if (attackerEffectConsumption.size() > 0) {
        	OID oid = combatInfo.getOwnerOid();
    	    for (int i = 0; i < attackerEffectConsumption.size(); i++) {
    	    	int effectConsumed = attackerEffectConsumption.get(i);
    		    for (EffectState existingState : state.getSource().getCurrentEffects()) {
    			    if (effectConsumed == existingState.getEffect().getID()) {
    			    	Log.debug("ANDREW - attacker effect being consumed - effect num/pos: " 
    			    			+ existingState.getEffect().getIcon());
    			    	AgisEffect.removeEffect(existingState);
        			    break;
    			    } 
    		    }
    	    }
    	    //attackerEffectConsumption.clear();
        }
        
        if (targetEffectConsumption.size() > 0) {
        	OID oid = target.getOwnerOid();
    	    for (int i = 0; i < targetEffectConsumption.size(); i++) {
    	    	int effectConsumed = targetEffectConsumption.get(i);
    	    	for (EffectState existingState : state.getTarget().getCurrentEffects()) {
    			    if (effectConsumed == existingState.getEffect().getID()) {
    			    	Log.debug("ANDREW - target effect being consumed - effect num/pos: " 
    			    			+ existingState.getEffect().getIcon());
    			    	//WorldManagerClient.sendObjChatMsg(oid, 1,
    			    	//	    "Removing effect: " + effects.get(j) + "; objOid: " + oid);
    			    	AgisEffect.removeEffect(existingState);
        			    break;
    			    } 
    		    }
    	    }
    	    //targetEffectConsumption.clear();
        }
        
        if (ammoReq > 0) {
        	// Remove an ammo item from the caster
        	Integer ammoLoaded = (Integer) combatInfo.getProperty(CombatInfo.COMBAT_AMMO_LOADED);
        	if (ammoLoaded != null && ammoLoaded > 0) {
        		AgisInventoryClient.removeGenericItem(combatInfo.getOwnerOid(), ammoLoaded, false, ammoReq);
        	}
        }
        
        /* AgisItem item = */ state.getItem();
        
        // Send a skill used message
        Log.debug("SKILL: checking send ability skill up for skill: " + skillType + " with sendSkill: " + sendSkillUpChance);
        if (combatInfo.isUser() && skillType != -1 && sendSkillUpChance) {
        	Log.debug("SKILL: sending ability Used");
            CombatClient.abilityUsed(combatInfo.getOwnerOid(), skillType);
        }
        sendSkillUpChance = true;
        
        // Activate cooldowns. 
        Collection<Cooldown>cooldowns = state.getAbility().getCooldownMap().values();
        Cooldown.activateCooldowns(cooldowns, combatInfo, 100);
        Log.debug("AgisAbility.completeActivation: finished");
        
        // Send weapon used?
        if (state.item == null) {
        	// Item Used message is not sent if the source is hitting their dueling partner
        	Integer duelID = (Integer)state.getSource().getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
        	if (duelID == null || duelID < 0 || !state.getTarget().isUser())
        		AgisInventoryClient.equippedItemUsed(state.getSourceOid(), "Attack");
        }
    }

    public void beginChannelling(AgisAbilityState state) {
        Log.debug("AgisAbility.beginChannelling:");
    }

    public void pulseChannelling(AgisAbilityState state) {
		CombatInfo source = state.getSource();
        if (Log.loggingDebug)
            Log.debug("AgisAbility.pulseChannelling: cost=" + channelCost);
		if (costProp != null) {
			source.statModifyBaseValue(costProp, -channelCost);
			source.sendStatusUpdate();
		}
    }

    public void completeChannelling(AgisAbilityState state) {
		Log.debug("AgisAbility.completeChannelling:");
    }

    public void beginActivated(AgisAbilityState state) {
		Log.debug("AgisAbility.beginActivated:");
    }

    public void pulseActivated(AgisAbilityState state) {
		Log.debug("AgisAbility.pulseActivated:");
		CombatInfo source = state.getSource();
		if (costProp != null) {
			source.statModifyBaseValue(costProp, -activePulseCost);
			source.sendStatusUpdate();
		}
    }

    public void endActivated(AgisAbilityState state) {
		Log.debug("AgisAbility.endActivated:");
    }

    public void interrupt(AgisAbilityState state) {
		Log.debug("AgisAbility.interrupt:");
    }

    /**
     * exposes a way for the client to execute ability with a slash command
     */
    public void setSlashCommand(String slashCommand) {
        this.slashCommand = slashCommand;
    }
    public String getSlashCommand() {
        return slashCommand;
    }
    String slashCommand = null;

    public void setRequiredSkill(AgisSkill skill, int level) {
        requiredSkill = skill;
        requiredSkillLevel = level;
    }
    public AgisSkill getRequiredSkill() {
        return requiredSkill;
    }
    public int getRequiredSkillLevel() {
        return requiredSkillLevel;
    }
    AgisSkill requiredSkill = null;
    int requiredSkillLevel = -1;

    public enum AbilityResult {
	SUCCESS,
	OUT_OF_RANGE,
	INVALID_TARGET,
	NO_TARGET,
	DEAD,
	UNKNOWN,
	PASSIVE,
	NOT_READY,
	TOO_CLOSE,
	OUT_OF_LOS,
	INSUFFICIENT_ENERGY,
	BAD_ASPECT,
	MISSING_REAGENT,
	MISSING_TOOL,
	MISSING_AMMO,
	BUSY,
	ABORT,
	INSUFFICIENT_VIGOR,
	NOT_IN_FRONT,
	NOT_BEHIND,
	NOT_BESIDE,
	EFFECT_MISSING,
	WRONG_STANCE,
	MISSING_WEAPON,
	INTERRUPTED
    }

    /**
     * Checks if the caster's current target is a valid target for the ability. 
     * @param caster
     * @param target
     * @return AbilityResult.SUCCESS if it is a valid target otherwise AbilityResult.INVALID_TARGET
     */
    protected AbilityResult checkTarget(CombatInfo caster, CombatInfo target) {
        if (Log.loggingDebug)
            Log.debug( "AgisAbility.checkTarget: obj=" + caster + " isUser=" + caster.isUser() + "target="
		       + target + " attackable=" + ((target==null)?"N/A":target.attackable()) );
        Log.debug("ABILITY: target type is: " + targetType);
        Log.debug("TARGET: friendly targets: " + caster.getFriendlyTargets());
        Log.debug("TARGET: attackable targets: " + caster.getAttackableTargets());
        switch (targetType) {
        	case NONE:
        	case SELF:
        		return AbilityResult.SUCCESS;
        	case ANY:
        		return AbilityResult.SUCCESS;
        	case AREA_ENEMY:
        		return AbilityResult.SUCCESS;
        	case AREA_FRIENDLY:
        		return AbilityResult.SUCCESS;
        	case FRIENDNOTSELF:
        		if (target.equals(caster))
        			return AbilityResult.INVALID_TARGET;
        		if (caster.getFriendlyTargets().containsKey(target.getOwnerOid())) {
        			return AbilityResult.SUCCESS;
        		} else {
        			return AbilityResult.INVALID_TARGET;
        		}
        	case FRIEND:
        		if (target.equals(caster) || caster.getFriendlyTargets().containsKey(target.getOwnerOid())) {
        			return AbilityResult.SUCCESS;
        		} else {
        			return AbilityResult.INVALID_TARGET;
        		}
        	case GROUP:
        		//TODO: add a group check in here
        		if (caster.isUser() && target.attackable())
        			return AbilityResult.INVALID_TARGET;
        		else
        			return AbilityResult.SUCCESS;
        	case ENEMY:
        		if (target.equals(caster)) {
        			if (reqTarget) {
        				Log.debug("MISS: enemy equals caster, but a target is required");
        				return AbilityResult.INVALID_TARGET;
        			} else {
        				Log.debug("MISS: enemy equals caster, but a target is not required, so setting to miss");
        				return AbilityResult.SUCCESS;
        			}
        		}
        		if (/*caster.isUser() && */!target.attackable()) {
        			if (reqTarget) {
        				return AbilityResult.INVALID_TARGET;
        			} else {
        				return AbilityResult.SUCCESS;
        			}
        		}
        		if ((caster.getAttackableTargets().containsKey(target.getOwnerOid()))) {
        			return AbilityResult.SUCCESS;
        		} else {
        			if (reqTarget) {
        				return AbilityResult.INVALID_TARGET;
        			} else {
        				return AbilityResult.SUCCESS;
        			}
        		}
        		//return AbilityResult.SUCCESS;
        	case OTHER:
        		if (target.equals(caster))
        			return AbilityResult.INVALID_TARGET;
        		return AbilityResult.SUCCESS;
        	default:
        		return AbilityResult.INVALID_TARGET;
        }
    }
    
    protected AbilityResult checkTargetSpecies(CombatInfo caster, CombatInfo target) {
    	OID targetOid = target.getOwnerOid();
    	String targetSpecies = "";
    	if (caster.equals(target))
    		return AbilityResult.SUCCESS;
    	if (caster.getAttackableTargets().containsKey(targetOid)) {
    		targetSpecies = caster.getAttackableTargets().get(targetOid).getSpecies();
    	} else if (caster.getFriendlyTargets().containsKey(targetOid)) {
    		targetSpecies = caster.getFriendlyTargets().get(targetOid).getSpecies();
    	}
    	for (TargetSpecies species: targetableSpecies) {
    		switch (species) {
    		case BEAST:
    			if (!targetSpecies.equals("Beast")) {
    				return AbilityResult.INVALID_TARGET;
    			}
    			break;
    		case HUMANOID:
    			if (!targetSpecies.equals("Humanoid")) {
    				return AbilityResult.INVALID_TARGET;
    			}
    			break;
    		case ELEMENTAL:
    			if (!targetSpecies.equals("Elemental")) {
    				return AbilityResult.INVALID_TARGET;
    			}
    			break;
    		case UNDEAD:
    			if (!targetSpecies.equals("Undead")) {
    				return AbilityResult.INVALID_TARGET;
    			}
    			break;
    		case PLAYER:
    			if (!target.isUser()) {
    				return AbilityResult.INVALID_TARGET;
    			}
    			break;
    		case NONPLAYER:
    			if (target.isUser()) {
    				return AbilityResult.INVALID_TARGET;
    			}
    			break;
    		}
    	}
    	
    	return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkSpecificTarget(CombatInfo caster, CombatInfo target) {
        if (Log.loggingDebug)
            Log.debug( "AgisAbility.checkSpecificTarget: obj=" + caster + " isUser=" + caster.isUser() + "target="
		       + target + " attackable=" + ((target==null)?"N/A":target.attackable()) );
        if (specificTargets != null) {
        	boolean acceptableTarget = false;
        	Log.debug("Checking specific target names: " + target.getName());
        	for (int i = 0; i < specificTargets.size(); i++) {
        		if (target.getName().equals(specificTargets.get(i))) {
        			acceptableTarget = true;
        		}
        	}
        	if (acceptableTarget == false)
        		return AbilityResult.INVALID_TARGET;
        }
        
        return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkState(CombatInfo obj, CombatInfo target) {
    	if (obj.getState() != null && obj.getState().equals(CombatInfo.COMBAT_STATE_INCAPACITATED)) {
    		return AbilityResult.BUSY;
    	} else if (obj.getState() != null && obj.getState().equals(CombatInfo.COMBAT_STATE_SPIRIT)) {
    		return AbilityResult.DEAD;
    	}
    	return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkHasAbility(CombatInfo obj, CombatInfo target) {
    	if (obj.isUser() && (this.skillType > 0) && obj.getAutoAttackAbility() != id) {
    		Log.debug("AGISABILITY: checking if player knows the ability: " + id);
    		ArrayList<Integer> currentAbilities = obj.getCurrentAbilities();
    		if (!currentAbilities.contains(id)) {
    			Log.debug("AGISABILITY: player does not know this ability: " + id);
    			return AbilityResult.UNKNOWN;
    		}
    	}
    	Log.debug("AGISABILITY: player knows the ability");
    	return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkAbilityType(CombatInfo obj, CombatInfo target) {
    	if (abilityType == 2) {
    		Log.debug("AGISABILITY: ability is passive, cannot be activated");
    	    return AbilityResult.PASSIVE;
    	}
    	return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkDeath(CombatInfo obj, CombatInfo target) {
    	// First check if the player is alive
    	if (obj.dead()) {
    		return AbilityResult.DEAD;
    	}
    	if (targetDeath == 0) {
    		// Ability requires a dead target
    		if (target.dead())
    			return AbilityResult.SUCCESS;
    		else
    			return AbilityResult.INVALID_TARGET;
    	} else if (targetDeath == 1) {
    		// Ability requires an alive target
    		if (target.dead())
    			return AbilityResult.INVALID_TARGET;
    		else
    			return AbilityResult.SUCCESS;
    	} else {
    		return AbilityResult.SUCCESS;
    	}
    }

    protected AbilityResult checkRange(CombatInfo obj, CombatInfo target, float rangeTolerance) {
        switch (targetType) {
        case ENEMY:
        case FRIEND:
        case GROUP:
            BasicWorldNode casterWNode = WorldManagerClient.getWorldNode(obj.getOwnerOid());
            BasicWorldNode targetWNode = WorldManagerClient.getWorldNode(target.getOwnerOid());
            Integer movementState = (Integer) EnginePlugin.getObjectProperty(target.getOwnerOid(), WorldManagerClient.NAMESPACE, 
            		AgisWorldManagerPlugin.PROP_MOVEMENT_STATE);
            if (casterWNode == null) {
            	Log.error("RANGE CHECK: wnode is null for caster: " + obj.getOwnerOid());
            	return AbilityResult.SUCCESS;
            }
            if (targetWNode == null) {
            	Log.error("RANGE CHECK: wnode is null for target: " + target.getOwnerOid());
            	return AbilityResult.SUCCESS;
            }
            Point casterLoc = casterWNode.getLoc();
            Point targetLoc = targetWNode.getLoc();
            int hitbox = (Integer) obj.getProperty(CombatPlugin.PROP_HITBOX) - 1;
            int targetHitbox = (Integer) target.getProperty(CombatPlugin.PROP_HITBOX) - 1;
            int distance = 0;
            if (CombatPlugin.RANGE_CHECK_VERTICAL) {
            	distance = (int)Point.distanceTo(casterLoc, targetLoc) - hitbox - targetHitbox;
            } else {
            	// If the target is flying do a 3 dimensional check
            	if (movementState != null && movementState == AgisWorldManagerPlugin.MOVEMENT_STATE_FLYING) {
            		distance = (int)Point.distanceTo(casterLoc, targetLoc) - hitbox - targetHitbox;
            	} else {
            		// Otherwise do a 2D check
            		distance = (int)Point.distanceToXZ(casterLoc, targetLoc) - hitbox - targetHitbox;
            		/*if (Math.abs(casterLoc.getY() - targetLoc.getY()) > 20) {
            			return AbilityResult.OUT_OF_RANGE;
            		}*/
            	}
            }
            Log.debug("AgisAbility.checkRange: range=" + distance + " casterLoc=" + casterLoc + " targetLoc=" + targetLoc);
            if (distance > (getMaxRange() * rangeTolerance)) {
                return AbilityResult.OUT_OF_RANGE;
            }
            if (distance < (getMinRange() / rangeTolerance) && getMinRange() > 0) {
                return AbilityResult.TOO_CLOSE;
            }
            return AbilityResult.SUCCESS;
        default:
            return AbilityResult.SUCCESS;
        }
    }

    protected AbilityResult checkReady(CombatInfo obj, CombatInfo target) {
 	if (obj.getCurrentAction() != null) {
 	    return AbilityResult.BUSY;
 	}
	if (!Cooldown.checkReady(cooldownMap.values(), obj)) {
	    return AbilityResult.NOT_READY;
	}
	return AbilityResult.SUCCESS;
    }

    protected AbilityResult checkCost(CombatInfo obj, CombatInfo target, ActivationState state) {
    	if (costProp == null || getActivationCost() < 1) {
            if (Log.loggingDebug)
                Log.debug("AgisAbility.checkCost: costProp=" + costProp);
            return AbilityResult.SUCCESS;
    	}
    	Integer costValue = obj.statGetCurrentValue(costProp);
        if (Log.loggingDebug)
            Log.debug("AgisAbility.checkCost: costProp=" + costProp + " value=" + costValue);
        switch (state) {
        case INIT:
        case ACTIVATING:
        	if (getActivationCost() > obj.statGetCurrentValue(costProp)) {
        		return AbilityResult.INSUFFICIENT_ENERGY;
        	}
        	break;
	case CHANNELLING:
	    if (getChannelCost() > obj.statGetCurrentValue(costProp)) {
		return AbilityResult.INSUFFICIENT_ENERGY;
	    }
	    break;
	case ACTIVATED:
	    if (getActiveCost() > obj.statGetCurrentValue(costProp)) {
		return AbilityResult.INSUFFICIENT_ENERGY;
	    }
	    break;
	}
	return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkVigor(CombatInfo obj, CombatInfo target, ActivationState state) {
    	if (getVigor() > -1 || obj.isMob()) {
            Log.debug("AgisAbility.checkCost: vigor=" + vigor);
    	    return AbilityResult.SUCCESS;
    	}
    	int vigorcost = getVigor() * -1;
    	switch (state) {
    	case INIT:
    	case ACTIVATING:
    	    if (vigorcost > obj.statGetCurrentValue("vigor")) {
    	    	Log.debug("ANDREW - vigorcost: " + vigorcost + "current vigor: " + obj.statGetCurrentValue("vigor"));
    		return AbilityResult.INSUFFICIENT_VIGOR;
    	    }
    	    break;
    	case CHANNELLING:
    	    break;
    	case ACTIVATED:
    	    break;
    	}
    	return AbilityResult.SUCCESS;
    }
    
    /*
     * This method checks if this ability has any effect requirements such as having dealt a crit,
     * having their last attack dodged, having used some ability within the past say 5 seconds.
     * 
     * As you can imagine, there are a huge amount of possibilities that we would have to check. So
     * the solution is to store all effects a player gets into a list (of numeric values) and then
     * just list what numeric effect values an ability requires to activate.
     * 
     * Note: there can be more than 1 effect required.
     */
    protected AbilityResult checkEffects(CombatInfo obj, CombatInfo target, ActivationState state) {
    	// First scroll through the players effects
    	/*Long playerOid = obj.getOwnerOid();*/
    	LinkedList<Integer> effects = (LinkedList) obj.getProperty("effects");
    	for (int i = 0; i < attackerEffectReqs.size(); i++) {
    		boolean effectPresent = false;
    	    for (int j = 0; j < effects.size(); j++) {
    		    if (attackerEffectReqs.get(i).equals(effects.get(j)))
    			    effectPresent = true;
    	    }
    	    if (!effectPresent) {
    		   	Log.debug("ANDREW - attacker missing effect: " + attackerEffectReqs.get(i));
    		    Log.debug("ANDREW - attacker missing effect. Attacker effects: " + effects);
    		    return AbilityResult.EFFECT_MISSING;
    	    }
        }
    	
    	    /*Long targetOid = target.getOwnerOid();*/
    	    effects = (LinkedList) target.getProperty("effects");
    	    for (int i = 0; i < targetEffectReqs.size(); i++) {
    		    boolean effectPresent = false;
    		    for (int j = 0; j < effects.size(); j++) {
    			    if (targetEffectReqs.get(i).equals(effects.get(j)))
    				    effectPresent = true;
    			    else
    			    	Log.debug("ANDREW - target missing effect: " + targetEffectReqs.get(i) + "; " + effects.get(j));
    		    }
    		    if (!effectPresent) {
    		    	Log.debug("ANDREW - target missing effect: " + targetEffectReqs.get(i));
    		        Log.debug("ANDREW - target missing effect. Target effects: " + effects);
    			    return AbilityResult.EFFECT_MISSING;
    		    }
    	    }
        return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkStance(CombatInfo obj, CombatInfo target, ActivationState state) {
    	OID playerOid = obj.getOwnerOid();
    	return AbilityResult.SUCCESS;
    }
    
    /*
     * This method checks to see if the ability requires the player to be at a certain position
     * relative to the target.
     */
    protected AbilityResult checkPosition(CombatInfo obj, CombatInfo target, ActivationState state) {
    	/*PlayerAngle angle = new PlayerAngle(CombatHelper.calculateValue(obj, target));
    	switch (position) {
	    	case 1:
	    		// Attacker must be in front of the target
	    		if (!angle.is_within(315, 45, false))
	    			return AbilityResult.NOT_IN_FRONT;
	    		break;
	    	case 2:
	    		// Attacker must be to the side of the target
	    		if (!(angle.is_within(45, 135, false) || angle.is_within(225, 315, false)))
	    			return AbilityResult.NOT_BESIDE;
	    		break;
	    	case 3:
	    		// Attacker must be behind the target
	    		if (!(angle.is_within(135, 225, false)))
	    			return AbilityResult.NOT_BEHIND;
	    		break;
    	}*/
    	return AbilityResult.SUCCESS;
    }
    
    /*
     * This method checks to see if there are any equipment requirements such as having a sword or shield.
     */
    protected AbilityResult checkEquip(CombatInfo obj, CombatInfo target, ActivationState state) {
    	//Long ioid = (Long)AgisInventoryClient.findItem(obj.getOid(), AgisEquipSlot.PRIMARYWEAPON);
    	//String weapType = "Unarmed";
    	String weapType = obj.getStringProperty("weaponType");
    	String weapType2 = obj.getStringProperty("weapon2Type");
    	if (weapType2 != null && !weapType2.equals("") && !weapType2.equals("null"))
    		weapType += obj.getStringProperty("weapon2Type");
    	
        //if (ioid != null)
            //weapType = (String)EnginePlugin.getObjectProperty(ioid, InventoryClient.ITEM_NAMESPACE, "type");
    	Log.debug("WEAPON: checking for weapon requirement: " + weaponReq + " against users weapon: " + weapType);
        
    	if (CombatPlugin.WEAPON_REQ_USES_SHARED_TYPES) {
    		if (weaponReq.equals("Melee Weapon")) {
            	if (weapType.contains("Unarmed"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (weaponReq.equals("1 Hand")) {
            	if (!weapType.contains("1H"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (weaponReq.equals("2 Hand")) {
            	if (!weapType.contains("2H"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (weaponReq.equals("Hammer")) {
            	if (!weapType.contains("Hammer"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (weaponReq.equals("Sword")) {
            	if (!weapType.contains("Sword"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (weaponReq.equals("Axe")) {
            	if (!weapType.contains("Axe"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (weaponReq.equals("Staff")) {
            	if (!weapType.contains("Staff"))
            		return AbilityResult.MISSING_WEAPON;
            } else if (!weaponReq.equals("")) {
            	if (!weapType.contains(weaponReq))
            		return AbilityResult.MISSING_WEAPON;
            }
    	} else if (!weaponReq.equals("")) {
    		if (!weapType.equals(weaponReq))
        		return AbilityResult.MISSING_WEAPON;
    	}
    	
        return AbilityResult.SUCCESS;
    }

    protected AbilityResult checkReagent(CombatInfo obj, CombatInfo target, ActivationState state) {
        if (state == ActivationState.INIT || state == ActivationState.ACTIVATING) {
            if (!reagentList.isEmpty()) {
                List<OID> itemList = InventoryClient.findItems(obj.getOwnerOid(), reagentList);
                if ((itemList == null) || itemList.contains(null)) {
                    return AbilityResult.MISSING_REAGENT;
                }
            }
        }
        return AbilityResult.SUCCESS;
    }

    protected AbilityResult checkTool(CombatInfo obj, CombatInfo target, ActivationState state) {
        if (state == ActivationState.INIT || state == ActivationState.ACTIVATING) {
            if (!toolList.isEmpty()) {
                List<OID> itemList = InventoryClient.findItems(obj.getOwnerOid(), toolList);
                if ((itemList == null) || itemList.contains(null)) {
                    return AbilityResult.MISSING_TOOL;
                }
            }
        }
        return AbilityResult.SUCCESS;
    }
    
    protected AbilityResult checkAmmo(CombatInfo obj, CombatInfo target, ActivationState state) {
        if (state == ActivationState.INIT || state == ActivationState.ACTIVATING) {
            if (ammoReq > 0) {
            	Integer ammoLoaded = (Integer) obj.getProperty(CombatInfo.COMBAT_AMMO_LOADED);
            	if (ammoLoaded == null || ammoLoaded < 1) {
            		return AbilityResult.MISSING_AMMO;
            	}
            	OID itemFound = InventoryClient.findItem(obj.getOwnerOid(), ammoLoaded);
                if (itemFound == null) {
                    return AbilityResult.MISSING_AMMO;
                }
            }
        }
        return AbilityResult.SUCCESS;
    }

    public AbilityResult checkAbility(CombatInfo obj, CombatInfo target) {
    	return checkAbility(obj, target, ActivationState.INIT);
    }

    protected AbilityResult checkAbility(CombatInfo obj, CombatInfo target, ActivationState state) {
    	AbilityResult result = AbilityResult.SUCCESS;
    	if (state == ActivationState.INIT) {
    	    result = checkReady(obj, target);
    	    if (result != AbilityResult.SUCCESS)
    	    	return result;
    	    result = checkTarget(obj, target);
    	    if (result != AbilityResult.SUCCESS) {
    	    	Log.debug("ABILITY: checkTarget failed");
    	    	// If this ability is the auto attack, stop it
    	    	if (id == obj.getAutoAttackAbility()) {
    	    		obj.stopAutoAttack();
    	    	}
    		    return result;
    	    }
    	    result = checkTargetSpecies(obj, target);
    	    if (result != AbilityResult.SUCCESS) {
    	    	Log.debug("ABILITY: checkTargetSpecies failed");
    	    	// If this ability is the auto attack, stop it
    	    	if (id == obj.getAutoAttackAbility()) {
    	    		obj.stopAutoAttack();
    	    	}
    	    	return result;
    	    }
    	    result = checkSpecificTarget(obj, target);
    	    if (result != AbilityResult.SUCCESS) {
    	    	Log.debug("ABILITY: checkSpecificTarget failed");
    	    	// If this ability is the auto attack, stop it
    	    	if (id == obj.getAutoAttackAbility()) {
    	    		obj.stopAutoAttack();
    	    	}
    	    	return result;
    	    }result = checkState(obj, target);
    	    if (result != AbilityResult.SUCCESS) {
    	    	Log.debug("ABILITY: checkState failed");
    	    	// If this ability is the auto attack, stop it
    	    	/*if (id == obj.getAutoAttackAbility()) {
    	    		obj.stopAutoAttack();
    	    	}*/
    	    	return result;
    	    }
    	    result = checkDeath(obj, target);
    	    if (result != AbilityResult.SUCCESS) {
    	    	Log.debug("ABILITY: checkDeath failed");
    	    	// If this ability is the auto attack, stop it
    	    	if (id == obj.getAutoAttackAbility()) {
    	    		obj.stopAutoAttack();
    	    	}
    	    	return result;
    	    }
    	    result = checkEffects(obj, target, state);
        	if (result != AbilityResult.SUCCESS)
        	    return result;
    	}
    	
    	result = checkHasAbility(obj, target);
    	if (result != AbilityResult.SUCCESS)
    		return result;
    	
    	result = checkAbilityType(obj, target);
    	if (result != AbilityResult.SUCCESS)
    		return result;

    	result = checkTool(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;
    	
    	result = checkEquip(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;

    	result = checkReagent(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;
    	
    	result = checkAmmo(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;

    	result = checkCost(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;
    	
    	// Check for vigor
    	result = checkVigor(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;
    	
    	// Check for stance
    	/*result = checkStance(obj, target, state);
    	if (result != AbilityResult.SUCCESS)
    	    return result;*/

     	if (state == ActivationState.INIT) {
     	    result = checkRange(obj, target, 1.0f);
     	}
     	else {
     		// Add a 20% tolerance for distance when the ability is finishing.
     	    result = checkRange(obj, target, 1.2f);
     	}
        if (Log.loggingDebug)
            Log.debug("AgisAbility.checkAbility result=" + result);
        return result;
    }

    protected AgisAbilityState generateState(CombatInfo source, CombatInfo target, AgisItem item, Point loc) {
		return new AgisAbilityState(this, source, target, item, loc);
    }
    
    public static void startAbility(AgisAbility ability, CombatInfo source, CombatInfo target, AgisItem item) {
		startAbility(ability, source, target, item, null);
    }

    public static void startAbility(AgisAbility ability, CombatInfo source, CombatInfo target, AgisItem item, Point loc) {
        if (Log.loggingDebug) {
            Log.debug("AgisAbility.startAbility ability=" + ability.getName() + " source=" + source + " target=" + target + " item=" + item + " loc=" + loc);
		}
        AgisAbilityState state = ability.generateState(source, target, item, loc);
        Log.debug("AgisAbility.startAbility generated state");
		state.updateState();
    }
    
    public static void abortAbility(AgisAbilityState state) {
		interruptAbility(state, AbilityResult.ABORT);

        CombatInfo combatInfo = state.getSource();
        Collection<Cooldown>cooldowns = state.getAbility().getCooldownMap().values();
        Cooldown.abortAllCooldowns(cooldowns, combatInfo);
		return;
    }

    public static void interruptAbility(AgisAbilityState state, AbilityResult reason) {
        if (Log.loggingDebug)
            Log.debug("AgisAbility.interruptAbility: reason=" + reason + " state=" + state.getState());

		if (state.getState() != ActivationState.INIT) {
			Engine.getExecutor().remove(state);
			if (state.getSource().getCurrentAction() == state) {
				state.getSource().setCurrentAction(null);
				EnginePlugin.setObjectPropertyNoResponse(state.getSource().getOwnerOid(), WorldManagerClient.NAMESPACE, "casting", "");
			    EnginePlugin.setObjectPropertyNoResponse(state.getSource().getOwnerOid(), WorldManagerClient.NAMESPACE, "castingAffinity", "");
			    //ExtendedCombatMessages.sendCastingCancelledMessage(state.getSource());
			    EventMessageHelper.SendCombatEvent(state.getSourceOid(), state.getTargetOid(), EventMessageHelper.COMBAT_CASTING_CANCELLED, 
        				state.getAbility().getID(), -1, -1, -1);
			}
			if (state.getState() == ActivationState.COMPLETED)
				return;
		}

		state.getAbility().interrupt(state);

		// XXX do something here about adjusting the time remaining
		// XXX up or down for cast or channelled abilities
		return;
    }

    public enum ActivationState {
		INIT,
		ACTIVATING,
		CHANNELLING,
		ACTIVATED,
		COMPLETED,
		CANCELLED,
		INTERRUPTED,
		FAILED
	}
    
    public AgisEffect getResultVal(String result, boolean caster) {
    	Log.debug("RESULT: getting effect for result: " + result);
    	
    	return null;
    }

    public static class Entry {
	public Entry() {
	}

	public Entry(String abilityName, String icon, String category) {
	    setAbilityName(abilityName);
	    setIcon(icon);
	    setCategory(category);
	}

	public Entry(AgisAbility ability, String category) {
	    setAbilityName(ability.getName());
	    setIcon(ability.getIcon());
	    setCategory(category);
	}

	public String getAbilityName() { return abilityName; }
	public void setAbilityName(String abilityName) {
	    this.abilityName = abilityName;
	}
	protected String abilityName;
	
	public int getAbilityID() { return abilityID; }
	public void setAbilityID(int abilityID) {
	    this.abilityID = abilityID;
	}
	protected int abilityID;

	public String getIcon() { return icon; }
	public void setIcon(String icon) { this.icon = icon; }
	protected String icon;

	public String getCategory() { return category; }
	public void setCategory(String category) { this.category = category; }
	protected String category;

	public AgisAbility getAbility() {
	    return Agis.AbilityManager.get(abilityID);
	}
    }
    
    /**
     * -Experience system component - Not currently used
     * 
     * This variable is used for setting up how much experience each successful
     * use of an ability is.
     */
    int exp_per_use = 0;

    /**
     * -Experience system component - Not currently used
     * 
     * Returns the amount of experience should be gained by successful use of
     * this ability.
     */
    public int getExperiencePerUse() {
        return exp_per_use;
    }

    /**
     * -Experience system component - Not currently used
     * 
     * Sets the amount of experience should be gained by successful use of this
     * ability.
     */
    public void setExperiencePerUse(int xp) {
        exp_per_use = xp;
    }

    LevelingMap lm = new LevelingMap();

    public void setLevelingMap(LevelingMap lm) {
        this.lm = lm;
    }

    public LevelingMap getLevelingMap() {
        return this.lm;
    }

    int exp_max = 100;

    /**
     * -Experience system component - Not currently used
     * 
     * Returns the default max experience that will be needed before the ability
     * gains a level.
     */
    public int getBaseExpThreshold() {
        return exp_max;
    }

    /**
     * -Experience system component - Not currently used
     * 
     * Sets the default max experience that will be needed before the ability
     * gains a level.
     */
    public void setBaseExpThreshold(int max) {
        exp_max = max;
    }

    int rank_max = 3;

    /**
     * -Experience system component - Not currently used
     * 
     * Returns the max rank that an ability may achieve.
     */
    public int getMaxRank() {
        return rank_max;
    }

    /**
     * -Experience system component - Not currently used
     * 
     * Sets the max rank that an ability may achieve.
     */
    public void setMaxRank(int rank) {
        rank_max = rank;
    }
    
    // Ability Result Values
    public static final int RESULT_HIT = 1;
    public static final int RESULT_CRITICAL = 2;
    public static final int RESULT_MISSED = 3;
    public static final int RESULT_PARRIED = 4;
    public static final int RESULT_DODGED = 5;
    public static final int RESULT_BLOCKED = 6;
    public static final int RESULT_EVADED = 10;
    public static final int RESULT_IMMUNE = 11;
    
}
