package atavism.agis.core;

import atavism.agis.core.AgisAbility.AbilityResult;
import atavism.agis.core.AgisAbility.ActivationState;
import atavism.agis.core.AgisAbility.TargetType;
import atavism.agis.objects.AgisItem;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.plugins.CombatClient;
import atavism.agis.util.EventMessageHelper;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;
import atavism.server.util.ObjectLockManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class AgisAbilityState implements Runnable, MessageCallback {
    public AgisAbilityState(AgisAbility ability, CombatInfo source, CombatInfo target, AgisItem item, Point loc) {
        if (ability.targetType == TargetType.SELF) {
            target = source;
        }
        this.ability = ability;
        this.source = source;
        if (target == null) {
            this.target = source;
        } else {
            this.target = target;
        }
        this.item = item;
        this.location = loc;
        
        if (ability.getActivationTime() > 0) {
	    	// Set up a message hook for when the caster is moving so we can interrupt
	    	SubjectFilter filter = new SubjectFilter(source.getOwnerOid());
	        filter.addType(CombatClient.MSG_TYPE_INTERRUPT_ABILITY);
	        sub = Engine.getAgent().createSubscription(filter, this);
	        Log.debug("AGIS ABILITY: subscribed to interrupt message");
	    }
    }
    
    /**
     * process network messages
     */
    public void handleMessage(Message msg, int flags) {
        if (msg instanceof CombatClient.interruptAbilityMessage) {
            processInterrupt();
        } else {
            Log.error("unknown msg: " + msg);
        }
    }
    
    private void processInterrupt() {
    	Log.debug("AGIS ABILITY: got ability interrupt");
    	state = ActivationState.INTERRUPTED;
    	AgisAbility.interruptAbility(this, AbilityResult.INTERRUPTED);
    	if (sub != null)
            Engine.getAgent().removeSubscription(sub);
    }

    public ActivationState nextState() {
        ActivationState newState;

        switch (state) {
        case INIT:
            newState = ActivationState.ACTIVATING;
            break;
        case ACTIVATING:
            if (ability.getChannelled()) {
                newState = ActivationState.CHANNELLING;
                break;
            }
            if (ability.getPersistent()) {
                newState =  ActivationState.ACTIVATED;
                break;
            }
            newState =  ActivationState.COMPLETED;
            break;
        case CHANNELLING:
            if (ability.getPersistent()) {
                newState = ActivationState.ACTIVATED;
                break;
            }
            newState = ActivationState.COMPLETED;
            break;
        case INTERRUPTED:
        	newState = ActivationState.FAILED;
        	break;
        default:
            Log.error("AgisAbility.nextState: invalid state=" + state);
            newState = ActivationState.COMPLETED;
            break;
        }
        if (Log.loggingDebug)
            Log.debug("AgisAbility.nextState: switching from " + state + " to " + newState);
        return newState;
    }

    public void run() {
        try {
        	Log.debug("AUTO: running AgisAbilityState");
            updateState();
        }
        catch (Exception e) {
            Log.exception("AgisAbility.State.run: got exception", e);
        }
    }

    // this is executed when the ability needs to update
    public void updateState() {
        if (potentialTargets == null) {
            if (state == ActivationState.INIT)
                potentialTargets = ability.getPotentialTargets(this);
        }            
        List<Lock> requiredLocks =  new ArrayList<Lock>();
        requiredLocks.add(source.getLock());
        if (potentialTargets == null && target != null) {
            requiredLocks.add(target.getLock());
        } else {
            for (CombatInfo potentialTarget : potentialTargets) {
                requiredLocks.add(potentialTarget.getLock());
            }
        }

        try {
            ObjectLockManager.lockAll(requiredLocks);

            Log.debug("AgisAbility.updateState: got locks");
            AbilityResult result;

            switch (state) {
            case INIT:
                result = ability.checkAbility(source, target, state);
                if (result != AbilityResult.SUCCESS) {
                    // we run init effects when we fail -- :(
                    for(CoordinatedEffect effect : ability.getCoordEffects(ActivationState.INIT)) {
                    	effect.putArgument("abilityID", ability.getID());
                        effect.invoke(getSourceOid(), getTargetOid(), location, this);
                    }
                    AgisAbility.interruptAbility(this, result);
                    return;
                }
                source.setCurrentAction(this);
                break;
            case ACTIVATING:
                result = ability.checkAbility(source, target, state);
                if (result != AbilityResult.SUCCESS) {
                    AgisAbility.interruptAbility(this, result);
                    return;
                }
                ability.completeActivation(this);
                break;
            case CHANNELLING:
                result = ability.checkAbility(source, target, state);
                if (result != AbilityResult.SUCCESS) {
                    AgisAbility.interruptAbility(this, result);
                    return;
                }
                ability.pulseChannelling(this);
                nextPulse++;
                if (nextPulse < ability.getChannelPulses()) {
                    schedule(ability.getChannelPulseTime());
                    return;
                }
                ability.completeChannelling(this);
                break;
            case ACTIVATED:
                result = ability.checkAbility(source, target, state);
                if (result != AbilityResult.SUCCESS) {
                    AgisAbility.interruptAbility(this, result);
                    return;
                }
                ability.pulseActivated(this);
                nextPulse++;
                schedule(ability.getActivePulseTime());
                return;
			case CANCELLED:
				break;
			case COMPLETED:
				break;
			case FAILED:
				break;
			case INTERRUPTED:
				break;
			default:
				break;
            }

            state = nextState();
            nextPulse = 0;

            for(CoordinatedEffect effect : ability.getCoordEffects(state)) {
            	effect.putArgument("abilityID", ability.getID());
                effect.invoke(getSourceOid(), getTargetOid(), location, this);
            }

            switch (state) {
            case ACTIVATING:
                ability.beginActivation(this);
                Long activationTime = ability.getActivationTime();
                if (activationTime > 0) {
                	String anim = ability.getCastingAnim();
                	if (!anim.equals("")) {
                		EnginePlugin.setObjectPropertyNoResponse(getSource().getOwnerOid(), WorldManagerClient.NAMESPACE, "casting", anim);
                	}
                	String affinity = ability.getCastingAffinity();
                	if (!affinity.equals("")) 
                		EnginePlugin.setObjectPropertyNoResponse(getSource().getOwnerOid(), WorldManagerClient.NAMESPACE, "castingAffinity", affinity);
                }
                // Send casting started event
                EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), EventMessageHelper.COMBAT_CASTING_STARTED, 
        				ability.id, -1, (int)(ability.getActivationTime() / 1000), -1);
                //ExtendedCombatMessages.sendCastingStartedMessage(getSource(), ability.getActivationTime());
                Log.debug("AUTO: setting duration");
                setDuration(ability.getActivationTime());
                Log.debug("AUTO: scheduling duration");
                schedule(ability.getActivationTime());
                Log.debug("AUTO: duration scheduled");
                break;
            case CHANNELLING:
                ability.beginChannelling(this);
                setDuration(ability.getChannelPulses() * ability.getChannelPulseTime());
                schedule(ability.getChannelPulseTime());
                break;
            case ACTIVATED:
                ability.beginActivated(this);
                source.setCurrentAction(null);
                source.addActiveAbility(this);
                schedule(ability.getActivePulseTime());
                break;
            case COMPLETED:
                if (source.getCurrentAction() == this) {
                    source.setCurrentAction(null);
                }
                EnginePlugin.setObjectPropertyNoResponse(getSource().getOwnerOid(), WorldManagerClient.NAMESPACE, "casting", "");
                EnginePlugin.setObjectPropertyNoResponse(getSource().getOwnerOid(), WorldManagerClient.NAMESPACE, "castingAffinity", "");
                break;
            default:
                Log.error("AgisAbility.State.run: new state invalid=" + state);
                break;
            }
            Log.debug("AUTO: sending ability progress message");
            //Engine.getAgent().sendBroadcast(new CombatClient.AbilityProgressMessage(this));
            Log.debug("AUTO: finished ability state update");
            return;
        }
        finally {
        	Log.debug("AUTO: unlocking ability state update");
            ObjectLockManager.unlockAll(requiredLocks);
            Log.debug("AUTO: returning from ability state update");
        }
    }

    protected void schedule(long delay) {
        setTimeRemaining(delay);
        Engine.getExecutor().schedule(this, delay, TimeUnit.MILLISECONDS);
    }

    public AgisAbility getAbility() { return ability; }
    public void setAbility(AgisAbility ability) { this.ability = ability; }
    protected AgisAbility ability;

    protected List<CombatInfo> potentialTargets = null;
    public List<CombatInfo> getPotentialTargets() {
        return potentialTargets;
    }
    /**
     * @deprecated  Replaced by {@link #getSource()}
     */
    @Deprecated public CombatInfo getObject() { return source; }
    /**
     * @deprecated  Replaced by {@link #setSource()}
     */
    @Deprecated public void setObject(CombatInfo source) { this.source = source; }
    public CombatInfo getSource() { return source; }
    public OID getSourceOid() { return (source == null ? null : source.getOwnerOid()); }
    public void setSource(CombatInfo source) { this.source = source; }
    protected CombatInfo source;

    public CombatInfo getTarget() { return target; }
    public OID getTargetOid() { return (target == null ? null : target.getOwnerOid()); }
    public void setTarget(CombatInfo target) { this.target = target; }
    protected CombatInfo target;

    public AgisItem getItem() { return item; }
    public void setItem(AgisItem item) { this.item = item; }
    protected AgisItem item;

    public long getNextWakeupTime() { return nextWakeupTime; }
    public long getTimeRemaining() { return nextWakeupTime - System.currentTimeMillis(); }
    public void setTimeRemaining(long time) { nextWakeupTime = System.currentTimeMillis() + time; }
    protected long nextWakeupTime;

    public long getDuration() { return duration; }
    public void setDuration(long duration) { this.duration = duration; }
    protected long duration;

    public ActivationState getState() { return state; }
    public void setState(ActivationState state) { this.state = state; }
    protected ActivationState state = ActivationState.INIT;

    public int getNextPulse() { return nextPulse; }
    public void setNextPulse(int num) { nextPulse = num; }
    protected int nextPulse = 0;

    public Point getLocation() { return location; }
    public void setLocation(Point loc) { location = loc; }
    protected Point location = null;
    
    transient Long sub = null;
}
