package atavism.agis.core;

import atavism.agis.effects.DamageMitigationEffect;
import atavism.agis.objects.AgisItem;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.VitalityStatDef;
import atavism.agis.plugins.ArenaClient;
import atavism.agis.plugins.CombatPlugin;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.util.AORuntimeException;
import atavism.server.util.Log;
import atavism.server.util.ObjectLockManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class AgisEffect implements Serializable{
    public AgisEffect() {
    }

    public AgisEffect(int id, String name) {
    	setID(id);
        setName(name);
    }
    
    /**
     * Variables for effect stacking
     */
	public boolean multipleCopies = false;
	public boolean replacable = true;
	public int stackLimit = 1; // A default value of 1 means it cannot be stacked
	public int getStackLimit() {
		return this.stackLimit;
	}
	public void setStackLimit(int stackLimit) {
		this.stackLimit = stackLimit;
	}
	public void setMultipleCopies(boolean multipleCopies) {
		this.multipleCopies = multipleCopies;
	}
	public void setReplacable(boolean replacable) {
		this.replacable = replacable;
	}
	

    public String toString() {
        return "[AgisEffect: " + getName() + "]";
    }

    public boolean equals(Object other) {
        AgisEffect otherEffect = (AgisEffect) other;
        boolean val = getName().equals(otherEffect.getName());
        return val;
    }

    public int hashCode() {
        int hash = getName().hashCode();
        return hash;
    }
    
    /**
     * the name is used to refer to the effect, so use a unique name
     */
    public void setID(int id) { this.id = id; }
    public int getID() { return id; }
    int id = -1;
    
    /**
     * the name is used to refer to the effect, so use a unique name
     */
    public void setName(String name) { this.name = name; }
    public String getName() { return name; }
    String name = null;

    // add the effect to the object
    public void apply(EffectState state) {
        if (Log.loggingDebug)
            Log.debug("AgisEffect.apply: applying effect " + state.getEffectName() + " to " +
                      state.getSource());
        
        // There is no point running the rest of the code if there is no effectVal
        if (state.getParams() == null)
        	return;
        if (state.getParams().get("effectVal") == null) 
        	return;
        
        // If this effect uses any effects to activate another effect
    	if (bonusEffect != -1 && bonusEffectReq != -1) {
    		CombatInfo target = state.getTarget();
            CombatInfo caster = state.getSource();
    		Log.debug("EFFECT: going to apply bonus effect: " + bonusEffect + " with req: " + bonusEffectReq);
    		boolean effectPresent = false;
		    for (EffectState existingState : target.getCurrentEffects()) {
			    if (bonusEffectReq == existingState.getEffect().getID()) {
				    effectPresent = true;
		    	}
		    }
		    if (effectPresent) {
		    	if (bonusEffectReqConsumed) {
    		    	AgisEffect.removeEffectByID(target, bonusEffectReq);
    		    	Log.debug("BONUS: removed effect position: " + effectPresent);
		    	}
		    	AgisEffect extraEffect = Agis.EffectManager.get(bonusEffect);
		    	AgisEffect.applyEffect(extraEffect, caster, target, state.getAbilityID(), state.getParams());
		    	Log.debug("BONUS: applied bonus effect: " + bonusEffect + " to obj: " + target.getName());
		    }
    	}
    }
    
    public void unload(EffectState state, CombatInfo target) {
    	if (Log.loggingDebug)
            Log.debug("AgisEffect.unload: unloading effect " + state.getEffectName() + " from " +
                      state.getTargetOid());
    }

    // remove the effect from the object
    public void remove(EffectState state) {
        if (Log.loggingDebug)
            Log.debug("AgisEffect.remove: removing effect " + state.getEffectName() + " from " +
            		state.getTargetOid());
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
        if (Log.loggingDebug)
            Log.debug("AgisEffect.pulse: pulsing effect " + state.getEffectName() + " on " + state.getSource());
    }
    
    /**
     * This function checks to see what stacking behaviour this effect has, and returns an existing
     * EffectState if there is one to be over-written.
     * Return value meanings:
     * 0: Do not apply this new effect
     * 1: Apply this new effect and remove and existing one by the same player
     * 2: Apply and increment the stack but remove the one that belongs to this player
     * 3: Apply and remove the existing one
     * 4: Apply and increment the stack while removing the existing one
     * @return an Integer referring to what should be done.
     */
    public int stackCheck() {
    	// If an effect isn't replaceable there is no point checking anything else
    	if (this.replacable == false)
    		return 0;
    	if (this.multipleCopies == true) {
    		if (this.stackLimit == 1)
    			return 1;
    		else
    			return 2;
    	} else {
    		// There is only 1 copy of the effect allowed on the target
    		if (this.stackLimit == 1)
    			return 3;
    		else
    			return 4;
    	}
    }
    
    protected int checkDamageMitigation(EffectState state, int damageAmount) {
    	CombatInfo target = state.getTarget();
    	DamageMitigationEffect dEffect = null;
    	for (EffectState existingState : target.getCurrentEffects()) {
		    if (existingState.getEffect().getClass().equals(DamageMitigationEffect.class)) {
		    	dEffect = (DamageMitigationEffect) existingState.getEffect();
			    break;
	    	}
	    }
    	if (dEffect != null) {
    		damageAmount = dEffect.mitigateDamage(damageAmount);
    		if (dEffect.isEffectCompleted())
    			AgisEffect.removeEffectByID(target, dEffect.getID());
    	}
    	return damageAmount;
    }
    
    public String getDamageType() { return damageType; }
    public void setDamageType(String type) { damageType = type; }
    protected String damageType = "";
    
    public int GetEffectSkillType() { return effectSkillType; }
    public void setEffectSkillType(int type) { effectSkillType = type; }
    public int effectSkillType = 0;
    
    public List<Float> getSkillEffectMod() { return skillEffectMod; }
    public void setSkillEffectMod(float mod) { skillEffectMod.add(mod);}
    protected List<Float> skillEffectMod = new ArrayList<Float>();
    
    public float getSkillDurationMod() { return skillDurationMod; }
    public void setSkillDurationMod(float mod) { skillDurationMod = mod; }
    protected float skillDurationMod = 0.0f;

    public long getDuration() { return duration; }
    public void setDuration(long dur) { duration = dur; }
    protected long duration = 0;

    public int getNumPulses() { return numPulses; }
    public  void setNumPulses(int num) { this.numPulses = num; }
    protected int numPulses = 0;
    public long getPulseTime() { return (numPulses > 0) ? (duration/numPulses) : 0; }
    
    public void setBonusEffectReq(int effectNum) { bonusEffectReq = effectNum;}
    public int getBonusEffectReq() { return bonusEffectReq;}
    protected int bonusEffectReq = -1;
    
    public void setBonusEffectReqConsumed(boolean consumed) { bonusEffectReqConsumed = consumed;}
    public boolean getBonusEffectReqConsumed() { return bonusEffectReqConsumed;}
    protected boolean bonusEffectReqConsumed = false;
    
    public void setBonusEffect(int bonusEffect) { this.bonusEffect = bonusEffect;}
    public int getBonusEffect() { return bonusEffect;}
    protected int bonusEffect = -1;
    
    public void isBuff(boolean isBuff) { this.isBuff = isBuff;}
    public boolean isBuff() { return isBuff;}
    protected boolean isBuff = false;

    public void setIcon(String icon) { this.icon = icon; }
    public String getIcon() { return (icon == null) ? "UNKNOWN_ICON" : icon; }
    String icon = null;

    public boolean isPeriodic() { return periodic; }
    public void isPeriodic(boolean b) { periodic = b; }
    private boolean periodic = false;

    // Persistent effects are non-instant effects. They will hav a scheduler run when being applied.
    public boolean isPersistent() { return persistent; }
    public void isPersistent(boolean b) { persistent = b; }
    private boolean persistent = false;
    
    // Passive effects are never removed unless the person unlearns the ability
    public boolean isPassive() { return passive; }
    public void isPassive(boolean b) { passive = b; }
    private boolean passive = false;
    
    // Continuous effects don't get removed after login/logout and upon death
    // Usually only stat and property effects should be continuous
    public boolean isContinuous() { return continuous; }
    public void isContinuous(boolean b) { continuous = b; }
    private boolean continuous = false;
    
    public boolean canApplyToImmune() { return applyToImmune; }
    public void canApplyToImmine(boolean canApply) { applyToImmune = canApply; }
    private boolean applyToImmune = false;
    
    public int getDuelEffect() { return duelEffect; }
    public void setDuelEffect(int duelID) { duelEffect = duelID; }
    private int duelEffect = -1;
    
    protected boolean checkDuelDefeat(CombatInfo target, CombatInfo caster, String damageProperty) {
    	// Is the target a player and in a duel?
    	if (target.isUser()) {
    		Integer duelID = (Integer) target.getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
    		if (duelID != null && duelID > -1) {
    			// The target is in a duel
    			if (caster.isUser()) {
    				// Check if the caster is in the same duel as the target
        			Integer duel2ID = (Integer) caster.getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
        			if (duel2ID != null && duelID == duel2ID) {
            			// Does the damage property stat kill the player if it reaches 0?
            			if (CombatPlugin.lookupStatDef(damageProperty) instanceof VitalityStatDef) {
            				VitalityStatDef statDef = (VitalityStatDef) CombatPlugin.lookupStatDef(damageProperty);
            				if (statDef.getOnMinHit() != null && statDef.getOnMinHit().equals("death")) {
            					// Send a defeat/removal message
            					ArenaClient.duelDefeat(target.getOwnerOid());
                            	return true;
            				}
            			}
            		}
        		}
    			ArenaClient.duelDefeat(target.getOwnerOid());
    		}
    	}
    	return false;
    }
    
    public LinkedList<EffectState> getTargetEffectsOfMatchingType(CombatInfo target) {
    	LinkedList<EffectState> matchingStates = new LinkedList<EffectState>();
    	if (target != null) {
    		for (EffectState state :target.getCurrentEffects()) {
    			if (state.getEffect().getClass().equals(this.getClass()))
    				matchingStates.add(state);
    		}
    	}
    	return matchingStates;
    }

    protected EffectState generateState(CombatInfo source, CombatInfo target, Map params) {
        return new EffectState(this, source, target, params, -1);
    }
    protected EffectState generateState(CombatInfo source, CombatInfo target, Map params, int abilityID) {
        return new EffectState(this, source, target, params, abilityID);
    }
    protected EffectState generateState(CombatInfo source, CombatInfo target, Map params, int abilityID, AgisItem item) {
        return new EffectState(this, source, target, params, abilityID, item);
    }

    public static EffectState applyEffect(AgisEffect effect, CombatInfo source, CombatInfo target, int abilityID) {
        return applyEffect(effect, source, target, null, abilityID, null);
    }

    public static EffectState applyEffect(AgisEffect effect, CombatInfo source, CombatInfo target, int abilityID, Map params) {
        return applyEffect(effect, source, target, params, abilityID, null);
    }
    public static EffectState applyEffect(AgisEffect effect, CombatInfo source, CombatInfo target, int abilityID, Map params, AgisItem item) {
        return applyEffect(effect, source, target, params, abilityID, item);
    }

    public static EffectState applyEffect(AgisEffect effect, CombatInfo source, CombatInfo target, Map params, int abilityID, AgisItem item) {
    	// Not sure where to go this, but we need to go an Evade and Immune check if it isn't a friendly ability
    	if (target != null && target.getState() != null && target.getState().equals(CombatInfo.COMBAT_STATE_EVADE)) {
    		EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), EventMessageHelper.COMBAT_EVADED, 
    				abilityID, effect.id, -1, -1);
    		return null;
    	}
    	if (!effect.canApplyToImmune() && target != null && target.getState() != null 
    			&& target.getState().equals(CombatInfo.COMBAT_STATE_IMMUNE)) {
    		EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), EventMessageHelper.COMBAT_IMMUNE, 
    				abilityID, effect.id, -1, -1);
    		return null;
    	}
    	
    	List<Lock> requiredLocks = new ArrayList<Lock>();
        if (source != null) {
            requiredLocks.add(source.getLock());
        }
        if (target != null) {
            requiredLocks.add(target.getLock());
        }

        try {
            ObjectLockManager.lockAll(requiredLocks);
        	//Log.debug("AGISEFFECT: attempting to apply effect: " + effect.getName() + " to: " + obj.getName());
            EffectState state = effect.generateState(source, target, params, abilityID, item);

            if (effect.isPeriodic() && !effect.isPersistent()) {
                throw new AORuntimeException("AgisEffect: periodic effects must be persistent");
            }
            if (effect.isPersistent()) {
                target.addEffect(state);
                if (effect.isPeriodic()) {
                    state.setNextPulse(0);
                    state.schedule(effect.getPulseTime());
                }
                else 
                {
                    state.schedule(effect.getDuration());
                    Log.debug("AGISEFFECT: effect being applied is persistent but not periodic with a duration of:" + effect.getDuration());
                }
            } else if (effect.isPassive() || effect.isContinuous()) {
            	// Save to the characters properties so it can be retrieved later
            	target.addEffect(state);
            	Log.debug("AGISEFFECT: effect being applied is passive or continuous");
            }
            effect.apply(state);
            return state;
        }
        finally {
            ObjectLockManager.unlockAll(requiredLocks);
        }
    }
    
    /**
     * Finds an effect on the target that matches the ID passed in
     * @param state
     */
    public static boolean removeEffectByID(CombatInfo target, int effectID) {
    	EffectState stateToRemove = null;
    	for (EffectState effect :target.getCurrentEffects()) {
    		if (effect.effect.id == effectID) {
    			stateToRemove = effect;
    			break;
    		}
    	}
    	if (stateToRemove == null)
    		return false;
    	
    	CombatInfo source = stateToRemove.getSource();
        List<Lock> requiredLocks = new ArrayList<Lock>();
        if (source != null) {
            requiredLocks.add(source.getLock());
        }
        if (target != null) {
            requiredLocks.add(target.getLock());
        }
        Log.debug("EFFECT: removing effect with ID: " + stateToRemove.getEffectName());
        try {
            ObjectLockManager.lockAll(requiredLocks);
            if (stateToRemove.getEffect() == null) {  // broken effect... clean up -- CYC 2009/07/16
            	stateToRemove.isActive(false);
                Engine.getExecutor().remove(stateToRemove);
                stateToRemove.getTarget().removeEffect(stateToRemove);
                Log.warn("AgisEffect.removeEffect: removing a null effect - effectName=" + stateToRemove.getEffectName());
            } else {
                if (!stateToRemove.getEffect().isPersistent()) {
                    Log.warn("AgisEffect.removeEffect: removing a non-persistent effect: oid=" 
                    		+ stateToRemove.getTargetOid() + " sourceOid=" + stateToRemove.getSourceOid() 
                    		+ " effectName=" + stateToRemove.getEffectName());
                }
                stateToRemove.isActive(false);
                Engine.getExecutor().remove(stateToRemove);
                stateToRemove.getEffect().remove(stateToRemove);
                stateToRemove.getTarget().removeEffect(stateToRemove);
            }
        }
        finally {
            ObjectLockManager.unlockAll(requiredLocks);
        }
        return true;
    }
    
    public static void removeEffect(AgisEffect.EffectState state) {
    	CombatInfo target = state.getTarget();
    	removeEffect(state, target);
    }

    public static void removeEffect(AgisEffect.EffectState state, CombatInfo target) {
    	CombatInfo source = state.getSource();
        List<Lock> requiredLocks = new ArrayList<Lock>();
        if (source != null) {
            requiredLocks.add(source.getLock());
        }
        if (target != null) {
            requiredLocks.add(target.getLock());
        }
        Log.debug("EFFECT: removing effect: " + state.getEffectName());
        try {
            ObjectLockManager.lockAll(requiredLocks);
            if (state.getEffect() == null) {  // broken effect... clean up -- CYC 2009/07/16
                state.isActive(false);
                Engine.getExecutor().remove(state);
                target.removeEffect(state);
                Log.warn("AgisEffect.removeEffect: removing a null effect - effectName=" + state.getEffectName() + "; ID=" + state.getEffectID());
            } else {
                if (!state.getEffect().isPersistent()) {
                    Log.warn("AgisEffect.removeEffect: removing a non-persistent effect: oid=" + state.getTargetOid() + " sourceOid=" + state.getSourceOid() + " effectName=" + state.getEffectName());
                }
                state.isActive(false);
                Engine.getExecutor().remove(state);
                state.getEffect().remove(state);
                Log.debug("EFFECT: about to remove effect from target: " + target);
                target.removeEffect(state);
                Log.debug("EFFECT: removed effect from target");
            }
        }
        finally {
            ObjectLockManager.unlockAll(requiredLocks);
        }
    }
    
    /**
     * Used when a player logs in to ensure all old non-continuous effects are removed.
     * @param target
     */
    public static void removeNonContinuousEffects(CombatInfo target, boolean resume) {
    	LinkedList<EffectState> effectsToRemove = new LinkedList<EffectState>();
    	for (EffectState state :target.getCurrentEffects()) {
    		Log.debug("AGISEFFECT: checking to remove effect: " + state.effectName + " it is null? " + state.getEffect());
    		if (resume) {
    			AgisEffect effect = Agis.EffectManager.get(state.getEffectID());
    			if (!effect.isContinuous()) {
    				effectsToRemove.add(state);
    				Log.debug("AGISEFFECT: adding effect to remove: " + state.effectName);
    			} else {
    				state.resume();
    			}
    		} else if (state.getEffect() == null || !state.effect.isContinuous()) {
    			effectsToRemove.add(state);
    			Log.debug("AGISEFFECT: adding effect to remove: " + state.effectName);
    		}
    	}
    	for (EffectState state : effectsToRemove) {
    		CombatInfo source = state.getSource();
            List<Lock> requiredLocks = new ArrayList<Lock>();
            if (source != null) {
                requiredLocks.add(source.getLock());
            }
            if (target != null) {
                requiredLocks.add(target.getLock());
            }
            Log.debug("EFFECT: unloading effect: " + state.getEffectName());
            try {
                ObjectLockManager.lockAll(requiredLocks);
                if (state.getEffect() == null) {  // broken effect... clean up -- CYC 2009/07/16
                    state.isActive(false);
                    Engine.getExecutor().remove(state);
                    target.removeEffect(state);
                    Log.warn("AgisEffect.removeNonContinuousEffects: removing a null effect - effectName=" + state.getEffectName() + "; ID=" + state.getEffectID());
                } else {
                    if (!state.getEffect().isPersistent()) {
                        Log.warn("AgisEffect.removeNonContinuousEffects: removing a non-persistent effect: oid=" + state.getTargetOid() + " sourceOid=" + state.getSourceOid() + " effectName=" + state.getEffectName());
                    }
                    //state.getEffect().remove(state);
                    state.isActive(false);
                    Engine.getExecutor().remove(state);
                    state.getEffect().unload(state, target);
                    target.removeEffect(state);
                }
            }
            finally {
                ObjectLockManager.unlockAll(requiredLocks);
            }
    	}
    }
    
    /**
     * Used when a target dies in to ensure all old non-passive effects are removed.
     * @param target
     */
    public static void removeNonPassiveEffects(CombatInfo target) {
    	LinkedList<EffectState> effectsToRemove = new LinkedList<EffectState>();
    	for (EffectState state :target.getCurrentEffects()) {
    		if (!state.effect.isPassive())
    			effectsToRemove.add(state);
    	}
    	for (EffectState state : effectsToRemove) {
    		removeEffect(state);
    	}
    }

    public static class EffectState implements Runnable, Serializable {
        public EffectState() {
        	defaultName = null;
        	defaultValue = null;
        }

        public EffectState(AgisEffect effect, CombatInfo source, CombatInfo target, Map params) {
        	this();
            this.effect = effect;
            this.effectID = effect.getID();
            this.effectName = effect.getName();
            this.sourceOid = source.getOid();
            this.targetOid = target.getOid();
            this.params = params;
            this.abilityID = -1;
            
            this.setTimeUntilEnd(effect.getDuration());
        }
        public EffectState(AgisEffect effect, CombatInfo source, CombatInfo target, Map params, int abilityID) {
        	this();
            this.effect = effect;
            this.effectID = effect.getID();
            this.effectName = effect.getName();
            this.sourceOid = source.getOid();
            this.targetOid = target.getOid();
            this.params = params;
            this.abilityID = abilityID;
            
            this.setTimeUntilEnd(effect.getDuration());
        }
        public EffectState(AgisEffect effect, CombatInfo source, CombatInfo target, Map params, int abilityID, AgisItem item) {
        	this();
            this.effect = effect;
            this.effectID = effect.getID();
            this.effectName = effect.getName();
            this.sourceOid = source.getOid();
            this.targetOid = target.getOid();
            this.params = params;
            this.abilityID = abilityID;
            this.item = item;
            
            this.setTimeUntilEnd(effect.getDuration());
        }

        public void run() {
            try {
                updateState();
            }
            catch (AORuntimeException e) {
                Log.exception("EffectState.run: got exception", e);
            }
        }

        public void updateState() {
        	Log.debug("EFFECT: running updateState for " + effectName);
            if (!isActive()) {
                return;
            }
            if (effect.isPeriodic()) {
                effect.pulse(this);
                nextPulse++;
                if (nextPulse < effect.getNumPulses() || effect.isPassive()) {
                    schedule(effect.getPulseTime());
                    return;
                }
            }
            if (effect.isPassive())
            	return;
            Log.debug("EFFECT: going to remove effect " + getEffectName());
            AgisEffect.removeEffect(this);
        }

        public void schedule(long delay) {
            setTimeRemaining(delay);
            Engine.getExecutor().schedule(this, delay, TimeUnit.MILLISECONDS);
        }

        public void resume() {
            effect = Agis.EffectManager.get(effectID);
            if (Log.loggingDebug)
                Log.debug("AgisEffect.resume: effectName=" + effectName + " effect=" + effect + " timeRemaining="
		          + getTimeRemaining() + " isContinuous? " + effect.isContinuous() + " or passive: " + effect.isPassive());
            if (/*!effect.isContinuous() &&*/ !effect.isPassive())
            	Engine.getExecutor().schedule(this, getTimeRemaining(), TimeUnit.MILLISECONDS);
        }

        public AgisEffect getEffect() { return effect; }
        protected transient AgisEffect effect = null;
        //protected AgisEffect effect = null;
        
        public int getEffectID() { return effectID; }
        public void setEffectID(int effectID) { this.effectID = effectID; }
        protected int effectID;

        public String getEffectName() { return effectName; }
        public void setEffectName(String effectName) { this.effectName = effectName; }
        protected String effectName;
        
        public String getDefaultName() { return defaultName; }
        public void setDefaultName(String defaultName) { this.defaultName = defaultName; }
        protected String defaultName;
        
        public Serializable getDefaultValue() { return defaultValue; }
        public void setDefaultValue(Serializable defaultValue) { this.defaultValue = defaultValue; }
        protected Serializable defaultValue;
        
        public int getAbilityID() { return abilityID; }
        public void setAbilityID(int abilityID) { this.abilityID = abilityID; }
        protected int abilityID;

        public CombatInfo getTarget() { 
        	Log.debug("EFFECT: getting target: " + targetOid);
        	return CombatPlugin.getCombatInfo(targetOid); 
        }
        public OID getTargetOid() { return targetOid; }
        public void setTargetOid(OID oid) { targetOid = oid; }
        protected OID targetOid;

        public CombatInfo getSource() { return CombatPlugin.getCombatInfo(sourceOid); }
        public OID getSourceOid() { return sourceOid; }
        public void setSourceOid(OID oid) { sourceOid = oid; }
        protected OID sourceOid = null;

        public long getNextWakeupTime() { return nextWakeupTime; }
        public long getTimeRemaining() { return nextWakeupTime - System.currentTimeMillis(); }
        public void setTimeRemaining(long time) { nextWakeupTime = System.currentTimeMillis() + time; }
        protected long nextWakeupTime;
        
        public long getEndTime() { return endTime; }
        public long getTimeUntilEnd() { return endTime - System.currentTimeMillis(); }
        public void setTimeUntilEnd(long time) { endTime = System.currentTimeMillis() + time; }
        protected long endTime;

        public int getNextPulse() { return nextPulse; }
        public void setNextPulse(int num) { nextPulse = num; }
        protected int nextPulse = 0;

        public boolean isActive() { return active; }
        public void isActive(boolean active) { this.active = active; }
        protected boolean active = true;

        public Map getParams() { return params; }
        public void setParams(Map params) { this.params = params; }
        protected Map params = null;
        
        public AgisItem getItem() { return item; }
        public void setItem(AgisItem item) { this.item = item; }
        protected AgisItem item = null;
        
        public int getCurrentStack() { return this.currentStack; }
        public void setCurrentStack(int currentStack) {
        	if (effect != null) {
        		if (currentStack > effect.stackLimit)
        			currentStack = effect.stackLimit;
        	}
    		this.currentStack = currentStack;
    	}
        protected int currentStack = 1;
        
        public OID getStackCaster() { return stackCaster; }
        public void setStackCaster(OID caster) { this.stackCaster = caster; }
        protected OID stackCaster;
        
        private static final long serialVersionUID = 1L;
    }
    
    private static final long serialVersionUID = 1L;
}