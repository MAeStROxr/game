package atavism.agis.core;

import atavism.agis.objects.AgisEquipSlot;
import atavism.agis.objects.AgisItem;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.InventoryInfo;
import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.AgisInventoryPlugin;
import atavism.agis.plugins.AgisInventoryPlugin.EquipMap;
import atavism.agis.plugins.CombatClient;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.util.Log;

/**
 * an activate hook for items that trigger abilities
 * when the item is activated, the mob uses the ability
 */
public class AmmoItemActivateHook implements ActivateHook {
    public AmmoItemActivateHook() {
    	super();
    }
    public AmmoItemActivateHook(int ammoType) {
    	super();
    	setAmmo(ammoType);
    }

    public void setAmmo(int ammoType) {
        Log.debug("AJ: setting ammoType to: " + ammoType);
        this.ammoType = ammoType;
    }
    public int getAmmo() {
    	return ammoType;
    }
    protected int ammoType;
    
    public void setAmmoEffectID(int ammoEffectID) {
        Log.debug("AJ: setting ammoType to: " + ammoEffectID);
        this.ammoEffectID = ammoEffectID;
    }
    public int getAmmoEffectID() {
    	return ammoEffectID;
    }
    protected int ammoEffectID;

    public boolean activate(OID activatorOid, AgisItem item, OID targetOid) {
        if (Log.loggingDebug)
            Log.debug("AmmoItemActivateHook.activate: activator=" + activatorOid + " item=" + item + " target=" + targetOid);
        //CombatClient.startAbility(abilityID, activatorOid, targetOid, item, null);
        
        // Check if the Ammo Type suits the weapon
        InventoryInfo iInfo = AgisInventoryPlugin.getInventoryInfo(activatorOid);
        if (iInfo == null) {
        	Log.debug("AMMO: no inventoryInfo found");
        	return false;
        }
        
        EquipMap equipMap = (EquipMap) iInfo.getProperty(AgisInventoryPlugin.EQUIP_MAP_PROP);
        if (equipMap != null) {
        	OID oItemOid = equipMap.get(AgisEquipSlot.PRIMARYWEAPON);
        	if (oItemOid != null) {
        		AgisItem oItemObj = AgisInventoryPlugin.getAgisItem(oItemOid);
        		if (oItemObj != null) {
        			Integer ammoTypeReq = (Integer) oItemObj.getProperty(AgisItem.AMMO_TYPE);
        			if (ammoTypeReq != null && ammoTypeReq != ammoType) {
        				Log.error("AMMO: Ammo of type: " + ammoType + " could not be activated as the weapon requires: " + ammoTypeReq);
        				return false;
        			}
        		}
        	}
        }
        
        // Set combat property to indicate item template ID of ammo equipped
        EnginePlugin.setObjectPropertiesNoResponse(activatorOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_LOADED, item.getTemplateID());
        
        // Set combat ammo damage amount
        int damage = (Integer) item.getProperty("damage");
        EnginePlugin.setObjectPropertiesNoResponse(activatorOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_DAMAGE, damage);
        
        // Set combat ammo effect
        
        Log.debug("AMMO: ammo set: " + ammoType);
        
        AgisInventoryClient.SendInventoryUpdateMessage invUpdateMsg = new AgisInventoryClient.SendInventoryUpdateMessage(activatorOid);
		Engine.getAgent().sendBroadcast(invUpdateMsg);
        
        return true;
    }
    
    public String toString() {
    	return "AmmoItemActivateHook=" + ammoType;
    }

    private static final long serialVersionUID = 1L;
}
