package atavism.agis.core;

import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CombatClient.CooldownMessage;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Cooldown {
    public Cooldown() {
    }

    public Cooldown(String id) {
        setID(id);
    }

    public Cooldown(String id, long duration) {
        setID(id);
        setDuration(duration);
    }
    public String toString() {
        return "[Cooldown: " + getID() + ":" + getDuration() + "]";
    }

    public long getDuration() { return duration; }
    public void setDuration(long dur) { duration = dur; }
    protected long duration = 0;

    public String getID() { return id; }
    public void setID(String id) { this.id = id; }
    protected String id;

    public static void activateCooldown(Cooldown cd, CooldownObject obj) {
        State state = new State(cd.getID(), cd.getDuration(), obj);
        state.start();
        Engine.getAgent().sendBroadcast(new CombatClient.CooldownMessage(state));
    }

    public static void activateCooldowns(Collection<Cooldown> cooldowns, CooldownObject obj, int quickness) {
        CooldownMessage msg = new CooldownMessage(obj.getOid());
        for (Cooldown cd : cooldowns) {
        	double cdDur = cd.getDuration();
        	double length = cdDur / ((double)quickness/100.0);
        	int duration = (int) length;
            State state = new State(cd.getID(), duration, obj);
            state.start();
            msg.addCooldown(state);
        }
        Engine.getAgent().sendBroadcast(msg);
    }

    // returns true if none of the Cooldowns in cdset are active, false otherwise
    public static boolean checkReady(Collection<Cooldown> cdset, CooldownObject obj) {
        for (Cooldown cd : cdset) {
            if (obj.getCooldownState(cd.getID()) != null)
                return false;
        }
        return true;
    }

    public static void resumeCooldowns(CooldownObject obj, Collection<State> cooldowns) {
        for (State state : cooldowns) {
            state.resume();
        }
    }

    public static void abortCooldown(Collection<Cooldown> cooldowns, CooldownObject obj, String cdID) {
	CooldownMessage msg = new CooldownMessage(obj.getOid());
	for (Cooldown cd: cooldowns) {
	    if (cdID.equals(cd.getID())) {
		State state = obj.getCooldownState(cdID);
		if (state != null) {
		    state.cancel();
		}
		msg.addCooldown(state);
	    }
	}
	Engine.getAgent().sendBroadcast(msg);
    }

    public static void abortAllCooldowns(Collection<Cooldown> cooldowns, CooldownObject obj) {
	CooldownMessage msg = new CooldownMessage(obj.getOid());
	for (Cooldown cd: cooldowns) {
	    String cdID = cd.getID();
	    State state = obj.getCooldownState(cdID);
	    if (state != null) {
		state.cancel();
	    }
	    msg.addCooldown(state);
	}
	Engine.getAgent().sendBroadcast(msg);
    }

    public static class State implements Runnable, Serializable {
        public State() {
        }

        public State(String id, long duration, CooldownObject obj) {
            setID(id);
            setDuration(duration);
            setObject(obj);
        }

        public String getID() { return id; }
        public void setID(String id) { this.id = id; }
        protected String id = "UNINIT";

        public CooldownObject getObject() { return obj; }
        public void setObject(CooldownObject obj) { this.obj = obj; }
        protected CooldownObject obj = null;

        public long getDuration() { return duration; }
        public void setDuration(long duration) { this.duration = duration; }
        protected long duration = 0;

        public long getTimeRemaining() { return endTime - System.currentTimeMillis(); }
        public void setTimeRemaining(long time) { endTime = System.currentTimeMillis() + time; }
        public long getEndTime() { return endTime; }
        protected long endTime = 0;

	protected transient ScheduledFuture<?> future = null;
        protected transient boolean running = false;

        public void start() {
            if (running != false) {
                Log.error("Cooldown.State.start: already running");
                return;
            }
            setTimeRemaining(duration);
            obj.addCooldownState(this);
            future = Engine.getExecutor().schedule(this, duration, TimeUnit.MILLISECONDS);
            running = true;
            ExtendedCombatMessages.sendCooldownMessage(this.getObject().getOid(), this.getID(), this.getDuration());
        }

        public void resume() {
            if (Log.loggingDebug)
                Log.debug("Cooldown.State.resume: resuming cooldown " + id);
            if (running != false) {
                Log.error("Cooldown.State.resume: already running");
                return;
            }
            running = true;
            Engine.getExecutor().schedule(this, duration, TimeUnit.MILLISECONDS);
        }

        public void run() {
            if (running != true) {
                Log.error("Cooldown.State.run: not running");
                return;
            }
            try {
                obj.removeCooldownState(this);
            }
            catch (Exception e) {
                Log.exception("Cooldown.State.run", e);
            }
            running = false;
        }

        public void cancel() {
            if (running != true) {
                Log.error("Cooldown.State.cancel: not running");
                return;
            }
            running = false;
            obj.removeCooldownState(this);
            future.cancel(false);
        }
        
        public void timeAdjustment(Long adjustment) {
        	if (running != true) {
                Log.error("Cooldown.State.run: not running");
                return;
            }
        	Long timeLeft = endTime - System.currentTimeMillis();
        	if (adjustment == -1)
        		timeLeft = duration;
        	else
        		timeLeft += adjustment;
        	Engine.getExecutor().remove(this);
        	Engine.getExecutor().schedule(this, duration, TimeUnit.MILLISECONDS);
        	ExtendedCombatMessages.sendCooldownMessage(this.getObject().getOid(), this.getID(), this.getDuration());
        }

        private static final long serialVersionUID = 1L;
    }

    public static interface CooldownObject {
        public void addCooldownState(State state);
        public void removeCooldownState(State state);
        public State getCooldownState(String id);
        public OID getOid();
    }
}
