package atavism.agis.core;

import atavism.agis.objects.AgisEquipSlot;
import atavism.agis.objects.AgisItem;
import atavism.agis.plugins.AgisInventoryPlugin;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.plugins.InventoryPlugin;
import atavism.server.util.Log;

/**
 * an activate hook attached to equippable items, eg: weapons, armor
 * hook will unequip the item in the current slot and equip the item
 * associated with the hook
 */
public class EquipActivateHook implements ActivateHook {
    public EquipActivateHook() {
        super();
    }

    /**
     * returns whether the item was successfully activated
     */
    public boolean activate(OID activatorOid, AgisItem item, OID targetOid) {
    	// get the inventoryplugin
    	AgisInventoryPlugin invPlugin = (AgisInventoryPlugin)Engine.getPlugin(InventoryPlugin.INVENTORY_PLUGIN_NAME);
    	if (Log.loggingDebug)
    		Log.debug("EquipActivateHook: calling invPlugin, item=" + item +
	            ", activatorOid=" + activatorOid + ", targetOid=" + targetOid);
	
    	// is this item already equipped
    	AgisInventoryPlugin.EquipMap equipMap = invPlugin.getEquipMap(activatorOid);
    	AgisEquipSlot slot;
    	invPlugin.getLock().lock();
    	try {
    		slot = equipMap.getSlot(item.getMasterOid());
    	}
    	finally {
    		invPlugin.getLock().unlock();
    	}
    	if (slot == null) {
    		// its not equipped
    		if (Log.loggingDebug)
    			Log.debug("EquipActivateHook: item not equipped: " + item);
    		return invPlugin.equipItem(item, activatorOid, true);
    	}
    	else {
    		// it is equipped, unequip it
    		if (Log.loggingDebug)
    			Log.debug("EquipActivateHook: item IS equipped: " + item);
    		return invPlugin.unequipItem(item, activatorOid, false);
    	}
    }

    // use oids since cheaper to serialize
    protected OID itemOid;
    private static final long serialVersionUID = 1L;
}
