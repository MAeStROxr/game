package atavism.agis.core;

import atavism.agis.objects.AgisItem;
import atavism.agis.plugins.QuestClient;
import atavism.server.engine.OID;

import java.util.LinkedList;

/**
 * an activate hook for items that trigger abilities
 * when the item is activated, the mob uses the ability
 */
public class QuestStartItemActivateHook implements ActivateHook {
    public QuestStartItemActivateHook() {
    	super();
    }

    public QuestStartItemActivateHook(int questID) {
    	super();
    	setQuestID(questID);
    }

    public void setQuestID(int questID) {
        if (questID < 1) {
            throw new RuntimeException("QuestStartItemActivateHook.setQuestID: bad quest");
        }
        this.questID = questID;
    }
    public int getQuestID() {
    	return questID;
    }
    protected int questID;

    public boolean activate(OID activatorOid, AgisItem item, OID targetOid) {
    	LinkedList<Integer> quests = new LinkedList<Integer>();
    	quests.add(questID);
    	QuestClient.offerQuestToPlayer(activatorOid, item.getOid(), quests, true);
        //return QuestClient.startQuestForPlayer(activatorOid, questID);
    	return false;
    }
    
    public String toString() {
    	return "QuestStartItemActivateHook.quest=" + questID;
    }

    private static final long serialVersionUID = 1L;
}
