package atavism.agis.database;

import atavism.agis.objects.Mail;
import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.AgisInventoryPlugin;
import atavism.agis.util.HelperFunctions;
import atavism.server.engine.OID;
import atavism.server.objects.InstanceTemplate;
import atavism.server.util.Log;

import java.io.Serializable;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;


public class AccountDatabase {
	protected static AdminQueries queries = new AdminQueries();


	public AccountDatabase() {

	}

	/**
	 * Adds a newly created character to an account entry. If there is no account entry, a new one will be made.
	 * @param accountID
	 * @param characterOID
	 * @param characterName
	 * @return
	 */
	public boolean characterCreated(OID accountID, String accountName, OID characterOID, String characterName) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: attempting to add character oid: " + characterOID + " to account entry: " + accountID);
			// First add the character
			addAccountCharacter(accountID, characterOID, characterName);

			// Now check if an account entry exists in the admin database - if not - create one
			ps = queries.prepare("SELECT username FROM " + accountTableName + " where id=" + accountID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				if (!rs.next()) {
					createAccount(accountID, accountName, characterOID, characterName);
				}
			}	
		} catch (SQLException e) {

		} finally {
			queries.closeStatement(ps, rs);
		}
		return true;
	}

	/**
	 * Creates a new account entry
	 * @param accountID
	 * @param characterOID
	 * @param characterName
	 * @return
	 */
	public int createAccount(OID accountID, String accountName, OID characterOID, String characterName) {
		long timestamp = System.currentTimeMillis() / timeDivider;
		Log.debug("ACCOUNT: creating entry for accountID: " + accountID + " with character OID:" + characterOID
				+ " and character name: " + characterName + " and current time: " + timestamp);
		
		String columnNames = "id,username,status,last_login,created,coin_current,coin_total,coin_used";
		String values = accountID.toLong() + ",'" + accountName + "'," + 1 + ",FROM_UNIXTIME(" + timestamp + "),FROM_UNIXTIME(" + timestamp + ")," + 0 + "," + 0 + "," + 0;
		String insertString = "INSERT INTO `" + accountTableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		return inserted;
	}

	private boolean addAccountCharacter(OID accountID, OID characterOID, String characterName) {
		Log.debug("ACCOUNT: inserting character:" + characterOID + " in account: " + accountID);
		PreparedStatement stmt = null;
		try {
			String columnNames = "characterId,characterName,accountId";
			stmt = queries.prepare("INSERT INTO " + characterTableName + " (" + columnNames 
					+ ") values (?, ?, ?)");
			stmt.setLong(1, characterOID.toLong());
			stmt.setString(2, characterName);
			stmt.setLong(3, accountID.toLong());
			queries.executeInsert(stmt);
		} catch (SQLException e) {
			return false;
		} finally {
			queries.closeStatement(stmt);
		}

		return true;
	}

	/**
	 * Removes a deleted character from an account entry.
	 * @param accountID
	 * @param characterOID
	 * @param characterName
	 * @return
	 */
	public boolean characterDeleted(OID accountID, OID characterOID, String characterName) {
		String deleteString = "DELETE FROM `" + characterTableName + "` WHERE characterId = " + characterOID.toLong() ;
		queries.executeUpdate(deleteString);
		return true;
	}

	public int getAccountStatus(OID accountID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting account status for account entry: " + accountID);
			ps = queries.prepare("SELECT status FROM " + accountTableName + " where id=" + accountID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int status = rs.getInt("status");
					Log.debug("ACCOUNT: account status for account: " + accountID + " is: " + status);
					return status;
				}
			}	
		} catch (SQLException e) {

		} finally {
			queries.closeStatement(ps, rs);
		}
		return 1;
	}

	public int getNumIslands(OID accountID) {
		//TODO: First get account status - if GM or Admin then we have no limit?
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting island limit for account entry: " + accountID);
			ps = queries.prepare("SELECT islands_available FROM " + accountTableName + " where id=" + accountID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int numAvailable = rs.getInt("islands_available");
					Log.debug("ACCOUNT: account status for account: " + accountID + " is: " + numAvailable);
					return numAvailable;
				}
			}	
		} catch (SQLException e) {

		} finally {
			queries.closeStatement(ps, rs);
		}
		return 0;
	}

	/**
	 * Gets the number of character slots this account has. It will first check to see if the account has
	 * any unclaimed character slot purchases.
	 * @param accountID
	 * @return
	 */
	public int getNumCharacterSlots(OID accountID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting account status for account entry: " + accountID);
			// Now fetch the number of character slots
			ps = queries.prepare("SELECT character_slots FROM " + accountTableName + " where id=" + accountID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int characterSlots = rs.getInt("character_slots");
					return characterSlots;
				}
			}
		} catch (SQLException e) {

		} finally {
			queries.closeStatement(ps, rs);
		}
		return 2;
	}

	public void checkCharacterPurchases(OID characterOID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting purchases for character: " + characterOID.toLong());
			ps = queries.prepare("SELECT * FROM character_purchases where used = 0 and character_id=" + characterOID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int itemID = rs.getInt("itemID");
					AgisInventoryClient.sendPurchaseMail(characterOID, itemID);
					String updateString = "UPDATE character_purchases set used = 1 where id=" + rs.getInt("id");
					queries.executeUpdate(updateString);
				}
			}
		} catch (SQLException e) {

		} finally {
			queries.closeStatement(ps, rs);
		}
		return;
	}

	public int getCharacterCoinAmount(OID accountID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting coin amount for account entry: " + accountID);
			// Now fetch the number of character slots
			ps = queries.prepare("SELECT coin_current FROM " + accountTableName + " where id=" + accountID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					return rs.getInt("coin_current");
				}
			}
		} catch (SQLException e) {

		} finally {
			queries.closeStatement(ps, rs);
		}
		return 0;
	}

	public void alterCharacterCoinAmount(OID accountID, int delta) {
		Log.debug("ACCOUNT: getting purchases for character: " + accountID.toLong());
		String updateString = "UPDATE " + accountTableName + " set coin_current = coin_current + " + delta + " where id=" + accountID.toLong();
		queries.executeUpdate(updateString);
		return;
	}

	public boolean characterLoggedIn(OID accountID) {
		Log.debug("ACCOUNT: updating last login: for account: " + accountID);
		long timestamp = System.currentTimeMillis() / timeDivider;
		String updateString = "UPDATE " + accountTableName + " set last_login = FROM_UNIXTIME(" + timestamp + ") where id=" + accountID.toLong();
		queries.executeUpdate(updateString);
		return true;
	}

	public boolean characterLoggedOut(OID accountID) {
		Log.debug("ACCOUNT: updating last logout: for account: " + accountID);
		long timestamp = System.currentTimeMillis() / timeDivider;
		String updateString = "UPDATE " + accountTableName + " set last_logout = FROM_UNIXTIME(" + timestamp + ") where id=" + accountID.toLong();
		queries.executeUpdate(updateString);
		return true;
	}

	/**
	 * Gets the list of friends a player has so it can be shown to them in their UI
	 * @param characterOID
	 * @return
	 */
	public HashMap<OID, String> getFriends(OID characterOID) {
		HashMap<OID, String> friends = new HashMap<OID, String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting friends of character: " + characterOID.toLong());
			ps = queries.prepare("SELECT * FROM character_friends where character_id=" + characterOID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String friendName = HelperFunctions.readEncodedString(rs.getBytes("friend_name"));
					OID friendOID = OID.fromLong(rs.getLong("friend_id"));
					friends.put(friendOID, friendName);
				}
			}
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(ps, rs);
		}
		return friends;
	}

	/**
	 * Gets the list of players to notify when this person logs in or logs out.
	 * @param characterOID
	 * @return
	 */
	public LinkedList<OID> getFriendsOf(OID characterOID) {
		LinkedList<OID> friends = new LinkedList<OID>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting friends of character: " + characterOID.toLong());
			ps = queries.prepare("SELECT * FROM character_friends where friend_id=" + characterOID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					OID friendOID = OID.fromLong(rs.getLong("character_id"));
					friends.add(friendOID);
				}
			}
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(ps, rs);
		}
		return friends;
	}

	/**
	 * Creates a new database entry listing a player as another players friend.
	 * @param characterOID
	 * @param friendOID
	 * @param friendName
	 */
	public void addFriend(OID characterOID, OID friendOID, String friendName) {
		Log.debug("ACCOUNT: creating friend entry linking character OID:" + characterOID.toLong()
				+ " and friend name: " + friendName);
		String tableName = "character_friends";
		String columnNames = "character_id,friend_id,friend_name";
		String values = "" + characterOID.toLong() + "," + friendOID.toLong() + ",'" + friendName + "'";
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		queries.executeInsert(insertString);
	}

	/**
	 * Adds the specified skin to the account. First checks if the account already has the skin.
	 * @param characterOID
	 * @param friendOID
	 * @param friendName
	 */
	public boolean addSkin(OID accountOID, String skin) {
		Log.debug("ACCOUNT: creating skin entry with account OID:" + accountOID.toLong()
				+ " and skin: " + skin);
		String tableName = "character_skins";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting skins of account: " + accountOID.toLong());
			ps = queries.prepare("SELECT * FROM " + tableName + " where account_id=" + accountOID.toLong() 
					+ " and character_skin='" + skin + "'");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					return false;
				}
			}
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(ps, rs);
		}
		String columnNames = "account_id,character_skin,created,source";
		String values = "" + accountOID.toLong() + ",'" + skin + "', NOW(), 'Merchant'";
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		queries.executeInsert(insertString);
		return true;
	}

	/**
	 * Adds the specified item to the account. First checks if the account already has the item.
	 * @param characterOID
	 * @param friendOID
	 * @param friendName
	 */
	public boolean addItem(OID accountOID, int itemID, int amount) {
		Log.debug("ACCOUNT: creating item entry with account OID:" + accountOID.toLong()
				+ " and item: " + itemID);
		String tableName = "character_items";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting items of account: " + accountOID.toLong());
			ps = queries.prepare("SELECT * FROM " + tableName + " where account_id=" + accountOID.toLong() 
					+ " and itemID=" + itemID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					return false;
				}
			}
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(ps, rs);
		}
		String columnNames = "account_id,itemID,amount";
		String values = "" + accountOID.toLong() + "," + itemID + ", " + amount;
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		queries.executeInsert(insertString);
		return true;
	}

	public void alterItemAmount(OID accountID, int itemID, int delta) {
		Log.debug("ACCOUNT: altering item amount for account " + accountID.toLong());
		String tableName = "character_items";
		String updateString = "UPDATE " + tableName + " set amount = amount + " + delta + " where account_id=" + accountID.toLong()
				+ " and itemID = " + itemID;
		queries.executeUpdate(updateString);
		return;
	}

	/**
	 * Removes the specified items from the account.
	 * @param characterOID
	 * @param friendOID
	 * @param friendName
	 */
	public boolean removeItem(OID accountOID, int itemID) {
		Log.debug("ACCOUNT: creating weapon entry with account OID:" + accountOID.toLong()
				+ " and skin: " + itemID);
		String tableName = "character_items";
		int entryID = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting items of account: " + accountOID.toLong());
			ps = queries.prepare("SELECT id FROM " + tableName + " where account_id=" + accountOID.toLong() 
					+ " and itemID=" + itemID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					entryID = rs.getInt("id");
				}
			}
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(ps, rs);
		}
		if (entryID == -1)
			return false;
		String deleteString = "DELETE FROM `" + tableName + "` where id = " + entryID;
		queries.executeUpdate(deleteString);
		return true;
	}

	/**
	 * Gets the items that the passed in account has access to.
	 * @param accountOID
	 */
	public HashMap<Integer,Integer> getItems(OID accountOID) {
		HashMap<Integer,Integer> items = new HashMap<Integer,Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("ACCOUNT: getting items from account: " + accountOID.toLong());
			ps = queries.prepare("SELECT * FROM character_items where account_id=" + accountOID.toLong());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					items.put(rs.getInt("itemID"), rs.getInt("amount"));
				}
			}
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(ps, rs);
		}
		return items;
	}

	/**
	 * Instance Template related database queries
	 */
	public HashMap<Integer, InstanceTemplate> loadInstanceTemplateData() {
		HashMap<Integer, InstanceTemplate> list = new HashMap<Integer, InstanceTemplate>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM instance_template");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					InstanceTemplate island = new InstanceTemplate();
					island.setID(rs.getInt("id"));
					island.setCategory(rs.getInt("category"));
					island.setName(HelperFunctions.readEncodedString(rs.getBytes("island_name")));
					OID administrator = OID.fromLong(rs.getLong("administrator"));
					island.setAdministrator(administrator);
					island.setIsPublic(rs.getBoolean("public"));
					island.setPassword(HelperFunctions.readEncodedString(rs.getBytes("password")));
					island.setDevelopers(loadIslandDevelopers(island.getID()));
					island.setIslandType(rs.getInt("islandType"));
					// Only allow createOnStartup to be read for world type instances
					if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_WORLD) {
						island.setCreateOnStartup(rs.getBoolean("createOnStartup"));
					} else {
						island.setCreateOnStartup(false);
					}
					island.setGlobalWaterHeight(rs.getFloat("globalWaterHeight"));
					island.setStyle(HelperFunctions.readEncodedString(rs.getBytes("style")));
					island.setDescription(HelperFunctions.readEncodedString(rs.getBytes("description")));
					island.setRating(rs.getInt("rating"));
					island.setSize(rs.getInt("size"));
					island.setPopulationLimit(rs.getInt("populationLimit"));
					Date subscriptionExpiration = rs.getDate("subscription");
					Log.warn("Island " + island.getName() + " has subscription Expiration: " + subscriptionExpiration);
					island.setContentPacks(loadIslandContentPacks(island.getID()));
					island.setPortals(loadIslandPortals(island.getID()));
					list.put(island.getID(), island);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public LinkedList<String> loadIslandContentPacks(int islandID) {
		LinkedList<String> contentPacks = new LinkedList<String>();

		return contentPacks;
	}

	public LinkedList<OID> loadIslandDevelopers(int islandID) {
		LinkedList<OID> list = new LinkedList<OID>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM island_developers where island = " + islandID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					OID developer = OID.fromLong(rs.getLong("developer"));
					list.add(developer);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public ArrayList<String> getIslandName(String islandName) {
		ArrayList<String> list = new ArrayList<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT island_name FROM instance_template where island_name = '" + islandName + "'");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					list.add(HelperFunctions.readEncodedString(rs.getBytes("island_name")));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public OID getIslandAdministrator(int islandID) {
		OID administrator = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT administrator FROM instance_template where id = " + islandID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					administrator = OID.fromLong(rs.getLong("administrator"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return administrator;
	}

	public HashMap<String, HashMap<String, Float>> loadIslandPortals(int islandID) {
		HashMap<String, HashMap<String, Float>> list = new HashMap<String, HashMap<String, Float>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM island_portals where island = " + islandID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					HashMap<String, Float> portalProps = new HashMap<String, Float>();
					portalProps.put("id", (float)rs.getInt("id"));
					portalProps.put("portalType", (float)rs.getInt("portalType"));
					portalProps.put("faction", (float)rs.getInt("faction"));
					portalProps.put("displayID", (float)rs.getInt("displayID"));
					portalProps.put("locX", rs.getFloat("locX"));
					portalProps.put("locY", rs.getFloat("locY"));
					portalProps.put("locZ", rs.getFloat("locZ"));
					portalProps.put("orientX", rs.getFloat("orientX"));
					portalProps.put("orientY", rs.getFloat("orientY"));
					portalProps.put("orientZ", rs.getFloat("orientZ"));
					portalProps.put("orientW", rs.getFloat("orientW"));
					list.put(HelperFunctions.readEncodedString(rs.getBytes("name")), portalProps);
					Log.debug("PORTAL: loaded portal " + rs.getString("name") + " in island: " + islandID);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public void writeIslandData(InstanceTemplate island, String template) {
		Log.debug("Writing island data to database");
		String tableName = "instance_template";
		String columnNames = "island_name,template,administrator,status,public,password,islandType,createOnStartup,style,recommendedLevel,description,size";
		String values = "'" + island.getName() + "','" + template + "'," + island.getAdministrator().toLong() + ",'Active'," + island.getIsPublic() 
				+ ",'',0," + island.getCreateOnStartup() + ",'',1,'',1";
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote island data to database with inserted: " + inserted);
		return;
	}

	public HashMap<String, HashMap<String, Float>> writePortalData(int islandID, HashMap<String, HashMap<String, Float>> portals) {
		Log.debug("Writing island portal data to database");
		HashMap<String, HashMap<String, Float>> newPortals = new HashMap<String, HashMap<String, Float>>();
		String tableName = "island_portals";
		String columnNames = "island,portalType,faction,locX,locY,locZ,orientX,orientY,orientZ,orientW,displayID";
		for (String name : portals.keySet()) {
			HashMap<String, Float> portalProps = portals.get(name);
			String values =  islandID + "," + 1 + "," + portalProps.get("faction") + "," + portalProps.get("locX")
					+ "," + portalProps.get("locY") + "," + portalProps.get("locZ") + "," + portalProps.get("orientX") + "," 
					+ portalProps.get("orientY") + "," + portalProps.get("orientZ")+ "," + portalProps.get("orientW")
					+ "," + portalProps.get("displayID");
			String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
			int inserted = queries.executeInsert(insertString);
			Log.debug("Wrote island portal data to database with inserted: " + inserted);
			if (inserted != -1)
				newPortals.put(name, portalProps);
		}
		return newPortals;
	}

	public void editPortalData(String portalName, HashMap<String, Float> portalProps) {
		Log.debug("Writing portal data to database");
		String tableName = "island_portals";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set portalType=?, faction=?, locX=?, " 
					+ "locY=?, locZ=?, orientX=?, orientY=?, orientZ=?, orientW=?, name=?,  where id=?");
			stmt.setInt(1, portalProps.get("portalType").intValue());
			stmt.setInt(2, portalProps.get("faction").intValue());
			stmt.setFloat(3, portalProps.get("locX"));
			stmt.setFloat(4, portalProps.get("locY"));
			stmt.setFloat(5, portalProps.get("locZ"));
			stmt.setFloat(6, portalProps.get("orientX"));
			stmt.setFloat(7, portalProps.get("orientY"));
			stmt.setFloat(8, portalProps.get("orientZ"));
			stmt.setFloat(9, portalProps.get("orientW"));
			stmt.setInt(10, portalProps.get("displayID").intValue());
			stmt.setString(11, portalName);
			stmt.setInt(12, portalProps.get("id").intValue());
			Log.debug("ISLANDDB: updating island portal with statement: " + stmt.toString());
			queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return;
		} finally {
			queries.closeStatement(stmt);
		}
	}

	/***
	 * Island Template Database Code
	 */

	public LinkedList<HashMap<String, Serializable>> loadTemplateIslands() {
		LinkedList<HashMap<String, Serializable>> list = new LinkedList<HashMap<String, Serializable>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM " + instanceTemplateTableName);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					HashMap<String, Serializable> templateProps = new HashMap<String, Serializable>();
					templateProps.put("templateID", rs.getInt("id"));
					templateProps.put("name", HelperFunctions.readEncodedString(rs.getBytes("name")));
					templateProps.put("size", rs.getInt("size"));
					list.add(templateProps);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public HashMap<String, HashMap<String, Float>> loadIslandTemplatePortals(int templateID) {
		HashMap<String, HashMap<String, Float>> list = new HashMap<String, HashMap<String, Float>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM templatePortals where templateID = " + templateID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					HashMap<String, Float> portalProps = new HashMap<String, Float>();
					portalProps.put("id", (float)rs.getInt("id"));
					portalProps.put("portalType", (float)rs.getInt("portalType"));
					portalProps.put("faction", (float)rs.getInt("faction"));
					portalProps.put("displayID", (float)rs.getInt("displayID"));
					portalProps.put("locX", rs.getFloat("locX"));
					portalProps.put("locY", rs.getFloat("locY"));
					portalProps.put("locZ", rs.getFloat("locZ"));
					portalProps.put("orientX", rs.getFloat("orientX"));
					portalProps.put("orientY", rs.getFloat("orientY"));
					portalProps.put("orientZ", rs.getFloat("orientZ"));
					portalProps.put("orientW", rs.getFloat("orientW"));
					list.put(HelperFunctions.readEncodedString(rs.getBytes("name")), portalProps);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}


	/**
	 * Inserts a new entry into the character_mail table.
	 * @param iMailToSend : The email to be saved in the database.
	 * @return true if success
	 *  
	 */
	public boolean addNewMail(Mail iMailToSend) {
		Log.debug("MAILING: attempting to add mail oid: " + iMailToSend.getID());
		PreparedStatement stmt = null;
		// Insert Mail into the SQL Database
		try {
			stmt = queries.prepare("INSERT INTO " + mailTableName + " (mailArchive, recipientId, recipientName, senderId, "
					+ "senderName, mailRead, mailSubject, mailMessage, currencyType, currencyAmount, currencyTaken, CoD, expiry, "
					+ "mailAttachmentItemId1Taken, mailAttachmentItemId1, mailAttachmentItemId2Taken, mailAttachmentItemId2, "
					+ "mailAttachmentItemId3Taken, mailAttachmentItemId3, mailAttachmentItemId4Taken, mailAttachmentItemId4, "
					+ "mailAttachmentItemId5Taken, mailAttachmentItemId5, mailAttachmentItemId6Taken, mailAttachmentItemId6, "
					+ "mailAttachmentItemId7Taken, mailAttachmentItemId7, mailAttachmentItemId8Taken, mailAttachmentItemId8, "
					+ "mailAttachmentItemId9Taken, mailAttachmentItemId9, mailAttachmentItemId10Taken, mailAttachmentItemId10"
					+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setBoolean(1 ,iMailToSend.getMailArchive());
			stmt.setLong(2, iMailToSend.getRecipientOID().toLong());
			stmt.setString(3, iMailToSend.getRecipientName());
			stmt.setLong(4, iMailToSend.getSenderOID().toLong());
			stmt.setString(5, iMailToSend.getSenderName());
			stmt.setBoolean(6, iMailToSend.getMailRead());
			stmt.setString(7, iMailToSend.getSubject());
			stmt.setString(8, iMailToSend.getMessage());
			stmt.setInt(9, iMailToSend.getCurrencyType());
			stmt.setInt(10, iMailToSend.getCurrencyAmount());
			stmt.setBoolean(11, false);
			stmt.setBoolean(12, iMailToSend.getCoD());
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
			Calendar cal = Calendar.getInstance();
	        cal.setTime(new java.util.Date());
			if (iMailToSend.getCoD()) {
				cal.add(Calendar.DATE, AgisInventoryPlugin.MAIL_COD_LIFE_DAYS);
			} else {
				cal.add(Calendar.DATE, AgisInventoryPlugin.MAIL_LIFE_DAYS);
			}
			String strDate = sdfDate.format(cal.getTime());
			stmt.setTimestamp (13, Timestamp.valueOf(strDate));
			
			int startingNum = 14;
			for (int i = 0; i < AgisInventoryPlugin.MAIL_ATTACHMENT_COUNT; i++) {
				stmt.setBoolean(startingNum + (i*2), false);
				if (iMailToSend.getItems() != null && iMailToSend.getItems().size() > i && iMailToSend.getItems().get(i) != null) {
					stmt.setLong(startingNum + (i*2) + 1, iMailToSend.getItems().get(i).toLong());
				} else {
					stmt.setLong(startingNum + (i*2) + 1, -1);
				}
			}
			
			int id = queries.executeInsert(stmt);
			iMailToSend.setID(id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			queries.closeStatement(stmt);
		}
		return true;
	}

	/**
	 * Read in all Mail Item for a given CharacterOID
	 * @param characterOID
	 * @return
	 */
	public ArrayList<Mail> retrieveMail(OID characterOID) {
		ResultSet rs = null;
		try {
			ArrayList<Mail> wMails = new ArrayList<Mail>();
			Log.debug("MAIL: retrieving all mail by char oid: " + characterOID);
			
			// First check if any mail the user has sent has expired
			String selectString  = "SELECT * FROM " + mailTableName + " WHERE senderId=" + characterOID.toLong() 
					+ " AND mailArchive=0 AND expiry < now()";
			rs = queries.executeSelect(selectString);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("MAIL: found sent expired mail");
					returnMail(rs.getInt("mailId"), OID.fromLong(rs.getLong("senderId")), HelperFunctions.readEncodedString(rs.getBytes("senderName")), 
							OID.fromLong(rs.getLong("recipientId")), HelperFunctions.readEncodedString(rs.getBytes("recipientName")), 
							HelperFunctions.readEncodedString(rs.getBytes("mailSubject")), rs.getBoolean("CoD"));
				}
			}
			
			// Now check if the user has any expired mail in their mailbox
			selectString  = "SELECT * FROM " + mailTableName + " WHERE recipientId=" + characterOID.toLong() 
					+ " AND mailArchive=0 AND expiry < now()";
			rs = queries.executeSelect(selectString);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("MAIL: found expired mail in users inbox");
					returnMail(rs.getInt("mailId"), OID.fromLong(rs.getLong("senderId")), HelperFunctions.readEncodedString(rs.getBytes("senderName")), 
							OID.fromLong(rs.getLong("recipientId")), HelperFunctions.readEncodedString(rs.getBytes("recipientName")), 
							HelperFunctions.readEncodedString(rs.getBytes("mailSubject")), rs.getBoolean("CoD"));
				}
			}

			Log.debug("MAIL: completed expired mail check");
			
			// Select Mail into the SQL Database using character OID
			selectString = "SELECT mailId FROM " + mailTableName + " WHERE recipientId=" + characterOID.toLong() + " AND mailArchive=0";
			rs = queries.executeSelect(selectString);
			if (rs != null) {
				while (rs.next()) {
					wMails.add(retrieveSingleMail(rs.getInt("mailId")));
				}
			}
			
			return wMails;
		} catch (SQLException e) {
			Log.error("MAIL: got error retreiving mail: " + e.toString());
			return null;
		}
		
	}
	
	/**
	 * Retrieve mail form DB using mailOID
	 * @param mailOID
	 * @return
	 */
	public Mail retrieveSingleMail(int mailOID) {
		Log.debug("MAIL: Retrieving single email by oid: " + mailOID);
		// SelectMail from SQL Database using mailOID
		String selectString = "SELECT * FROM " + mailTableName + " WHERE mailId=" + mailOID;
		ResultSet rs = null;
		rs = queries.executeSelect(selectString); //Re
		if (rs != null) {
			try {
				if(rs.first())
				{
					Log.debug("MAIL: Found mail");
					Mail wMail = new Mail();
					wMail.setID(mailOID);
					wMail.setRecipientOID(getOIDFromLongInResultSet(rs, "recipientId"));
					wMail.setRecipientName(HelperFunctions.readEncodedString(rs.getBytes("recipientName")));
					wMail.setSenderOID(getOIDFromLongInResultSet(rs, "senderId"));
					wMail.setSenderName(HelperFunctions.readEncodedString(rs.getBytes("senderName")));
					wMail.setMailRead(rs.getBoolean("mailRead"));
					wMail.setSubject(HelperFunctions.readEncodedString(rs.getBytes("mailSubject")));
					wMail.setMessage(HelperFunctions.readEncodedString(rs.getBytes("mailMessage")));
					wMail.setCurrencyType(rs.getInt("currencyType"));
					if (!rs.getBoolean("currencyTaken")) {
						wMail.setCurrencyAmount(rs.getInt("currencyAmount"));
					} else {
						wMail.setCurrencyAmount(0);
					}
					wMail.setCoD(rs.getBoolean("CoD"));
					Log.debug("MAIL: Retrieving attachment data");
					// Add email Attachments
					ArrayList<OID> attachments = new ArrayList<OID>();
					for (int i = 1; i <= AgisInventoryPlugin.MAIL_ATTACHMENT_COUNT; i++) {
						Long itemOid = rs.getLong("mailAttachmentItemId" + i);
						if (itemOid != null && itemOid > 0 && !rs.getBoolean("mailAttachmentItemId" + i + "Taken")) {
							attachments.add(OID.fromLong(itemOid));
						} else {
							attachments.add(null);
						}
					}
					wMail.setItems(attachments);
					Log.debug("MAIL: Added mail: " + mailOID);
					return wMail;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null; 
	}
	
	/**
	 * Returns the mail back to the sender
	 * @param mailOID
	 * @return
	 */
	public boolean returnMail(int mailID, OID senderOid, String senderName, OID recipientOid, String recipientName, 
			String subject, boolean removeCurrency) {
		Log.debug("MAILING: Deleting mail by oid: " + mailID);
		// Switch around the Sender and Recipient Mail in the SQL Database using mailOID
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + mailTableName + " set recipientId=?, recipientName=?, senderId=?, "
				+ "senderName=?, mailSubject=?, expiry=?, currencyTaken=?, CoD=? where mailId=?");
			stmt.setLong(1, senderOid.toLong());
			stmt.setString(2, senderName);
			stmt.setLong(3, recipientOid.toLong());
			stmt.setString(4, recipientName);
			if (!subject.startsWith("Returned")) {
				stmt.setString(5, "Returned: " + subject);
			} else {
				stmt.setString(5, subject);
			}
			
			Calendar cal = Calendar.getInstance();
	        cal.setTime(new java.util.Date());
			cal.add(Calendar.DATE, AgisInventoryPlugin.MAIL_LIFE_DAYS);
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
			String strDate = sdfDate.format(cal.getTime());
			stmt.setTimestamp (6, Timestamp.valueOf(strDate));
			stmt.setBoolean(7, removeCurrency);
			stmt.setBoolean(8, false);
			stmt.setInt(9, mailID);
			Log.debug("MAILING: returning mail stmt=" + stmt.toString());
			queries.executeUpdate(stmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			queries.closeStatement(stmt);
		}
		return true;
	}

	/**
	 * Set the mail to deleted (archived) using mailOID
	 * @param mailOID
	 * @return
	 */
	public boolean deleteMail(int mailOID) {
		Log.debug("MAILING: Deleting mail by oid: " + mailOID);
		// Delete Mail from SQL Database using mailOID (modified to ArchiveMail)
		String updateString = "UPDATE " + mailTableName + " SET mailArchive=1 WHERE mailId=" + mailOID;
		queries.executeUpdate(updateString);
		return true;
	}

	/**
	 * Set mail status to read using mailOID
	 * @param mailOID
	 * @return
	 */
	public boolean readMail(int mailOID) {
		Log.debug("MAILING: putting status read to: " + mailOID);

		// Putting read Mail from SQL Database using mailOID
		String updateString = "UPDATE " + mailTableName + " SET mailRead=1 WHERE mailId=" + mailOID;
		queries.executeUpdate(updateString);
		return true;
	}
	
	/**
	 * Set mail attachment to taken using mailOID and the item position
	 * @param mailOID
	 * @param itemPos
	 * @param CoD: if true, CoD is set to false and currencyTaken is set to true
	 * @return
	 */
	public boolean takeMailItem(int mailOID, int itemPos, boolean CoD) {
		Log.debug("MAILING: taking mail item: " + mailOID);

		// Putting read Mail from SQL Database using mailOID
		if (CoD) {
			String updateString = "UPDATE " + mailTableName + " SET mailAttachmentItemId" + (itemPos+1) + "Taken=1, CoD=0, currencyTaken=1 WHERE mailId=" + mailOID;
			queries.executeUpdate(updateString);
		} else {
			String updateString = "UPDATE " + mailTableName + " SET mailAttachmentItemId" + (itemPos+1) + "Taken=1 WHERE mailId=" + mailOID;
			queries.executeUpdate(updateString);
		}
		return true;
	}
	
	/**
	 * Set mail attachment to taken using mailOID and the item position
	 * @param mailOID
	 * @return
	 */
	public boolean takeMailCurrency(int mailOID) {
		Log.debug("MAILING: setting currency to 0 for mail: " + mailOID);

		// Putting read Mail from SQL Database using mailOID
		String updateString = "UPDATE " + mailTableName + " SET currencyTaken=1 WHERE mailId=" + mailOID;
		queries.executeUpdate(updateString);
		return true;
	}
	
	/**
	 * 
	 * @param rs
	 * @param wColoumnLabel
	 * @return
	 * @throws SQLException
	 */
	public OID getOIDFromLongInResultSet(ResultSet rs, String wColoumnLabel) throws SQLException
	{
		return OID.fromLong(rs.getLong(wColoumnLabel));
	}

	/**
	 * Server stats
	 */
	public void updateServerStat(String event) {
		Log.debug("SERVER: updating server event: " + event);
		long timestamp = System.currentTimeMillis() / timeDivider;
		if (event.equals("player_login")) {
			String updateString = "UPDATE " + statsTableName + " set players_online = players_online + 1"  
					+ ", last_login = FROM_UNIXTIME(" + timestamp + "), logins_since_restart = logins_since_restart + 1";
			queries.executeUpdate(updateString);
		} else if (event.equals("player_logout")) {
			String updateString = "UPDATE " + statsTableName + " set players_online = players_online - 1";
			queries.executeUpdate(updateString);
		} else if (event.equals("restart")) {
			String updateString = "UPDATE " + statsTableName + " set last_restart = FROM_UNIXTIME(" + timestamp + ")"
					+ ", players_online = 0, logins_since_restart = 0";
			queries.executeUpdate(updateString);
		}

		return;
	}
	
	private long timeDivider = 1L; //1000L

	private static final String accountTableName = "account";
	private static final String characterTableName = "account_character";
	private static final String statsTableName = "server_stats";
	private static final String instanceTemplateTableName = "instance_template";
	private static final String mailTableName = "character_mail";

}

