package atavism.agis.database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class ArenaQueries {
	static Connection con = initConnection();
	private static int PORT;
	private static String USERNAME;
	private static String PASSWORD;
	private static String IPADDRESS;
	private static String DATABASENAME;
	
	private static boolean loadStrings() {
		Properties p = new Properties();
		FileReader r;
		try {
			r = new FileReader("arena.ini");
		} catch (FileNotFoundException e) {
			System.out.println("ANDREW: File arena.ini could not be found, "
					+ "using default connection strings");
			PORT = 3306;
			USERNAME = "root";
			PASSWORD = "test";
			DATABASENAME = "admin";
			IPADDRESS =  "127.0.0.1";
			return true;
		}
		BufferedReader b = new BufferedReader(r);
		try {
			p.load(b);
		} catch (IOException e) {
			System.out.println("ANDREW: File arena.ini could not be read, "
					+ "using default connection strings");
			PORT = 3306;
			USERNAME = "root";
			PASSWORD = "test";
			DATABASENAME = "admin";
			IPADDRESS = "127.0.0.1";
			return true;
		}
		try {
			PORT = Integer.parseInt(p.getProperty("port"));
		} catch (NumberFormatException e) {
			PORT = 3306;
		}
		IPADDRESS = p.getProperty("ip");
		USERNAME = p.getProperty("username");
		PASSWORD = p.getProperty("password");
		DATABASENAME = p.getProperty("database");
		System.out.println("ANDREW: File arena.ini should have been successfully read");
		return true;
	}
	
	public static String getConnectionString() {
		return "jdbc:mysql://" + IPADDRESS + ":" + PORT + "/" + DATABASENAME;
	}

	public static Connection initConnection() {
		try {
			if (loadStrings()) { 
			    Class.forName("com.mysql.jdbc.Driver").newInstance();
			    //System.out.println("ANDREW: Connecting to db with info: " + getConnectionString());
			    return DriverManager.getConnection(getConnectionString(), USERNAME, PASSWORD);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void finalize() throws Throwable {
		con.close();
	}

	public void close() {
		try {
			con.commit();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public PreparedStatement prepare(String sql) {
		checkConnection();
		try {
			return con.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ResultSet executeSelect(String query) {
		checkConnection();
		try {
			return con.createStatement().executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ResultSet executeSelect(PreparedStatement ps) {
		checkConnection();
		try {
			return ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean executeInsert(String query) {
		checkConnection();
		try {
			return con.createStatement().execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public int executeInsert(PreparedStatement ps) {
		checkConnection();
		try {
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public int executeUpdate(String query) {
		checkConnection();
		try {
			return con.createStatement().executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public int executeUpdate(PreparedStatement ps) {
		checkConnection();
		try {
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	private void checkConnection() {
		try {
			if (con.isClosed()) {
				con = initConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
