package atavism.agis.database;

import atavism.agis.util.HelperFunctions;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.util.Log;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Manages Server and Account information retrieval and storage for the Authentication Database. The main purpose
 * is to let the Authentication server know this server is still running and how many players are currently connected.
 * @author Andrew Harrison
 *
 */
public class AuthDatabase {
	protected static AuthQueries queries = new AuthQueries();

	public AuthDatabase() {

	}

	/**
	 * Retrieves the list of servers from the Authentication Database and verifies they are still online.
	 * @return
	 */
	public ArrayList<HashMap<String, Serializable>> getServers() {
		ArrayList<HashMap<String, Serializable>> serverList = new ArrayList<HashMap<String, Serializable>>();
		try {
			Log.debug("AUTH: getting server statuses");
			String selectString = "SELECT * FROM " + worldServerTable;
			ResultSet rs = queries.executeSelect(selectString);
			if (rs != null) {
				while (rs.next()) {
					HashMap<String, Serializable> server = new HashMap<String, Serializable>();
					server.put("id", rs.getInt("world_id"));
					server.put("name", HelperFunctions.readEncodedString(rs.getBytes("world_name")));
					server.put("hostname", HelperFunctions.readEncodedString(rs.getBytes("server_name")));
					server.put("port", rs.getInt("server_port"));
					
					String status = rs.getString("status");
					if (status == null || status.equals("") || status.equals("Online")) {
						// If the server hasn't recieved an update in the last 2 minutes, mark it as offline
						Timestamp lastUpdate = rs.getTimestamp("last_update");
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.SECOND, -20);
						Log.debug("AUTH: comparing serverUpdate: " + lastUpdate.toString() + " against 20 seconds ago: " + cal.getTime().toString());
						if (lastUpdate.after(cal.getTime())) {
							server.put("status", "Online");
						} else {
							server.put("status", "Offline");
						}
					} else {
						server.put("status", status);
					}
			    	
			    	serverList.add(server);
				}
			}	
		} catch (SQLException e) {

		}
		return serverList;
	}
	
	/**
     * Reads in the servers the player has characters on.
     * @param accountID
     * @return
     */
    public HashMap<Integer, Integer> getWorldWithPlayersCharacters(int accountID) {
    	HashMap<Integer, Integer> worldIDs = new HashMap<Integer, Integer>();
		try {
			Log.debug("AUTH: getting server statuses");
            String selectString = "SELECT world_server_id, count(*) FROM " + accountCharacterTable + " where account_id=" + accountID 
            		+ " GROUP BY world_server_id";
            ResultSet rs = queries.executeSelect(selectString);
			if (rs != null) {
				while (rs.next()) {
					worldIDs.put(rs.getInt(1), rs.getInt(2));
				}
			}	
		} catch (SQLException e) {

		}
		return worldIDs;
	}
	
	/**
	 * Reads in the id for this server from the world table.
	 */
	public void loadServerID() {
		// Read in the servername from the world.properties file
		String serverName = Engine.getProperty("atavism.servername");
		try {
			String selectString = "SELECT world_id FROM " + worldServerTable + " where world_name='" + serverName + "'";
			ResultSet rs = queries.executeSelect(selectString);
			if (rs != null) {
				while (rs.next()) {
					server_id = rs.getInt("world_id");
					break;
				}
			}
		} catch (SQLException e) {
			
		}
	}

	/**
	 * Updates the entry in the world table for this server with the current population to help the 
	 * Authentication server manage player loads.
	 * @param population
	 */
	public void sendServerStatusUpdate(int population) {
		String serverName = Engine.getProperty("atavism.servername");
		// See if row already exists for this server
		if (server_id == -1) {
			loadServerID();
			
			if (server_id == -1) {
				// Insert a row into the database
				try {
					String serverAddress = Engine.getProperty("atavism.login.bindaddress");
					int port = Integer.parseInt(Engine.getProperty("atavism.login.bindport"));
					String columnNames = "world_name,server_name,server_port,population";
					PreparedStatement stmt = queries.prepare("INSERT INTO " + worldServerTable + " (" + columnNames 
						+ ") values (?, ?, ?, ?)");
					stmt.setString(1, serverName);
					stmt.setString(2, serverAddress);
					stmt.setInt(3, port);
					stmt.setInt(4, population);
					server_id = queries.executeInsert(stmt);
				} catch (SQLException e) {
				}
			}
		}
		
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
	        cal.setTime(new java.util.Date());
			String strDate = sdfDate.format(cal.getTime());
			String serverAddress = Engine.getProperty("atavism.login.bindaddress");
			int port = Integer.parseInt(Engine.getProperty("atavism.login.bindport"));
			
			PreparedStatement stmt = queries.prepare("UPDATE " + worldServerTable + " set server_name=?, server_port=?, population=?, status=?, last_update=? where world_id=?");
			stmt.setString(1, serverAddress);
			stmt.setInt(2, port);
			stmt.setInt(3, population);
			stmt.setString(4, "Online");
			stmt.setTimestamp (5, Timestamp.valueOf(strDate));
			stmt.setInt(6, server_id);
			Log.debug("AUTH: updating world_server with statement: " + stmt.toString());
			queries.executeUpdate(stmt);
		} catch (SQLException e) {
		}
	}
	
	/**
	 * Removes a deleted character from an account entry.
	 * @param accountID
	 * @param characterOID
	 * @param characterName
	 * @return
	 */
	public boolean updateAccountCurrentWorld(OID accountID) {
		if (server_id == -1) {
			loadServerID();
		}
		
		try {
			PreparedStatement stmt = queries.prepare("UPDATE " + accountTable + " set current_world_id=? where id=?");
			stmt.setInt(1, server_id);
			stmt.setInt(2, (int)accountID.toLong());
			Log.debug("AUTH: updating account with statement: " + stmt.toString());
			queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Adds an entry into the account_character table in the Auth Database.
	 * @param accountID
	 * @param characterOID
	 * @return
	 */
	public boolean addAccountCharacter(OID accountID, OID characterOID) {
		if (server_id == -1) {
			loadServerID();
		}
		
		Log.debug("AUTH: inserting character:" + characterOID + " in account: " + accountID);
		try {
			String columnNames = "account_id,character_id,world_server_id";
			PreparedStatement stmt = queries.prepare("INSERT INTO " + accountCharacterTable + " (" + columnNames 
					+ ") values (?, ?, ?)");
			stmt.setInt(1, (int)accountID.toLong());
			stmt.setLong(2, characterOID.toLong());
			stmt.setInt(3, server_id);
			
			queries.executeInsert(stmt);
		} catch (SQLException e) {
			return false;
		}

		return true;
	}

	/**
	 * Removes a deleted character from an account entry.
	 * @param accountID
	 * @param characterOID
	 * @param characterName
	 * @return
	 */
	public boolean characterDeleted(OID accountID, OID characterOID) {
		try {
			PreparedStatement stmt = queries.prepare("UPDATE " + accountCharacterTable + " set status=? where account_id=? and character_id=?");
			stmt.setInt(1, 0);
			stmt.setInt(2, (int)accountID.toLong());
			stmt.setLong(3, characterOID.toLong());
			Log.debug("AUTH: updating account_character with statement: " + stmt.toString());
			queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return false;
		}
		return true;
	}
	
	private int server_id = -1;
	private static final String worldServerTable = "world";
	private static final String accountTable = "account";
	private static final String accountCharacterTable = "account_character";

}

