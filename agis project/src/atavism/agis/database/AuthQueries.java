package atavism.agis.database;

import atavism.server.engine.Engine;
import atavism.server.util.AORuntimeException;
import atavism.server.util.Log;

import java.sql.*;

public class AuthQueries {
	static Connection con = initConnection();
	private static int PORT;
	private static String USERNAME;
	private static String PASSWORD;
	private static String IPADDRESS;
	private static String DATABASENAME;
	
	//boolean keepAliveStarted = false;
	
	public AuthQueries() {
		// start up the keepalive class
		//if (!keepAliveStarted) {
			Log.debug("AuthDatabase: starting keepalive");
		
			Thread keepAliveThread = new Thread(new KeepAlive(), "DBKeepalive");
			keepAliveThread.start();
			//keepAliveStarted = true;
		//}
	}
	
	private static boolean loadStrings() {
		boolean differentSettings = Boolean.parseBoolean(Engine.getProperty("atavism.auth.db_differentsettings"));
		
		DATABASENAME = Engine.getProperty("atavism.auth.db_name");
		if (differentSettings) {
			USERNAME = Engine.getProperty("atavism.auth.db_user");
			PASSWORD = Engine.getProperty("atavism.auth.db_password");
			IPADDRESS =  Engine.getProperty("atavism.auth.db_hostname");
		} else {
			USERNAME = Engine.getProperty("atavism.db_user");
			PASSWORD = Engine.getProperty("atavism.db_password");
			IPADDRESS =  Engine.getProperty("atavism.db_hostname");
		}
		
		if (IPADDRESS.contains(":")) {
			String[] ipdetails = IPADDRESS.split(":");
			IPADDRESS = ipdetails[0];
			PORT = Integer.parseInt(ipdetails[1]);
		} else {
			PORT = 3306;
		}
		
		return true;
	}
	
	public static String getConnectionString() {
		return "jdbc:mysql://" + IPADDRESS + ":" + PORT + "/" + DATABASENAME + "?useUnicode=yes&characterEncoding=UTF-8";
	}

	public static Connection initConnection() {
		try {
			if (loadStrings()) { 
			    Class.forName("com.mysql.jdbc.Driver").newInstance();
			    //System.out.println("ANDREW: Connecting to db with info: " + getConnectionString());
			    return DriverManager.getConnection(getConnectionString(), USERNAME, PASSWORD);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void finalize() throws Throwable {
		con.close();
	}

	public void close() {
		try {
			con.commit();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public PreparedStatement prepare(String sql) {
		checkConnection();
		try {
			return con.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ResultSet executeSelect(String query) {
		checkConnection();
		try {
			return con.createStatement().executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ResultSet executeSelect(PreparedStatement ps) {
		checkConnection();
		try {
			return ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public int executeInsert(String query) {
		checkConnection();
		try {
			Statement stmt = con.createStatement();
			stmt.execute(query, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			int insertedKeyValue = -1;
			if (rs.next()) {
				insertedKeyValue = rs.getInt(1);
			}
			Log.debug("Executed insert and got key: " + insertedKeyValue);
			return insertedKeyValue;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int executeInsert(PreparedStatement ps) {
		checkConnection();
		try {
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			int insertedKeyValue = -1;
			if (rs.next()) {
				insertedKeyValue = rs.getInt(1);
			}
			Log.debug("Executed insert and got key: " + insertedKeyValue);
			return insertedKeyValue;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int executeUpdate(String query) {
		checkConnection();
		try {
			return con.createStatement().executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public int executeUpdate(PreparedStatement ps) {
		checkConnection();
		try {
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public void closeStatement(PreparedStatement ps, ResultSet rs) {
		try { 
			if (rs != null) rs.close(); 
		} catch (Exception e) {
		};
	    try { 
	    	if (ps != null) ps.close(); 
	    } catch (Exception e) {
	    };
	}
	
	public void closeStatement(PreparedStatement ps) {
	    try { 
	    	if (ps != null) ps.close(); 
	    } catch (Exception e) {
	    };
	}
	
	private void checkConnection() {
		try {
			
			if (con.isClosed()) {
				con = initConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * Runs a select statement to make sure we can still talk to the
     * database.
     */
    public void ping() {
        Log.debug("AuthDatabase: ping");
        Statement stmt = null;
        try {
            String sql = "SELECT 1 from account";
            stmt = con.createStatement();
            stmt.executeQuery(sql);
        } catch (Exception e) {
            reconnect();
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                    stmt = null;
                }
            }
        }
    }
    
    /**
     * Reestablish contact with the database, or throw an error if we
     * can't.
     */
    void reconnect() {
        // looks like the database connection went away, re-establish
        Log.info("Database reconnect: url=" + Engine.getDBUrl());

        int failCount=0;
        try {
            while (true) {
                try {
                    con = initConnection();
                    Log.info("Database: reconnected to "+Engine.getDBUrl());
                    return;
                } catch (Exception e) {
                    try {
                        if (failCount == 0)
                            Log.exception("Database reconnect failed, retrying",e);
                        else if (failCount % 300 == 299)
                            Log.error("Database reconnect failed, retrying: "+e);
                        failCount++;
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        /* ignore */
                    }
                }
            }
        } finally {
        }
    }

	class KeepAlive implements Runnable {
        KeepAlive() {
        }
        
        public void run() {
            while (true) {
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    Log.exception("AdminDatabase.KeepAlive: interrupted", e);
                }
                try {
                    if (AuthQueries.this.con != null) {
                    	AuthQueries.this.ping();
                    }
                } catch (AORuntimeException e) {
                    Log.exception("AdminDatabase.KeepAlive: ping caught exception", e);
                }
            }
        }
    }
}
