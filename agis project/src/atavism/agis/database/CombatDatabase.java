package atavism.agis.database;

import atavism.agis.abilities.CombatMeleeAbility;
import atavism.agis.abilities.EffectAbility;
import atavism.agis.abilities.FriendlyEffectAbility;
import atavism.agis.abilities.MagicalAttackAbility;
import atavism.agis.arenas.ArenaCategory;
import atavism.agis.core.Agis;
import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisEffect;
import atavism.agis.core.Cooldown;
import atavism.agis.effects.*;
import atavism.agis.objects.*;
import atavism.agis.plugins.CombatPlugin;
import atavism.agis.util.HelperFunctions;
import atavism.server.math.Point;
import atavism.server.util.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class CombatDatabase {
	protected static Queries queries;

	public CombatDatabase(boolean keepAlive) {
		if (queries == null)
			queries = new Queries(keepAlive);
	}

	public LinkedList<String> LoadStats() {
		LinkedList<String> statlist = new LinkedList<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// First load in base stats
			ps = queries.prepare("SELECT * FROM stat where type = 0 AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String statname = HelperFunctions.readEncodedString(rs.getBytes("name"));
					// Base stat
					BaseStatDef baseStatDef = new BaseStatDef(statname);
					baseStatDef.setMobStartingValue(rs.getInt("mob_base"));
					baseStatDef.setMobLevelIncrease(rs.getInt("mob_level_increase"));
					baseStatDef.setMobLevelPercentIncrease(rs.getFloat("mob_level_percent_increase"));
					Log.debug("STAT: registering stat: " + statname);
					CombatPlugin.registerStat(baseStatDef);
					statlist.add(statname);
					// Check for purpose of stat
					String statFunction = HelperFunctions.readEncodedString(rs.getBytes("stat_function"));
					if (statFunction == null) {
						// Do nothing
					} else if (statFunction.equals("Physical Power")) {
						CombatPlugin.PHYSICAL_POWER_STAT = statname;
					} else if (statFunction.equals("Magical Power")) {
						CombatPlugin.MAGICAL_POWER_STAT = statname;
					} else if (statFunction.equals("Physical Accuracy")) {
						CombatPlugin.PHYSICAL_ACCURACY_STAT = statname;
					} else if (statFunction.equals("Magical Accuracy")) {
						CombatPlugin.MAGICAL_ACCURACY_STAT = statname;
					}
					Log.debug("STAT: added base stat:" + statname);

				}
			}

			// Now load in resistance stats
			ps = queries.prepare("SELECT * FROM stat where type = 1 AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String statname = HelperFunctions.readEncodedString(rs.getBytes("name"));
					// Resistance stat (armor stat)
					ResistanceStatDef statDef = new ResistanceStatDef(statname);
					statDef.setMobStartingValue(rs.getInt("mob_base"));
					statDef.setMobLevelIncrease(rs.getInt("mob_level_increase"));
					statDef.setMobLevelPercentIncrease(rs.getFloat("mob_level_percent_increase"));
					Log.debug("STAT: added resistance stat:" + statname);
					CombatPlugin.registerStat(statDef);
					statlist.add(statname);
				}
			}

			// Finally load in vitality stats
			ps = queries.prepare("SELECT * FROM stat where type = 2 AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String statname = HelperFunctions.readEncodedString(rs.getBytes("name"));

					String maxStat = HelperFunctions.readEncodedString(rs.getBytes("maxStat"));
					VitalityStatDef statDef = new VitalityStatDef(statname, CombatPlugin.lookupStatDef(maxStat));
					setVitalityStatSettings(statDef, rs);
					statDef.setMobStartingValue(rs.getInt("mob_base"));
					statDef.setMobLevelIncrease(rs.getInt("mob_level_increase"));
					statDef.setMobLevelPercentIncrease(rs.getFloat("mob_level_percent_increase"));
					CombatPlugin.registerStat(statDef, false, maxStat);

					statlist.add(statname);
					String statFunction = HelperFunctions.readEncodedString(rs.getBytes("stat_function"));
					if (statFunction != null && statFunction.equals("Weight")) {
						CombatPlugin.WEIGHT_STAT = statname;
					} 
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		// Load in stat links
		for(String statname : statlist) {
			LoadStatLinks(CombatPlugin.lookupStatDef(statname));
		}
				
		return statlist;
	}

	/**
	 * Set the properties for the VitalityStatDef. Loads in the values from the
	 * ResultSet.
	 * 
	 * @param statDef
	 * @param rs
	 */
	public void setVitalityStatSettings(VitalityStatDef statDef, ResultSet rs) {
		try {
			int min = rs.getInt("min");
			int shiftTarget = rs.getInt("shiftTarget");
			int shiftValue = rs.getInt("shiftValue");
			int shiftReverseValue = rs.getInt("shiftReverseValue");
			int shiftInterval = rs.getInt("shiftInterval");
			boolean isShiftPercent = rs.getBoolean("isShiftPercent");
			String onMaxHit = HelperFunctions.readEncodedString(rs.getBytes("onMaxHit"));
			String onMinHit = HelperFunctions.readEncodedString(rs.getBytes("onMinHit"));
			boolean canExceedMax = rs.getBoolean("canExceedMax");

			statDef.setMin(min);
			statDef.setMax(100);
			statDef.setShiftTarget(shiftTarget);
			statDef.setShiftValue(shiftValue);
			statDef.setReverseShiftValue(shiftReverseValue);
			statDef.setShiftInterval(shiftInterval);
			statDef.isShiftPercent(isShiftPercent);
			if (onMaxHit != null && !onMaxHit.isEmpty()
					&& !onMaxHit.equals("~ none ~")) {
				statDef.setOnMaxHit(onMaxHit);
			}
			if (onMinHit != null && !onMinHit.isEmpty()
					&& !onMinHit.equals("~ none ~")) {
				statDef.setOnMinHit(onMinHit);
			}
			// Requirements
			String shiftReq = HelperFunctions.readEncodedString(rs.getBytes("shiftReq1"));
			boolean shiftReqState = rs.getBoolean("shiftReq1State");
			boolean shiftReqSetReverse = rs.getBoolean("shiftReq1SetReverse");
			if (shiftReq != null && !shiftReq.isEmpty() && !shiftReq.equals("~ none ~")) {
				statDef.addShiftRequirement(shiftReq, shiftReqState,
						shiftReqSetReverse);
			}
			shiftReq = HelperFunctions.readEncodedString(rs.getBytes("shiftReq2"));
			shiftReqState = rs.getBoolean("shiftReq2State");
			shiftReqSetReverse = rs.getBoolean("shiftReq2SetReverse");
			if (shiftReq != null && !shiftReq.isEmpty() && !shiftReq.equals("~ none ~")) {
				statDef.addShiftRequirement(shiftReq, shiftReqState,
						shiftReqSetReverse);
			}
			shiftReq = HelperFunctions.readEncodedString(rs.getBytes("shiftReq3"));
			shiftReqState = rs.getBoolean("shiftReq3State");
			shiftReqSetReverse = rs.getBoolean("shiftReq3SetReverse");
			if (shiftReq != null && !shiftReq.isEmpty() && !shiftReq.equals("~ none ~")) {
				statDef.addShiftRequirement(shiftReq, shiftReqState, shiftReqSetReverse);
			}
			
			// Death and Release reset settings
			statDef.setStartPercent(rs.getInt("startPercent"));
			statDef.setDeathResetPercent(rs.getInt("deathResetPercent"));
			statDef.setReleaseResetPercent(rs.getInt("releaseResetPercent"));
						
			// Can the stat exceed max?
			statDef.setCanExceedMax(canExceedMax);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void LoadStatLinks(AgisStatDef statDef) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// First load in base stats
			ps = queries.prepare("SELECT * FROM stat_link where stat = '" + statDef.getName() + "' AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					statDef.addStatLink(rs.getString("statTo"), rs.getFloat("changePerPoint"));
					statDef.addDependent(CombatPlugin.lookupStatDef(rs.getString("statTo")));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads in the stats that will be shared/shown to other group members.
	 * @return
	 */
	public LinkedList<String> LoadGroupSharedStats() {
		LinkedList<String> statlist = new LinkedList<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// First load in base stats
			ps = queries.prepare("SELECT name FROM stat where sharedWithGroup = 1 AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String statname = HelperFunctions.readEncodedString(rs.getBytes("name"));
					statlist.add(statname);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		return statlist;
	}

	public HashMap<String, String> LoadDamageTypes() {
		HashMap<String, String> damageTypesMap = new HashMap<String, String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM damage_type where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String damageTypeName = HelperFunctions.readEncodedString(rs.getBytes("name"));
					String resistanceStat = HelperFunctions.readEncodedString(rs.getBytes("resistance_stat"));
					damageTypesMap.put(damageTypeName, resistanceStat);
					Log.debug("DMGTYPE: added damage type: " + damageTypeName
							+ " with resistance stat: " + resistanceStat);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return damageTypesMap;
	}

	public HashMap<Integer, Integer> loadLevelExpRequirements() {
		HashMap<Integer, Integer> levelExpRequirements = new HashMap<Integer, Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries
					.prepare("SELECT * FROM `level_xp_requirements` where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int level = rs.getInt("level");
					int expRequired = rs.getInt("xpRequired");
					levelExpRequirements.put(level, expRequired);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return levelExpRequirements;
	}

	/**
	 * Loads in all of the combat effects from the effects table. It gets the
	 * effectMainType field for each property then calls the matching function
	 * to read in and set the data needed for the effect type.
	 * 
	 * @return
	 */
	public ArrayList<AgisEffect> loadCombatEffects() {
		ArrayList<AgisEffect> list = new ArrayList<AgisEffect>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM effects where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String effectMainType = HelperFunctions.readEncodedString(rs.getBytes("effectMainType"));
					if (effectMainType.equals("Damage")) {
						AgisEffect effect = loadDamageEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Restore")) {
						AgisEffect effect = loadRestorationEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Revive")) {
						AgisEffect effect = loadReviveEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Damage Mitigation")) {
						AgisEffect effect = loadDamageMitigationEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Stat")) {
						AgisEffect effect = loadStatEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Property")) {
						AgisEffect effect = loadPropertyEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("State")) {
						AgisEffect effect = loadStateEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Morph")) {
						AgisEffect effect = loadMorphEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Cooldown")) {
						AgisEffect effect = loadCooldownEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Stun")) {
						AgisEffect effect = loadStunEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Immune")) {
						AgisEffect effect = loadImmuneEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Teleport")) {
						AgisEffect effect = loadTeleportEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Create Item")) {
						AgisEffect effect = loadCreateItemEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Task")) {
						AgisEffect effect = loadTaskEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Extension Message")) {
						AgisEffect effect = loadExtensionMessageEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Spawn")) {
						AgisEffect effect = loadSpawnEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Despawn")) {
						AgisEffect effect = loadDespawnEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Alter Skill Level")) {
						AgisEffect effect = loadAlterSkillCurrentEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Teach Skill")) {
						AgisEffect effect = loadTeachSkillEffect(rs);
						if (effect != null)
							list.add(effect);
					}  else if (effectMainType.equals("Teach Ability")) {
						AgisEffect effect = loadTeachAbilityEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Mount")) {
						AgisEffect effect = loadMountEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Threat")) {
						AgisEffect effect = loadThreatEffect(rs);
						if (effect != null)
							list.add(effect);
					} else if (effectMainType.equals("Other") || effectMainType.equals("Build Object") ) {
						AgisEffect effect = loadOtherEffect(rs);
						if (effect != null)
							list.add(effect);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	/**
	 * Load in the specific data for a Damage Effect.
	 * @param rs
	 * @return
	 */
	public AgisEffect loadDamageEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("FlatDamageEffect")) {
				DamageEffect effect = new DamageEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinInstantDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageMod(rs.getFloat("floatValue1"));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("MeleeStrikeEffect")) {
				MeleeStrikeEffect effect = new MeleeStrikeEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinInstantDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageMod(rs.getFloat("floatValue1"));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.addBonusDmgEffectVal(rs.getInt("intValue2"));
				effect.addBonusDmgVal(rs.getInt("intValue3"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("MagicalStrikeEffect")) {
				MagicalStrikeEffect effect = new MagicalStrikeEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinInstantDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageMod(rs.getFloat("floatValue1"));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.addBonusDmgEffectVal(rs.getInt("intValue2"));
				effect.addBonusDmgVal(rs.getInt("intValue3"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("PhysicalDotEffect")) {
				PhysicalDotEffect effect = new PhysicalDotEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setEffectName(HelperFunctions.readEncodedString(rs.getBytes("displayName")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageMod(rs.getFloat("floatValue1"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setNumPulses(rs.getInt("pulseCount"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				if (rs.getBoolean("passive")) {
					effect.isPassive(true);
					effect.isContinuous(true);
				}
				effect.isPersistent(true);
				effect.isPeriodic(true);
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("MagicalDotEffect")) {
				MagicalDotEffect effect = new MagicalDotEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageProperty(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				effect.setDamageMod(rs.getFloat("floatValue1"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setNumPulses(rs.getInt("pulseCount"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				if (rs.getBoolean("passive")) {
					effect.isPassive(true);
					effect.isContinuous(true);
				}
				effect.isPersistent(true);
				effect.isPeriodic(true);
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("HealthStealEffect")) {
				HealthStealEffect effect = new HealthStealEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinInstantDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageMod(rs.getFloat("floatValue1"));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.addBonusDmgEffectVal(rs.getInt("intValue2"));
				effect.addBonusDmgVal(rs.getInt("intValue3"));
				effect.setTransferModifier(rs.getFloat("floatValue2"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("HealthStealDotEffect")) {
				HealthStealDotEffect effect = new HealthStealDotEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setEffectName(HelperFunctions.readEncodedString(rs.getBytes("displayName")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinDamage(rs.getInt("intValue1"));
				String damageType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (damageType != null)
					effect.setDamageType(damageType);
				effect.setDamageMod(rs.getFloat("floatValue1"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setNumPulses(rs.getInt("pulseCount"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				effect.isPersistent(true);
				effect.isPeriodic(true);
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.setTransferModifier(rs.getFloat("floatValue2"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadRestorationEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("HealInstantEffect")) {
				Log.debug("Adding heal effect of type: HealInstantEffect");
				HealInstantEffect effect = new HealInstantEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinInstantHeal(rs.getInt("intValue1"));
				effect.setHealProperty(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				Log.debug("Added heal effect: " + effect.effectVal);
				return effect;
			} else if (effectType.equals("HealOverTimeEffect")) {
				HealOverTimeEffect effect = new HealOverTimeEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setDisplayName(HelperFunctions.readEncodedString(rs.getBytes("displayName")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinHeal(rs.getInt("intValue1"));
				effect.setHealProperty(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setNumPulses(rs.getInt("pulseCount"));
				effect.setPulseCoordEffect(rs.getString("pulseCoordEffect"));
				if (rs.getBoolean("passive")) {
					effect.isPassive(true);
					effect.isContinuous(true);
				}
				effect.isPersistent(true);
				effect.isPeriodic(true);
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(true);
				return effect;
			} else if (effectType.equals("HealthTransferEffect")) {
				HealthTransferEffect effect = new HealthTransferEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMinInstantHeal(rs.getInt("intValue1"));
				effect.setHealProperty(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.setTransferModifier(rs.getFloat("floatValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(true);
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadReviveEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("ReviveEffect")) {
				ReviveEffect effect = new ReviveEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				// effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				effect.setHealthPercent(rs.getInt("intValue1"));
				effect.setHealthStat(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				String vitalityStat = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				if (vitalityStat != null && vitalityStat != "" && !vitalityStat.contains("none")) {
					effect.setVitalityPercent(rs.getInt("intValue2"));
					effect.setVitalityStat(vitalityStat);
				}
				String vitalityStat2 = HelperFunctions.readEncodedString(rs.getBytes("stringValue3"));
				if (vitalityStat2 != null && vitalityStat2 != "" && !vitalityStat2.contains("none")) {
					effect.setVitalityPercent2(rs.getInt("intValue3"));
					effect.setVitalityStat2(vitalityStat2);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadDamageMitigationEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("DamageMitigationEffect")) {
				DamageMitigationEffect effect = new DamageMitigationEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				// effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setAmountMitigated(rs.getInt("intValue1"));
				effect.setAttacksMitigated(rs.getInt("intValue2"));
				effect.isBuff(true);
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadStatEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("StatEffect")) {
				StatEffect effect = new StatEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setDisplayName(HelperFunctions.readEncodedString(rs.getBytes("displayName")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setModifyPercentage(rs.getBoolean("boolValue1"));
				for (int i = 1; i < 6; i++) {
					String statName = HelperFunctions.readEncodedString(rs.getBytes("stringValue" + i));
					float statValue = rs.getFloat("floatValue" + i);
					if (statName != null && statValue != 0)
						effect.setStat(statName, statValue);
				}
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setNumPulses(rs.getInt("pulseCount"));
				if (rs.getBoolean("passive")) {
					effect.isPassive(true);
					effect.isContinuous(true);
				} else if (effect.getDuration() > 0) {
					effect.isPersistent(true);
					// effect.isPeriodic(true);
				}
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadPropertyEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("PropertyEffect")) {
				PropertyEffect effect = new PropertyEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				String property = HelperFunctions.readEncodedString(rs.getBytes("stringValue1"));
				effect.setPropertyName(property);
				String propertyType = HelperFunctions.readEncodedString(rs.getBytes("stringValue2"));
				String propertyValue = HelperFunctions.readEncodedString(rs.getBytes("stringValue3"));
				String propertyDefault = HelperFunctions.readEncodedString(rs.getBytes("stringValue4"));
				if (propertyType.equals("Boolean") && propertyValue.equals("True")) {
					effect.setPropertyValue(true);
					if (propertyDefault.equals("True"))
						effect.setPropertyDefault(true);
					else
						effect.setPropertyDefault(false);
				} else if (propertyType.equals("Boolean") && propertyValue.equals("False")) {
					effect.setPropertyValue(false);
					if (propertyDefault.equals("True"))
						effect.setPropertyDefault(true);
					else
						effect.setPropertyDefault(false);
				} else if (propertyType.equals("Integer")) {
					int propVal = Integer.parseInt(propertyValue);
					effect.setPropertyValue(propVal);
					int propDef = Integer.parseInt(propertyDefault);
					effect.setPropertyDefault(propDef);
				} else if (propertyType.equals("Long")) {
					Long propVal = Long.parseLong(propertyValue);
					effect.setPropertyValue(propVal);
					Long propDef = Long.parseLong(propertyDefault);
					effect.setPropertyDefault(propDef);
				} else if (propertyType.equals("Double")) {
					double propVal = Double.parseDouble(propertyValue);
					effect.setPropertyValue(propVal);
					double propDef = Double.parseDouble(propertyDefault);
					effect.setPropertyDefault(propDef);
				} else {
					effect.setPropertyValue(propertyValue);
					effect.setPropertyDefault(propertyDefault);
				}
				effect.setPropertyType(propertyType);
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				if (rs.getBoolean("passive")) {
					Log.debug("CDB: setting effect " + effect.getName()
							+ " to passive");
					effect.isPassive(true);
					effect.isContinuous(true);
				}
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadStateEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("StateEffect")) {
				StateEffect effect = new StateEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				String propName = HelperFunctions.readEncodedString(rs.getBytes("stringValue1"));
				effect.setPropertyName(propName);
				
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				// Set it to continuous so it isn't removed on logout/in
				effect.isContinuous(true);
				if (duration < 1) {
					// Set persistent = false when we have no duration so this lasts until cancelled
					effect.isPersistent(false);
					effect.isPassive(true);
				}
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadMorphEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("MorphEffect")) {
				MorphEffect effect = new MorphEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				String model = HelperFunctions.readEncodedString(rs.getBytes("stringValue1"));
				effect.setModel(model);
				
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				// Set it to continuous so it isn't removed on logout/in
				effect.isContinuous(true);
				if (duration < 1) {
					// Set persistent = false when we have no duration so this lasts until cancelled
					effect.isPersistent(false);
				}
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadCooldownEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("CooldownEffect")) {
				CooldownEffect effect = new CooldownEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				String cooldown = HelperFunctions.readEncodedString(rs.getBytes("stringValue1"));
				effect.addCooldownToAlter(cooldown);
				boolean resetCooldown = rs.getBoolean("boolValue1");
				if (resetCooldown) {
					effect.setCooldownOffset(-1l);
				} else {
					int cooldownOffset = rs.getInt("intValue1");
					effect.setCooldownOffset((long) cooldownOffset);
				}
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadStunEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("StunEffect")) {
				StunEffect effect = new StunEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadImmuneEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("ImmuneEffect")) {
				ImmuneEffect effect = new ImmuneEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setEffectVal(rs.getInt("id"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setSkillEffectMod(rs.getFloat("skillLevelMod"));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadTeleportEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("TeleportEffect")) {
				TeleportEffect effect = new TeleportEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				// effect.setEffectName(rs.getString("displayName"));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				//String marker = rs.getString("marker");

				//if (marker == null) {
					effect.setTeleportType(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
					float locX = rs.getInt("floatValue1");
					float locY = rs.getInt("floatValue2");
					float locZ = rs.getInt("floatValue3");
					Point p = new Point(locX, locY, locZ);
					effect.setTeleportLocation(p);
				/*} else {
					effect.setMarkerName(marker);
				}*/
					
				effect.setInstance(rs.getInt("intValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadCreateItemEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("CreateItemEffect")) {
				CreateItemEffect effect = new CreateItemEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setItem(rs.getInt("intValue1"));
				effect.setNumberToCreate(rs.getInt("intValue2"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadTaskEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("TaskCompleteEffect")) {
				TaskCompleteEffect effect = new TaskCompleteEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setTaskID(rs.getInt("intValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadExtensionMessageEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("MessageEffect")) {
				SendExtensionMessageEffect effect = new SendExtensionMessageEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMessageType(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadSpawnEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("SpawnEffect")) {
				SpawnEffect effect = new SpawnEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMobID(rs.getInt("intValue1"));
				effect.setSpawnType(rs.getInt("intValue2"));
				effect.setPassiveEffect(rs.getInt("intValue3"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadDespawnEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("DespawnEffect")) {
				DespawnEffect effect = new DespawnEffect(rs.getInt("id"),
						HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMobID(rs.getInt("intValue1"));
				effect.setDespawnType(rs.getInt("intValue2"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadAlterSkillCurrentEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("AlterSkillCurrentEffect")) {
				AlterSkillCurrentEffect effect = new AlterSkillCurrentEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setSkillType(rs.getInt("intValue1"));
				effect.setAlterValue(rs.getInt("intValue2"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadTeachAbilityEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("TeachAbilityEffect")) {
				TeachAbilityEffect effect = new TeachAbilityEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setAbilityID(rs.getInt("intValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadTeachSkillEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("TeachSkillEffect")) {
				TeachSkillEffect effect = new TeachSkillEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setSkillType(rs.getInt("intValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadMountEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("MountEffect")) {
				MountEffect effect = new MountEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setMountType(rs.getInt("intValue1"));
				effect.setMountSpeedIncrease(rs.getInt("intValue2"));
				effect.setModel(HelperFunctions.readEncodedString(rs.getBytes("stringValue1")));
				effect.isContinuous(true);
				effect.isPersistent(false);
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				effect.isBuff(rs.getBoolean("isBuff"));
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public AgisEffect loadThreatEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("ThreatEffect")) {
				ThreatEffect effect = new ThreatEffect(
						rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				effect.setAlterValue(rs.getInt("intValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public AgisEffect loadOtherEffect(ResultSet rs) {
		try {
			String effectType = HelperFunctions.readEncodedString(rs.getBytes("effectType"));
			if (effectType.equals("TameEffect")) {
				TameEffect effect = new TameEffect(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("ResultEffect")) {
				ResultEffect effect = new ResultEffect(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("CreateClaimEffect")) {
				CreateClaimEffect effect = new CreateClaimEffect(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			} else if (effectType.equals("BuildObjectEffect")) {
				BuildObjectEffect effect = new BuildObjectEffect(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
				effect.setIcon(HelperFunctions.readEncodedString(rs.getBytes("icon")));
				float duration = 1000f * rs.getFloat("duration");
				effect.setDuration((int) duration);
				effect.setBuildObjectTemplateID(rs.getInt("intValue1"));
				int bonusEffectReq = rs.getInt("bonusEffectReq");
				boolean bonusEffectReqConsumed = rs
						.getBoolean("bonusEffectReqConsumed");
				int bonusEffect = rs.getInt("bonusEffect");
				if (bonusEffect != -1) {
					effect.setBonusEffectReq(bonusEffectReq);
					effect.setBonusEffectReqConsumed(bonusEffectReqConsumed);
					effect.setBonusEffect(bonusEffect);
				}
				return effect;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<AgisAbility> loadAbilities() {
		ArrayList<AgisAbility> list = new ArrayList<AgisAbility>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM abilities where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("ABILITY: Reading in another ability.");
					String abilityType = HelperFunctions.readEncodedString(rs.getBytes("abilityType"));
					Log.debug("ABILITY: spot 0");
					if (abilityType.equals("CombatMeleeAbility")) {
						Log.debug("ABILITY: spot 1");
						CombatMeleeAbility ability = new CombatMeleeAbility(
								HelperFunctions.readEncodedString(rs.getBytes("name")));
						setAbilityData(rs, ability);
						Integer activationEffect = rs.getInt("activationEffect1");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							String target = HelperFunctions.readEncodedString(rs.getBytes("activationTarget1"));
							ability.addEffectTarget(target);
							Log.debug("ActivationEffect1: " + activationEffect
									+ " with target: " + target);
						}
						activationEffect = rs.getInt("activationEffect2");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget2"));
							Log.debug("ActivationEffect2: " + activationEffect);
						}
						activationEffect = rs.getInt("activationEffect3");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget3"));
							Log.debug("ActivationEffect3: " + activationEffect);
						}
						/*
						 * String coordEffect =
						 * rs.getString("coordEffect1effect"); if
						 * (!coordEffect.equals("")) {
						 * ability.addCoordEffect(state, new
						 * CoordinatedEffect()); }
						 */
						list.add(ability);
						Log.debug("ABILITY: added " + ability.getName()
								+ " to the template list.");
					} else if (abilityType.equals("MagicalAttackAbility")) {
						Log.debug("ABILITY: spot 1");
						MagicalAttackAbility ability = new MagicalAttackAbility(
								HelperFunctions.readEncodedString(rs.getBytes("name")));
						setAbilityData(rs, ability);
						Integer activationEffect = rs.getInt("activationEffect1");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget1"));
						}
						activationEffect = rs.getInt("activationEffect2");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget2"));
						}
						activationEffect = rs.getInt("activationEffect3");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget3"));
						}
						/*
						 * String coordEffect =
						 * HelperFunctions.readEncodedString(rs.getBytes("coordEffect1effect"); if
						 * (!coordEffect.equals("")) {
						 * ability.addCoordEffect(state, new
						 * CoordinatedEffect()); }
						 */
						list.add(ability);
						Log.debug("ABILITY: added " + ability.getName()
								+ " to the template list.");
					} else if (abilityType.equals("FriendlyEffectAbility")) {
						Log.debug("ABILITY: spot 1");
						FriendlyEffectAbility ability = new FriendlyEffectAbility(
								HelperFunctions.readEncodedString(rs.getBytes("name")));
						setAbilityData(rs, ability);
						Integer activationEffect = rs.getInt("activationEffect1");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							String target = HelperFunctions.readEncodedString(rs.getBytes("activationTarget1"));
							ability.addEffectTarget(target);
							Log.debug("ActivationEffect1: " + activationEffect
									+ " with target: " + target);
						}
						activationEffect = rs.getInt("activationEffect2");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget2"));
						}
						activationEffect = rs.getInt("activationEffect3");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget3"));
						}
						/*
						 * String coordEffect =
						 * rs.getString("coordEffect1effect"); if
						 * (!coordEffect.equals("")) {
						 * ability.addCoordEffect(state, new
						 * CoordinatedEffect()); }
						 */
						list.add(ability);
						Log.debug("ABILITY: added " + ability.getName()
								+ " to the template list.");
					} else if (abilityType.equals("EffectAbility")) {
						Log.debug("ABILITY: spot 1");
						EffectAbility ability = new EffectAbility(
								HelperFunctions.readEncodedString(rs.getBytes("name")));
						setAbilityData(rs, ability);
						Integer activationEffect = rs.getInt("activationEffect1");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget1"));
						}
						activationEffect = rs.getInt("activationEffect2");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget2"));
						}
						activationEffect = rs.getInt("activationEffect3");
						if (activationEffect != null && activationEffect > 0) {
							ability.addActivationEffect(Agis.EffectManager.get(activationEffect));
							ability.addEffectTarget(rs.getString("activationTarget3"));
						}
						list.add(ability);
						Log.debug("ABILITY: added " + ability.getName()
								+ " to the template list.");
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public void setAbilityData(ResultSet rs, AgisAbility ability) {
		try {
			Log.debug("Getting ability Data: " + rs.getInt("id"));
			ability.setID(rs.getInt("id"));
			ability.setSkillType(rs.getInt("skill"));
			if (rs.getBoolean("passive"))
				ability.setAbilityType(2);
			else
				ability.setAbilityType(1);
			ability.setActivationCost(rs.getInt("activationCost"));
			ability.setCostProperty(HelperFunctions.readEncodedString(rs.getBytes("activationCostType")));
			float activationTime = rs.getFloat("activationLength") * 1000f;
			ability.setActivationTime((int) activationTime);
			String castingAnim = HelperFunctions.readEncodedString(rs.getBytes("activationAnimation"));
			if (castingAnim == null)
				castingAnim = "";
			ability.setCastingAnim(castingAnim);
			String castingParticles = HelperFunctions.readEncodedString(rs.getBytes("activationParticles"));
			if (castingParticles == null)
				castingParticles = "";
			ability.setCastingAffinity(castingParticles);
			// ability.setVigor(rs.getInt("vigor"));
			// Log.debug("Got vigor: " + rs.getInt("vigor"));
			int casterEffectRequired = rs.getInt("casterEffectRequired");
			if (casterEffectRequired > 0) {
				ability.addAttackerEffectReq(casterEffectRequired);
				if (rs.getBoolean("casterEffectConsumed"))
					ability.addAttackerEffectConsumption(casterEffectRequired);
			}
			int targetEffectRequired = rs.getInt("targetEffectRequired");
			if (targetEffectRequired > 0) {
				ability.addTargetEffectReq(targetEffectRequired);
				if (rs.getBoolean("targetEffectConsumed"))
					ability.addTargetEffectConsumption(targetEffectRequired);
			}
			String weaponRequired = HelperFunctions.readEncodedString(rs.getBytes("weaponRequired"));
			if (weaponRequired != null && !weaponRequired.equals("")
					&& !weaponRequired.contains("none")
					&& !weaponRequired.contains("None")) {
				ability.setWeaponReq(weaponRequired);
			}
			// if (rs.getBoolean("decrementWeaponUses"))
			// ability.setDecrementWeaponUses(true);
			int reagentRequired = rs.getInt("reagentRequired");
			if (reagentRequired > 0)
				ability.addReagent(reagentRequired);
			if (rs.getBoolean("reagentConsumed"))
				ability.setConsumeReagents(true);
			Integer ammoUsed = rs.getInt("ammoUsed");
			if (ammoUsed != null) {
				ability.setAmmoReq(ammoUsed);
			}
			ability.setMaxRange(rs.getInt("maxRange"));
			ability.setMinRange(rs.getInt("minRange"));
			ability.setAreaOfEffectRadius(rs.getInt("aoeRadius"));
			ability.setReqTarget(rs.getBoolean("reqTarget"));
			String targetType = HelperFunctions.readEncodedString(rs.getBytes("targetType"));
			Log.debug("Setting target type: " + targetType);
			if (targetType.equals("Enemy")) {
				ability.setTargetType(AgisAbility.TargetType.ENEMY);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to Enemy");
			} else if (targetType.equals("Self")) {
				ability.setTargetType(AgisAbility.TargetType.SELF);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to Self");
			} else if (targetType.equals("Friendly")) {
				ability.setTargetType(AgisAbility.TargetType.FRIEND);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to Friend");
			} else if (targetType.equals("Friend Not Self")) {
				ability.setTargetType(AgisAbility.TargetType.FRIENDNOTSELF);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to FriendNotSelf");
			} else if (targetType.equals("Group")) {
				ability.setTargetType(AgisAbility.TargetType.GROUP);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to Group");
			} else if (targetType.equals("AoE Enemy")) {
				ability.setTargetType(AgisAbility.TargetType.AREA_ENEMY);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to AoE Enemy");
			} else if (targetType.equals("AoE Friendly")) {
				ability.setTargetType(AgisAbility.TargetType.AREA_FRIENDLY);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to AoE Friendly");
			} else {
				ability.setTargetType(AgisAbility.TargetType.ANY);
				Log.debug("ABILITY: targetType for ability "
						+ ability.getName() + " to Any");
			}
			// Targetable Species
			String targetableSpecies = HelperFunctions.readEncodedString(rs.getBytes("speciesTargetReq"));
			if (targetableSpecies == null || targetableSpecies.equals("")
					|| targetableSpecies.equals("Any")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.ANY);
			} else if (targetableSpecies.equals("Beast")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.BEAST);
			} else if (targetableSpecies.equals("Humanoid")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.HUMANOID);
			} else if (targetableSpecies.equals("Elemental")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.ELEMENTAL);
			} else if (targetableSpecies.equals("Undead")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.UNDEAD);
			} else if (targetableSpecies.equals("Player")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.PLAYER);
			} else if (targetableSpecies.equals("Non Player")) {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.NONPLAYER);
			} else {
				ability.addTargetableSpecies(AgisAbility.TargetSpecies.UNINIT);
			}
			String specificTarget = HelperFunctions.readEncodedString(rs.getBytes("specificTargetReq"));
			if (specificTarget != null && !specificTarget.equals("")) {
				LinkedList<String> specificTargets = new LinkedList<String>();
				specificTargets.add(specificTarget);
				ability.setSpecificTargets(specificTargets);
			}

			ability.setTargetDeath(rs.getInt("targetState"));
			if (rs.getBoolean("globalCooldown"))
				ability.addCooldown(new Cooldown("GLOBAL", 1000));
			// TODO: Change the weapon cooldown to be a property for the ability
			// that adds a cooldown that is the same as the equipped weapon
			// cooldown
			if (rs.getBoolean("weaponCooldown"))
				ability.addCooldown(new Cooldown("WEAPON", 3000));
			String cooldown = HelperFunctions.readEncodedString(rs.getBytes("cooldown1Type"));
			float cooldownDuration = rs.getFloat("cooldown1Duration") * 1000f;
			if (cooldownDuration > 0) {
				if (cooldown != null && !cooldown.equals("")) {
					ability.addCooldown(new Cooldown(cooldown,
							(int) cooldownDuration));
				} else {
					ability.addCooldown(new Cooldown("" + ability.getID(),
							(int) cooldownDuration));
				}
			}
			/*
			 * cooldown = rs.getString("cooldown2Type"); if (cooldown != null &&
			 * !cooldown.equals("")) ability.addCooldown(new Cooldown(cooldown,
			 * rs.getInt("cooldown2Duration")));
			 */
			// ability.setTooltip(rs.getString("tooltip"));
			// Coordinated Effects
			for (int i = 1; i <= 2; i++) {
				String coordinatedEffect = HelperFunctions.readEncodedString(rs.getBytes("coordEffect" + i));
				if (coordinatedEffect != null) {
					CoordinatedEffect coordEffect = loadCoordEffect(coordinatedEffect);
					if (coordEffect != null) {
						String stateString = HelperFunctions.readEncodedString(rs.getBytes("coordEffect" + i
								+ "event"));
						AgisAbility.ActivationState state = AgisAbility.ActivationState.COMPLETED;
						if (stateString.equals("activating")) {
							state = AgisAbility.ActivationState.ACTIVATING;
						} else if (stateString.equals("completed")) {
							state = AgisAbility.ActivationState.COMPLETED;
						} else if (stateString.equals("activated")) {
							state = AgisAbility.ActivationState.ACTIVATED;
						} else if (stateString.equals("initializing")) {
							state = AgisAbility.ActivationState.INIT;
						} else if (stateString.equals("channelling")) {
							state = AgisAbility.ActivationState.CHANNELLING;
						} else if (stateString.equals("interrupted")) {
							state = AgisAbility.ActivationState.INTERRUPTED;
						} else if (stateString.equals("failed")) {
							state = AgisAbility.ActivationState.FAILED;
						}
						ability.addCoordEffect(state, coordEffect);
						Log.debug("COORD: added coord effect: "
								+ coordEffect.getEffectName() + " to state: "
								+ state + " of ability: " + ability.getName());
					}
				}
			}
		} catch (SQLException e) {

		}
	}

	public HashMap<Integer, SkillTemplate> loadSkills() {
		HashMap<Integer, SkillTemplate> skills = new HashMap<Integer, SkillTemplate>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `skills` where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int skillID = rs.getInt("id");
					String name = HelperFunctions.readEncodedString(rs.getBytes("name"));
					int aspect = rs.getInt("aspect");
					int oppositeAspect = rs.getInt("oppositeAspect");
					boolean mainAspectOnly = rs.getBoolean("mainAspectOnly");
					String stat1 = HelperFunctions.readEncodedString(rs.getBytes("primaryStat"));
					String stat2 = HelperFunctions.readEncodedString(rs.getBytes("secondaryStat"));
					String stat3 = HelperFunctions.readEncodedString(rs.getBytes("thirdStat"));
					String stat4 = HelperFunctions.readEncodedString(rs.getBytes("fourthStat"));
					boolean autoLearn = rs.getBoolean("automaticallyLearn");
					SkillTemplate skillTmpl = new SkillTemplate(skillID, name,
							aspect, oppositeAspect, mainAspectOnly, stat1, stat2, stat3, stat4,
							autoLearn);

					// Load in other properties
					skillTmpl.setSkillPointCost(rs.getInt("skillPointCost"));
					skillTmpl.setMaxLevel(rs.getInt("maxLevel"));

					Integer parentSkill = rs.getInt("parentSkill");
					if (parentSkill != null && parentSkill != -1) {
						skillTmpl.setParentSkill(parentSkill);
						skillTmpl.setParentSkillLevelReq(rs.getInt("parentSkillLevelReq"));
					}

					// Load skill pre-reqs
					int prereqSkillID = rs.getInt("prereqSkill1");
					if (prereqSkillID != -1) {
						int prereqSkillLevel = rs.getInt("prereqSkill1Level");
						skillTmpl.setPrereqSkill1(prereqSkillID);
						skillTmpl.setPrereqSkill1Level(prereqSkillLevel);
					}
					prereqSkillID = rs.getInt("prereqSkill2");
					if (prereqSkillID != -1) {
						int prereqSkillLevel = rs.getInt("prereqSkill2Level");
						skillTmpl.setPrereqSkill2(prereqSkillID);
						skillTmpl.setPrereqSkill2Level(prereqSkillLevel);
					}
					prereqSkillID = rs.getInt("prereqSkill3");
					if (prereqSkillID != -1) {
						int prereqSkillLevel = rs.getInt("prereqSkill3Level");
						skillTmpl.setPrereqSkill3(prereqSkillID);
						skillTmpl.setPrereqSkill3Level(prereqSkillLevel);
					}
					skillTmpl.setPlayerLevelReq(rs.getInt("playerLevelReq"));
					// Load skill abilities
					LoadSkillAbilities(skillTmpl);
					skills.put(skillID, skillTmpl);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return skills;
	}

	/**
	 * Loads in the Abilities the Skill gives the player and adds them to the
	 * Skill Template.
	 * 
	 * @param skillTmpl
	 */
	private void LoadSkillAbilities(SkillTemplate skillTmpl) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `skill_ability_gain` where skillID = "
							+ skillTmpl.getSkillID()  + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Integer abilityID = rs.getInt("abilityID");
					if (abilityID != null && abilityID > 0) {
						int level = rs.getInt("skillLevelReq");
						String abilityName = getAbilityName(abilityID);
						boolean autoLearn = rs.getBoolean("automaticallyLearn");
						skillTmpl.addSkillAbility(level, abilityID,abilityName, autoLearn);
						Log.debug("SKILL: adding ability " + abilityID
								+ " to skill: " + skillTmpl.getSkillID()
								+ " with skill level: " + level);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}

	/**
	 * Gets the name for the Ability specified.
	 * 
	 * @param abilityID
	 * @return
	 */
	private String getAbilityName(int abilityID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT name FROM `abilities` where id=" + abilityID + "");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String name = HelperFunctions.readEncodedString(rs.getBytes("name"));
					return name;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return null;
	}

	private CoordinatedEffect loadCoordEffect(String coordEffectName) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `coordinated_effects` where name='"
							+ coordEffectName + "'");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					/*
					 * CoordinatedEffect coordEffect = new
					 * CoordinatedEffect(rs.getString("coordName"));
					 * coordEffect.sendSourceOid(rs.getBoolean("sendCasterID"));
					 * coordEffect.sendTargetOid(rs.getBoolean("sendTargetID"));
					 * for (int i = 1; i <=3; i++) { String arg =
					 * rs.getString("arg" + i); if (arg != null) { String argVal
					 * = rs.getString("arg" + i + "val");
					 * coordEffect.putArgument(arg, argVal); } }
					 */
					int coordID = rs.getInt("id");
					String coordType = HelperFunctions.readEncodedString(rs.getBytes("prefab"));
					Log.debug("COORD: coordType is: " + coordType);
					// Custom co-ordinated effect
					CoordinatedEffect coordEffect = new CoordinatedEffect(
							coordType);
					coordEffect.sendSourceOid(true);
					coordEffect.sendTargetOid(true);
					coordEffect.putArgument("result", "success");
					Log.debug("COORD: returning coordEffect: " + coordID);
					return coordEffect;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return null;
	}

	public ArrayList<ArenaCategory> loadArenaCategories() {
		ArrayList<ArenaCategory> list = new ArrayList<ArenaCategory>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM arena_categories where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					Log.debug("ARENADB: loading in classic arena: " + id);

					ArrayList<String> skins = new ArrayList<String>();
					for (int i = 1; i <= 4; i++) {
						if (HelperFunctions.readEncodedString(rs.getBytes("skin" + i)) != null)
							skins.add(HelperFunctions.readEncodedString(rs.getBytes("skin" + i)));
					}
					ArenaCategory category = new ArenaCategory(id, skins);
					list.add(category);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public ArrayList<ArenaTemplate> loadArenaTemplates() {
		ArrayList<ArenaTemplate> list = new ArrayList<ArenaTemplate>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM arena_templates where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int arenaType = rs.getInt("arenaType");
					int id = rs.getInt("id");
					Log.debug("ARENADB: loading in classic arena: " + id);
					String arenaName = HelperFunctions.readEncodedString(rs.getBytes("name"));
					int arenaCategory = rs.getInt("arenaCategory");
					int instanceTemplateID = -1;
					//String worldFile = HelperFunctions.readEncodedString(rs.getBytes("worldFile"));
					int duration = rs.getInt("length");
					int victoryCondition = rs.getInt("defaultWinner");
					boolean raceSpecific = false;
					int numRounds = 1;
					ArrayList<ArrayList<Integer>> spawns = new ArrayList<ArrayList<Integer>>();
					ArenaTemplate tmpl = new ArenaTemplate(id, arenaType,
							arenaCategory, duration, victoryCondition,
							instanceTemplateID, arenaName, raceSpecific, numRounds,
							spawns);
					for (int i = 1; i <= 4; i++) {
						int teamID = rs.getInt("team" + i);
						if (teamID != -1) {
							loadArenaTeam(teamID, tmpl);
						}
					}
					tmpl.setLevelReq(rs.getInt("levelReq"));
					int victoryCurrency = rs.getInt("victoryCurrency");
					int victoryPayment = rs.getInt("victoryPayment");
					HashMap<Integer, Integer> victoryPayments = new HashMap<Integer, Integer>();
					victoryPayments.put(victoryCurrency, victoryPayment);
					tmpl.setVictoryPayment(victoryPayments);
					int defeatCurrency = rs.getInt("defeatCurrency");
					int defeatPayment = rs.getInt("defeatPayment");
					HashMap<Integer, Integer> defeatPayments = new HashMap<Integer, Integer>();
					defeatPayments.put(defeatCurrency, defeatPayment);
					tmpl.setDefeatPayment(defeatPayments);
					int victoryExp = rs.getInt("victoryExp");
					int defeatExp = rs.getInt("defeatExp");
					tmpl.setVictoryExp(victoryExp);
					tmpl.setDefeatExp(defeatExp);
					tmpl.setUseWeapons(rs.getBoolean("useWeapons"));
					list.add(tmpl);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	public void loadArenaTeam(int teamID, ArenaTemplate tmpl) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM arena_teams where id = " + teamID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("ARENADB: loading in arena team: " + teamID);
					String name = HelperFunctions.readEncodedString(rs.getBytes("name"));
					int size = rs.getInt("size");
					String race = HelperFunctions.readEncodedString(rs.getBytes("race"));
					int goal = rs.getInt("goal");
					tmpl.addTeam(name, size, race, goal);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}

	/**
	 * Having too many connection errors, so adding this function to help cope
	 * with it
	 */
	public void close() {
		queries.close();
	}
}
