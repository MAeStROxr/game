package atavism.agis.database;

import atavism.agis.objects.*;
import atavism.agis.plugins.CraftingPlugin;
import atavism.agis.util.EditorOptionMapping;
import atavism.agis.util.HelperFunctions;
import atavism.agis.util.RequirementChecker;
import atavism.server.engine.OID;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.util.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ContentDatabase {
	protected static Queries queries;
	
	public ContentDatabase(boolean keepAlive) {
		if (queries == null)
			queries = new Queries(keepAlive);
	}
	
	public String loadGameSetting(String settingName) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `game_setting` where name = '" + settingName + "'  AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					return HelperFunctions.readEncodedString(rs.getBytes("value"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return null;
	}
	
	/**
	 * Loads in the options and choices from the Atavism Editor window. Saves a copy of the editor options map
	 * to the RequirementChecker class.
	 */
	public void loadEditorOptions() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM editor_option where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					
					EditorOptionMapping editorOption = new EditorOptionMapping(rs.getInt("id"), 
							HelperFunctions.readEncodedString(rs.getBytes("optionType")));
					editor_options.put(rs.getInt("id"), editorOption);
				}
			}
			
			ps = queries.prepare("SELECT * FROM editor_option_choice where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					EditorOptionMapping editorOption = editor_options.get(rs.getInt("optionTypeID"));
					editorOption.choiceMapping.put(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("choice")));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		RequirementChecker.setEditorOptions(editor_options);
	}
	
	public HashMap<String, CharacterTemplate> loadCharacterFactoryTemplates() {
		HashMap<String, CharacterTemplate> templates = new HashMap<String, CharacterTemplate>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `character_create_template` where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int templateID = rs.getInt("id");
					int race = rs.getInt("race");
					int aspect = rs.getInt("aspect");
					int faction = rs.getInt("faction");
					int instanceID = rs.getInt("instance");
					float pos_x = rs.getFloat("pos_x");
					float pos_y = rs.getFloat("pos_y");
					float pos_z = rs.getFloat("pos_z");
					//float orientation = rs.getFloat("orientation");
					int autoAttackID = rs.getInt("autoAttack");
					CharacterTemplate tmpl = new CharacterTemplate();
					tmpl.setRace(race);
					tmpl.setAspect(aspect);
					tmpl.setFaction(faction);
					tmpl.setInstanceTemplateID(instanceID);
					//tmpl.setPortalName("spawn");
					tmpl.setSpawnPoint(new Point(pos_x, pos_y, pos_z));
					tmpl.setAutoAttack(autoAttackID);
					loadCharacterFactoryStats(tmpl, templateID);
					tmpl.setStartingSkills(loadCharacterFactorySkills(templateID));
					loadCharacterStartingItems(tmpl, templateID);
					templates.put(race + " " + aspect, tmpl);
					Log.debug("CHARTMPL: added character template: " + race + aspect);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return templates;
	}
	
	public void loadCharacterFactoryStats(CharacterTemplate tmpl, int id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `character_create_stats` where character_create_id = " + id + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String stat = HelperFunctions.readEncodedString(rs.getBytes("stat"));
					int baseValue = rs.getInt("value");
					float levelIncrease = rs.getFloat("levelIncrease");
					float levelPercentIncrease = rs.getFloat("levelPercentIncrease");
					Log.debug("CHARTMPL: added stat " + stat + " with value: " + baseValue + " to character template");
					tmpl.AddStatProgression(stat, baseValue, levelIncrease, levelPercentIncrease);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public ArrayList<Integer> loadCharacterFactorySkills(int id) {
		ArrayList<Integer> skills = new ArrayList<Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `character_create_skills` where character_create_id = " + id + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int skill = rs.getInt("skill");
					Log.debug("CHARTMPL: added skill " + skill);
					skills.add(skill);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return skills;
	}
	
	public void loadCharacterStartingItems(CharacterTemplate tmpl, int id) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `character_create_items` where character_create_id = " + id + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int item = rs.getInt("item_id");
					int count = rs.getInt("count");
					boolean equipped = rs.getBoolean("equipped");
					Log.debug("CHARTMPL: added item " + item);
					tmpl.addStartingItem(item, count, equipped);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public HashMap<Integer, ArrayList<Graveyard>> loadGraveyards() {
		Log.debug("GRAVEYARD: loading graveyards");
		HashMap<Integer, ArrayList<Graveyard>> nodes = new HashMap<Integer, ArrayList<Graveyard>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `graveyard` where isactive = 1");
			Log.debug("GRAVEYARD: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					int instanceID = rs.getInt("instance");
					Point loc = new Point();
					loc.setX(rs.getFloat("locX"));
					loc.setY(rs.getFloat("locY"));
					loc.setZ(rs.getFloat("locZ"));
					
					Graveyard obj = new Graveyard(id, HelperFunctions.readEncodedString(rs.getBytes("name")), 
							loc, rs.getInt("factionReq"), rs.getInt("factionRepReq"));
					
					if (nodes.containsKey(instanceID)) {
						nodes.get(instanceID).add(obj);
					} else {
						ArrayList<Graveyard> graveyardList = new ArrayList<Graveyard>();
						graveyardList.add(obj);
						nodes.put(instanceID, graveyardList);
					}
					Log.debug("GRAVEYARD: added node " + obj.getID() + " to map");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return nodes;
	}
	
	public HashMap<Integer, VolumetricRegion> loadRegions(int instanceID, OID instanceOid) {
		Log.debug("REGION: loading interactive objects for instance: " + instanceID);
		HashMap<Integer, VolumetricRegion> nodes = new HashMap<Integer, VolumetricRegion>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `region` where instance=? and isactive = 1");
			ps.setInt(1, instanceID);
			Log.debug("REGION: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					AOVector loc = new AOVector();
					loc.setX(rs.getFloat("locX"));
					loc.setY(rs.getFloat("locY"));
					loc.setZ(rs.getFloat("locZ"));
					VolumetricRegion obj = new VolumetricRegion(id,loc, instanceOid);
					obj.setName(HelperFunctions.readEncodedString(rs.getBytes("name")));
					obj.setRegionType(HelperFunctions.readEncodedString(rs.getBytes("regionType")));
					obj.setActionID(rs.getInt("actionID"));
					obj.setActionData1(HelperFunctions.readEncodedString(rs.getBytes("actionData1")));
					obj.setActionData2(HelperFunctions.readEncodedString(rs.getBytes("actionData2")));
					obj.setActionData3(HelperFunctions.readEncodedString(rs.getBytes("actionData3")));
					//loadRegionShapes(obj);
					
					nodes.put(obj.getID(), obj);
					Log.debug("REGION: added node " + obj.getID() + " to map");
					obj.spawn();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for (VolumetricRegion region : nodes.values()) {
			loadRegionShapes(region);
		}
		return nodes;
	}
	
	public void loadRegionShapes(VolumetricRegion region) {
		Log.debug("REGION: loading interactive objects for region: " + region.getID());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `region_shape` where regionID=? and isactive = 1");
			ps.setInt(1, region.getID());
			Log.debug("REGION: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					AOVector loc = new AOVector();
					loc.setX(rs.getFloat("locX"));
					loc.setY(rs.getFloat("locY"));
					loc.setZ(rs.getFloat("locZ"));
					String shapeType = HelperFunctions.readEncodedString(rs.getBytes("shape"));
					AOVector loc2 = new AOVector();
					loc2.setX(rs.getFloat("loc2X"));
					loc2.setY(rs.getFloat("loc2Y"));
					loc2.setZ(rs.getFloat("loc2Z"));
					Quaternion orient = new Quaternion();
					orient.setX(rs.getFloat("orientX"));
					orient.setY(rs.getFloat("orientY"));
					orient.setZ(rs.getFloat("orientZ"));
					orient.setW(rs.getFloat("orientW"));
					region.addShape(shapeType, loc, loc2, orient, rs.getFloat("size1"), rs.getFloat("size2"), rs.getFloat("size3"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	public HashMap<Integer, InteractiveObject> loadInteractiveObjects(int instanceID, OID instanceOid) {
		Log.debug("INTERACTIVE: loading interactive objects for instance: " + instanceID);
		HashMap<Integer, InteractiveObject> nodes = new HashMap<Integer, InteractiveObject>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `interactive_object` where instance=? and isactive = 1");
			ps.setInt(1, instanceID);
			Log.debug("GRID: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					AOVector p = new AOVector();
					p.setX(rs.getFloat("locX"));
					p.setY(rs.getFloat("locY"));
					p.setZ(rs.getFloat("locZ"));
					InteractiveObject obj = new InteractiveObject(id,p, instanceOid);
					obj.setName(HelperFunctions.readEncodedString(rs.getBytes("name")));
					obj.setGameObject(HelperFunctions.readEncodedString(rs.getBytes("gameObject")));
					obj.setCoordEffect(HelperFunctions.readEncodedString(rs.getBytes("coordEffect")));
					obj.setQuestIDReq(rs.getInt("questReqID"));
					obj.setInteractionType(HelperFunctions.readEncodedString(rs.getBytes("interactionType")));
					obj.setInteractionID(rs.getInt("interactionID"));
					obj.setInteractionData1(HelperFunctions.readEncodedString(rs.getBytes("interactionData1")));
					obj.setInteractionData2(HelperFunctions.readEncodedString(rs.getBytes("interactionData2")));
					obj.setInteractionData3(HelperFunctions.readEncodedString(rs.getBytes("interactionData3")));
					obj.setRespawnTime(rs.getInt("respawnTime"));
					obj.setHarvestTimeReq(rs.getFloat("interactTimeReq"));
					
					nodes.put(obj.getID(), obj);
					Log.debug("INTERACTIVE: added node " + obj.getID() + " to map");
					obj.spawn();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return nodes;
	}
	
	public HashMap<Integer, ResourceNode> loadResourceNodes(int instanceID, OID instanceOid, 
			HashMap<OID, HashMap<String, ResourceNodeGroup>> resourceNodeGroups) {
		HashMap<Integer, List<Integer[]>> resourceDrops = loadResourceDrops();
		HashMap<Integer, ResourceNode> nodes = new HashMap<Integer, ResourceNode>();
		HashMap<String, ResourceNodeGroup> nodeGroupMap = new HashMap<String, ResourceNodeGroup>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `resource_node_template` where instance=?");
			ps.setInt(1, instanceID);
			Log.debug("GRID: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					AOVector p = new AOVector();
					p.setX(rs.getFloat("locX"));
					p.setY(rs.getFloat("locY"));
					p.setZ(rs.getFloat("locZ"));
					ResourceNode resourceNode = new ResourceNode(id,p, instanceOid);
					resourceNode.setName(HelperFunctions.readEncodedString(rs.getBytes("name")));
					resourceNode.setGameObject(HelperFunctions.readEncodedString(rs.getBytes("gameObject")));
					resourceNode.setCoordEffect(HelperFunctions.readEncodedString(rs.getBytes("coordEffect")));
					resourceNode.setSkill(rs.getInt("skill"));
					resourceNode.setSkillLevelReq(rs.getInt("skillLevel"));
					resourceNode.setSkillLevelMax(rs.getInt("skillLevelMax"));
					resourceNode.setWeaponReq(HelperFunctions.readEncodedString(rs.getBytes("weaponReq")));
					resourceNode.setEquippedReq(rs.getBoolean("equipped"));
					resourceNode.setRespawnTime(rs.getInt("respawnTime"));
					resourceNode.setHarvestCount(rs.getInt("harvestCount"));
					resourceNode.setHarvestTimeReq(rs.getFloat("harvestTimeReq"));
					if (resourceDrops.containsKey(id)) {
						for(Integer[] data : resourceDrops.get(id)) {
							resourceNode.AddResourceDrop(data[0], data[1], data[2], data[3]);
						}
					}
					//loadResourceDrops(resourceNode);
					
					nodes.put(resourceNode.getID(), resourceNode);
					Log.debug("RESOURCE: added node " + resourceNode.getID() + " to map");
					if (CraftingPlugin.USE_RESOURCE_GROUPS) {
						// Round position to nearest half of group size
						int locX = ((int)p.getX()+24)/CraftingPlugin.RESOURCE_GROUP_SIZE * CraftingPlugin.RESOURCE_GROUP_SIZE;
						int locZ = ((int)p.getZ()+24)/CraftingPlugin.RESOURCE_GROUP_SIZE * CraftingPlugin.RESOURCE_GROUP_SIZE;
						AOVector newLoc = new AOVector(locX, 0, locZ);
						// See if a HashMap exists for this rounded position
						if (!nodeGroupMap.containsKey(newLoc.toString())) {
							Log.debug("RESOURCE: creating new node group at loc " + newLoc + " to map");
							// If not, create group
							ResourceNodeGroup nodeGroup = new ResourceNodeGroup(newLoc, instanceOid);
							// Add node to node group
							nodeGroup.AddResourceNode(resourceNode);
							nodeGroupMap.put(newLoc.toString(), nodeGroup);
						} else {
							// Add node to node group
							nodeGroupMap.get(newLoc.toString()).AddResourceNode(resourceNode);
							Log.debug("RESOURCE: adding node to node group at loc " + newLoc);
						}
					} else {
						resourceNode.spawn();
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		if (CraftingPlugin.USE_RESOURCE_GROUPS) {
			for (ResourceNodeGroup group : nodeGroupMap.values()) {
				group.spawn(instanceOid);
			}
			resourceNodeGroups.put(instanceOid, nodeGroupMap);
		}
		
		return nodes;
	}
	
	/*public void loadResourceDrops(ResourceNode resourceNode) {
		try {
			ps = queries.prepare("SELECT * FROM `resource_drop` where resource_template = " + resourceNode.getID());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int item = rs.getInt("item");
					int min = rs.getInt("min");
					int max = rs.getInt("max");
					int chance = rs.getInt("chance");
					if (max < min)
						max = min;
					if (min > 0)
						resourceNode.AddResourceDrop(item, min, max, chance);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}*/
	
	public HashMap<Integer, List<Integer[]>> loadResourceDrops() {
		HashMap<Integer, List<Integer[]>> resourceDrops = new HashMap<Integer, List<Integer[]>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `resource_drop`");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Integer[] data = new Integer[4];
					int resource_template = rs.getInt("resource_template");
					int item = rs.getInt("item");
					int min = rs.getInt("min");
					int max = rs.getInt("max");
					int chance = rs.getInt("chance");
					if (max < min)
						max = min;
					if (min > 0) {
						data[0] = item;
						data[1] = min;
						data[2] = max;
						data[3] = chance;
						if (resourceDrops.containsKey(resource_template)) {
							ArrayList<Integer[]> list = (ArrayList<Integer[]>) resourceDrops.get(resource_template);
							list.add(data);
							resourceDrops.put(resource_template, list);
						} else {
							ArrayList<Integer[]> list = new ArrayList<Integer[]>();
							list.add(data);
							resourceDrops.put(resource_template, list);
						}
						
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return resourceDrops;
	}
	
	/**
	 * Building System Database Functions
	 */
	
	public HashMap<Integer, BuildObjectTemplate> loadBuildObjectTemplates() {
		// Load in build stages first
		HashMap<Integer, BuildObjectStage> stages = loadBuildObjectStages();
		HashMap<Integer, BuildObjectTemplate> grids = new HashMap<Integer, BuildObjectTemplate>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `build_object_template` where isactive = 1");
			Log.debug("BUILD: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					BuildObjectTemplate bg = new BuildObjectTemplate(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")),
							rs.getInt("skill"), rs.getInt("skillLevelReq"), HelperFunctions.readEncodedString(rs.getBytes("weaponReq")),
							rs.getFloat("distanceReq"));
					// Set stages
					int stageID = rs.getInt("firstStageID");
					while (stageID > 0) {
						if (stages.containsKey(stageID)) {
							bg.addStage(stages.get(stageID));
							stageID = stages.get(stageID).getNextStageID();
						} else {
							stageID = -1;
						}	
					}
					
					// Load in items and get their health property and add to the total health for the object
					int health = 0;
					for (int i = 1; i < bg.getStages().size(); i++) {
						health += bg.getStage(i).getHealth();
					}
					bg.setMaxHealth(health);
					grids.put(bg.getId(), bg);
					//Log.debug("GRID: added grid " + bg.getID() + " to map");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return grids;
	}
	
	public HashMap<Integer, BuildObjectStage> loadBuildObjectStages() {
		HashMap<Integer, BuildObjectStage> stages = new HashMap<Integer, BuildObjectStage>();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `build_object_stage` where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int health = 0;
					HashMap<Integer, Integer> itemReqs = new HashMap<Integer, Integer>();
					for (int i = 1; i <= 6; i++) {
						Integer itemID = rs.getInt("itemReq" + i);
						Integer itemCount = rs.getInt("itemReq" + i + "Count");
						if (itemID != null && itemID > 0 && itemCount != null && itemCount > 0) {
							itemReqs.put(itemID, itemCount);
							health += getItemHealthValue(itemID) * itemCount;
						}
					}
					BuildObjectStage bg = new BuildObjectStage(HelperFunctions.readEncodedString(rs.getBytes("gameObject")),
							rs.getFloat("buildTimeReq"), itemReqs, health, rs.getInt("nextStage"));
					// Load in items and get their health property and add to the total health for the object
					stages.put(rs.getInt("id"), bg);
					//Log.debug("GRID: added grid " + bg.getID() + " to map");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return stages;
	}
	
	public int getItemHealthValue(int itemID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM " + ItemDatabase.ITEM_TABLE + " where id = " + itemID);
			Log.debug("BUILD: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					for (int i = 1; i <= 12; i++) {
						String itemEffectType = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "type"));
						if (itemEffectType != null && itemEffectType.equals("BuildingMaterial")) {
							String itemEffectValue = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							if (itemEffectValue != null && !itemEffectValue.equals("")) {
								return Integer.parseInt(itemEffectValue);
							}
						}
						
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		return 0;
	}
	
	public HashMap<Integer, Claim> loadClaims(int instanceID) {
		HashMap<Integer, Claim> claims = new HashMap<Integer, Claim>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `claim` where instance=?");
			ps.setInt(1, instanceID);
			Log.debug("GRID: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Claim bg = new Claim();
					bg.setID(rs.getInt("id"));
					bg.setName(HelperFunctions.readEncodedString(rs.getBytes("name")));
					AOVector p = new AOVector();
					p.setX(rs.getFloat("locX"));
					p.setY(rs.getFloat("locY"));
					p.setZ(rs.getFloat("locZ"));
					bg.setLoc(p);
					bg.setSize(rs.getInt("size"));
					Long oid = rs.getLong("owner");
					if (oid == null || oid == -1l) {
						bg.setOwner(null);
					} else {
						bg.setOwner((OID.fromLong(oid)));
					}
					bg.setForSale(rs.getBoolean("forSale"));
					bg.setCost(rs.getInt("cost"));
					bg.setCurrency(rs.getInt("currency"));
					bg.setSellerName(rs.getString("sellerName"));
					bg.setClaimItemTemplate(rs.getInt("claimItemTemplate"));
					bg.setInstanceID(instanceID);
					bg.setContentDatabase(this);
					claims.put(bg.getID(), bg);
					//Log.debug("GRID: added grid " + bg.getID() + " to map");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for(Claim bg : claims.values()) {
			loadClaimActions(bg);
			loadClaimObjects(bg);
			//loadClaimResources(bg);
			loadClaimPermissions(bg);
		}
		return claims;
	}
	
	public void loadClaimActions(Claim claim) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `claim_action` where claimID = " + claim.getID());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					String action = HelperFunctions.readEncodedString(rs.getBytes("action"));
					String brushType = HelperFunctions.readEncodedString(rs.getBytes("brushType"));
					float locX = rs.getFloat("locX");
					float locY = rs.getFloat("locY");
					float locZ = rs.getFloat("locZ");
					float normalX = rs.getFloat("normalX");
					float normalY = rs.getFloat("normalY");
					float normalZ = rs.getFloat("normalZ");
					int material = rs.getShort("material");
					int sizeX = rs.getInt("sizeX");
					int sizeY = rs.getInt("sizeY");
					int sizeZ = rs.getInt("sizeZ");
					claim.AddActionData(id, action, brushType, new AOVector(sizeX, sizeY, sizeZ), new AOVector(locX, locY, locZ), new AOVector(normalX, normalY, normalZ), material);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public void loadClaimObjects(Claim claim) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `claim_object` where claimID = " + claim.getID());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					int templateId = rs.getInt("template");
					int stage = rs.getInt("stage");
					String gameObject = HelperFunctions.readEncodedString(rs.getBytes("gameObject"));
					float locX = rs.getFloat("locX");
					float locY = rs.getFloat("locY");
					float locZ = rs.getFloat("locZ");
					float orientX = rs.getFloat("orientX");
					float orientY = rs.getFloat("orientY");
					float orientZ = rs.getFloat("orientZ");
					float orientW = rs.getFloat("orientW");
					int itemID = rs.getShort("itemID");
					String state = HelperFunctions.readEncodedString(rs.getBytes("objectState"));
					boolean complete = rs.getBoolean("complete");
					int health = rs.getInt("health");
					int maxHealth = rs.getInt("maxHealth");
					HashMap<Integer, Integer> itemCounts = new HashMap<Integer, Integer>();
					for (int i = 1; i <= 6; i++) {
						Integer itemId = rs.getInt("item"+i);
						Integer itemCount = rs.getInt("item" + i + "Count");
						if (itemId != null && itemId > 0 && itemCount != null && itemCount > 0) {
							itemCounts.put(itemId, itemCount);
						}
					}
					claim.AddClaimObject(id, templateId, stage, complete, gameObject, new AOVector(locX, locY, locZ), 
							new Quaternion(orientX, orientY, orientZ, orientW), itemID, state, health, maxHealth, itemCounts);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public void loadClaimResources(Claim claim) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `claim_resource` where claimID = " + claim.getID());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					int itemID = rs.getInt("itemID");
					int count = rs.getInt("count");
					claim.AddClaimResource(id, itemID, count);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public void loadClaimPermissions(Claim claim) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `claim_permission` where claimID = " + claim.getID());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					OID playerOid = OID.fromLong(rs.getLong("playerOid"));
					String playerName = HelperFunctions.readEncodedString(rs.getBytes("playerName"));
					int permissionLevel = rs.getInt("permissionLevel");
					claim.AddClaimPermission(playerOid, playerName, permissionLevel);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public int writeClaim(Claim claim, int instanceID) {
		Log.debug("Writing claim data to database");
		// Set up the displays
		int inserted = -1;
		String tableName = "claim";
		String columnNames = "instance,locX,locY,locZ,owner,size,forSale,cost,currency,sellerName,name,claimItemTemplate,priority";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, instanceID);
			stmt.setFloat(2, claim.getLoc().getX());
			stmt.setFloat(3, claim.getLoc().getY());
			stmt.setFloat(4, claim.getLoc().getZ());
			if (claim.getOwner() != null) {
				stmt.setLong(5, claim.getOwner().toLong());
			} else {
				stmt.setLong(5, 0);
			}
			stmt.setInt(6, claim.getSize());
			stmt.setBoolean(7, claim.getForSale());
			stmt.setInt(8, claim.getCost());
			stmt.setInt(9, claim.getCurrency());
			stmt.setString(10, claim.getSellerName());
			stmt.setString(11, claim.getName());
			stmt.setInt(12, claim.getClaimItemTemplate());
			stmt.setInt(13, claim.getPriority());
			inserted = queries.executeInsert(stmt);
			claim.setID(inserted);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return inserted;
	}
	
	public int updateClaim(Claim claim) {
		Log.debug("Updating claim data to database");
		String tableName = "claim";
		int updated;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set name=?, owner=?, forSale=?, "
					+ "cost=?, currency=?, sellerName=? where id=?");
			stmt.setString(1, claim.getName());
			stmt.setLong(2, claim.getOwner().toLong());
			stmt.setBoolean(3, claim.getForSale());
			stmt.setInt(4, claim.getCost());
			stmt.setInt(5, claim.getCurrency());
			stmt.setString(6, claim.getSellerName());
			stmt.setInt(7, claim.getID());
			Log.debug("CONTENTDB: updating claim with statement: " + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return updated;
	}
	
	public int deleteClaim(int claimID) {
		String tableName = "claim";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE id = " + claimID;
		int deleted = queries.executeUpdate(deleteString);
		// Also delete all associated actions and objects
		tableName = "claim_action";
		deleteString = "DELETE FROM `" + tableName + "` WHERE claimID = " + claimID;
		deleted = queries.executeUpdate(deleteString);
		tableName = "claim_object";
		deleteString = "DELETE FROM `" + tableName + "` WHERE claimID = " + claimID;
		deleted = queries.executeUpdate(deleteString);
		return deleted;
	}
	
	public int writeClaimAction(int claimID, String action, String brushType, AOVector size, AOVector loc, AOVector normal, int material) {
		Log.debug("Writing claim data to database");
		int inserted = -1;
		String tableName = "claim_action";
		String columnNames = "claimID,action,brushType,locX,locY,locZ,material,normalX,normalY,normalZ,sizeX,sizeY,sizeZ";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, claimID);
			stmt.setString(2, action);
			stmt.setString(3, brushType);
			stmt.setFloat(4, loc.getX());
			stmt.setFloat(5, loc.getY());
			stmt.setFloat(6, loc.getZ());
			stmt.setInt(7, material);
			stmt.setFloat(8, normal.getX());
			stmt.setFloat(9, normal.getY());
			stmt.setFloat(10, normal.getZ());
			stmt.setInt(11, (int)size.getX());
			stmt.setInt(12, (int)size.getY());
			stmt.setInt(13, (int)size.getZ());
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return inserted;
	}
	
	public void deleteClaimAction(int spawnID) {
		String tableName = "claim_action";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE id = " + spawnID;
		queries.executeUpdate(deleteString);
	}
	
	public int writeClaimObject(int claimID, int templateId, int stage, boolean complete, String gameObject, AOVector loc, 
			Quaternion orient, int itemID, String state, int health, int maxHealth, HashMap<Integer, Integer> itemCounts) {
		Log.debug("Writing claim data to database");
		int inserted = -1;
		String tableName = "claim_object";
		String columnNames = "claimID,template,stage,complete,gameObject,locX,locY,locZ,orientX,orientY,orientZ,orientW,itemID,objectState,"
				+ "health,maxHealth,item1,item1Count,item2,item2Count,item3,item3Count,item4,item4Count,item5,item5Count,item6,item6Count";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, claimID);
			stmt.setInt(2, templateId);
			stmt.setInt(3, stage);
			stmt.setBoolean(4, complete);
			stmt.setString(5, gameObject);
			stmt.setFloat(6, loc.getX());
			stmt.setFloat(7, loc.getY());
			stmt.setFloat(8, loc.getZ());
			stmt.setFloat(9, orient.getX());
			stmt.setFloat(10, orient.getY());
			stmt.setFloat(11, orient.getZ());
			stmt.setFloat(12, orient.getW());
			stmt.setInt(13, itemID);
			stmt.setString(14, state);
			stmt.setInt(15, health);
			stmt.setInt(16, maxHealth);
			int parameterIndex = 17;
			for (int item : itemCounts.keySet()) {
				stmt.setInt(parameterIndex, item);
				parameterIndex++;
				stmt.setInt(parameterIndex, itemCounts.get(item));
				parameterIndex++;
			}
			// Fill in the missing slots
			for (int i = itemCounts.size(); i < 6; i++) {
				stmt.setInt(parameterIndex, -1);
				parameterIndex++;
				stmt.setInt(parameterIndex, -1);
				parameterIndex++;
			}
			
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return inserted;
	}
	
	public int updateClaimObjectPosition(int id, AOVector loc, Quaternion orient) {
		Log.debug("Updating claim object in database");
		String tableName = "claim_object";
		int updated;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set locX=?,locY=?,locZ=?,orientX=?,orientY=?,orientZ=?,orientW=? where id=?");
			stmt.setFloat(1, loc.getX());
			stmt.setFloat(2, loc.getY());
			stmt.setFloat(3, loc.getZ());
			stmt.setFloat(4, orient.getX());
			stmt.setFloat(5, orient.getY());
			stmt.setFloat(6, orient.getZ());
			stmt.setFloat(7, orient.getW());
			stmt.setInt(8, id);
			Log.debug("CONTENTDB: updating claim resource with statement: " + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return updated;
	}
	
	public int updateClaimObjectState(int id, int templateID, int stage, boolean complete, String state, String gameObject, 
			int health, int maxHealth, HashMap<Integer, Integer> itemCounts) {
		Log.debug("Updating claim object state in database");
		String tableName = "claim_object";
		int updated;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set template=?, stage=?, complete=?, objectState=?, "
					+ "gameObject=?, health=?, maxHealth=?, item1=?, item1Count=?, item2=?, item2Count=?, item3=?, item3Count=?, "
					+ "item4=?, item4Count=?, item5=?, item5Count=?, item6=?, item6Count=? where id=?");
			stmt.setInt(1, templateID);
			stmt.setInt(2, stage);
			stmt.setBoolean(3, complete);
			stmt.setString(4, state);
			stmt.setString(5, gameObject);
			stmt.setInt(6, health);
			stmt.setInt(7, maxHealth);
			int parameterIndex = 8;
			for (int item : itemCounts.keySet()) {
				stmt.setInt(parameterIndex, item);
				parameterIndex++;
				stmt.setInt(parameterIndex, itemCounts.get(item));
				parameterIndex++;
			}
			// Fill in the missing slots
			for (int i = itemCounts.size(); i < 6; i++) {
				stmt.setInt(parameterIndex, -1);
				parameterIndex++;
				stmt.setInt(parameterIndex, -1);
				parameterIndex++;
			}
			stmt.setInt(20, id);
			
			Log.debug("CONTENTDB: updating claim object with statement: " + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return updated;
	}
	
	public void deleteClaimObject(int objectID) {
		String tableName = "claim_object";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE id = " + objectID;
		queries.executeUpdate(deleteString);
	}
	
	public int writeClaimResource(int claimID, int itemID, int count) {
		Log.debug("Writing claim data to database");
		int inserted = -1;
		String tableName = "claim_resource";
		String columnNames = "claimID,itemID,count";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?)");
			stmt.setInt(1, claimID);
			stmt.setInt(2, itemID);
			stmt.setInt(3, count);
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return inserted;
	}
	
	public int updateClaimResource(int id, int itemID, int count) {
		Log.debug("Writing claim data to database");
		String tableName = "claim_resource";
		int updated;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set count=? where id=?");
			stmt.setInt(1, count);
			stmt.setInt(2, id);
			Log.debug("CONTENTDB: updating claim resource with statement: " + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return updated;
	}
	
	public int writeClaimPermission(int claimID, OID playerOid, String playerName, int permissionLevel) {
		Log.debug("Writing claim data to database");
		int inserted = -1;
		String tableName = "claim_permission";
		String columnNames = "claimID,playerOid,playerName,permissionLevel";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?, ?)");
			stmt.setInt(1, claimID);
			stmt.setLong(2, playerOid.toLong());
			stmt.setString(3, playerName);
			stmt.setInt(4, permissionLevel);
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return inserted;
	}
	
	public int updateClaimPermission(int claimID, OID playerOid, int permissionLevel) {
		Log.debug("Writing claim data to database");
		String tableName = "claim_permission";
		int updated;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set permissionLevel=? where claimID=? AND playerOid=?");
			stmt.setInt(1, permissionLevel);
			stmt.setInt(2, claimID);
			stmt.setLong(3, playerOid.toLong());
			Log.debug("CONTENTDB: updating claim resource with statement: " + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote claim data to database");
		return updated;
	}
	
	public void deleteClaimPermission(int claimID, OID playerOid) {
		String tableName = "claim_permission";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE claimID = " + claimID + " AND playerOid = " + playerOid.toLong();
		queries.executeUpdate(deleteString);
	}
	
	/**
	 * Having too many connection errors, so adding this function to help cope with it
	 */
	public void close() {
		queries.close();
	}
	
	HashMap<Integer, EditorOptionMapping> editor_options = new HashMap<Integer, EditorOptionMapping>();
}