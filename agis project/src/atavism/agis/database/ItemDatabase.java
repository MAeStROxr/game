package atavism.agis.database;

import atavism.agis.core.*;
import atavism.agis.objects.*;
import atavism.agis.plugins.CraftingPlugin;
import atavism.agis.util.HelperFunctions;
import atavism.server.objects.Template;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.ObjectManagerPlugin;
import atavism.server.util.Log;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Contains functions for reading and writing item related content information to/from
 * the content database.
 * @author Andrew Harrison
 *
 */
public class ItemDatabase {
	protected static Queries queries;
	
	public ItemDatabase(boolean keepAlive) {
		if (queries == null)
			queries = new Queries(keepAlive);
	}

	/**
	 * Loads in the item templates from the content database.
	 * @return
	 */
	public ArrayList<Template> loadItemTemplates() {
		Log.debug("ITEM: loading item templates");
		ArrayList<Template> list = new ArrayList<Template>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM " + ITEM_TABLE + " where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Template tmpl = new Template(HelperFunctions.readEncodedString(rs.getBytes("name")), rs.getInt("id"), ObjectManagerPlugin.ITEM_TEMPLATE);
					Log.debug("ITEM: loading item template " + rs.getInt("id"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "baseName", HelperFunctions.readEncodedString(rs.getBytes("name")));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "category", HelperFunctions.readEncodedString(rs.getBytes("category")));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "subcategory", HelperFunctions.readEncodedString(rs.getBytes("subcategory")));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "itemID", rs.getInt("id"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ICON, HelperFunctions.readEncodedString(rs.getBytes("icon")));
					String itemType = HelperFunctions.readEncodedString(rs.getBytes("itemType"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "itemType", itemType);
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "subType", HelperFunctions.readEncodedString(rs.getBytes("subType")));
					if (itemType.equals("Weapon") || itemType.equals("Armor")) {
						AgisEquipInfo eqInfo = getEquipInfo(HelperFunctions.readEncodedString(rs.getBytes("slot")));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "slot", HelperFunctions.readEncodedString(rs.getBytes("slot")));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_EQUIP_INFO, eqInfo);
						tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new EquipActivateHook());
						String displayID = HelperFunctions.readEncodedString(rs.getBytes("display"));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "displayVal", displayID);
					}
					
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "itemGrade", rs.getInt("itemQuality"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "binding", rs.getInt("binding"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "isUnique", rs.getBoolean("isUnique"));
					if (itemType.equals("Bag") || itemType.equals("Container")) {
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "numSlots", rs.getInt("stackLimit"));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "stackLimit", 1);
					} else {
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "stackLimit", rs.getInt("stackLimit"));
					}
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "purchaseCurrency", rs.getInt("purchaseCurrency"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "purchaseCost", rs.getInt("purchaseCost"));
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "sellable", rs.getBoolean("sellable"));
					
					HashMap<String, Integer> itemStats =  new HashMap<String, Integer>();
					// Item Effects
					// Add code here to handle new Item Effect types you have added
					for (int i = 1; i <= 12; i++) {
						String effectType = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "type"));
						if (effectType == null || effectType.equals("")) {
							break;
						} else if (effectType.equals("Stat")) {
							// Stats can only exist on weapons and armor
							if (itemType.equals("Weapon") || itemType.equals("Armor")) {
								String stat = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "name"));
								String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
								if (stat != null && !stat.isEmpty()) {
									itemStats.put(stat, Integer.parseInt(value));
								}
							}
						} else if (effectType.equals("UseAbility")) {
							Log.debug("USEABILITY: got useAbility Item Effect");
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int abilityID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, "abilityID", abilityID);
						    tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new AbilityActivateHook(abilityID));
						    Log.debug("USEABILITY: added Item Effect: " + abilityID);
						    // We also need to get the cooldown(s) for this associated ability
						    /*LinkedList<String> cooldowns = getAbilityCooldowns(abilityID);
						    for (int j = 0; j < cooldowns.size(); j++) {
						    	tmpl.put(InventoryClient.ITEM_NAMESPACE, "cooldown_" + j, cooldowns.get(j));
						    }*/
						} else if (effectType.equals("AutoAttack")) {
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int abilityID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, "autoAttack", abilityID);
						} else if (effectType.equals("CreateClaim")) {
							// Used to turn an item into a Building Resource when acquired
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int size = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new CreateClaimActivateHook(size));
						} else if (effectType.equals("Currency")) {
							// Used to turn an item into a Currency when activated
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int currencyID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.TEMPL_ACQUIRE_HOOK, new CurrencyItemAcquireHook(currencyID));
						} else if (effectType.equals("CurrencyItem")) {
							// Used to turn an item into a Currency when activated
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int currencyID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new CurrencyItemActivateHook(currencyID));
						} else if (effectType.equals("BuildingMaterial")) {
							// Used to turn an item into a Building Resource when acquired
							//String value = rs.getString("effect" + i + "value");
							//int resourceID = Integer.parseInt(value);
							//tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.TEMPL_ACQUIRE_HOOK, new BuildingResourceAcquireHook(rs.getInt("id")));
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int buildHealthValue = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, "buildHealthValue", buildHealthValue);
						} else if (effectType.equals("Blueprint")) {
							// Used to turn an item into a recipe when acquired
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int recipeID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new RecipeItemActivateHook(recipeID));
						} else if (effectType.equals("StartQuest")) {
							// Starts a quest for the player when the item is activated
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int questID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new QuestStartItemActivateHook(questID));
						} else if (effectType.equals("SpawnMob")) {
							// Used to turn an item into a Currency when activated
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int mobTemplateID = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.TEMPL_ACQUIRE_HOOK, new SpawnMobAcquireHook(mobTemplateID));
						} else if (effectType.equals("UseAmmo")) {
							String ammoType = rs.getString("effect" + i + "name");
							//String value = rs.getString("effect" + i + "value");
							int ammoTypeID = Integer.parseInt(ammoType);
							//int capacity = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.AMMO_TYPE, ammoTypeID);
							//Log.debug("AMMO: set UseAmmo type to: " + ammoTypeID);
							/*tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.AMMO_CAPACITY, capacity);
							if (ammoTypeID < 1) {
								tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.AMMO_LOADED, capacity);
							} else {
								tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.AMMO_LOADED, 0);
							}*/
						} else if (effectType.equals("Durability")) {
							// Gives an item a durability amount
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int durability = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, "durability", durability);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, "maxDurability", durability);
						} else if (effectType.equals("Weight")) {
							// Gives an item a weight value
							String value = HelperFunctions.readEncodedString(rs.getBytes("effect" + i + "value"));
							int weight = Integer.parseInt(value);
							tmpl.put(InventoryClient.ITEM_NAMESPACE, "weight", weight);
							Log.debug("ITEM: added weight property with value: " + weight);
						}
					}
					
					tmpl.put(InventoryClient.ITEM_NAMESPACE, "bonusStats", itemStats);
					
					// Weapon damage
					if (itemType.equals("Weapon")) {
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "damage", rs.getInt("damage"));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "damageType", HelperFunctions.readEncodedString(rs.getBytes("damageType")));
						float delay = rs.getFloat("delay") * 1000;
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "delay", (int)delay);
					} else if (itemType.equals("Ammo")) {
						// Set ammo activate hook
						String value = HelperFunctions.readEncodedString(rs.getBytes("slot"));
						int ammoTypeID = Integer.parseInt(value);
						//TODO: read in ammo effect
						tmpl.put(InventoryClient.ITEM_NAMESPACE, InventoryClient.TEMPL_ACTIVATE_HOOK, new AmmoItemActivateHook(ammoTypeID));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, "damage", rs.getInt("damage"));
						tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.AMMO_TYPE, ammoTypeID);
						tmpl.put(InventoryClient.ITEM_NAMESPACE, AgisItem.TEMPL_UNACQUIRE_HOOK, new AmmoItemUnacquireHook());
					}
					//tmpl.put(InventoryClient.ITEM_NAMESPACE, "tooltip", rs.getString("tooltip"));
					list.add(tmpl);
					Log.debug("ITEM: loaded item template " + rs.getInt("id") + " with name: " + HelperFunctions.readEncodedString(rs.getBytes("name")));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for (Template tmpl : list) {
			// Load in requirements
			tmpl.put(InventoryClient.ITEM_NAMESPACE, "requirements", getItemRequirements(tmpl.getTemplateID()));
		}
		return list;
	}
	
	private HashMap<Integer, HashMap<String, Integer>> getItemRequirements(int itemID) {
		HashMap<Integer, HashMap<String, Integer>> requirementMap = new HashMap<Integer, HashMap<String, Integer>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM item_templates_options where item_id=" + itemID + " AND isactive=1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int requirementType = rs.getInt("editor_option_type_id");
					String requirementOption = HelperFunctions.readEncodedString(rs.getBytes("editor_option_choice_type_id"));
					int requiredValue = rs.getInt("required_value");
					if (requirementMap.containsKey(requirementType)) {
						HashMap<String, Integer> requirementOptions = requirementMap.get(requirementType);
						requirementOptions.put(requirementOption, requiredValue);
						requirementMap.put(requirementType, requirementOptions);
					} else {
						HashMap<String, Integer> requirementOptions = new HashMap<String, Integer>();
						requirementOptions.put(requirementOption, requiredValue);
						requirementMap.put(requirementType, requirementOptions);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return requirementMap;
	}
	
	private LinkedList<String> getAbilityCooldowns(int abilityID) {
		LinkedList<String> list = new LinkedList<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT cooldown1Type FROM abilities where id=" + abilityID);
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("COOLDOWN: Reading in cooldown data for ability: " + abilityID);
					String cooldown = HelperFunctions.readEncodedString(rs.getBytes("cooldown1Type"));
					if (cooldown != null && !cooldown.equals(""))
						list.add(cooldown);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		Log.debug("COOLDOWN: " + abilityID + " has cooldowns: " + list);
		return list;
	}
	
	private AgisEquipInfo getEquipInfo(String slot) {
		AgisEquipInfo eqInfo = new AgisEquipInfo();
		if (slot.equals("Main Hand") || slot.equals("Two Hand")) {
			eqInfo.setName("weapon");
			eqInfo.addEquipSlot(AgisEquipSlot.PRIMARYWEAPON);
		} else if (slot.equals("Off Hand")) {
			eqInfo.setName("weapon");
			eqInfo.addEquipSlot(AgisEquipSlot.SECONDARYWEAPON);
		} else if (slot.equals("Any Hand")) {
			//TODO: figure out a way to figure out which slot to put the weapon in?
			eqInfo.setName("weapon");
			eqInfo.addEquipSlot(AgisEquipSlot.PRIMARYWEAPON);
			eqInfo.addEquipSlot(AgisEquipSlot.SECONDARYWEAPON);
		} else if (slot.equals("Head")) {
			eqInfo.setName("head");
			eqInfo.addEquipSlot(AgisEquipSlot.HEAD);
		} else if (slot.equals("Shoulder")) {
			eqInfo.setName("shoulder");
			eqInfo.addEquipSlot(AgisEquipSlot.SHOULDER);
		} else if (slot.equals("Chest")) {
			eqInfo.setName("chest");
			eqInfo.addEquipSlot(AgisEquipSlot.CHEST);
		} else if (slot.equals("Hands")) {
			eqInfo.setName("hands");
			eqInfo.addEquipSlot(AgisEquipSlot.HANDS);
		} else if (slot.equals("Waist")) {
			eqInfo.setName("belt");
			eqInfo.addEquipSlot(AgisEquipSlot.BELT);
		} else if (slot.equals("Legs")) {
			eqInfo.setName("legs");
			eqInfo.addEquipSlot(AgisEquipSlot.LEGS);
		} else if (slot.equals("Feet")) {
			eqInfo.setName("feet");
			eqInfo.addEquipSlot(AgisEquipSlot.FEET);
		} else if (slot.equals("Back")) {
			eqInfo.setName("back");
			eqInfo.addEquipSlot(AgisEquipSlot.BACK);
		} else if (slot.equals("Neck")) {
			eqInfo.setName("neck");
			eqInfo.addEquipSlot(AgisEquipSlot.NECK);
		} else if (slot.equals("Ring")) {
			eqInfo.setName("ring");
			eqInfo.addEquipSlot(AgisEquipSlot.PRIMARYRING);
			eqInfo.addEquipSlot(AgisEquipSlot.SECONDARYRING);
		} else if (slot.equals("Shirt")) {
			eqInfo.setName("shirt");
			eqInfo.addEquipSlot(AgisEquipSlot.SHIRT);
		}
		
		return eqInfo;
	}
	
	public HashMap<Integer, MerchantTable> loadMerchantTables() {
		HashMap<Integer, MerchantTable> map = new HashMap<Integer, MerchantTable>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM merchant_tables where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int tableID = rs.getInt("id");
				    MerchantTable m = new MerchantTable(tableID, HelperFunctions.readEncodedString(rs.getBytes("name")));
				    loadMerchantItems(m);
				    map.put(tableID, m);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return map;
	}
	
	public void loadMerchantItems(MerchantTable mTbl) {
		ArrayList<Integer> items = new ArrayList<Integer>();
		ArrayList<Integer> counts = new ArrayList<Integer>();
		ArrayList<Integer> refreshTimes = new ArrayList<Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM merchant_item where tableID=" + mTbl.getID() + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					items.add(rs.getInt("itemID"));
					counts.add(rs.getInt("count"));
					refreshTimes.add(rs.getInt("refreshTime"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		mTbl.setItems(items);
		mTbl.setItemCounts(counts);
		mTbl.setItemRespawns(refreshTimes);
	}
	
	public HashMap<Integer, CraftingRecipe> loadCraftingRecipes() {
		HashMap<Integer, CraftingRecipe> list = new HashMap<Integer, CraftingRecipe>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM crafting_recipes where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("CRAFTING: loading recipe:" + rs.getInt("id"));
					CraftingRecipe recipe = new CraftingRecipe(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
					ArrayList<Integer> resultItems = new ArrayList<Integer>();
					ArrayList<Integer> resultItemCounts = new ArrayList<Integer>();
					resultItems.add(rs.getInt("resultItemID"));
					resultItemCounts.add(rs.getInt("resultItemCount"));
					for (int i = 2; i <= 4; i++) {
						Integer resultItem = rs.getInt("resultItem" + i + "ID");
						if (resultItem != null && resultItem > 0) {
							resultItems.add(resultItem);
							resultItemCounts.add(rs.getInt("resultItem" + i + "Count"));
						}
					}
					recipe.setResultItemIds(resultItems);
					recipe.setResultItemCounts(resultItemCounts);
					recipe.setSkillID(rs.getInt("skillID"));
					recipe.setRequiredSkillLevel(rs.getInt("skillLevelReq"));
					recipe.setRecipeItemId(rs.getInt("recipeItemID"));
					recipe.setQualityChangeable(rs.getBoolean("qualityChangeable"));
					recipe.setAllowDyes(rs.getBoolean("allowDyes"));
					recipe.setAllowEssences(rs.getBoolean("allowEssences"));
					recipe.setStationReq(HelperFunctions.readEncodedString(rs.getBytes("stationReq")));
					recipe.setCreationTime(rs.getInt("creationTime"));
					recipe.setMustMatchLayout(rs.getBoolean("layoutReq"));
					for (int i = 0; i < CraftingPlugin.GRID_SIZE; i++) {
						LinkedList<CraftingComponent> componentRow = new LinkedList<CraftingComponent>();
						for (int j = 0; j < CraftingPlugin.GRID_SIZE; j++) {
							CraftingComponent component = new CraftingComponent("", 
									rs.getInt("component" + (i*CraftingPlugin.GRID_SIZE+j+1) + "Count"), rs.getInt("component" + (i*CraftingPlugin.GRID_SIZE+j+1)));
							componentRow.add(component);
							Log.debug("CRAFTING: adding item: " + component.getItemId() + " to row: " + i + " in column: " + j);
						}
						recipe.addCraftingComponentRow(componentRow);
					}
					list.put(recipe.getID(), recipe);
					Log.debug("CRAFTING: put recipe:" + recipe.getID());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}

	/**
	 * Searches through the objstore table to get the oids of all player
	 * characters.
	 * 
	 * @return: A list of all player character oids
	 */
	public List<Long> getCharacterOids() {
		List<Long> list = new ArrayList<Long>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT DISTINCT obj_id FROM objstore WHERE type = 'PLAYER'");
			rs = queries.executeSelect(ps);
			while (rs.next()) {
				list.add(rs.getLong("obj_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Having too many connection errors, so adding this function to help cope with it
	 */
	public void close() {
		queries.close();
	}
	
	public static final String ITEM_TABLE = "item_templates";
}
