package atavism.agis.database;

import atavism.agis.objects.*;
import atavism.agis.objects.AgisBasicQuest.CollectionGoal;
import atavism.agis.objects.AgisBasicQuest.KillGoal;
import atavism.agis.plugins.AgisMobPlugin;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CombatPlugin;
import atavism.agis.util.HelperFunctions;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.ObjectTypes;
import atavism.server.objects.SpawnData;
import atavism.server.objects.Template;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.ObjectManagerPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class MobDatabase {
	protected static Queries queries;
	
	public MobDatabase(boolean keepAlive) {
		if (queries == null)
			queries = new Queries(keepAlive);
	}
	
	public HashMap<Integer, SpawnData> loadInstanceSpawnData(int instanceID) {
		HashMap<Integer, SpawnData> list = new HashMap<Integer, SpawnData>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("DB: loading spawnData from instance: " + instanceID);
			ps = queries.prepare("SELECT * FROM spawn_data where instance = " + instanceID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					SpawnData sd = readSpawnData(rs);
					if (sd != null)
						list.put(rs.getInt("id"), sd);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}
	
	public SpawnData loadSpecificSpawnData(int spawnID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `spawn_data` where id = " + spawnID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					SpawnData sd = readSpawnData(rs);
					return sd;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return null;
	}
	
	SpawnData readSpawnData(ResultSet rs) {
		SpawnData sd = new SpawnData();
		try {
			String markerName = HelperFunctions.readEncodedString(rs.getBytes("markerName"));
			if (markerName == null || markerName.equals("")) {
				sd.setProperty("markerName", "");
				sd.setLoc(new Point(rs.getFloat("locX"), rs.getFloat("locY"), rs.getFloat("locZ")));
				sd.setOrientation(new Quaternion(rs.getFloat("orientX"), rs.getFloat("orientY"), rs.getFloat("orientZ"), rs.getFloat("orientW")));
			} else {
				sd.setProperty("markerName", markerName);
			}
			
			sd.setProperty("instance", rs.getInt("instance"));
			sd.setCategory(rs.getInt("category"));
			int mobID = rs.getInt("mobTemplate");
			if (mobID == -1)
				return null;
			sd.setTemplateID(mobID);
			Log.debug("DB: loading spawnData with template: " + mobID);
			sd.setNumSpawns(rs.getInt("numSpawns"));
			sd.setSpawnRadius(rs.getInt("spawnRadius"));
			sd.setRespawnTime(rs.getInt("respawnTime"));
			sd.setCorpseDespawnTime(rs.getInt("corpseDespawnTime"));
			sd.setProperty("id", rs.getInt("id"));
			
			// Spawn Behaviour
			Log.debug("DB: loading mobBehaviour with template: " + mobID);
			BehaviorTemplate behavTmpl = new BehaviorTemplate(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")));
			String baseAction = HelperFunctions.readEncodedString(rs.getBytes("baseAction"));
			if (baseAction != null && !baseAction.equals(""))
				behavTmpl.setBaseAction(baseAction);
			behavTmpl.setWeaponsSheathed(rs.getBoolean("weaponSheathed"));
			behavTmpl.setRoamRadius(rs.getInt("roamRadius"));
			// Load in patrol data
			Integer patrolPath = rs.getInt("patrolPath");
			if (patrolPath != null) {
				behavTmpl.setPatrolPathID(patrolPath);
				setPatrolPath(behavTmpl);
			}
			behavTmpl.setHasCombat(rs.getBoolean("combat"));
			//behavTmpl.setAggroRadius(rs.getInt("aggroRadius"));
			Log.debug("DB: loading quests with template: " + mobID);
			// Load in the string for starts quests and split to get the quest IDs
			String startsQuests = HelperFunctions.readEncodedString(rs.getBytes("startsQuests"));
			ArrayList<Integer> questList = new ArrayList<Integer>();
			if (startsQuests != null && !startsQuests.equals("")) {
			    String[] questNums = startsQuests.split(",");
			    Log.debug("MOB: num start quests: " + questNums.length);
			    for (int i = 0; i < questNums.length; i++) {
				    String questID = questNums[i].replace(",", "");
				    Log.debug("MOB: added start quest " + questID + " to behav: " + behavTmpl.getName());
				    if (!questList.equals(""))
				        questList.add(Integer.parseInt(questID));
			    }
			}
			behavTmpl.setStartsQuests(questList);
			Log.debug("MOB: moving onto end quests");
			// Load in the string for ends quests and split to get the quest IDs
			String endsQuests = HelperFunctions.readEncodedString(rs.getBytes("endsQuests"));
			questList = new ArrayList<Integer>();
			if (endsQuests != null && !endsQuests.equals("")) {
			    String[] questNums = endsQuests.split(",");
			    Log.debug("MOB: num end quests: " + questNums.length);
			    for (int i = 0; i < questNums.length; i++) {
				    String questID = questNums[i].replace(",", "");
				    Log.debug("MOB: added end quest " + questID + " to behav: " + behavTmpl.getName());
				    if (!questList.equals(""))
				        questList.add(Integer.parseInt(questID));
			    }
			}
			behavTmpl.setEndsQuests(questList);
			// Load in the string for starts dialogues and split to get the dialogue IDs
			String startsDialogues = HelperFunctions.readEncodedString(rs.getBytes("startsDialogues"));
			ArrayList<Integer> dialogueList = new ArrayList<Integer>();
			if (startsDialogues != null && !startsDialogues.equals("")) {
			    String[] dialogueNums = startsDialogues.split(",");
			    Log.debug("MOB: num start dialogues: " + dialogueNums.length);
			    for (int i = 0; i < dialogueNums.length; i++) {
				    String dialogueID = dialogueNums[i].replace(",", "");
				    if (!dialogueID.equals("")) {
				    	dialogueList.add(Integer.parseInt(dialogueID));
				    	Log.debug("MOB: added start dialogue " + dialogueID + " to behav: " + behavTmpl.getName());
				    }
			    }
			}
			behavTmpl.setStartsDialogues(dialogueList);
			// Merchant Table
			behavTmpl.setMerchantTable(rs.getInt("merchantTable"));
			// Quest item open table
			Integer questOpenLootTable = rs.getInt("questOpenLootTable");
			if (questOpenLootTable != null) {
			    behavTmpl.setQuestOpenLoot(questOpenLootTable);
			}
			Boolean isChest = rs.getBoolean("isChest");
			behavTmpl.setIsChest(isChest);
			Integer pickupItem = rs.getInt("pickupItem");
			if (pickupItem != null) {
			    behavTmpl.setPickupItem(pickupItem);
			}
			//behavTmpl.setOtherUse(HelperFunctions.readEncodedString(rs.getBytes("otherUse"));
			
			// Put the behaviour template in the spawn data
			sd.setProperty(AgisMobPlugin.BEHAVIOR_TMPL_PROP, behavTmpl);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sd;
	}
	
	public void setPatrolPath(BehaviorTemplate behavTmpl) {
		int patrolPath = behavTmpl.getPatrolPathID();
		if (AgisMobPlugin.patrolPoints.containsKey(patrolPath)) {
			behavTmpl.addPatrolPoint(AgisMobPlugin.patrolPoints.get(patrolPath));
			boolean travelReverse = false;
			if (AgisMobPlugin.patrolPoints.get(patrolPath).travelReverse)
				travelReverse = true;
			
			while (AgisMobPlugin.patrolPoints.get(patrolPath).nextPoint > 0) {
				patrolPath = AgisMobPlugin.patrolPoints.get(patrolPath).nextPoint;
				behavTmpl.addPatrolPoint(AgisMobPlugin.patrolPoints.get(patrolPath));
			}
			
			if (travelReverse)
				behavTmpl.travelReverse();
		}
	}
	
	public int getSpawnCount(int instanceID) {
		int spawnCount = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Log.debug("DB: loading spawnData from instance: " + instanceID);
			ps = queries.prepare("SELECT COUNT(*) FROM spawn_data where instance = " + instanceID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				rs.next();
				spawnCount = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return spawnCount;
	}
	
	/*public HashMap<Integer, NpcDisplay> loadMobDisplays() {
		HashMap<Integer, NpcDisplay> map = new HashMap<Integer, NpcDisplay>();
		try {
			ps = queries.prepare("SELECT * FROM `mob_display`");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					String name = HelperFunctions.readEncodedString(rs.getBytes("name");
					String prefab = HelperFunctions.readEncodedString(rs.getBytes("prefab");
					String gender = HelperFunctions.readEncodedString(rs.getBytes("gender");
					String portrait = "";
					NpcDisplay display = new NpcDisplay(id, name, prefab, gender, portrait);
					map.put(id, display);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}*/
	
	public ArrayList<Template> loadMobTemplates(int category) {
		ArrayList<Template> list = new ArrayList<Template>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `mob_templates` where category=" + category  + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int mobID = rs.getInt("id");
					String mobName = HelperFunctions.readEncodedString(rs.getBytes("name"));
					mobName.trim();
					Template tmpl = new Template(mobName, mobID, ObjectManagerPlugin.MOB_TEMPLATE);
					tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_ID, mobID);
					tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.mob);
					tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
					String subTitle = HelperFunctions.readEncodedString(rs.getBytes("subTitle"));
					if (subTitle == null) {
						tmpl.put(WorldManagerClient.NAMESPACE, "subTitle", "");
					} else {
						tmpl.put(WorldManagerClient.NAMESPACE, "subTitle", subTitle);
					}
					
					int mobType = rs.getInt("mobType");
					tmpl.put(WorldManagerClient.NAMESPACE, "mobType", mobType);
					if (mobType == 1) {
						// It is an object - remove name display?
						tmpl.put(WorldManagerClient.NAMESPACE, "nameDisplay", false);
						tmpl.put(WorldManagerClient.NAMESPACE, "targetable", false);
					}
					LinkedList<String> displays = new LinkedList<String>();
					if (HelperFunctions.readEncodedString(rs.getBytes("display1")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display1")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display1")));
					if (HelperFunctions.readEncodedString(rs.getBytes("display2")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display2")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display2")));
					if (HelperFunctions.readEncodedString(rs.getBytes("display3")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display3")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display3")));
					if (HelperFunctions.readEncodedString(rs.getBytes("display4")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display4")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display4")));
					if (displays.isEmpty()) {
						Log.warn("No displays found for: " + mobName);
					}
					tmpl.put(WorldManagerClient.NAMESPACE, "displays", displays);
					float scaleVal = rs.getFloat("scale");
					AOVector v = new AOVector(scaleVal, scaleVal, scaleVal);
					tmpl.put(WorldManagerClient.NAMESPACE, /*WorldManagerClient.TEMPL_SCALE*/ "scale", v);
					tmpl.put(CombatClient.NAMESPACE, CombatPlugin.PROP_HITBOX, rs.getInt("hitBox"));
					
					/*String gender = HelperFunctions.readEncodedString(rs.getBytes("gender");
					gender = gender.trim();
					tmpl.put(WorldManagerClient.NAMESPACE, "genderOptions", gender);*/
					
					tmpl.put(WorldManagerClient.NAMESPACE, "animationState", rs.getInt("baseAnimationState"));
					
					tmpl.put(WorldManagerClient.NAMESPACE, "faction", rs.getInt("faction"));
					tmpl.put(CombatClient.NAMESPACE, "attackable", rs.getBoolean("attackable"));
					int minLevel = rs.getInt("minLevel");
					// TODO: level randomising getting in the maxLevel as well
					//tmpl.put(CombatClient.NAMESPACE, "level", new AgisStat("level", level));
					tmpl.put(CombatClient.NAMESPACE, "minLevel", minLevel);
					int maxLevel = rs.getInt("maxLevel");
					tmpl.put(CombatClient.NAMESPACE, "maxLevel", maxLevel);
					int dmg_base = rs.getInt("minDmg");
					tmpl.put(CombatClient.NAMESPACE, "dmg-base", new AgisStat("dmg-base", dmg_base));
					String attackType = HelperFunctions.readEncodedString(rs.getBytes("dmgType"));
					tmpl.put(CombatClient.NAMESPACE, "attackType", attackType);
					tmpl.put(CombatClient.NAMESPACE, "weaponType", "Unarmed");
					//float attackspeed = rs.getFloat("attackSpeed") * 1000f;
					//tmpl.put(CombatClient.NAMESPACE, "attack_speed", new AgisStat("attack_speed", (int)attackspeed));
					int exp_val = 100;
					// Now set the stats based on the stat variables above
					tmpl.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY, rs.getInt("autoAttack"));
					//tmpl.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_REGEN_EFFECT, "regen effect");
					tmpl.put(CombatClient.NAMESPACE, "combat.mobflag", true);
					tmpl.put(CombatClient.NAMESPACE, "kill_exp", exp_val);
					
					// Effects
					LinkedList<Integer> effectsList = new LinkedList<Integer>();
					tmpl.put(CombatClient.NAMESPACE, "effects", effectsList);
					
					tmpl.put(WorldManagerClient.NAMESPACE, "species", HelperFunctions.readEncodedString(rs.getBytes("species")));
					tmpl.put(WorldManagerClient.NAMESPACE, "race", HelperFunctions.readEncodedString(rs.getBytes("species")));
					tmpl.put(WorldManagerClient.NAMESPACE, "subSpecies", HelperFunctions.readEncodedString(rs.getBytes("subSpecies")));
					tmpl.put(WorldManagerClient.NAMESPACE, "speed_walk", rs.getFloat("speed_walk"));
					tmpl.put(WorldManagerClient.NAMESPACE, "speed_run", rs.getFloat("speed_run"));
					/*String questCategory = HelperFunctions.readEncodedString(rs.getBytes("questCategory");
					if (questCategory != null) {
						LinkedList<String> questCategories = new LinkedList<String>();
						questCategories.add(questCategory);
						tmpl.put(WorldManagerClient.NAMESPACE, "questCategories", questCategories);
					}*/
					String equipment = "";
					Integer primaryWeapon = rs.getInt("primaryWeapon");
					if (primaryWeapon != null && primaryWeapon > 0) {
						equipment = equipment + "*" + primaryWeapon + "; ";
					}
					Integer secondaryWeapon = rs.getInt("secondaryWeapon");
					if (secondaryWeapon != null && secondaryWeapon > 0) {
						equipment = equipment + "*" + secondaryWeapon + "; ";
					}
					tmpl.put(InventoryClient.NAMESPACE, InventoryClient.TEMPL_ITEMS, equipment);
					Log.debug("ITEMS: mob " + tmpl.getName() + " now has equipment: " + equipment);
					
					String specialUse = HelperFunctions.readEncodedString(rs.getBytes("specialUse"));
					if (specialUse != null && !specialUse.equals("")) {
						tmpl.put(WorldManagerClient.NAMESPACE, "specialUse", specialUse);
					}
					
					// Skinning
					if (SKINNING_ENABLED) {
						Integer skinningLootTable = rs.getInt("skinningLootTable");
						tmpl.put(WorldManagerClient.NAMESPACE, "skinningLootTable", skinningLootTable);
						Integer skinningLevelReq = rs.getInt("skinningLevelReq");
						tmpl.put(WorldManagerClient.NAMESPACE, "skinningLevelReq", skinningLevelReq);
					}
					
					list.add(tmpl);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for (Template tmpl : list) {
			// Load in specific mob stats
			tmpl.put(CombatClient.NAMESPACE, ":statOverrides", loadMobStats(tmpl.getTemplateID()));
			// Load in mob loot
			tmpl.put(InventoryClient.NAMESPACE, "lootTables", loadMobLoot(tmpl.getTemplateID()));
		}
		
		return list;
	}
	
	public ArrayList<HashMap<String, Serializable>> getMobTemplates(int category, int baseCategory) {
		Log.warn("MOB: getting mob templates");
		ArrayList<HashMap<String, Serializable>> list = new ArrayList<HashMap<String, Serializable>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `mob_templates` where category=" + category + 
					" OR category=" + baseCategory  + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					HashMap<String, Serializable> map = new HashMap<String, Serializable>();
					int mobID = rs.getInt("id");
					map.put("id", mobID);
					String mobName = HelperFunctions.readEncodedString(rs.getBytes("name"));
					mobName.trim();
					map.put("name", mobName);
					Log.warn("MOB: found mob template: " + mobName);
					map.put("subTitle", HelperFunctions.readEncodedString(rs.getBytes("subTitle")));
					map.put("mobType", rs.getInt("mobType"));
					Log.warn("MOB: has mob type: " + rs.getInt("mobType"));
					LinkedList<String> displays = new LinkedList<String>();
					if (HelperFunctions.readEncodedString(rs.getBytes("display1")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display1")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display1")));
					if (HelperFunctions.readEncodedString(rs.getBytes("display2")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display2")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display2")));
					if (HelperFunctions.readEncodedString(rs.getBytes("display3")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display3")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display3")));
					if (HelperFunctions.readEncodedString(rs.getBytes("display4")) != null && !HelperFunctions.readEncodedString(rs.getBytes("display4")).equals(""))
						displays.add(HelperFunctions.readEncodedString(rs.getBytes("display4")));
					Log.warn("MOB: about to check if any displays were found");
					if (displays.isEmpty()) {
						Log.warn("No displays found for: " + mobName);
					}
					map.put("displays", displays);
					map.put("scale", rs.getFloat("scale"));
					
					map.put("level", rs.getInt("minLevel"));
					map.put("attackable", rs.getBoolean("attackable"));
					
					map.put("faction", rs.getInt("faction"));
					map.put("species", HelperFunctions.readEncodedString(rs.getBytes("species")));
					map.put("subSpecies", HelperFunctions.readEncodedString(rs.getBytes("subSpecies")));
					//map.put("gender", HelperFunctions.readEncodedString(rs.getBytes("gender"));
					
					
					//map.put("equipment", loadMobEquipment(mobID));
					//map.put("lootTables", loadMobLoot(mobID));
					Log.warn("MOB: added mob template: " + mobName);
					list.add(map);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		Log.warn("MOB: returning mob template list with " + list.size() + " templates");
		return list;
	}
	
	private HashMap<String, Integer> loadMobStats(int mobID) {
		HashMap<String, Integer> statOverrides = new HashMap<String, Integer>();
		Log.debug("MOB: loading in stat data for mob: " + mobID);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `mob_stat` where mobTemplate=" + mobID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String stat = HelperFunctions.readEncodedString(rs.getBytes("stat"));
					int value = rs.getInt("value");
					statOverrides.put(stat, value);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		Log.debug("MOB: finished loading in stat data for mob: " + mobID);
		return statOverrides;
	}
	
	public HashMap<Integer, PatrolPoint> loadPatrolPathPoints() {
		HashMap<Integer, PatrolPoint> patrolPoints = new HashMap<Integer, PatrolPoint>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `patrol_path` where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Point p = new Point(rs.getFloat("locX"), rs.getFloat("locY"), rs.getFloat("locZ"));
					Float lingerTime = rs.getFloat("lingerTime");
					PatrolPoint point = new PatrolPoint(rs.getInt("id"), p, lingerTime);
					
					point.startingPoint = rs.getBoolean("startingPoint");
					if (point.startingPoint) {
						point.name = HelperFunctions.readEncodedString(rs.getBytes("name"));
						if (rs.getBoolean("travelReverse")) {
							point.travelReverse = rs.getBoolean("travelReverse");
						}
					}
					point.nextPoint = rs.getInt("nextPoint");
					
					patrolPoints.put(rs.getInt("id"), point);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return patrolPoints;
	}
	
	private HashMap<Integer, Integer> loadMobLoot(int mobID) {
		HashMap<Integer, Integer> lootTables = new HashMap<Integer, Integer>();
		Log.debug("MOB: loading in loot data for mob: " + mobID);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `mob_loot` where mobTemplate=" + mobID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int lootTable = rs.getInt("lootTable");
					int tableChance = rs.getInt("dropChance");
					lootTables.put(lootTable, tableChance);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		Log.debug("MOB: finished loading in loot data for mob: " + mobID);
		return lootTables;
	}
	
	public ArrayList<Faction> loadFactions(int category) {
		ArrayList<Faction> list = new ArrayList<Faction>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM factions where category=" + category + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Faction f = new Faction(rs.getInt("id"), HelperFunctions.readEncodedString(rs.getBytes("name")), HelperFunctions.readEncodedString(rs.getBytes("factionGroup")),
							rs.getInt("category"));
					f.setIsPublic(rs.getBoolean("public"));
					f.setDefaultStance(rs.getInt("defaultStance"));
					list.add(f);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for(Faction f : list) {
			// Load stances
			f.setDefaultStances(loadFactionStances(f.getID()));
		}
		
		return list;
	}
	
	public HashMap<Integer, Integer> loadFactionStances(int factionID) {
		HashMap<Integer, Integer> factionStances = new HashMap<Integer, Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `faction_stances` where factionID = " + factionID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					factionStances.put(rs.getInt("otherFaction"), rs.getInt("defaultStance"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return factionStances;
	}
	
	public HashMap<Integer, AgisBasicQuest> loadQuests(int category) {
		HashMap<Integer, AgisBasicQuest> list = new HashMap<Integer, AgisBasicQuest>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM quests where category=" + category + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					AgisBasicQuest q = new AgisBasicQuest();
					int questID = rs.getInt("id");
					q.setID(questID);
					q.setName(HelperFunctions.readEncodedString(rs.getBytes("name")));
					q.setFaction(rs.getInt("faction"));
					q.setRepeatable(rs.getBoolean("repeatable"));
					q.setSecondaryGrades(rs.getInt("numGrades")-1);
					q.setDesc(HelperFunctions.readEncodedString(rs.getBytes("description")));
					q.setObjective(HelperFunctions.readEncodedString(rs.getBytes("objectiveText")));
					q.setProgressText(HelperFunctions.readEncodedString(rs.getBytes("progressText")));
					int deliveryItem = rs.getInt("deliveryItem1");
					if (deliveryItem != -1) {
						q.addDeliveryItem(deliveryItem);
					}
					deliveryItem = rs.getInt("deliveryItem2");
					if (deliveryItem != -1) {
						q.addDeliveryItem(deliveryItem);
					}
					deliveryItem = rs.getInt("deliveryItem3");
					if (deliveryItem != -1) {
						q.addDeliveryItem(deliveryItem);
					}
					// Requirements
					int questPrereq = rs.getInt("questPrereq");
					if (questPrereq != -1)
					    q.addQuestPrereq(questPrereq);
					int questStartedReq = rs.getInt("questStartedReq");
					if (questStartedReq != -1)
					    q.setQuestStartedReq(questStartedReq);
					
					//Load Rewards
					//loadQuestRewards(q, questID);
					int rewardLevel = 0;
					q.setCompletionText(rewardLevel, HelperFunctions.readEncodedString(rs.getBytes("completionText")));
					q.setXpReward(rewardLevel, rs.getInt("experience"));
					for (int i = 1; i < 5; i++) {
						int item = rs.getInt("item" + i);
						if (item != -1) {
							int itemCount = rs.getInt("item" + i + "count");
							if (itemCount > 0)
								q.addReward(rewardLevel, item, itemCount);
						}
						int itemToChoose = rs.getInt("chooseItem" + i);
						if (itemToChoose != -1) {
							int itemCount = rs.getInt("chooseItem" + i + "count");
							if (itemCount > 0)
								q.addRewardToChoose(rewardLevel, itemToChoose, itemCount);
						}
					}
					for (int i = 1; i < 3; i++) {
						int currency = rs.getInt("currency" + i);
						if (currency > 0) {
							int currencyCount = rs.getInt("currency" + i + "count");
							q.setCurrencyReward(rewardLevel, currency, currencyCount);
						}
						int faction = rs.getInt("rep" + i);
						if (faction > 0) {
							int repCount = rs.getInt("rep" + i + "gain");
							q.setRepReward(rewardLevel, faction, repCount);
						}
					}
					Log.debug("QDB: loaded quest rewards for quest: " + questID + ". Has experience:" 
							+ q.getXpReward().get(rewardLevel) + ". and completionText: " + q.getCompletionText());
					list.put(questID, q);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for (AgisBasicQuest q : list.values()) {
			//Load Requirements
			q.setRequirements(getQuestRequirements(q.getID()));
			
			//Load Objectives
			loadQuestObjectives(q, q.getID());
		}
		return list;
	}
	
	private HashMap<Integer, HashMap<String, Integer>> getQuestRequirements(int questID) {
		HashMap<Integer, HashMap<String, Integer>> requirementMap = new HashMap<Integer, HashMap<String, Integer>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM quest_requirement where quest_id=" + questID + " AND isactive=1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int requirementType = rs.getInt("editor_option_type_id");
					String requirementOption = HelperFunctions.readEncodedString(rs.getBytes("editor_option_choice_type_id"));
					int requiredValue = rs.getInt("required_value");
					if (requirementMap.containsKey(requirementType)) {
						HashMap<String, Integer> requirementOptions = requirementMap.get(requirementType);
						requirementOptions.put(requirementOption, requiredValue);
						requirementMap.put(requirementType, requirementOptions);
					} else {
						HashMap<String, Integer> requirementOptions = new HashMap<String, Integer>();
						requirementOptions.put(requirementOption, requiredValue);
						requirementMap.put(requirementType, requirementOptions);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return requirementMap;
	}
	
	public void loadQuestObjectives(AgisBasicQuest q, int questID) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM quest_objectives where questID=" + questID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					String objectiveType = HelperFunctions.readEncodedString(rs.getBytes("objectiveType"));
					boolean primaryObjective = rs.getBoolean("primaryObjective");
					int objectiveLevel = 0;
					if (!primaryObjective)
						objectiveLevel = 1;
					int target = rs.getInt("target");
					int targetCount = rs.getInt("targetCount");
					String targetText = HelperFunctions.readEncodedString(rs.getBytes("targetText"));
					if (objectiveType.equals("mob")) {
						AgisBasicQuest.KillGoal goal = new AgisBasicQuest.KillGoal(objectiveLevel, target, targetText, targetCount);
						q.addKillGoal(goal);
					} else if (objectiveType.equals("item")) {
						AgisBasicQuest.CollectionGoal goal = new AgisBasicQuest.CollectionGoal(objectiveLevel, target, targetText, targetCount);
						q.addCollectionGoal(goal);
					} else if (objectiveType.equals("mobCategory")) {
						AgisBasicQuest.CategoryKillGoal goal = new AgisBasicQuest.CategoryKillGoal(objectiveLevel, targetText, targetText, targetCount);
						q.addCategoryKillGoal(goal);
					} else if (objectiveType.equals("task")) {
						AgisBasicQuest.TaskGoal goal = new AgisBasicQuest.TaskGoal(objectiveLevel, target, targetText, targetCount);
						q.addTaskGoal(goal);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	/*public void loadQuestRewards(AgisBasicQuest q, int questID) {
		try {
			ps = queries.prepare("SELECT * FROM quest_rewards where questID=" + questID + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int rewardLevel = rs.getInt("rewardLevel");
					q.setCompletionText(rewardLevel, HelperFunctions.readEncodedString(rs.getBytes("completionText")));
					q.setXpReward(rewardLevel, rs.getInt("experience"));
					for (int i = 1; i < 5; i++) {
						int item = rs.getInt("item" + i);
						if (item != -1) {
							int itemCount = rs.getInt("item" + i + "count");
							q.addReward(rewardLevel, item, itemCount);
						}
						int itemToChoose = rs.getInt("itemToChoose" + i);
						if (itemToChoose != -1) {
							int itemToChooseCount = rs.getInt("itemToChoose" + i + "count");
							q.addRewardToChoose(rewardLevel, itemToChoose, itemToChooseCount);
						}
					}
					for (int i = 1; i < 3; i++) {
						int currency = rs.getInt("currency" + i);
						if (currency != -1) {
							int currencyCount = rs.getInt("currency" + i + "count");
							q.setCurrencyReward(rewardLevel, currency, currencyCount);
						}
						int faction = rs.getInt("reputation" + i);
						if (faction != -1) {
							int repCount = rs.getInt("reputation" + i + "count");
							q.setRepReward(rewardLevel, faction, repCount);
						}
					}
					Log.debug("QDB: loaded quest rewards for quest: " + questID + ". Has experience:" 
							+ q.getXpReward().get(rewardLevel) + ". and completionText: " + q.getCompletionText());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Loads in the list of currencies from the database.
	 * @param category
	 * @return
	 */
	public ArrayList<Currency> loadCurrencies(int category) {
		ArrayList<Currency> currencies = new ArrayList<Currency>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//if (category == -1)
			//	ps = queries.prepare("SELECT * FROM `currencies`");
			//else
				ps = queries.prepare("SELECT * FROM `currencies` where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Currency newCurrency = new Currency();
					int currencyID = rs.getInt("id");
					newCurrency.setCurrencyID(currencyID);
					String currencyName = HelperFunctions.readEncodedString(rs.getBytes("name"));
					newCurrency.setCurrencyName(currencyName);
					String currencyIcon = HelperFunctions.readEncodedString(rs.getBytes("icon"));
					newCurrency.setCurrencyIcon(currencyIcon);
					String currencyDescription = HelperFunctions.readEncodedString(rs.getBytes("description"));
					newCurrency.setCurrencyDescription(currencyDescription);
					int currencyMax = rs.getInt("maximum");
					newCurrency.setCurrencyMax(currencyMax);
					boolean external = rs.getBoolean("external");
					newCurrency.setExternal(external);
					currencies.add(newCurrency);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		for (Currency c : currencies) {
			loadCurrencyConversions(c);
		}
		
		return currencies;
	}
	
	/**
	 * Loads in conversion information from one currency to others
	 * @param currencyId
	 * @return
	 */
	public void loadCurrencyConversions(Currency currency) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `currency_conversion` where currencyID=" + currency.getCurrencyID() + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int currencyToID = rs.getInt("currencyToID");
					int amountReq = rs.getInt("amount");
					boolean autoConversion = rs.getBoolean("autoConverts");
					currency.addConversionOption(currencyToID, amountReq, autoConversion);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
	}
	
	public HashMap<Integer, LootTable> loadLootTables(int category) {
		HashMap<Integer, LootTable> list = new HashMap<Integer, LootTable>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			if (category == -1)
				ps = queries.prepare("SELECT * FROM loot_tables where isactive = 1");
			else
				ps = queries.prepare("SELECT * FROM loot_tables where category=" + category + " AND isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					Log.debug("LOOT: loading loot table:" + rs.getInt("id"));
					LootTable lTbl = new LootTable();
					lTbl.setID(rs.getInt("id"));
					lTbl.setName(HelperFunctions.readEncodedString(rs.getBytes("name")));
					ArrayList<Integer> items = new ArrayList<Integer>();
					ArrayList<Integer> itemCounts = new ArrayList<Integer>();
					ArrayList<Integer> itemChances = new ArrayList<Integer>();
					for (int i = 1; i <= 10; i++) {
						int itemID = rs.getInt("item" + i);
						if (itemID != -1) {
							items.add(itemID);
							itemCounts.add(rs.getInt("item" + i + "Count"));
							itemChances.add(rs.getInt("item" + i + "Chance"));
						}
					}
					lTbl.setItems(items);
					lTbl.setItemCounts(itemCounts);
					lTbl.setItemChances(itemChances);
					//loadLootTableDrops(lTbl);
					list.put(lTbl.getID(), lTbl);
					Log.debug("LOOT: put loot table:" + lTbl.getID());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return list;
	}
	
	/**
	 * Unused.
	 * @param lTbl
	 */
	public void loadLootTableDrops(LootTable lTbl) {
		ArrayList<Integer> items = new ArrayList<Integer>();
		ArrayList<Integer> itemCounts = new ArrayList<Integer>();
		ArrayList<Integer> itemChances = new ArrayList<Integer>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM loot_table_drops where lootTable=" + lTbl.getID() + " where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					items.add(rs.getInt("item"));
					itemCounts.add(rs.getInt("itemCount"));
					itemChances.add(rs.getInt("dropChance"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		
		lTbl.setItems(items);
		lTbl.setItemCounts(itemCounts);
		lTbl.setItemChances(itemChances);
	}
	
	public int writeQuest(int category, AgisBasicQuest q) {
		Log.debug("Writing quest data to database");
		int questPrereq = -1;
		if (q.getQuestPrereqs().size() > 0)
			questPrereq = q.getQuestPrereqs().get(0);
		int inserted = -1;
		String tableName = "quests";
		String columnNames = "category,name,faction,numGrades,repeatable,description,objectiveText,progressText,deliveryItem1,questPrereq,levelReq";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
					+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, category);
			stmt.setString(2, q.getName());
			stmt.setInt(3, q.getFaction());
			stmt.setInt(4, q.getSecondaryGrades());
			stmt.setBoolean(5, q.getRepeatable());
			stmt.setString(6, q.getDesc());
			stmt.setString(7, q.getObjective());
			stmt.setString(8, q.getProgressText());
			stmt.setInt(9, q.getDeliveryItems().get(0));
			stmt.setInt(10, questPrereq);
			stmt.setInt(11, q.getQuestLevelReq());
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		if (inserted == -1)
			return inserted;
		
		writeQuestObjectives(inserted, q);
		// Write the base for the rewards entry
		tableName = "questRewards";
		columnNames = "questID,rewardLevel,completionText,experience";
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
					+ ") values (?, ?, ?, ?)");
			stmt.setInt(1, inserted);
			stmt.setInt(2, 0);
			stmt.setString(3, q.getCompletionText().get(0));
			stmt.setInt(4, q.getXpReward().get(0));
			queries.executeInsert(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		// Now update it with item/currency/rep rewards
		writeQuestRewards(inserted, q);
		
		Log.debug("Wrote quest data to database");
		return inserted;
	}
	
	public int editQuest(int questID, AgisBasicQuest q) {
		Log.debug("Writing quest data to database");
		int questPrereq = -1;
		if (q.getQuestPrereqs().size() > 0)
			questPrereq = q.getQuestPrereqs().get(0);
		String tableName = "quests";
		int updated;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set name=?, faction=?, numGrades=?, " 
					+ "repeatable=?, description=?, objectiveText=?, progressText=?, deliveryItem1=?, "
					+ "questPrereq=?, levelReq=? where id=?");
			stmt.setString(1, q.getName());
			stmt.setInt(2, q.getFaction());
			stmt.setInt(3, q.getSecondaryGrades());
			stmt.setBoolean(4, q.getRepeatable());
			stmt.setString(5, q.getDesc());
			stmt.setString(6, q.getObjective());
			stmt.setString(7, q.getProgressText());
			stmt.setInt(8, q.getDeliveryItems().get(0));
			stmt.setInt(9, questPrereq);
			stmt.setInt(10, q.getQuestLevelReq());
			stmt.setInt(11, questID);
			Log.debug("QUESTDB: updating quest with statement: " + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		if (updated == -1)
			return updated;
		
		// To handle objectives we first delete all the existing objectives then insert new ones
		tableName = "questObjectives";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE questID = " + questID;
		queries.executeUpdate(deleteString);

		writeQuestObjectives(questID, q);
		
		// Write in new progress text etc.
		tableName = "questRewards";
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set completionText = ?, experience = ? where questID=? AND rewardLevel=0");
			stmt.setString(1, q.getCompletionText().get(0));
			stmt.setInt(2, q.getXpReward().get(0));
			stmt.setInt(3, questID);
			Log.debug("QUESTDB: placing item stmt=" + stmt.toString());
			queries.executeUpdate(stmt);
		} catch (SQLException e) {
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		writeQuestRewards(questID, q);
		
		Log.debug("Wrote quest data to database");
		return updated;
	}
	
	public void writeQuestObjectives(int questID, AgisBasicQuest q) {
		String tableName = "questObjectives";
		List<CollectionGoal> cGoals = q.getCollectionGoals();
		for (int i = 1; i <= cGoals.size(); i++) {
			CollectionGoal cGoal = cGoals.get(i-1);
			PreparedStatement stmt = null;
			try {
				String columnNames = "questID,primaryObjective,objectiveType,target,targetCount,targetText";
				stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
						+ ") values (?, ?, ?, ?, ?, ?)");
				stmt.setInt(1, questID);
				stmt.setInt(2, 1);
				stmt.setString(3, "item");
				stmt.setInt(4, cGoal.templateID);
				stmt.setInt(5, cGoal.num);
				stmt.setString(6, cGoal.templateName);
				queries.executeInsert(stmt);
			} catch (SQLException e) {
				return;
			} finally {
				queries.closeStatement(stmt);
			}
		}
		List<KillGoal> kGoals = q.getKillGoals();
		for (int i = 1; i <= kGoals.size(); i++) {
			KillGoal kGoal = kGoals.get(i-1);
			PreparedStatement stmt = null;
			try {
				String columnNames = "questID,primaryObjective,objectiveType,target,targetCount,targetText";
				stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
						+ ") values (?, ?, ?, ?, ?, ?)");
				stmt.setInt(1, questID);
				stmt.setInt(2, 1);
				stmt.setString(3, "mob");
				stmt.setInt(4, kGoal.mobID);
				stmt.setInt(5, kGoal.num);
				stmt.setString(6, kGoal.mobName);
				queries.executeInsert(stmt);
			} catch (SQLException e) {
				return;
			} finally {
				queries.closeStatement(stmt);
			}
		}
	}
	
	public void writeQuestRewards(int questID, AgisBasicQuest q) {
		String tableName = "questRewards";
		int numRewards = 1;
		if (q.getRewards().containsKey(0)) {
			HashMap<Integer, Integer> rewards = q.getRewards().get(0);
			for (int item: rewards.keySet()) {
				PreparedStatement stmt = null;
				try {
					stmt = queries.prepare("UPDATE " + tableName + " set item" + numRewards + " = ?, item" + numRewards 
							+ "count = ? where questID=? AND rewardLevel=0");
					stmt.setInt(1, item);
					stmt.setInt(2, rewards.get(item));
					stmt.setInt(3, questID);
					Log.debug("QUESTDB: placing item stmt=" + stmt.toString());
					queries.executeUpdate(stmt);
				} catch (SQLException e) {
					return;
				} finally {
					queries.closeStatement(stmt);
				}
				numRewards++;
			}
		}
		if (q.getRewardsToChoose().containsKey(0)) {
			HashMap<Integer, Integer> rewards = q.getRewardsToChoose().get(0);
			numRewards = 1;
			for (int item: rewards.keySet()) {
				PreparedStatement stmt = null;
				try {
					stmt = queries.prepare("UPDATE " + tableName + " set itemToChoose" + numRewards 
							+ " = ?, itemToChoose" + numRewards + "count = ? where questID=? AND rewardLevel=0");
					stmt.setInt(1, item);
					stmt.setInt(2, rewards.get(item));
					stmt.setInt(3, questID);
					Log.debug("QUESTDB: placing itemToChoose stmt=" + stmt.toString());
					queries.executeUpdate(stmt);
				} catch (SQLException e) {
					return;
				} finally {
					queries.closeStatement(stmt);
				}
				numRewards++;
			}
		}
		HashMap<Integer, Integer> currencyRewards = q.getCurrencyRewards().get(0);
		int numCurrencyRewards = 1;
		for (int currency: currencyRewards.keySet()) {
			PreparedStatement stmt = null;
			try {
				stmt = queries.prepare("UPDATE " + tableName + " set currency" + numCurrencyRewards 
						+ " = ?, currency" + numCurrencyRewards + "count = ? where questID=? AND rewardLevel=0");
				stmt.setInt(1, currency);
				stmt.setInt(2, currencyRewards.get(currency));
				stmt.setInt(3, questID);
				Log.debug("QUESTDB: placing currency stmt=" + stmt.toString());
				queries.executeUpdate(stmt);
			} catch (SQLException e) {
				return;
			} finally {
				queries.closeStatement(stmt);
			}
			numCurrencyRewards++;
		}
		HashMap<Integer, Integer> repRewards = q.getRepRewards().get(0);
		int numRepRewards = 1;
		for (int faction: repRewards.keySet()) {
			PreparedStatement stmt = null;
			try {
				stmt = queries.prepare("UPDATE " + tableName + " set reputation" + numRepRewards 
						+ " = ?, reputation" + numRepRewards + "count = ? where questID=? AND rewardLevel=0");
				stmt.setInt(1, faction);
				stmt.setInt(2, repRewards.get(faction));
				stmt.setInt(3, questID);
				Log.debug("QUESTDB: placing rep stmt=" + stmt.toString());
				queries.executeUpdate(stmt);
			} catch (SQLException e) {
				return;
			} finally {
				queries.closeStatement(stmt);
			}
			numRepRewards++;
		}
	}
	
	public int writeSpawnData(SpawnData sd, Point loc, Quaternion orient, BehaviorTemplate behavTmpl, int instanceID) {
		Log.debug("Writing spawn data to database");
		String startsQuests = "";
		for (int questID : behavTmpl.getStartsQuests()) {
			Log.debug("Starts quests:" + behavTmpl.getStartsQuests());
			startsQuests += "" + questID + ",";
		}
		String endsQuests = "";
		for (int questID : behavTmpl.getEndsQuests()) {
			Log.debug("Ends quests:" + behavTmpl.getEndsQuests());
			endsQuests += "" + questID + ",";
		}
		String startsDialogues = "";
		for (int dialogueID : behavTmpl.getStartsDialogues()) {
			startsDialogues += "" + dialogueID + ",";
		}
		int inserted = -1;
		PreparedStatement stmt = null;
		try {
			String tableName = "spawn_data";
			String columnNames = "category,name,mobTemplate,markerName,locX,locY,locZ,orientX,orientY,orientZ,orientW,instance,numSpawns," + 
			"spawnRadius,respawnTime,corpseDespawnTime,combat,roamRadius,startsQuests,endsQuests,startsDialogues,baseAction,weaponSheathed,"
			+ "merchantTable,patrolPath,questOpenLootTable,isChest,pickupItem";
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, sd.getCategory());
			stmt.setString(2, "spawn");
			stmt.setInt(3, sd.getTemplateID());
			stmt.setString(4, "");
			stmt.setFloat(5, loc.getX());
			stmt.setFloat(6, loc.getY());
			stmt.setFloat(7, loc.getZ());
			stmt.setFloat(8, orient.getX());
			stmt.setFloat(9, orient.getY());
			stmt.setFloat(10, orient.getZ());
			stmt.setFloat(11, orient.getW());
			stmt.setInt(12, instanceID);
			stmt.setInt(13, sd.getNumSpawns());
			stmt.setInt(14, sd.getSpawnRadius());
			stmt.setInt(15, sd.getRespawnTime());
			stmt.setInt(16, sd.getCorpseDespawnTime());
			stmt.setBoolean(17, behavTmpl.getHasCombat());
			stmt.setInt(18, behavTmpl.getRoamRadius());
			stmt.setString(19, startsQuests);
			stmt.setString(20, endsQuests);
			stmt.setString(21, startsDialogues);
			stmt.setString(22, behavTmpl.getBaseAction());
			stmt.setBoolean(23, behavTmpl.getWeaponsSheathed());
			stmt.setInt(24, behavTmpl.getMerchantTable());
			stmt.setInt(25, behavTmpl.getPatrolPathID());
			stmt.setInt(26, behavTmpl.getQuestOpenLoot());
			stmt.setBoolean(27, behavTmpl.getIsChest());
			stmt.setInt(28, behavTmpl.getPickupItem());
			//stmt.setInt(27, sd.getIntProperty("domeID"));
			Log.debug("Spawn Data statement = " + stmt.toString());
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
			Log.error("Failed to write spawn data to database with id= " + inserted);
			return -1;
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote spawn data to database with id= " + inserted);
		return inserted;
	}
	
	public int editSpawnData(SpawnData sd, int spawnID, Point loc, Quaternion orient, BehaviorTemplate behavTmpl) {
		Log.debug("Editing spawn data to database");
		int updated;
		String startsQuests = "";
		for (int questID : behavTmpl.getStartsQuests()) {
			Log.debug("Starts quests:" + behavTmpl.getStartsQuests());
			startsQuests += "" + questID + ",";
		}
		String endsQuests = "";
		for (int questID : behavTmpl.getEndsQuests()) {
			Log.debug("Ends quests:" + behavTmpl.getEndsQuests());
			endsQuests += "" + questID + ",";
		}
		String startsDialogues = "";
		for (int dialogueID : behavTmpl.getStartsDialogues()) {
			startsDialogues += "" + dialogueID + ",";
		}
		String tableName = "spawn_data";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set mobTemplate=?, locX=?, locY=?, locZ=?, orientX=?, orientY=?,"
					+ " orientZ=?, orientW=?, numSpawns=?, spawnRadius=?, respawnTime=?, corpseDespawnTime=?, combat=?, roamRadius=?,"
					+ " startsQuests=?, endsQuests=?, startsDialogues=?, baseAction=?, weaponSheathed=?, merchantTable=?, patrolPath=?,"
					+ " questOpenLootTable=?, isChest=?, pickupItem=? where id=?");
			stmt.setInt(1, sd.getTemplateID());
			stmt.setFloat(2, loc.getX());
			stmt.setFloat(3, loc.getY());
			stmt.setFloat(4, loc.getZ());
			stmt.setFloat(5, orient.getX());
			stmt.setFloat(6, orient.getY());
			stmt.setFloat(7, orient.getZ());
			stmt.setFloat(8, orient.getW());
			stmt.setInt(9, sd.getNumSpawns());
			stmt.setInt(10, sd.getSpawnRadius());
			stmt.setInt(11, sd.getRespawnTime());
			stmt.setInt(12, sd.getCorpseDespawnTime());
			stmt.setBoolean(13, behavTmpl.getHasCombat());
			stmt.setInt(14, behavTmpl.getRoamRadius());
			stmt.setString(15, startsQuests);
			stmt.setString(16, endsQuests);
			stmt.setString(17, startsDialogues);
			stmt.setString(18, behavTmpl.getBaseAction());
			stmt.setBoolean(19, behavTmpl.getWeaponsSheathed());
			stmt.setInt(20, behavTmpl.getMerchantTable());
			stmt.setInt(21, behavTmpl.getPatrolPathID());
			stmt.setInt(22, behavTmpl.getQuestOpenLoot());
			stmt.setBoolean(23, behavTmpl.getIsChest());
			stmt.setInt(24, behavTmpl.getPickupItem());
			stmt.setInt(25, spawnID);
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			updated = -1;
		} finally {
			queries.closeStatement(stmt);
		}
		
		Log.debug("Edited spawn data to database");
		return updated;
	}
	
	public void deleteSpawnData(int spawnID) {
		String tableName = "spawn_data";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE id = " + spawnID;
		queries.executeUpdate(deleteString);
	}
	
	public int writePatrolPath(ArrayList<PatrolPoint> points, boolean travelReverse) {
		String tableName = "patrol_path";
		int lastInsertedID = -1;
		for (int i = points.size()-1; i >= 0; i--) {
			points.get(i).nextPoint = lastInsertedID;
			PreparedStatement stmt = null;
			try {
				String columnNames = "name,startingPoint,travelReverse,locX,locY,locZ,lingerTime,nextPoint";
				stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
						+ ") values (?, ?, ?, ?, ?, ?, ?, ?)");
				stmt.setString(1, "");
				stmt.setBoolean(2, i == 0 ? true : false);
				stmt.setBoolean(3, travelReverse);
				stmt.setFloat(4, points.get(i).loc.getX());
				stmt.setFloat(5, points.get(i).loc.getY());
				stmt.setFloat(6, points.get(i).loc.getZ ());
				stmt.setFloat(7, points.get(i).lingerTime);
				stmt.setInt(8, points.get(i).nextPoint);
				lastInsertedID = queries.executeInsert(stmt);
				points.get(i).id = lastInsertedID;
			} catch (SQLException e) {
				return -1;
			} finally {
				queries.closeStatement(stmt);
			}
		}
		return lastInsertedID;
	}
	
	public int writeNpcDisplayData(String name, String race, String gender) {
		Log.debug("Writing npc appearance data to database");
		String tableName = "npcDisplay";
		String columnNames = "name,race,gender,skinColour";
		String values = "'" + name + "','" + race + "','" + gender;
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote npc appearance data to database");
		return inserted;
	}
	
	public int writeMobData(int category, String name, String subtitle, int mobType, int soundSet, 
			LinkedList<Integer> displays, int animState, float scale, int offset, int hitBox, int runThreshold, String gender,
			int level, boolean attackable, int faction, String species, String subSpecies, String questCategory) {
		Log.debug("Writing mob data to database");
		// Set up the displays
		int inserted = -1;
		String tableName = "mob_templates";
		String columnNames = "category,name,subTitle,mobType,soundSet,display1,display2,display3,display4,baseAnimationState,scale," +
		"overheadOffset,hitBox,runThreshold,gender,level,attackable,faction,species,subSpecies,questCategory";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
				+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, category);
			stmt.setString(2, name);
			stmt.setString(3, subtitle);
			stmt.setInt(4, mobType);
			stmt.setInt(5, soundSet);
			for (int i = 0; i <= 4; i++) {
				if (displays.size() > i)
					stmt.setInt(6 + i, displays.get(i));
				else
					stmt.setInt(6 + i, -1);
			}
			stmt.setInt(10, animState);
			stmt.setFloat(11, scale);
			stmt.setInt(12, offset);
			stmt.setInt(13, hitBox);
			stmt.setInt(14, runThreshold);
			stmt.setString(15, gender);
			stmt.setInt(16, level);
			stmt.setBoolean(17, attackable);
			stmt.setInt(18, faction);
			stmt.setString(19, species);
			stmt.setString(20, subSpecies);
			stmt.setString(21, questCategory);
			inserted = queries.executeInsert(stmt);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote mob data to database");
		return inserted;
	}
	
	public int writeMobCombatData(int mobID, int health, String attackType) {
		Log.debug("Writing mob combat data to database");
		String tableName = "mobCombatStats";
		String columnNames = "id,health,attackType";
		String values = mobID + "," + health + ",'" + attackType + "'";
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote mob combat data to database");
		return inserted;
	}
	
	public int writeMobEquipmentData(int category, int mobID, int item) {
		Log.debug("Writing mob equip data to database");
		String tableName = "mobEquipment";
		String columnNames = "category,mobTemplate,item";
		String values = category + "," + mobID + "," + item;
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote mob equip data to database");
		return inserted;
	}
	
	public void writeMobLootTables(int category, int mobTemplate, HashMap<Integer, Integer> lootTables) {
		Log.debug("Writing mob loot tables to database");
		String tableName = "mobLoot";
		// First delete all entries for this mob. I know this is not very efficient for the 
		// database but I don't have time to write it properly
		String deleteString = "DELETE FROM `" + tableName + "` WHERE mobTemplate = " + mobTemplate;
		queries.executeUpdate(deleteString);
		
		String columnNames = "category,mobTemplate,lootTable,dropChance";
		for (int tableID : lootTables.keySet()) {
			PreparedStatement stmt = null;
			try {
				stmt = queries.prepare("INSERT INTO " + tableName + " (" + columnNames 
					+ ") values (?, ?, ?, ?)");
				stmt.setInt(1, category);
				stmt.setInt(2, mobTemplate);
				stmt.setInt(3, tableID);
				stmt.setInt(4, lootTables.get(tableID));
				queries.executeInsert(stmt);
			} catch (SQLException e) {
			} finally {
				queries.closeStatement(stmt);
			}
		}
		Log.debug("Wrote mob loot tables to database");
		return;
	}
	
	public int editMobData(int templateID, String name, String subtitle, int mobType, int soundSet, 
			LinkedList<Integer> displays, int animState, float scale, int offset, int hitBox, int runThreshold, String gender,
			int level, boolean attackable, int faction, String species, String subSpecies, String questCategory) {
		Log.debug("Writing mob data to database");
		int updated = -1;
		String tableName = "mobTemplates";
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set name=?, subTitle=?, mobType=?, soundSet=?, display1=?,"
					+ "display2=?, display3=?, display4=?, baseAnimationState=?, scale=?, overheadOffset=?, hitBox=?,"
					+ "runThreshold=?, gender=?, level=?, attackable=?, faction=?, species=?, subSpecies=?, questCategory=? where id=?");
			stmt.setString(1, name);
			stmt.setString(2, subtitle);
			stmt.setInt(3, mobType);
			stmt.setInt(4, soundSet);
			for (int i = 0; i <= 4; i++) {
				if (displays.size() > i)
					stmt.setInt(5 + i, displays.get(i));
				else
					stmt.setInt(5 + i, -1);
			}
			stmt.setInt(9, animState);
			stmt.setFloat(10, scale);
			stmt.setInt(11, offset);
			stmt.setInt(12, hitBox);
			stmt.setInt(13, runThreshold);
			stmt.setString(14, gender);
			stmt.setInt(15, level);
			stmt.setBoolean(16, attackable);
			stmt.setInt(17, faction);
			stmt.setString(18, species);
			stmt.setString(19, subSpecies);
			stmt.setString(20, questCategory);
			stmt.setInt(21, templateID);
			Log.debug("MOBDB: placing mob stmt=" + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
		} finally {
			queries.closeStatement(stmt);
		}
		Log.debug("Wrote mob data to database");
		return updated;
	}
	
	public int editMobCombatData(int mobID, int health, String attackType) {
		Log.debug("Writing mob combat data to database");
		String tableName = "mobCombatStats";
		String updateString = "UPDATE `" + tableName + "` set health=" + health + ", attackType='" + attackType + 
		 "' where id=" + mobID;
		int updated = queries.executeUpdate(updateString);
		Log.debug("Wrote mob combat data to database");
		return updated;
	}
	
	public int deleteMobEquipmentData(int mobID, int item) {
		Log.debug("Deleting mob equip data to database");
		String tableName = "mobEquipment";
		String deleteString = "DELETE FROM `" + tableName + "` WHERE mobTemplate = " + mobID + " AND item = " + item;
		int deleted = queries.executeUpdate(deleteString);
		Log.debug("Deleting mob equip data to database");
		return deleted;
	}
	
	public int writeFactionData(int category, String name, String group, boolean isPublic, int defaultStance) {
		Log.debug("Writing faction data to database");
		String tableName = "factions";
		String columnNames = "category,name,factionGroup,public,defaultStance";
		String values = category + ",'" + name + "','" + group + "'," + isPublic + "," + defaultStance;
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote faction data to database:\n"+insertString);
		return inserted;
	}
	
	public int writeFactionStanceData(int factionID, int otherFaction, int defaultStance) {
		Log.debug("Writing faction stance data to database");
		String tableName = "faction_stances";
		String columnNames = "factionID,otherFaction,defaultStance";
		String values = factionID + "," + otherFaction + "," + defaultStance;
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote faction stance data to database");
		return inserted;
	}
	
	public int writeLootTable(int category, LootTable lTbl) {
		Log.debug("Writing loot table data to database");
		String tableName = "lootTables";
		String columnNames = "category,name";
		String values = category + ",'" + lTbl.getName() + "'";
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote loot table data to database");
		return inserted;
	}
	
	public int editLootTable(int tableID, LootTable lTable) {
		Log.debug("Writing loot table data to database");
		String tableName = "lootTables";
		String updateString = "UPDATE `" + tableName + "` set name='" + lTable.getName() + 
		 "' where id=" + tableID;
		int updated = queries.executeUpdate(updateString);
		// Now delete all the old items from the table to insert the new ones
		tableName = "lootTableDrops";
		updateString = "DELETE from " + tableName + " where lootTable=" + tableID;
		queries.executeUpdate(updateString);
		Log.debug("Wrote loot table data to database");
		return updated;
	}
	
	public int writeLootTableDrops(int tableID, int item, int itemCount, int dropChance) {
		Log.debug("Writing loot table data to database");
		String tableName = "lootTableDrops";
		String columnNames = "lootTable,item,itemCount,dropChance";
		String values = tableID + "," + item + "," + itemCount + "," + dropChance;
		String insertString = "INSERT INTO `" + tableName + "` (" + columnNames + ") VALUES (" + values + ")";
		int inserted = queries.executeInsert(insertString);
		Log.debug("Wrote loot table data to database");
		return inserted;
	}
	
	/**
	 * Unused.
	 * @param instance
	 * @return
	 */
	public HashMap<Integer, ResourceGrid> loadResourceGrids(String instance) {
		HashMap<Integer, ResourceGrid> grids = new HashMap<Integer, ResourceGrid>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM `resource_grids` where instance=?");
			ps.setString(1, instance);
			Log.debug("GRID: " + ps.toString());
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					ResourceGrid bg = new ResourceGrid();
					bg.setID(rs.getInt("id"));
					bg.setInstance(instance);
					Point p = new Point();
					p.setX(rs.getFloat("locX"));
					p.setY(rs.getFloat("locY"));
					p.setZ(rs.getFloat("locZ"));
					bg.setPosition(p);
					bg.setResourceType(HelperFunctions.readEncodedString(rs.getBytes("type")));
					bg.setCount(rs.getInt("count"));
					bg.setRotation(rs.getFloat("rotation"));
					grids.put(bg.getID(), bg);
					//Log.debug("GRID: added grid " + bg.getID() + " to map");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return grids;
	}
	
	public int resourceGridUpdated(ResourceGrid grid) {
		Log.debug("GRID: Updating resource grid in the database");
		String tableName = "resource_grids";
		int updated = -1;
		PreparedStatement stmt = null;
		try {
			stmt = queries.prepare("UPDATE " + tableName + " set count=? where id=?");
			stmt.setInt(1, grid.getCount());
			stmt.setInt(2, grid.getID());
			Log.debug("MOBDB: placing resource grid stmt=" + stmt.toString());
			updated = queries.executeUpdate(stmt);
		} catch (SQLException e) {
		}
		Log.debug("GRID: Updated resource grid data in the database");
		return updated;
	}
	
	/**
	 * Loads in the Dialogues in the World Content Database
	 * @return
	 */
	public HashMap<Integer, Dialogue> loadDialogues() {
		HashMap<Integer, Dialogue> dialogues = new HashMap<Integer, Dialogue>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = queries.prepare("SELECT * FROM dialogue where isactive = 1");
			rs = queries.executeSelect(ps);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					Dialogue d = new Dialogue(id, HelperFunctions.readEncodedString(rs.getBytes("name")), HelperFunctions.readEncodedString(rs.getBytes("text")));
					d.setOpeningDialogue(rs.getBoolean("openingDialogue"));
					d.setRepeatable(rs.getBoolean("repeatable"));
					
					// Load in options
					for (int i = 1; i < 3; i++) {
						String action = HelperFunctions.readEncodedString(rs.getBytes("option" + i + "action"));
						int actionID = rs.getInt("option" + i + "actionID");
						// Only add the option if it has an action and actionID defined
						if (action != null && !action.equals("") && actionID > 0) {
							String text = HelperFunctions.readEncodedString(rs.getBytes("option" + i + "text"));
							d.addOption(text, action, actionID);
						}
					}
					
					dialogues.put(id, d);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			queries.closeStatement(ps, rs);
		}
		return dialogues;
	}
	
	/**
	 * Having "too many connections" errors, so adding this function to help cope with it
	 */
	public void close() {
		queries.close();
	}

	private static final boolean SKINNING_ENABLED = false;
}
