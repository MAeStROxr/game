package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.ClassAbilityClient;

/**
 * Effect child class that permanently alters the level of a skill.
 * Not tested in a long time.
 * @author Andrew Harrison
 *
 */
public class AlterSkillCurrentEffect extends AgisEffect {

    public AlterSkillCurrentEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getSource();
		
		if (skillType == -1) {
			// Update all skills
    	    for (int skillID : obj.getCurrentSkillInfo().getSkills().keySet()) {
    	    	ClassAbilityClient.skillAlterCurrent(obj.getOwnerOid(), skillID, alterValue);
    	    }
		} else {
			ClassAbilityClient.skillAlterCurrent(obj.getOwnerOid(), skillType, alterValue);
		}
    }

    public int getSkillType() { return skillType; }
    public void setSkillType(int skillType) { this.skillType = skillType; }
    protected int skillType = -1;
    
    public int getAlterValue() { return alterValue; }
    public void setAlterValue(int alterValue) { this.alterValue = alterValue; }
    protected int alterValue = -1;
    
    private static final long serialVersionUID = 1L;
}
