package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.VoxelClient;
import atavism.server.engine.Engine;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;

/**
 * Effect child class that creates a Claim at the casters location.
 * @author Andrew Harrison
 *
 */
public class CreateClaimEffect extends AgisEffect {

	public CreateClaimEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getTarget();
		CombatInfo caster = state.getSource();
		
		ExtensionMessage createMsg = new ExtensionMessage(caster.getOwnerOid());
		createMsg.setMsgType(VoxelClient.MSG_TYPE_CREATE_CLAIM);
		Point loc = WorldManagerClient.getObjectInfo(caster.getOwnerOid()).loc;
		createMsg.setProperty("loc", new AOVector(loc));
        Engine.getAgent().sendBroadcast(createMsg);
    }
    
    private static final long serialVersionUID = 1L;
}
