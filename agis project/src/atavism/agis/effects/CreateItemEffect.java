package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.server.engine.Namespace;
import atavism.server.engine.OID;
import atavism.server.objects.Template;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.ObjectManagerPlugin;

/**
 * Effect child class that creates an item and gives it to the caster. 
 * Can create multiple of the same item, but only of one type.
 * @author Andrew Harrison
 *
 */
public class CreateItemEffect extends AgisEffect {

    public CreateItemEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		OID playerOid = state.getSource().getOwnerOid();
        OID bagOid = playerOid;
		
		Template overrideTemplate = new Template();
        overrideTemplate.put(Namespace.OBJECT_MANAGER,
                ObjectManagerClient.TEMPL_PERSISTENT, true);

        OID itemOid = ObjectManagerClient.generateObject(item, ObjectManagerPlugin.ITEM_TEMPLATE, overrideTemplate);
        InventoryClient.addItem(bagOid, playerOid, bagOid, itemOid);
    }

    protected int item = -1;
    public int getItem() { return this.item; }
    public void setItem(int template) { this.item = template; }
    
    protected int numberToCreate = 1;
    public int getNumberToCreate() { return numberToCreate; }
    public void setNumberToCreate(int numberToCreate) { this.numberToCreate = numberToCreate; }
    
    private static final long serialVersionUID = 1L;
}
