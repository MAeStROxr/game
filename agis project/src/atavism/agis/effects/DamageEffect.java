package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.plugins.CombatClient;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.Engine;
import atavism.server.util.Log;

import java.util.Random;

public class DamageEffect extends AgisEffect {
	
	static Random random = new Random();

    public DamageEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		
		CombatInfo target = state.getTarget();
	    CombatInfo caster = state.getSource();
	    String abilityEvent = EventMessageHelper.COMBAT_PHYSICAL_DAMAGE;
	    int dmg = minDmg;
	        	
	    if (Log.loggingDebug)
	        Log.debug("DamageEffect.apply: doing instant damage to obj=" + target +
	        		" from=" + caster);
	    
	    if (maxDmg > minDmg) {
	            dmg += random.nextInt(maxDmg - minDmg);
	    }
	    	    
	    int targetHealth = target.statGetCurrentValue(getDamageProperty());
        if (dmg >= targetHealth) {
        	// Check if this will cause a duel defeat
        	if (checkDuelDefeat(target, caster, getDamageProperty())) {
        		dmg = targetHealth - 1;
        	}
        }
	            
	    // Hopefully this will make the ai respond even if the attack was a miss etc.
	    target.statModifyBaseValue(getDamageProperty(), -dmg);
	    target.statSendUpdate(true);
	    
	    Engine.getAgent().sendBroadcast(new CombatClient.DamageMessage(target.getOwnerOid(),
	    		state.getSource().getOwnerOid(), dmg, damageType));
	    	    
	    EventMessageHelper.SendCombatEvent(state.getSourceOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), dmg, -1);        
	    
	    // If there is a pulse coord effect, run it now
        if (pulseCoordEffect != null && !pulseCoordEffect.isEmpty()) {
        	CoordinatedEffect cE = new CoordinatedEffect(pulseCoordEffect);
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.invoke(target.getOwnerOid(), target.getOwnerOid());
        }
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
        int dmg = minPulseDmg;

        if (maxPulseDmg > minPulseDmg) {
            dmg += random.nextInt(maxPulseDmg - minPulseDmg);
        }
        
        if(dmg > 0){
        	CombatInfo obj = state.getTarget();
        	//CombatInfo caster = state.getSource();
            
            obj.statModifyBaseValue(getDamageProperty(), -dmg);
            obj.sendStatusUpdate();
	        Engine.getAgent().sendBroadcast(new CombatClient.DamageMessage(obj.getOwnerOid(),
							  state.getSource().getOwnerOid(), dmg, damageType));
	        
	        String abilityEvent = EventMessageHelper.COMBAT_PHYSICAL_DAMAGE;
	        EventMessageHelper.SendCombatEvent(state.getSourceOid(), obj.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), dmg, -1);    
        }
    }
    
    // remove the effect from the object
    public void remove(EffectState state) {
	    //CombatInfo obj = state.getTarget();
	    // Remove the effectVal on the object

	    super.remove(state);
    }

    public int getMinInstantDamage() { return minDmg; }
    public void setMinInstantDamage(int hps) { minDmg = hps; }
    protected int minDmg = 0;

    public int getMaxInstantDamage() { return maxDmg; }
    public void setMaxInstantDamage(int hps) { maxDmg = hps; }
    protected int maxDmg = 0;

    public int getMinPulseDamage() { return minPulseDmg; }
    public void setMinPulseDamage(int hps) { minPulseDmg = hps; }
    protected int minPulseDmg = 0;

    public int getMaxPulseDamage() { return maxPulseDmg; }
    public void setMaxPulseDamage(int hps) { maxPulseDmg = hps; }
    protected int maxPulseDmg = 0;

    public String getDamageProperty() { return damageProperty; }
    public void setDamageProperty(String property) { damageProperty = property; }
    protected String damageProperty = CombatInfo.COMBAT_PROP_HEALTH;

    public String getDamageType() { return damageType; }
    public void setDamageType(String damageType) { this.damageType = damageType; }
    protected String damageType = "";
    
    public float getDamageMod() { return DamageMod; }
    public void setDamageMod(float hps) { DamageMod = hps; }
    protected float DamageMod = 1.0f;
    
    public String getPulseCoordEffect() { return pulseCoordEffect; }
    public void setPulseCoordEffect(String coordEffect) { pulseCoordEffect = coordEffect; }
    protected String pulseCoordEffect;
    
    // Effect Value that needs to be removed upon effect removal
    public void SetEffectVal(int effect) {
    	effectVal = effect;
    }
    public int GetEffectVal() {
        return effectVal;
    }
    public int effectVal = 0;
    
    private static final long serialVersionUID = 1L;
}
