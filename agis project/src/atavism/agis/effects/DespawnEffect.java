package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.TamedPet;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.messages.PropertyMessage;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

/**
 * Effect child class that despawns an object. 
 * Can be used to despawn a pet or any other object.
 * @author Andrew Harrison
 *
 */
public class DespawnEffect extends AgisEffect {

    public DespawnEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getTarget();
		CombatInfo caster = state.getSource();
		
		if (despawnType == 3) {
			TamedPet oldPet = (TamedPet) EnginePlugin.getObjectProperty(caster.getOwnerOid(), WorldManagerClient.NAMESPACE, "CombatPet");
	    	if (oldPet != null) {
	    		oldPet.despawnPet();
	    	} else {
	    		String petKey = (String) EnginePlugin.getObjectProperty(caster.getOwnerOid(), WorldManagerClient.NAMESPACE, "activePet");
	    		TamedPet pet = (TamedPet) ObjectManagerClient.loadObjectData(petKey);
        		pet.despawnPet();
	    	}
		} else if (despawnType == 0) {
			WorldManagerClient.despawn(obj.getOid());
		} else {
			PropertyMessage propMsg = new PropertyMessage(obj.getOwnerOid());
	        propMsg.setProperty("tamed", true);
	        Engine.getAgent().sendBroadcast(propMsg);
		}
        
        Log.debug("DESPAWNEFFECT: despawning object: " + obj.getOwnerOid());
    }
    
    /**
     * 0: The effect target
     * 3: Combat pet (player can control it with commands)
     */
    protected int despawnType = 0;
    public int getDespawnType() { return despawnType; }
    public void setDespawnType(int despawnType) { this.despawnType = despawnType; }
    
    
    public int getMobID() { return mobID; }
    public void setMobID(int mobID) { this.mobID = mobID; }
    protected int mobID = -1;
    
    private static final long serialVersionUID = 1L;
}
