package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.EventMessageHelper;

import java.util.Map;
import java.util.Random;

/**
 * Effect child class that restores health instantly.
 * @author Andrew Harrison
 *
 */
public class HealInstantEffect extends AgisEffect {
	
	static Random random = new Random();

    public HealInstantEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		Map<String, Integer> params = state.getParams();
        effectSkillType = params.get("skillType");
        
        String abilityEvent = EventMessageHelper.COMBAT_HEAL;
        
        int heal = minHeal;
        if (maxHeal > minHeal) {
        	heal += random.nextInt(maxHeal - minHeal);
        }
        heal = CombatHelper.CalcHeal(state.getTarget(), state.getSource(), heal, skillEffectMod.get(0), effectSkillType);
        
        if (heal == 0) {
		    return;
		}

        CombatInfo target = state.getTarget();
        CombatInfo caster = state.getSource();
        
        target.statModifyBaseValue(getHealProperty(), heal);
        target.sendStatusUpdate();
        
        EventMessageHelper.SendCombatEvent(caster.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), heal, -1);
        
        // If there is a pulse coord effect, run it now
        if (pulseCoordEffect != null && !pulseCoordEffect.isEmpty()) {
        	CoordinatedEffect cE = new CoordinatedEffect(pulseCoordEffect);
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.invoke(target.getOwnerOid(), target.getOwnerOid());
        }
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
		super.pulse(state);
		
		String abilityEvent = EventMessageHelper.COMBAT_HEAL;
		
        int heal = minPulseHeal;
        if (maxPulseHeal > minPulseHeal) {
            heal += random.nextInt(maxPulseHeal - minPulseHeal);
        }
		if (heal == 0) {
		    return;
		}
		
        CombatInfo target = state.getTarget();
        CombatInfo caster = state.getSource();
        target.statModifyBaseValue(getHealProperty(), heal);
        target.sendStatusUpdate();
        
        EventMessageHelper.SendCombatEvent(caster.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), heal, -1);
    }

    public int getMinInstantHeal() { return minHeal; }
    public void setMinInstantHeal(int hps) { minHeal = hps; }
    protected int minHeal = 0;

    public int getMaxInstantHeal() { return maxHeal; }
    public void setMaxInstantHeal(int hps) { maxHeal = hps; }
    protected int maxHeal = 0;

    public int getMinPulseHeal() { return minPulseHeal; }
    public void setMinPulseHeal(int hps) { minPulseHeal = hps; }
    protected int minPulseHeal = 0;

    public int getMaxPulseHeal() { return maxPulseHeal; }
    public void setMaxPulseHeal(int hps) { maxPulseHeal = hps; }
    protected int maxPulseHeal = 0;

    public String getHealProperty() { return healProperty; }
    public void setHealProperty(String property) { healProperty = property; }
    protected String healProperty = CombatInfo.COMBAT_PROP_HEALTH;
    
    public String getPulseCoordEffect() { return pulseCoordEffect; }
    public void setPulseCoordEffect(String coordEffect) { pulseCoordEffect = coordEffect; }
    protected String pulseCoordEffect;
    
    // Effect Value that needs to be removed upon effect removal
    public void setEffectVal(int effect) {
    	effectVal = effect;
    }
    public int GetEffectVal() {
        return effectVal;
    }
    public int effectVal = 0;
    
    public void setEffectName(String eName) {
    	effectName = eName;
    }
    public String getEffectName() {
	return effectName;
    }
    protected String effectName = "";
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    public void setEffectSkillType(int type) {
    	effectSkillType = type;
    }
    public int GetEffectSkillType() {
        return effectSkillType;
    }
    public int effectSkillType = 0;
    
    private static final long serialVersionUID = 1L;
}
