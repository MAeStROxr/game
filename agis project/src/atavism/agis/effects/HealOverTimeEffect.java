package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.EventMessageHelper;
import atavism.server.util.Log;

import java.util.Map;
import java.util.Random;

/**
 * Effect child class that restores health over time.
 * 
 * @author Andrew Harrison
 */
public class HealOverTimeEffect extends AgisEffect {
    static Random random = new Random();

    public HealOverTimeEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
	    Map<String, Integer> params = state.getParams();
        //int effectNum = params.get("effectVal");
        effectSkillType = params.get("skillType");
        
        String abilityEvent = EventMessageHelper.COMBAT_BUFF_GAINED;
        
        CombatInfo target = state.getTarget();
        CombatInfo source = state.getSource();
  
        int heal = minHeal;
        if (maxHeal > minHeal) {
        	heal += random.nextInt(maxHeal - minHeal);
        }
        heal = CombatHelper.CalcHeal(target, source, heal, skillEffectMod.get(0), effectSkillType);
        
        // Divide the total damage by the number of pulses
        pulseHeal = heal / this.numPulses;
        
        EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), -1, -1);
        Log.debug("PULSECALC: total heal is: " + heal + " with pulse heal: " + pulseHeal);
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    	
    	String abilityEvent = EventMessageHelper.COMBAT_HEAL;
    	
    	CombatInfo target = state.getTarget();
        CombatInfo source = state.getSource();
        
        if (pulseHeal > 0) {
        	Log.debug("PULSE: giving heal: " + pulseHeal);
            
        	target.statModifyBaseValue(getHealProperty(), pulseHeal);
            target.sendStatusUpdate();
	        //Engine.getAgent().sendBroadcast(new CombatClient.DamageMessage(target.getOwnerOid(),
	        //		source.getOwnerOid(), pulseHeal, this.damageType));
	        
	        // If there is a pulse coord effect, run it now
	        if (pulseCoordEffect != null && !pulseCoordEffect.isEmpty()) {
	        	CoordinatedEffect cE = new CoordinatedEffect(pulseCoordEffect);
	    	    cE.sendSourceOid(true);
	    	    cE.sendTargetOid(true);
	    	    cE.invoke(target.getOwnerOid(), target.getOwnerOid());
	        }
	        
	        EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), pulseHeal, -1);
        }
    }

    public int getMinHeal() { return minHeal; }
    public void setMinHeal(int hps) { minHeal = hps; }
    protected int minHeal = 0;

    public int getMaxHeal() { return maxHeal; }
    public void setMaxHeal(int hps) { maxHeal = hps; }
    protected int maxHeal = 0;
    
    protected int pulseHeal = 0;
    
    public String getPulseCoordEffect() { return pulseCoordEffect; }
    public void setPulseCoordEffect(String coordEffect) { pulseCoordEffect = coordEffect; }
    protected String pulseCoordEffect;

    public String getHealProperty() { return healProperty; }
    public void setHealProperty(String property) { healProperty = property; }
    protected String healProperty = CombatInfo.COMBAT_PROP_HEALTH;
    
    public float getHealMod() { return HealMod; }
    public void setHealMod(float hps) { HealMod = hps; }
    protected float HealMod = 1.0f;
    
    // Effect Value that needs to be removed upon effect removal
    public void setEffectVal(int effect) {
    	effectVal = effect;
    }
    public int GetEffectVal() {
        return effectVal;
    }
    public int effectVal = 0;
    
    public void setDisplayName(String eName) {
    	displayName = eName;
    }
    public String getDisplayName() {
	return displayName;
    }
    protected String displayName = "";
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    public void setEffectSkillType(int type) {
    	effectSkillType = type;
    }
    public int GetEffectSkillType() {
        return effectSkillType;
    }
    public int effectSkillType = 0;
    
    private static final long serialVersionUID = 1L;
}
