package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.EventMessageHelper;

import java.util.Random;

/**
 * Effect child class that takes health from the caster and gives it to the target. Happens instantly.
 * @author Andrew Harrison
 *
 */
public class HealthTransferEffect extends AgisEffect {
	
	static Random random = new Random();

    public HealthTransferEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		
		int heal = minHeal;
        if (maxHeal > minHeal) {
        	heal += random.nextInt(maxHeal - minHeal);
        }
        heal = CombatHelper.CalcHeal(state.getTarget(), state.getSource(), heal, skillEffectMod.get(0), effectSkillType);
        
        String abilityEvent = EventMessageHelper.COMBAT_HEALTH_TRANSFER;
        
        CombatInfo caster = state.getSource();
        int casterHealth = caster.statGetCurrentValue(getHealProperty());
        if (heal > casterHealth)
        	heal = casterHealth - 1;

        CombatInfo target = state.getTarget();
		if (heal < 1) {
		    return;
		}
		caster.statModifyBaseValue(getHealProperty(), -heal);
        caster.sendStatusUpdate();
        double healModified = heal * transferModifier;
        int newHeal = (int) healModified;
        target.statModifyBaseValue(getHealProperty(), newHeal);
        target.sendStatusUpdate();
        
        EventMessageHelper.SendCombatEvent(caster.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), heal, -1);
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
		super.pulse(state);
        int heal = minPulseHeal;
        
        String abilityEvent = EventMessageHelper.COMBAT_HEALTH_TRANSFER;
        
        CombatInfo caster = state.getSource();
        int casterHealth = caster.statGetCurrentValue(getHealProperty());
        if (heal > casterHealth)
        	heal = casterHealth - 1;

		if (heal < 1) {
		    return;
		}
		caster.statModifyBaseValue(getHealProperty(), -heal);
        caster.sendStatusUpdate();
        double healModified = heal * transferModifier;
        int newHeal = (int) healModified;
        CombatInfo target = state.getTarget();
        target.statModifyBaseValue(getHealProperty(), newHeal);
        target.sendStatusUpdate();
        
        EventMessageHelper.SendCombatEvent(caster.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), heal, -1);
    }

    public int getMinInstantHeal() { return minHeal; }
    public void setMinInstantHeal(int hps) { minHeal = hps; }
    protected int minHeal = 0;

    public int getMaxInstantHeal() { return maxHeal; }
    public void setMaxInstantHeal(int hps) { maxHeal = hps; }
    protected int maxHeal = 0;

    public int getMinPulseHeal() { return minPulseHeal; }
    public void setMinPulseHeal(int hps) { minPulseHeal = hps; }
    protected int minPulseHeal = 0;

    public int getMaxPulseHeal() { return maxPulseHeal; }
    public void setMaxPulseHeal(int hps) { maxPulseHeal = hps; }
    protected int maxPulseHeal = 0;

    public String getHealProperty() { return healProperty; }
    public void setHealProperty(String property) { healProperty = property; }
    protected String healProperty = CombatInfo.COMBAT_PROP_HEALTH;
    
    public double getTransferModifier() { return transferModifier; }
    public void setTransferModifier(double modifier) { transferModifier = modifier; }
    protected double transferModifier = 1.0;
    
    private static final long serialVersionUID = 1L;
}
