package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.CombatClient;
import atavism.agis.util.EventMessageHelper;
import atavism.server.util.Log;

import java.util.Map;

/**
 * Effect child class that makes the target unattackable for a period of time.
 * @author Andrew Harrison
 *
 */
public class ImmuneEffect extends AgisEffect {
    public ImmuneEffect(int id, String name) {
    	super(id, name);
    	isPeriodic(false);
    	isPersistent(true);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
    	Map<String, Integer> params = state.getParams();
        effectSkillType = params.get("skillType");
        
        String abilityEvent = EventMessageHelper.COMBAT_BUFF_GAINED;
        
        CombatInfo target = state.getTarget();
        CombatClient.setCombatInfoState(target.getOid(), CombatInfo.COMBAT_STATE_IMMUNE);
        
        EventMessageHelper.SendCombatEvent(state.getSourceOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), -1, -1);
    }

    // remove the effect from the object
    public void remove(EffectState state) {
    	CombatInfo target = state.getTarget();
        boolean anotherEffectExists = false;
        
        if (getTargetEffectsOfMatchingType(target).size() > 1) {
        	anotherEffectExists = true;
        	Log.debug("IMMUNEEFFECT: found another immune effect so will not remove state");
        }
        if (!anotherEffectExists) {
            CombatClient.clearCombatInfoState(target.getOid(), CombatInfo.COMBAT_STATE_IMMUNE);
        }
	    super.remove(state);
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    }
    
 // Effect Value that needs to be removed upon effect removal
    public void setEffectVal(int effect) {
    	effectVal = effect;
    }
    public int GetEffectVal() {
        return effectVal;
    }
    public int effectVal = 0;
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    public void setEffectSkillType(int type) {
    	effectSkillType = type;
    }
    public int GetEffectSkillType() {
        return effectSkillType;
    }
    public int effectSkillType = 0;
    
    private static final long serialVersionUID = 1L;
}