package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CombatPlugin;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.Engine;
import atavism.server.util.Log;

import java.util.Map;
import java.util.Random;

/**
 * Effect child class that deals Magical Damage over time to the target.
 * @author Andrew Harrison
 *
 */
public class MagicalDotEffect extends AgisEffect {
    static Random random = new Random();

    public MagicalDotEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
	    Map<String, Integer> params = state.getParams();
        effectSkillType = params.get("skillType");
        hitRoll = params.get("hitRoll");
        
        String abilityEvent = EventMessageHelper.COMBAT_DEBUFF_GAINED;
        
        CombatInfo target = state.getTarget();
        CombatInfo source = state.getSource();
  
        int dmg = minDmg;
        if (maxDmg > minDmg) {
            dmg += random.nextInt(maxDmg - minDmg);
        }
        if (CombatPlugin.MAGICAL_ATTACKS_USE_WEAPON_DAMAGE) {
    		dmg += source.statGetCurrentValue("dmg-base");
    	}
        dmg = CombatHelper.CalcMagicalDamage(target, source, dmg, damageType, 
        		skillEffectMod.get(0), effectSkillType, 100, false);
        
        // Divide the total damage by the number of pulses
        pulseDamage = dmg / this.numPulses;
        if (pulseDamage == 0)
        	pulseDamage = 1;
        
        EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), -1, -1);
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    	Log.debug("DOT: pulsing effect: " + state.getEffectName() + " with damage: " + pulseDamage);
    	
    	String abilityEvent = EventMessageHelper.COMBAT_MAGICAL_DAMAGE;
    	
    	CombatInfo target = state.getTarget();
        CombatInfo source = state.getSource();
    	
        int targetHealth = target.statGetCurrentValue(getDamageProperty());
        if (pulseDamage >= targetHealth) {
        	// Check if this will cause a duel defeat
        	if (checkDuelDefeat(target, source, getDamageProperty())) {
        		pulseDamage = targetHealth - 1;
        	}
        }
        
        if (pulseDamage > 0) {
        	target.statModifyBaseValue(getDamageProperty(), -pulseDamage);
        	target.sendStatusUpdate();
	        Engine.getAgent().sendBroadcast(new CombatClient.DamageMessage(target.getOwnerOid(),
							  state.getSource().getOwnerOid(), pulseDamage, this.damageType));
        }
        
        // If there is a pulse coord effect, run it now
        if (pulseCoordEffect != null && !pulseCoordEffect.isEmpty()) {
        	CoordinatedEffect cE = new CoordinatedEffect(pulseCoordEffect);
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.invoke(target.getOwnerOid(), target.getOwnerOid());
        }
        
        EventMessageHelper.SendCombatEvent(source.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), pulseDamage, -1);
    }

    public int getMinDamage() { return minDmg; }
    public void setMinDamage(int hps) { minDmg = hps; }
    protected int minDmg = 0;

    public int getMaxDamage() { return maxDmg; }
    public void setMaxDamage(int hps) { maxDmg = hps; }
    protected int maxDmg = 0;
    
    protected int pulseDamage = 0;
    
    public String getPulseCoordEffect() { return pulseCoordEffect; }
    public void setPulseCoordEffect(String coordEffect) { pulseCoordEffect = coordEffect; }
    protected String pulseCoordEffect;

    public String getDamageProperty() { return damageProperty; }
    public void setDamageProperty(String property) { damageProperty = property; }
    protected String damageProperty = CombatInfo.COMBAT_PROP_HEALTH;
    
    public float getDamageMod() { return DamageMod; }
    public void setDamageMod(float hps) { DamageMod = hps; }
    protected float DamageMod = 1.0f;
    
    // Effect Value that needs to be removed upon effect removal
    public void setEffectVal(int effect) {
    	effectVal = effect;
    }
    public int GetEffectVal() {
        return effectVal;
    }
    public int effectVal = 0;
    
    public void setEffectName(String eName) {
    	effectName = eName;
    }
    public String getEffectName() {
	return effectName;
    }
    protected String effectName = "";
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    public void setEffectSkillType(int type) {
    	effectSkillType = type;
    }
    public int GetEffectSkillType() {
        return effectSkillType;
    }
    public int effectSkillType = 0;
    
    public void setHitRoll(int roll) {
    	hitRoll = roll;
    }
    public int GetHitRoll() {
        return hitRoll;
    }
    public int hitRoll = 0;
    
    private static final long serialVersionUID = 1L;
}
