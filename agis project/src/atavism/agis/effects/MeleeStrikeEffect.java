package atavism.agis.effects;

import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.CoordinatedEffect;
import atavism.agis.plugins.CombatClient;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.Engine;
import atavism.server.util.Log;

import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

/**
 * Effect child class that deals Physical Damage to the target, reducing one of their value stats (e.g Health or Mana)
 * @author Andrew Harrison
 *
 */
public class MeleeStrikeEffect extends AgisEffect {
	
	static Random random = new Random();

    public MeleeStrikeEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
	    Map<String, Integer> params = state.getParams();
	    Log.debug("RESULT: effect params is: " + params);
        int result = params.get("result");
        effectSkillType = params.get("skillType");
        hitRoll = params.get("hitRoll");
        
        String abilityEvent = EventMessageHelper.COMBAT_PHYSICAL_DAMAGE;
        
        CombatInfo target = state.getTarget();
        CombatInfo caster = state.getSource();
        int dmg = 0;
        
        switch (result) {
        	case AgisAbility.RESULT_MISSED:
        		abilityEvent = EventMessageHelper.COMBAT_MISSED;
            break;
        	case AgisAbility.RESULT_PARRIED:	
        		abilityEvent = EventMessageHelper.COMBAT_PARRIED;
            break;
        	case AgisAbility.RESULT_DODGED:
        		abilityEvent = EventMessageHelper.COMBAT_DODGED;
            break;
        	case AgisAbility.RESULT_EVADED:
        		abilityEvent = EventMessageHelper.COMBAT_EVADED;
        		break;
        	case AgisAbility.RESULT_IMMUNE:
        		abilityEvent = EventMessageHelper.COMBAT_IMMUNE;
        		break;
            default:
            	dmg = minDmg;
            	// Get level difference
            	// Now lets take the players stats into account
	        	dmg += caster.statGetCurrentValue("dmg-base");
	        	dmg = CombatHelper.CalcMeleeDamage(target, caster, dmg, damageType, 
	        			skillEffectMod.get(0), effectSkillType, hitRoll, true);
	        	// Take in the effect base damage modifiers
	        	if (DamageMod != 1.0f) {
	        		Log.debug("MELEESTRIKE: DamageMod: " + DamageMod + " Damage: " + dmg);
	        	    float dmgF = (float)dmg * DamageMod;
	        	    Log.debug("MELEESTRIKE: DamageFloat: " + dmgF);
	        	    dmg = Math.round(dmgF);
	        	    Log.debug("MELEESTRIKE: Damage: " + dmg);
	        	}
	        	
	        	// If this ability uses any effects to boost up dmg (and consumes the effect 
	        	// at the same time
	        	if (bonusDmgEffectVals != null) {
	        		Log.debug("ANDREW - effect has bonusDmgEffectVal; effects required: " + 
	        				bonusDmgEffectVals.toString());
	        		for (int i = 0; i < bonusDmgEffectVals.size(); i++) {
	        			boolean effectPresent = false;
		    		    for (EffectState existingState : caster.getCurrentEffects()) {
		    			    if (bonusDmgEffectVals.get(i) == existingState.getEffect().getID()) {
		    				    effectPresent = true;
		    		    	}
		    		    }
	    		    	if (effectPresent) {
	    		    		AgisEffect.removeEffectByID(target, bonusEffectReq);
				    	
	    		    		dmg = dmg + bonusDmgVals.get(i);
	    		    		Log.debug("ANDREW - removed effect position: " + effectPresent + 
	    		    				"; and boosted dmg by: " + bonusDmgVals.get(i));
	    		    	}
	        	    }
	        	}
	        	
	        	switch (result) {
			        // Finally lets check for critical or blocks
			        case 6:
			        	dmg = dmg / 2;
			        	abilityEvent = EventMessageHelper.COMBAT_BLOCKED;
			        	break;
			        case 2:
		        		dmg = dmg * 2;
		        		abilityEvent = EventMessageHelper.COMBAT_PHYSICAL_CRITICAL;
		        		break;
	        	}
        	
	        	if (Log.loggingDebug)
	                Log.debug("DamageEffect.apply: doing instant damage to obj=" + state.getTarget() +
	    		      " from=" + state.getSource());
        }
                
        // Check if applying the damage from this effect is going to kill the target
        int targetHealth = target.statGetCurrentValue(getDamageProperty());
        if (dmg >= targetHealth) {
        	// Check if this will cause a duel defeat
        	if (checkDuelDefeat(target, caster, getDamageProperty())) {
        		dmg = targetHealth - 1;
        	}
        }
        
        // Hopefully this will make the ai respond even if the attack was a miss etc.
        target.statModifyBaseValue(getDamageProperty(), -dmg);
        target.statSendUpdate(true);
        
        //if (dmg > 0)
	    Engine.getAgent().sendBroadcast(new CombatClient.DamageMessage(target.getOwnerOid(),
						state.getSource().getOwnerOid(), dmg, damageType));
        
	    EventMessageHelper.SendCombatEvent(caster.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), dmg, -1);
	    
	    // If there is a pulse coord effect, run it now
        if (pulseCoordEffect != null && !pulseCoordEffect.isEmpty()) {
        	CoordinatedEffect cE = new CoordinatedEffect(pulseCoordEffect);
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.invoke(target.getOwnerOid(), target.getOwnerOid());
        }
    }

    public int getMinInstantDamage() { return minDmg; }
    public void setMinInstantDamage(int hps) { minDmg = hps; }
    protected int minDmg = 0;

    public int getMaxInstantDamage() { return maxDmg; }
    public void setMaxInstantDamage(int hps) { maxDmg = hps; }
    protected int maxDmg = 0;

    public int getMinPulseDamage() { return minPulseDmg; }
    public void setMinPulseDamage(int hps) { minPulseDmg = hps; }
    protected int minPulseDmg = 0;

    public int getMaxPulseDamage() { return maxPulseDmg; }
    public void setMaxPulseDamage(int hps) { maxPulseDmg = hps; }
    protected int maxPulseDmg = 0;

    public String getDamageProperty() { return damageProperty; }
    public void setDamageProperty(String property) { damageProperty = property; }
    protected String damageProperty = CombatInfo.COMBAT_PROP_HEALTH;
    
    public float getDamageMod() { return DamageMod; }
    public void setDamageMod(float hps) { DamageMod = hps; }
    protected float DamageMod = 1.0f;
    
    public String getPulseCoordEffect() { return pulseCoordEffect; }
    public void setPulseCoordEffect(String coordEffect) { pulseCoordEffect = coordEffect; }
    protected String pulseCoordEffect;
    
    // List of bonus damage effect values
    public void addBonusDmgEffectVal(int effect) {
    	bonusDmgEffectVals.add(effect);
    }
    public LinkedList<Integer> GetBonusDmgEffectVal() {
        return bonusDmgEffectVals;
    }
    public LinkedList<Integer> bonusDmgEffectVals = new LinkedList<Integer>();
    
    // List of bonus damage effect values
    public void addBonusDmgVal(int val) {
    	bonusDmgVals.add(val);
    }
    public LinkedList<Integer> GetBonusDmgVal() {
        return bonusDmgVals;
    }
    public LinkedList<Integer> bonusDmgVals = new LinkedList<Integer>();
    
    public void setEffectSkillType(int type) {
    	effectSkillType = type;
    }
    public int GetEffectSkillType() {
        return effectSkillType;
    }
    public int effectSkillType = 0;
    
    public void setHitRoll(int roll) {
    	hitRoll = roll;
    }
    public int GetHitRoll() {
        return hitRoll;
    }
    public int hitRoll = 0;
    
    private static final long serialVersionUID = 1L;
}
