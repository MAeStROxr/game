package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.EnginePlugin;
import atavism.server.objects.DisplayContext.Submesh;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.util.ArrayList;

/**
 * Effect child class that sets a property on the target. 
 * The property will revert back to its current setting when the effect has finished.
 * @author Andrew Harrison
 *
 */
public class MorphEffect extends AgisEffect {
    public MorphEffect(int id, String name) {
    	super(id, name);
    	isPeriodic(false);
    	isPersistent(true);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
    	
    	String abilityEvent = EventMessageHelper.COMBAT_BUFF_GAINED;
    	
    	CombatInfo target = state.getTarget();
    	
    	defaultValue = null;
    	// First we need to check for the property default. If it is empty, then we need to get the current property
    	// value and set it as the default. However this gets trickier... if the player already has a property effect
    	// of the same property type, we need to get that property default, as the current property value will be 
    	// set to whatever the other effect set it to
    	Log.debug("MORPHEFFECT: default was null");
    	for (AgisEffect.EffectState eState : getTargetEffectsOfMatchingType(target)) {
    		if (state.equals(eState))
    			continue;
    		String pName = eState.getDefaultName();
    		String stateDefault = (String)eState.getDefaultValue();
    		if (pName.equals(propertyName) && stateDefault != null) {
    			defaultValue = stateDefault;
    			Log.debug("MORPHEFFECT: getting default from existing effect: " + defaultValue);
    		}
    	}
    	Log.debug("MORPHEFFECT: before null check default is: " + defaultValue);
    	// If the propertyDefault is still empty we will now get it from the current property value
    	if (defaultValue == null) {
    		defaultValue = (String)EnginePlugin.getObjectProperty(target.getOid(), WorldManagerClient.NAMESPACE, propertyName);
    		Log.debug("MORPHEFFECT: stored default was: " + defaultValue);
    	}
    	
    	// Now we need to check priorities and if there is a Property Effect with the same propertyName then
    	// we need to do a priority comparison.
    	boolean applyProperty = true;
		for (AgisEffect.EffectState eState: getTargetEffectsOfMatchingType(target)) {
			AgisEffect e = eState.getEffect();
			MorphEffect pEffect = (MorphEffect) e;
			if (pEffect.getPriority() > priority)
				applyProperty = false;
		}
		if (applyProperty == true) {
			EnginePlugin.setObjectPropertyNoResponse(target.getOid(), WorldManagerClient.NAMESPACE, propertyName, model);
			ArrayList<Submesh> submeshes = new ArrayList<Submesh>();
			WorldManagerClient.modifyDisplayContext(target.getOid(), WorldManagerClient.modifyDisplayContextActionReplace, model, submeshes);
		}
    	
    	Log.debug("MORPHEFFECT: applied property " + propertyName + " with value " + model 
    			+ " and default: " + defaultValue);
    	state.setDefaultName(propertyName);
    	state.setDefaultValue(defaultValue);
    	
    	EventMessageHelper.SendCombatEvent(state.getSourceOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), -1, -1);
    }

    // remove the effect from the object
    public void remove(EffectState state) {
		CombatInfo target = state.getTarget();
		Log.debug("MORPHEFFECT: removing effect from target: " + target + " with targetOid: " + state.getTargetOid());
		// We need to go through each effect the player has on them and see if they have a PropertyEffect with
		// the same property name. If they do, we need to check priorities and get the one with the highest priority.
		String value = (String) state.getDefaultValue();
		int highestPriority = 0;
		for (AgisEffect.EffectState eState: getTargetEffectsOfMatchingType(target)) {
			AgisEffect e = eState.getEffect();
			MorphEffect pEffect = (MorphEffect) e;
			if (pEffect.getPriority() > highestPriority)
				value = pEffect.getModel();
		}
		EnginePlugin.setObjectPropertyNoResponse(state.getTargetOid(), WorldManagerClient.NAMESPACE, propertyName, value);
		ArrayList<Submesh> submeshes = new ArrayList<Submesh>();
		WorldManagerClient.modifyDisplayContext(target.getOid(), WorldManagerClient.modifyDisplayContextActionReplace, value, submeshes);
		Log.debug("MORPHEFFECT: set property " + propertyName + " back to default: " + value);
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    }
    
    protected String propertyName = "model";
    protected String model = null;
    protected String defaultValue = null;
    protected int priority = 0;
    public void setModel(String value) {
    	model = value;
    }
    public String getModel() {
    	return model;
    }
    public void setPriority(int priority) {
    	this.priority = priority;
    }
    public int getPriority() {
    	return priority;
    }
    
    public void setDisplayName(String eName) {
    	displayName = eName;
    }
    public String getDisplayName() {
	return displayName;
    }
    protected String displayName = "";
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    private static final long serialVersionUID = 1L;
    
}