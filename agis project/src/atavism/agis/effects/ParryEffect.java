package atavism.agis.effects;

import atavism.agis.core.AgisEffect;

import java.util.Map;

/**
 * Effect child class that is used to show the player/mob can Parry attacks.
 * @author Andrew
 *
 */
public class ParryEffect extends AgisEffect {
    public ParryEffect(int id, String name) {
    	super(id, name);
    	isPeriodic(false);
    	isPersistent(true);
    }

    public void setWeapon(String weapon) {
    	this.weapon = weapon;
    }
    public String getWeapon() {
    	return weapon;
    }
    protected String weapon = "";

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
	    Map<String, Integer> params = state.getParams();
        //result = params.get("effect1");
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    }
    
    private static final long serialVersionUID = 1L;
}