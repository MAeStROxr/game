package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.server.engine.Engine;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Effect child class used to send an ExtensionMessage to the player. 
 * Not sure if this is a good idea.
 * @author Andrew Harrison
 *
 */
public class SendExtensionMessageEffect extends AgisEffect {

    public SendExtensionMessageEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getTarget();
		CombatInfo caster = state.getSource();
		
		Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", messageType);

		TargetedExtensionMessage eMsg = new TargetedExtensionMessage(
			WorldManagerClient.MSG_TYPE_EXTENSION, caster.getOwnerOid(), 
			caster.getOwnerOid(), false, props);
		Engine.getAgent().sendBroadcast(eMsg);
    }

    public String getMessageType() { return messageType; }
    public void setMessageType(String messageType) { this.messageType = messageType; }
    protected String messageType = "";
}
