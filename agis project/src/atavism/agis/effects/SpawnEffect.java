package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.AgisMobClient;

import java.util.Map;

/**
 * Effect child class used to spawn an object. 
 * Currently used for spawning pets, but not tested in a long time.
 * @author Andrew Harrison
 *
 */
public class SpawnEffect extends AgisEffect {

    public SpawnEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		Map<String, Integer> params = state.getParams();
		CombatInfo obj = state.getTarget();
		CombatInfo caster = state.getSource();
		effectSkillType = params.get("skillType");
		if (mobID == -1) {
			// Use the name of the target
			mobID = obj.getID();
		}
	    AgisMobClient.spawnPet(caster.getOwnerOid(), mobID, spawnType, duration, passiveEffect, effectSkillType);
    }

    /**
     * 0: Wild (Will do it's own thing)
     * 1: Quest Follower
     * 2: Non Combat pet (Follows the player)
     * 3: Combat pet (player can control it with commands)
     * @return
     */
    protected int spawnType = 0;
    public int getSpawnType() { return spawnType; }
    public void setSpawnType(int spawnType) { this.spawnType = spawnType; }
    
    public int getMobID() { return mobID; }
    public void setMobID(int mobID) { this.mobID = mobID; }
    protected int mobID = -1;
    
    public int getPassiveEffect() { return passiveEffect; }
    public void setPassiveEffect(int passiveEffect) { this.passiveEffect = passiveEffect; }
    protected int passiveEffect = -1;
    
    /*public String getInstanceName() { return instanceName; }
    public void setInstanceName(String instanceName) { this.instanceName = instanceName; }
    protected String instanceName = null;*/
    
    private static final long serialVersionUID = 1L;
}
