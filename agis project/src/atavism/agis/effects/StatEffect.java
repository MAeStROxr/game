package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.util.EventMessageHelper;
import atavism.server.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Effect child class that alters the value of a stat on the target for a period of time.
 * Can be permanent if the effect is a passive one.
 * @author Andrew Harrison
 *
 */
public class StatEffect extends AgisEffect {
    public StatEffect(int id, String name) {
    	super(id, name);
    	isPeriodic(false);
    	isPersistent(true);
    }

    protected Map<String, Float> statMap = new HashMap<String, Float>();
    public void setStat(String stat, float adj) {
    	statMap.put(stat, new Float(adj));
    }
    public Float getStat(String stat) {
    	return statMap.get(stat);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
    	Map<String, Integer> params = state.getParams();
    	//int effectNum = params.get("effectVal");
    	int skillType = params.get("skillType");
    	
    	String abilityEvent = EventMessageHelper.COMBAT_BUFF_GAINED;
    	
    	CombatInfo caster = state.getSource();
    	CombatInfo target = state.getTarget();
    	
    	// Stacking handling
    	int stackCase = stackCheck();
    	if (stackCase == 0) {
    		// Do not apply this effect
    		return;
    	} 
    	
    	int stackLevel = 1;
    	boolean hasThisEffect = false;
    	boolean fromThisCaster = false;
    	// AgisEffect effect = state.getEffect();
    	EffectState similarEffect = null;
    	EffectState sameCasterEffect = null;
    	for (EffectState existingState : target.getCurrentEffects()) {
    		if (existingState.getEffect().getID() == getID() && !this.equals(existingState.getEffect())) {
    			hasThisEffect = true;
    			similarEffect = existingState;
    		    if (caster.getOwnerOid().equals(similarEffect.getStackCaster())) {
    		    	fromThisCaster = true;
    		    	sameCasterEffect = similarEffect;
    		    }
    		}
    	}
    	Log.debug("STATEFFECT: target has this effect: " + hasThisEffect + "; from this caster: " + fromThisCaster);
    	
    	if (stackCase == 1) {
    		// If the target already has the same effect type from the same caster, remove the old one
    		if (fromThisCaster)
    			AgisEffect.removeEffect(sameCasterEffect);
    	} else if (stackCase == 2) {
    		// If the target already has the same effect type from the same caster, remove the old one and increment the stack
    		if (fromThisCaster) {
    			stackLevel = sameCasterEffect.getCurrentStack();
    			AgisEffect.removeEffect(sameCasterEffect);
    		}
    		if (stackLevel < this.stackLimit)
    		    stackLevel++;
    	} else if (stackCase == 3) {
    		// If the target already has the same effect type, remove the old one
    		if (hasThisEffect)
    			AgisEffect.removeEffect(similarEffect);
    	} else if (stackCase == 4) {
    		// If the target already has the same effect type, remove the old one and increment the stack
    		if (hasThisEffect) {
    			stackLevel = similarEffect.getCurrentStack();
    			AgisEffect.removeEffect(similarEffect);
    		}
    		if (stackLevel < this.stackLimit)
    		    stackLevel++;
    	}
    	
    	// if (!result.equals("normal"))
    	//WorldManagerClient.sendObjChatMsg(state.getCaster().getOwnerOid(), 1,
    	//    "Effect value is: " + effectNum);
    	
    	int skillLevel = 0;
		Log.debug("COMBATHELPER: about to check for skill level");
		if (skillType != -1) {
	        if (!caster.getCurrentSkillInfo().getSkills().containsKey(skillType))
	    	    Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
	        else
	        	skillLevel = caster.getCurrentSkillInfo().getSkills().get(skillType).getSkillLevel();
		}
		
    	Log.debug("STATEFFECT: applying effect: " + getName() + " with effectVal: " + getID());
    	for (Map.Entry<String, Float> entry : statMap.entrySet()) {
    		Log.debug("STATEFFECT: adding stat modifier: " + entry.getKey() + "=" + entry.getValue() + " to: " + target.getOwnerOid());
    		float statModifier =  (entry.getValue() + (float)skillLevel * skillEffectMod.get(0));
    		// Multiply the modification by the stack level
    		statModifier = statModifier * stackLevel;
    		Log.debug("STATEFFECT: statModifier: " + statModifier);
    		if (modifyPercentage) {
    			target.statAddPercentModifier(entry.getKey(), state, statModifier);
    		} else {
    			target.statAddModifier(entry.getKey(), state, (int)statModifier);
    		}
    	}
    	
    	// Set the caster oid and the stack level variables
    	state.setStackCaster(caster.getOwnerOid());
    	state.setCurrentStack(stackLevel);
    	
    	EventMessageHelper.SendCombatEvent(caster.getOwnerOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), -1, -1);
    }
    
    public void remove(EffectState state) {
    	CombatInfo target = state.getTarget();
    	remove(state, target);
    }

    // remove the effect from the object
    public void remove(EffectState state, CombatInfo target) {
		if (target == null)
			return;
		Log.debug("STATEFFECT: removing statEffect: " + this.getName());
		for (Map.Entry<String, Float> entry : statMap.entrySet()) {
			Log.debug("STATEFFECT: removing stat effect stat: " + entry.getKey() + " from: " + target.getOwnerOid());
			if (modifyPercentage) {
				target.statRemovePercentModifier(entry.getKey(), state);
			} else {
				target.statRemoveModifier(entry.getKey(), state);
			}
		}
		EventMessageHelper.SendCombatEvent(state.getSource().getOwnerOid(), target.getOwnerOid(), 
				EventMessageHelper.COMBAT_BUFF_LOST, state.getAbilityID(), getID(), -1, -1);
		super.remove(state);
    }
    
    public void unload(EffectState state, CombatInfo target) {
    	remove(state, target);
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    }
    
    public void setDisplayName(String eName) {
    	displayName = eName;
    }
    public String getDisplayName() {
    	return displayName;
    }
    protected String displayName = "";
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    public void setModifyPercentage(boolean modifyPercentage) {
    	this.modifyPercentage = modifyPercentage;
    }
    public boolean getModifyPercentage() {
    	return modifyPercentage;
    }
    public boolean modifyPercentage = false;
    
    private static final long serialVersionUID = 1L;
}