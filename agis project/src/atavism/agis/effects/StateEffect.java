package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.CombatClient;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.EnginePlugin;
import atavism.server.util.Log;

/**
 * Effect child class that sets a property on the target. 
 * The property will revert back to its current setting when the effect has finished.
 * @author Andrew Harrison
 *
 */
public class StateEffect extends AgisEffect {
    public StateEffect(int id, String name) {
    	super(id, name);
    	isPeriodic(false);
    	isPersistent(true);
    }

    // add the effect to the object
    public void apply(EffectState state) {
    	super.apply(state);
    	
    	String abilityEvent = EventMessageHelper.COMBAT_BUFF_GAINED;
    	
    	CombatInfo target = state.getTarget();
    	
    	if (propertyName.equals("")) {
    		Log.error("STATEEFFECT: property value or name has not been set. Effect name: " + 
    				this.getName());
    		return;
    	}
    	
    	// Now we need to check priorities and if there is a Property Effect with the same propertyName then
    	// we need to do a priority comparison.
    	boolean applyProperty = true;
		for (AgisEffect.EffectState eState: getTargetEffectsOfMatchingType(target)) {
			AgisEffect e = eState.getEffect();
			StateEffect pEffect = (StateEffect) e;
			String pName = pEffect.getPropertyName();
			if (pName.equals(propertyName))
				if (pEffect.getPriority() > priority)
					applyProperty = false;
		}
		if (applyProperty == true)
			EnginePlugin.setObjectPropertyNoResponse(target.getOid(), CombatClient.NAMESPACE, propertyName, true);
    	
    	Log.debug("STATEEFFECT: set property " + propertyName + " to true");
    	
    	EventMessageHelper.SendCombatEvent(state.getSourceOid(), target.getOwnerOid(), abilityEvent, state.getAbilityID(), getID(), -1, -1);
    }

    // remove the effect from the object
    public void remove(EffectState state) {
		CombatInfo target = state.getTarget();
		// We need to go through each effect the player has on them and see if they have a PropertyEffect with
		// the same property name. If they do, we need to check priorities and get the one with the highest priority.
		for (AgisEffect.EffectState eState: getTargetEffectsOfMatchingType(target)) {
			AgisEffect e = eState.getEffect();
			StateEffect pEffect = (StateEffect) e;
			String pName = pEffect.getPropertyName();
			if (pName.equals(propertyName) && !eState.equals(state)) {
				Log.debug("STATEEFFECT: not setting property " + propertyName + " back to false as another effect was found: " + eState.getEffectName());
				// Don't set property to false if other effects are still active
				return;
			}
		}
		EnginePlugin.setObjectPropertyNoResponse(target.getOid(), CombatClient.NAMESPACE, propertyName, false);
		Log.debug("STATEEFFECT: set property " + propertyName + " back to false");
    }

    // perform the next periodic pulse for this effect on the object
    public void pulse(EffectState state) {
    	super.pulse(state);
    }
    
    protected String propertyName = "";
    protected int priority = 0;
    public void setPropertyName(String name) {
    	propertyName = name;
    }
    public String getPropertyName() {
    	return propertyName;
    }
    public void setPriority(int priority) {
    	this.priority = priority;
    }
    public int getPriority() {
    	return priority;
    }
    
    public void setDisplayName(String eName) {
    	displayName = eName;
    }
    public String getDisplayName() {
	return displayName;
    }
    protected String displayName = "";
    
    public void setEffectType(int type) {
    	effectType = type;
    }
    public int GetEffectType() {
        return effectType;
    }
    public int effectType = 0;
    
    private static final long serialVersionUID = 1L;
    
}