package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.AgisMobClient;

import java.util.Map;
//import atavism.server.util.Log;

/**
 * Effect child class used to Tame a mob giving the player control of it.
 * Has not been tested in a long time.
 * @author Andrew Harrison
 *
 */
public class TameEffect extends AgisEffect {

    public TameEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		Map<String, Integer> params = state.getParams();
		CombatInfo obj = state.getTarget();
        CombatInfo caster = state.getSource();
        int skillType = params.get("skillType");
	    AgisMobClient.tameBeast(caster.getOwnerOid(), obj.getOwnerOid(), skillType);
    }
    
    /*public String getInstanceName() { return instanceName; }
    public void setInstanceName(String instanceName) { this.instanceName = instanceName; }
    protected String instanceName = null;*/
    
    private static final long serialVersionUID = 1L;
}
