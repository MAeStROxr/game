package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.QuestClient;
import atavism.server.engine.Engine;

/**
 * Effect child class that sends a TaskUpdateMessage. This is commonly used to mark objectives complete for quests.
 * Usage examples could be when a player has entered a certain area or has finished an escort.
 * @author Andrew Harrison
 *
 */
public class TaskCompleteEffect extends AgisEffect {

	public TaskCompleteEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getTarget();
		CombatInfo caster = state.getSource();
		
		QuestClient.TaskUpdateMessage msg = new QuestClient.TaskUpdateMessage(caster.getOwnerOid(), taskID, 1);
		Engine.getAgent().sendBroadcast(msg);
    }

    public int getTaskID() { return taskID; }
    public void setTaskID(int taskID) { this.taskID = taskID; }
    protected int taskID = -1;
    
    private static final long serialVersionUID = 1L;
}
