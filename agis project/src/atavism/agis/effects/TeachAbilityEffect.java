package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.plugins.ClassAbilityClient;

/**
 * Effect child class that is used to teach the target an ability.
 * Currently not used.
 *
 */
public class TeachAbilityEffect extends AgisEffect {

	public TeachAbilityEffect(int id, String name) {
    	super(id, name);
    	isPeriodic(false);
    	isPersistent(false);
    }

    public TeachAbilityEffect(int id, String name, int abilityID) {
    	super(id, name);
    	isPeriodic(false);
		isPersistent(false);
		setAbilityID(abilityID);
    }

    public int getAbilityID() { return abilityID; }
    public void setAbilityID(int id) { abilityID = id; }
    protected int abilityID = -1;

    public String getCategory() { return category; }
    public void setCategory(String name) { category = name; }
    protected String category = null;

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		//CombatInfo mob = state.getSource();
		ClassAbilityClient.learnAbility(state.getTargetOid(), abilityID);
		//CombatPlugin.sendAbilityUpdate(mob);
    }
    private static final long serialVersionUID = 1L;
}
