package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.SkillInfo;
import atavism.agis.util.EventMessageHelper;

/**
 * Effect child class that permanently alters the level of a skill.
 * Not tested in a long time.
 * @author Andrew Harrison
 *
 */
public class TeachSkillEffect extends AgisEffect {

    public TeachSkillEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getSource();
		
		if (skillType != -1 && !obj.getCurrentSkillInfo().getSkills().keySet().contains(skillType)) {
			//ClassAbilityClient.skillIncreased(obj.getOwnerOid(), skillType);
			SkillInfo.learnSkill(obj.getCurrentSkillInfo(), skillType, obj.aspect(), obj);
		} else {
			EventMessageHelper.SendErrorEvent(obj.getOwnerOid(), EventMessageHelper.SKILL_ALREADY_KNOWN, skillType, "");
		}
    }

    public int getSkillType() { return skillType; }
    public void setSkillType(int skillType) { this.skillType = skillType; }
    protected int skillType = -1;
    
    public int getAlterValue() { return alterValue; }
    public void setAlterValue(int alterValue) { this.alterValue = alterValue; }
    protected int alterValue = -1;
    
    private static final long serialVersionUID = 1L;
}
