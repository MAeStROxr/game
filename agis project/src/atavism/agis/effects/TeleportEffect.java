package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.objects.AgisItem;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.AgisWorldManagerClient;
import atavism.agis.plugins.ArenaClient;
import atavism.server.engine.BasicWorldNode;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.objects.Marker;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.WorldManagerClient;

/**
 * Effect child class that teleports the target somewhere. Also handles the special case of item based 
 * teleportation (such as hearth stones).
 * @author Andrew Harrison
 *
 */
public class TeleportEffect extends AgisEffect {

    public TeleportEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		CombatInfo obj = state.getTarget();
	    BasicWorldNode wnode = WorldManagerClient.getWorldNode(obj.getOid());
	    OID instanceOid = null;
	    Point loc = location;
    	if (instanceID < 1) {
    		instanceOid = wnode.getInstanceOid();
    	}
	    if (teleportType.equals("Item")) {
	    	AgisItem item = state.getItem();
	    	if (item == null)
	    		return;
	    	loc = (Point) item.getProperty("teleportLoc");
	    	wnode.setLoc(loc);
	    	int hearthInstance = (Integer) item.getProperty("teleportInstance");
	    	if (hearthInstance > 1) {
	    		AgisWorldManagerClient.sendChangeInstance(obj.getOid(), hearthInstance, loc);
	    		return;
	    	}
	    } else if (teleportType.equals("To Target")) {
	    	// Get the loc of the target and then set the target to the source and teleport them
	    	loc = wnode.getLoc();
	    	instanceOid = wnode.getInstanceOid();
	    	obj = state.getSource();
	    	wnode = WorldManagerClient.getWorldNode(obj.getOid());
	    	wnode.setLoc(loc);
	    	wnode.setInstanceOid(instanceOid);
	    } else if (teleportType.equals("Standard")) {
	    	wnode.setLoc(location);
	    	loc = location;
	    	AgisWorldManagerClient.sendChangeInstance(obj.getOid(), instanceID, loc);
	    	return;
	    } else {
	    	Marker m = InstanceClient.getMarker(instanceOid, teleportType);
	    	loc = m.getPoint();
	    	wnode.setLoc(loc);
	    }
	    
	    if (instanceID < 1 || instanceOid == null || instanceOid.equals(wnode.getInstanceOid())) {
	    	wnode.setInstanceOid(wnode.getInstanceOid());
	    	WorldManagerClient.updateWorldNode(obj.getOid(), wnode, true);
	    } else {
	    	AgisWorldManagerClient.sendChangeInstance(obj.getOid(), instanceID, loc);
	    }
	    // They have left the arena
	    ArenaClient.removePlayer(obj.getOid());
    }


    public Point getTeleportLocation() { return location; }
    public void setTeleportLocation(Point loc) { location = loc; }
    protected Point location = null;
    
    public String getTeleportType() { return teleportType; }
    public void setTeleportType(String teleportType) { this.teleportType = teleportType; }
    protected String teleportType = null;
    
    public int getInstance() { return instanceID; }
    public void setInstance(int instanceID) { this.instanceID = instanceID; }
    protected int instanceID = -1;
    
    private static final long serialVersionUID = 1L;
}
