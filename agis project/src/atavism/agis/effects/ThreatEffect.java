package atavism.agis.effects;

import atavism.agis.core.AgisEffect;
import atavism.agis.plugins.CombatClient;

/**
 * Effect child class that permanently alters the level of a skill.
 * Not tested in a long time.
 * @author Andrew Harrison
 *
 */
public class ThreatEffect extends AgisEffect {

    public ThreatEffect(int id, String name) {
        super(id, name);
    }

    // add the effect to the object
    public void apply(EffectState state) {
		super.apply(state);
		
		CombatClient.sendAlterThreat(state.getTargetOid(), state.getSourceOid(), alterValue);
		
    }
    
    public int getAlterValue() { return alterValue; }
    public void setAlterValue(int alterValue) { this.alterValue = alterValue; }
    protected int alterValue = 0;
    
    private static final long serialVersionUID = 1L;
}
