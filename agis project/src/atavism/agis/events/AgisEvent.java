package atavism.agis.events;

import atavism.agis.objects.AgisObject;
import atavism.server.engine.Event;
import atavism.server.network.AOByteBuffer;
import atavism.server.network.ClientConnection;

public abstract class AgisEvent extends Event {
    public AgisEvent() {
	super();
    }

    public AgisEvent(AgisObject obj) {
	super();
	setObject(obj);
    }

    public AgisEvent(AOByteBuffer buf, ClientConnection con) {
	super(buf,con);
    }

}
