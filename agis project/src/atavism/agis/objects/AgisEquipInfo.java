package atavism.agis.objects;

import atavism.server.util.LockFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

/**
 * stores information about how to handle equipping.
 * says what equipslots are valid.
 * says what socket the equipslot maps to.
 * agismobs all refer to an object like this
 */
public class AgisEquipInfo implements Cloneable, Serializable {
    public AgisEquipInfo() {
	setupTransient();
    }

    public AgisEquipInfo(String name) {
	setupTransient();
        setName(name);
    }

    public String toString() {
        localLock.lock();
        try {
            String s = "[AgisEquipInfo: name=" + name;
            for (AgisEquipSlot slot : equipSlots) {
                s += ", slot=" + slot;
            }
            return s + "]";
        }
        finally {
            localLock.unlock();
        }
    }

    public String getName() {
	return name;
    }
    public void setName(String name) {
	staticMapLock.lock();
	try {
            this.name = name;
	    equipInfoMap.put(name, this);
	}
	finally {
	    staticMapLock.unlock();
	}
    }
    private String name;


    public void addEquipSlot(AgisEquipSlot slot) {
	localLock.lock();
	try {
	    equipSlots.add(slot);
	}
	finally {
	    localLock.unlock();
	}
    }
    public List<AgisEquipSlot> getEquippableSlots() {
	localLock.lock();
	try {
	    return new ArrayList<AgisEquipSlot>(equipSlots);
	}
	finally {
	    localLock.unlock();
	}
    }
    public void setEquippableSlots(List<AgisEquipSlot> slots) {
        localLock.lock();
        try {
            equipSlots = new ArrayList<AgisEquipSlot>(slots);
        }
        finally {
            localLock.unlock();
        }
    }
    List<AgisEquipSlot> equipSlots = new ArrayList<AgisEquipSlot>();
    
    public static AgisEquipInfo getEquipInfo(String name) {
	staticMapLock.lock();
	try {
	    return equipInfoMap.get(name);
	}
	finally {
	    staticMapLock.unlock();
	}
    }

    private static Map<String, AgisEquipInfo> equipInfoMap =
	new HashMap<String, AgisEquipInfo>();


    private static Lock staticMapLock = LockFactory.makeLock("StaticAgisEquipInfo");
    transient private Lock localLock = null;

    void setupTransient() {
	localLock = LockFactory.makeLock("AgisEquipInfo");
    }
    private void readObject(ObjectInputStream in) 
	throws IOException, ClassNotFoundException {
	in.defaultReadObject();
	setupTransient();
    }

    // define the standard mob equippable slots
    public static AgisEquipInfo DefaultEquipInfo =
	new AgisEquipInfo("AgisDefaultEquipInfo");
    static {
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.PRIMARYWEAPON);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.SHIRT);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.CHEST);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.LEGS);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.HEAD);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.FEET);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.HANDS);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.SHOULDER);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.BACK);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.BELT);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.SECONDARYWEAPON);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.PRIMARYRING);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.SECONDARYRING);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.NECK);
    	DefaultEquipInfo.addEquipSlot(AgisEquipSlot.RANGEDWEAPON);
    }

    private static final long serialVersionUID = 1L;
}