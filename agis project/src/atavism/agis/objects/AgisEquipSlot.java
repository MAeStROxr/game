package atavism.agis.objects;

import atavism.server.util.LockFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;

public class AgisEquipSlot implements Serializable {
    public AgisEquipSlot() {
    }

    public AgisEquipSlot(String slotName) {
	this.name = slotName;
	mapLock.lock();
	try {
	    slotNameMapping.put(slotName, this);
	}
	finally {
	    mapLock.unlock();
	}
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
	return name;
    }
    private String name = null;

    public int hashCode() {
        return name.hashCode();
    }
    public boolean equals(Object other) {
        if (other instanceof AgisEquipSlot) {
            AgisEquipSlot otherSlot = (AgisEquipSlot)other;
            return otherSlot.getName().equals(name);
        }
        return false;
    }

    public String toString() {
	return "[AgisEquipSlot name=" + getName() + "]";
    }

    public static AgisEquipSlot getSlotByName(String slotName) {
	mapLock.lock();
	try {
	    return slotNameMapping.get(slotName);
	}
	finally {
	    mapLock.unlock();
	}
    }
    private static Map<String, AgisEquipSlot> slotNameMapping =
	new HashMap<String, AgisEquipSlot>();

    private static Lock mapLock = LockFactory.makeLock("AgisEquipSlot");

    public static AgisEquipSlot PRIMARYWEAPON = 
    	new AgisEquipSlot("primaryWeapon");
        
        public static AgisEquipSlot SHIRT = 
        	new AgisEquipSlot("shirt");

        public static AgisEquipSlot CHEST = 
    	new AgisEquipSlot("chest");

        public static AgisEquipSlot LEGS = 
    	new AgisEquipSlot("legs");

        public static AgisEquipSlot HEAD = 
    	new AgisEquipSlot("head");

        public static AgisEquipSlot FEET = 
    	new AgisEquipSlot("feet");

        public static AgisEquipSlot HANDS = 
    	new AgisEquipSlot("hands");

        public static AgisEquipSlot SHOULDER = 
        	new AgisEquipSlot("shoulder");
        
        public static AgisEquipSlot BACK = 
        	new AgisEquipSlot("back");
        
        public static AgisEquipSlot CAPE = 
        	new AgisEquipSlot("cape");
        
        public static AgisEquipSlot BELT = 
        	new AgisEquipSlot("belt");
        
        public static AgisEquipSlot SECONDARYWEAPON = 
        	new AgisEquipSlot("secondaryWeapon");
        
        public static AgisEquipSlot PRIMARYRING = 
        	new AgisEquipSlot("primaryRing");
        
        public static AgisEquipSlot SECONDARYRING = 
        	new AgisEquipSlot("secondaryRing");
        
        public static AgisEquipSlot NECK = 
        	new AgisEquipSlot("neck");
        
        public static AgisEquipSlot RANGEDWEAPON = 
        	new AgisEquipSlot("rangedWeapon");
        
        public static AgisEquipSlot UNKNOWN = 
        	new AgisEquipSlot("unknown");

    private static final long serialVersionUID = 1L;
}