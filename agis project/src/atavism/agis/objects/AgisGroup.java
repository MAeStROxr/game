package atavism.agis.objects;

import atavism.agis.plugins.GroupClient;
import atavism.server.engine.OID;
import atavism.server.objects.Entity;
import atavism.server.plugins.VoiceClient;
import atavism.server.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class AgisGroup extends Entity {
    // properties
    private Hashtable<OID, AgisGroupMember> _groupMembers;
    private static final long serialVersionUID = 1L;
    private OID _groupLeaderOid;
    private Boolean _groupMuted = false;

    // constructors
    public AgisGroup() {
        super("");
        _groupMembers = new Hashtable<OID, AgisGroupMember>();
        if(Log.loggingDebug)
            log.debug("AgisGroup - creating new group " + this.getOid().toString());
        //SetupVoiceGroup();        
    }

    // events

    // methods
    public OID GetGroupOid() {
        return this.getOid();
    }

    public AgisGroupMember AddGroupMember(CombatInfo combatInfo) {
        AgisGroupMember newMember = new AgisGroupMember(combatInfo, this.getOid());
        _groupMembers.put(newMember.getOid(), newMember);
        combatInfo.setGroupOid(this.GetGroupOid());
        combatInfo.setGroupMemberOid(newMember.getOid());
        GroupClient.SendGroupEventMessage(GroupClient.GroupEventType.JOINED, this, combatInfo.getOwnerOid());
        return newMember;
    }

    public void RemoveGroupMember(CombatInfo combatInfo) {
        GroupClient.SendGroupEventMessage(GroupClient.GroupEventType.LEFT, this, combatInfo.getOwnerOid());
        _groupMembers.remove(combatInfo.getGroupMemberOid());
        combatInfo.setGroupMemberOid(null);
        combatInfo.setGroupOid(null);
        //If no more members are in the group, then group is disolving and we need to clean up the voice group
        //if(_groupMembers.size() == 0)
        //    RemoveVoiceGroup();
    }

    public Hashtable<OID, AgisGroupMember> GetGroupMembers() {
        return _groupMembers;
    }

    public int GetNumGroupMembers() {
        return _groupMembers.size();
    }

    public OID GetGroupLeaderOid() {
        return _groupLeaderOid;
    }
    
    public AgisGroupMember GetGroupMember(OID groupMemberOid){
        for (AgisGroupMember groupMember : _groupMembers.values()) {
            if (groupMember.GetGroupMemberOid().equals(groupMemberOid))
                return groupMember;
        }
        return null;
    }
    
    public void SetGroupLeaderOid(OID value) {
        _groupLeaderOid = value;
        if (value != null)
            GroupClient.SendGroupEventMessage(GroupClient.GroupEventType.LEADERCHANGED, this, value);
        //Default to remaining person until group is cleaned up and removed
        if (_groupMembers.size() == 1){
            List<AgisGroupMember> groupMembers = new ArrayList<AgisGroupMember>(_groupMembers.values());
            _groupLeaderOid = groupMembers.get(0).GetGroupMemberOid();
        }
    }
    
    protected void SetupVoiceGroup(){
        int error = 0;

        // Create a new voice chat group specific to this group that is non-positional
        error = VoiceClient.addVoiceGroup(this.GetGroupOid(), false, 4);
        if(error != VoiceClient.SUCCESS){
            Log.error("AgisGroup.SetupGroupVoice : Create Voice Group Response - " + VoiceClient.errorString(error));            
        }
    }
    
    public void RemoveVoiceGroup(){
        int error = 0;
        // Remove voice group voice server
        error = VoiceClient.removeVoiceGroup(this.GetGroupOid());
        if(error != VoiceClient.SUCCESS){
            Log.error("AgisGroup.RemoveVoiceGroup : Remove Voice Group Response - " + VoiceClient.errorString(error));            
        }  
    }
    
    public void SetGroupMuted(Boolean value){
        this._groupMuted = value;
    }
    
    public Boolean GetGroupMuted(){
        return this._groupMuted;
    }
}
