package atavism.agis.objects;

import atavism.agis.plugins.GroupPlugin;
import atavism.server.engine.OID;
import atavism.server.objects.Entity;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AgisGroupMember extends Entity {
    // properties
    private static final long serialVersionUID = 1L;
    private OID _groupMemberOid;
    private OID _groupOid;
    private String _groupMemberName;
    private Boolean _voiceEnabled = Boolean.FALSE;
    private Boolean _allowedSpeaker = Boolean.TRUE; // everyone can speak by default   
    private Map<String, Serializable> _entryStats = new HashMap<String, Serializable>();
    private static final Logger _log = new Logger("GroupMember");

    // constructor
    public AgisGroupMember(CombatInfo combatInfo, OID groupOid) {
        super("");
        this._groupMemberOid = combatInfo.getOwnerOid();
        this._groupMemberName = WorldManagerClient.getObjectInfo(combatInfo
                .getOwnerOid()).name;
        this._groupOid = groupOid;
        SetGroupMemberStats(combatInfo);
    }

    // methods
    public OID GetGroupMemberOid() {
        return this._groupMemberOid;
    }

    public String GetGroupMemberName() {
        return this._groupMemberName;
    }

    public OID GetGroupOid(){
        return this._groupOid;
    }

    protected void SetGroupMemberStats(CombatInfo combatInfo) {
        for (String stat : GroupPlugin.GetRegisteredStats()) {
        	Log.debug("GROUP: setting member stat: " + stat);
            _entryStats.put(stat, combatInfo.statGetCurrentValue(stat));
        	/*int statVal = (Integer) EnginePlugin.getObjectProperty(this._groupMemberOid, 
        			WorldManagerClient.NAMESPACE, stat);
        			_entryStats.put(stat, statVal);*/
        }
    }

    public Serializable GetGroupMemberStat(String stat) {
        if (Log.loggingDebug) {
            _log.debug("AgisGroup.GetGroupMemberStat : " + stat + " = "
                    + _entryStats.get(stat));
        }
        return _entryStats.get(stat);
    }
    
    public void SetVoiceEnabled(Boolean value){        
        this._voiceEnabled = value;
    }
    
    public Boolean GetVoiceEnabled(){
        return this._voiceEnabled;
    }
    
    public void SetAllowedSpeaker(Boolean value){
        this._allowedSpeaker = value;
    }
    
    public Boolean GetAllowedSpeaker(){
        return this._allowedSpeaker;
    }
}
