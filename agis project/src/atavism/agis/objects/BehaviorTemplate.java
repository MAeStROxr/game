package atavism.agis.objects;

import atavism.server.math.Point;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public class BehaviorTemplate implements Serializable {
    public BehaviorTemplate() {
    }
    
    public BehaviorTemplate(int id, String name) {
    	this.id = id;
    	this.name = name;
    }
    
    public int getID() { return id;}
    public void setID(int id) {
    	this.id = id;
    }
    
    public String getName() { return name;}
    public void setName(String name) {
    	this.name = name;
    }
    
    public String getBaseAction() { return baseAction;}
    public void setBaseAction(String baseAction) {
    	this.baseAction = baseAction;
    }
    
    public boolean getWeaponsSheathed() { return weaponsSheathed;}
    public void setWeaponsSheathed(boolean weaponsSheathed) {
    	this.weaponsSheathed = weaponsSheathed;
    }
    
    public int getRoamRadius() { return roamRadius;}
    public void setRoamRadius(int roamRadius) {
    	this.roamRadius = roamRadius;
    }
    
    public int getPatrolPathID() { return patrolPathID;}
    public void setPatrolPathID(int patrolPathID) {
    	this.patrolPathID = patrolPathID;
    }
    
    public ArrayList<Point> getPatrolPoints() { return patrolPoints;}
    public void setPatrolPoints(ArrayList<Point> patrolPoints) {
    	this.patrolPoints = patrolPoints;
    }
    public void addPatrolPoint(PatrolPoint point) {
    	patrolPoints.add(point.loc);
    	patrolPauses.add(point.lingerTime);
    }
    
    public void travelReverse() {
    	for (int i = patrolPoints.size() - 2; i > 0; i--) {
    		patrolPoints.add(patrolPoints.get(i));
    		patrolPauses.add(patrolPauses.get(i));
    	}
    	
    	String patrolString = "";
    	for (int i = 0; i < patrolPoints.size(); i++) {
    		patrolString += i + " delay: " + patrolPauses.get(i) + ", ";
    	}
    	Log.debug("PATROL: " + patrolString);
    }
    
    public ArrayList<Float> getPatrolPauses() { return patrolPauses;}
    public void setPatrolPauses(ArrayList<Float> patrolPauses) {
    	this.patrolPauses = patrolPauses;
    }
    
    public boolean getHasCombat() { return hasCombat;}
    public void setHasCombat(boolean hasCombat) {
    	this.hasCombat = hasCombat;
    }
    
    public int getAggroRadius() { return aggroRadius;}
    public void setAggroRadius(int aggroRadius) {
    	this.aggroRadius = aggroRadius;
    }
    
    public ArrayList<Integer> getStartsQuests() { return startsQuests;}
    public void setStartsQuests(ArrayList<Integer> startsQuests) {
    	this.startsQuests = startsQuests;
    }
    public ArrayList<Integer> getEndsQuests() { return endsQuests;}
    public void setEndsQuests(ArrayList<Integer> endsQuests) {
    	this.endsQuests = endsQuests;
    }
    
    public ArrayList<Integer> getStartsDialogues() { return startsDialogues;}
    public void setStartsDialogues(ArrayList<Integer> startsDialogues) {
    	this.startsDialogues = startsDialogues;
    }
    
    public int getMerchantTable() { return merchantTable;}
    public void setMerchantTable(int merchantTable) {
    	this.merchantTable = merchantTable;
    }
    
    public int getQuestOpenLoot() { return questOpenLoot;}
    public void setQuestOpenLoot(int questOpenLoot) {
    	this.questOpenLoot = questOpenLoot;
    }
    
    public boolean getIsChest() { return isChest;}
    public void setIsChest(boolean isChest) {
    	this.isChest = isChest;
    }
    
    public int getPickupItem() { return pickupItem;}
    public void setPickupItem(int pickupItem) {
    	this.pickupItem = pickupItem;
    }
    
    public boolean getIsPlayerCorpse() { return isPlayerCorpse;}
    public void setIsPlayerCorpse(boolean isPlayerCorpse) {
    	this.isPlayerCorpse = isPlayerCorpse;
    }
    
    public String getOtherUse() { return otherUse;}
    public void setOtherUse(String otherUse) {
    	this.otherUse = otherUse;
    }
    
    public String toString() {
    	return id + ":" + name;
    }

    int id;
    String name;
    String baseAction = "";
    boolean weaponsSheathed = false;
    int roamRadius = 0;
    int patrolPathID = -1;
    ArrayList<Point> patrolPoints = new ArrayList<Point>();
    ArrayList<Float> patrolPauses = new ArrayList<Float>();
    boolean hasCombat = false;
    int aggroRadius = 0;
    ArrayList<Integer> startsQuests = new ArrayList<Integer>();
    ArrayList<Integer> endsQuests = new ArrayList<Integer>();
    ArrayList<Integer> startsDialogues = new ArrayList<Integer>();
    int merchantTable = -1;
    int questOpenLoot = -1;
    boolean isChest = false;
    int pickupItem = -1;
    boolean isPlayerCorpse = false;
    String otherUse = null;

    private static final long serialVersionUID = 1L;
}
