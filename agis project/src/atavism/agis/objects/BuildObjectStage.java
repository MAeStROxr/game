package atavism.agis.objects;

import java.io.Serializable;
import java.util.HashMap;

public class BuildObjectStage implements Serializable {
	
	protected HashMap<Integer, Integer> itemReqs;
    protected String gameObject;
    protected float buildTimeReq = 1;
    protected int health = 0;
    protected int nextStageID = -1;
    	
    public BuildObjectStage(String gameObject, float buildTimeReq, HashMap<Integer, Integer> itemReqs, 
    		int health, int nextStageID) {
    	this.gameObject = gameObject;
    	this.buildTimeReq = buildTimeReq;
    	this.itemReqs = itemReqs;
    	this.health = health;
    	this.nextStageID = nextStageID;
    }
    	
    public String getGameObject() { return gameObject; }
    public void setGameObject(String gameObject) { this.gameObject = gameObject; }
        
    public HashMap<Integer, Integer> getItemReqs() { return itemReqs; }
    public void setItemReqs(HashMap<Integer, Integer> itemReqs) { this.itemReqs = itemReqs; }
    	
    public float getBuildTimeReq() { return buildTimeReq; }
    public void setBuildTimeReq(float buildTimeReq) { this.buildTimeReq = buildTimeReq; }
    
    public int getHealth() { return health; }
    public void setHealth(int health) { this.health = health; }
    
    public int getNextStageID() { return nextStageID; }
    public void setNextStageID(int nextStageID) { this.nextStageID = nextStageID; }
    
    private static final long serialVersionUID = 1L;
    
}