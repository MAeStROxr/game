package atavism.agis.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The BuildObjectTemplate class stores all the information needed about a building object. 
 * @author Andrew Harrison
 *
 */
public class BuildObjectTemplate implements Serializable {
	protected int id;
	protected String name;
    protected int skill = -1;
    protected int skillLevelReq = 0;
    protected String weaponReq = "";
    protected float maxDistance = 3f;
    protected int maxHealth = 1;
    protected ArrayList<BuildObjectStage> stages = new ArrayList<BuildObjectStage>();
    
    public BuildObjectTemplate() {
    	
    }

    public BuildObjectTemplate(int id, String name, int skill, int skillLevelReq, 
    		String weaponReq, float maxDistance) {
    	//Log.debug("SKILL TEMPLATE: starting skillTemplate creation");
    	this.id = id;
    	this.name = name;
    	this.skill = skill;
    	this.skillLevelReq = skillLevelReq;
    	this.weaponReq = weaponReq;
    	this.maxDistance = maxDistance;
	    //Log.debug("SKILL TEMPLATE: finished skillTemplate creation");
    }
    
    /**
     * Adds a stage from the database.
     * @param stage
     */
    public void addStage(BuildObjectStage stage) {
    	stages.add(stage);
    }
    
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    
    public int getSkill() { return skill; }
    public void setSkill(int skill) { this.skill = skill; }
    
    public int getSkillLevelReq() { return skillLevelReq; }
    public void setSkillLevelReq(int skillLevelReq) { this.skillLevelReq = skillLevelReq; }
    
    public String getWeaponReq() { return weaponReq; }
    public void setWeaponReq(String weaponReq) { this.weaponReq = weaponReq; }
    
    public float getMaxDistance() { return maxDistance; }
    public void setMaxDistance(float maxDistance) { this.maxDistance = maxDistance; }
    
    public int getMaxHealth() { return maxHealth; }
    public void setMaxHealth(int maxHealth) { this.maxHealth = maxHealth; }
    
    public ArrayList<BuildObjectStage> getStages() { return stages; }
    public void setStages(ArrayList<BuildObjectStage> stages) { this.stages = stages; }
    
    public BuildObjectStage getStage(int index) {
    	if (stages.size() > index)
    		return stages.get(index);
    	else
    		return null;
    }
    
    private static final long serialVersionUID = 1L;
}

