package atavism.agis.objects;

import atavism.agis.plugins.*;
import atavism.server.engine.Namespace;
import atavism.server.engine.OID;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.*;
import atavism.server.plugins.*;
import atavism.server.plugins.InstanceClient.InstanceInfo;
import atavism.server.util.Log;
import atavism.server.worldmgr.CharacterFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class CharacterTemplate extends CharacterFactory {
	
	int aspect;
	int race;
	HashMap<String, CharacterStatProgression> startingStats = new HashMap<String, CharacterStatProgression>();
	ArrayList<Integer> startingSkills;
	int faction = 1;
	int instanceTemplateID;
	String portalName;
	Point spawnPoint;
	int health;
	int mana;
	int autoAttack = -1;
	ArrayList<CharacterStartingItem> items = new ArrayList<CharacterStartingItem>();
	
	
	@Override
	public OID createCharacter(String worldName, OID atavismID, Map properties) {
		HashMap<String, Serializable> props = (HashMap)properties;
		
		// First verify name is suitable and not taken
		String name = (String) props.get("characterName");
		String errorMsg = checkName(name);
		if (checkName(name) != null) {
			properties.put("errorMessage", errorMsg);
			return null;
		}
		
		// Name is ok, create template
		Template player = new Template("DefaultPlayer", -1, ObjectManagerPlugin.MOB_TEMPLATE);
		
		// TODO: remove display context from the server
		String meshName = (String) props.get("prefab");
		DisplayContext dc = new DisplayContext(meshName, true);
		dc.addSubmesh(new DisplayContext.Submesh("", ""));

		player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
		player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.player);
		player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
		
		// General properties such as instance, race,
		OID instanceOid = InstanceClient.getInstanceOid(instanceTemplateID);
		Log.debug("POP: getting population for instance: " + instanceTemplateID + " and Oid:" + instanceOid);
		
		// Verify there is space in the specified instance
		InstanceInfo instanceInfo = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_ALL_INFO);
    	if (instanceInfo.populationLimit != -1 && instanceInfo.playerPopulation >= instanceInfo.populationLimit) {
    		Log.debug("POP: got population: " + instanceInfo.playerPopulation + " and limit: " + instanceInfo.populationLimit);
    		instanceOid = ProxyPlugin.handleFullInstance(instanceInfo.templateID, instanceInfo);
    		instanceInfo = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_ALL_INFO);
    	}
    	
		if (instanceOid == null) {
            Log.error("SampleFactory: no 'default' instance");
            properties.put("errorMessage", "No default instance");
            return null;
		}
		Log.debug("SPAWN: spawn marker name=" + portalName);
		
        Marker spawnMarker = InstanceClient.getMarker(instanceOid, portalName);
        player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_NAME, name);
        player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_INSTANCE, instanceOid);
        player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_LOC, spawnPoint);
        player.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_ORIENT, Quaternion.Identity/*spawnMarker.getOrientation()*/);
        player.put(WorldManagerClient.NAMESPACE, "accountId", atavismID);
        player.put(WorldManagerClient.NAMESPACE, "model", meshName);
        player.put(WorldManagerClient.NAMESPACE, "race", race);
        player.put(WorldManagerClient.NAMESPACE, "gender", props.get("gender"));
        player.put(WorldManagerClient.NAMESPACE, "charactername", name);
        player.put(WorldManagerClient.NAMESPACE, "world", instanceTemplateID);
        player.put(WorldManagerClient.NAMESPACE, "category", 1);
        player.put(WorldManagerClient.NAMESPACE, "zone", "");
        LinkedList zones = new LinkedList();
        player.put(WorldManagerClient.NAMESPACE, "zones", zones);
        player.put(WorldManagerClient.NAMESPACE, "subzone", "");
        LinkedList subzones = new LinkedList();
        player.put(WorldManagerClient.NAMESPACE, "subzones", subzones);
        player.put(WorldManagerClient.NAMESPACE, "hearthLoc", spawnPoint);
        player.put(WorldManagerClient.NAMESPACE, "hearthInstance", instanceTemplateID);
        AOVector scale = new AOVector(1, 1, 1);
        player.put(WorldManagerClient.NAMESPACE, "scaleFactor", scale);
        player.put(WorldManagerClient.NAMESPACE, "walk_speed", 3);
        player.put(WorldManagerClient.NAMESPACE, AgisWorldManagerPlugin.PROP_MOVEMENT_STATE, AgisWorldManagerPlugin.MOVEMENT_STATE_RUNNING);
        
        // TODO: Read in faction data from the db
        HashMap factionData = new HashMap();
        //factionData.put(4, new PlayerFactionData(4, "Randarock", 1000, "The Legion", 1));
        player.put(WorldManagerClient.NAMESPACE, "factionData", factionData);
        player.put(WorldManagerClient.NAMESPACE, "faction", faction);
        
        //Starting inventory
        String startingItems = "";
        for (CharacterStartingItem item : items) {
        	Log.debug("ITEM: adding item: " + item.itemID + " to character");
        	if (item.equipped) {
        		startingItems = startingItems + "*" + item.itemID + ";";
        	} else {
        		for (int i = 0; i < item.count; i++) {
        			startingItems = startingItems + "" + item.itemID + ";";
        		}
        	}
        	Log.debug("ITEM: starting items is now: " + startingItems);
        }
        if (startingItems.endsWith(";"))
        	startingItems = startingItems.substring(0, startingItems.length()-1);
        Log.debug("ITEM: character is starting with items: " + startingItems);
        player.put(InventoryClient.NAMESPACE, InventoryClient.TEMPL_ITEMS, startingItems);
        player.put(InventoryClient.NAMESPACE, "isPlayer", true);
        
        // Other Properties
        player.put(WorldManagerClient.NAMESPACE, "busy", false);
        player.put(Namespace.QUEST, ":currentQuests", "");
        player.put(SocialClient.NAMESPACE, ":channels", "");
        player.put(Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_PERSISTENT, true);
        
        InstanceRestorePoint restorePoint = new InstanceRestorePoint(instanceTemplateID, spawnPoint);
        restorePoint.setFallbackFlag(true);
        LinkedList restoreStack = new LinkedList();
        restoreStack.add(restorePoint);
        player.put(Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_INSTANCE_RESTORE_STACK, restoreStack);
        player.put(Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_CURRENT_INSTANCE_NAME, instanceTemplateID);
                
        // Combat Properties/stats
        player.put(CombatClient.NAMESPACE, "aspect", aspect);
        player.put(CombatClient.NAMESPACE, "attackable", true);
        player.put(CombatClient.NAMESPACE, "attackType", "crush");
        player.put(CombatClient.NAMESPACE, "weaponType", "Unarmed");

        player.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_USERFLAG, true);
		player.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE, false);
		//player.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_REGEN_EFFECT, "regen effect");
		player.put(CombatClient.NAMESPACE, CombatPlugin.PROP_HITBOX, 1);
		// Stats from the database
		for (String stat : startingStats.keySet()) {
			int value = startingStats.get(stat).baseValue;
			player.put(CombatClient.NAMESPACE, stat, new AgisStat(stat, value));
		}
				
		//player.put(CombatClient.NAMESPACE, ":health", health);
		//player.put(CombatClient.NAMESPACE, ":mana", mana);
		
		player.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY, autoAttack);
		player.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_AUTOATTACK_BASE, autoAttack);
		
		// Add skill list
		player.put(CombatClient.NAMESPACE, ":startingSkills", startingSkills);
		
		LinkedList effectsList = new LinkedList();
        player.put(CombatClient.NAMESPACE, "effects", effectsList);
        
        // Custom properties
        //HashMap<String, Serializable> customProps = (HashMap)properties.get("customProperties");
        HashMap<String, HashMap<String, Serializable>> customPropMapping = new HashMap<String, HashMap<String, Serializable>>();
        for (String prop : props.keySet()) {
        	if (prop.startsWith("custom:")) {
        		String propName = prop.substring(7);
        		Serializable propValue = props.get(prop);
        		Log.debug("CUSTOM: got custom property: " + propName + " with value: " + propValue);
        		// Check for extra layer mapping
        		if (propName.contains(":")) {
        			String[] mapNames = propName.split(":");
        			String mapName = mapNames[0];
        			Log.debug("CUSTOM: got custom mapping: " + mapName + " with property: " + mapNames[1]);
        			if (customPropMapping.containsKey(mapName)) {
        				HashMap<String, Serializable> mapProps = customPropMapping.get(mapName);
        				if (propValue instanceof Double) {
        					mapProps.put(mapNames[1], (Float)propValue);
        				} else {
        					mapProps.put(mapNames[1], propValue);
        				}
        				customPropMapping.put(mapName, mapProps);
        			} else {
        				HashMap<String, Serializable> mapProps = new HashMap<String, Serializable>();
        				if (propValue instanceof Double) {
        					mapProps.put(mapNames[1], (Float)propValue);
        				} else {
        					mapProps.put(mapNames[1], propValue);
        				}
        				customPropMapping.put(mapName, mapProps);
        			}
        		} else {
        			if (propValue instanceof Double) {
        				player.put(WorldManagerClient.NAMESPACE, propName, (Float)propValue);
        			} else {
        				player.put(WorldManagerClient.NAMESPACE, propName, propValue);
        			}
        			Log.debug("CUSTOM: added custom property: " + propName + " with value: " + propValue);
        		}
        	}
        }
        
        // Handle any custom mappings
        for (String mapName : customPropMapping.keySet()) {
        	player.put(WorldManagerClient.NAMESPACE, mapName, customPropMapping.get(mapName));
        	Log.debug("CUSTOM: added custom mapping: " + mapName + " with numProps: " + customPropMapping.get(mapName).size());
        }
		
		// generate the object
        OID objOid = ObjectManagerClient.generateObject(-1, ObjectManagerPlugin.MOB_TEMPLATE, player);
        Log.debug("SampleFactory: generated obj oid=" + objOid);
        return objOid;
	}
	
	private String checkName (String name) {
		// check to see that the name is valid
		if (name == null || name.equals("")) {
		    return "Invalid name";
		}
		if (name.length() > AgisLoginPlugin.CHARACTER_NAME_MAX_LENGTH) {
		    return "Your characters name must contain less than " + (AgisLoginPlugin.CHARACTER_NAME_MAX_LENGTH+1) + " characters";
		}
		if (name.length() < AgisLoginPlugin.CHARACTER_NAME_MIN_LENGTH) {
		    return "Your characters name must contain more than " + (AgisLoginPlugin.CHARACTER_NAME_MIN_LENGTH-1) + " characters";
		}
		
		if (AgisLoginPlugin.CHARACTER_NAME_ALLOW_SPACES) {
			if (AgisLoginPlugin.CHARACTER_NAME_ALLOW_NUMBERS) {
				if (!name.matches("[a-zA-Z0-9 ]+")) {
					return "Your characters name can only contain letters";	  
				}
			} else {
				if (!name.matches("[a-zA-Z ]+")) {
					return "Your characters name can only contain letters";	  
				}
			}
		} else {
			if (AgisLoginPlugin.CHARACTER_NAME_ALLOW_NUMBERS) {
				if (!name.matches("[a-zA-Z0-9]+")) {
					return "Your characters name can only contain letters";	  
				}
			} else {
				if (!name.matches("[a-zA-Z]+")) {
					return "Your characters name can only contain letters";	  
				}
			}
		}
		
		
		return null;
	}

	public void setAspect(int aspect) {
		this.aspect = aspect;
	}
	
	public void setRace(int race) {
		this.race = race;
	}
	
	public void setFaction(int faction) {
		this.faction = faction;
	}
	
	public void setInstanceTemplateID(int instanceTemplateID) {
		this.instanceTemplateID = instanceTemplateID;
	}
	
	public void setPortalName(String portalName) {
		this.portalName = portalName;
	}
	
	public void setSpawnPoint(Point spawnPoint) {
		this.spawnPoint = spawnPoint;
	}
	
	public void setHealth(int health) {
		this.health = health;
	}
	
	public void setMana(int mana) {
		this.mana = mana;
	}
	
	public void setAutoAttack(int autoAttackAbility) {
		this.autoAttack = autoAttackAbility;
	}
	
	public void setStartingStats(HashMap<String, CharacterStatProgression> stats) {
		this.startingStats = stats;
	}
	public HashMap<String, CharacterStatProgression> getStartingStats() {
		return startingStats;
	}
	
	public void AddStatProgression(String name, int baseValue, float levelIncrease, float levelPercentIncrease) {
		CharacterStatProgression statProgress = new CharacterStatProgression();
		statProgress.baseValue = baseValue;
		statProgress.levelIncrease = levelIncrease;
		statProgress.levelPercentIncrease = levelPercentIncrease;
		startingStats.put(name, statProgress);
		//Log.debug("CHAR: adding stat progression: " + name);
	}
	
	public void setStartingSkills(ArrayList<Integer> skills) {
		this.startingSkills = skills;
	}
	
	public void addStartingItem(int itemID, int count, boolean equipped) {
		CharacterStartingItem item = new CharacterStartingItem();
		item.itemID = itemID;
		item.count = count;
		item.equipped = equipped;
		items.add(item);
	}
	
	public class CharacterStatProgression {
		public String statName;
		public int baseValue;
		public float levelIncrease;
		public float levelPercentIncrease;
	}
	
	public class CharacterStartingItem {
		public int itemID;
		public int count;
		public boolean equipped;
	}
	
}