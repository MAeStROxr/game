package atavism.agis.objects;

import atavism.agis.database.ContentDatabase;
import atavism.agis.plugins.*;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.*;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * A claim is an area of the world that a user has control of and can perform actions (such as build, dig) 
 * and place objects in.
 * @author Andrew Harrison
 *
 */
public class Claim implements Serializable, MessageCallback {
	
    public Claim() {
    }
    
    public Claim(int id, AOVector loc, int size, OID instanceOID, OID owner, DisplayContext dc, 
    		HashMap<String, Serializable> props) {
    	this.id = id;
    	this.loc = loc;
    	this.instanceOID = instanceOID;
    	this.owner = owner;
    	this.dc = dc;
    	this.props = props;
    	if (dc != null)
    		spawn();
    }
    
    /**
     * Add in an action from the database.
     * @param id
     * @param action
     * @param type
     * @param size
     * @param loc
     * @param material
     */
    public void AddActionData(int id, String action, String type, AOVector size, AOVector loc, AOVector normal, int material) {
    	ClaimAction claimAction = new ClaimAction();
    	claimAction.id = id;
    	claimAction.action = action;
    	claimAction.brushType = type;
    	claimAction.size = size;
    	claimAction.loc = loc;
    	claimAction.normal = normal;
    	claimAction.mat = material;
    	actions.add(claimAction);
    }
    
    /**
     * Add in an object from the database
     * @param id
     * @param templateId
     * @param stage
     * @param complete
     * @param gameObject
     * @param loc
     * @param orient
     * @param itemID
     * @param state
     * @param health
     * @param maxHealth
     * @param itemCounts
     */
    public void AddClaimObject(int id, int templateId, int stage, boolean complete, String gameObject, AOVector loc, 
    		Quaternion orient, int itemID, String state, int health, int maxHealth, HashMap<Integer, Integer> itemCounts) {
    	ClaimObject obj = new ClaimObject();
    	obj.id = id;
    	obj.templateId = templateId;
    	obj.stage = stage;
    	obj.complete = complete;
    	obj.gameObject = gameObject;
    	obj.itemID = itemID;
    	obj.loc = loc;
    	obj.orient = orient;
    	obj.state = state;
    	obj.health = health;
    	obj.maxHealth = maxHealth;
    	obj.itemReqs = itemCounts;
    	objects.add(obj);
    }
    
    /**
     * Add in a resource from the database.
     * @param id
     * @param itemID
     * @param count
     */
    public void AddClaimResource(int id, int itemID, int count) {
    	ClaimResource resource = new ClaimResource();
    	resource.id = id;
    	resource.itemID = itemID;
    	resource.count = count;
    	resources.put(itemID, resource);
    }
    
    /**
     * Add in a permission from the database
     * @param playerOid
     * @param playerName
     * @param permissionLevel
     */
    public void AddClaimPermission(OID playerOid, String playerName, int permissionLevel) {
    	ClaimPermission permission = new ClaimPermission();
    	permission.playerOid = playerOid;
    	permission.playerName = playerName;
    	permission.permissionLevel = permissionLevel;
    	permissions.put(playerOid, permission);
    }
    
    /**
     * Subscribes the instance to receive certain relevant messages that are sent to the world object 
     * created by this instance.
     */
    public void activate() {
        SubjectFilter filter = new SubjectFilter(objectOID);
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        eventSub = Engine.getAgent().createSubscription(filter, this);
        // Set the reaction radius tracker to alert the object if a player has entered its draw radius
        MobManagerPlugin.getTracker(instanceOID).addReactionRadius(objectOID, CLAIM_DRAW_RADIUS);
        active = true;
        Log.debug("CLAIM: claim with oid: " + objectOID + " activated");
    }
    
    public void deactivate() {
    	Engine.getAgent().removeSubscription(eventSub);
    	MobManagerPlugin.getTracker(instanceOID).removeReactionRadius(objectOID);
    	active = false;
    }
    
    /**
     * Deals with the messages the instance has picked up.
     */
    public void handleMessage(Message msg, int flags) {
    	if (active == false) {
    	    return;
    	}
    	if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS) {
    	    ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage)msg;
     	    Log.debug("Claim: myOid=" + objectOID + " objOid=" + nMsg.getSubject()
     		      + " inRadius=" + nMsg.getInRadius() + " wasInRadius=" + nMsg.getWasInRadius());
    	    if (nMsg.getInRadius()) {
    	    	addPlayer(nMsg.getSubject());
    	    } else {
    	    	// Remove subject from targets in range
    	    	removePlayer(nMsg.getSubject(), false);
    	    	//WorldManagerClient.sendObjChatMsg(nMsg.getSubject(), 2, "You have left the radius for Claim: " + id);
    	    	Map<String, Serializable> props = new HashMap<String, Serializable>();
    			props.put("ext_msg_subtype", "remove_claim");
    			props.put("claimID", id);

    			TargetedExtensionMessage eMsg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, nMsg.getSubject(), 
    				nMsg.getSubject(), false, props);
    			Engine.getAgent().sendBroadcast(eMsg);
    	    }
    	}
    }
    
    /**
     * Add a player to the update list for this Claim. The player will receive data about the claim and any updates
     * that occur.
     * @param playerOID
     */
    public void addPlayer(OID playerOID) {
    	sendClaimData(playerOID);
			
		if (!playersInRange.contains(playerOID)) {
	    	playersInRange.add(playerOID);
	    	sendActionsToPlayer(playerOID);
	    	sendObjectsToPlayer(playerOID);
	    }
    }
    
    /**
     * Removes a player from the claim. They will no longer receive updates.
     * @param playerOID
     * @param removeLastID
     */
    public void removePlayer(OID playerOID, boolean removeLastID) {
    	if (playersInRange.contains(playerOID))
    		playersInRange.remove(playerOID);
    	if (removeLastID && playersLastIDSent.containsKey(playerOID))
    		playersLastIDSent.remove(playerOID);
    	if (/*removeLastID && */playersLastObjectIDSent.containsKey(playerOID))
    		playersLastObjectIDSent.remove(playerOID);
    }
    
    
    /**
     * Alerts the players and owner of the Claim that its settings have been updated.
     * @param currentPlayer
     */
    public void claimUpdated(OID currentPlayer) {
    	for (OID playerOid : playersInRange) {
    		sendClaimData(playerOid);
    	}
    	sendClaimData(currentPlayer);
    }
    
    /**
     * Sends down the claim information to the specified player.
     * @param playerOID
     */
    public void sendClaimData(OID playerOid) {
    	OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
    	props.put("ext_msg_subtype", "claim_data");
		props.put("claimID", id);
		props.put("claimName", name);
		props.put("claimLoc", loc);
		props.put("claimArea", size);
		props.put("forSale", forSale);
		if (permissions.containsKey(playerOid))
			props.put("permissionLevel", permissions.get(playerOid).permissionLevel);
		else
			props.put("permissionLevel", 0);
		if (forSale) {
			props.put("cost", cost);
			props.put("currency", currency);
		}
    	
    	if (owner != null && owner.equals(accountID)) {
    		props.put("myClaim", true);
    		// Also include the resources
    		/*props.put("resourceCount", resources.size());
    		int pos = 0;
    		for (int itemID : resources.keySet()) {
    			props.put("resource" + pos, itemID);
    			props.put("resource" + pos + "Count", resources.get(itemID).count);
    		}*/
    	} else {
    		props.put("myClaim", false);
    	}
    	
    	int permissionCount = 0;
    	if ((owner != null && owner.equals(accountID)) || (permissions.containsKey(playerOid) 
    			&& permissions.get(playerOid).permissionLevel >= PERMISSION_ADD_USERS)) 
    	{
    		for (ClaimPermission per : permissions.values()) {
    			props.put("permission_" + permissionCount, per.playerName);
    			props.put("permissionLevel_" + permissionCount, per.permissionLevel);
    			permissionCount++;
    		}
    	}
    	props.put("permissionCount", permissionCount);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    public void sendClaimRemovedData(OID playerOID) {
    	if (playerOID == null)
    		return;
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
    	props.put("ext_msg_subtype", "remove_claim_data");
		props.put("claimID", id);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOID, 
				playerOID, false, props);
			Engine.getAgent().sendBroadcast(msg);
    }
    
    /**
     * An external call to spawn a world object for the claim.
     * @param instanceOID
     */
    public void spawn(OID instanceOID) {
    	this.instanceOID = instanceOID;
    	spawn();
    }
    
    /**
     * Spawn a world object for the claim.
     */
    public void spawn() {
    	Template markerTemplate = new Template();
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_NAME, "Claim" + id);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.mob);
    	markerTemplate.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_INSTANCE, instanceOID);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_LOC, new Point(loc));
    	//markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_ORIENT, orientation);
    	DisplayContext dc = new DisplayContext(model, true);
		dc.addSubmesh(new DisplayContext.Submesh("", ""));
		markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
		markerTemplate.put(Namespace.WORLD_MANAGER, "model", model); 
		AOVector v = new AOVector(size, size, size);
		markerTemplate.put(WorldManagerClient.NAMESPACE, "scale", v);
    	// Put in any additional props
    	if (props != null) {
    		for (String propName : props.keySet()) {
    			markerTemplate.put(Namespace.WORLD_MANAGER, propName, props.get(propName));
    		}
    	}
    	// Create the object
    	objectOID = ObjectManagerClient.generateObject(ObjectManagerClient.BASE_TEMPLATE_ID,
                ObjectManagerClient.BASE_TEMPLATE, markerTemplate);
    	
    	if (objectOID != null) {
    		// Need to create an interpolated world node to add a tracker/reaction radius to the claim world object
    		BasicWorldNode bwNode = WorldManagerClient.getWorldNode(objectOID);
    		InterpolatedWorldNode iwNode = new InterpolatedWorldNode(bwNode);
    		claimEntity = new ClaimEntity(objectOID, iwNode);
    		EntityManager.registerEntityByNamespace(claimEntity, Namespace.MOB);
    		MobManagerPlugin.getTracker(instanceOID).addLocalObject(objectOID, CLAIM_DRAW_RADIUS);
        
            WorldManagerClient.spawn(objectOID);
            Log.debug("CLAIM: spawned claim at : " + loc);
            activate();
        }
    }
    
    /**
     * Changes the claim owner to the buyer of the claim. Alerts all players nearby that
     * the claim owner has changed.
     * @param buyerOID
     * @param newOwner
     * @return
     */
    public OID changeClaimOwner(OID buyerOID, OID newOwner) {
    	forSale = false;
    	OID oldOwner = owner;
    	owner = newOwner;
    	sellerName = "";
    	
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "claim_updated");
		props.put("claimID", id);
		props.put("forSale", forSale);
		if (forSale) {
			props.put("cost", cost);
			props.put("currency", currency);
		}
    	
		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			if (playerOid.equals(buyerOID)) {
				props.put("myClaim", true);
			} else {
				props.put("myClaim", false);
			}
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
		}
		
		sendClaimRemovedData(oldOwner);
		sendClaimData(buyerOID);
		
    	return oldOwner;
    }
    
    /**
     * Alerts all nearby players that the claim has been deleted, then despawns the 
     * world object.
     */
    public void claimDeleted() {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "claim_deleted");
		props.put("claimID", id);

		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
		}
		
		// despawn the object
		deactivate();
		WorldManagerClient.despawn(objectOID);
    }
    
    /**
     * Adds a players permission to the claim. Verifies that the permission giver can give the permission
     * and that the user does not already have a higher permission level.
     * @param giverOid
     * @param targetOid
     * @param playerName
     * @param permissionLevel
     */
    public void addPermission(OID giverOid, OID giverAccountID, OID targetOid, String playerName, int permissionLevel) {
    	// First check if the permission giver has permission to give permission
    	if (!permissions.containsKey(giverOid) && !owner.equals(giverAccountID)) {
    		return;
    	}
    	if (permissions.containsKey(giverOid) && permissions.get(giverOid).permissionLevel < PERMISSION_ADD_USERS) {
    		return;
    	}
    	
    	// Check if the user already has a permission
    	if (permissions.containsKey(targetOid)) {
    		if (permissionLevel > permissions.get(targetOid).permissionLevel) {
    			// Update their permission level
    			permissions.get(targetOid).permissionLevel = permissionLevel;
    			cDB.updateClaimPermission(id, targetOid, permissionLevel);
    		}
    	} else {
    		// Add permission
    		ClaimPermission permission = new ClaimPermission();
    		permission.playerOid = targetOid;
    		permission.playerName = playerName;
    		permission.permissionLevel = permissionLevel;
    		permissions.put(targetOid, permission);
    		cDB.writeClaimPermission(id, targetOid, playerName, permissionLevel);
    	}
    	for (OID playerOid : playersInRange) {
    		sendClaimData(playerOid);
    	}
    }
    
    /**
     * Removes a players permission from the claim. Verifies that the permission remover does
     * have the permission required to remove another users permission and that the target is
     * not the owner of the claim.
     * @param removerOid
     * @param targetOid
     */
    public void removePermission(OID removerOid, OID removerAccountID, OID targetOid) {
    	// First check if the permission giver has permission to give permission
    	if (!permissions.containsKey(removerOid) && !owner.equals(removerAccountID)) {
    		return;
    	}
    	if (permissions.containsKey(removerOid) && 
    			permissions.get(removerOid).permissionLevel < PERMISSION_MANAGE_USERS) {
    		return;
    	}
    	
    	// Ensure the user being removed is not the owner
    	if (targetOid.equals(owner)) {
    		return;
    	}
    	
    	if (!permissions.containsKey(targetOid) || (permissions.get(targetOid).permissionLevel >= PERMISSION_MANAGE_USERS 
    			&& !owner.equals(removerAccountID))) {
    		return;
    	}
    	
    	permissions.remove(targetOid);
    	cDB.deleteClaimPermission(id, targetOid);
    	for (OID playerOid : playersInRange) {
    		sendClaimData(playerOid);
    	}
    }
    
    /**
     * Returns the permission level for the player. Checks if the players
     * account matches the owner, and failing that will see if it can find the 
     * characters permission level.
     * @param playerOid
     * @param accountID
     * @return
     */
    public int getPlayerPermission(OID playerOid, OID accountID) {
    	if (accountID.equals(owner)) {
    		return PERMISSION_OWNER;
    	} else if (permissions.containsKey(playerOid)) {
    		return permissions.get(playerOid).permissionLevel;
    	}
    	return 0;
    }
    
    /*
     * Action Functions
     */
    
    /**
     * Add an action to the claim. The data is stored in the database and sent down to all players within
     * the draw/reaction radius.
     * @param action
     * @param type
     * @param size
     * @param loc
     * @param material
     */
    public void performClaimAction(String action, String type, AOVector size, AOVector loc, AOVector normal, int material) {
    	// Add to list
    	ClaimAction claimAction = new ClaimAction();
    	claimAction.action = action;
    	claimAction.brushType = type;
    	claimAction.size = size;
    	claimAction.loc = loc;
    	claimAction.normal = normal;
    	claimAction.mat = material;
    	actions.add(claimAction);
    	// Save action to the database
    	claimAction.id = cDB.writeClaimAction(id, action, type, size, loc, normal, material);
    	// Send the action down to all in the radius
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "claim_action");
		props.put("action", action);
		props.put("type", type);
		props.put("size", size);
		props.put("loc", loc);
		props.put("normal", normal);
		props.put("mat", material);

		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
			// Add to mapping of last action sent to the player
			playersLastIDSent.put(playerOid, claimAction.id);
		}
    }
    
    /**
     * Undo the last action performed. Deletes the last entry from the database and sends the undo 
     * message to all players within the reaction radius.
     */
    public void undoAction() {
    	ClaimAction lastAction = actions.removeLast();
    	cDB.deleteClaimAction(lastAction.id);
    	
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "claim_action");
		props.put("action", "heal");
		props.put("type", lastAction.brushType);
		props.put("size", lastAction.size);
		props.put("loc", lastAction.loc);
		props.put("normal", lastAction.normal);
		props.put("mat", lastAction.mat);

		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
		}
    }
    
    public void sendActionsToPlayers() {
    	for (OID playerOid : playersInRange) {
    		sendActionsToPlayer(playerOid);
    		sendObjectsToPlayer(playerOid);
    	}
    }
    
    /**
     * Used when a player first enters the radius for the claim. Sends down all the changes. 
     * Checks the last action that was sent down to the player and will not send any more
     * @param playerOid
     */
    private void sendActionsToPlayer(OID playerOid) {
    	// Send 50 changes per message
    	int chunkSize = 50;
    	// Only send data higher than the last id that was sent to the player
    	int lastIDSent = -1;
    	if (playersLastIDSent.containsKey(playerOid))
    		lastIDSent = playersLastIDSent.get(playerOid);
    	// No need to continue if the last id sent is equal to or greater than the number of actions
    	if (actions.size() == 0 || lastIDSent >= actions.getLast().id)
    		return;
    	for (int i = 0; i < actions.size(); i += chunkSize) {
    		Map<String, Serializable> props = new HashMap<String, Serializable>();
    		props.put("ext_msg_subtype", "claim_action_bulk");
    		int actionCount = chunkSize;
    		if (actions.size() - i < chunkSize) {
    			actionCount = actions.size() - i;
    		}
    		
    		Log.debug("CLAIM: Comparing lastID: " + lastIDSent + " against last from chunk: " + actions.get(i+actionCount-1).id);
    		if (actions.get(i+actionCount-1).id <= lastIDSent)
    			continue;
    		
    		int numActions = 0;
    		for (int j = 0; j < actionCount; j++) {
    			Log.debug("CLAIM: Comparing action id: " + actions.get(j+i).id + " against lastID: " + lastIDSent);
    			if (actions.get(j+i).id <= lastIDSent) {
    				continue;
    			}
    			String actionString = actions.get(j+i).action + ";" + actions.get(j+i).brushType + ";";
    			actionString += actions.get(j+i).size.getX() + ","+ actions.get(j+i).size.getY() + "," + actions.get(j+i).size.getZ() + ";";		
    			actionString += actions.get(j+i).loc.getX() + ","+ actions.get(j+i).loc.getY() + "," + actions.get(j+i).loc.getZ() + ";";
    			actionString += actions.get(j+i).normal.getX() + ","+ actions.get(j+i).normal.getY() + "," + actions.get(j+i).normal.getZ() + ";";
    			actionString += actions.get(j+i).mat;
    			props.put("action_" + j, actionString);
    			//Log.debug("CLAIM: Sending actionString: " + actionString);
    			/*props.put("type_" + j, actions.get(j+i).brushType);
    			props.put("size_" + j, actions.get(j+i).size);
    			props.put("loc_" + j, actions.get(j+i).loc);
    			props.put("mat_" + j, actions.get(j+i).mat);*/
    			playersLastIDSent.put(playerOid, actions.get(j+i).id);
    			numActions++;
    		}
    		Log.debug("CLAIM: Sending action count: " + numActions + " to player: " + playerOid);
    		props.put("numActions", numActions);
    		TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    			Engine.getAgent().sendBroadcast(msg);
    		// New code - only send the one chunk
    		break;
    	}
    }
    
    
    /*
     * Object Functions
     */
    
    boolean hasRequiredWeapon(OID playerOid, BuildObjectTemplate buildObjectTemplate) {
    	if (buildObjectTemplate.weaponReq != null && !buildObjectTemplate.weaponReq.equals("") && 
    			!buildObjectTemplate.weaponReq.toLowerCase().contains("none")) {
			String weaponType = (String) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, "weaponType");
			Log.debug("RESOURCE: checking weaponReq: " + buildObjectTemplate.weaponReq + " against: " + weaponType);
			if (!weaponType.contains(buildObjectTemplate.weaponReq)) {
				return false;
			}
		}
    	return true;
    }
    
    boolean isCloseEnough(OID playerOid, BuildObjectTemplate buildObjectTemplate, AOVector loc) {
    	BasicWorldNode wNode = WorldManagerClient.getWorldNode(playerOid);
        if (wNode == null) {
        	Log.error("RANGE CHECK: wnode is null for builder: " + playerOid);
        	return false;
        }
        Point casterLoc = wNode.getLoc();
        int distance = (int)Point.distanceTo(casterLoc, new Point(loc));
        if (distance > buildObjectTemplate.maxDistance) {
        	Log.debug("RANGE CHECK: distance: " + distance + " is greater than: " + buildObjectTemplate.maxDistance 
        			+ " with loc: " + loc);
        	return false;
        }
    	return true;
    }

    /**
     * Kicks off a BuildTask to add a claim object to the claim based on the given buildObjectTemplate.
     * @param playerOid
     * @param buildObjectTemplate
     * @param loc
     * @param orient
     * @param itemID
     * @param itemOid
     * @return
     */
    public boolean buildClaimObject(OID playerOid, BuildObjectTemplate buildObjectTemplate, AOVector loc, Quaternion orient, 
    		int itemID, OID itemOid) {
    	// Player cannot start a new task if they are currently on a task
    	if (getPlayersBuildTask(playerOid) != null) {
    		return false;
    	}
    	
    	// First verify the user has the right weapon equipped, is close enough and has the items required
    	if (!hasRequiredWeapon(playerOid, buildObjectTemplate)) {
    		if (buildObjectTemplate.weaponReq.startsWith("a") || buildObjectTemplate.weaponReq.startsWith("e") || 
					buildObjectTemplate.weaponReq.startsWith("i") || buildObjectTemplate.weaponReq.startsWith("o") || 
					buildObjectTemplate.weaponReq.startsWith("u")) {
				ExtendedCombatMessages.sendErrorMessage(playerOid, "An " + buildObjectTemplate.weaponReq + " is required to build this object");
			} else {
				ExtendedCombatMessages.sendErrorMessage(playerOid, "A " + buildObjectTemplate.weaponReq + " is required to build this object");
			}
    		return false;
    	}
    	
    	if (!isCloseEnough(playerOid, buildObjectTemplate, loc)) {
    		ExtendedCombatMessages.sendErrorMessage(playerOid, "You are too far away from the object to build it");
    		return false;
    	}
    	
    	LinkedList<Integer> components = new LinkedList<Integer>();
		LinkedList<Integer> componentCounts = new LinkedList<Integer>();
		for (int itemReq : buildObjectTemplate.getStage(0).itemReqs.keySet()) {
			components.add(itemReq);
			componentCounts.add(buildObjectTemplate.getStage(0).itemReqs.get(itemReq));
		}
    	boolean hasItems = AgisInventoryClient.checkComponents(playerOid, components, componentCounts);
		if (!hasItems) {
			ExtendedCombatMessages.sendErrorMessage(playerOid, "You do not have the required items to build this object");
			return false;
		}
		
		// Check if the user has the required skill level
		int playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, buildObjectTemplate.skill);
		if (buildObjectTemplate.skill > 0 && playerSkillLevel < buildObjectTemplate.skillLevelReq) {
			ExtendedCombatMessages.sendErrorMessage(playerOid, "You do not have the required skill level to build this object");
			return false;
		}
		
		// Now create the build task
		ClaimTask task = new ClaimTask();
		task.StartBuildTask(buildObjectTemplate, loc, orient, itemID, itemOid, playerOid, this);
    	tasks.add(task);
    	
    	// Schedule the task completion
    	if (buildObjectTemplate.getStages().get(0).buildTimeReq > 0) {
    		Engine.getExecutor().schedule(task, (long) buildObjectTemplate.getStages().get(0).buildTimeReq * 1000, TimeUnit.MILLISECONDS);
    		task.sendStartBuildTask(buildObjectTemplate.getStages().get(0).buildTimeReq);
    		return true;
    	} else {
    		task.run();
    		return false;
    	}
    }
    
    /**
     * Add an object to the claim. The data is stored in the database and sent down to all players within
     * the draw/reaction radius.
     * @param task
     */
    private void addClaimObject(ClaimTask task) {
    	Log.debug("BUILD: adding claim object from task");
    	// Add to list
    	ClaimObject claimObject = new ClaimObject();
    	claimObject.templateId = task.template.id;
    	claimObject.gameObject = task.template.getStage(0).getGameObject();
    	claimObject.loc = task.loc.sub(this.loc);
    	claimObject.orient = task.orient;
    	claimObject.itemID = task.itemID;
    	claimObject.stage = 0;
    	claimObject.health = 0;
    	// Remove items from the creators inventory
    	HashMap<Integer, Integer> itemsToRemove = new HashMap<Integer, Integer>();
    	for (Integer item : task.template.getStage(0).getItemReqs().keySet()) {
			if (item > 0) {
				itemsToRemove.put(item, task.template.getStage(0).getItemReqs().get(item));
			}
		}
    	AgisInventoryClient.removeGenericItems(task.playerOid, itemsToRemove, false);
    	// Set itemReqs for next level
    	HashMap<Integer, Integer> itemReqs = new HashMap<Integer, Integer>();
    	if (task.template.getStages().size() > 1) {
    		// Max health should be the maxHealth of all future items combined (if there are any)
    		claimObject.maxHealth = task.template.getStage(1).health;
    		for (int itemReq : task.template.getStages().get(1).getItemReqs().keySet()) {
    			itemReqs.put(itemReq, task.template.getStages().get(1).getItemReqs().get(itemReq));
    		}
    	} else {
    		claimObject.maxHealth = 0;
    	}
    	claimObject.itemReqs = itemReqs;
    	objects.add(claimObject);
    	// Save action to the database
    	claimObject.id = cDB.writeClaimObject(id, task.template.id, claimObject.stage, claimObject.complete, 
    			claimObject.gameObject, claimObject.loc, claimObject.orient, task.itemID, claimObject.state, 
    			claimObject.health, claimObject.maxHealth, claimObject.itemReqs);
    	// Send the object down to all in the radius
    	sendObject(claimObject);
    }
    
    public boolean interruptBuildTask(OID playerOid) {
    	ClaimTask task = getPlayersBuildTask(playerOid);
    	if (task != null) {
    		task.interrupt();
    		// Remove task from list
    		tasks.remove(task);
    		return true;
    	}
    	return false;
    }
    
    private ClaimTask getPlayersBuildTask(OID playerOid) {
    	for (ClaimTask task : tasks) {
    		if (task.playerOid.equals(playerOid)) {
    			return task;
    		}
    	}
    	return null;
    }
    
    private void sendObject(ClaimObject claimObject) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "claim_object");
		props.put("id", claimObject.id);
		props.put("templateID", claimObject.templateId);
		props.put("gameObject", claimObject.gameObject);
		props.put("loc", claimObject.loc);
		props.put("orient", claimObject.orient);
		props.put("state", claimObject.state);
		props.put("maxHealth", claimObject.maxHealth);
		props.put("health", claimObject.health);
		props.put("complete", claimObject.complete);
		props.put("claimID", id);
		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
			// Add to mapping of last action sent to the player
			playersLastObjectIDSent.put(playerOid, claimObject.id);
		}
    }
    
    private ClaimObject getClaimObject(int objectID) {
    	ClaimObject cObject = null;
    	for (ClaimObject cObj : objects) {
    		if (cObj.id == objectID) {
    			cObject = cObj;
    			break;
    		}
    	}
    	return cObject;
    }
    
    public int removeClaimObject(int objectID) {
    	// Get the claim object
    	ClaimObject cObject = getClaimObject(objectID);
    	
    	if (cObject == null)
    		return -1;
    	int itemID = cObject.itemID;
    	
    	sendRemoveObject(cObject);
		objects.remove(cObject);
		cDB.deleteClaimObject(objectID);
    	
    	return itemID;
    }
    
    private void sendRemoveObject(ClaimObject cObject) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "remove_claim_object");
		props.put("id", cObject.id);
		props.put("claimID", id);
		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
		}
    }
    
    public void moveClaimObject(int objectID, AOVector loc, Quaternion orient) {
    	ClaimObject cObject = getClaimObject(objectID);
    	if (cObject == null)
    		return;
    	
    	cObject.loc = loc.sub(this.loc);
    	cObject.orient = orient;
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "move_claim_object");
		props.put("id", cObject.id);
		props.put("loc", cObject.loc);
		props.put("orient", orient);
		props.put("claimID", id);
		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
		}
    	
    	cDB.updateClaimObjectPosition(objectID, cObject.loc, orient);
    }
    
    public void updateClaimObjectState(int objectID, String state) {
    	ClaimObject cObject = getClaimObject(objectID);
    	if (cObject == null)
    		return;
    	
    	cObject.state = state;
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "update_claim_object_state");
		props.put("id", cObject.id);
		props.put("state", cObject.state);
		props.put("claimID", id);
		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			Engine.getAgent().sendBroadcast(msg);
		}
    	
    	cDB.updateClaimObjectState(objectID, cObject.templateId, cObject.stage, cObject.complete, state, 
    			cObject.gameObject, cObject.health, cObject.maxHealth, cObject.itemReqs);
    }
   
    /**
     * Adds the specified item to a claim object with the aim of upgrading it. Kicks off a BuildTask
     * if the right item is provided.
     * @param playerOid
     * @param objectID
     * @param itemID
     * @param itemOid
     * @param count
     */
    public boolean addItemToUpgradeClaimObject(OID playerOid, int objectID, int itemID, OID itemOid, int count) {
    	// Player cannot start a new task if they are currently on a task
    	if (getPlayersBuildTask(playerOid) != null) {
    		ExtendedCombatMessages.sendErrorMessage(playerOid, "You cannot perform another task yet");
    		return false;
    	}
    	ClaimObject cObject = getClaimObject(objectID);
    	if (cObject == null)
    		return false;
    	
    	BuildObjectTemplate tmpl = VoxelClient.getBuildingTemplate(cObject.templateId);
    	if (tmpl == null) {
    		return false;
    	}
    	
    	// Verify the player has the required weapon and is close enough
    	if (!hasRequiredWeapon(playerOid, tmpl)) {
    		if (tmpl.weaponReq.startsWith("a") || tmpl.weaponReq.startsWith("e") || 
    				tmpl.weaponReq.startsWith("i") || tmpl.weaponReq.startsWith("o") || 
    				tmpl.weaponReq.startsWith("u")) {
				ExtendedCombatMessages.sendErrorMessage(playerOid, "An " + tmpl.weaponReq + " is required to repair this object");
			} else {
				ExtendedCombatMessages.sendErrorMessage(playerOid, "A " + tmpl.weaponReq + " is required to repair this object");
			}
    		return false;
    	}
    	
    	if (!isCloseEnough(playerOid, tmpl, AOVector.add(loc, cObject.loc))) {
    		ExtendedCombatMessages.sendErrorMessage(playerOid, "You are too far away from the object to repair it");
    		return false;
    	}
    	
    	// Is this a repair job?
    	boolean repairJob = false;
    	if (cObject.complete && (cObject.health < cObject.maxHealth || cObject.stage+1 < tmpl.getStages().size()))
    		repairJob = true;
    	
    	// Check if there is another stage (for non repair jobs)
    	if (!repairJob && (cObject.stage + 1) >= tmpl.getStages().size()) {
    		ExtendedCombatMessages.sendErrorMessage(playerOid, "That object cannot be upgraded");
    		return false;
    	}
    	
    	ClaimTask task = new ClaimTask();
    	float buildTime = 0;
    	
    	if (repairJob) {
    		task.StartRepairTask(tmpl, cObject, itemID, itemOid, playerOid, this);
        	tasks.add(task);
        	Log.debug("BUILD: getting repair time for stage: " + cObject.stage + " with num stages: " + tmpl.getStages().size());
        	buildTime = tmpl.getStage(cObject.stage).buildTimeReq;
    	} else if (cObject.itemReqs.containsKey(itemID) && cObject.itemReqs.get(itemID) > 0) {
    		task.StartUpgradeTask(tmpl, cObject, itemID, itemOid, playerOid, this);
        	tasks.add(task);
        	buildTime = tmpl.getStage(cObject.stage + 1).buildTimeReq;
    	} else {
    		ExtendedCombatMessages.sendErrorMessage(playerOid, "That item cannot be used on this object");
    		return false;
    	}
    	
    	// Schedule the task completion
    	if (buildTime > 0) {
    		Engine.getExecutor().schedule(task, (long) buildTime * 1000, TimeUnit.MILLISECONDS);
    		task.sendStartBuildTask(buildTime);
    		return true;
    	} else {
    		task.run();
    		return false;
    	}
    }
    
    private void upgradeClaimObject(ClaimTask task) {
    	Log.debug("BUILD: upgrading claim object from task");
    	// Add to list
    	int count = task.cObject.itemReqs.get(task.itemID);
    	count--;
    	Log.debug("BUILD: got itemReq");
    	//if (count > 0) {
    		// Put the item back in with the lowered count 
    		task.cObject.itemReqs.put(task.itemID, count);
    		Log.debug("BUILD: put back itemReq");
    	//} else {
    		// Remove item
        	//task.cObject.itemReqs.remove(task.itemID);
    	//}
    	
    	Template itemTemplate = AgisInventoryClient.getGenericItemData(task.playerOid, task.itemID);
    	Log.debug("BUILD: got itemTemplate");
    	if (itemTemplate != null) {
    		Integer buildHealthValue = (Integer)itemTemplate.get(InventoryClient.ITEM_NAMESPACE, "buildHealthValue");
    		if (buildHealthValue != null)
    			task.cObject.health += buildHealthValue;
    	}
    	Log.debug("BUILD: upgraded health");
    	boolean readyToUpgrade = true;
    	for (int itemCount : task.cObject.itemReqs.values()) {
    		if (itemCount > 0)
    			readyToUpgrade = false;
    	}
    	
    	Log.debug("BUILD: reduced item count from item reqs, count is now: " + count + " for item: " + task.itemID);
    	// Check if there are any items left, if not - upgrade!
    	if (readyToUpgrade) {
    		Log.debug("BUILD: itemreqs are empty, upgrading item to : " + task.template);
    		task.cObject.stage++;
    		task.cObject.gameObject = task.template.getStage(task.cObject.stage).getGameObject();
    		if ((task.cObject.stage + 1) < task.template.getStages().size()) {
    			// Get the next upgraded version and set the item reqs from that
    			task.cObject.itemReqs = task.template.getStage(task.cObject.stage+1).getItemReqs();
    			task.cObject.maxHealth = task.template.getStage(task.cObject.stage+1).getHealth();
    			task.cObject.health = 0;
    		} else {
    			// We are at the final stage, set complete
    			//task.cObject.itemReqs = new HashMap<Integer, Integer>();
    			task.cObject.complete = true;
    		}
    		// Need to send down new model
    		sendRemoveObject(task.cObject);
    		sendObject(task.cObject);
    	}
    	
    	Log.debug("BUILD: about to update state on the database");
    	// Save the updated state, remove the item from the players inventory and send the info down to the player
    	cDB.updateClaimObjectState(task.cObject.id, task.cObject.templateId, task.cObject.stage, task.cObject.complete, 
    			task.cObject.state, task.cObject.gameObject, task.cObject.health, task.cObject.maxHealth, task.cObject.itemReqs);
    	Log.debug("BUILD: removing specific item: " + task.itemOid + " from player: " + task.playerOid);
    	ExtendedCombatMessages.sendAnouncementMessage(task.playerOid, "Building improvement complete", "");
        sendObjectInfo(task.playerOid, task.cObject.id);
    }
    
    /**
     * 
     * @param task
     */
    private void repairClaimObject(ClaimTask task) {
    	Log.debug("BUILD: repairing claim object from task");
    	
    	Template itemTemplate = AgisInventoryClient.getGenericItemData(task.playerOid, task.itemID);
    	if (itemTemplate != null) {
    		int buildHealthValue = (Integer)itemTemplate.get(InventoryClient.ITEM_NAMESPACE, "buildHealthValue");
    		task.cObject.health += buildHealthValue;
    	}
    	
    	// Check if health is now higher than max
    	if (task.cObject.health >= task.cObject.maxHealth) {
    		Log.debug("BUILD: health has reached max for : " + task.template);
    		if ((task.cObject.stage + 1) == task.template.getStages().size()) {
    			// We are at final stage, set health to maxHealth
    			task.cObject.health = task.cObject.maxHealth;
    		} else {
    			task.cObject.stage++;
    			task.cObject.gameObject = task.template.getStage(task.cObject.stage).getGameObject();
    			// Get the health and item reqs from the new stage
    			task.cObject.itemReqs = task.template.getStage(task.cObject.stage).getItemReqs();
    			task.cObject.health = task.cObject.health - task.cObject.maxHealth;
    			task.cObject.maxHealth = task.template.getStage(task.cObject.stage).getHealth();
    			
    			// Need to send down new model
        		sendRemoveObject(task.cObject);
        		sendObject(task.cObject);
    		}
    	}
    	
    	Log.debug("BUILD: about to update state on the database");
    	// Save the updated state, remove the item from the players inventory and send the info down to the player
    	cDB.updateClaimObjectState(task.cObject.id, task.cObject.templateId, task.cObject.stage, task.cObject.complete, 
    			task.cObject.state, task.cObject.gameObject, task.cObject.health, task.cObject.maxHealth, task.cObject.itemReqs);
    	Log.debug("BUILD: removing specific item: " + task.itemOid + " from player: " + task.playerOid);
    	ExtendedCombatMessages.sendAnouncementMessage(task.playerOid, "Building repair complete", "");
        sendObjectInfo(task.playerOid, task.cObject.id);
    }
    
    /**
     * Sends down the information about a claim object to the requesting player. The message will
     * contain information such as what items are needed for it to be upgraded.
     * @param playerOid
     * @param objectID
     */
    public void sendObjectInfo(OID playerOid, int objectID) {
    	ClaimObject cObject = getClaimObject(objectID);
    	if (cObject == null)
    		return;
    	
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
    	props.put("ext_msg_subtype", "claim_object_info");
		props.put("claimID", id);
		props.put("id", cObject.id);
		props.put("health", cObject.health);
		props.put("maxHealth", cObject.maxHealth);
		props.put("complete", cObject.complete);
		int itemCount = 0;
		for (Integer itemID : cObject.itemReqs.keySet()) {
			if (itemID > 0) {
				props.put("item" + itemCount, itemID);
				props.put("itemCount" + itemCount, cObject.itemReqs.get(itemID));
				itemCount++;
			}
		}
    	
		props.put("itemCount", itemCount);
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    /**
     * Used when a player first enters the radius for the claim. Sends down all the changes.
     * @param playerOid
     */
    private void sendObjectsToPlayer(OID playerOid) {
    	// Send 50 changes per message
    	int chunkSize = 50;
    	// Only send data higher than the last id that was sent to the player
    	int lastIDSent = -1;
    	if (playersLastObjectIDSent.containsKey(playerOid))
    		lastIDSent = playersLastObjectIDSent.get(playerOid);
    	// No need to continue if the last id sent is equal to or greater than the number of actions
    	if (objects.size() == 0 || lastIDSent >= objects.getLast().id)
    		return;
    	
    	for (int i = 0; i < objects.size(); i += chunkSize) {
    		Map<String, Serializable> props = new HashMap<String, Serializable>();
    		props.put("ext_msg_subtype", "claim_object_bulk");
    		props.put("claimID", id);
    		int actionCount = chunkSize;
    		if (objects.size() - i < chunkSize) {
    			actionCount = objects.size() - i;
    		}
    		
    		Log.debug("CLAIM: Comparing object lastID: " + lastIDSent + " against last from chunk: " + objects.get(i+actionCount-1).id);
    		if (objects.get(i+actionCount-1).id <= lastIDSent)
    			continue;
    		
    		int numObjects = 0;
    		for (int j = 0; j < actionCount; j++) {
    			Log.debug("CLAIM: Comparing action id: " + objects.get(j+i).id + " against lastID: " + lastIDSent);
    			if (objects.get(j+i).id <= lastIDSent) {
    				continue;
    			}
    			String actionString = objects.get(j+i).id + ";" + objects.get(j+i).templateId + ";" + objects.get(j+i).gameObject + ";" + objects.get(j+i).loc.getX() + "," 
    			+ objects.get(j+i).loc.getY() + "," + objects.get(j+i).loc.getZ() + ";" + objects.get(j+i).orient.getX() 
    			+ "," + objects.get(j+i).orient.getY() + "," + objects.get(j+i).orient.getZ() + "," + objects.get(j+i).orient.getW()
    			+ ";" + objects.get(j+i).state + ";" + objects.get(j+i).health + ";" + objects.get(j+i).maxHealth + ";" + objects.get(j+i).complete;
    			props.put("object_" + j, actionString);
    			Log.debug("CLAIM: Sending objectString: " + actionString);
    			playersLastObjectIDSent.put(playerOid, objects.get(j+i).id);
    			numObjects++;
    		}
    		Log.debug("CLAIM: Sending objects count: " + numObjects);
    		props.put("numObjects", numObjects);
    		TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    			Engine.getAgent().sendBroadcast(msg);
    		// New code - only send the one chunk
        	break;
    	}
    }
    
    /**
     * Starts an Attack task to deal damage to the specified claim building object.
     * @param playerOid
     * @param objectID
     */
    public void attackBuildObject(OID playerOid, int objectID) {
    	ClaimObject cObject = getClaimObject(objectID);
    	if (cObject == null)
    		return;
    	
    	// Check if the building is complete
    	if (!cObject.complete)
    		return;
    	
    	BuildObjectTemplate tmpl = VoxelClient.getBuildingTemplate(cObject.templateId);
    	if (!isCloseEnough(playerOid, tmpl, loc)) {
    		ExtendedCombatMessages.sendErrorMessage(playerOid, "You are too far away from the object to attack it");
    		return;
    	}
    	
    	// Start the attack task
    	ClaimTask task = new ClaimTask();
		task.StartAttackTask(cObject, playerOid, this);
    	tasks.add(task);
    	
    	// Schedule the task completion
    	Engine.getExecutor().schedule(task, 1000, TimeUnit.MILLISECONDS);
    	task.sendStartAttackTask(1);
    }
    
    /**
     * Damages a build object when an attack ClaimTask is completed. Downgrades
     * the building or removes it (if it is stage 1) when the health is below 0.
     * @param task
     */
    private void damageBuildObject(ClaimTask task) {
    	Log.debug("BUILD: dealing damage to build object");
    	// Get damage amount from player
    	int damage = 8;
    	// Subtract health from claim object. If the health goes below 0 then
    	// down-grade to previous 
    	task.cObject.health -= damage;
    	if (task.cObject.health < 0) {
    		Log.debug("BUILD: objects health is less than 1, downgrade");
    		
    		BuildObjectTemplate template = VoxelClient.getBuildingTemplate(task.cObject.templateId);
    		if (template == null) {
    			Log.error("BUILD: no template found for id: " + task.cObject.templateId + ". Object cannot be attacked.");
    			return;
    		}
    		
    		if (task.cObject.stage < 1) {
    			Log.debug("BUILD: hit stage 0 for " + template.id);
    			// This is a first stage object - set health to 0
    			task.cObject.health = 0;
    			//removeClaimObject(task.cObject.id);
    		} else {
    			// Loop through subtracting the health left until we either hit the first object
    			// or the health is above 0
    			Log.debug("BUILD: looping through prereqs");
    			// Set the health to the objects current health. It will be less than 1
    			int healthLeft = task.cObject.health;
    			task.cObject.stage--;
    			while (healthLeft < 1) {
        			// Add the health of the previous object and see if it gets above 0
        			healthLeft += template.getStage(task.cObject.stage).getHealth();
        			if (healthLeft < 1 && task.cObject.stage < 1) {
        				// Remove object
        				Log.debug("BUILD: hit stage 0 for " + template.id);
        				//removeClaimObject(task.cObject.id);
        				//return;
        				healthLeft = 0;
        				break;
        			} else if (healthLeft < 1) {
        				Log.debug("BUILD: health is still below 0 and there is still a prereq, downgrade again");
        				task.cObject.stage--;
        			}
    			}
    			
    			Log.debug("BUILD: changing object " + task.cObject.templateId + " down to stage: " + task.cObject.stage);
    			task.cObject.gameObject = template.getStage(task.cObject.stage).getGameObject();
    			task.cObject.health = healthLeft;
    			task.cObject.maxHealth = template.getStage(task.cObject.stage).getHealth();
    	    	task.cObject.itemReqs = template.getStage(task.cObject.stage).getItemReqs();
    				
    			// Send down the model change
    			sendRemoveObject(task.cObject);
    			sendObject(task.cObject);
    		}
    	}
    	
    	// Alter object
		Log.debug("BUILD: about to update state on the database after attack");
		// Save the updated state, remove the item from the players inventory and send the info down to the player
		cDB.updateClaimObjectState(task.cObject.id, task.cObject.templateId, task.cObject.stage, task.cObject.complete, 
				task.cObject.state, task.cObject.gameObject, task.cObject.health, task.cObject.maxHealth, task.cObject.itemReqs);
		sendObjectInfo(task.playerOid, task.cObject.id);
    }
    
    public void alterResource(OID playerOID, int itemID, int count) {
    	if (count > 0) {
    		if (resources.containsKey(itemID)) {
    			resources.get(itemID).count += count;
    			cDB.updateClaimResource(resources.get(itemID).id, itemID, resources.get(itemID).count);
    		} else {
    			ClaimResource resource = new ClaimResource();
    			resource.itemID = itemID;
    			resource.count = count;
    			resource.id = cDB.writeClaimResource(id, itemID, count);
    			resources.put(itemID, resource);
    		}
    	} else if (count < 0) {
    		if (resources.containsKey(itemID)) {
    			resources.get(itemID).count -= count;
    			if (resources.get(itemID).count < 0)
    				resources.get(itemID).count = 0;
    			cDB.updateClaimResource(resources.get(itemID).id, itemID, resources.get(itemID).count);
    		}
    	}
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "claim_resource_update");
		props.put("claimID", id);
		props.put("resource", itemID);
		props.put("resourceCount", resources.get(itemID).count);
		
		TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOID, 
				playerOID, false, props);
			Engine.getAgent().sendBroadcast(msg);
    }
    
    

	public int getID() { return id;}
    public void setID(int id) {
    	this.id = id;
    }
    
    public String getName() { return name;}
    public void setName(String name) {
    	this.name = name;
    }
    
    public int getInstanceID() { return instanceID;}
    public void setInstanceID(int instanceID) {
    	this.instanceID = instanceID;
    }
    
    public AOVector getLoc() { return loc;}
    public void setLoc(AOVector loc) {
    	this.loc = loc;
    }
    
    public int getSize() { return size;}
    public void setSize(int size) {
    	this.size = size;
    }
    
    public HashMap<String, Serializable> getProps() { return props;}
    public void setProps(HashMap<String, Serializable> props) {
    	this.props = props;
    }
    
    public OID getInstanceOID() { return instanceOID;}
    public void setInstanceOID(OID instanceOID) {
    	this.instanceOID = instanceOID;
    }
    
    public OID getObjectOID() { return objectOID;}
    public void setObjectOID(OID objectOID) {
    	this.objectOID = objectOID;
    }
    
    public OID getOwner() { return owner;}
    public void setOwner(OID owner) {
    	this.owner = owner;
    }
    
    public boolean getForSale() { return forSale;}
    public void setForSale(boolean forSale) {
    	this.forSale = forSale;
    }
    
    public int getCost() { return cost;}
    public void setCost(int cost) {
    	this.cost = cost;
    }
    
    public int getCurrency() { return currency;}
    public void setCurrency(int currency) {
    	this.currency = currency;
    }
    
    public String getSellerName() { return sellerName;}
    public void setSellerName(String sellerName) {
    	this.sellerName = sellerName;
    }
    
    public int getClaimItemTemplate() { return claimItemTemplate;}
    public void setClaimItemTemplate(int claimItemTemplate) {
    	this.claimItemTemplate = claimItemTemplate;
    }
    
    public int getPriority() { return priority;}
    public void setPriority(int priority) {
    	this.priority = priority;
    }
    
    public String getData() { return data;}
    public void setData(String data) {
    	this.data = data;
    }

    public boolean getActive() { return active;}
    public void setActive(boolean active) {
    	this.active = active;
    }
    
    public void setContentDatabase(ContentDatabase cDB) {
    	this.cDB = cDB;
    }

    int id;
    String name;
    int instanceID;
    AOVector loc;
    int size;
    DisplayContext dc;
    OID instanceOID;
    OID objectOID;
    OID owner;
    boolean forSale = false;
    int cost = 0;
    int currency;
    String sellerName = "";
    int claimItemTemplate = -1;
    int priority = 1;
    String data;
    HashMap<String, Serializable> props;
    LinkedList<ClaimAction> actions = new LinkedList<ClaimAction>();
    LinkedList<ClaimObject> objects = new LinkedList<ClaimObject>();
    HashMap<Integer, ClaimResource> resources = new HashMap<Integer, ClaimResource>();
    HashMap<OID, ClaimPermission> permissions = new HashMap<OID, ClaimPermission>();
    LinkedList<OID> playersInRange = new LinkedList<OID>();
    HashMap<OID, Integer> playersLastIDSent = new HashMap<OID, Integer>();
    HashMap<OID, Integer> playersLastObjectIDSent = new HashMap<OID, Integer>();
    LinkedList<ClaimTask> tasks = new LinkedList<ClaimTask>();
    boolean active;
    Long eventSub = null;
    ContentDatabase cDB;
    
    ClaimEntity claimEntity;
    
    // The name of the prefab found in the Resources folder in Unity for the claim. 
    // This is a terrible way to do it, needs changed.
    String model = "ClaimCube";
    
    public static final int CLAIM_DRAW_RADIUS = 300; //75
    public static final int CLAIM_EDIT_RADIUS = 30;
    private static final long serialVersionUID = 1L;
    
    enum TaskType {
		BUILD,
		UPGRADE,
		ATTACK,
		REPAIR
	}
    
    /**
     * Should probably be a struct
     * @author Andrew
     *
     */
    class ClaimAction {
    	public int id;
    	public String action;
    	public String brushType;
    	public AOVector size;
    	public AOVector loc;
    	public AOVector normal;
    	public int mat;
    }
    
    class ClaimObject {
    	public int id;
    	public int templateId;
    	public String gameObject;
    	public AOVector loc;
    	public Quaternion orient;
    	public int stage;
    	public boolean complete;
    	public int itemID;
    	public String state;
    	public int health;
    	public int maxHealth;
    	public HashMap<Integer, Integer> itemReqs;
    }
    
    class ClaimResource {
    	public int id;
    	public int itemID;
    	public int count;
    }
    
    class ClaimPermission {
    	public OID playerOid;
    	public String playerName;
    	public int permissionLevel;
    }
    
    
    /**
     * A Runnable class that adds an object to the claim when it is run. 
     * @author Andrew Harrison
     *
     */
    public class ClaimTask implements Runnable {
    	
    	protected BuildObjectTemplate template;
    	protected ClaimObject cObject;
    	protected TaskType taskType;
    	protected AOVector loc;
    	protected Quaternion orient;
    	protected int itemID;
    	protected OID itemOid;
    	protected OID playerOid;
    	protected Claim claim;
    	protected boolean interrupted;
    	
    	public ClaimTask() {
    		
    	}
    	
    	public void StartBuildTask(BuildObjectTemplate template, AOVector loc, Quaternion orient,
    			int itemID, OID itemOid, OID playerOid, Claim claim) {
    		Log.debug("BUILD: creating new build claim task");
    		this.template = template;
    		this.taskType = TaskType.BUILD;
    		this.loc = loc;
    		this.orient = orient;
    		this.itemID = itemID;
    		this.itemOid = itemOid;
    		this.playerOid = playerOid;
    		this.claim = claim;
    	}
    	
    	public void StartUpgradeTask(BuildObjectTemplate template, ClaimObject cObject, int itemID, OID itemOid, OID playerOid, Claim claim) {
    		Log.debug("BUILD: creating new upgrade claim task");
    		this.template = template;
    		this.cObject = cObject;
    		this.taskType = TaskType.UPGRADE;
    		this.itemID = itemID;
    		this.itemOid = itemOid;
    		this.playerOid = playerOid;
    		this.claim = claim;
    	}
    	
    	public void StartAttackTask(ClaimObject cObject, OID playerOid, Claim claim) {
    		Log.debug("BUILD: creating new attack claim task");
    		this.cObject = cObject;
    		this.taskType = TaskType.ATTACK;
    		this.playerOid = playerOid;
    		this.claim = claim;
    	}
    	
    	public void StartRepairTask(BuildObjectTemplate template, ClaimObject cObject, int itemID, OID itemOid, OID playerOid, Claim claim) {
    		Log.debug("BUILD: creating new repair claim task");
    		this.template = template;
    		this.cObject = cObject;
    		this.taskType = TaskType.REPAIR;
    		this.itemID = itemID;
    		this.itemOid = itemOid;
    		this.playerOid = playerOid;
    		this.claim = claim;
    	}
    	
    	public void sendStartBuildTask(float length) {
    		Log.debug("BUILD: sending start build task");
    		Map<String, Serializable> props = new HashMap<String, Serializable>();
        	props.put("ext_msg_subtype", "start_build_task");
    		props.put("claimID", id);
    		props.put("id", template.id);
    		props.put("length", length);
        	TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    		Engine.getAgent().sendBroadcast(msg);
    		
    		// Also send down a coord effect here
        	CoordinatedEffect cE = new CoordinatedEffect("StandardBuilding");
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.putArgument("length", length);
    	    cE.invoke(playerOid, playerOid);
    		//EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, AgisWorldManagerPlugin.PROP_ACTION_STATE, "Building");
    	}
    	
    	public void sendStartAttackTask(float length) {
    		CoordinatedEffect cE = new CoordinatedEffect("AttackBuilding");
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.putArgument("length", length);
    	    cE.invoke(playerOid, playerOid);
    	}
    	
		@Override
		public void run() {
			// Check user still has items
			Log.debug("BUILD: running task");
			if (interrupted) {
				Log.debug("BUILD: task was interrupted, not completing run");
				claim.tasks.remove(this);
				return;
			}
			
			EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, AgisWorldManagerPlugin.PROP_ACTION_STATE, "");
			
	    	// Add object to the claim
	    	if (taskType == TaskType.BUILD) {
	    		runNewBuild();
	    	} else if (taskType == TaskType.UPGRADE) {
	    		runUpgrade();
	    	} else if (taskType == TaskType.ATTACK) {
	    		runAttack();
	    	} else if (taskType == TaskType.REPAIR) {
	    		runRepair();
	    	}
	    	claim.tasks.remove(this);
		}
		
		void runNewBuild() {
			// Remove item from players inventory
			if (itemOid != null) {
				AgisInventoryClient.removeSpecificItem(playerOid, itemOid, false, 1);
			}
						
			LinkedList<Integer> components = new LinkedList<Integer>();
			LinkedList<Integer> componentCounts = new LinkedList<Integer>();
			
			for (int itemReq : template.getStage(0).getItemReqs().keySet()) {
				components.add(itemReq);
				componentCounts.add(template.getStage(0).getItemReqs().get(itemReq));
			}
			
			boolean hasItems = AgisInventoryClient.checkComponents(playerOid, components, componentCounts);
			if (!hasItems) {
				ExtendedCombatMessages.sendErrorMessage(playerOid, "You do not have the required items to build this object");
				return;
			}
			
			Log.debug("BUILD: getting player skill for new build");
			int playerSkillLevel = -1;
	    	if (template.skill > 0)
	    		playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, template.skill);
			// Do skillup chance
	    	if (template.skill > 0) {
				Log.debug("BUILD: checking skill: " + template.skill + " against playerSkillLevel: " + playerSkillLevel);
				//if (playerSkillLevel < skillLevelMax) {
					CombatClient.abilityUsed(playerOid, template.skill);
				//} else if (CraftingPlugin.GAIN_SKILL_AFTER_MAX && rand.nextInt(4) == 0) {
				//	CombatClient.abilityUsed(playerOid, skill);
				//}
	    	}
	    	
			claim.addClaimObject(this);
		}
		
		void runUpgrade() {
			Log.debug("BUILD: getting player skill for upgrade");
			int playerSkillLevel = 1;
	    	if (template.skill > 0)
	    		playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, template.skill);
	    	
	    	Log.debug("BUILD: checking success");
			int rollMax = 100;
    		int skillLevelCalc = ((playerSkillLevel / 4) + 45);
    		Random rand = new Random();
    		if (skillLevelCalc < rand.nextInt(rollMax)) {
    			ExtendedCombatMessages.sendErrorMessage(playerOid, "You failed to improve the claim object");
    			
    			if (VoxelPlugin.REMOVE_ITEM_ON_BUILD_FAIL) {
    				// Remove item from players inventory
    				if (itemOid != null) {
    					AgisInventoryClient.removeSpecificItem(playerOid, itemOid, false, 1);
    				}
    			}
				return;
    		} else {
    			// Remove item from players inventory
    			if (itemOid != null) {
    				AgisInventoryClient.removeSpecificItem(playerOid, itemOid, false, 1);
    			}
    		}
    		
			// Do skillup chance
	    	if (template.skill > 0) {
				Log.debug("BUILD: checking skill: " + template.skill + " against playerSkillLevel: " + playerSkillLevel);
				//if (playerSkillLevel < skillLevelMax) {
					CombatClient.abilityUsed(playerOid, template.skill);
				//} else if (CraftingPlugin.GAIN_SKILL_AFTER_MAX && rand.nextInt(4) == 0) {
				//	CombatClient.abilityUsed(playerOid, skill);
				//}
	    	}
	    	
			claim.upgradeClaimObject(this);
		}
		
		void runAttack() {
			claim.damageBuildObject(this);
		}
		
		void runRepair() {
			Log.debug("BUILD: getting player skill for repair");
			int playerSkillLevel = 1;
	    	if (template.skill > 0)
	    		playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, template.skill);
	    	
	    	Log.debug("BUILD: checking success");
			int rollMax = 100;
    		int skillLevelCalc = ((playerSkillLevel / 4) + 45);
    		Random rand = new Random();
    		if (skillLevelCalc < rand.nextInt(rollMax)) {
    			ExtendedCombatMessages.sendErrorMessage(playerOid, "You failed to repair the claim object");
    			if (VoxelPlugin.REMOVE_ITEM_ON_BUILD_FAIL) {
    				// Remove item from players inventory
    				if (itemOid != null) {
    					AgisInventoryClient.removeSpecificItem(playerOid, itemOid, false, 1);
    				}
    			}
				return;
    		} else {
    			// Remove item from players inventory
    			if (itemOid != null) {
    				AgisInventoryClient.removeSpecificItem(playerOid, itemOid, false, 1);
    			}
    		}
    		
			// Do skillup chance
	    	if (template.skill > 0) {
				Log.debug("BUILD: checking skill: " + template.skill + " against playerSkillLevel: " + playerSkillLevel);
				//if (playerSkillLevel < skillLevelMax) {
					CombatClient.abilityUsed(playerOid, template.skill);
				//} else if (CraftingPlugin.GAIN_SKILL_AFTER_MAX && rand.nextInt(4) == 0) {
				//	CombatClient.abilityUsed(playerOid, skill);
				//}
	    	}
	    	
			claim.repairClaimObject(this);
		}
		
		public void interrupt() {
			interrupted = true;
			Map<String, Serializable> props = new HashMap<String, Serializable>();
        	props.put("ext_msg_subtype", "build_task_interrupted");
    		props.put("claimID", id); 
    		props.put("id", template.id);
        	TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    		Engine.getAgent().sendBroadcast(msg);
    		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, AgisWorldManagerPlugin.PROP_ACTION_STATE, "");
		}
    }

    /**
     * Sub-class needed for the interpolated world node so a perceiver can be created.
     * @author Andrew
     *
     */
	public class ClaimEntity extends Entity implements EntityWithWorldNode
	{

		public ClaimEntity(OID oid, InterpolatedWorldNode node) {
	    	setWorldNode(node);
	    	setOid(oid);
	    }
		
		public InterpolatedWorldNode getWorldNode() { return node; }
	    public void setWorldNode(InterpolatedWorldNode node) { this.node = node; }
	    InterpolatedWorldNode node;

		@Override
		public void setDirLocOrient(BasicWorldNode bnode) {
			if (node != null)
	            node.setDirLocOrient(bnode);
		}

		@Override
		public Entity getEntity() {
			return (Entity)this;
		}
		
		private static final long serialVersionUID = 1L;
	}
	
	public static final int PERMISSION_ADD_ONLY = 1;
	public static final int PERMISSION_ADD_DELETE = 2;
	public static final int PERMISSION_ADD_USERS = 3;
	public static final int PERMISSION_MANAGE_USERS = 4;
	public static final int PERMISSION_OWNER = 5;
}
