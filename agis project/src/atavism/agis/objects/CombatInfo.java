package atavism.agis.objects;

import atavism.agis.core.AgisAbilityState;
import atavism.agis.core.AgisEffect.EffectState;
import atavism.agis.core.Cooldown;
import atavism.agis.plugins.AgisMobClient;
import atavism.agis.plugins.CombatPlugin;
import atavism.server.engine.*;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.Entity;
import atavism.server.objects.EntityManager;
import atavism.server.objects.ObjectType;
import atavism.server.objects.ObjectTypes;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedPropertyMessage;
import atavism.server.util.Log;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;


/**
 * Information related to the combat system. Any object that wants to be involved
 * in combat needs one of these.
 */
public class CombatInfo extends Entity implements Runnable, Cooldown.CooldownObject {
	public CombatInfo() {
		super();
		setNamespace(Namespace.COMBAT);
	}

	public CombatInfo(OID objOid, int id) {
		super(objOid);
		setNamespace(Namespace.COMBAT);
		setState("");
		this.id = id;
	}

    public String toString() {
        return "[Entity: " + getName() + ":" + getOid() + "]";
    }

    public ObjectType getType() {
        return ObjectTypes.combatInfo;
    }
    
    public void overrideAutoAttackAbility(int abilityID) {
    	//Log.debug("AUTO: setting auto attack ability to: " + abilityID);
    	setProperty(COMBAT_PROP_AUTOATTACK_ABILITY, abilityID);
    }
    
    public void resetAutoAttackAbility() {
    	//Log.debug("AUTO: setting auto attack back to: " + getProperty(COMBAT_PROP_AUTOATTACK_BASE));
    	setProperty(COMBAT_PROP_AUTOATTACK_ABILITY, getProperty(COMBAT_PROP_AUTOATTACK_BASE));
    }
    
    public int getAutoAttackAbility() {
    	return (Integer)getProperty(CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY);
    }

    public void setAutoAttack(OID newTarget) {
		lock.lock();
		try {
			Log.debug("AUTO: getting old target");
			OID oldTarget = target;
			
			if (oldTarget != null && oldTarget.equals(newTarget)) {
				Log.debug("AUTO: old target = new target");
				return;
			}
			target = newTarget;
			Log.debug("AUTO: got new target: " + target);
			if (oldTarget == null) {
				setCombatState(true);
				if (!scheduled) {
					Log.debug("AUTO: scheduling auto attack for new target");
					schedule(/*getAttackDelay()*/500);
				}
			}
			else {
				CombatPlugin.removeAttacker(oldTarget, getOwnerOid());
			}
			if (target == null) {
				setCombatState(false);
				Log.debug("AUTO: new target is null");
			}
			else {
				CombatPlugin.addAttacker(target, getOwnerOid());
				//if (!recovering) {
				//	CombatPlugin.resolveAutoAttack(this);
				//}
			}
			Log.debug("AUTO: finished set auto attack");
		}
		finally {
			lock.unlock();
		}
	}
    public void stopAutoAttack() {
        lock.lock();
        try {
            if (target != null) {
                CombatPlugin.removeAttacker(target, getOwnerOid());
            }
            setCombatState(false);
            target = null;
            Long petOwner = (Long) getProperty("petOwner");
            if (petOwner != null) {
            	AgisMobClient.petTargetLost(getOid());
            }
        }
        finally {
            lock.unlock();
        }
    }
    public OID getAutoAttackTarget() {
		lock.lock();
		try {
			return target;
		}
		finally {
			lock.unlock();
		}
	}
	protected OID target = null;
	boolean scheduled = false;

    public long getAttackDelay() {
    	Log.debug("AUTO: Getting attack delay");
        return statGetCurrentValue(CombatPlugin.ATTACK_SPEED_STAT);
    }

    protected void schedule(long delay) {
		if (Log.loggingDebug)
			Log.debug("CombatInfo.schedule: scheduling obj=" + this + " for delay=" + delay);
		Engine.getExecutor().schedule(this, delay, TimeUnit.MILLISECONDS);
		scheduled = true;
	}
	protected void cancel() {
		Engine.getExecutor().remove(this);
		scheduled = false;
	}

	public void run() {
		Lock targetLock = null;
		lock.lock();
		try {
			Log.debug("AUTO: combat info - run");
			int abilityID = (Integer)getProperty(CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY);
			if (target == null) {
				scheduled = false;
			}
			else if (abilityID > 0 && getAttackDelay() > 0) {
				// Try get entity
				Entity e = EntityManager.getEntityByNamespace(target, Namespace.COMBAT);
				if (e == null) {
					target = null;
				} else {
					targetLock = e.getLock();
					if (targetLock != null) {
						while (!targetLock.tryLock()) {
							lock.unlock();
							Thread.yield();
							Log.debug("AUTO: yielding in run");
							lock.lock();
						}
					}
					Log.debug("AUTO: About to resolve auto attack");
					CombatPlugin.resolveAutoAttack(this);
					
					Log.debug("AUTO: About to schedule next auto attack");
					schedule(getAttackDelay());
					Log.debug("AUTO: scheduled next auto attack");
				}
				
			}
		}
		catch (Exception e) {
			Log.exception("CombatInfo.run: got exception", e);
			Log.debug("CombatInfo.run: got exception: " + e);
		}
		finally {
			if (targetLock != null) {
				targetLock.unlock();
			}
			lock.unlock();
			Log.debug("AUTO: finished combat info - run");
		}
	}
	
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	private int id;
	
	/**
	 * Category control
	 * Each category has its own collection of quests.
	 */
	public int getCurrentCategory() {
		return currentCategory;
	}
	public void setCurrentCategory(int category) {
		Log.debug("QSI: setting current category to: " + category + " from: " + currentCategory);
		this.currentCategory = category;
	}
	public boolean categoryUpdated(int category) {
		boolean createSkills = false;
		if (!skills.containsKey(category)) {
			createSkills = true;
		}
		this.currentCategory = category;
		Engine.getPersistenceManager().setDirty(this);
		return createSkills;
	}
	protected int currentCategory;
	
	public void addSkill(int skillID) {
		
	}
	
	public SkillInfo getCurrentSkillInfo() {
        lock.lock();
        try {
            return getSkillInfo(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void setCurrentSkillInfo(SkillInfo skills) {
        lock.lock();
        try {
            this.skills.put(currentCategory, skills);
            //ExtendedCombatMessages.sendSkills(getOwnerOid(), skills);
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, SkillInfo> getSkillInfos() {
        lock.lock();
        try {
            return new HashMap<Integer, SkillInfo>(skills);
        } finally {
            lock.unlock();
        }
    }
	public void setSkillInfos(HashMap<Integer, SkillInfo> skills) {
        lock.lock();
        try {
            this.skills = new HashMap<Integer, SkillInfo>(skills);
        } finally {
            lock.unlock();
        }
	}
	public SkillInfo getSkillInfo(int category) {
        lock.lock();
        try {
        	if (!skills.containsKey(category))
        		skills.put(category, new SkillInfo(category));
            return skills.get(category);
        } finally {
            lock.unlock();
        }
    }
	protected HashMap<Integer, SkillInfo> skills = new HashMap<Integer, SkillInfo>();
	
	public ArrayList<Integer> getCurrentAbilities() {
        lock.lock();
        try {
            return getAbilities(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void setCurrentAbilities(ArrayList<Integer> abilities) {
        lock.lock();
        try {
            this.abilities.put(currentCategory, abilities);
            //ExtendedCombatMessages.sendAbilities(getOwnerOid(), abilities);
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, ArrayList<Integer>> getAbilities() {
        lock.lock();
        try {
            return new HashMap<Integer, ArrayList<Integer>>(abilities);
        } finally {
            lock.unlock();
        }
    }
	public void setAbilities(HashMap<Integer, ArrayList<Integer>> abilities) {
        lock.lock();
        try {
            this.abilities = new HashMap<Integer, ArrayList<Integer>>(abilities);
        } finally {
            lock.unlock();
        }
	}
	public ArrayList<Integer> getAbilities(int category) {
        lock.lock();
        try {
        	if (!abilities.containsKey(category))
        		abilities.put(category, new ArrayList<Integer>());
            return abilities.get(category);
        } finally {
            lock.unlock();
        }
    }
	protected HashMap<Integer, ArrayList<Integer>> abilities = new HashMap<Integer, ArrayList<Integer>>();
	
	public void addAction(int abilityNum) {
    	ArrayList<String> currentActions = getCurrentActions();
		for (int i = 0; i < 10; i++) {
			if (currentActions.get(i).equals("")) {
				Log.debug("SKILL: adding new action to spot " + i);
				currentActions.set(i, "a" + abilityNum);
				return;
			}
		}
    }
    
    public void removeAction(int abilityNum) {
    	ArrayList<String> currentActions = getCurrentActions();
		for (int i = 0; i < currentActions.size(); i++) {
			if (currentActions.get(i).equals("a" + abilityNum)) {
				Log.debug("SKILL: removing action at spot " + i);
				currentActions.set(i, "");
				return;
			}
		}
    }
	public ArrayList<String> getCurrentActions() {
        lock.lock();
        try {
            return getActions(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void setCurrentActions(ArrayList<String> actions) {
        lock.lock();
        try {
            this.actions.put(currentCategory, actions);
            //ExtendedCombatMessages.sendActions(getOwnerOid(), actions);
        } finally {
            lock.unlock();
        }
        Engine.getPersistenceManager().setDirty(this);
	}
	public HashMap<Integer, ArrayList<String>> getActions() {
        lock.lock();
        try {
            return new HashMap<Integer, ArrayList<String>>(actions);
        } finally {
            lock.unlock();
        }
    }
	public void setActions(HashMap<Integer, ArrayList<String>> actions) {
        lock.lock();
        try {
            this.actions = new HashMap<Integer, ArrayList<String>>(actions);
        } finally {
            lock.unlock();
        }
        Engine.getPersistenceManager().setDirty(this);
	}
	public ArrayList<String> getActions(int category) {
        lock.lock();
        try {
        	if (!actions.containsKey(category))
        		actions.put(category, new ArrayList<String>());
            return actions.get(category);
        } finally {
            lock.unlock();
        }
    }
	protected HashMap<Integer, ArrayList<String>> actions = new HashMap<Integer, ArrayList<String>>();

	public void addCooldownState(Cooldown.State state) {
        lock.lock();
        try {
            cooldownMap.put(state.getID(), state);
        } finally {
            lock.unlock();
        }
    }
	public void removeCooldownState(Cooldown.State state) {
		lock.lock();
		try {
			cooldownMap.remove(state.getID());
		}
		finally {
			lock.unlock();
		}
	}
	public Cooldown.State getCooldownState(String id) {
        lock.lock();
        try {
            return cooldownMap.get(id);
        } finally {
            lock.unlock();
        }
    }
	public Map<String, Cooldown.State> getCooldownMap() { return cooldownMap; }
	protected Map<String, Cooldown.State> cooldownMap = new HashMap<String, Cooldown.State>();

	public void setCurrentAction(AgisAbilityState action) { currentAction = action; }
	public AgisAbilityState getCurrentAction() { return currentAction; }
	protected transient AgisAbilityState currentAction;


	public void addActiveAbility(AgisAbilityState abilityState) {
        lock.lock();
        try {
            activeAbilities.add(abilityState);
        } finally {
            lock.unlock();
        }
	}
	public void removeActiveAbility(AgisAbilityState abilityState) {
        lock.lock();
        try {
            activeAbilities.remove(abilityState);
        } finally {
            lock.unlock();
        }
	}
	protected transient Set<AgisAbilityState> activeAbilities;

	public Set<EffectState> getCurrentEffects() {
        lock.lock();
        try {
            return getEffects(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void addEffect(EffectState effectState) {
        lock.lock();
        try {
        	getEffects(currentCategory).add(effectState);
        } finally {
            lock.unlock();
        }
        //addEffectsProperty(effectState.getEffect().getID());
        updateEffectsProperty();
    }
	public void removeEffect(EffectState effectState) {
        lock.lock();
        try {
        	HashSet<EffectState> currentEffects = (HashSet<EffectState>) getEffects(currentCategory);
        	currentEffects.remove(effectState);
        	effects.put(currentCategory, currentEffects);
        } finally {
            lock.unlock();
        }
        //removeEffectsProperty(effectState.getEffectID());
        updateEffectsProperty();
    }
	public HashMap<Integer, HashSet<EffectState>> getEffects() {
        lock.lock();
        try {
            return new HashMap<Integer, HashSet<EffectState>>(effects);
        } finally {
            lock.unlock();
        }
    }
	public void setEffects(HashMap<Integer, HashSet<EffectState>> effects) {
        lock.lock();
        try {
            this.effects = new HashMap<Integer, HashSet<EffectState>>(effects);
        } finally {
            lock.unlock();
        }
    }
	public Set<EffectState> getEffects(int category) {
        lock.lock();
        try {
        	if (!effects.containsKey(category))
        		effects.put(category, new HashSet<EffectState>());
            return effects.get(category);
        } finally {
            lock.unlock();
        }
    }
	protected HashMap<Integer, HashSet<EffectState>> effects = new HashMap<Integer, HashSet<EffectState>>();
	
	/* 
	 * Target tracking
	 */
	public void addFriendlyTarget(OID oid, TargetInfo info) {
        lock.lock();
        try {
        	friendlyTargets.put(oid, info);
        } finally {
            lock.unlock();
        }
    }
	public void removeFriendlyTarget(OID oid) {
        lock.lock();
        try {
        	friendlyTargets.remove(oid);
        } finally {
            lock.unlock();
        }
    }
	public HashMap<OID, TargetInfo> getFriendlyTargets() {
        return friendlyTargets;
    }
	public void setFriendlyTargets(HashMap<OID, TargetInfo> targets) {
        this.friendlyTargets = targets;
    }
	
	public void addAttackableTarget(OID oid, TargetInfo info) {
        lock.lock();
        try {
        	attackableTargets.put(oid, info);
        } finally {
            lock.unlock();
        }
    }
	public void removeAttackableTarget(OID oid) {
        lock.lock();
        try {
        	attackableTargets.remove(oid);
        } finally {
            lock.unlock();
        }
    }
	public HashMap<OID, TargetInfo> getAttackableTargets() {
        return new HashMap<OID, TargetInfo>(attackableTargets);
    }
	public void setAttackableTargets(HashMap<OID, TargetInfo> targets) {
        this.attackableTargets = new HashMap<OID, TargetInfo>(targets);
    }
	
	// Lists containing the mobs/players that are currently either healing/buffable
	// and attackable. These are used by AgisAbility to determine if a target is
	// acceptable for the ability in question.
	protected HashMap<OID, TargetInfo> friendlyTargets = new HashMap<OID, TargetInfo>();
	protected HashMap<OID, TargetInfo> attackableTargets = new HashMap<OID, TargetInfo>();
	
	public void addTargetInCombat(OID oid) {
        lock.lock();
        try {
        	targetsInCombat.add(oid);
        } finally {
            lock.unlock();
        }
    }
	public void removeTargetInCombat(OID oid) {
        lock.lock();
        try {
        	targetsInCombat.remove(oid);
        } finally {
            lock.unlock();
        }
    }
	public ArrayList<OID> getTargetsInCombat() {
        return new ArrayList<OID>(targetsInCombat);
    }
	public void setTargetsInCombat(ArrayList<OID> targetsInCombat) {
        this.targetsInCombat = new ArrayList<OID>(targetsInCombat);
    }
	protected ArrayList<OID> targetsInCombat = new ArrayList<OID>();
	
	/**
	 * Does the CombatInfo belong to a players character.
	 * @return
	 */
	public boolean isUser() { return getBooleanProperty(COMBAT_PROP_USERFLAG); }
	public boolean isMob() { return getBooleanProperty(COMBAT_PROP_MOBFLAG); }
	public boolean attackable() { return getBooleanProperty(COMBAT_PROP_ATTACKABLE); }
	public boolean dead() { return getBooleanProperty(COMBAT_PROP_DEADSTATE); }
	public int aspect() { return getIntProperty(COMBAT_PROP_ASPECT); }
	public String team() { return getStringProperty(COMBAT_PROP_TEAM); }
	public void team(String newTeam) {
		setProperty(COMBAT_PROP_TEAM, newTeam);
		PropertyMessage propMsg = new PropertyMessage(getOwnerOid());
		propMsg.setProperty(COMBAT_PROP_TEAM, newTeam);
		Engine.getAgent().sendBroadcast(propMsg);
	}
	public OID getOwnerOid() { return getOid(); }
    public boolean inCombat() { return getBooleanProperty(COMBAT_PROP_COMBATSTATE); }

    public void setCombatState(boolean state) {
        setProperty(COMBAT_PROP_COMBATSTATE, new Boolean(state));
        PropertyMessage propMsg = new PropertyMessage(getOwnerOid());
        propMsg.setProperty(COMBAT_PROP_COMBATSTATE, new Boolean(state));
        Engine.getAgent().sendBroadcast(propMsg);
        if (state == false) {
        	if (!dead()) {
        		setProperty(COMBAT_TAG_OWNER, null);
        	}
        	EnginePlugin.setObjectPropertyNoResponse(getOwnerOid(), WorldManagerClient.NAMESPACE, "weaponsSheathed", true);
        } else {
        	EnginePlugin.setObjectPropertyNoResponse(getOwnerOid(), WorldManagerClient.NAMESPACE, "weaponsSheathed", false);
        }
    }

    public void setDeadState(boolean state) {
        setProperty(COMBAT_PROP_DEADSTATE, new Boolean(state));
        PropertyMessage propMsg = new PropertyMessage(getOwnerOid());
        propMsg.setProperty(COMBAT_PROP_DEADSTATE, new Boolean(state));
        Engine.getPersistenceManager().setDirty(this);
        Engine.getAgent().sendBroadcast(propMsg);
    }
    
    public String getState() {
    	return getStringProperty(COMBAT_PROP_STATE); 
    }
    
    public void setState(String state) {
    	setProperty(COMBAT_PROP_STATE, state);
    	PropertyMessage propMsg = new PropertyMessage(getOwnerOid());
        propMsg.setProperty(COMBAT_PROP_STATE, state);
        Engine.getPersistenceManager().setDirty(this);
        Engine.getAgent().sendBroadcast(propMsg);
    }
    
    public void clearState(String state) {
    	String currentState = (String) getProperty(COMBAT_PROP_STATE);
    	if (currentState != null && currentState.equals(state)) {
    		setProperty(COMBAT_PROP_STATE, "");
    		PropertyMessage propMsg = new PropertyMessage(getOwnerOid());
            propMsg.setProperty(COMBAT_PROP_STATE, "");
            Engine.getPersistenceManager().setDirty(this);
            Engine.getAgent().sendBroadcast(propMsg);
    	}
    }

    public void sendStatusUpdate() {
    }
    
    public void updateEffectsProperty() {
    	LinkedList<String> effectsProp = new LinkedList<String>();
    	for (EffectState eState : getEffects(currentCategory)) {
    		String effectData = eState.getEffectID() + "," + eState.getCurrentStack() + "," + eState.getEffect().isBuff() + "," + 
    				+ eState.getEndTime() + "," + eState.getTimeUntilEnd();
    		effectsProp.add(effectData);
    	}
    	
    	setProperty("effects", effectsProp);
    	PropertyMessage propMsg = new PropertyMessage(getOwnerOid());
        propMsg.setProperty("effects", effectsProp);
        Engine.getAgent().sendBroadcast(propMsg);
        Engine.getPersistenceManager().setDirty(this);
    }

    public InterpolatedWorldNode getWorldNode() { return node; }
    public void setWorldNode(InterpolatedWorldNode node) { this.node = node; }
    InterpolatedWorldNode node;

    /**
     * Modifies the base value of the specified stat. It is highly recommended that 
     * statAddModifier is used instead unless reversing the modification is not needed.
     * @param statName
     * @param delta
     */
    public void statModifyBaseValue(String statName, int delta) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			stat.modifyBaseValue(delta);
			AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
			statDef.update(stat, this);
			statSendUpdate(false);
			Engine.getPersistenceManager().setDirty(this);
		}
		finally {
			lock.unlock();
		}
	}

    /**
     * Sets the base value of the specified stat. It is highly recommended that statAddModifier
     * is used instead unless reversing the change is not needed.
     * @param statName
     * @param value
     */
    public void statSetBaseValue(String statName, int value) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			stat.setBaseValue(value);
			AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
			statDef.update(stat, this);
			statSendUpdate(false);
			Engine.getPersistenceManager().setDirty(this);
		}
		finally {
			lock.unlock();
		}
	}

	public void statSetMaxValue(String statName, int value) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
            stat.setMaxValue(value);
            AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
            statDef.update(stat, this);
            statSendUpdate(false);
            Engine.getPersistenceManager().setDirty(this);
		}
		finally {
			lock.unlock();
		}
	}

	/**
	 * Adds a stat modifier to the specified stat changing the value of the stat and recording the 
	 * change in a map so it can be removed at any time.
	 * @param statName
	 * @param id
	 * @param delta
	 */
	public void statAddModifier(String statName, Object id, int delta) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			if (stat == null) {
				Log.error("CombatInfo.statAddModifier: statName=" + statName + " does not exist for this=" + this);
			} else {
				stat.addModifier(id, delta);
				AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
				if (statDef == null) {
					Log.error("CombatInfo.statAddModifier: statName=" + statName + " is not registered with CombatPlugin.");
				} else {
					statDef.update(stat, this);
					statSendUpdate(false);
					Engine.getPersistenceManager().setDirty(this);
				}
			}
		}
		finally {
			lock.unlock();
		}
	}
	
	/**
	 * Adds a stat modifier to the specified stat changing the value of the stat by the given percent and recording the 
	 * change in a map so it can be removed at any time.
	 * @param statName
	 * @param id
	 * @param delta
	 */
	public void statAddPercentModifier(String statName, Object id, float percent) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			if (stat == null) {
				Log.error("CombatInfo.statAddPercentModifier: statName=" + statName + " does not exist for this=" + this);
			} else {
				stat.addPercentModifier(id, percent);
				AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
				if (statDef == null) {
					Log.error("CombatInfo.statAddModifier: statName=" + statName + " is not registered with CombatPlugin.");
				} else {
					statDef.update(stat, this);
					statSendUpdate(false);
					Engine.getPersistenceManager().setDirty(this);
				}
			}
		}
		finally {
			lock.unlock();
		}
	}

	public void statRemoveModifier(String statName, Object id) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			Log.debug("STAT: removing modifier with stat: " + statName + " and current value: " + stat.getCurrentValue());
			stat.removeModifier(id);
			AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
			statDef.update(stat, this);
			statSendUpdate(false);
			Engine.getPersistenceManager().setDirty(this);
			Log.debug("STAT: removed modifier from stat: " + statName + " and current value: " + stat.getCurrentValue());
		}
		finally {
			lock.unlock();
		}
	}
	
	public void statRemovePercentModifier(String statName, Object id) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			Log.debug("STAT: removing percent modifier with stat: " + statName + " and current value: " + stat.getCurrentValue());
			stat.removePercentModifier(id);
			AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
			statDef.update(stat, this);
			statSendUpdate(false);
			Engine.getPersistenceManager().setDirty(this);
			Log.debug("STAT: removed percent modifier from stat: " + statName + " and current value: " + stat.getCurrentValue());
		}
		finally {
			lock.unlock();
		}
	}
	
	public void statReapplyModifier(String statName, Object id, int delta) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			if (stat == null) {
				Log.error("CombatInfo.statReapplyModifier: statName=" + statName + " does not exist for this=" + this);
			} else {
				stat.removeModifier(id);
				stat.addModifier(id, delta);
				AgisStatDef statDef = CombatPlugin.lookupStatDef(statName);
				if (statDef == null) {
					Log.error("CombatInfo.statReapplyModifier: statName=" + statName + " is not registered with CombatPlugin.");
				} else {
					statDef.update(stat, this);
					statSendUpdate(false);
					Engine.getPersistenceManager().setDirty(this);
				}
			}
		}
		finally {
			lock.unlock();
		}
	}

	public int statGetCurrentValue(String statName) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			return stat.getCurrentValue();
		}
		finally {
			lock.unlock();
		}
	}

	public int statGetBaseValue(String statName) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			return stat.getBaseValue();
		}
		finally {
			lock.unlock();
		}
	}

	public int statGetMinValue(String statName) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			return stat.getMinValue();
		}
		finally {
			lock.unlock();
		}
	}

	public int statGetMaxValue(String statName) {
		lock.lock();
		try {
			AgisStat stat = (AgisStat) getProperty(statName);
			return stat.getMaxValue();
		}
		finally {
			lock.unlock();
		}
	}

	public void statSendUpdate(boolean sendAll) {
		statSendUpdate(sendAll, null);
	}

	public void statSendUpdate(boolean sendAll, OID targetOid) {
		lock.lock();
		try {
	        PropertyMessage propMsg = null;
	        TargetedPropertyMessage targetPropMsg = null;
	        if (targetOid == null)
	            propMsg = new PropertyMessage(getOwnerOid());
	        else
	            targetPropMsg =
	                new TargetedPropertyMessage(targetOid,getOwnerOid());
	        int count = 0;
		    for (Object value : getPropertyMap().values()) {
		    	if (value instanceof AgisStat) {
		    		AgisStat stat = (AgisStat) value;
		    		if (sendAll || stat.isDirty()) {
		    			if (propMsg != null)
	                    	propMsg.setProperty(stat.getName(), stat.getCurrentValue());
	                	else
	                		targetPropMsg.setProperty(stat.getName(), stat.getCurrentValue());
	                	if (! sendAll)
	                		stat.setDirty(false);
	                	count++;
		    		}
		    	}
		    }
		    if (count > 0) {
		    	Engine.getPersistenceManager().setDirty(this);
	            if (propMsg != null)
	                Engine.getAgent().sendBroadcast(propMsg);
	            else
	                Engine.getAgent().sendBroadcast(targetPropMsg);
		    }
		}
		finally {
		    lock.unlock();
		}
	}
	
	/**
	 * Cycles through the vitalityStats map to see if any vitality stats should have the shift run.
	 */
	public void runCombatTick() {
		for (String statName : vitalityStats.keySet()) {
			// Has enough time elapsed for another shift update
			//Log.debug("COMBAT: comparing statUpdateTime: " + vitalityStats.get(statName) + " against current time: " + System.currentTimeMillis());
			if (vitalityStats.get(statName) < System.currentTimeMillis()) {
				VitalityStatDef statDef = (VitalityStatDef)CombatPlugin.lookupStatDef(statName);
				// Apply the shift
				applyStatShift(statName);
				// Reset the update time
				
				int updateInterval = statDef.getShiftInterval();
				vitalityStats.put(statName, System.currentTimeMillis() + (updateInterval * 1000));
			}
		}
	}
	
	private void applyStatShift(String statName) {
		AgisStat stat = (AgisStat) getProperty(statName);
		VitalityStatDef statDef = (VitalityStatDef)CombatPlugin.lookupStatDef(statName);
		// Is there a shift to run?
		int shiftDirection = statDef.getShiftDirection(stat, this);
		
		lock.lock();
		try {
			if (shiftDirection != 0) {
				int delta = stat.getShift(shiftDirection);
				//Log.debug("SHIFT: applying shift with delta: " + delta);
				stat.modifyBaseValue(delta);
			
				statDef.update(stat, this);
				statSendUpdate(false);
				Engine.getPersistenceManager().setDirty(this);
			}
		}
		finally {
			lock.unlock();
		}
	}
	
	public HashMap<String, Long> getVitalityStats() {
		return vitalityStats;
	}
	public void setVitalityStats(HashMap<String, Long> vitalityStats) {
		this.vitalityStats = vitalityStats;
	}
	
	public void addVitalityStat(AgisStat stat, long updateInterval) {
		Log.debug("COMBAT: adding vitality stat: " + stat.name);
		VitalityStatDef statDef = (VitalityStatDef)CombatPlugin.lookupStatDef(stat.name);
		stat.setBaseShiftValue(statDef.getShiftValue(), statDef.getReverseShiftValue());
		vitalityStats.put(stat.name, System.currentTimeMillis() + (updateInterval * 1000));
	}
	HashMap<String, Long> vitalityStats = new HashMap<String, Long>();
    
    /*
     * Group specific data
     */
    
	transient protected OID groupOid = null;
	
	public void setGroupOid(OID groupOid){
		this.groupOid = groupOid;
	}

	public OID getGroupOid(){
		return groupOid;
	}	
	
	transient protected OID groupMemberOid = null;
	
	public void setGroupMemberOid(OID groupMemberOid){
		this.groupMemberOid = groupMemberOid;
	}
	
	public OID getGroupMemberOid(){
		return groupMemberOid;
	}	 
	
	public boolean isGrouped(){
		return groupOid != null;
	}
	
	transient protected boolean pendingGroupInvite = false;
	
	public void setPendingGroupInvite(boolean flag){
		this.pendingGroupInvite = flag;
	}
	
	public boolean isPendingGroupInvite(){
		return this.pendingGroupInvite;
	}
	
	/*
	 * Final Static properties
	 */
	public final static String COMBAT_PROP_BACKREF_KEY = "combat.backref";
	public final static String COMBAT_PROP_USERFLAG = "combat.userflag";
	public final static String COMBAT_PROP_MOBFLAG = "combat.mobflag";

	public final static String COMBAT_PROP_AUTOATTACK_ABILITY = "combat.autoability";
	public final static String COMBAT_PROP_AUTOATTACK_BASE = "combat.autoabilitybase";
	public final static String COMBAT_PROP_REGEN_EFFECT = "combat.regeneffect";

	public final static String COMBAT_PROP_ENERGY = "energy";
	public final static String COMBAT_PROP_HEALTH = "health";

	public final static String COMBAT_PROP_COMBATSTATE = "combatstate";
	public final static String COMBAT_PROP_DEADSTATE = "deadstate";
	public final static String COMBAT_PROP_ATTACKABLE = "attackable";
	public final static String COMBAT_PROP_STATE = "state";
	
	public final static String COMBAT_PROP_ASPECT = "aspect";
	public final static String COMBAT_PROP_TEAM = "team";
	public final static String COMBAT_TAG_OWNER = "tagOwner";
	
	public final static String COMBAT_STATE_INCAPACITATED = "incapacitated";
	public final static String COMBAT_STATE_EVADE = "evade";
	public final static String COMBAT_STATE_IMMUNE = "immune";
	public final static String COMBAT_STATE_SPIRIT = "spirit";
	
	public final static String COMBAT_AMMO_LOADED = "ammoloaded";
	//public final static String COMBAT_AMMO_TYPE = "ammotype";
	public final static String COMBAT_AMMO_DAMAGE = "ammodamage";
	public final static String COMBAT_AMMO_EFFECT = "ammoeffect";
	
	public final static String COMBAT_PROP_DUEL_ID = "duelID";
	
	public final static int NUM_ACTIONS = 10;
	
	/*class CombatTick implements Runnable {

		@Override
		public void run() {
			// Go through each vitality stat and run the update
			
		}
		
	}*/

	private static final long serialVersionUID = 1L;

	static {
		try {
			BeanInfo info = Introspector.getBeanInfo(CombatInfo.class);
			PropertyDescriptor[] propertyDescriptors = info.getPropertyDescriptors();
			for (int i = 0; i < propertyDescriptors.length; ++i) {
				PropertyDescriptor pd = propertyDescriptors[i];
				if (pd.getName().equals("currentAction")) {
					pd.setValue("transient", Boolean.TRUE);
				}
			}
		} catch (Exception e) {
			Log.error("failed beans initalization");
		}
	}
}
