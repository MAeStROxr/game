package atavism.agis.objects;

import atavism.agis.behaviors.CombatPetBehavior;
import atavism.agis.plugins.AgisMobClient;
import atavism.agis.plugins.AgisMobPlugin;
import atavism.agis.plugins.CombatClient;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.NoRecipientsException;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.ObjectStub;
import atavism.server.objects.SpawnData;
import atavism.server.objects.Template;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.ObjectManagerPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class CombatPet extends Pet implements Serializable, MessageCallback
{
    public CombatPet() {
    }

    public CombatPet(int mobTemplateID, OID ownerOid, Long duration, int passiveEffect) {
    	this.mobTemplateID = mobTemplateID;
    	this.ownerOid = ownerOid;
    	this.passiveEffect = passiveEffect;
    	summonPet();
    }
    
    /**
     * Spawns a copy of the pet at the owners location.
     */
    public void summonPet() {
    	// Do we need to make sure the template has been registered?
    	Log.debug("PET: summon pet hit");
    	if (isSpawned == true) {
    		Log.debug("PET: pet is already spawned");
    		boolean wasSpawned = despawnPet();
    		/*if (wasSpawned) {
    			return;
    		}*/
    	}
    	//Log.debug("PET: summoning pet with props: " + propMapWM);
    	MobFactory mobFactory = new MobFactory(mobTemplateID);
    	mobFactory.addBehav(new BaseBehavior());
    	CombatPetBehavior ncpBehav = new CombatPetBehavior();
    	ncpBehav.setOwnerOid(ownerOid);
    	mobFactory.addBehav(ncpBehav);
    	BasicWorldNode bwNode = WorldManagerClient.getWorldNode(ownerOid);
    	SpawnData spawnData = new SpawnData();
    	ObjectStub obj = null;
        obj = mobFactory.makeObject(spawnData, bwNode.getInstanceOid(), bwNode.getLoc());
        obj.spawn();
        InterpolatedWorldNode iwNode = obj.getWorldNode();
        Log.debug("PET: pet " + mobName + " spawned at: " + iwNode.getLoc() + " in instance: " + iwNode.getInstanceOid());
        Log.debug("PET: owner is at: " + bwNode.getLoc() + " in instance: " + bwNode.getInstanceOid());
        // Update the display - this really needs fixed...
        Template tmpl = ObjectManagerClient.getTemplate(mobTemplateID, ObjectManagerPlugin.MOB_TEMPLATE);
        String gender = (String) tmpl.get(WorldManagerClient.NAMESPACE, "genderOptions");
    	if (gender.equals("Either")) {
    		Random random = new Random();
    		if (random.nextInt(2) == 0)
    			gender = "Male";
    		else
    			gender = "Female";
    	}
        isSpawned = true;
        mobObj = obj.getOid();
        AgisMobPlugin.setDisplay(mobObj, gender);
        AgisMobClient.petCommandUpdate(mobObj, attitude, null);
        AgisMobClient.petCommandUpdate(mobObj, currentCommand, null);
        boolean activated = activate();
        EnginePlugin.setObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "activePet", mobObj);
        // Copy the owners factions to the pet
        String faction = (String) EnginePlugin.getObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "faction");
        String tempFaction = (String) EnginePlugin.getObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "temporaryFaction");
        EnginePlugin.setObjectProperty(mobObj, WorldManagerClient.NAMESPACE, "faction", faction);
        EnginePlugin.setObjectProperty(mobObj, WorldManagerClient.NAMESPACE, "temporaryFaction", tempFaction);
        
        updatePetCombatStats();
        applyPassiveEffect();
    }
    
    /**
     * Applies the passive Effect tied to this pet to the spawned pet mob.
     */
    private void applyPassiveEffect() {
    	if (passiveEffect != -1) {
    		CombatClient.applyEffect(mobObj, passiveEffect);
    	}
    }
    
    /**
     * Sends out a message to update the combat stats of the spawned pet mob.
     */
    private void updatePetCombatStats() {
    	AgisMobClient.updatePetStats(ownerOid, mobObj, currentLevel, 20);
    }
    
    /**
     * Despawns the pet.
     */
    public boolean despawnPet() {
    	boolean wasSpawned = false;
    	//mobObj.despawn();
    	Log.debug("PET: despawn hit with isSpawned: " + isSpawned);
    	if (isSpawned) {
    		try {
    			Log.debug("PET: despawning pet: " + mobObj);
    	        WorldManagerClient.despawn(mobObj);
    	        Log.debug("PET: despawned pet: " + mobObj);
    		} catch (NoRecipientsException e) {
    			Log.debug("PET: no recipients found for despawn pet.");
    		}
    		isSpawned = false;
    		boolean deactivated = deactivate();
    	    EnginePlugin.setObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "activePet", null);
    	    wasSpawned = true;
    	}
    	return wasSpawned;
    }

    /**
     * Activates the message subscriptions so this pet object can pick up relevant messages.
     * @return
     */
    public boolean activate() {
        Log.debug("PET: in activate: this " + this);
        // Clear the old subscribers
        sub = null;
        sub2 = null;
        // subscribe for some messages
        SubjectFilter filter = new SubjectFilter(mobObj);
        //filter.addType(InventoryClient.MSG_TYPE_INV_UPDATE);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter.addType(AgisMobClient.MSG_TYPE_PET_TARGET_LOST);
        sub = Engine.getAgent().createSubscription(filter, this);
        if (sub == null)
        	Log.debug("PET: sub is null");
        
        SubjectFilter filter2 = new SubjectFilter(ownerOid);
        filter2.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter2.addType(AgisMobClient.MSG_TYPE_SEND_PET_COMMAND);
        //sub2 = Engine.getAgent().createSubscription(filter2, this, MessageAgent.RESPONDER);
        sub2 = Engine.getAgent().createSubscription(filter2, this);
        Log.debug("PET: set up subscription for pet owner: " + ownerOid);
        return true;
    }
    
    /**
     * Deactivates the message subscriptions so this pet object will no longer pick up any messages.
     * @return
     */
    public boolean deactivate() {
	   Log.debug("PET: deactivating");
        if (sub != null) {
            Engine.getAgent().removeSubscription(sub);
            sub = null;
            Log.debug("PET: removing sub");
	    }
        if (sub2 != null) {
            Engine.getAgent().removeSubscription(sub2);
            sub2 = null;
            Log.debug("PET: removing sub 2");
	    }
        return true;
    }
    
    /**
     * process network messages
     */
    public void handleMessage(Message msg, int flags) {
    	//Log.debug("PET: got message with type: " + msg.getMsgType());
    	if (msg instanceof PropertyMessage) {
    		PropertyMessage propMsg = (PropertyMessage) msg;
    		OID subject = propMsg.getSubject();
    		if (subject.equals(mobObj)) {
    			handlePetPropertyUpdate(propMsg);
    		} else if (subject.equals(ownerOid)){
    			handleOwnerPropertyUpdate(propMsg);
    		}
    	} else if (msg.getMsgType() == AgisMobClient.MSG_TYPE_SEND_PET_COMMAND) {
    		Log.debug("PET: got send pet command message");
    		AgisMobClient.sendPetCommandMessage spcMsg = (AgisMobClient.sendPetCommandMessage) msg;
    		String command = spcMsg.getCommand();
    		OID targetOid = spcMsg.getTargetOid();
    		handleCommand(command, targetOid);
    	} else if (msg.getMsgType() == AgisMobClient.MSG_TYPE_PET_TARGET_LOST) {
    		Log.debug("PET: got send pet command message");
    		AgisMobClient.petTargetLostMessage spcMsg = (AgisMobClient.petTargetLostMessage) msg;
    		handleTargetLost();
    	} else {
            log.error("PET: unknown msg: " + msg);
        }
        //return true;
    }
    
    /**
     * Processes commands, which will either update the pets attitude, the current command, or activate
     * an ability that the pet has.
     * @param command
     */
    public void handleCommand(String command, OID targetOid) {
    	if (!isSpawned) {
    		Log.debug("PET: command issued to pet that is not spawned");
    		return;
    	}
    	Log.debug("PET: issuing pet command: " + command);
    	if (command.equals("passive")) {
    		updateAttitude(1);
    	} else if (command.equals("defensive")) {
    		updateAttitude(2);
    	} else if (command.equals("aggressive")) {
    		updateAttitude(3);
    	} else if (command.equals("stay")) {
    		updateCommand(-1, targetOid);
    	} else if (command.equals("follow")) {
    		updateCommand(-2, targetOid);
    	} else if (command.equals("attack")) {
    		if (targetOid.equals(ownerOid) || targetOid.equals(mobObj)) {
    			ExtendedCombatMessages.sendErrorMessage(ownerOid, "Your pet cannot attack that target");
    		} else {
    			updateCommand(-3, targetOid);
    		}
    	}
    }
    
    /**
     * Sets the pet to follow the owner if its current command was set to attack.
     */
    private void handleTargetLost() {
    	Log.debug("PET: pet has lost target, checking current command: " + currentCommand);
    	if (currentCommand == -3) {
    		updateCommand(-2, null);
    	}
    }
    
    /**
     * Deals with the different property updates that have occurred for the pet.
     * @param propMsg
     */
    protected void handlePetPropertyUpdate(PropertyMessage propMsg) {
    	Boolean dead = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);
	    if (dead != null && dead) {
	    	Log.debug("PET: got pet death, despawning");
	    	DespawnPet despawnPet = new DespawnPet();
	    	Engine.getExecutor().schedule(despawnPet, 2000, TimeUnit.MILLISECONDS);
	    }
    }
    
    /**
     * Deals with the different property updates that have occurred for the owner of the pet.
     * @param propMsg
     */
    protected void handleOwnerPropertyUpdate(PropertyMessage propMsg) {
    	Log.debug("");
    	Boolean dead = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);
	    if (dead != null && dead) {
	    	Log.debug("PET: got owner death, despawning");
	    	//despawnPet();
	    	DespawnPet despawnPet = new DespawnPet();
	    	Engine.getExecutor().schedule(despawnPet, 2000, TimeUnit.MILLISECONDS);
	    	return;
	    }
	    //TODO: if faction update, update the pets
    }
    
    /**
     * A runnable class that will despawn the spawned pet mob when run.
     * @author Andrew
     *
     */
    class DespawnPet implements Runnable, Serializable {
    	public void run() {
    		if (isSpawned) {
        		try {
        			Log.debug("PET: despawning pet: " + mobObj);
        	        WorldManagerClient.despawn(mobObj);
        	        Log.debug("PET: despawned pet: " + mobObj);
        		} catch (NoRecipientsException e) {
        			Log.debug("PET: no recipients found for despawn pet.");
        		}
        		isSpawned = false;
        		boolean deactivated = deactivate();
        	    EnginePlugin.setObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "activePet", null);
        	}
    	}
    	private static final long serialVersionUID = 1L;
    }

    static final Logger log = new Logger("CombatPet");

    /**
     * Sends out a command update message so the behavior for this pet will act in the specified manner.
     * @param attitude
     */
    public void updateAttitude(int attitude) {
    	this.attitude = attitude;
    	AgisMobClient.petCommandUpdate(mobObj, attitude, null);
    }
    /**
     * Sends out a command update message so the behavior for this pet will perform the requested command.
     * @param attitude
     */
    public void updateCommand(int command, OID target) {
    	this.currentCommand = command;
    	AgisMobClient.petCommandUpdate(mobObj, currentCommand, target);
    }
    
    public String getMobName() {
    	return mobName;
    }
    public void setMobName(String mobName) {
    	this.mobName = mobName;
    }
    public OID getMobObj() {
    	return mobObj;
    }
    public void setMobObj(OID mobObj) {
    	this.mobObj = mobObj;
    }
    public boolean getSpawned() {
    	return isSpawned;
    }
    public void setSpawned(boolean isSpawned) {
    	this.isSpawned = isSpawned;
    }
    public OID getOwnerOid() {
    	return ownerOid;
    }
    public void setOwnerOid(OID ownerOid) {
    	this.ownerOid = ownerOid;
    }
    public int getAttitude() {
    	return attitude;
    }
    public void setAttitude(int attitude) {
    	this.attitude = attitude;
    }
    public int getCurrentCommand() {
    	return currentCommand;
    }
    public void setCurrentCommand(int currentCommand) {
    	this.currentCommand = currentCommand;
    }
    public int getCurrentLevel() {
    	return currentLevel;
    }
    public void setCurrentLevel(int currentLevel) {
    	this.currentLevel = currentLevel;
    }
    public Long getDespawnTime() {
    	return despawnTime;
    }
    public void setDespawnTime(Long despawnTime) {
    	this.despawnTime = despawnTime;
    }
    public int getPassiveEffect() {
    	return passiveEffect;
    }
    public void setPassiveEffect(int passiveEffect) {
    	this.passiveEffect = passiveEffect;
    }
    /*public Map<String, Serializable> getPropMapWM() {
    	return propMapWM;
    }
    public void setPropMapWM(Map<String, Serializable> propMapWM) {
    	this.propMapWM = propMapWM;
    }
    public Map<String, Serializable> getPropMapC() {
    	return propMapC;
    }
    public void setPropMapC(Map<String, Serializable> propMapC) {
    	this.propMapC = propMapC;
    }*/
    public Long getSub() {
    	return sub;
    }
    public void setSub(Long sub) {
    	this.sub = sub;
    }
    public Long getSub2() {
    	return sub2;
    }
    public void setSub2(Long sub2) {
    	this.sub2 = sub2;
    }

    private String mobName;
    //private Template mobTemplate = null;
    private OID mobObj;
    private boolean isSpawned;
    private OID ownerOid;
    private int attitude = 2; // 1: Passive, 2: Defensive, 3: Aggressive
    private int currentCommand = -2; // -1: Stay, -2: Follow, -3: Attack
    private int currentLevel = 1;
    private Long despawnTime;
    int passiveEffect = -1;
    //private Map<String, Serializable> propMapWM = new HashMap<String, Serializable>();
    //private Map<String, Serializable> propMapC = new HashMap<String, Serializable>();
    private Long sub;
    private Long sub2;
    
    private static final long serialVersionUID = 1L;
}
