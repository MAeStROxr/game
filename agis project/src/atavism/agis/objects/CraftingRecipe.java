package atavism.agis.objects;

import atavism.server.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Contains all of the information needed to define a recipe for crafting an item.
 * @author Andrew Harrison
 *
 */
public class CraftingRecipe {
	protected int id;
	protected String name;
	protected String iconName;
	
	protected boolean isHiddenRecipe;
	
	protected String stationReq;
	protected boolean mustMatchLayout;
	
	protected int skillID;
	protected int requiredSkillLevel;
	protected ArrayList<Integer> resultItemIds;
	protected ArrayList<Integer> resultItemCounts;
	
	protected int recipeItemId;
	protected boolean qualityChangeable;
	protected boolean allowDyes;
	protected boolean allowEssences;
	
	protected int creationTime = 0;
	
	
	protected LinkedList<LinkedList<CraftingComponent>> requiredCraftingComponents = new LinkedList<LinkedList<CraftingComponent>>();
	int gridSize = 4;
	
	public CraftingRecipe(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getIconName() {
		return iconName;
	}
	public void setIconName(String icon) {
		iconName = icon;
	}
	
	public String getStationReq() {
		return stationReq;
	}
	public void setStationReq(String req) {
		stationReq = req;
	}
	
	public int getCreationTime()
	{
		return creationTime;
	}
	public void setCreationTime(int time) {
		creationTime = time;
	}
	
	public boolean getMustMatchLayout(){
		return mustMatchLayout;
	}
	public void setMustMatchLayout(Boolean layoutReq){
		mustMatchLayout = layoutReq;
	}
	
	public boolean getIsHiddenRecipe(){
		return isHiddenRecipe;
	}
	public void setIsHiddenRecipe(Boolean hidden){
		isHiddenRecipe = hidden;
	}
	
	public int getSkillID(){
		return skillID;
	}
	public void setSkillID(int skill){
		skillID = skill;
	}
	
	public int getRequiredSkillLevel(){
		return requiredSkillLevel;
	}
	public void setRequiredSkillLevel(int level){
		requiredSkillLevel = level;
	}
	
	public ArrayList<Integer> getResultItemIds(){
		return resultItemIds;
	}
	public void setResultItemIds(ArrayList<Integer> ids){
		resultItemIds = ids;
	}
	
	public ArrayList<Integer> getResultItemCounts() {
		return resultItemCounts;
	}
	public void setResultItemCounts(ArrayList<Integer> counts) {
		resultItemCounts = counts;
	}
	
	public int getRecipeItemId(){
		return recipeItemId;
	}
	public void setRecipeItemId(int id){
		recipeItemId = id;
	}
	
	public boolean getQualityChangeable(){
		return qualityChangeable;
	}
	public void setQualityChangeable(boolean changeable){
		qualityChangeable = changeable;
	}
	
	public boolean getAllowDyes(){
		return allowDyes;
	}
	public void setAllowDyes(boolean allow){
		allowDyes = allow;
	}
	
	public boolean getAllowEssences(){
		return allowEssences;
	}
	public void setAllowEssences(boolean allow){
		allowEssences = allow;
	}
	
	public String getName(){
		return name;
	}
	
	public int getID() {
		return id;
	}
	
	public LinkedList<LinkedList<CraftingComponent>> getRequiredCraftingComponents(){
		return requiredCraftingComponents;
	}
	public void addCraftingComponentRow(LinkedList<CraftingComponent> defs){
		requiredCraftingComponents.add(defs);
	}
	
	public LinkedList<Integer> getRequiredItems() {
		LinkedList<Integer> requiredItems = new LinkedList<Integer>();
		for (int i = 0; i < requiredCraftingComponents.size(); i++) {
			for (int j = 0; j < requiredCraftingComponents.size(); j++) {
				if (requiredCraftingComponents.get(i).get(j).itemId != -1) {
					requiredItems.add(requiredCraftingComponents.get(i).get(j).itemId);
				}
			}
		}
		return requiredItems;
	}
	
	public LinkedList<Integer> getRequiredItemCounts() {
		LinkedList<Integer> requiredCounts = new LinkedList<Integer>();
		for (int i = 0; i < requiredCraftingComponents.size(); i++) {
			for (int j = 0; j < requiredCraftingComponents.size(); j++) {
				if (requiredCraftingComponents.get(i).get(j).itemId != -1) {
					requiredCounts.add(requiredCraftingComponents.get(i).get(j).count);
				}
			}
		}
		return requiredCounts;
	}
	
	public boolean DoesRecipeMatch(LinkedList<LinkedList<CraftingComponent>> components, String stationType) {
		// First check station type
		Log.debug("CRAFTING: recipeMatch station: " + stationType + " against required station: " + stationReq);
		if (!stationReq.equals(stationType) && !stationReq.contains("none")) {
			return false;
		}
		
		if (!mustMatchLayout) {
			LinkedList<CraftingComponent> componentIds = new LinkedList<CraftingComponent>();
			for (int i = 0; i < requiredCraftingComponents.size(); i++) {
				for (int j = 0; j < requiredCraftingComponents.get(i).size(); j++) {
					componentIds.add(requiredCraftingComponents.get(i).get(j));
				}
			}
			
			// Loop through all required component Ids and see if they are provided
			for (CraftingComponent component : componentIds) {
				boolean componentFound = false;
				for (int i = 0; i < components.size(); i++) {
					LinkedList<CraftingComponent> rowComponents = components.get(i);
					for (int j = 0; j < rowComponents.size(); j++) {
						if (rowComponents.get(j).itemId == component.itemId && rowComponents.get(j).count >= component.count) {
							componentFound = true;
							break;
						}
					}
				}
				
				if (!componentFound) {
					return false;
				}
			}
			
			return true;
		}
		
		for (int i = 0; i < requiredCraftingComponents.size(); i++) {
			LinkedList<CraftingComponent> reqRowComponents = requiredCraftingComponents.get(i);
			LinkedList<CraftingComponent> rowComponents = null;
			if (components.size() > i)
				rowComponents = components.get(i);
			for (int j = 0; j < reqRowComponents.size(); j++) {
				CraftingComponent reqComponent = reqRowComponents.get(j);
				if (reqComponent != null && reqComponent.itemId != -1) {
					if (rowComponents == null || rowComponents.size() <= j) {
						Log.debug("CRAFTING: item in row: " + i + " slot: " + j + " is null");
						return false;
					}
					if (reqComponent.itemId != rowComponents.get(j).itemId) {
						Log.debug("CRAFTING: item in row: " + i + " slot: " + j + " does not match item: " + reqComponent.itemId + " got: " + rowComponents.get(j).itemId);
						return false;
					}
					if (reqComponent.count > rowComponents.get(j).count) {
						Log.debug("CRAFTING: item in row: " + i + " slot: " + j + " does not match count for item: " + reqComponent.itemId + " got: " + rowComponents.get(j).count);
						return false;
					}
				} else {
					// If the slot should be null, but the player placed an item in, we also return false
					if (rowComponents != null && rowComponents.size() > j 
							&& rowComponents.get(j) != null && rowComponents.get(j).itemId != -1) {
						Log.debug("CRAFTING: item in row: " + i + " slot: " + j + " should be null but isn't, got item: " + rowComponents.get(j).itemId);
						return false;
					}
				}
			}
		}
		
		return true;
	}
}
