package atavism.agis.objects;

import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CraftingPlugin;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Runs the task of crafting resulting in an item upon completion.
 * @author Andrew
 *
 */
public class CraftingTask implements Runnable, MessageCallback {
	
	protected CraftingRecipe recipe;
	LinkedList<Integer> componentIds;
	LinkedList<Integer> componentStacks;
	LinkedList<Long> specificComponents;
	boolean useSpecificItems = false;
	OID playerOid;
	int recipeId;
	
	boolean interrupted = false;
	transient Long sub = null;
	
	public CraftingTask(CraftingRecipe recipe, OID oid, int recipeId){
		this.recipe = recipe;
		componentIds = new LinkedList<Integer>();
		componentStacks = new LinkedList<Integer>();
		playerOid = oid;
		this.recipeId = recipeId;
		
		LinkedList<LinkedList<CraftingComponent>> components = recipe.getRequiredCraftingComponents();
		for(int i = 0; i < CraftingPlugin.GRID_SIZE; i++) {
			for(int j = 0; j < CraftingPlugin.GRID_SIZE; j++) {
				componentIds.add(components.get(i).get(j).getItemId());
				componentStacks.add(components.get(i).get(j).getCount());
			}
		}
		
		if (recipe.getCreationTime() > 0) {
			setupMessageSubscription();
			EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "currentTask", CraftingPlugin.TASK_CRAFTING);
		}
	}
	
	public CraftingTask(CraftingRecipe r, LinkedList<Long> cid, LinkedList<Integer> cs, OID oid, int recipeId){
		recipe = r;
		specificComponents = cid;
		componentStacks = cs;
		playerOid = oid;
		this.recipeId = recipeId;
		useSpecificItems = true;
		
		componentIds = new LinkedList<Integer>();
		LinkedList<LinkedList<CraftingComponent>> components = recipe.getRequiredCraftingComponents();
		for(int i = 0; i < CraftingPlugin.GRID_SIZE; i++) {
			for(int j = 0; j < CraftingPlugin.GRID_SIZE; j++) {
				componentIds.add(components.get(i).get(j).getItemId());
				componentStacks.add(components.get(i).get(j).getCount());
			}
		}
		
		if (recipe.getCreationTime() > 0) {
			setupMessageSubscription();
			EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "currentTask", CraftingPlugin.TASK_CRAFTING);
		}
	}
	
	void setupMessageSubscription() {
	    // Set up a message hook for when the caster is moving so we can interrupt
	    SubjectFilter filter = new SubjectFilter(playerOid);
	    filter.addType(CombatClient.MSG_TYPE_INTERRUPT_ABILITY);
	    sub = Engine.getAgent().createSubscription(filter, this);
	    Log.debug("CRAFTING: subscribed to interrupt message");
	}
	
	public void handleMessage(Message msg, int flags) {
        if (msg instanceof CombatClient.interruptAbilityMessage) {
            processInterrupt();
        } else {
            Log.error("unknown msg: " + msg);
        }
    }
    
    private void processInterrupt() {
    	Log.debug("CRAFTING: got interrupt");
    	interrupted = true;
    	if (sub != null)
            Engine.getAgent().removeSubscription(sub);
    	Log.debug("TASK: processing interrupt ");
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "CraftingMsg");
		props.put("PluginMessageType", "CraftingInterrupted");
		TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		
		//String currentTask = (String) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "currentTask");
		//Log.debug("TASK: Got current task: " + currentTask);
		//if (currentTask.equals(CraftingPlugin.TASK_CRAFTING)) {
			
		//}
		Engine.getAgent().sendBroadcast(playerMsg);
		
		EnginePlugin.setObjectPropertyNoResponse(playerOid, WorldManagerClient.NAMESPACE, "currentTask", "");
    }
    
    public void PlayCoordinatedEffect(String coordEffect) {
    	CoordinatedEffect cE = new CoordinatedEffect(coordEffect);
	    cE.sendSourceOid(true);
	    cE.sendTargetOid(true);
	    cE.putArgument("length", (float)recipe.getCreationTime());
	    cE.invoke(playerOid, playerOid);
    }

	@Override
	public void run() {
		Log.debug("CRAFT: running crafting task with specific items: " + useSpecificItems);
		if (sub != null)
            Engine.getAgent().removeSubscription(sub);
		
		if (interrupted)
			return;
		
		// Reset currentTask
		String currentTask = (String) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "currentTask");
		if (currentTask.equals(CraftingPlugin.TASK_CRAFTING)) {
			EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "currentTask", "");
		}
		
		// Dead check
    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
    	if (dead) {
    		return;
    	}
		
		if (useSpecificItems) {
			// Get AgisItem Info for each Item
			/*for (int i = 0; i < specificComponents.size(); i++) {
				Log.debug("CRAFTING: removing specific item: " + OID.fromLong(specificComponents.get(i)) + " x" + componentStacks.get(i));
			    AgisInventoryClient.removeSpecificItem(playerOid, OID.fromLong(specificComponents.get(i)), false, componentStacks.get(i));
			}*/
			HashMap<Integer, Integer> itemsToRemove = new HashMap<Integer, Integer>();
			for (int i = 0; i < componentIds.size(); i++) {
				if (componentIds.get(i) > 0) {
					Log.debug("CRAFTING: removing generic item: " + componentIds.get(i) + " x" + componentStacks.get(i));
					//AgisInventoryClient.removeGenericItem(playerOid, componentIds.get(i), false, componentStacks.get(i));
					itemsToRemove.put(componentIds.get(i), componentStacks.get(i));
				}
			}
			Log.debug("CRAFTING: sending removeGenericItems message");
			AgisInventoryClient.removeGenericItems(playerOid, itemsToRemove, false);
		} else {
			//ArrayList<Integer> test = new ArrayList<Integer>();
			//test.addAll(componentIds);
			//List<OID> itemList = InventoryClient.findItems(playerOid, test);
			HashMap<Integer, Integer> itemsToRemove = new HashMap<Integer, Integer>();
			for (int i = 0; i < componentIds.size(); i++) {
				if (componentIds.get(i) > 0) {
					Log.debug("CRAFTING: removing generic item: " + componentIds.get(i) + " x" + componentStacks.get(i));
					//AgisInventoryClient.removeGenericItem(playerOid, componentIds.get(i), false, componentStacks.get(i));
					itemsToRemove.put(componentIds.get(i), componentStacks.get(i));
				}
			}
			AgisInventoryClient.removeGenericItems(playerOid, itemsToRemove, false);
		}
		
		for (int i = 0; i < recipe.getResultItemIds().size(); i++) {
			if (recipe.getResultItemIds().get(i) > 0)
				AgisInventoryClient.generateItem(playerOid, recipe.getResultItemIds().get(i), "" /*recipe.getName()*/, recipe.getResultItemCounts().get(i), null);
		}
		CombatClient.abilityUsed(playerOid, recipe.getSkillID());
		Log.debug("CRAFTING PLUGIN: Crafting Recipe " + recipeId + " with skill: " + recipe.getSkillID());
		
		Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "CraftingMsg");
		props.put("PluginMessageType", "CraftingCompleted");
		TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(playerMsg);
	}
}
