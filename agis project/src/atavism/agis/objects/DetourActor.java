package atavism.agis.objects;

import atavism.agis.plugins.AgisMobClient;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.engine.BaseBehavior.ArrivedEventMessage;
import atavism.server.engine.BaseBehavior.FollowCommandMessage;
import atavism.server.engine.BaseBehavior.GotoCommandMessage;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.EntityHandle;
import atavism.server.objects.EntityWithWorldNode;
import atavism.server.objects.ObjectStub;
import atavism.server.pathing.PathLocAndDir;
import atavism.server.pathing.PathState;
import atavism.server.pathing.crowd.CrowdAgentParams;
import atavism.server.pathing.crowd.UpdateFlags;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.util.ArrayList;

public class DetourActor implements MessageCallback {
	
	public DetourActor(OID oid, ObjectStub obj) {
		this.oid = oid;
		this.obj = obj;
		InterpolatedWorldNode node = (InterpolatedWorldNode) obj.getWorldNode();
		lastDir = node.getDir();
		lastLoc = node.getCurrentLoc();
		lastTargetLoc = node.getCurrentLoc();
        
        // Create Params
        params = new CrowdAgentParams();
        params.CollisionQueryRange = 6f;
        params.Height = 2f;
        params.MaxAcceleration = 50f;
        params.MaxSpeed = 50f;
        params.ObstacleAvoidanceType= 3;
        params.PathOptimizationRange = 18f;
        params.Radius = 0.4f;
        params.SeparationWeight = 2.0f;
        params.UpdateFlags = UpdateFlags.None;
	}
	
	/**
	 * Sets up the subscription to receive messages.
	 */
	public void activate() {
		SubjectFilter filter = new SubjectFilter(oid);
        filter.addType(Behavior.MSG_TYPE_COMMAND);
        filter.addType(WorldManagerClient.MSG_TYPE_MOB_PATH_CORRECTION);
        commandSub = Engine.getAgent().createSubscription(filter, this);
        Engine.getAgent().sendBroadcast(new ArrivedEventMessage(oid));
        pathState = new PathState(oid, "Generic", true);
        // Disable the base behavior message handler
        Engine.getAgent().sendBroadcast(new BaseBehavior.DisableCommandMessage(oid));
	}
	
	/**
	 * Removes the subscription to messages.
	 */
	public void deactivate() {
		if (commandSub != null) {
	        Engine.getAgent().removeSubscription(commandSub);
	        commandSub = null;
	    }
	}
	
	@Override
	/**
	 * Handles the messages this instance receives. Gets the message type and works out
	 * what needs to be done.
	 */
	public void handleMessage(Message msg, int flags) {
		if (msg.getMsgType() == Behavior.MSG_TYPE_COMMAND) {
            Behavior.CommandMessage cmdMsg = (Behavior.CommandMessage)msg;
            String command = cmdMsg.getCmd();
            if (Log.loggingDebug)
                Log.debug("DetourActor.onMessage: command = " + command + "; oid = " + oid);
            if (command.equals(BaseBehavior.MSG_CMD_TYPE_GOTO)) {
                GotoCommandMessage gotoMsg = (GotoCommandMessage)msg;
                Point destination = gotoMsg.getDestination();
                mode = BaseBehavior.MSG_CMD_TYPE_GOTO;
                roamingBehavior = true;
                setupGoto(destination, gotoMsg.getSpeed());
            }
            else if (command.equals(BaseBehavior.MSG_CMD_TYPE_STOP)) {
                followTarget = null;
                pathState.clear();
                interpolatingPath = false;
                InterpolatedWorldNode node = (InterpolatedWorldNode) obj.getWorldNode();
                node.setDir(new AOVector(0,0,0));
                WorldManagerClient.updateWorldNode(oid, new BasicWorldNode(node));
                mode = BaseBehavior.MSG_CMD_TYPE_STOP;
                // If roamingBehavior is set, that means that we formerly had a roaming behavior, so send
                // an ArrivedEventMessage so that the other behavior starts up again.
                if (roamingBehavior) {
                    try {
                        Engine.getAgent().sendBroadcast(new ArrivedEventMessage(oid));
                    }
                    catch (Exception e) {
                        Log.error("BaseBehavior.onMessage: Error sending ArrivedEventMessage, error was '" + e.getMessage() + "'");
                        throw new RuntimeException(e);
                    }
                }
            }
            else if (command.equals(BaseBehavior.MSG_CMD_TYPE_FOLLOW)) {
                FollowCommandMessage followMsg = (FollowCommandMessage)msg;
                mode = BaseBehavior.MSG_CMD_TYPE_FOLLOW;
                followTarget = followMsg.getTarget();
                target = (ObjectStub)followTarget.getEntity(Namespace.MOB);
                speed = followMsg.getSpeed();
                distanceToFollowAt = followMsg.getDistanceToFollowAt();
                setupFollow(lastLoc);
            }
        }
        else if (msg.getMsgType() == WorldManagerClient.MSG_TYPE_MOB_PATH_CORRECTION) {
            // Do we need to do anything here?
        }
	}
	
	void setupGoto(Point destination, float speed) {
		InterpolatedWorldNode node = (InterpolatedWorldNode) obj.getWorldNode();
        ArrayList<AOVector> generatedPath = navMeshManager.GeneratePath(node.getCurrentLoc(), destination);
        if (generatedPath == null) {
        	AgisMobClient.sendInvalidPath(oid);
        	return;
        }
        String pathPoints = "PATH: ";
        for (AOVector point : generatedPath) {
        	pathPoints += point.toString() + "; ";
        }
        Log.debug(pathPoints);
        
        setupPathInterpolator(node.getCurrentLoc(), targetLoc, speed, generatedPath);	
        	
		navMeshManager.setActorTarget(oid, destination);
        navMeshManager.setActorSpeed(oid, speed);
        targetLoc = new Point(destination.getX(), destination.getY(), destination.getZ());
	}
	
	/**
	 * Update the target for the Actor by getting the current position of the target object.
	 */
	void setupFollow(Point pos) {
		Log.debug("DETOUR: setupFollow hit");
		lastTargetLoc = target.getWorldNode().getLoc();
		float len = Point.distanceTo(lastTargetLoc, pos);
     	if (len < distanceToFollowAt) {
     		navMeshManager.resetActorTarget(oid);
     		lastDir = AOVector.Zero;
     		navMeshManager.setActorSpeed(oid, 0);
     		return;
     	}
    	InterpolatedWorldNode node = (InterpolatedWorldNode) obj.getWorldNode();
        ArrayList<AOVector> generatedPath = navMeshManager.GeneratePath(pos, lastTargetLoc);
        if (generatedPath == null) {
        	AgisMobClient.sendInvalidPath(oid);
        	return;
        }
        
        // Do fancy calculation to change the position to be distanceToFollowAt away from the targets actual position
     	// This is currently not quite getting the position correctly. The mob is going a bit too far
        AOVector lastPos = generatedPath.get(generatedPath.size()-1);
        AOVector secondLastPos = generatedPath.get(generatedPath.size()-2);
        len = AOVector.distanceTo(lastPos, secondLastPos);
        if (len < distanceToFollowAt && generatedPath.size() > 2) {
        	generatedPath.remove(generatedPath.size()-1);
        } else if (len > distanceToFollowAt){
        	len -= (distanceToFollowAt);
        	AOVector newp2 = new AOVector(lastPos);
        	newp2.sub(secondLastPos);
        	newp2.normalize();
        	newp2.multiply(len);
        	newp2.add(secondLastPos);
        	generatedPath.set(generatedPath.size()-1, newp2);
        }
        
        String generatedPathString = "";
        for (AOVector loc : generatedPath) {
        	generatedPathString += "Point: " + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "; ";
        }
        
        lastPos = generatedPath.get(generatedPath.size()-1);
        
        setupPathInterpolator(pos, new Point(lastPos.getX(), lastPos.getY(), lastPos.getZ()), speed, generatedPath);
        //Log.debug("PATH: generatedPath: " + generatedPathString);
    	
		navMeshManager.setActorTarget(oid, new Point(lastPos.getX(), lastPos.getY(), lastPos.getZ()));
        navMeshManager.setActorSpeed(oid, speed);
        targetLoc = new Point(lastPos.getX(), lastPos.getY(), lastPos.getZ());
        //Log.debug("DETOUR: targetLoc: " + targetLoc + " from followLoc: " + lastTargetLoc + " and currentPos: " + pos);
	}
	
	/**
	 * Called each update of the InstanceNavMesh. Checks if the location or direction has changed much and if so
	 * sends an update to the clients.
	 * @param dir
	 * @param pos
	 */
	public void updateDirLoc(AOVector dir, Point pos) {
		//Log.debug("DETOUR: updateDirLoc for: " + oid + " new Dir: " + dir + ", new loc: " + pos + " and oldDir: " + lastDir + " and lastLoc: " + lastPos);
		/*if (lastLoc == null || pos == null || lastDir == null) {
			Log.debug("DETOUR: hit null pos or dir");
		}
		// Work out if we need to send down an update to the clients
		float distanceSquared = Point.distanceToSquared(lastLoc, pos);
		AOVector dirDiff = AOVector.sub(dir,lastDir);
		if (Math.abs(dirDiff.getX()) > 0.1 || Math.abs(dirDiff.getY()) > 0.1 || Math.abs(dirDiff.getZ()) > 0.1 || distanceSquared > 36) {
			Log.debug("DETOUR: direction vector or position has changed for: " + oid + " new Dir: " + dir + ", new loc: " + pos);
			lastDir = dir;
			lastLoc = pos;
			BasicWorldNode wnode = new BasicWorldNode();
			wnode.setInstanceOid(navMeshManager.getInstanceOid());
			if (targetLoc != null) {
				distanceSquared = Point.distanceToSquared(pos, targetLoc);
				Log.debug("DETOUR: distanceSquared = " + distanceSquared);
				if (distanceSquared < 0.5f) {
					Engine.getAgent().sendBroadcast(new ArrivedEventMessage(oid));
					navMeshManager.resetActorTarget(oid);
					lastDir = AOVector.Zero;
					navMeshManager.setActorSpeed(oid, 0);
					// The mob is within 0.5m distance of the target loc, set it to stop unless it is set to follow
					if (!mode.equals(BaseBehavior.MSG_CMD_TYPE_FOLLOW)) {
						Log.debug("DETOUR: setting behaviour to stop");
						targetLoc = null;
						mode = BaseBehavior.MSG_CMD_TYPE_STOP;
					}
				}
			}
			wnode.setDir(lastDir);
			// Set orientation to match direction
			if (targetLoc != null && !lastDir.isZero()) {
				float yaw = AOVector.getLookAtYaw(lastDir);
				lastOrient = new Quaternion();
				lastOrient.setEulerAngles(0, yaw, 0);
				wnode.setOrientation(lastOrient);
			} else if (lastOrient != null){
				wnode.setOrientation(lastOrient);
			}
			wnode.setLoc(pos);
			WorldManagerClient.updateWorldNode(oid, wnode);
		}*/
		
		// update follow if applicable
		/*if (mode != null && mode.equals(BaseBehavior.MSG_CMD_TYPE_FOLLOW)) {
			//float followDistanceSq = distanceToFollowAt * distanceToFollowAt;
			Point followLoc = target.getCurrentLoc();
			if (followLoc != null && lastTargetLoc != null && Point.distanceToSquared(followLoc, lastTargetLoc) > 1) {
				setupFollow(pos);
			}
			//if (Point.distanceToSquared(pos, targetLoc) > followDistanceSq) {
			//	Log.debug("DETOUR: mob is " + Point.distanceToSquared(pos, targetLoc) + " distance from player with min follow: " + followDistanceSq
            //		+ " with followLoc: " + followLoc + " and targetPos: " + targetLoc);
				// If the target has moved set follow up again to get the new position to aim for
				
			//}
		}*/
		
		InterpolatedWorldNode node = (InterpolatedWorldNode) obj.getWorldNode();
		
		if (mode != null && mode.equals(BaseBehavior.MSG_CMD_TYPE_FOLLOW)) {
			Point followLoc = target.getWorldNode().getLoc();
			Point myLoc = node.getLoc();
			float fdist = Point.distanceTo(followLoc, lastTargetLoc);
		    float dist = Point.distanceTo(followLoc, myLoc);
		    if (Log.loggingDebug)
		        Log.debug("BaseBehavior.followUpdate: oid = " + oid + "; myLoc = " + myLoc + "; followLoc = " + followLoc + 
		                      "; fdist = " + fdist + "; dist = " + dist);
		    // If the new target location is more than a meter from
		    // the old one, create a new path.
		    if (fdist > 1) {
		    	if (interpolatingPath)
		    		updateInterpolation();
		    	setupFollow(myLoc);
		    	/*ArrayList<AOVector> generatedPath = navMeshManager.GeneratePath(node.getCurrentLoc(), followLoc);
		    	setupPathInterpolator(node.getCurrentLoc(), followLoc, speed, generatedPath);
		        lastTargetLoc = followLoc;
		        interpolatingPath = true;*/
		    } else {
		    	if (!interpolatingPath)
					return;
				updateInterpolation();
		    }
		} else {
			if (!interpolatingPath)
				return;
			updateInterpolation();
			if (!interpolatingPath) {
	            Engine.getAgent().sendBroadcast(new ArrivedEventMessage(obj));
	            if (Log.loggingDebug)
	                Log.debug("BaseBehavior.gotoUpdate sending ArrivedEventMessage: oid = " + oid);
	            mode = BaseBehavior.MSG_CMD_TYPE_STOP;
			}
		}
		
	}
	
	protected long setupPathInterpolator(Point myLoc, Point dest, float speed, ArrayList<AOVector> points) {
        long timeNow = System.currentTimeMillis();
        WorldManagerClient.MobPathReqMessage reqMsg = pathState.setupDetourPathInterpolator(timeNow, myLoc, dest, speed, points);
        if (reqMsg != null) {
            try {
                Engine.getAgent().sendBroadcast(reqMsg);
                if (Log.loggingDebug)
                    Log.debug("BaseBehavior.setupPathInterpolator: send MobPathReqMessage " + reqMsg);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
            interpolatingPath = true;
            return pathState.pathTimeRemaining();
        }
        else {
            interpolatingPath = false;
            return 0;
        }
    }
	
	public void updateInterpolation() {
		//Log.debug("PATH: updating interpolation: " + interpolatingPath);
		if (interpolatingPath) {
			long timeNow = System.currentTimeMillis();
		    PathLocAndDir locAndDir = pathState.interpolatePath(timeNow);
		    OID oid = obj.getOid();
		    InterpolatedWorldNode node = (InterpolatedWorldNode) obj.getWorldNode();
		     
		    if (locAndDir == null) {
		        // We have arrived - - turn off interpolation, and cancel that path
		        if (interpolatingPath) {
		        	if (Log.loggingDebug)
		                Log.debug("BaseBehavior.interpolatePath: cancelling path: oid = " + oid + "; myLoc = " + node.getLoc());
		            cancelPathInterpolator(oid);
		            interpolatingPath = false;
		        }
		        	
		        node.setDir(new AOVector(0,0,0));
		    } else {
		        node.setPathInterpolatorValues(timeNow, locAndDir.getDir(), 
		                                                         locAndDir.getLoc(), locAndDir.getOrientation());
			    MobManagerPlugin.getTracker(node.getInstanceOid()).updateEntity((EntityWithWorldNode) obj);
			}
		}
	}
	
	protected void cancelPathInterpolator(OID oid) {
		Log.debug("PATH: cancelling interpolation");
        WorldManagerClient.MobPathReqMessage cancelMsg = new WorldManagerClient.MobPathReqMessage(oid);
        try {
            Engine.getAgent().sendBroadcast(cancelMsg);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	public CrowdAgentParams getParams() {
		return params;
	}
	
	public void addToNavMeshManager(InstanceNavMeshManager navMeshManager, int id) {
		this.navMeshManager = navMeshManager;
		this.agentId = id;
	}
	
	public OID getOid() {
		return oid;
	}
	
	public int getAgentId() {
		return agentId;
	}
	public void setAgentId(int id) {
		agentId = id;
	}
	
	OID oid;
	ObjectStub obj;
	ObjectStub target;
	Point lastTargetLoc;
	Point targetLoc;
	Point lastLoc;
	AOVector lastDir;
	Quaternion lastOrient;
	float speed;
	CrowdAgentParams params;
	InstanceNavMeshManager navMeshManager;
	private int agentId;
	
	// The state of the pathing system for this mob
    PathState pathState = null;
    boolean interpolatingPath = false;
	
	EntityHandle followTarget;
	float distanceToFollowAt = 0;
	protected String mode = BaseBehavior.MSG_CMD_TYPE_STOP;
    protected boolean roamingBehavior = false;
	Long commandSub = null;
	
}