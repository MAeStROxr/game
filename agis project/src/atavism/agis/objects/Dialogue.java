package atavism.agis.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Holds information about a Dialogue (conversation). NPC's can be given dialogues for players
 * to read and make choices based on the actions a Dialogue has.
 * @author Andrew Harrison
 *
 */
public class Dialogue implements Serializable {
    public Dialogue() {
    }
    
    public Dialogue(int id, String name, String text) {
    	this.id = id;
    	this.name = name;
    	this.text = text;
    }
    
    public int getID() { return id;}
    public void setID(int id) {
    	this.id = id;
    }

	public String getName() { return name;}
    public void setName(String name) {
    	this.name = name;
    }
    
    public boolean getOpeningDialogue() { return openingDialogue;}
    public void setOpeningDialogue(boolean openingDialogue) {
    	this.openingDialogue = openingDialogue;
    }
    
    public boolean getRepeatable() { return repeatable;}
    public void setRepeatable(boolean repeatable) {
    	this.repeatable = repeatable;
    }
    
    public int getPrereqDialogue() { return prereqDialogue;}
    public void setPrereqDialogue(int prereqDialogue) {
    	this.prereqDialogue = prereqDialogue;
    }
    
    public int getPrereqQuest() { return prereqQuest;}
    public void setPrereqQuest(int prereqQuest) {
    	this.prereqQuest = prereqQuest;
    }
    
    public int getPrereqFaction() { return prereqFaction;}
    public void setPrereqFaction(int prereqFaction) {
    	this.prereqFaction = prereqFaction;
    }
    
    public int getPrereqFactionStance() { return prereqFactionStance;}
    public void setPrereqFactionStance(int prereqFactionStance) {
    	this.prereqFactionStance = prereqFactionStance;
    }
    
    public String getText() { return text;}
    public void setText(String text) {
    	this.text = text;
    }
    
    public ArrayList<DialogueOption> getOptions() { return options;}
    public void setOptions(ArrayList<DialogueOption> options) {
    	this.options = options;
    }
    public void addOption(String text, String action, int actionID) {
    	DialogueOption option = new DialogueOption();
    	option.text = text;
    	option.action = action;
    	option.actionID = actionID;
    	options.add(option);
    }
    
    int id;
    String name;
    boolean openingDialogue;
    boolean repeatable;
    int prereqDialogue;
    int prereqQuest;
    int prereqFaction;
    int prereqFactionStance;
    boolean reactionAutoStart;
    String text;
    ArrayList<DialogueOption> options = new ArrayList<DialogueOption>();
    
    /**
     * An option a player can choose from this Dialogue.
     * @author Andrew Harrison
     *
     */
    public class DialogueOption {
    	public String text;
    	public String action;
    	public int actionID;
    }
    
    private static final long serialVersionUID = 1L;
}
