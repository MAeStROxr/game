package atavism.agis.objects;

import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The Guild class handles an instance of a guild. It keeps track of the guilds members and ranks.
 * @author Andrew Harrison
 *
 */
public class Guild implements Serializable {
	//private int guildID;
	private String guildName;
	private String faction;
	private ArrayList<GuildRank> ranks;
	private ArrayList<GuildMember> members;
	private String motd;
	private String omotd;
	
	public Guild(String guildName, String faction, ArrayList<String> rankNames, 
			ArrayList<ArrayList<String>> rankPermissions, OID leaderOid, ArrayList<OID> initiates) {
		//this.guildID = guildID;
		this.guildName = guildName;
		this.faction = faction;
		this.ranks = new ArrayList<GuildRank>();
		for (int i = 0; i < rankNames.size(); i++) {
			GuildRank newRank = new GuildRank(rankNames.get(i), rankPermissions.get(i));
			ranks.add(newRank);
		}
		this.members = new ArrayList<GuildMember>();
		GuildMember leader = new GuildMember(leaderOid, 0);
		members.add(leader);
		for (int i = 0; i < initiates.size(); i++) {
			GuildMember initiate = new GuildMember(initiates.get(i), ranks.size()-1);
			members.add(initiate);
		}
		this.motd = "Welcome to the guild.";
		this.omotd = "Welcome to the guild.";
	}
	
	/**
	 * This is the core function of the Guild class. Whenever someone issues a guild command
	 * this function will take it, along with some form of data and then decide which 
	 * function should be run from there.
	 * @param commandType: a string identifying the type of command
	 * @param commandData: data needed to carry out the command
	 */
	public void handleCommand(OID oid, String commandType, Serializable commandData, Serializable commandDataTwo) {
		// Put any commands that don't require permissions before the permission check
		// & make sure you return
		if (commandType.equals("getUpdate")) {
			sendMessageSingle("sendGuildData", oid, null);
			return;
		} else if (commandType.equals("acceptInvite")) {
			//TODO: Some function goes here
			return;
		}
		
		// The permission check
		if (!hasPermission(oid, commandType)) {
			WorldManagerClient.sendObjChatMsg(oid, 1, "You do not have permission to perform that command.");
			return;
		}
		
		if (commandType.equals("addRank")) {
			String rankName = (String) commandData;
			ArrayList<String> permissions = (ArrayList) commandDataTwo;
			addRank(rankName, permissions);
		} else if (commandType.equals("setMotd")) {
			motd = (String) commandData;
			sendMessageAll("newMOTD", null);
		}
	}
	
	/**
	 * This function checks the players rank to see if they can perform the
	 * requested command.
	 * @param oid: the identifier of the player who issued the command
	 * @param command: the command issued by the player
	 * @return
	 */
	private boolean hasPermission(OID oid, String command) {
		int rankNum = -1;
		for (int i = 0; i < members.size(); i++) {
			OID memberOid = members.get(i).oid;
			if (memberOid.equals(oid)) {
				rankNum = members.get(i).rank;
				break;
			}
		}
		
		if (rankNum == -1) {
			Log.error("GUILD: Command issuer has no rank in this guild.");
			return false;
		}
		
		GuildRank rank = ranks.get(rankNum);
		if (rank.permissions.contains(command))
		    return true;
		return false;
	}
	
	private void addRank(String rankName, ArrayList<String> permissions) {
		GuildRank newRank = new GuildRank(rankName, permissions);
		ranks.add(newRank);
		sendMessageAll("rankUpdate", null);
	}
	
	/**
     * Calls sendMessageSingle for each member in the guild.
     * @param msgType: the message type
     * @param data: some form of data to be sent
     */
    private void sendMessageAll(String msgType, Serializable data) {
    	for (int i = 0; i < members.size(); i++) {
    		GuildMember member = members.get(i);
    		if (member.status != 0)
    		    sendMessageSingle(msgType, member.oid, data);
    	}
    }
    
    /**
     * Calls sendMessageSingle for each member in the specified rank.
     * @param msgType: the message type
     * @param rank: the rank to send the message to
     * @param data: some form of data to be sent
     */
    private void sendMessageRank(String msgType, int rank, Serializable data) {
		for (int i = 0; i < members.size(); i++) {
			GuildMember member = members.get(i);
			if (member.status != 0 && member.rank == rank)
			    sendMessageSingle(msgType, member.oid, data);
		}
    }
    
    /**
     * Sends an extension message of the specified type to the specified player
     * @param msgType: the message type
     * @param oid: the oid to send the message to
     * @param data: some form of data to be sent
     */
    private void sendMessageSingle(String msgType, OID oid, Serializable data) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", msgType);
		// Check for type and fill props here
		if (msgType == "sendGuildData") {
			props.put("guildName", guildName);
			props.put("motd", motd);
			props.put("omotd", omotd);
			props.put("numMembers", members.size());
			for (int i = 0; i < members.size(); i++) {
				GuildMember member = members.get(i);
				props.put("memberOid" + i, member.oid);
				//props.put("memberID" + i, member.memberID);
				props.put("memberName" + i, member.name);
				props.put("memberRank" + i, member.rank);
				props.put("memberLevel" + i, member.level);
				props.put("memberZone" + i, member.zone);
				props.put("memberNote" + i, member.note);
				props.put("memberStatus" + i, member.status);
			}
		} else if (msgType == "memberUpdate") {
			Long memberOid = (Long) data;
			for (int i = 0; i < members.size(); i++) {
	    		GuildMember member = members.get(i);
	    		if (memberOid.equals(member.oid)) {
	    			props.put("memberOid" + i, member.oid);
					//props.put("memberID" + i, member.memberID);
					props.put("memberName" + i, member.name);
					props.put("memberRank" + i, member.rank);
					props.put("memberLevel" + i, member.level);
					props.put("memberZone" + i, member.zone);
					props.put("memberNote" + i, member.note);
					props.put("memberStatus" + i, member.status);
	    		}
	    	}
		} else if (msgType == "rankUpdate") {
			props.put("numRanks", ranks.size());
			for (int i = 0; i < ranks.size(); i++) {
	    		GuildRank rank = ranks.get(i);
	    		//props.put("rankNum" + i, rank.rankNum);
	    		props.put("rankName" + i, rank.rankName);
	    		props.put("rankNumPermissions" + i, rank.permissions.size());
	    		for (int j = 0; j < rank.permissions.size(); j++) {
	    			props.put("rankNum" + i + "Permission" + j, rank.permissions.get(j));
	    		}
			}
		} else if (msgType == "newMOTD") {
			props.put("motd", motd);
		}
		
		/*
		 * These probably aren't needed
		if (msgType == "LoggedOn") {
			props.put("loggedOnMember", data);
		} else if (msgType == "LoggedOff") {
			props.put("loggedOffMember", data);
		}*/
		
		TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, oid, 
				oid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }

	
	private class GuildRank implements Serializable {
		//protected int rankNum;
		protected String rankName;
		protected ArrayList<String> permissions;
		// Some kind of enum for rank permissions
		
		public GuildRank(String rankName, ArrayList<String> permissions) {
			//this.rankNum = rankNum;
			this.rankName = rankName;
			this.permissions = permissions;
		}
		
		public void setRankName(String rankName) { this.rankName = rankName; }
		public String getRankName() { return rankName; }
		
		public void setPermissions(ArrayList<String> permissions) { this.permissions = permissions; }
		public ArrayList<String> getPermissions() { return permissions; }
		
		private static final long serialVersionUID = 1L;
	}
	
	private class GuildMember implements Serializable {
		protected OID oid;
		//protected int memberID;
		protected String name;
		protected int rank;
		protected int level;
		protected String zone;
		protected String note;
		protected int status; // 0: Offline; 1: Online; 2: AFK?
		// This variable, when set to true, allows guild members to see
		// when the player is on another character on the same account
		//protected boolean altNotify;
		
		public GuildMember(OID oid, int rank) {
			this.oid = oid;
			//this.memberID = memberID;
			this.name = WorldManagerClient.getObjectInfo(oid).name;
			this.rank = rank;
			this.zone = (String) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "zone");
			this.note = "";
			this.status = 1;
			//this.altNotify = false;
		}
		
		public void setOid(OID oid) { this.oid = oid; }
		public OID getOid() { return oid; }
		
		public void setName(String name) { this.name = name; }
		public String getName() { return name; }
		
		public void setRank(int rank) { this.rank = rank; }
		public int getRank() { return rank; }
		
		public void setLevel(int level) { this.level = level; }
		public int getLevel() { return level; }
		
		public void setZone(String zone) { this.zone = zone; }
		public String getZone() { return zone; }
		
		public void setNote(String note) { this.note = note; }
		public String getNote() { return note; }
		
		public void setStatus(int status) { this.status = status; }
		public int getStatus() { return status; }
		
		private static final long serialVersionUID = 1L;
	}
	
	public void setGuildName(String guildName) { this.guildName = guildName; }
	public String getGuildName() { return guildName; }
	
	public void setFaction(String faction) { this.faction = faction; }
	public String getFaction() { return faction; }
	
	public void setRanks(ArrayList<GuildRank> ranks) { this.ranks = ranks; }
	public ArrayList<GuildRank> getRanks() { return ranks; }
	
	public void setMembers(ArrayList<GuildMember> members) { this.members = members; }
	public ArrayList<GuildMember> getMembers() { return members; }
	
	public void setMOTD(String motd) { this.motd = motd; }
	public String getMOTD() { return motd; }
	
	public void setOMOTD(String omotd) { this.omotd = omotd; }
	public String getOMOTD() { return omotd; }
	
	private static final long serialVersionUID = 1L;
}