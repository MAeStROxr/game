package atavism.agis.objects;

import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.pathing.crowd.Crowd;
import atavism.server.pathing.crowd.CrowdAgentDebugInfo;
import atavism.server.pathing.detour.*;
import atavism.server.pathing.recast.Helper;
import atavism.server.pathing.recast.NavMeshParamXmlLoader;
import atavism.server.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

public class InstanceNavMeshManager implements Runnable{
	
	public InstanceNavMeshManager(String instanceName, OID instanceOid) {
		this.instanceOid = instanceOid;
		loadWorldNavMesh(instanceName);
	}
	
	public boolean loadWorldNavMesh(String name)
    {
    	String navMeshFilePath = "../navmesh/" + name + "/" + name + ".xml";
        NavMeshParamXmlLoader loader = new NavMeshParamXmlLoader(navMeshFilePath);
        boolean navMeshLoaded = loader.load(navMesh);
        if (navMeshLoaded) {
        	navMeshQuery = new NavMeshQuery();
            navMeshQuery.Init(navMesh, 2048);
            filter = new QueryFilter();
            filter.IncludeFlags = 15;
            filter.ExcludeFlags = 0;
            filter.SetAreaCost(1, 1.0f);
            filter.SetAreaCost(2, 10.0f);
            filter.SetAreaCost(3, 1.0f);
            filter.SetAreaCost(4, 1.0f);
            filter.SetAreaCost(5, 2);
            filter.SetAreaCost(6, 1.5f);
            crowd = new Crowd();
            crowd.setFilter(filter);
            crowd.Init(5000, 0.5f, navMesh);
            lastUpdate = System.currentTimeMillis();
            Engine.getExecutor().scheduleAtFixedRate(this, 500, 250, TimeUnit.MILLISECONDS);
        } else {
        	Log.debug("NAVMESH: navMesh not loaded for instance: " + name);
        }
        return navMeshLoaded;
    }
	
	@Override
	public void run() {
		Log.debug("NAVMESH: running Update for instance: " + instanceOid);
		try {
		
			// First add all actors to add and remove all actors to remove
			for (DetourActor actor : actorsToAdd) {
				actors.add(actor);
				Log.debug("DETOUR: added Actor: " + actor.getAgentId());
			}
			actorsToAdd.clear();
			for (DetourActor actor : actorsToRemove) {
				crowd.RemoveAgent(actor.getAgentId());
				actors.remove(actor);
			}
			actorsToRemove.clear();
			// Work out time since last update and then run the update
			float timeDif = (float)(System.currentTimeMillis() - lastUpdate) / 1000f;
			CrowdAgentDebugInfo info = new CrowdAgentDebugInfo();
			crowd.Update(timeDif, info);
			lastUpdate = System.currentTimeMillis();
			//Log.debug("NAVMESH: running Update complete, moving onto Actors");
			for (DetourActor actor : actors) {
				if (actor == null || crowd.GetAgent(actor.getAgentId()) == null)
					continue;
				//Log.debug("NAVMESH: running update for actor: " + actor.getAgentId());
				float[] pos = crowd.GetAgent(actor.getAgentId()).npos;
				float[] vel = crowd.GetAgent(actor.getAgentId()).vel;
				//Log.debug("NAVMESH: running updateDirLoc with vel: " + vel[0] + "," + vel[1] + "," + vel[2]);
				actor.updateDirLoc(new AOVector(vel[0], vel[1], vel[2]), new Point(pos[0], pos[1], pos[2]));
			}
		
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			Log.error("ERROR: caught exception in InstanceMavMesh.Update: " + sw.toString());
		}
		Log.debug("NAVMESH: completed actor update");
	}
	
	public void addActor(OID actorOid, Point loc, DetourActor actor) {
		if (navMeshQuery == null) {
			Log.debug("DETOUR: no navMeshQuery so actor not added.");
			return;
		}
		actorsToAdd.add(actor);
		float[] pos = new float[3];
		pos[0] = loc.getX();
		pos[1] = loc.getY();
		pos[2] = loc.getZ();
		actor.addToNavMeshManager(this, crowd.AddAgent(pos, actor.getParams()));
		actor.activate();
		Log.debug("DETOUR: added Actor to addList: " + actorOid);
	}
	
	public void setActorTarget(OID actorOid, Point loc) {
		DetourActor actor = getDetourActorByOid(actorOid);
		if (actor != null) {
			float[] endPos = { loc.getX(), loc.getY(), loc.getZ() };
            float[] extents = { 4f, 4f, 4f };
            float[] nearestPt = new float[3];
            long endRef = navMeshQuery.FindNearestPoly(endPos, extents, filter, nearestPt).longValue;
            crowd.RequestMoveTarget(actor.getAgentId(), endRef, nearestPt);
		}
	}
	
	public void resetActorTarget(OID actorOid) {
		DetourActor actor = getDetourActorByOid(actorOid);
		if (actor != null) {
            crowd.ResetMoveTarget(actor.getAgentId());
		}
	}
	
	public void setActorSpeed(OID actorOid, float speed) {
		DetourActor actor = getDetourActorByOid(actorOid);
		if (actor != null) {
			crowd.GetAgent(actor.getAgentId()).DesiredSpeed = speed;
		}
	}
	
	public void removeActor(OID actorOid) {
		Log.debug("DETOUR: remove actor: " + actorOid);
		DetourActor actor = getDetourActorByOid(actorOid);
		if (actor != null) {
			actor.deactivate();
			actorsToRemove.add(actor);
		}
	}
	
	DetourActor getDetourActorByOid(OID actorOid) {
		for (int i = 0; i < actors.size(); i++) {
			if (actors.get(i).getOid().equals(actorOid))
				return actors.get(i);
		}
		return null;
	}
	
	/*
	 * Path generation
	 */
	
	public ArrayList<AOVector> GeneratePath(Point startPosition, Point endPosition)
    {
		int maxPolys = 256;
        float[] startPos = new float[] { startPosition.getX(), startPosition.getY(), startPosition.getZ() };
        float[] endPos = new float[]{ endPosition.getX(), endPosition.getY(), endPosition.getZ() };
        float[] extents = new float[]{ 2f, 4f, 2f };
        float[] nearestPoint = null;
        DetourNumericReturn numericReturn = navMeshQuery.FindNearestPoly(startPos, extents, filter, nearestPoint);
        long startRef = numericReturn.longValue;
        //Log.debug("PATH: Found start position status: " + numericReturn.status + ", Ref " + startRef + ", pos " + startPos[0] + "," + startPos[1] + "," + startPos[2]);
        numericReturn = navMeshQuery.FindNearestPoly(endPos, extents, filter, nearestPoint);
        long endRef = numericReturn.longValue;
        //Log.debug("PATH: Found end position status: " + numericReturn.status + ", Ref " + endRef + ", pos " + endPos[0] + "," + endPos[1] + "," + endPos[2]);
        long[] path = new long[maxPolys];
        DetourStatusReturn statusReturn = navMeshQuery.FindPath(startRef, endRef, startPos, endPos, filter, path, 256);
        int polyCount = statusReturn.intValue;
        if (polyCount < 1) {
        	return null;
        }
        
        long[] smoothPolys = new long[maxPolys];
        System.arraycopy(path, 0, smoothPolys, 0, polyCount);
        int smoothPolyCount = polyCount;
        float[] smoothPath = new float[2048 * 3];

        float[] iterPos = new float[3], targetPos = new float[3];
        navMeshQuery.ClosestPointOnPoly(startRef, startPos, iterPos, false);
        //navMeshQuery.ClosestPointOnPoly(smoothPolys[smoothPolyCount - 1], endPos, targetPos, false);
        targetPos = endPos;
        
        float StepSize = 0.5f;
        float Slop = 0.01f;

        int smoothPathNum = 0;
        System.arraycopy(iterPos, 0, smoothPath, smoothPathNum * 3, 3);
        smoothPathNum++;
        
        /*for (int i = 0; i < smoothPolyCount; i++) {
			navMeshQuery.ClosestPointOnPoly(smoothPolys[i], startPos, iterPos, false);
			System.arraycopy(iterPos, 0, smoothPath, i * 3, 3);
			smoothPathNum++;
        }*/
        
        if (smoothPolyCount == 2) {
        	// Try send the direct path
        	ArrayList<AOVector> pathPoints = new ArrayList<AOVector>();
        	pathPoints.add(new AOVector(iterPos[0], iterPos[1], iterPos[2]));
        	pathPoints.add(new AOVector(targetPos[0], targetPos[1], targetPos[2]));
            
            return pathPoints;
        }
        
        //Log.debug("PATH: smoothPolyCount = " + smoothPolyCount);
        // Move towards target a small advancement at a time until target reached or
    	// when ran out of memory to store the path.	
        while (smoothPolyCount > 0 && smoothPathNum < 2048)
        {
        	// Find location to steer towards.
            float[] steerPos = new float[3];
            
            numericReturn = GetSteerTarget(navMeshQuery, iterPos, targetPos, Slop, smoothPolys, smoothPolyCount, steerPos);
            //Log.debug("PATH: steerTarget = " + numericReturn.boolValue);

            if (!numericReturn.boolValue)
                break;
            short steerPosFlag = (short) numericReturn.intValue;
            long steerPosRef = numericReturn.longValue;

            boolean endOfPath = (steerPosFlag & StraightPathEnd) != 0;
            boolean offMeshConnection = (steerPosFlag & StraightPathOffMeshConnection) != 0;

            // Find movement delta.
            float[] delta = Helper.VSub(steerPos[0], steerPos[1], steerPos[2], iterPos[0], iterPos[1], iterPos[2]);
            float len = (float)Math.sqrt(Helper.VDot(delta, delta));

        	// If the steer target is end of path or off-mesh link, do not move past the location.
            if ((endOfPath || offMeshConnection) && len < StepSize)
                len = 1;
            else
            {
                len = StepSize / len;
            }

            float[] moveTarget = new float[3];
            Helper.VMad(moveTarget, iterPos, delta, len);

            // Move
            float[] result = new float[3];
            long[] visited = new long[16];

            statusReturn = navMeshQuery.MoveAlongSurface(smoothPolys[0], iterPos, moveTarget, filter, result, visited, 16);
            int nVisited = statusReturn.intValue;
            
            smoothPolyCount = FixupCorridor(smoothPolys, smoothPolyCount, maxPolys, visited, nVisited);
            smoothPolyCount = FixupShortcuts(smoothPolys, smoothPolyCount, navMeshQuery);
            
            numericReturn = navMeshQuery.GetPolyHeight(smoothPolys[0], result);
            float h = numericReturn.floatValue;
            result[1] = h;
            System.arraycopy(result, 0, iterPos, 0, 3);

        	// Handle end of path and off-mesh links when close enough.
            if (endOfPath && InRange(iterPos, steerPos, Slop, 1.0f))
            {
            	// Reached end of path.
            	System.arraycopy(targetPos, 0, iterPos, 0, 3);
                if (smoothPathNum < 2048)
                {
                	System.arraycopy(iterPos, 0, smoothPath, smoothPathNum * 3, 3);
                	smoothPathNum++;
                }
                break;
            }
            else if (offMeshConnection && InRange(iterPos, steerPos, Slop, 1.0f))
            {
            	// Reached off-mesh connection.
                float[] startPosOffMesh = new float[3], endPosOffMesh = new float[3];
                
            	// Advance the path up to and over the off-mesh connection.
                long prevRef = 0, polyRef = smoothPolys[0];
                int npos = 0;
                while (npos < smoothPolyCount && polyRef != steerPosRef)
                {
                    prevRef = polyRef;
                    polyRef = smoothPolys[npos];
                    npos++;
                }
                for (int i = npos; i < smoothPolyCount; i++)
                {
                    smoothPolys[i - npos] = smoothPolys[i];
                }
                smoothPolyCount -= npos;

                // Handle the connection.
                EnumSet<Status> status = navMeshQuery.NavMesh.GetOffMeshConnectionPolyEndPoints(prevRef, polyRef, startPosOffMesh, endPosOffMesh);
                if (status.contains(Status.Success))
                {
                    if (smoothPathNum < MaxSmooth)
                    {
                    	System.arraycopy(startPosOffMesh, 0, smoothPath, smoothPathNum*3, 3);
                        smoothPathNum++;
                    	// Hack to make the dotted path not visible during off-mesh connection.
                        if ((smoothPathNum & 1) == 1)
                        {
                        	System.arraycopy(startPosOffMesh, 0, smoothPath, smoothPathNum*3, 3);
                            smoothPathNum++;
                        }
                    }
                	// Move position at the other side of the off-mesh link.
                    System.arraycopy(endPosOffMesh, 0, iterPos, 0, 3);
                    numericReturn = navMeshQuery.GetPolyHeight(smoothPolys[0], iterPos);
                    iterPos[1] = numericReturn.floatValue;
                }
            }

            // Store results.
            if (smoothPathNum < 2048)
            {
            	System.arraycopy(iterPos, 0, smoothPath, smoothPathNum * 3, 3);
                smoothPathNum++;
            }
        }
        
        // Put path together
        Log.debug("PATH: num smooth path: " + smoothPathNum);
        ArrayList<AOVector> pathPoints = new ArrayList<AOVector>();
        AOVector lastDirection = AOVector.Zero;
        AOVector prev = new AOVector(smoothPath[0], smoothPath[1], smoothPath[2]);
        for (int i = 1; i < smoothPathNum; i++)
        {
        	AOVector currentPoint = new AOVector(smoothPath[i * 3 + 0], smoothPath[i * 3 + 1] + 0.1f, smoothPath[i * 3 + 2]);
        	AOVector currentDirection = AOVector.sub(currentPoint, prev).normalize();
			float dif = AOVector.sub(currentDirection, lastDirection).length();  
			prev = currentPoint;
			lastDirection = currentDirection;
        	
        	if (Math.abs(dif) > 0.01f) {
        		pathPoints.add(new AOVector(smoothPath[i * 3 - 3], smoothPath[i * 3 - 2], smoothPath[i * 3 - 1]));
        	}
        	//Log.debug("PATH: adding smooth path: " + i);
        }
        pathPoints.add(new AOVector(smoothPath[smoothPathNum * 3 - 3], smoothPath[smoothPathNum * 3 - 2], smoothPath[smoothPathNum * 3 - 1]));
        // HACK - remove second point
        if (pathPoints.size() > 3) {
        	pathPoints.remove(1);
        }
        
        /*for (int i = 1; i < smoothPathNum; i++)
        {
        	pathPoints.add(new AOVector(smoothPath[i * 3 + 0], smoothPath[i * 3 + 1], smoothPath[i * 3 + 2]));
        	//Log.debug("PATH: adding smooth path: " + i);
        }*/
        
        return pathPoints;
    }
	
	private int FixupCorridor(long[] path, int npath, int maxPath, long[] visited, int nVisited)
    {
        int furthestPath = -1;
        int furthestVisited = -1;

    	// Find furthest common polygon.
        for (int i = npath - 1; i >= 0; --i)
        {
            boolean found = false;
            for (int j = nVisited - 1; j >= 0; --j)
            {
                if (path[i] == visited[j])
                {
                    furthestPath = i;
                    furthestVisited = j;
                    found = true;
                }
            }
            if (found)
                break;
        }

    	// If no intersection found just return current path.
        if (furthestPath == -1 || furthestVisited == -1)
            return npath;

        // Concatenate paths.	
		
		// Adjust beginning of the buffer to include the visited.
        int req = nVisited - furthestVisited;
        int orig = Math.min(furthestPath + 1, npath);
        int size = Math.max(0, npath - orig);
        if (req + size > maxPath)
            size = maxPath - req;
        if (size > 0)
        	System.arraycopy(path, orig, path, req, size);

        // Store visited
        for (int i = 0; i < req; i++)
        {
            path[i] = visited[(nVisited - 1) - i];
        }

        return req + size;
    }
	
	// This function checks if the path has a small U-turn, that is,
	// a polygon further in the path is adjacent to the first polygon
	// in the path. If that happens, a shortcut is taken.
	// This can happen if the target (T) location is at tile boundary,
	// and we're (S) approaching it parallel to the tile edge.
	// The choice at the vertex can be arbitrary, 
	//  +---+---+
	//  |:::|:::|
	//  +-S-+-T-+
	//  |:::|   | <-- the step can end up in here, resulting U-turn path.
	//  +---+---+
	private int FixupShortcuts(long[] path, int npath, NavMeshQuery navQuery) {
		if (npath < 3)
			return npath;
			
		// Get connected polygons
		int maxNeis = 16;
		long[] neis = new long[maxNeis];
		int nneis = 0;
		
		
		DetourMeshTileAndPoly tileAndPoly = navQuery.NavMesh.GetTileAndPolyByRef(path[0]);
		if (tileAndPoly.status.contains(Status.Failure))
			return npath;
		MeshTile tile = tileAndPoly.tile;
		Poly poly = tileAndPoly.poly;
		
		for (long k = poly.FirstLink; k != NavMesh.NullLink; k = tile.Links[(int)k].Next) {
			Link link = tile.Links[(int)k];
			if (link.Ref != 0) 
			{
				if (nneis < maxNeis)
					neis[nneis++] = link.Ref;
			}
		}
			
		// If any of the neighbour polygons is within the next few polygons
		// in the path, short cut to that polygon directly.
		int maxLookAhead = 6;
		int cut = 0;
		for (int i = Math.min(maxLookAhead, npath) - 1; i > 1 && cut == 0; i--) {
			for (int j = 0; j < nneis; j++)
			{
				if (path[i] == neis[j]) {
					cut = i;
					break;
				}
			}
		}
		if (cut > 1)
		{
			int offset = cut-1;
			npath -= offset;
			for (int i = 1; i < npath; i++)
				path[i] = path[i+offset];
		}
		
		return npath;
	}
	
	/// <summary>
    /// Helper class for using an array instead of splitting out v1
    /// </summary>
    /// <param name="v1">array of 3 points for vector 1</param>
    /// <param name="v2">array of 3 points for vector 2</param>
    /// <param name="r">radius around which to search</param>
    /// <param name="h">height above or below to check for</param>
    /// <returns></returns>
    private boolean InRange(float[] v1, float[] v2, float r, float h)
    {
        return InRange(v1[0], v1[1], v1[2], v2, r, h);
    }
    
    /// <summary>
    /// Checking if V1 is in range of V2
    /// </summary>
    /// <param name="v1x">V1 x-component</param>
    /// <param name="v1y">V1 y-component</param>
    /// <param name="v1z">V1 z-component</param>
    /// <param name="v2">Vector to check with</param>
    /// <param name="r">radius around v1 to check</param>
    /// <param name="h">height above and below v1 to check</param>
    /// <returns></returns>
    private boolean InRange(float v1x, float v1y, float v1z, float[] v2, float r, float h)
    {
        float dx = v2[0] - v1x;
        float dy = v2[1] - v1y;
        float dz = v2[2] - v1z;
        return (dx * dx + dz * dz) < r * r && Math.abs(dy) < h;
    }
    
  /// <summary>
    /// Tries to find the straightest path between 2 polygons
    /// </summary>
    /// <param name="navMeshQuery"></param>
    /// <param name="startPos"></param>
    /// <param name="endPos"></param>
    /// <param name="minTargetDistance"></param>
    /// <param name="path"></param>
    /// <param name="pathSize"></param>
    /// <param name="steerPos"></param>
    /// <param name="steerPosFlag"></param>
    /// <param name="steerPosRef"></param>
    /// <param name="outPoints"></param>
    /// <param name="outPointCount"></param>
    /// <returns></returns>
    private DetourNumericReturn GetSteerTarget(NavMeshQuery navMeshQuery, float[] startPos, float[] endPos, float minTargetDistance, long[] path, int pathSize, float[] steerPos)
    {
    	// Find steer target.
    	DetourNumericReturn numericReturn = new DetourNumericReturn();
        int MaxSteerPoints = 3;
        float[] steerPath = new float[MaxSteerPoints * 3];
        short[] steerPathFlags = new short[MaxSteerPoints];
        long[] steerPathPolys = new long[MaxSteerPoints];

        DetourStatusReturn statusReturn = navMeshQuery.FindStraightPath(startPos, endPos, path, pathSize, steerPath, steerPathFlags,
                                      steerPathPolys, MaxSteerPoints);
        int nSteerPath = statusReturn.intValue;

        if (nSteerPath == 0) {
        	numericReturn.boolValue = false;
            return numericReturn;
        }

    	// Find vertex far enough to steer to.
        int ns = 0;
        while (ns < nSteerPath)
        {
        	// Stop at Off-Mesh link or when point is further than slop away.
            if ((steerPathFlags[ns] & StraightPathOffMeshConnection) != 0 ||
                !InRange(steerPath[ns * 3 + 0], steerPath[ns * 3 + 1], steerPath[ns * 3 + 2], startPos, minTargetDistance, 1000.0f))
                break;
            ns++;
        }

    	// Failed to find good point to steer to.
        if (ns >= nSteerPath) {
        	numericReturn.boolValue = false;
            return numericReturn;
        }

        System.arraycopy(steerPath, ns * 3, steerPos, 0, 3);
        steerPos[1] = startPos[1];
        numericReturn.intValue = steerPathFlags[ns];
        numericReturn.longValue = steerPathPolys[ns];

        numericReturn.boolValue = true;
        return numericReturn;
    }
	
	public OID getInstanceOid() {
		return instanceOid;
	}
	
	private ArrayList<DetourActor> actors = new ArrayList<DetourActor>();
	private ArrayList<DetourActor> actorsToAdd = new ArrayList<DetourActor>();
	private ArrayList<DetourActor> actorsToRemove = new ArrayList<DetourActor>();
	
	transient protected Lock lock = null;
	
	private OID instanceOid;
	private NavMesh navMesh = new NavMesh();
    private NavMeshQuery navMeshQuery;
    private QueryFilter filter;
    private Crowd crowd;
    private long lastUpdate;
	
    private static final int StraightPathEnd = 2;
    private static final int StraightPathOffMeshConnection = 4;
    private static final int MaxSmooth = 2048;
}