package atavism.agis.objects;

import atavism.agis.core.Agis;
import atavism.agis.plugins.AgisInventoryPlugin;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.engine.Engine;
import atavism.server.engine.Namespace;
import atavism.server.engine.OID;
import atavism.server.objects.Entity;
import atavism.server.objects.ObjectType;
import atavism.server.objects.ObjectTypes;
import atavism.server.util.Log;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Information related to the inventory system. Any object that wants to carry items or
 * currency requires one of these.
 */
public class InventoryInfo extends Entity {
	public InventoryInfo() {
		super();
		setNamespace(Namespace.BAG);
	}

	public InventoryInfo(OID objOid) {
		super(objOid);
		setNamespace(Namespace.BAG);
	}

    public String toString() {
        return "[Entity: " + getName() + ":" + getOid() + "]";
    }

    public ObjectType getType() {
        return ObjectTypes.bag;
    }
	
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	private int id;
	
	/**
	 * Category control
	 * Each category has its own collection of items.
	 */
	public int getCurrentCategory() {
		return currentCategory;
	}
	public void setCurrentCategory(int category) {
		Log.debug("QSI: setting current category to: " + category + " from: " + currentCategory);
		this.currentCategory = category;
	}
	public boolean categoryUpdated(int category) {
		boolean createInventory = false;
		if (!bags.containsKey(category)) {
			//bags.put(category, new OID[numBags]);
			createInventory = true;
		}
		if (!currencies.containsKey(category))
    		currencies.put(category, new HashMap<Integer, Integer>());
		this.currentCategory = category;
		Engine.getPersistenceManager().setDirty(this);
		return createInventory;
	}
	protected int currentCategory;
	
	/*
	 * Currencies
	 */
	
	public int getCurrencyAmount(int currencyID, boolean includeConversions) {
		Currency c = Agis.CurrencyManager.get(currencyID);
		if (getCurrencies(currentCategory).containsKey(currencyID)) {
			//TODO: run external check, if its an external currency then query the account db
			int amount = getCurrencies(currentCategory).get(currencyID);
			// Check if there is a parent currency, if so, that needs added to the amount
			int conversionCurrency = c.getAutoConversionCurrency();
			if (conversionCurrency > 0 && includeConversions) {
				amount += getCurrencyAmount(conversionCurrency, true) * c.getConversionAmountReq(conversionCurrency);
			}
			return amount;
		} else {
			// Player does not yet have this currency, lets create it
			addCurrency(currencyID, 0);
			return 0;
		}
	}
	
	/**
	 * Alters the amount of the specified currency the player by the specified amount. Due to the
	 * use of subCurrencies this may alter more than one currency.
	 * @param currencyID
	 * @param delta
	 */
	public boolean alterCurrencyAmount(int currencyID, int delta) {
		int currentAmount = getCurrencyAmount(currencyID, false);
		currentAmount += delta;
		Currency c = Agis.CurrencyManager.get(currencyID);
		// First check against the maximum
		int conversionCurrency = c.getAutoConversionCurrency();
		if (conversionCurrency != -1 && currentAmount >= c.getConversionAmountReq(conversionCurrency)) {
			int newCurrencyAlteration = currentAmount / c.getConversionAmountReq(conversionCurrency);
			alterCurrencyAmount(conversionCurrency, newCurrencyAlteration);
			currentAmount = currentAmount % c.getConversionAmountReq(conversionCurrency);
		} else if (conversionCurrency != -1 && currentAmount < 0) {
			// Reduce the higher up currency as this one is now below 0
			int newCurrencyAlteration = (int) Math.floor((float)currentAmount / (float)c.getConversionAmountReq(conversionCurrency));
			alterCurrencyAmount(conversionCurrency, newCurrencyAlteration/*-1*/);
			int leftOver = Math.abs(currentAmount % c.getConversionAmountReq(conversionCurrency));
			if (leftOver > 0)
				currentAmount = c.getConversionAmountReq(conversionCurrency) - leftOver;
			else
				currentAmount = 0;
		}
		
		if (currentAmount > c.maximumAmount) {
			currentAmount = c.maximumAmount;
		} else if (currentAmount < 0) {
			currentAmount = 0;
		}
		
		getCurrencies(currentCategory).put(currencyID, currentAmount);
		// Return true to show the currency was increased
		return true;
	}

	public void addCurrency(int currencyID, int amount) {
		if (Log.loggingDebug) {
			Log.debug("InventoryInfo.addCurrency: adding currency=" + currencyID + " to obj=" + this);
        }
        lock.lock();
        try {
            if (getCurrencies(currentCategory).containsKey(currencyID)) {
                return;
            }
            getCurrencies(currentCategory).put(currencyID, amount);
            Engine.getPersistenceManager().setDirty(this);
        } finally {
            lock.unlock();
        }
	}
	public void removeCurrency(int currencyID) {
		if (Log.loggingDebug) {
			Log.debug("InventoryInfo.removeCurrency: removing currency=" + currencyID + " from obj=" + this);
        }
        lock.lock();
        try {
        	getCurrencies(currentCategory).remove(currencyID);
            Engine.getPersistenceManager().setDirty(this);
            ExtendedCombatMessages.sendCurrencies(this.getOid(), getCurrencies(currentCategory));
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, Integer> getCurrentCurrencies() {
        lock.lock();
        try {
        	Log.debug("II: currencies: " + currencies);
            return getCurrencies(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void setCurrentCurrencies(HashMap<Integer, Integer>currencies) {
        lock.lock();
        try {
            this.currencies.put(currentCategory, new HashMap<Integer, Integer>(currencies));
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, HashMap<Integer, Integer>> getCurrencies() {
        lock.lock();
        try {
        	Log.debug("II: currencies: " + currencies);
            return new HashMap<Integer, HashMap<Integer, Integer>>(currencies);
        } finally {
            lock.unlock();
        }
    }
	public void setCurrencies(HashMap<Integer, HashMap<Integer, Integer>>currencies) {
        lock.lock();
        try {
            this.currencies = new HashMap<Integer, HashMap<Integer, Integer>>(currencies);
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, Integer> getCurrencies(int category) {
        lock.lock();
        try {
        	Log.debug("II: currencies: " + currencies);
        	if (!currencies.containsKey(category))
        		currencies.put(category, new HashMap<Integer, Integer>());
            return currencies.get(category);
        } finally {
            lock.unlock();
        }
    }
	
	private HashMap<Integer, HashMap<Integer, Integer>> currencies = new HashMap<Integer, HashMap<Integer, Integer>>();
	
	/*
	 * Standard Inventory Bags
	 */

	public OID[] getBags() {
        lock.lock();
        try {
        	Log.debug("II: bags: " + bags.get(currentCategory) + " from current category: " + currentCategory);
            return getBags(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void setBags(OID bags[]) {
        lock.lock();
        try {
        	Log.debug("II: setting bags for currentCategory: " + currentCategory);
            this.bags.put(currentCategory, bags);
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, OID[]> getBagsMap() {
        lock.lock();
        try {
            return new HashMap<Integer, OID[]>(bags);
        } finally {
            lock.unlock();
        }
    }
	public void setBagsMap(HashMap<Integer, OID[]> rootBags) {
        lock.lock();
        try {
            this.bags = new HashMap<Integer, OID[]>(rootBags);
        } finally {
            lock.unlock();
        }
	}
	public OID[] getBags(int category) {
        lock.lock();
        try {
        	if (!bags.containsKey(category))
        		bags.put(category, new OID[AgisInventoryPlugin.INVENTORY_BAG_COUNT]);
            return bags.get(category);
        } finally {
            lock.unlock();
        }
    }
	
	private HashMap<Integer, OID[]> bags = new HashMap<Integer, OID[]>();
	
	/*
	 * Equipped Items
	 */
	public OID getEquipmentItemBag() {
        lock.lock();
        try {
        	Log.debug("II: bags: " + equippedItemsBag.get(currentCategory) + " from current category: " + currentCategory);
            return getEquippedItemsBag(currentCategory);
        } finally {
            lock.unlock();
        }
    }
	public void setEquipmentItemBag(OID bag) {
        lock.lock();
        try {
        	Log.debug("II: setting bags for currentCategory: " + currentCategory);
            this.equippedItemsBag.put(currentCategory, bag);
        } finally {
            lock.unlock();
        }
	}
	public HashMap<Integer, OID> getEquippedItemsBagMap() {
        lock.lock();
        try {
            return new HashMap<Integer, OID>(equippedItemsBag);
        } finally {
            lock.unlock();
        }
    }
	public void setEquippedItemsBagMap(HashMap<Integer, OID> rootBags) {
        lock.lock();
        try {
            this.equippedItemsBag = new HashMap<Integer, OID>(rootBags);
        } finally {
            lock.unlock();
        }
	}
	public OID getEquippedItemsBag(int category) {
        lock.lock();
        try {
        	if (!equippedItemsBag.containsKey(category))
        		equippedItemsBag.put(category, null);
            return equippedItemsBag.get(category);
        } finally {
            lock.unlock();
        }
    }
	
	private HashMap<Integer, OID> equippedItemsBag = new HashMap<Integer, OID>();
	
	public void addMail(Mail m) {
		mail.add(m);
		Engine.getPersistenceManager().setDirty(this);
	}
	public ArrayList<Mail> getMail() {
		return new ArrayList<Mail>(mail);
	}
	public void setMail(ArrayList<Mail> mail) {
		this.mail = new ArrayList<Mail>(mail);
	}
	private ArrayList<Mail> mail = new ArrayList<Mail>();
	
	/*
	 * Final Static properties
	 */

	private static final long serialVersionUID = 1L;
	public static final int equipmentBag = 4;
}
