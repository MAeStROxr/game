package atavism.agis.objects;

import atavism.server.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class LootTable {
    public LootTable() {
    }
    
    public LootTable(int id, String name, ArrayList<Integer> items, ArrayList<Integer> itemCounts, ArrayList<Integer> itemChances) {
    	this.id = id;
    	this.name = name;
    	this.items = items;
    	this.itemCounts = itemCounts;
    	this.itemChances = itemChances;
    }
    
    public int getID() { return id;}
    public void setID(int id) {
    	this.id = id;
    }
    
    public String getName() { return name;}
    public void setName(String name) {
    	this.name = name;
    }
    
    public ArrayList<Integer> getItems() { return items;}
    public void setItems(ArrayList<Integer> items) {
    	this.items = items;
    }
    public ArrayList<Integer> getItemCounts() { return itemCounts;}
    public void setItemCounts(ArrayList<Integer> itemCounts) {
    	this.itemCounts = itemCounts;
    }
    public ArrayList<Integer> getItemChances() { return itemChances;}
    public void setItemChances(ArrayList<Integer> itemChances) {
    	this.itemChances = itemChances;
    }
    
    public int getTotalRollChance() {
    	int totalRollChance = 0;
    	for (int i = 0; i < itemChances.size(); i++) {
    		totalRollChance += itemChances.get(i);
    	}
    	return totalRollChance;
    }
    
    public int getRandomItemNum() {
    	Random rand = new Random();
    	int roll = rand.nextInt(getTotalRollChance());
    	int currentTotal = 0;
    	Log.debug("LOOT: generating random number for table: " + id + ". Roll is: " + roll + "; with " + itemChances.size() + " items");
    	for (int i = 0; i < itemChances.size(); i++) {
    		currentTotal += itemChances.get(i);
    		Log.debug("LOOT: currentTotal for itemChance: " + i + " is: "+ currentTotal);
    		if (currentTotal >= roll)
    			return i;
    	}
    	return -1;
    }
    
    public String toString() {
    	return id + ", " + name;
    }

    int id;
    String name;
    ArrayList<Integer> items = new ArrayList<Integer>();
    ArrayList<Integer> itemCounts = new ArrayList<Integer>();
    ArrayList<Integer> itemChances = new ArrayList<Integer>();

    private static final long serialVersionUID = 1L;
}
