package atavism.agis.objects;

import atavism.server.engine.OID;

import java.io.Serializable;
import java.util.ArrayList;

public class Mail implements Serializable {
    public Mail() {
    }
    
    public Mail(int id, OID recipientOID, String recipientName, OID senderOID, String senderName, String subject,
    		String message, int currencyType, int currencyAmount, ArrayList<OID> items, int category, boolean CoD) {
    	this.id = id;
    	this.recipientOID = recipientOID;
    	this.recipientName = recipientName;
    	this.senderOID = senderOID;
    	this.senderName = senderName;
    	this.subject = subject;
    	this.message = message;
    	this.currencyType = currencyType;
    	this.currencyAmount = currencyAmount;
    	this.items = items;
    	this.CoD = CoD;
    	mailRead = false;
    	mailArchive = false;
    }
    
    public void addItem(OID itemOid, int pos) {
    	if (pos < items.size())
    		items.add(pos, itemOid);
    }
    
    public void itemTaken(int pos) {
    	items.set(pos, null);
    }
    
    public String toString() {
    	return "Mail subject: " + subject;
    }
    
    public int getID() { return id;}
    public void setID(int id) {
    	this.id = id;
    }
    
    public String getRecipientName() { return recipientName;}
    public void setRecipientName(String recipientName) {
    	this.recipientName = recipientName;
    }
    
    public OID getRecipientOID() { return recipientOID;}
    public void setRecipientOID(OID recipientOID) {
    	this.recipientOID = recipientOID;
    }
    
    public String getSenderName() { return senderName;}
    public void setSenderName(String senderName) {
    	this.senderName = senderName;
    }
    
    public OID getSenderOID() { return senderOID;}
    public void setSenderOID(OID senderOID) {
    	this.senderOID = senderOID;
    }
    
    public String getSubject() { return subject;}
    public void setSubject(String subject) {
    	this.subject = subject;
    }
    
    public String getMessage() { return message;}
    public void setMessage(String message) {
    	this.message = message;
    }
    
    public int getCurrencyType() { return currencyType;}
    public void setCurrencyType(int currencyType) {
    	this.currencyType = currencyType;
    }
    
    public int getCurrencyAmount() { return currencyAmount;}
    public void setCurrencyAmount(int currencyAmount) {
    	this.currencyAmount = currencyAmount;
    }
    
    public ArrayList<OID> getItems() { return items;}
    public void setItems(ArrayList<OID> items) {
    	this.items = items;
    }
    
    public int getItemCategory() { return itemCategory;}
    public void setItemCategory(int itemCategory) {
    	this.itemCategory = itemCategory;
    }
    
    public boolean getCoD() { return CoD;}
    public void setCoD(boolean CoD) {
    	this.CoD = CoD;
    }
    
    public boolean getMailRead() { return mailRead;}
    public void setMailRead(boolean mailRead) {
    	this.mailRead = mailRead;
    }
    
    public boolean getMailArchive() { return mailRead;}
    public void setMailArchive(boolean mailRead) {
    	this.mailArchive = mailArchive;
    }

    int id;
    String recipientName;
    OID recipientOID;
    String senderName;
    OID senderOID;
    String subject;
    String message;
    int currencyType;
    int currencyAmount;
    ArrayList<OID> items;
    int itemCategory;
    boolean CoD;
    boolean mailRead;
    boolean mailArchive;

    private static final long serialVersionUID = 1L;
}
