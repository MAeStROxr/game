package atavism.agis.objects;

public class ResistanceStatDef extends AgisStatDef {

	public ResistanceStatDef(String name) {
		super(name);
	}
    
	public void update(AgisStat stat, CombatInfo info) {
        stat.max = 80;
        stat.min = 0;
        stat.setDirty(true);
        super.update(stat, info);
	}
}