package atavism.agis.objects;

import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.ClassAbilityClient;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CraftingPlugin;
import atavism.agis.util.EventMessageHelper;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.*;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * A ResourceNode is an object players can gather items from. The ResourceNode randomly generates its items upon spawn
 * from the items it has been given and allows the player to gather them if they meet the requirements.
 * @author Andrew Harrison
 *
 */
public class ResourceNode implements Serializable, MessageCallback, Runnable {
    public ResourceNode() {
    }
    
    public ResourceNode(int id, AOVector loc,  OID instanceOID) {
    	this.id = id;
    	this.loc = loc;
    	this.instanceOID = instanceOID;
    }
    
    public void AddResourceDrop(int item, int min, int max, int chance) {
    	drops.add(new ResourceDrop(item, min, max, chance));
    }
    
    /**
     * Subscribes the instance to receive certain relevant messages that are sent to the world object 
     * created by this instance.
     */
    public void activate() {
    	SubjectFilter filter = new SubjectFilter(objectOID);
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        eventSub = Engine.getAgent().createSubscription(filter, this);
        // Set the reaction radius tracker to alert the object if a player has entered its draw radius
        MobManagerPlugin.getTracker(instanceOID).addReactionRadius(objectOID, 100);
        active = true;
        Log.debug("RESOURCE: node with oid: " + objectOID + " activated");
    }
    
    /**
     * Deals with the messages the instance has picked up.
     */
    public void handleMessage(Message msg, int flags) {
    	if (active == false) {
    	    return;
    	}
    	if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS) {
    	    ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage)msg;
     	    Log.debug("RESOURCE: myOid=" + objectOID + " objOid=" + nMsg.getSubject()
     		      + " inRadius=" + nMsg.getInRadius() + " wasInRadius=" + nMsg.getWasInRadius());
    	    if (nMsg.getInRadius()) {
    	    	addPlayer(nMsg.getSubject());
    	    } else {
    	    	// Remove subject from targets in range
    	    	removePlayer(nMsg.getSubject());
    	    }
    	} else if (msg instanceof CombatClient.interruptAbilityMessage) {
            interruptHarvestTask();
        }
    }
    
    @Override
	public void run() {
    	active = true;
    	harvestsLeft = harvestCount;
        generateItems();

		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			sendState(playerOid);
		}
	}
    
    /**
     * An external call to spawn a world object for the claim.
     * @param instanceOID
     */
    public void spawn(OID instanceOID) {
    	this.instanceOID = instanceOID;
    	spawn();
    }
    
    /**
     * Spawn a world object for the claim.
     */
    public void spawn() {
    	Template markerTemplate = new Template();
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_NAME, "_ign_" + name + id);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.mob);
    	markerTemplate.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_INSTANCE, instanceOID);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_LOC, new Point(loc));
    	//markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_ORIENT, orientation);
    	DisplayContext dc = new DisplayContext(gameObject, true);
		dc.addSubmesh(new DisplayContext.Submesh("", ""));
		markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
		markerTemplate.put(Namespace.WORLD_MANAGER, "model", gameObject); 
    	// Put in any additional props
    	if (props != null) {
    		for (String propName : props.keySet()) {
    			markerTemplate.put(Namespace.WORLD_MANAGER, propName, props.get(propName));
    		}
    	}
    	// Create the object
    	objectOID = ObjectManagerClient.generateObject(ObjectManagerClient.BASE_TEMPLATE_ID,
                ObjectManagerClient.BASE_TEMPLATE, markerTemplate);
    	
    	if (objectOID != null) {
    		// Need to create an interpolated world node to add a tracker/reaction radius to the claim world object
    		BasicWorldNode bwNode = WorldManagerClient.getWorldNode(objectOID);
    		InterpolatedWorldNode iwNode = new InterpolatedWorldNode(bwNode);
    		resourceNodeEntity = new ResourceNodeEntity(objectOID, iwNode);
    		EntityManager.registerEntityByNamespace(resourceNodeEntity, Namespace.MOB);
    		MobManagerPlugin.getTracker(instanceOID).addLocalObject(objectOID, 100);
    		
            WorldManagerClient.spawn(objectOID);
            Log.debug("RESOURCE: spawned resource at : " + loc);
            activate();
            
            harvestsLeft = harvestCount;
            generateItems();
        }
    }
    
    public void activateAsChildOfGroup() {
    	active = true;
    	harvestsLeft = harvestCount;
        generateItems();
    }
    
    public void setMobAsSkinnable(OID mobOid) {
    	this.objectOID = mobOid;
    	harvestsLeft = harvestCount;
        generateItems();
    }
    
    void generateItems() {
    	currentItems = new HashMap<Integer, Integer>();
    	Random rand = new Random();
    	for (ResourceDrop drop : drops) {
    		if (rand.nextInt(100 * 100) < (drop.chance * 100f)) {
    			int amount = drop.min + rand.nextInt(drop.max - drop.min + 1);
    			currentItems.put(drop.item, amount);
    			if (CraftingPlugin.RESOURCE_COUNT_IS_LOOT_COUNT && currentItems.size() == harvestCount) {
    	    		break;
    	    	}
    		}
    	}
    	
    	if (currentItems.size() == 0 && drops.size() > 0) {
    		ResourceDrop drop = drops.get(0);
    		int amount = drop.min + rand.nextInt(drop.max - drop.min + 1);
			currentItems.put(drop.item, amount);
    	}
    	skillupGiven = false;
    }
    
    /**
     * Add a player to the update list for this ResourceNode. The player will receive data about the node and any updates
     * that occur.
     * @param playerOID
     */
    public void addPlayer(OID playerOid) {
    	Log.debug("RESOURCE: added player: " + playerOid);
    	// Send down the state to the player
    	sendState(playerOid);
			
		if (!playersInRange.contains(playerOid)) {
	    	playersInRange.add(playerOid);
	    }
    }
    
    /**
     * Removes a player from the ResourceNode. They will no longer receive updates.
     * @param playerOID
     * @param removeLastID
     */
    public void removePlayer(OID playerOid) {
    	if (playersInRange.contains(playerOid))
    		playersInRange.remove(playerOid);
    }
    
    /**
     * Checks whether the player can gather items from this resource. Checks their skill level
     * and weapon.
     * @param playerOID
     * @return
     */
    boolean playerCanGather(OID playerOid, boolean checkSkillAndWeapon, int playerSkillLevel) {
    	// No one else is currently gathering are they?
    	if (task != null) {
    		EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.RESOURCE_NODE_BUSY, 0, "");
    		Log.debug("RESOURCE: node is busy");
    		return false;
    	}
    	// Harvests left check
    	if (harvestsLeft == 0) {
    		Log.debug("RESOURCE: no harvests left");
    		return false;
    	}
    	// Dead check
    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
    	if (dead) {
    		Log.debug("RESOURCE: player is dead");
    		return false;
    	}
    	
    	// location check
    	Point p = WorldManagerClient.getObjectInfo(playerOid).loc;
    	// Player must be within 4 meters of the node (16 for squared)
    	if (Point.distanceToSquared(p, new Point(loc)) > CraftingPlugin.RESOURCE_GATHER_DISTANCE * CraftingPlugin.RESOURCE_GATHER_DISTANCE) {
    		EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.TOO_FAR_AWAY, 0, "");
    		Log.debug("RESOURCE: player is too far away");
    		return false;
    	}
    	if (checkSkillAndWeapon) {
    		// skill check
    		if (skill > 0 && skillLevelReq > 0) {
    			Log.debug("RESOURCE: checking skill: " + skill + " against playerSkillLevel: " + playerSkillLevel);
    			if (playerSkillLevel < skillLevelReq) {
    				EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.SKILL_LEVEL_TOO_LOW, skill, "" + skillLevelReq);
    				return false;
    			}
    		}
    		// weapon check
    		if (weaponReq != null && !weaponReq.equals("") && !weaponReq.equals("None")) {
    			String weaponType = (String) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, "weaponType");
    			weaponType = weaponType.toLowerCase();
    			String reqWeapon = weaponReq.toLowerCase();
    			Log.debug("RESOURCE: checking weaponReq: " + weaponReq + " against: " + weaponType);
    			if (!weaponType.equals(reqWeapon) && !(weaponType.contains(reqWeapon) && Character.isDigit(weaponType.charAt(0)))) {
    				EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.EQUIP_MISSING, 0, weaponReq);
    				return false;
    			}
    		}
    	}
    	
    	Log.debug("RESOURCE: player can harvest resource");
    	return true;
    }
    
    public void tryHarvestResources(OID playerOid) {
    	Log.debug("RESOURCE: got player trying to harvest resource with skillID: " + skill);
    	int playerSkillLevel = 1;
    	if (skill > 0)
    		playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, skill);
    	if (!playerCanGather(playerOid, true, playerSkillLevel)) {
    		return;
    	}
	    
	    task = new HarvestTask();
	    task.StartHarvestTask(loc, Quaternion.Identity, playerOid, playerSkillLevel, this);
	    
	    if (harvestTimeReq > 0) {
    		Engine.getExecutor().schedule(task, (long) harvestTimeReq * 1000, TimeUnit.MILLISECONDS);
    		task.sendStartHarvestTask(harvestTimeReq);
    		// Register for player movement to interrupt the gathering
    		SubjectFilter filter = new SubjectFilter(playerOid);
	        filter.addType(CombatClient.MSG_TYPE_INTERRUPT_ABILITY);
	        sub = Engine.getAgent().createSubscription(filter, this);
    	} else {
    		task.run();
    	}
    }
    
    void interruptHarvestTask() {
    	if (task != null) {
    		task.interrupt();
    		task = null;
    		if (sub != null)
                Engine.getAgent().removeSubscription(sub);
    	}
    }
    
    void harvestComplete(HarvestTask task) {
    	// Do a success calculation check
	    if (skillLevelMax < 2)
	    	skillLevelMax = 2;
	    if (skillLevelMax < skillLevelReq)
	    	skillLevelMax = skillLevelReq+1;
	    int rollMax = (skillLevelMax-skillLevelReq) * 130 / 100;
	    int skillLevelCalc = (task.playerSkillLevel - skillLevelReq + ((skillLevelMax-skillLevelReq) / 2)) * 200 / 300;
	    if (skillLevelCalc > skillLevelMax)
	    	skillLevelCalc = skillLevelMax;
	    
	    Random rand = new Random();
    	// Check if there are any items to give
    	if (currentItems.isEmpty() || (CraftingPlugin.RESOURCE_GATHER_CAN_FAIL && skill > 0 
    			&& skillLevelReq > 0 && skillLevelCalc < rand.nextInt(rollMax))) {
    		if (CraftingPlugin.RESOURCE_DROPS_ON_FAIL && !CraftingPlugin.RESOURCE_COUNT_IS_LOOT_COUNT)
    			harvestsLeft--;
			if (harvestsLeft == 0) {
				despawnResource();
			} else {
				Log.debug("RESOURCE: generating items from tryHarvestResources as currentItems is empty");
				generateItems();
			}
			// Still send down item list (which will be empty)
			sendNoItems(task.playerOid);
			//TODO: send down some form of failed to harvest message
			EventMessageHelper.SendErrorEvent(task.playerOid, EventMessageHelper.RESOURCE_HARVEST_FAILED, 0, "");
			return;
		}
    	
    	// Do skill up
    	if (skill > 0 && !skillupGiven) {
			Log.debug("RESOURCE: checking skill: " + skill + " against playerSkillLevel: " + task.playerSkillLevel);
			if (task.playerSkillLevel < skillLevelMax) {
				CombatClient.abilityUsed(task.playerOid, skill);
				skillupGiven = true;
			} else if (CraftingPlugin.GAIN_SKILL_AFTER_MAX && rand.nextInt(4) == 0) {
				CombatClient.abilityUsed(task.playerOid, skill);
				skillupGiven = true;
			}
    	}
    	
    	// Send items
    	if (CraftingPlugin.AUTO_PICKUP_RESOURCES) {
    		OID playerOid = task.playerOid;
    		this.task = null;
    		gatherAllItems(playerOid);
    	} else {
    		sendItems(task.playerOid);
    	}
    	
    	// Reduce item durability
    	if (weaponReq != null && !weaponReq.contains("None"))
    		AgisInventoryClient.equippedItemUsed(task.playerOid, "Gather");
    }
    
    void sendItems(OID playerOID) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
    	props.put("ext_msg_subtype", "resource_drops");
		props.put("resourceNode", id);
		props.put("harvestCount", harvestCount);
		props.put("harvestsLeft", harvestsLeft);
		props.put("numDrops", currentItems.size());
		int dropNum = 0;
		for (int item : currentItems.keySet()) {
			props.put("drop" + dropNum, item);
			props.put("dropCount" + dropNum, currentItems.get(item));
			dropNum++;
		}
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOID, 
				playerOID, false, props);
			Engine.getAgent().sendBroadcast(msg);
    }
    
    void sendNoItems(OID playerOID) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
    	props.put("ext_msg_subtype", "resource_drops");
		props.put("resourceNode", id);
		props.put("harvestCount", harvestCount);
		props.put("harvestsLeft", harvestsLeft);
		props.put("numDrops", 0);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOID, 
				playerOID, false, props);
			Engine.getAgent().sendBroadcast(msg);
    }
    
    public void gatherItem(OID playerOid, int itemID) {
    	int playerSkillLevel = -1;
    	if (skill > 0)
    		playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, skill);
    	if (!playerCanGather(playerOid, false, playerSkillLevel)) {
    		return;
    	}
    	if (currentItems.containsKey(itemID)) {
    		int count = currentItems.get(itemID);
    		// Add item to player
    		AgisInventoryClient.generateItem(playerOid, itemID, null, count, null);
    		EventMessageHelper.SendInventoryEvent(playerOid, EventMessageHelper.ITEM_HARVESTED, itemID, count, "");
    		
    		// Remove item from currentItems
    		currentItems.remove(itemID);
    		sendItems(playerOid);
    		
    		if (currentItems.isEmpty()) {
    			harvestsLeft--;
    			if (harvestsLeft == 0) {
    				despawnResource();
    			} else {
    				generateItems();
    				return;
    			}
    		}
    	} else {
    		sendItems(playerOid);
    	}
    	
    }
    
    public void gatherAllItems(OID playerOid) {
    	int playerSkillLevel = -1;
    	if (skill > 0)
    		playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, skill);
    	if (!playerCanGather(playerOid, false, playerSkillLevel)) {
    		return;
    	}
    	for (int itemID : currentItems.keySet()) {
    		int count = currentItems.get(itemID);
    		// Add item to player
    		AgisInventoryClient.generateItem(playerOid, itemID, null, count, null);
    		EventMessageHelper.SendInventoryEvent(playerOid, EventMessageHelper.ITEM_HARVESTED, itemID, count, "");
		}
    	currentItems.clear();
    	harvestsLeft--;
    	sendItems(playerOid);
		if (harvestsLeft == 0) {
			despawnResource();
		} else {
			generateItems();
		}
    	
    }
    
    public void despawnResource() {
    	Log.debug("RESOURCE: despawning resource");
    	active = false;

		// Loop through players in range and send them the update
		for (OID playerOid : playersInRange) {
			sendState(playerOid);
		}
		
		
		// Schedule the respawn
		if (respawnTime > 0) {
			Engine.getExecutor().schedule(this, respawnTime, TimeUnit.SECONDS);
		} else if (!CraftingPlugin.USE_RESOURCE_GROUPS) {
			WorldManagerClient.despawn(objectOID);
		}
    }
    
    void sendState(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "resource_state");
		props.put("nodeID", id);
		props.put("active", active);
		TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }

	public int getID() { return id; }
    public void setID(int id) {
    	this.id = id;
    }
    
    public String getName() { return name; }
    public void setName(String name) {
    	this.name = name;
    }
    
    public String getGameObject() { return gameObject; }
    public void setGameObject(String gameObject) {
    	this.gameObject = gameObject;
    }
    
    public String getCoordEffect() { return coordinatedEffect; }
    public void setCoordEffect(String coordinatedEffect) {
    	this.coordinatedEffect = coordinatedEffect;
    }
    
    public AOVector getLoc() { return loc; }
    public void setLoc(AOVector loc) {
    	this.loc = loc;
    }
    
    public HashMap<String, Serializable> getProps() { return props; }
    public void setProps(HashMap<String, Serializable> props) {
    	this.props = props;
    }
    
    public OID getInstanceOID() { return instanceOID; }
    public void setInstanceOID(OID instanceOID) {
    	this.instanceOID = instanceOID;
    }
    
    public OID getObjectOID() { return objectOID; }
    public void setObjectOID(OID objectOID) {
    	this.objectOID = objectOID;
    }
    
    public boolean getEquippedReq() { return equippedReq; }
    public void setEquippedReq(boolean equippedReq) {
    	this.equippedReq = equippedReq;
    }
    
    public int getSkill() { return skill; }
    public void setSkill(int skill) {
    	this.skill = skill;
    }
    
    public int getSkillLevelReq() { return skillLevelReq; }
    public void setSkillLevelReq(int skillLevelReq) {
    	this.skillLevelReq = skillLevelReq;
    	if (this.skillLevelReq > skillLevelMax) 
    		this.skillLevelMax = this.skillLevelReq;
    }
    
    public int getSkillLevelMax() { return skillLevelMax; }
    public void setSkillLevelMax(int skillLevelMax) {
    	this.skillLevelMax = skillLevelMax;
    	if (this.skillLevelMax < this.skillLevelReq)
    		this.skillLevelMax = this.skillLevelReq;
    }
    
    public String getWeaponReq() { return weaponReq; }
    public void setWeaponReq(String weaponReq) {
    	this.weaponReq = weaponReq;
    }

    public boolean getActive() { return active; }
    public void setActive(boolean active) {
    	this.active = active;
    }
    
    public int getRespawnTime() { return respawnTime; }
    public void setRespawnTime(int respawnTime) {
    	this.respawnTime = respawnTime;
    }
    
    public int getHarvestCount() { return harvestCount; }
    public void setHarvestCount(int harvestCount) {
    	this.harvestCount = harvestCount;
    }
    
    public float getHarvestTimeReq() { return harvestTimeReq; }
    public void setHarvestTimeReq(float harvestTimeReq) {
    	this.harvestTimeReq = harvestTimeReq;
    }

    int id;
    String name;
    int skill;
    int skillLevelReq;
    int skillLevelMax;
    String weaponReq;
    boolean equippedReq = false;
    String gameObject;
    String coordinatedEffect;
    AOVector loc;
    int respawnTime;
    OID instanceOID;
    OID objectOID;
    HashMap<String, Serializable> props;
    int harvestCount;
    int harvestsLeft;
    float harvestTimeReq = 0;
    List<ResourceDrop> drops = new LinkedList<ResourceDrop>();
    HashMap<Integer, Integer> currentItems;
    boolean skillupGiven = false;
    boolean active;
    Long eventSub = null;
    LinkedList<OID> playersInRange = new LinkedList<OID>();
    
    HarvestTask task;
    Long sub = null;
    ResourceNodeEntity resourceNodeEntity;
    
    /**
     * A Runnable class that adds an object to the claim when it is run. 
     * @author Andrew Harrison
     *
     */
    public class HarvestTask implements Runnable {
    	
    	protected AOVector loc;
    	protected Quaternion orient;
    	protected OID playerOid;
    	protected int playerSkillLevel;
    	protected ResourceNode resourceNode;
    	protected boolean interrupted;
    	
    	public HarvestTask() {
    		
    	}
    	
    	public void StartHarvestTask(AOVector loc, Quaternion orient, OID playerOid, int playerSkillLevel, ResourceNode resourceNode) {
    		Log.debug("RESOURCE: creating new harvest task");
    		this.loc = loc;
    		this.orient = orient;
    		this.playerOid = playerOid;
    		this.playerSkillLevel = playerSkillLevel;
    		this.resourceNode = resourceNode;
    	}
    	
    	public void sendStartHarvestTask(float length) {
    		Log.debug("RESOURCE: sending start harvest task");
    		Map<String, Serializable> props = new HashMap<String, Serializable>();
        	props.put("ext_msg_subtype", "start_harvest_task");
    		props.put("length", length);
        	TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    		Engine.getAgent().sendBroadcast(msg);
    		
    		// Send animation
        	CoordinatedEffect cE = new CoordinatedEffect(resourceNode.coordinatedEffect);
    	    cE.sendSourceOid(true);
    	    cE.sendTargetOid(true);
    	    cE.putArgument("length", length);
    	    cE.invoke(playerOid, playerOid);
    	}
    	
		@Override
		public void run() {
			if (resourceNode.sub != null)
                Engine.getAgent().removeSubscription(resourceNode.sub);
			
			if (interrupted) {
				Log.debug("BUILD: task was interrupted, not completing run");
				resourceNode.task = null;
				return;
			}
			
			// Dead check
	    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
	    	if (!dead) {
	    		resourceNode.harvestComplete(this);
	    	}
	    	resourceNode.task = null;
		}
		
		public void interrupt() {
			interrupted = true;
			Map<String, Serializable> props = new HashMap<String, Serializable>();
        	props.put("ext_msg_subtype", "harvest_task_interrupted");
        	TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    		Engine.getAgent().sendBroadcast(msg);
		}
    }
    
    /**
     * Should probably be a struct
     * @author Andrew
     *
     */
    class ResourceDrop {
    	public int item;
    	public int min;
    	public int max;
    	public float chance;
    	
    	public ResourceDrop(int item, int min, int max, int chance) {
    		this.item = item;
    		this.min = min;
    		this.max = max;
    		this.chance = chance;
    	}
    }

    /**
     * Sub-class needed for the interpolated world node so a perceiver can be created.
     * @author Andrew
     *
     */
	public class ResourceNodeEntity extends Entity implements EntityWithWorldNode
	{

		public ResourceNodeEntity(OID oid, InterpolatedWorldNode node) {
	    	setWorldNode(node);
	    	setOid(oid);
	    }
		
		public InterpolatedWorldNode getWorldNode() { return node; }
	    public void setWorldNode(InterpolatedWorldNode node) { this.node = node; }
	    InterpolatedWorldNode node;

		@Override
		public void setDirLocOrient(BasicWorldNode bnode) {
			if (node != null)
	            node.setDirLocOrient(bnode);
		}

		@Override
		public Entity getEntity() {
			return (Entity)this;
		}
		
		private static final long serialVersionUID = 1L;
	}
	
	private static final long serialVersionUID = 1L;

	
}
