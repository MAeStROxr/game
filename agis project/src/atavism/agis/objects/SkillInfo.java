package atavism.agis.objects;

import atavism.agis.abilities.FriendlyEffectAbility;
import atavism.agis.core.Agis;
import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisEffect;
import atavism.agis.objects.SkillTemplate.SkillAbility;
import atavism.agis.plugins.ClassAbilityPlugin;
import atavism.agis.plugins.CombatPlugin;
import atavism.agis.util.CombatHelper;
import atavism.agis.util.EventMessageHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.server.engine.Engine;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.*;

/**
 * The SkillInfo class stores information about the skills a player has and provides functions
 * for learning skills, along with increasing and decreasing skill levels.
 * @author Andrew Harrison
 *
 */
public class SkillInfo implements Serializable {
	protected int category;
	protected int skillPoints;
	protected int pointsSpent;
	protected HashMap<Integer, SkillData> skills = new HashMap<Integer, SkillData>();
	
	public SkillInfo() {
	}
	
	public SkillInfo(int category) {
		this.category = category;
		this.skillPoints = 0;
		this.pointsSpent = 0;
		this.skills = new HashMap<Integer, SkillData>();
	}
	
	/**
	 * Adds a 
	 * @param tmpl
	 * @return
	 */
	public SkillData addSkill(SkillTemplate tmpl) {
		Log.debug("SKILL: adding skill " + tmpl.skillID + " to info");
		SkillData skillData = new SkillData(tmpl.getSkillID(), tmpl.getSkillName(), 0, 1, ClassAbilityPlugin.SKILL_STARTING_MAX, 
				tmpl.getParentSkill());
		this.skills.put(skillData.getSkillID(), skillData);
		Log.debug("SKILL: added new skill: " + tmpl.getSkillID() + "." + tmpl.getSkillName());
		return skillData;
	}
	
	public int getCategory() { return category; }
    public void setCategory(int category) { this.category = category; }
    
    public int getSkillPoints() { return skillPoints; }
    public void setSkillPoints(int skillPoints) { this.skillPoints = skillPoints; }
    
    public int getPointsSpent() { return pointsSpent; }
    public void setPointsSpent(int pointsSpent) { this.pointsSpent = pointsSpent; }
    
    public HashMap<Integer, SkillData> getSkills() { return skills; }
    public void setSkills(HashMap<Integer, SkillData> skills) { this.skills = skills; }
    
    public static void learnSkill(SkillInfo skillInfo, int skillType, int aspect, CombatInfo info) {
    	Log.debug("SKILL: learning skill: " + skillType);
    	
    	if (skillInfo.getSkills().containsKey(skillType)) {
    		return;
    	}
    	
    	SkillTemplate template = Agis.SkillManager.get(skillType);
    	if (template.mainAspectOnly() && template.getAspect() != aspect) {
	    	// Send some message to the client saying they don't have enough points
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "Your class cannot learn this skill.");
	    	return;
	    }
    	
    	SkillData skillData = skillInfo.addSkill(template);
	    // Give the player the first ability from this skill
	    newSkillGained(info, skillType);
    }
    
	
    /**
     * Increase the skill level of the given skill.
     * @param skillInfo
     * @param skillType
     * @param aspect
     * @param info
     */
	public static void increaseSkill(SkillInfo skillInfo, int skillType, int aspect, CombatInfo info) {
		Log.debug("SKILL: increasing skill: " + skillType);
	    SkillTemplate template = Agis.SkillManager.get(skillType);
	    //int skillAspect = template.getAspect();
	    //int oppositeAspect = template.getOppositeAspect();
	    int upgradeCost = 1; //5;
	    /*if (aspect.equals(skillAspect))
	        upgradeCost = 4;
	    else if (aspect.equals(oppositeAspect))
	    	upgradeCost = 6;*/
	    
	    //TODO: Check if the skill level will be higher than the player level?
	    int level = info.statGetCurrentValue("level");
	    
	    if (ClassAbilityPlugin.USE_SKILL_PURCHASE_POINTS && skillInfo.skillPoints < upgradeCost) {
	    	// Send some message to the client saying they don't have enough points
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You do not have enough points to increase that skill");
	    	return;
	    }
	    
	    if (template.mainAspectOnly() && template.getAspect() != aspect) {
	    	// Send some message to the client saying they don't have enough points
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "Your class cannot learn this skill.");
	    	return;
	    }
	    
	    SkillData skillData;
	    if (!skillInfo.skills.containsKey(skillType)) {
	    	// The player has not learned this skill at all
	    	skillData = skillInfo.addSkill(template);
    	    // Give the player the first ability from this skill
    	    newSkillGained(info, skillType);
	    } else {
	    	skillData = skillInfo.skills.get(skillType);
	    	if (level <= skillData.getSkillMaxLevel()) {
	    		// increase the cost of incrementing the skill by how many levels the player is behind
	    		//upgradeCost = upgradeCost + (skillData.getSkillMaxLevel() + 1 - level);
	    		//upgradeCost++;
	    		if (ClassAbilityPlugin.USE_SKILL_PURCHASE_POINTS && skillInfo.skillPoints < upgradeCost) {
	    	    	// Send some message to the client saying they don't have enough points
	    	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You do not have enough points to increase that skill");
	    	    	return;
	    	    }
	    	}
	    	// Check to see if the maximum is already at the limit
	    	if (/*skillData.getSkillMaxLevel() == ClassAbilityPlugin.SKILL_MAX ||*/ skillData.getSkillMaxLevel() == template.getMaxLevel()) {
	    		WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "Your " + template.getSkillName() 
	    				+ " skill maximum is already at the current limit (" + template.getMaxLevel() + ").");
	    		return;
	    	}
	    	skillData.alterSkillMax(1);
	    	if (!ClassAbilityPlugin.USE_SKILL_PURCHASE_POINTS) {
	    		Log.debug("SKILL: increasing skill level");
	    		skillData.alterSkillLevel(1);
	    		skillPointGain(info, skillType, skillData.getSkillLevel());
	    	}
	    }
	    if (ClassAbilityPlugin.USE_SKILL_PURCHASE_POINTS) {
	    	skillInfo.skillPoints -= upgradeCost;
	    	skillInfo.pointsSpent += upgradeCost;
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have " + skillInfo.skillPoints + " skill points left.");
	    }
	    
	    WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "Your " + template.getSkillName() 
	    		+ " skill maximum level is now: " + skillData.getSkillMaxLevel());
	}
	
	/**
	 * Decrease the level of the given skill. The skill will be removed from the player 
	 * if it reaches 0 unless it belongs to the same class as the player.
	 * @param skillInfo
	 * @param skillType
	 * @param aspect
	 * @param info
	 */
	public static void decreaseSkill(SkillInfo skillInfo, int skillType, int aspect, CombatInfo info) {
		SkillTemplate template = Agis.SkillManager.get(skillType);
	    int skillAspect = template.getAspect();
	    //int oppositeAspect = template.getOppositeAspect();
	    
	    if (!skillInfo.skills.containsKey(skillType)) {
	    	Log.warn("SKILL: player attempted to decrease a skill they do not have");
	    	return;
	    }
	    SkillData skillData = skillInfo.skills.get(skillType);
	    // Some things to check for:
	    // *if a skill is being decreased to 0 and as such, it needs removed
	    // *if a skill is being decreased to 0 but its from the players aspect
	    
	    // Decrease the skills max/current/value as appropriate
	    if (skillData.getSkillMaxLevel() == 1) {
	    	if (aspect == skillAspect) {
	    		// Not allowed to be decreased to 0
	    		WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You cannot unlearn " + template.getAspect() + " aspect skills.");
	    		return;
	    	} else {
	    		// Lets remove the skill
	    		skillInfo.skills.remove(skillType);
	    		skillLost(info, skillType);
	    		WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You no longer have the " + template.getSkillName() + " skill.");
	    	}
	    } else {
	    	skillData.alterSkillMax(-1);
	    	if (skillData.getSkillLevel() > skillData.getSkillMaxLevel()) {
	    		skillData.alterSkillLevel(-1);
	    		skillPointLoss(info, skillType, skillData.getSkillLevel());
	    	}
	    	
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "Your " + template.getSkillName() 
	    			+ " skill maximum level is now: " + skillData.getSkillMaxLevel());
	    }
	    
	    int level = info.statGetCurrentValue("level");
	    int downgradeCost = 1; //5;
	    /*if (aspect.equals(skillAspect))
	    	downgradeCost = 4;
	    else if (aspect.equals(oppositeAspect))
	    	downgradeCost = 6;*/
	    
	    /*if (level <= skillData.getSkillMaxLevel()) {
    		// increase the refund of decrementing the skill
	    	downgradeCost = downgradeCost + (skillData.getSkillMaxLevel() + 1 - level);
	    	//downgradeCost++;
    	}*/
	    skillInfo.pointsSpent -= downgradeCost;
	    skillInfo.skillPoints += downgradeCost;
	    WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have " + skillInfo.skillPoints + " skill points left.");
	    
	}
	
	/**
	 * Attempt to increase the players level and max level of the given skill.
	 * @param skillInfo
	 * @param skillType
	 * @param info
	 */
	public static void skillUpAttempt(SkillInfo skillInfo, int skillType, CombatInfo info) {
		if (!skillInfo.skills.containsKey(skillType)) {
	    	Log.warn("SKILL: player does not have this skill: " + skillType);
	    	return;
	    }
		SkillData skillData = skillInfo.skills.get(skillType);
		if (skillData.state != 1) {
			// Skill is not set to skill up
			return;
		}
		
		SkillData parentSkillData = null;
		if (skillData.getParentSkill() != -1) {
			parentSkillData = skillInfo.skills.get(skillData.getParentSkill());
		}
		
		// Calculate skill up chance and do a roll of the dice to see if it should increase
		float increaseChance = CombatHelper.calcSkillUpChance(skillData);
		Random random = new Random();
	    int rand = random.nextInt(100);
	    Log.debug("SKILL: increaseChance: " + increaseChance + "; rand: " + rand + " for skill: " + skillData.getSkillName());
	    if (increaseChance > rand && skillData.getSkillLevel() < skillData.getSkillMaxLevel()) {
	        //skillData.alterSkillCurrent(1);
	    	if (parentSkillData == null || parentSkillData.skillLevel > skillData.skillLevel) {
	    		Log.debug("SKILL: increasing skill level");
	    		// If we have reached the limit, find a skill to decrease
	    		if (canPlayerIncreaseSkill(skillInfo, info)) {
	    			skillData.alterSkillLevel(1);
	    			skillPointGain(info, skillType, skillData.getSkillLevel());
	    		}
	    	}
	        /*if (skillData.getSkillCurrent() == (ClassAbilityPlugin.POINTS_PER_SKILL_LEVEL * skillData.getSkillLevel() + 10)) {
	        	// Time to level up the skill
	        	skillData.alterSkillLevel(1);
	        	skillPointGain(info, skillType, skillData.getSkillLevel());
	        }*/
	    }
	    
	    SkillTemplate template = Agis.SkillManager.get(skillType);
	    
	    if (!ClassAbilityPlugin.USE_SKILL_PURCHASE_POINTS) {
	    	// Calculate skill level max skill up chance and do a roll of the dice to see if it should increase
	    	float maxIncreaseChance = CombatHelper.calcMaxSkillUpChance(skillData);
	    	rand = random.nextInt(100);
	    	if (maxIncreaseChance > rand) {
	    		// Level up the skill max
	    		if ((parentSkillData == null || parentSkillData.skillMaxLevel > skillData.skillMaxLevel)
	    				&& skillData.getSkillMaxLevel() < template.getMaxLevel()) {
	    			Log.debug("SKILL: increasing skill max level");
	    			skillData.alterSkillMax(1);
	    		}
	    	}
	    }
	    
		// Also increase the skill level and max level for the parent skill if it exists and the roll is high enough
	    if (parentSkillData != null) {
	    	rand = random.nextInt(100);
	    	if (increaseChance > rand && parentSkillData.getSkillLevel() < parentSkillData.getSkillMaxLevel()) {
	    		parentSkillData.alterSkillLevel(1);
	    		skillPointGain(info, parentSkillData.getSkillID(), parentSkillData.getSkillLevel());
	    	}
        	
	    	if (!ClassAbilityPlugin.USE_SKILL_PURCHASE_POINTS) {
	    		template = Agis.SkillManager.get(parentSkillData.getSkillID());
	    		float maxIncreaseChance = CombatHelper.calcMaxSkillUpChance(skillData);
	    		rand = random.nextInt(100);
	    		if (maxIncreaseChance > rand && parentSkillData.getSkillMaxLevel() < template.getMaxLevel()) {
	    			parentSkillData.alterSkillMax(1);
	    		}
	    	}
	    }
	    Engine.getPersistenceManager().setDirty(info);
	}
	
	/**
	 * Checks if the total skill level of the player is greater than the max, and if so, will find another skill
	 * to decrease. Returns false if the total skill level is greater than max, and there is no skill to decrease.
	 * @param skillInfo
	 * @param info
	 * @return
	 */
	static boolean canPlayerIncreaseSkill(SkillInfo skillInfo, CombatInfo info) {
		// Get total skillLevel
		int totalSkillLevel = 0;
		for(SkillData skillData : skillInfo.getSkills().values()) {
			totalSkillLevel += skillData.skillLevel;
		}
		
		// If greater than or equal to max, find skill to decrease
		if (totalSkillLevel >= ClassAbilityPlugin.TOTAL_SKILL_MAX) {
			ArrayList<SkillData> skillsToDecrease = new ArrayList<SkillData>();
			for(SkillData skillData : skillInfo.getSkills().values()) {
				if (skillData.state == -1 && skillData.skillLevel > 0) {
					skillsToDecrease.add(skillData);
				}
			}
			// If no skill to decrease, return false
			if (skillsToDecrease.size() == 0)
				return false;
			
			// Else randomly decrease a skill
			Random rand = new Random();
			int skillPos = rand.nextInt(skillsToDecrease.size());
			skillsToDecrease.get(skillPos).alterSkillLevel(-1);
    		skillPointLoss(info, skillsToDecrease.get(skillPos).getSkillID(), skillsToDecrease.get(skillPos).getSkillLevel());
		}
		
		return true;
	}
	
	/**
	 * Clear all skills and give the player back their skill points. WARNING: Needs tested and possibly
	 * fixed.
	 * @param skillInfo
	 * @param info
	 */
	public static void resetSkills(SkillInfo skillInfo, CombatInfo info) {
	    ArrayList<Integer> currentAbilities = info.getCurrentAbilities();
	    ArrayList<String> currentActions = info.getCurrentActions();
	    
	    for (SkillData skillData : skillInfo.skills.values()) {
	    	// Go through each skill and reset the players stats from it etc.
	    	int skillType = skillData.getSkillID();
	    	SkillTemplate template = Agis.SkillManager.get(skillType);
	    	int skillLevel = skillData.getSkillLevel();
	    	String stat = template.getPrimaryStat();
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have lost your " + stat + " bonus from " + template.getSkillName());
	    	stat = template.getSecondaryStat();
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have lost your " + stat + " bonus from " + template.getSkillName());
	    	stat = template.getThirdStat();
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have lost your " + stat + " bonus from " + template.getSkillName());
	    	stat = template.getFourthStat();
	    	WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have lost your " + stat + " bonus from " + template.getSkillName());
	    	applyStatModifications(info, template, 0);
	    	// Now remove any abilities they may have from this skill
	    	
	    	for (int j = 0; j <= skillLevel; j++) {
	    		//String ability = abilities.get(j);
	    		ArrayList<SkillAbility> abilities = template.getAbilitiesByLevel(j);
	    		for (SkillAbility ability: abilities) {
	    			AgisAbility ab = Agis.AbilityManager.get(ability.abilityID);
    	    		int abilityID = ab.getID();
    	    		currentAbilities.remove(abilityID);
    	    		// Check if this ability was on the action bar, if it was, remove it
    	    		for (int k = 0; k < 7; k++) {
    	    			if (currentActions.get(k).equals("a" + ability))
    	    				currentActions.set(k, "");
    	    		}
	    		}
	    	}
	    }
	    
	    // Clear all the lists
	    info.setCurrentAbilities(currentAbilities);
	    info.setCurrentActions(currentActions);
	    
	    // Now re-apply the aspects skills
	    /*String aspect = (String) EnginePlugin.getObjectProperty(oid, CombatClient.NAMESPACE, "aspect");
	    for (int i = 0; i < skillTemplates.size(); i++) {
	    	SkillTemplate temp = skillTemplates.get(i);
	    	if (temp.getAspect().equals(aspect)) {
	    		//TODO
	    	}
	    }*/
	    
	    WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "Your skills have been reset.");
	}
	
	/**
	 * Gives players skill points for having leveled up. 
	 * WARNING: Verify this function is still used, and open it up to allow the 
	 * amount of points to be set.
	 * @param skillInfo
	 * @param info
	 * @param newLevel
	 */
	public static void levelChanged(SkillInfo skillInfo, CombatInfo info, int newLevel) {
		int totalPoints = (newLevel - 1) * ClassAbilityPlugin.SKILL_POINTS_GIVEN_PER_LEVEL;
		skillInfo.skillPoints = totalPoints - skillInfo.pointsSpent;
        
        // Now we need to refund points for skills that now match the players level
	    /*for (SkillData skillData : skillInfo.skills.values()) {
	    	if (skillData.getSkillMaxLevel() >= newLevel) {
	    		skillInfo.skillPoints++;
	    		skillInfo.pointsSpent--;
	    	}
	    }*/
		
        Log.debug("LEVELUP: setting player skill points to: " + skillInfo.skillPoints);
        WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2,
			    "You have " + skillInfo.skillPoints + " skill points to spend.");
	}
	
	public static void increaseSkillCurrent(SkillInfo skillInfo, int skillType, int alterValue, CombatInfo info) {
		SkillData skillData = skillInfo.skills.get(skillType);
		for (int i = 0; i < alterValue; i++) {
	    	if (skillData.getSkillCurrent() >= (skillData.getSkillMaxLevel() * ClassAbilityPlugin.POINTS_PER_SKILL_LEVEL))
    	    	break;
    	    
    	    skillData.alterSkillCurrent(1);
    	    if (skillData.getSkillCurrent() == (ClassAbilityPlugin.POINTS_PER_SKILL_LEVEL * skillData.getSkillLevel() + 10)) {
    	    	// Time to level up the skill
    	    	skillData.alterSkillLevel(1);
    	    	skillPointGain(info, skillType, skillData.getSkillLevel());
    	    }
	    }
	}
	
	/**
     * Helper function. When a player learns a new skill this function will give them the start abilities
     * from that skill.
     * @param oid
     * @param skillType
     */
    public static void newSkillGained(CombatInfo info, int skillType) {
    	//SkillTemplate template = skillTemplates.get(skillType);
    	Log.debug("SKILL: learned new skill: " + skillType);
    	SkillTemplate template = Agis.SkillManager.get(skillType);
    	ArrayList<Integer> abilities = template.getStartAbilityIDs();
    	for (int i = 0; i < abilities.size(); i++) {
    		int ability = abilities.get(i);
    		learnAbility(info, ability);
    	}
		
    }
    
    /**
     * Helper function. When a player loses a skill this function will remove the starting abilities.
     * @param oid
     * @param skillType
     */
    public static void skillLost(CombatInfo info, int skillType) {
    	//SkillTemplate template = skillTemplates.get(skillType);
    	SkillTemplate template = Agis.SkillManager.get(skillType);
    	ArrayList<Integer> abilities = template.getStartAbilityIDs();
    	ArrayList<Integer> currentAbilities = info.getCurrentAbilities();
    	for (int i = 0; i < abilities.size(); i++) {
    		int ability = abilities.get(i);
    		AgisAbility ab = Agis.AbilityManager.get(ability);
    		int abilityID = ab.getID();
    		for (int j = 0; j < currentAbilities.size(); j++) {
    			if (currentAbilities.get(j) == abilityID) {
    				currentAbilities.remove(j);
    				break;
    			}
    		}
    		WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have forgotten the ability " + template.getSkillName());
    		ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "<Forgot: " + ability + ">", 16);
    		// Check if the action bar has any empty spots, if so, remove the ability from it
    		if (ab.getAbilityType() == 1)
    		    info.removeAction(abilityID);
    		else if (ab.getAbilityType() == 2)
    			removePassiveEffect(ab, info);
    	}
    	info.setCurrentAbilities(currentAbilities);
    }
    
    /**
     * Helper function. When a player gains a point in a skill this function will increase 
     * the player stats and give them any abilities they have now unlocked.
     * @param oid: The oid of the player who increased their skill value
     * @param skillType: The type of skill that was increased
     * @param skillValue: The new value of the skill
     */
    public static void skillPointGain(CombatInfo info, int skillType, int skillValue) {
    	Log.debug("SKILL: skillPointGain hit with skill: " + skillType);
    	SkillTemplate template = Agis.SkillManager.get(skillType);
    	ExtendedCombatMessages.sendAnouncementMessage(info.getOwnerOid(), "Your " + template.getSkillName() 
    			+ " skill has reached level " + skillValue + "!", "Skill");
    	ArrayList<SkillAbility> abilities = template.getAbilitiesByLevel(skillValue);
    	if (abilities != null) {
    		for (SkillAbility ability: abilities) {
    			if (ability.automaticallyLearn)
    				learnAbility(info, ability.abilityID);
        	}
    	}
    	if ((skillValue % SKILL_PRIMARY_STAT_GAIN_INCREMENT) == 0) {
    		String stat = template.getPrimaryStat();
    		if (!stat.equals("~ none ~")) {
    			EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_INCREASE, 1, stat);
    			ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "+1 " + stat + "", 15);
    		}
    	}
    	if ((skillValue % SKILL_SECONDARY_STAT_GAIN_INCREMENT) == 0) {
    		String stat = template.getSecondaryStat();
    		if (!stat.equals("~ none ~")) {
    			EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_INCREASE, 1, stat);
    			ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "+1 " + stat + "", 15);
    		}
    	}
    	if ((skillValue % SKILL_THIRD_STAT_GAIN_INCREMENT) == 0) {
    		String stat = template.getThirdStat();
    		if (!stat.equals("~ none ~")) {
    			EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_INCREASE, 1, stat);
    			ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "+1 " + stat + "", 15);
    		}
    	}
    	if ((skillValue % SKILL_FOURTH_STAT_GAIN_INCREMENT) == 0) {
    		String stat = template.getFourthStat();
    		if (!stat.equals("~ none ~")) {
    			EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_INCREASE, 1, stat);
    			ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "+1 " + stat + "", 15);
    		}
    	}
    	
    	applyStatModifications(info, template, skillValue);
    }
    
    /**
     * Helper function. When a player loses a point in a skill this function will decrease 
     * the player stats and remove any abilities they can no longer use.
     * @param oid: The oid of the player who decreased their skill value
     * @param skillType: The type of skill that was decreased
     * @param skillValue: The new value of the skill
     */
    public static void skillPointLoss(CombatInfo info, int skillType, int skillValue) {
    	SkillTemplate template = Agis.SkillManager.get(skillType);
    	ArrayList<SkillAbility> abilities = template.getAbilitiesByLevel(skillValue+1);
		ArrayList<Integer> currentAbilities = info.getCurrentAbilities();
		if (abilities != null) {
			for (SkillAbility ability: abilities) {
				currentAbilities.remove(currentAbilities.indexOf(ability.abilityID));
	    		String abilityName = ability.abilityName;
	    		WorldManagerClient.sendObjChatMsg(info.getOwnerOid(), 2, "You have forgotten the " 
	    				+ template.getSkillName() + " ability: " + abilityName);
	    		ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "<Forgot: " + ability + ">", 16);
	    		// Check if the action bar has any empty spots, if so, add the ability onto it
	    		AgisAbility ab = Agis.AbilityManager.get(ability.abilityID);
	    		if (ab.getAbilityType() == 1)
	    		    info.removeAction(ability.abilityID);
	    		else if (ab.getAbilityType() == 2)
	    			removePassiveEffect(ab, info);
			}
			info.setCurrentAbilities(currentAbilities);
			ExtendedCombatMessages.sendAbilities(info.getOwnerOid(), currentAbilities);
		}
    	if ((skillValue % SKILL_PRIMARY_STAT_GAIN_INCREMENT) == (SKILL_PRIMARY_STAT_GAIN_INCREMENT-1)) {
    		String stat = template.getPrimaryStat();
    		EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_DECREASE, 1, stat);
    		ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "<-1 " + stat + ">", 15);
    	}
    	if ((skillValue % SKILL_SECONDARY_STAT_GAIN_INCREMENT) == (SKILL_SECONDARY_STAT_GAIN_INCREMENT-1)) {
    		String stat = template.getSecondaryStat();
    		EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_DECREASE, 1, stat);
    		ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "<-1 " + stat + ">", 15);
    	}
    	if ((skillValue % SKILL_THIRD_STAT_GAIN_INCREMENT) == (SKILL_THIRD_STAT_GAIN_INCREMENT-1)) {
    		String stat = template.getThirdStat();
    		EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_DECREASE, 1, stat);
    		ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "<-1 " + stat + ">", 15);
    	}
    	if ((skillValue % SKILL_FOURTH_STAT_GAIN_INCREMENT) == (SKILL_FOURTH_STAT_GAIN_INCREMENT-1)) {
    		String stat = template.getFourthStat();
    		EventMessageHelper.SendGeneralEvent(info.getOwnerOid(), EventMessageHelper.STAT_DECREASE, 1, stat);
    		ExtendedCombatMessages.sendCombatText(info.getOwnerOid(), "<-1 " + stat + ">", 15);
    	}
    	
    	applyStatModifications(info, template, skillValue);
    }
    
    /**
     * Adds the given ability to the players list of abilities they know. Adds the ability
     * to their action bar if there is space and it isn't a passive ability.
     * @param info
     * @param abilityID
     */
    public static void learnAbility(CombatInfo info, int abilityID) {
    	ArrayList<Integer> currentAbilities = info.getCurrentAbilities();
    	if (currentAbilities.contains(abilityID)) {
    		// Player already knows the ability
    		return;
    	}
    	AgisAbility ab = Agis.AbilityManager.get(abilityID);
		//int abilityID = ab.getID();
		currentAbilities.add(abilityID);
		// Send a combat event that the client can pick up and display a message for.
		EventMessageHelper.SendCombatEvent(info.getOwnerOid(), info.getOwnerOid(), EventMessageHelper.COMBAT_ABILITY_LEARNED, 
				abilityID, -1, -1, -1);
		
		if (ab.getAbilityType() == 2) {
			// Passive ability, lets apply the effect now
			applyPassiveEffects(ab, info);
		} else {
			// Check if the action bar has any empty spots, if so, add the ability onto it
			ArrayList<String> actions = info.getCurrentActions();
    	    for (int j = 0; j < 10; j++) {
    	    	if (actions.size() <= j) {
    	    		actions.add("a" + abilityID);
    	    		break;
    	    	} else if (actions.get(j).equals("")) {
    	    		actions.set(j, "a" + abilityID);
    	    		break;
    	    	}
    	    }
    	    ExtendedCombatMessages.sendActions(info.getOwnerOid(), actions);
		}
		info.setCurrentAbilities(currentAbilities);
		ExtendedCombatMessages.sendAbilities(info.getOwnerOid(), currentAbilities);
    }
    
    /**
     * Applies the effects from the given ability to the player. Called when
     * a passive ability is learned.
     * @param ability
     * @param player
     */
    public static void applyPassiveEffects(AgisAbility ability, CombatInfo player) {
    	Log.debug("COMBATPLUGIN: about to apply passive ability: " + ability.getID() +
    			" to player: " + player.getOwnerOid());
    	if (ability instanceof FriendlyEffectAbility) {
    		FriendlyEffectAbility Eability = (FriendlyEffectAbility) ability;
    		LinkedList<AgisEffect> effectsToAdd = Eability.getActivationEffect();
    		for (int i = 0; i < effectsToAdd.size(); i++) {
    			Log.debug("COMBATPLUGIN: about to apply passive effect: " + effectsToAdd.get(i).getName() +
    	    			" to player: " + player.getOwnerOid());
    			Map<String, Integer> params = new HashMap<String, Integer>();
    			//params.put("effectVal", Eability.GetEffectVal().get(i));
    			params.put("skillType", Eability.getSkillType());
    			AgisEffect.applyEffect(effectsToAdd.get(i), player, player, ability.getID(), params);
    		}
    	}
    }
    
    /**
     * Removed the effects from the given ability from the player. Called when a passive
     * ability has been forgotten.
     * @param ability
     * @param player
     */
    public static void removePassiveEffect(AgisAbility ability, CombatInfo player) {
    	Log.debug("COMBATPLUGIN: about to remove passive ability: " + ability.getID() +
    			" from player: " + player.getOwnerOid());
    	
    	if (ability instanceof FriendlyEffectAbility) {
    		FriendlyEffectAbility Eability = (FriendlyEffectAbility) ability;
    		LinkedList<AgisEffect> effectsToRemove = Eability.getActivationEffect();
    		for (int i = 0; i < effectsToRemove.size(); i++) {
    			int effectToRemove = Eability.getActivationEffect().get(i).getID();
    			Log.debug("COMBATPLUGIN: about to remove passive effect: " + effectsToRemove.get(i) +
        	    		" from player: " + player.getOwnerOid());
    			AgisEffect.removeEffectByID(player, effectToRemove);
    		}
    	}
    }
    
    /**
     * Applies stat modifications to the players CombatInfo based on their skill level for
     * the specified skill template. Removes all existing stat modifications first and recalculates
     * all values.
     * @param info
     * @param tmpl
     */
    public static void applyStatModifications(CombatInfo info, SkillTemplate tmpl, int skillValue) {
    	// Clear all stats linked to the stat skill key
    	for (String stat : CombatPlugin.STAT_LIST) {
    		info.statRemoveModifier(stat, getStatSkillKey(tmpl, 1));
    		info.statRemoveModifier(stat, getStatSkillKey(tmpl, 2));
    		info.statRemoveModifier(stat, getStatSkillKey(tmpl, 3));
    		info.statRemoveModifier(stat, getStatSkillKey(tmpl, 4));
    	}
    	
    	String stat = tmpl.getPrimaryStat();
		if (!stat.equals("~ none ~")) {
			info.statAddModifier(stat, getStatSkillKey(tmpl, 1), skillValue / SKILL_PRIMARY_STAT_GAIN_INCREMENT);
		}
		stat = tmpl.getSecondaryStat();
		if (!stat.equals("~ none ~")) {
			info.statAddModifier(stat, getStatSkillKey(tmpl, 2), skillValue / SKILL_SECONDARY_STAT_GAIN_INCREMENT);
		}
		stat = tmpl.getThirdStat();
		if (!stat.equals("~ none ~")) {
			info.statAddModifier(stat, getStatSkillKey(tmpl, 3), skillValue / SKILL_SECONDARY_STAT_GAIN_INCREMENT);
		}
		stat = tmpl.getFourthStat();
		if (!stat.equals("~ none ~")) {
			info.statAddModifier(stat, getStatSkillKey(tmpl, 4), skillValue / SKILL_SECONDARY_STAT_GAIN_INCREMENT);
		}
    }
    
    public static String getStatSkillKey(SkillTemplate tmpl, int statNum) {
    	return "Skill" + tmpl.getSkillID() + "_" + statNum;
    }
    
    static final int SKILL_PRIMARY_STAT_GAIN_INCREMENT = 4;
    static final int SKILL_SECONDARY_STAT_GAIN_INCREMENT = 5;
    static final int SKILL_THIRD_STAT_GAIN_INCREMENT = 6;
    static final int SKILL_FOURTH_STAT_GAIN_INCREMENT = 7;
    
    private static final long serialVersionUID = 1L;
}
