package atavism.agis.objects;

import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * The SkillTemplate class stores all the information needed about a skill. 
 * @author Andrew Harrison
 *
 */
public class SkillTemplate implements Serializable {
	protected int skillID;
	protected String skillName;
	protected int aspect;
    protected int oppositeAspect;
    protected boolean mainAspectOnly = false;
    protected String primaryStat;
    protected String secondaryStat;
    protected String thirdStat;
    protected String fourthStat;
    protected LinkedList<Integer> subSkills;
    protected int parentSkill = -1;
    protected int parentSkillLevelReq = 1;
    protected int maxLevel;
    protected int prereqSkill1 = -1;
    protected int prereqSkill1Level = 1;
    protected int prereqSkill2 = -1;
    protected int prereqSkill2Level = 1;
    protected int prereqSkill3 = -1;
    protected int prereqSkill3Level = 1;
    protected int playerLevelReq = 1;
    protected int skillPointCost = 1;
    protected boolean automaticallyLearn = true;
    
    protected LinkedList<SkillAbility> abilities = new LinkedList<SkillAbility>();

    /**
     * 
     * @param type
     * @param skillName
     * @param aspect
     * @param oppositeAspect
     * @param primaryStat
     * @param secondaryStat
     * @param thirdStat
     * @param fourthStat
     * @param abilities
     */
    public SkillTemplate(int type, String skillName, int aspect, int oppositeAspect, boolean mainAspectOnly,
    		String primaryStat, String secondaryStat, String thirdStat,
    		String fourthStat, boolean autoLearn) {
    	//Log.debug("SKILL TEMPLATE: starting skillTemplate creation");
    	this.skillID = type;
    	this.skillName = skillName;
    	this.aspect = aspect;
    	this.oppositeAspect = oppositeAspect;
    	this.mainAspectOnly = mainAspectOnly;
    	this.primaryStat = primaryStat;
    	this.secondaryStat = secondaryStat;
    	this.thirdStat = thirdStat;
    	this.fourthStat = fourthStat;
    	this.automaticallyLearn = autoLearn;
	    //Log.debug("SKILL TEMPLATE: finished skillTemplate creation");
    }
    
    public void addSkillAbility(int skillLevelReq, int abilityID, String abilityName, boolean autoLearn) {
    	SkillAbility skillAbility = new SkillAbility();
    	skillAbility.skillLevelReq = skillLevelReq;
    	skillAbility.abilityID = abilityID;
    	skillAbility.abilityName = abilityName;
    	skillAbility.automaticallyLearn = autoLearn;
    	abilities.add(skillAbility);
    }
    
    public ArrayList<SkillAbility> getAbilitiesByLevel(int level) {
    	ArrayList<SkillAbility> levelAbilities = new ArrayList<SkillAbility>();
    	for (SkillAbility ability : abilities) {
    		if (ability.skillLevelReq == level) {
    			levelAbilities.add(ability);
    		}
    	}
    	Log.debug("SKILL: got abilities: " + levelAbilities + " for skill: " + skillID + " at level: " + level);
    	return levelAbilities;
    }
    
    public ArrayList<String> getStartAbilities() {
    	ArrayList<String> abilityNames = new ArrayList<String>();
    	for (SkillAbility ability : abilities) {
    		if ((ability.skillLevelReq == 1 || ability.skillLevelReq == 0) && ability.automaticallyLearn) {
    			abilityNames.add(ability.abilityName);
    		}
    	}
    	return abilityNames;
    }
    
    public ArrayList<Integer> getStartAbilityIDs() {
    	ArrayList<Integer> abilityIDs = new ArrayList<Integer>();
    	for (SkillAbility ability : abilities) {
    		if ((ability.skillLevelReq == 1 || ability.skillLevelReq == 0) && ability.automaticallyLearn) {
    			abilityIDs.add(ability.abilityID);
    		}
    	}
    	
    	return abilityIDs;
    }
    
    public int getSkillID() { return skillID; }
    public void setSkillID(int skillID) { this.skillID = skillID; }
    
    public String getSkillName() { return skillName; }
    public void setSkillName(String skillName) { this.skillName = skillName; }
    
    public int getAspect() { return aspect; }
    public void setAspect(int aspect) { this.aspect = aspect; }
    
    public int getOppositeAspect() { return oppositeAspect; }
    public void setOppositeAspect(int oppositeAspect) { this.oppositeAspect = oppositeAspect; }
    
    public boolean mainAspectOnly() { return mainAspectOnly; }
    public void mainAspectOnly(boolean mainAspectOnly) { this.mainAspectOnly = mainAspectOnly; }
    
    public String getPrimaryStat() { return primaryStat; }
    public void setPrimaryStat(String primaryStat) { this.primaryStat = primaryStat; }
    
    public String getSecondaryStat() { return secondaryStat; }
    public void setSecondaryStat(String secondaryStat) { this.secondaryStat = secondaryStat; }
    
    public String getThirdStat() { return thirdStat; }
    public void setThirdStat(String thirdStat) { this.thirdStat = thirdStat; }
    
    public String getFourthStat() { return fourthStat; }
    public void setFourthStat(String fourthStat) { this.fourthStat = fourthStat; }
    
    public LinkedList<SkillAbility> getAbilities() { return abilities; }
    public void setAbilities(LinkedList<SkillAbility> abilities) { this.abilities = abilities; }
    
    public int getParentSkillLevelReq() { return parentSkillLevelReq; }
    public void setParentSkillLevelReq(int parentSkillLevelReq) { this.parentSkillLevelReq = parentSkillLevelReq; }
    
    public int getParentSkill() { return parentSkill; }
    public void setParentSkill(int parentSkill) { this.parentSkill = parentSkill; }
    
    public LinkedList<Integer> getSubSkills() { return subSkills; }
    public void setSubSkills(LinkedList<Integer> subSkills) { this.subSkills = subSkills; }
    public void addSubSkill(int subSkill) { subSkills.add(subSkill); }
    
    public int getMaxLevel() { return maxLevel; }
    public void setMaxLevel(int maxLevel) { this.maxLevel = maxLevel; }
    
    public int getPrereqSkill1() { return prereqSkill1; }
    public void setPrereqSkill1(int prereqSkill1) { this.prereqSkill1 = prereqSkill1; }
    
    public int getPrereqSkill1Level() { return prereqSkill1Level; }
    public void setPrereqSkill1Level(int prereqSkill1Level) { this.prereqSkill1Level = prereqSkill1Level; }
    
    public int getPrereqSkill2() { return prereqSkill2; }
    public void setPrereqSkill2(int prereqSkill2) { this.prereqSkill2 = prereqSkill2; }
    
    public int getPrereqSkill2Level() { return prereqSkill2Level; }
    public void setPrereqSkill2Level(int prereqSkill2Level) { this.prereqSkill2Level = prereqSkill2Level; }
    
    public int getPrereqSkill3() { return prereqSkill3; }
    public void setPrereqSkill3(int prereqSkill3) { this.prereqSkill3 = prereqSkill3; }
    
    public int getPrereqSkill3Level() { return prereqSkill3Level; }
    public void setPrereqSkill3Level(int prereqSkill3Level) { this.prereqSkill3Level = prereqSkill3Level; }
    
    public int getPlayerLevelReq() { return playerLevelReq; }
    public void setPlayerLevelReq(int playerLevelReq) { this.playerLevelReq = playerLevelReq; }
    
    public int getSkillPointCost() { return skillPointCost; }
    public void setSkillPointCost(int skillPointCost) { this.skillPointCost = skillPointCost; }
    
    public boolean getAutomaticallyLearn() { return automaticallyLearn; }
    public void setAutomaticallyLearn(boolean automaticallyLearn) { this.automaticallyLearn = automaticallyLearn; }
    
    public class SkillAbility {
    	public int skillLevelReq = 1;
    	public int abilityID = -1;
    	public String abilityName = "";
    	public boolean automaticallyLearn = true;
    }
    
    private static final long serialVersionUID = 1L;
}
