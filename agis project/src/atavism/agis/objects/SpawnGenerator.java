package atavism.agis.objects;

import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.MessageDispatch;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Namespace;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.EntityManager;
import atavism.server.objects.ObjectFactory;
import atavism.server.objects.ObjectStub;
import atavism.server.objects.SpawnData;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.AORuntimeException;
import atavism.server.util.Log;
import atavism.server.util.Points;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

// spawn generators are objects that can be placed - these special objects
// spawn mobs and also keep track of when the mob dies so it can respawn them

public class SpawnGenerator
    implements MessageCallback, MessageDispatch, Runnable
{
    public SpawnGenerator() {
    }

    public SpawnGenerator(String name) {
        setName(name);
    }

    public SpawnGenerator(SpawnData data) {
        initialize(data);
    }

    public void initialize(SpawnData data) {
        setSpawnData(data);
        setName(data.getName());
        setSpawnID((Integer)data.getProperty("id"));
        setInstanceOid(data.getInstanceOid());
        setLoc(data.getLoc());
        setOrientation(data.getOrientation());
        setSpawnRadius(data.getSpawnRadius());
        setNumSpawns(data.getNumSpawns());
        setRespawnTime(data.getRespawnTime());
        if (data.getCorpseDespawnTime() != null)
            setCorpseDespawnTime(data.getCorpseDespawnTime());
    }

    public void activate() {
        try {
            spawns = new ArrayList<ObjectStub>(numSpawns);
            for (int i = 0; i < numSpawns; i++) {
                spawnObject();
            }
        }
        catch (Exception e) {
            throw new AORuntimeException("activate failed", e);
        }
    }

    public void deactivate()
    {
        if (spawns == null)
            return;

        List<ObjectStub> cleanupSpawns = spawns;
        for (ObjectStub obj : cleanupSpawns) {
            try {
                //obj.unload();
            	obj.despawn();
                ObjectManagerClient.unloadObject(obj.getOid());
                removeDeathWatch(obj.getOid());
            }
            catch (Exception e) {
                Log.exception("SpawnGenerator.deactivate()", e);
            }
        }
        spawns = null;
    }

    public void dispatchMessage(Message message, int flags,
                                MessageCallback callback)
    {
        Engine.defaultDispatchMessage(message, flags, callback);
    }

    public void handleMessage(Message msg, int flags) {
        if (msg instanceof PropertyMessage) {
            PropertyMessage propMsg = (PropertyMessage) msg;
            OID oid = propMsg.getSubject();
            Boolean dead = (Boolean)propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);
            if (dead != null && dead) {
                removeDeathWatch(oid);
                ObjectStub obj = (ObjectStub) EntityManager.getEntityByNamespace(oid, Namespace.MOB);
                if ((obj != null) && (corpseDespawnTime != -1)) {
                    Engine.getExecutor().schedule(new CorpseDespawner(obj), corpseDespawnTime,
                                                  TimeUnit.MILLISECONDS);
                }
                if (respawnTime != -1)
                	Engine.getExecutor().schedule(this, respawnTime, TimeUnit.MILLISECONDS);
                return;
            }
	        Boolean empty = (Boolean)propMsg.getProperty("objectEmpty");
	        if (empty != null && empty) {
	        	Log.debug("SPAWNGEN: mob is empty, despawning");
	        	removeDeathWatch(oid);
	            ObjectStub obj = (ObjectStub) EntityManager.getEntityByNamespace(oid, Namespace.MOB);
	            //if ((obj != null) && (corpseDespawnTime != -1))
	            //    Engine.getExecutor().schedule(new CorpseDespawner(obj), corpseDespawnTime, TimeUnit.MILLISECONDS);
	            // Despawn in 0.5 seconds
	            if ((obj != null))
		                Engine.getExecutor().schedule(new CorpseDespawner(obj), 500, TimeUnit.MILLISECONDS);
	            if (respawnTime != -1)
                	Engine.getExecutor().schedule(this, respawnTime, TimeUnit.MILLISECONDS);
	            return;
	        }	
	        Boolean tamed = (Boolean)propMsg.getProperty("tamed");
	        if (tamed != null && tamed) {
	        	Log.debug("SPAWNGEN: mob is tamed, despawning");
	        	removeDeathWatch(oid);
	            ObjectStub obj = (ObjectStub) EntityManager.getEntityByNamespace(oid, Namespace.MOB);
	            if ((obj != null) && (corpseDespawnTime != -1)) {
	                Engine.getExecutor().schedule(new CorpseDespawner(obj), corpseDespawnTime, TimeUnit.MILLISECONDS);
	            }
	            if (respawnTime != -1)
                	Engine.getExecutor().schedule(this, respawnTime, TimeUnit.MILLISECONDS);
	            return;
	        }
        }
    }

    protected void spawnObject() {
        if (spawns == null)
            return;
        Point loc = Points.findNearby(getLoc(), spawnRadius);
        ObjectStub obj = null;
        obj = factory.makeObject(spawnData, instanceOid, loc);
        if (obj == null) {
            Log.error("SpawnGenerator: Factory.makeObject failed, returned null, factory="+factory);
            return;
        }
        if (Log.loggingDebug)
            Log.debug("SpawnGenerator.spawnObject: name=" + getName() +
                ", created object " + obj + " at loc=" + loc);
        addDeathWatch(obj.getOid());
        obj.spawn();
        spawns.add(obj);
        updateObjectProperties(obj);
        if (Log.loggingDebug)
            Log.debug("SpawnGenerator.spawnObject: name=" + getName() +
                ", spawned obj " + obj);
        //Log.debug("SPAWN: mob has combat property: " + EnginePlugin.getObjectProperty(obj.getOid(), CombatClient.NAMESPACE, "combatstate"));
    }
    
    /**
     * Sets certain properties for the object that was just spawned.
     * @param obj
     */
    protected void updateObjectProperties(ObjectStub obj) {
    	HashMap<String, Serializable> props = (HashMap) spawnData.getProperty("props");
    	if (props != null) {
    		for (String prop : props.keySet()) {
    			EnginePlugin.setObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, prop, props.get(prop));
    		}
    	}
    	//TODO: remove all of the property setting below and incorporate 
    	String baseAction = (String) spawnData.getProperty("baseAction");
    	if (baseAction != null) {
    		EnginePlugin.setObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, "currentAction", baseAction);
    	}
    	/*boolean weaponsSheathed = spawnData.getBooleanProperty("weaponsSheathed");
    	EnginePlugin.setObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, "weaponsSheathed", weaponsSheathed);*/
    	// Merchant
    	Integer merchantTable = (Integer) spawnData.getProperty("merchantTable");
    	if (merchantTable != null) {
    		EnginePlugin.setObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, "merchantTable", merchantTable);
    	}
    	/*String otherUse = (String) spawnData.getProperty("otherUse");
    	if (otherUse != null) {
    		if (otherUse.equals("Arena Master"))
    		    EnginePlugin.setObjectProperty(obj.getOid(), WorldManagerClient.NAMESPACE, "arenaMaster", true);
    	}*/
    	// Display Updates
    	/*String gender = (String) spawnData.getProperty("genderOptions");
    	if (gender.contains("Either")) {
    		Random random = new Random();
    		if (random.nextInt(2) == 0)
    			gender = "Male";
    		else
    			gender = "Female";
    	}
    	
    	AgisMobPlugin.setDisplay(obj.getOid(), gender);*/
    }

    protected void spawnObject(int millis) {
        if (spawns == null)
            return;
        Log.debug("SpawnGenerator: adding spawn timer");
        Engine.getExecutor().schedule(this, millis, TimeUnit.MILLISECONDS);
    }

    // Called by scheduled executor
    public void run() {
        try {
            spawnObject();
        }
        catch (AORuntimeException e) {
            Log.exception("SpawnGenerator.run caught exception: ", e);
        }
    }

    protected void addDeathWatch(OID oid) {
        if (Log.loggingDebug)
            Log.debug("SpawnGenerator.addDeathWatch: oid=" + oid);
        SubjectFilter filter = new SubjectFilter(oid);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        Long sub = Engine.getAgent().createSubscription(filter, this);
        deathWatchMap.put(oid, sub);
    }
    protected void removeDeathWatch(OID oid) {
        Long sub = deathWatchMap.remove(oid);
        if (sub != null) {
            if (Log.loggingDebug)
                Log.debug("SpawnGenerator.removeDeathWatch: oid=" + oid);
            Engine.getAgent().removeSubscription(sub);
        }
    }
    
    public int getSpawnId()
    {
    	return spawnID;
    }
    
    public void setSpawnID(int spawnID)
    {
    	this.spawnID = spawnID;
    }

    public OID getInstanceOid()
    {
        return instanceOid;
    }

    public void setInstanceOid(OID oid)
    {
        if (instanceOid == null) {
            instanceOid = oid;
            addInstanceContent(this);
        }
        else
            throw new AORuntimeException("Cannot change SpawnGenerator instanceOid, from="+instanceOid + " to="+oid);
    }

    public void setName(String name) { this.name = name; }
    public String getName() { return name; }

    public void setLoc(Point p) { loc = p; }
    public Point getLoc() { return loc; }

    public void setOrientation(Quaternion o) { orient = o; }
    public Quaternion getOrientation() { return orient; }

    public int getSpawnRadius() { return spawnRadius; }
    public void setSpawnRadius(int radius) { spawnRadius = radius; }

    // how long after death does it take to respawn
    public int getRespawnTime() { return respawnTime; }
    public void setRespawnTime(int milliseconds) { respawnTime = milliseconds; }

    public int getNumSpawns() { return numSpawns; }
    public void setNumSpawns(int num) { numSpawns = num; }

    public int getCorpseDespawnTime() { return corpseDespawnTime; }
    public void setCorpseDespawnTime(int time) { corpseDespawnTime = time; }

    public ObjectFactory getObjectFactory() { return factory; }
    public void setObjectFactory(ObjectFactory factory) { this.factory = factory; }
    

    public SpawnData getSpawnData() { return spawnData; }

    public void setSpawnData(SpawnData spawnData) { this.spawnData = spawnData; }

    protected class CorpseDespawner implements Runnable {
    	public CorpseDespawner(ObjectStub obj) {
	    this.obj = obj;
    	}
    	protected ObjectStub obj;

    	public void run() {
            if (spawns == null)
                return;
            spawns.remove(obj);
            try {
                // ObjectStub.despawn() does a local unload then a
                // WM despawn.  The mob does not have a mob sub-object,
                // so the object manager unload won't affect the mob
                // manager local data structures.
            	obj.despawn();
            	ObjectManagerClient.unloadObject(obj.getOid());
            } catch (AORuntimeException e) {
            	Log.exception("SpawnGenerator.CorpseDespawner: exception: ", e);
	    	}
    	}
    }

    /**
     * Adds a SpawnGenerator to the instanceContent map.
     * @param spawnGen
     */
    private static void addInstanceContent(SpawnGenerator spawnGen)
    {
        synchronized (instanceContent) {
        	HashMap<String, SpawnGenerator> spawnGenList =
                instanceContent.get(spawnGen.getInstanceOid());
            if (spawnGenList == null) {
                spawnGenList = new HashMap<String, SpawnGenerator>();
                instanceContent.put(spawnGen.getInstanceOid(), spawnGenList);
            }
            if (spawnGen.getName() != null) {
            	spawnGenList.put(spawnGen.getName(), spawnGen);
            } else {
            	spawnGenList.put("InstanceSpawn_" + spawnGen.getSpawnId(), spawnGen);
            }
        }
    }

    /**
     * Deactivates all SpawnGenerators belonging to the instanceOid specified. 
     * @param instanceOid
     */
    public static void cleanupInstance(OID instanceOid)
    {
    	HashMap<String, SpawnGenerator> spawnGenList;
        synchronized (instanceContent) {
            spawnGenList = instanceContent.remove(instanceOid);
        }
        if (spawnGenList != null) {
            for (SpawnGenerator spawnGen : spawnGenList.values()) {
                spawnGen.deactivate();
            }
        }
    }
    
    /**
     * Deactivates and removes the SpawnGenerator based on the spawnName and instanceOid given. 
     * @param instanceOid
     * @param spawnID
     */
    public static void removeSpawnGenerator(OID instanceOid, String spawnName)
    {
    	HashMap<String, SpawnGenerator> spawnGenList;
        synchronized (instanceContent) {
            spawnGenList = instanceContent.get(instanceOid);
            Log.debug("AJ: instanceContent - " + instanceContent);
            Log.debug("AJ: spawnGenList - " + spawnGenList);
        }
        if (spawnGenList != null && spawnGenList.containsKey(spawnName)) {
            SpawnGenerator sg = spawnGenList.remove(spawnName);
            sg.deactivate();
        } else {
        	Log.debug("CORPSE: spawnName not found: " + spawnName);
        }
    }
    
    /**
     * Deactivates and removes the SpawnGenerator based on the spawnID and instanceOid given.
     * @param instanceOid
     * @param spawnName
     */
    public static void removeSpawnGeneratorByID(OID instanceOid, int spawnID)
    {
    	HashMap<String, SpawnGenerator> spawnGenList;
        synchronized (instanceContent) {
            spawnGenList = instanceContent.get(instanceOid);
            Log.debug("AJ: instanceContent - " + instanceContent);
            Log.debug("AJ: spawnGenList - " + spawnGenList);
        }
        if (spawnGenList != null) {
        	for (SpawnGenerator sg : spawnGenList.values()) {
        		Log.debug("DESPAWN: comparing spawnGen: " + sg.getSpawnId() + " against id: " + spawnID);
        		if (sg.getSpawnId() == spawnID) {
        			sg.deactivate();
        		}
        	}
        }
    }
    
    /**
     * Goes through all SpawnGenerators in the instanceOid and respawns all mobs that
     * match the mob template ID.
     * @param instanceOid
     * @param mobID
     */
    public static void respawnMatchingMobs(OID instanceOid, int mobID)
    {
    	HashMap<String, SpawnGenerator> spawnGenList;
        synchronized (instanceContent) {
            spawnGenList = instanceContent.get(instanceOid);
            Log.debug("AJ: instanceContent - " + instanceContent);
            Log.debug("AJ: spawnGenList - " + spawnGenList);
        }
        if (spawnGenList != null) {
            for (SpawnGenerator sg : spawnGenList.values()) {
            	if (sg.getObjectFactory().getTemplateID() == mobID) {
            		sg.deactivate();
            		sg.activate();
            	}
            }
        }
    }
    
    /**
     * Goes through all spawn generators and activates/deactivates them based on their active times.
     * @param hour
     * @param minute
     */
    /*public static void serverTimeUpdate(int hour, int minute) {
    	for(HashMap<String, SpawnGenerator> generators : instanceContent.values()) {
    		for(SpawnGenerator generator : generators.values()) {
    			if (generator.startHour == -1 || generator.endHour == -1)
    				continue;
    			
    			boolean withinTime = false;
    			if (generator.endHour < generator.startHour) {
					if (hour < generator.endHour || hour >= generator.startHour)
						withinTime = true;
				} else {
					if (hour >= generator.startHour && hour < generator.endHour)
						withinTime = true;
				}
    			
    			if (generator.active && !withinTime) {
    				Log.debug("TIME: deactivating spawnGenerator");
    				// Deactivate
    				generator.deactivateNotInCombat();
    			} else if (!generator.active && withinTime) {
    				// Activate
    				Log.debug("TIME: activating spawnGenerator");
    				generator.spawnMobs();
    			}
    		}
    	}
    }*/
    
    /**
     * Goes through all SpawnGenerators in the instanceOid and deactivates any that are
     * within the disabled area.
     * @param instanceOid
     * @param mobID
     */
    public static void disableSpawnsInArea(OID instanceOid, Point loc, float radius)
    {
    	HashMap<Point, Float> instanceDisabledAreas;
    	if (!disabledSpawnAreas.containsKey(instanceOid)) {
    		instanceDisabledAreas = new HashMap<Point, Float>();
    	} else {
    		instanceDisabledAreas = disabledSpawnAreas.get(instanceOid);
    	}
    	instanceDisabledAreas.put(loc, radius);
    	disabledSpawnAreas.put(instanceOid, instanceDisabledAreas);
    	
    	HashMap<String, SpawnGenerator> spawnGenList;
        synchronized (instanceContent) {
            spawnGenList = instanceContent.get(instanceOid);
            Log.debug("AJ: instanceContent - " + instanceContent);
            Log.debug("AJ: spawnGenList - " + spawnGenList);
        }
        if (spawnGenList != null) {
            for (SpawnGenerator sg : spawnGenList.values()) {
            	if (Point.distanceTo(sg.getLoc(), loc) < radius) {
            		sg.deactivate();
            	}
            }
        }
    }
    
    /**
     * Checks if the spawn generator is located within a disabled area.
     * @param instanceOid
     * @param loc
     * @return
     */
    public static boolean isSpawnInDisabledArea(OID instanceOid, Point loc) {
    	if (!disabledSpawnAreas.containsKey(instanceOid)) {
    		return false;
    	}
    	for (Point instanceDisabledArea : disabledSpawnAreas.get(instanceOid).keySet()) {
    		if (Point.distanceTo(instanceDisabledArea, loc) < disabledSpawnAreas.get(instanceOid).get(instanceDisabledArea)) {
        		return true;
        	}
    	}
    	return false;
    }

    protected int spawnID = -1;
    protected OID instanceOid = null;
    protected String name = null;
    protected Point loc = null;
    protected Quaternion orient = null;
    protected int spawnRadius = 0;
    protected int respawnTime = 0;
    protected int numSpawns = 3;
    protected int corpseDespawnTime = -1;
    protected int startHour = -1;
    protected int endHour = -1;
    protected boolean active = false;
    protected SpawnData spawnData = null;
    protected ObjectFactory factory = null;
    protected Map<OID, Long> deathWatchMap = new HashMap<OID, Long>();
    protected List<ObjectStub> spawns;

    private static Map<OID,HashMap<String, SpawnGenerator>> instanceContent =
        new HashMap<OID,HashMap<String, SpawnGenerator>>();
    
    private static Map<OID, HashMap<Point, Float>> disabledSpawnAreas = 
    		new HashMap<OID, HashMap<Point, Float>>();

    private static final long serialVersionUID = 1L;
}
