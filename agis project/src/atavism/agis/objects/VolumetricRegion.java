package atavism.agis.objects;

import atavism.agis.plugins.AgisWorldManagerClient;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.QuestClient;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.*;
import atavism.server.physics.CollisionCapsule;
import atavism.server.physics.CollisionOBB;
import atavism.server.physics.CollisionShape;
import atavism.server.physics.CollisionSphere;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * A ResourceNode is an object players can gather items from. The ResourceNode randomly generates its items upon spawn
 * from the items it has been given and allows the player to gather them if they meet the requirements.
 * @author Andrew Harrison
 *
 */
public class VolumetricRegion implements Serializable, MessageCallback, Runnable {
    public VolumetricRegion() {
    }
    
    public VolumetricRegion(int id, AOVector loc,  OID instanceOID) {
    	this.id = id;
    	this.loc = loc;
    	this.instanceOID = instanceOID;
    }
    
    public void addShape(String shapeType, AOVector loc, AOVector loc2, Quaternion orient, float size1, float size2, float size3) {
    	if (shapeType.equals("sphere")) {
    		shapes.add(new CollisionSphere(loc, size1));
    	} else if (shapeType.equals("capsule")) {
    		shapes.add(new CollisionCapsule(loc, loc2, size1));
    	} else if (shapeType.equals("box")) {
    		AOVector[] axes = new AOVector[3];
    		axes[0] = orient.getXAxis();
    		axes[1] = orient.getYAxis();
    		axes[2] = orient.getZAxis();
    		AOVector extents = new AOVector(size1 / 2, size2 / 2, size3 / 2);
    		shapes.add(new CollisionOBB(loc, axes, extents));
    	}
    }
    
    /**
     * Subscribes the instance to receive certain relevant messages that are sent to the world object 
     * created by this instance.
     */
    public void activate() {
    	SubjectFilter filter = new SubjectFilter(objectOID);
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        eventSub = Engine.getAgent().createSubscription(filter, this);
        
        if (regionType.equals("Water")) {
        	Engine.getExecutor().scheduleAtFixedRate(this, 5000, 250, TimeUnit.MICROSECONDS);
        } else {
        	MessageTypeFilter filter2 = new MessageTypeFilter();
        	filter2.addType(WorldManagerClient.MSG_TYPE_UPDATEWNODE);
        	sub = Engine.getAgent().createSubscription(filter2, this);
        }
        
        Log.debug("REGION: activating region: " + id);
        // Set the reaction radius tracker to alert the object if a player has entered its draw radius
        // Get two furtherest points on the shape
        AOVector minLoc = loc;
	    AOVector maxLoc = loc;
	    float largestSize = 0;
	    for (CollisionShape shape : shapes) {
	        if (shape.center.getX() < minLoc.getX()) {
	        	minLoc.setX(shape.center.getX());
	        } else if (shape.center.getX() > maxLoc.getX()) {
	        	maxLoc.setX(shape.center.getX());
	        }
	        	
	        if (shape.center.getZ() < minLoc.getZ()) {
	        	minLoc.setZ(shape.center.getZ());
	        } else if (shape.center.getZ() > maxLoc.getZ()) {
	        	maxLoc.setZ(shape.center.getZ());
	        }
	        	
	        if (shape instanceof CollisionOBB) {
	        	CollisionOBB obb = (CollisionOBB)shape;
	        	if (obb.extents.getX() > largestSize) {
	        		largestSize = obb.extents.getX();
	        	}
	        	if (obb.extents.getZ() > largestSize) {
	        		largestSize = obb.extents.getZ();
	        	}
	        } else if (shape.radius < largestSize) {
	        	largestSize = shape.radius;
	        }
	    }
	        
	    float distance = AOVector.distanceTo(minLoc, maxLoc);
	    Log.debug("REGION: got distance: " + distance + " and largestSize: " + largestSize);
		reactionRadius = distance + largestSize + 20;
       
        MobManagerPlugin.getTracker(instanceOID).addReactionRadius(objectOID, (int)(getReactionRadius()));
        active = true;
        Log.debug("REGION: node with oid: " + objectOID + " activated with radius: " + (getReactionRadius()));
    }
    
    /**
     * Deals with the messages the instance has picked up.
     */
    public void handleMessage(Message msg, int flags) {
    	if (active == false) {
    	    return;
    	}
    	if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS) {
    	    ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage)msg;
     	    Log.debug("REGION: myOid=" + objectOID + " objOid=" + nMsg.getSubject()
     		      + " inRadius=" + nMsg.getInRadius() + " wasInRadius=" + nMsg.getWasInRadius());
    	    if (nMsg.getInRadius()) {
    	    	addPlayer(nMsg.getSubject());
    	    } else {
    	    	// Remove subject from targets in range
    	    	removePlayer(nMsg.getSubject());
    	    }
    	} else if (msg instanceof WorldManagerClient.UpdateWorldNodeMessage) {
            WorldManagerClient.UpdateWorldNodeMessage updateMsg = (WorldManagerClient.UpdateWorldNodeMessage) msg;
            processUpdateMsg(updateMsg);
        }
    }
    
    public void processUpdateMsg(WorldManagerClient.UpdateWorldNodeMessage msg) {
        OID playerOid = msg.getSubject();
        Log.debug("REGION: got updateMsg for: " + playerOid);
        
        if (!playersInRange.contains(playerOid)) {
        	Log.debug("REGION: does not contain: " + playerOid);
        	return;
        }
        
        boolean playerInShape = false;
        for(CollisionShape shape : shapes) {
        	if (shape.pointInside(new AOVector(msg.getWorldNode().getLoc()))) {
        		playerInShape = true;
        		break;
        	}
        }
        
        Log.debug("REGION: playerInShape: " + playerInShape + " with loc: " + msg.getWorldNode().getLoc());
        
        if (playersInRegion.contains(playerOid)) {
        	// Is the player still in the region
        	if (!playerInShape) {
        		playerLeftRegion(playerOid);
        	}
        } else {
        	// Has the player now moved into the region
        	if (playerInShape) {
        		playerEnteredRegion(playerOid);
        	}
        }
    }
    
    /**
     * An external call to spawn a world object for the claim.
     * @param instanceOID
     */
    public void spawn(OID instanceOID) {
    	this.instanceOID = instanceOID;
    	spawn();
    }
    
    /**
     * Spawn a world object for the claim.
     */
    public void spawn() {
    	Template markerTemplate = new Template();
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_NAME, "_ign_region" + id);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.mob);
    	markerTemplate.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_INSTANCE, instanceOID);
    	markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_LOC, new Point(loc));
    	//markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_ORIENT, orientation);
    	DisplayContext dc = new DisplayContext("", true);
		dc.addSubmesh(new DisplayContext.Submesh("", ""));
		markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
		markerTemplate.put(Namespace.WORLD_MANAGER, "model", ""); 
    	// Put in any additional props
    	if (props != null) {
    		for (String propName : props.keySet()) {
    			markerTemplate.put(Namespace.WORLD_MANAGER, propName, props.get(propName));
    		}
    	}
    	// Create the object
    	objectOID = ObjectManagerClient.generateObject(ObjectManagerClient.BASE_TEMPLATE_ID,
                ObjectManagerClient.BASE_TEMPLATE, markerTemplate);
    	
    	if (objectOID != null) {
    		// Need to create an interpolated world node to add a tracker/reaction radius to the claim world object
    		BasicWorldNode bwNode = WorldManagerClient.getWorldNode(objectOID);
    		InterpolatedWorldNode iwNode = new InterpolatedWorldNode(bwNode);
    		resourceNodeEntity = new ResourceNodeEntity(objectOID, iwNode);
    		EntityManager.registerEntityByNamespace(resourceNodeEntity, Namespace.MOB);
    		MobManagerPlugin.getTracker(instanceOID).addLocalObject(objectOID, 100);
    		
            WorldManagerClient.spawn(objectOID);
            Log.debug("REGION: spawned resource at : " + loc);
            activate();
        }
    }
    
    /**
     * Add a player to the update list for this ResourceNode. The player will receive data about the node and any updates
     * that occur.
     * @param playerOID
     */
    public void addPlayer(OID playerOid) {
    	Log.debug("REGION: added player: " + playerOid);
    	// Send down the state to the player
			
		if (!playersInRange.contains(playerOid)) {
	    	playersInRange.add(playerOid);
	    }
		
		/*BasicWorldNode casterWNode = WorldManagerClient.getWorldNode(playerOid);
        float distance = AOVector.distanceTo(loc, new AOVector(casterWNode.getLoc()));
        // Has the player now moved into the region
    	if (distance < size1) {
    		playerEnteredRegion(playerOid);
    	}*/
    }
    
    /**
     * Removes a player from the ResourceNode. They will no longer receive updates.
     * @param playerOID
     * @param removeLastID
     */
    public void removePlayer(OID playerOid) {
    	Log.debug("REGION: removed player: " + playerOid);
    	if (playersInRange.contains(playerOid))
    		playersInRange.remove(playerOid);
    	
    	if (playersInRegion.contains(playerOid)) {
    		playerLeftRegion(playerOid);
    	}
    	
    	if (playersUnderwater.contains(playerOid)) {
    		playersUnderwater.remove(playerOid);
    		if (actionID > 0)
        		CombatClient.removeEffect(playerOid, actionID);
    	}
    }
    
    void playerEnteredRegion(OID playerOid) {
    	Log.debug("REGION: adding player to region: " + playerOid);
    	playersInRegion.add(playerOid);
    	
    	if (regionType.equals("Water")) {
    		AgisWorldManagerClient.sendWaterRegionTransitionMessage(playerOid, id, true);
    	} else if (regionType.equals("Dismount")) {
    		HashMap<String, Serializable> props = new HashMap<String, Serializable>();
    		ExtensionMessage eMsg = new WorldManagerClient.ExtensionMessage(CombatClient.MSG_TYPE_DISMOUNT, playerOid, props);
    		Engine.getAgent().sendBroadcast(eMsg);
    	} else if (regionType.equals("Teleport")) {
    		AgisWorldManagerClient.sendChangeInstance(playerOid, actionID, 
    				new Point(Float.parseFloat(actionData1),Float.parseFloat(actionData2),Float.parseFloat(actionData3)));
    	} else if (regionType.equals("CompleteTask")) {
    		QuestClient.TaskUpdateMessage msg = new QuestClient.TaskUpdateMessage(playerOid, actionID, 1);
    		Engine.getAgent().sendBroadcast(msg);
    	} else if (regionType.equals("StartQuest")) {
    		LinkedList<Integer> quests = new LinkedList<Integer>();
        	quests.add(actionID);
        	QuestClient.offerQuestToPlayer(playerOid, objectOID, quests, false);
    	} else {
    		if (actionID > 0)
        		CombatClient.applyEffect(playerOid, actionID);
    	}
    }
    
    void playerLeftRegion(OID playerOid) {
    	Log.debug("REGION: removing player from region: " + playerOid);
    	playersInRegion.remove(playerOid);
    	
    	if (regionType.equals("Water")) {
    		AgisWorldManagerClient.sendWaterRegionTransitionMessage(playerOid, id, false);
    		
    		if (playersUnderwater.contains(playerOid)) {
        		playersUnderwater.remove(playerOid);
        		CombatClient.removeEffect(playerOid, actionID);
        	}
    	} else {
    		if (actionID > 0)
        		CombatClient.removeEffect(playerOid, actionID);
    	}
    }
    

	@Override
	public void run() {
		// Do Check of all players to see if they are now in or out of the region
		for (OID playerOid : playersInRange) {
			boolean playerInShape = false;
			boolean playerUnderWater = false;
	        for(CollisionShape shape : shapes) {
	        	Entity entity = EntityManager.getEntityByNamespace(playerOid, Namespace.WORLD_MANAGER);
	    		// get the object's current world node
	            AOObject obj = (AOObject) entity;
	            InterpolatedWorldNode curWnode = (InterpolatedWorldNode) obj.worldNode();
	        	//BasicWorldNode wNode = WorldManagerClient.getWorldNode(playerOid);
	        	if (shape.pointInside(new AOVector(curWnode.getInterpLoc()))) {
	        		playerInShape = true;
	        		// Also check if they are a a full metre underwater for breath
	        		AOVector abovePosition = new AOVector(curWnode.getInterpLoc().getX(), curWnode.getInterpLoc().getY() + 1, 
	        				curWnode.getInterpLoc().getZ());
	        		if (shape.pointInside(abovePosition)) {
	        			playerUnderWater = true;
	        		}
	        		break;
	        	}
	        }
	        
	        if (playersInRegion.contains(playerOid)) {
	        	// Is the player still in the region
	        	if (!playerInShape) {
	        		playerLeftRegion(playerOid);
	        	}
	        } else {
	        	// Has the player now moved into the region
	        	if (playerInShape) {
	        		playerEnteredRegion(playerOid);
	        	}
	        }
	        
	        if (playersUnderwater.contains(playerOid)) {
	        	// Is the player still in the region
	        	if (!playerUnderWater && actionID > 0) {
	        		playersUnderwater.remove(playerOid);
	            	CombatClient.removeEffect(playerOid, actionID);
	        	}
	        } else {
	        	// Has the player now moved into the region
	        	if (playerUnderWater && actionID > 0) {
	        		Log.debug("UNDER: player is now under water");
	        		playersUnderwater.add(playerOid);
	        		CombatClient.applyEffect(playerOid, actionID);
	        	}
	        }
		}
	}
	
	public float getReactionRadius() {
		return reactionRadius;
	}

	public int getID() { return id; }
    public void setID(int id) {
    	this.id = id;
    }
    
    public String getName() { return name; }
    public void setName(String name) {
    	this.name = name;
    }
    
    public AOVector getLoc() { return loc; }
    public void setLoc(AOVector loc) {
    	this.loc = loc;
    }
    
    public HashMap<String, Serializable> getProps() { return props; }
    public void setProps(HashMap<String, Serializable> props) {
    	this.props = props;
    }
    
    public OID getInstanceOID() { return instanceOID; }
    public void setInstanceOID(OID instanceOID) {
    	this.instanceOID = instanceOID;
    }
    
    public OID getObjectOID() { return objectOID; }
    public void setObjectOID(OID objectOID) {
    	this.objectOID = objectOID;
    }
    
    public float getSize1() { return size1; }
    public void setSize1(float size1) {
    	this.size1 = size1;
    }
    
    public String getRegionType() { return regionType; }
    public void setRegionType(String regionType) {
    	this.regionType = regionType;
    }
    
    public int getActionID() { return actionID; }
    public void setActionID(int actionID) {
    	this.actionID = actionID;
    }
    
    public String getActionData1() { return actionData1; }
    public void setActionData1(String actionData1) {
    	this.actionData1 = actionData1;
    }
    
    public String getActionData2() { return actionData2; }
    public void setActionData2(String actionData2) {
    	this.actionData2 = actionData2;
    }
    
    public String getActionData3() { return actionData3; }
    public void setActionData3(String actionData3) {
    	this.actionData3 = actionData3;
    }

    public boolean getActive() { return active; }
    public void setActive(boolean active) {
    	this.active = active;
    }

    int id;
    String name;
    String regionType;
    AOVector loc;
    float reactionRadius;
    OID instanceOID;
    OID objectOID;
    HashMap<String, Serializable> props;
    ArrayList<CollisionShape> shapes = new ArrayList<CollisionShape>();
    float size1;
    float size2;
    float size3;
    int actionID;
    String actionData1;
    String actionData2;
    String actionData3;
    boolean active;
    Long eventSub = null;
    LinkedList<OID> playersInRange = new LinkedList<OID>();
    LinkedList<OID> playersInRegion = new LinkedList<OID>();
    LinkedList<OID> playersUnderwater = new LinkedList<OID>();
    
    Long sub = null;
    ResourceNodeEntity resourceNodeEntity;
    

    /**
     * Sub-class needed for the interpolated world node so a perceiver can be created.
     * @author Andrew
     *
     */
	public class ResourceNodeEntity extends Entity implements EntityWithWorldNode
	{

		public ResourceNodeEntity(OID oid, InterpolatedWorldNode node) {
	    	setWorldNode(node);
	    	setOid(oid);
	    }
		
		public InterpolatedWorldNode getWorldNode() { return node; }
	    public void setWorldNode(InterpolatedWorldNode node) { this.node = node; }
	    InterpolatedWorldNode node;

		@Override
		public void setDirLocOrient(BasicWorldNode bnode) {
			if (node != null)
	            node.setDirLocOrient(bnode);
		}

		@Override
		public Entity getEntity() {
			return (Entity)this;
		}
		
		private static final long serialVersionUID = 1L;
	}
	
	private static final long serialVersionUID = 1L;
	
}
