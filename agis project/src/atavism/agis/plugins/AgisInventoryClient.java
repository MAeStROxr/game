package atavism.agis.plugins;

import atavism.agis.objects.AgisEquipSlot;
import atavism.agis.objects.AgisItem;
import atavism.msgsys.MessageType;
import atavism.msgsys.SubjectMessage;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.Template;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


/**
 * AGIS-specific calls for sending/getting messages to the AgisInventoryPlugin
 */
public class AgisInventoryClient {
	
	/**
	 * Sends the RemoveOrFindItemMessage, returning the OID of the item if finds.
	 * @param mobOid
	 * @param slot
	 * @return
	 */
    public static OID findItem(OID mobOid, AgisEquipSlot slot) {
        InventoryClient.RemoveOrFindItemMessage msg = 
            new InventoryClient.RemoveOrFindItemMessage(MSG_TYPE_AGIS_INV_FIND, mobOid, INV_METHOD_SLOT, slot);
	    OID oid = Engine.getAgent().sendRPCReturnOID(msg);
        Log.debug("findItem: got response");
        return oid;
    }

    /**
     * Sends a message to start a Trade.
     * @param requesterOid
     * @param partnerOid
     */
    public static void tradeStart(OID requesterOid, OID partnerOid) {
        Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("requesterOid", requesterOid);
        props.put("partnerOid", partnerOid);
        ExtensionMessage msg = new ExtensionMessage(MSG_TYPE_TRADE_START_REQ, requesterOid, props);
        Engine.getAgent().sendBroadcast(msg);
    }

    /**
     * Sends a message to update the status of the trade (such as the items offered has changed or a player has accepted/declined).
     * @param requesterOid
     * @param partnerOid
     * @param offerItems
     * @param accepted
     * @param cancelled
     */
    public static void tradeUpdate(OID requesterOid, OID partnerOid,
				   LinkedList<OID> offerItems, boolean accepted, boolean cancelled) {
        Log.debug("AgisInventoryClient.tradeUpdate: requesterOid=" + requesterOid + " partnerOid="
		  + partnerOid + " offer=" + offerItems + " accepted=" + accepted + " cancelled=" + cancelled);
        Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("requesterOid", requesterOid);
        props.put("partnerOid", partnerOid);
        props.put("offerItems", offerItems);
        props.put("accepted", accepted);
        props.put("cancelled", cancelled);
        ExtensionMessage msg = new ExtensionMessage(MSG_TYPE_TRADE_OFFER_REQ, requesterOid, props);
        Engine.getAgent().sendBroadcast(msg);
    }
    
    /**
     * Sends the ItemAcquriedStatusMessage telling the system that the player has either
     * acquired or lost an item.
     * @param oid
     * @param item
     * @param equipping
     */
    public static void itemAcquiredStatusChange(OID oid, AgisItem item, boolean acquried) {
    	ItemAcquiredStatusMessage msg = new ItemAcquiredStatusMessage(oid, item, acquried);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("INV: ItemAcquiredStatusMessage sent");
	}

    /**
     * Message indicating that a player has either acquired or lost an item.
     * @author Andrew Harrison
     *
     */
	public static class ItemAcquiredStatusMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public ItemAcquiredStatusMessage() {
            super();
        }
		public ItemAcquiredStatusMessage(OID oid, AgisItem item, boolean acquried) {
			super(oid);
			setMsgType(MSG_TYPE_ITEM_ACQUIRE_STATUS_CHANGE);
			setItem(item);
			setAcquired(acquried);
		}
		
		public AgisItem getItem() {
			return item;
		}
		public void setItem(AgisItem item) {
			this.item = item;
		}
		
		public boolean getAcquired() {
			return acquired;
		}
		public void setAcquired(boolean acquired) {
			this.acquired = acquired;
		}
		
		private AgisItem item;
		private boolean acquired;
	}
    
    /**
     * Sends the ItemEquipStatusMessage telling the system that an item has been equipped.
     * @param oid
     * @param item
     * @param equipping
     */
    public static void itemEquipStatusChanged(OID oid, AgisItem item, boolean equipping, String slot) {
    	ItemEquipStatusMessage msg = new ItemEquipStatusMessage(oid, item, equipping, slot);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("INV: ItemEquipStatusMessage sent");
	}

    /**
     * Message indicating an item is being equipped or unequipped.
     * @author Andrew Harrison
     *
     */
	public static class ItemEquipStatusMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public ItemEquipStatusMessage() {
            super();
        }
		public ItemEquipStatusMessage(OID oid, AgisItem item, boolean equipping, String slot) {
			super(oid);
			setMsgType(MSG_TYPE_ITEM_EQUIP_STATUS_CHANGE);
			setItem(item);
			setEquipped(equipping);
			setSlot(slot);
		}
		
		public AgisItem getItem() {
			return item;
		}
		public void setItem(AgisItem item) {
			this.item = item;
		}
		
		public boolean getEquipped() {
			return equipped;
		}
		public void setEquipped(boolean equipped) {
			this.equipped = equipped;
		}
		
		public String getSlot() {
			return slot;
		}
		public void setSlot(String slot) {
			this.slot = slot;
		}
		
		private AgisItem item;
		private boolean equipped;
		private String slot;
	}
	
	/**
	 * Sends the RequestOpenMobMessage
	 * @param mobOid
	 * @param playerOid
	 */
	public static void requestOpenMob(OID mobOid, OID playerOid) {
		RequestOpenMobMessage msg = new RequestOpenMobMessage(mobOid, playerOid);
        Engine.getAgent().sendBroadcast(msg);
    }

	/**
	 * Message indicating the player is trying to open a mob (likely a chest or other resource object).
	 * @author Andrew
	 *
	 */
	public static class RequestOpenMobMessage extends SubjectMessage {

        public RequestOpenMobMessage() {
            super(MSG_TYPE_REQ_OPEN_MOB);
        }

        RequestOpenMobMessage(OID npcOid, OID playerOid) {
            super(MSG_TYPE_REQ_OPEN_MOB, npcOid);
            setPlayerOid(playerOid);
        }
        
        OID playerOid = null;

        public OID getPlayerOid() {
            return playerOid;
        }

        public void setPlayerOid(OID playerOid) {
            this.playerOid = playerOid;
        }

        private static final long serialVersionUID = 1L;
    }
	
	/**
	 * Sends the RemoveSpecificItemMessage.
	 * @param oid
	 * @param itemOid
	 * @param removeStack
	 * @param numToRemove
	 */
	public static void removeSpecificItem(OID oid, OID itemOid, boolean removeStack, int numToRemove) {
		removeSpecificItemMessage msg = new removeSpecificItemMessage(oid, itemOid, removeStack, numToRemove);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client removeSpecificItemMessage hit 1");
	}
	
	public static void removeSpecificItem(OID oid, HashMap<OID, Integer> itemsToRemove, boolean removeStack) {
		removeSpecificItemMessage msg = new removeSpecificItemMessage(oid, itemsToRemove, removeStack);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client removeSpecificItemMessage hit 1");
	}

	/**
	 * Message used to remove a specific item from the players inventory. Requires the OID of the item to be removed
	 * along with the amount to remove.
	 * @author Andrew Harrison
	 *
	 */
	public static class removeSpecificItemMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public removeSpecificItemMessage() {
            super();
        }
		public removeSpecificItemMessage(OID oid, OID itemOid, boolean removeStack, int numToRemove) {
			super(oid);
			setMsgType(MSG_TYPE_REMOVE_SPECIFIC_ITEM);
			itemsToRemove = new HashMap<OID, Integer>();
			itemsToRemove.put(itemOid, numToRemove);
			setRemoveStack(removeStack);
			Log.debug("ITEM: client removeSpecificItemMessage hit 2");
		}
		public removeSpecificItemMessage(OID oid, HashMap<OID, Integer> itemsToRemove, boolean removeStack) {
			super(oid);
			setMsgType(MSG_TYPE_REMOVE_SPECIFIC_ITEM);
			setItemsToRemove(itemsToRemove);
			setRemoveStack(removeStack);
			Log.debug("ITEM: client removeSpecificItemMessage hit 2");
		}
		
		public HashMap<OID, Integer> getItemsToRemove() {
            return itemsToRemove;
        }
        public void setItemsToRemove(HashMap<OID, Integer> itemsToRemove) {
            this.itemsToRemove = itemsToRemove;
        }
		HashMap<OID, Integer> itemsToRemove;
		
		public boolean getRemoveStack() {
            return removeStack;
        }
        public void setRemoveStack(boolean removeStack) {
            this.removeStack = removeStack;
        }
		boolean removeStack;
	}
	
	/**
	 * Sends the RemoveGenericItemMessage.
	 * @param oid
	 * @param itemID
	 * @param removeStack
	 * @param numToRemove
	 */
	public static void removeGenericItem(OID oid, int itemID, boolean removeStack, int numToRemove) {
		removeGenericItemMessage msg = new removeGenericItemMessage(oid, itemID, removeStack, numToRemove);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client removeGenericItemMessage hit 1");
	}
	
	public static void removeGenericItems(OID oid, HashMap<Integer, Integer> itemsToRemove, boolean removeStack) {
		removeGenericItemMessage msg = new removeGenericItemMessage(oid, itemsToRemove, removeStack);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client removeGenericItemMessage hit 1");
	}

	/**
	 * Message used to remove an item from the players inventory based on the type of item. Requires the
	 * templateID of the item to be removed along with how many to remove.
	 * @author Andrew
	 *
	 */
	public static class removeGenericItemMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public removeGenericItemMessage() {
            super();
        }
		public removeGenericItemMessage(OID oid, int itemID, boolean removeStack, int numToRemove) {
			super(oid);
			Log.debug("ITEM: client removeGenericItemMessage hit 2");
			setMsgType(MSG_TYPE_REMOVE_GENERIC_ITEM);
			itemsToRemove = new HashMap<Integer, Integer>();
			itemsToRemove.put(itemID, numToRemove);
			removeStack(removeStack);
			Log.debug("ITEM: client removeGenericItemMessage hit 3");
		}
		
		public removeGenericItemMessage(OID oid, HashMap<Integer, Integer> itemsToRemove, boolean removeStack) {
			super(oid);
			Log.debug("ITEM: client removeGenericItemMessage hit 2");
			setMsgType(MSG_TYPE_REMOVE_GENERIC_ITEM);
			setItemsToRemove(itemsToRemove);
			removeStack(removeStack);
			Log.debug("ITEM: client removeGenericItemMessage hit 3");
		}
		
		public HashMap<Integer, Integer> getItemsToRemove() {
            return itemsToRemove;
        }
        public void setItemsToRemove(HashMap<Integer, Integer> itemsToRemove) {
            this.itemsToRemove = itemsToRemove;
        }
		HashMap<Integer, Integer> itemsToRemove;
		
		public boolean removeStack() {
            return removeStack;
        }
        public void removeStack(boolean removeStack) {
            this.removeStack = removeStack;
        }
		boolean removeStack;
	}
	
	/**
	 * Sends the GetSpecificItemDataMessage. Used to send down information about an item to a players client.
	 * @param oid
	 * @param targetOid
	 * @param itemOids
	 */
	public static void getSpecificItemData(OID oid, OID targetOid, ArrayList<Long> itemOids) {
		getSpecificItemDataMessage msg = new getSpecificItemDataMessage(oid, targetOid, itemOids);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client getSpecificItemData hit 1");
	}

	/**
	 * Message used to request information about a list of items to be sent down to a players client.
	 * @author Andrew Harrison
	 *
	 */
	public static class getSpecificItemDataMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public getSpecificItemDataMessage() {
            super();
        }
		public getSpecificItemDataMessage(OID oid, OID targetOid, ArrayList<Long> itemOids) {
			super(oid);
			setMsgType(MSG_TYPE_GET_SPECIFIC_ITEM_DATA);
			setProperty("itemOids", itemOids);
			setProperty("targetOid", targetOid);
			Log.debug("ITEM: client getSpecificItemDataMessage hit 2");
		}
	}
	
	/**
	 * Sends a Message asking for the Template for an item to be returned.
	 * @param oid
	 * @param itemID
	 * @return
	 */
	public static Template getGenericItemData(OID oid, int itemID) {
		ExtensionMessage msg = new ExtensionMessage(MSG_TYPE_GET_GENERIC_ITEM_DATA, null, oid);
		msg.setProperty("itemID", itemID);
		msg.setProperty("dataType", "id");
		Log.debug("ITEM: client getGenericItemData hit 1");
		return (Template) Engine.getAgent().sendRPCReturnObject(msg);
	}

	/**
	 * Unused Message class, look to remove this in the near future as it looks to be obsolete.
	 * @author Andrew Harrison
	 *
	 */
	public static class getGenericItemDataMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public getGenericItemDataMessage() {
            super();
        }
		public getGenericItemDataMessage(OID oid, String itemName) {
			super(oid);
			setMsgType(MSG_TYPE_GET_GENERIC_ITEM_DATA);
			setProperty("itemName", itemName);
			Log.debug("ITEM: client getGenericItemDataMessage hit 2");
		}
	}
	
	/**
	 * Sends the GenerateItemMessage.
	 * @param oid
	 * @param templateID
	 * @param itemName
	 * @param itemProps
	 */
	public static void generateItem(OID oid, int templateID, String itemName, int count, HashMap<String, Serializable> itemProps) {
		generateItemMessage msg = new generateItemMessage(oid, templateID, itemName, count, itemProps);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client generateItem hit 1");
	}

	/**
	 * Message used to request the generation of an item based on the templateID specified and then to add the item
	 * to the players inventory.
	 * @author Andrew Harrison
	 *
	 */
	public static class generateItemMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public generateItemMessage() {
            super();
        }
		public generateItemMessage(OID oid, int templateID, String itemName, int count, HashMap<String, Serializable> itemProps) {
			super(oid);
			setMsgType(MSG_TYPE_GENERATE_ITEM);
			setProperty("itemID", templateID);
			setProperty("itemName", itemName);
			setProperty("count", count);
			setProperty("itemProps", itemProps);
			
			Log.debug("ITEM: client generateItemMessage hit 2");
		}
	}
	
	/**
	 * Sends the PlaceBagMessage.
	 * @param oid
	 * @param itemOid
	 * @param bagSpotNum
	 */
	public static void placeBag(OID oid, OID itemOid, int bagSpotNum) {
		placeBagMessage msg = new placeBagMessage(oid, itemOid, bagSpotNum);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client placeBag hit 1");
	}

	/**
	 * Message used to put a bag item from the players inventory into a bag slot. 
	 * Do not use this to move a bag from one slot to another.
	 * @author Andrew Harrison
	 *
	 */
	public static class placeBagMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public placeBagMessage() {
            super();
        }
		public placeBagMessage(OID oid, OID itemOid, int bagSpotNum) {
			super(oid);
			setMsgType(MSG_TYPE_PLACE_BAG);
			setProperty("itemOid", itemOid);
			setProperty("bagSpotNum", bagSpotNum);
			Log.debug("ITEM: client placeBagMessage hit 2");
		}
	}
	
	/**
	 * Sends the MoveBagMessage.
	 * @param oid
	 * @param bagSpotNum
	 * @param newSpotNum
	 */
	public static void moveBag(OID oid, int bagSpotNum, int newSpotNum) {
		moveBagMessage msg = new moveBagMessage(oid, bagSpotNum, newSpotNum);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client moveBagMessage hit 1");
	}

	/**
	 * Message used to move a bag from one bag slot into another.
	 * @author Andrew Harrison
	 *
	 */
	public static class moveBagMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public moveBagMessage() {
            super();
        }
		public moveBagMessage(OID oid, int bagSpotNum, int newSpotNum) {
			super(oid);
			setMsgType(MSG_TYPE_MOVE_BAG);
			setProperty("bagSpotNum", bagSpotNum);
			setProperty("newSpotNum", newSpotNum);
			Log.debug("ITEM: client moveBagMessage hit 2");
		}
	}
	
	/**
	 * Sends the RemoveBagMessage.
	 * @param oid
	 * @param bagSpotNum
	 * @param containerId
	 * @param slotId
	 */
	public static void removeBag(OID oid, int bagSpotNum, int containerId, int slotId) {
		removeBagMessage msg = new removeBagMessage(oid, bagSpotNum, containerId, slotId);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client removeBagMessage hit 1");
	}

	/**
	 * Message used to remove a bag from the players bag slots and put it back into the inventory.
	 * @author Andrew Harrison
	 *
	 */
	public static class removeBagMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public removeBagMessage() {
            super();
        }
		public removeBagMessage(OID oid, int bagSpotNum, int containerId, int slotId) {
			super(oid);
			setMsgType(MSG_TYPE_REMOVE_BAG);
			setProperty("bagSpotNum", bagSpotNum);
			setProperty("containerId", containerId);
			setProperty("slotId", slotId);
			Log.debug("ITEM: client removeBagMessage hit 2");
		}
	}
	
	/**
	 * Sends the LootItemMessage.
	 * @param oid
	 * @param itemOid
	 * @param mobOid
	 */
	public static void lootItem(OID oid, OID itemOid, OID mobOid) {
		lootItemMessage msg = new lootItemMessage(oid, itemOid, mobOid);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client lootItemMessage hit 1");
	}

	/**
	 * Message used to indicate the player wants to loot the specified the item 
	 * from the specified mob.
	 * @author Andrew Harrison
	 *
	 */
	public static class lootItemMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public lootItemMessage() {
            super();
        }
		public lootItemMessage(OID oid, OID itemOid, OID mobOid) {
			super(oid);
			setMsgType(MSG_TYPE_LOOT_ITEM);
			setProperty("mobOid", mobOid);
			setProperty("itemOid", itemOid);
			Log.debug("ITEM: client lootItemMessage hit 2");
		}
	}
	
	/**
	 * Sends the LootAllMessage.
	 * @param oid
	 * @param mobOid
	 */
	public static void lootAll(OID oid, OID mobOid) {
		lootAllMessage msg = new lootAllMessage(oid, mobOid);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client lootAllMessage hit 1");
	}

	/**
	 * Message used to indicate a player wants to loot all items from
	 * the specified mob.
	 * @author Andrew Harrison
	 *
	 */
	public static class lootAllMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public lootAllMessage() {
            super();
        }
		public lootAllMessage(OID oid, OID mobOid) {
			super(oid);
			setMsgType(MSG_TYPE_LOOT_ALL);
			setProperty("mobOid", mobOid);
			Log.debug("ITEM: client lootAllMessage hit 2");
		}
	}
	
	/**
	 * Sends the GenerateLootMessage.
	 * @param oid
	 */
	public static void generateLoot(OID oid) {
		generateLootMessage msg = new generateLootMessage(oid);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client generateLootMessage hit 1");
	}
	
	/**
	 * Sends the GenerateLootMessage.
	 * @param oid
	 */
	public static void generatePlayerLoot(OID oid, Point loc) {
		generateLootMessage msg = new generateLootMessage(oid, true, loc);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client generateLootMessage hit 1");
	}

	/**
	 * Message used to request the generation of loot for a mob.
	 * @author Andrew Harrison
	 *
	 */
	public static class generateLootMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public generateLootMessage() {
            super();
        }
		public generateLootMessage(OID oid) {
			super(oid);
			setMsgType(MSG_TYPE_GENERATE_LOOT);
			Log.debug("ITEM: generateLootMessage created");
		}
		public generateLootMessage(OID oid, boolean isPlayer) {
			super(oid);
			setMsgType(MSG_TYPE_GENERATE_LOOT);
			setIsPlayer(isPlayer);
			Log.debug("ITEM: generateLootMessage created with isPlayer");
		}
		public generateLootMessage(OID oid, boolean isPlayer, Point loc) {
			super(oid);
			setMsgType(MSG_TYPE_GENERATE_LOOT);
			setIsPlayer(isPlayer);
			setLoc(loc);
			Log.debug("ITEM: generateLootMessage created with isPlayer");
		}
		
		boolean isPlayer = false;
		public boolean getIsPlayer() {
			return isPlayer;
		}
		public void setIsPlayer(boolean isPlayer) {
			this.isPlayer = isPlayer;
		}
		
		Point loc;
		public Point getLoc() {
			return loc;
		}
		public void setLoc(Point loc) {
			this.loc = loc;
		}
	}
	
	/**
	 * Sends the GetLootListMessage.
	 * @param oid
	 * @param mobOid
	 */
	public static void getLootList(OID oid, OID mobOid) {
		getLootListMessage msg = new getLootListMessage(oid, mobOid);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client getLootListMessage hit 1");
	}

	/**
	 * Message used to request the list of loot for the specified mob
	 * is sent down to the requesting player.
	 * @author Andrew Harrison
	 *
	 */
	public static class getLootListMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public getLootListMessage() {
            super();
        }
		public getLootListMessage(OID oid, OID mobOid) {
			super(oid);
			setMsgType(MSG_TYPE_GET_LOOT_LIST);
			setProperty("mobOid", mobOid);
			Log.debug("ITEM: client getLootListMessage hit 2");
		}
	}
	
	/**
	 * Sends the GetMerchantListMessage.
	 * @param oid
	 * @param mobOid
	 */
	public static void getMerchantList(OID oid, OID mobOid) {
		getMerchantListMessage msg = new getMerchantListMessage(oid, mobOid);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client getMerchantListMessage hit 1");
	}

	/**
	 * Message used to request the list of items the specified merchant
	 * sells to be sent to the requesting player.
	 * @author Andrew Harrison
	 *
	 */
	public static class getMerchantListMessage extends SubjectMessage {
		private static final long serialVersionUID = 1L;
        public getMerchantListMessage() {
            super();
        }
		public getMerchantListMessage(OID oid, OID mobOid) {
			super(MSG_TYPE_GET_MERCHANT_LIST, mobOid);
			setPlayerOid(oid);
			Log.debug("ITEM: client getMerchantListMessage hit 2");
		}
		
		OID playerOid = null;

        public OID getPlayerOid() {
            return playerOid;
        }
        public void setPlayerOid(OID playerOid) {
            this.playerOid = playerOid;
        }
	}
	
	/**
	 * Sends the PurchaseItemFromMerchantMessage.
	 * @param oid
	 * @param mobOid
	 */
	public static void purchaseItemFromMerchant(OID oid, OID mobOid, int itemID, int count) {
		purchaseItemFromMerchantMessage msg = new purchaseItemFromMerchantMessage(oid, mobOid, itemID, count);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: purchaseItemFromMerchantMessage hit 1");
	}

	/**
	 * Message used to request the purchase of the specified item from the specified NPC.
	 * @author Andrew Harrison
	 *
	 */
	public static class purchaseItemFromMerchantMessage extends SubjectMessage {
		private static final long serialVersionUID = 1L;
        public purchaseItemFromMerchantMessage() {
            super();
        }
		public purchaseItemFromMerchantMessage(OID oid, OID mobOid, int itemID, int count) {
			super(MSG_TYPE_PURCHASE_ITEM_FROM_MERCHANT, mobOid);
			setPlayerOid(oid);
			setItemID(itemID);
			setCount(count);
			Log.debug("ITEM: purchaseItemFromMerchantMessage hit 2");
		}
		
		OID playerOid = null;
		int itemID = -1;
		int count = 1;

        public OID getPlayerOid() {
            return playerOid;
        }
        public void setPlayerOid(OID playerOid) {
            this.playerOid = playerOid;
        }
        
        public int getItemID() {
            return itemID;
        }
        public void setItemID(int itemID) {
            this.itemID = itemID;
        }
        
        public int getCount() {
            return count;
        }
        public void setCount(int count) {
            this.count = count;
        }
	}
	
	/**
	 * Sends the PurchaseItem Message. Sends back a boolean response indicating
	 * whether or not the purchase succeeded.
	 * @param playerOid
	 * @param itemID
	 * @param count
	 * @return
	 */
	public static Boolean purchaseItem(OID playerOid, int itemID, int count){
		purchaseItemMessage msg = new purchaseItemMessage(playerOid, itemID, count);
    	Boolean purchaseSuccessful = Engine.getAgent().sendRPCReturnBoolean(msg);
    	return purchaseSuccessful;
    }
	
	/**
	 * Message used to request the purchase of an item.
	 * @author Andrew
	 *
	 */
	public static class purchaseItemMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public purchaseItemMessage() {
            super();
        }
		public purchaseItemMessage(OID oid, int itemID, int count) {
			super(oid);
			setMsgType(MSG_TYPE_PURCHASE_ITEM);
			setItemID(itemID);
			setCount(count);
			Log.debug("ITEM: client getLootListMessage hit 2");
		}
		
		int itemID = -1;
		int count = 1;
		
		public int getItemID() {
            return itemID;
        }
        public void setItemID(int itemID) {
            this.itemID = itemID;
        }
        
        public int getCount() {
            return count;
        }
        public void setCount(int count) {
            this.count = count;
        }
	}
	
	
	/**
	 * Message used to update item collection quest status.
	 * Contains a map listing each item type and its item count.
	 * @author Andrew Harrison
	 *
	 */
	public static class QuestItemsListMessage extends SubjectMessage {

        public QuestItemsListMessage() {
            super(MSG_TYPE_QUEST_ITEMS_LIST);
        }

        public QuestItemsListMessage(OID playerOid, HashMap<Integer, Integer> itemList) {
            super(MSG_TYPE_QUEST_ITEMS_LIST, playerOid);
            setItemList(itemList);
        }
        
        public HashMap<Integer, Integer> getItemList() {
            return itemList;
        }

        public void setItemList(HashMap<Integer, Integer> itemList) {
            this.itemList = itemList;
        }
        HashMap<Integer, Integer> itemList;
        
        private static final long serialVersionUID = 1L;
    }
	
	/**
	 * Message used to request an inventory update message be sent so the other 
	 * parts of the server know what items the player has.
	 * @author Andrew Harrison
	 *
	 */
	public static class SendInventoryUpdateMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public SendInventoryUpdateMessage() {
            super();
        }
		public SendInventoryUpdateMessage(OID oid) {
			super(oid);
			setMsgType(MSG_TYPE_SEND_INV_UPDATE);
			Log.debug("ITEM: client SendInventoryUpdateMessage hit 2");
		}
	}

	/*
	 * Mail Messages
	 */
	
	/**
	 * Sends the GetMailMessage.
	 * @param oid
	 */
	public static void getMail(OID oid) {
		ExtensionMessage msg = new ExtensionMessage(oid);
		msg.setMsgType(MSG_TYPE_GET_MAIL);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client getMailMessage hit 1");
	}
	
	/**
	 * Sends the MailReadMessage.
	 * @param oid
	 * @param mailID
	 */
	public static void mailRead(OID oid, OID mailID) {
		ExtensionMessage msg = new ExtensionMessage(oid);
		msg.setMsgType(MSG_TYPE_MAIL_READ);
		msg.setProperty("mailID", mailID);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client mailReadMessage hit 1");
	}
	
	/**
	 * Sends the TakeMailItemMessage.
	 * @param oid
	 * @param mailID
	 */
	public static void takeMailItem(OID oid, OID mailID, int itemPos) {
		ExtensionMessage msg = new ExtensionMessage(oid);
		msg.setMsgType(MSG_TYPE_MAIL_TAKE_ITEM);
		msg.setProperty("mailID", mailID);
		msg.setProperty("itemPos", itemPos);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client takeMailItemMessage hit 1");
	}
	
	/**
	 * Sends the DeleteMailMessage.
	 * @param oid
	 * @param mailID
	 */
	public static void deleteMail(OID oid, OID mailID) {
		ExtensionMessage msg = new ExtensionMessage(oid);
		msg.setMsgType(MSG_TYPE_DELETE_MAIL);
		msg.setProperty("mailID", mailID);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client deleteMailMessage hit 1");
	}
	
	/**
	 * Sends the SendMailMessage.
	 * @param oid
	 * @param recipient
	 * @param subject
	 * @param message
	 * @param itemOID
	 */
	public static void sendMail(OID oid, String recipient, String subject, String message, int currencyType, 
			int currencyAmount, boolean CoD) {
		//sendMailMessage msg = new sendMailMessage(oid, recipient, subject, message, itemOID);
		//Engine.getAgent().sendBroadcast(msg);
		Log.debug("MAIL: client sendMailMessage hit 1");
		
		ExtensionMessage sendMailMessage = new ExtensionMessage();
        sendMailMessage.setMsgType(AgisInventoryClient.MSG_TYPE_SEND_MAIL);
        sendMailMessage.setSubject(oid); 
        sendMailMessage.setProperty("recipient", recipient);
        sendMailMessage.setProperty("subject", "Claim sold"); 
        sendMailMessage.setProperty("message", message);
        sendMailMessage.setProperty("numItems", 0);
        sendMailMessage.setProperty("currencyType", currencyType);
        sendMailMessage.setProperty("currencyAmount", currencyAmount);
        sendMailMessage.setProperty("CoD", CoD);
        Engine.getAgent().sendBroadcast(sendMailMessage);
	}

	/**
	 * Message used to request the sending of a piece of mail from the sender
	 * to the specified recipient.
	 * @author Andrew Harrison
	 *
	 */
	/*public static class sendMailMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public sendMailMessage() {
            super();
        }
		public sendMailMessage(OID oid, String recipient, String subject, String message, OID itemOID) {
			super(oid);
			setMsgType(MSG_TYPE_SEND_MAIL);
			setProperty("recipient", recipient);
			setProperty("subject", subject);
			setProperty("message", message);
			setProperty("itemOID", itemOID);
			Log.debug("ITEM: client sendMailMessage hit 2");
		}
	}*/
	
	/**
	 * Sends the SendPurchaseMailMessage.
	 * @param oid
	 * @param itemID
	 */
	public static void sendPurchaseMail(OID oid, int itemID) {
		sendPurchaseMailMessage msg = new sendPurchaseMailMessage(oid, itemID);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEM: client sendPurchaseMailMessage hit 1");
	}

	/**
	 * Message used to send a piece of mail containing an item a player purchased
	 * from the online store. 
	 * Not recommended for use at the current time.
	 * @author Andrew Harrison
	 *
	 */
	public static class sendPurchaseMailMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public sendPurchaseMailMessage() {
            super();
        }
		public sendPurchaseMailMessage(OID oid, int itemID) {
			super(oid);
			setMsgType(MSG_TYPE_SEND_PURCHASE_MAIL);
			setProperty("itemID", itemID);
			Log.debug("ITEM: client sendPurchaseMailMessage hit 2");
		}
	}
	
	/**
	 * Sends the CheckCurrency Message. Sends back a boolean response indicating
	 * whether or not the player has enough of the specified currency.
	 * @param playerOid
	 * @param itemID
	 * @param count
	 * @return
	 */
	public static Boolean checkCurrency(OID playerOid, int currencyID, int cost){
		checkCurrencyMessage msg = new checkCurrencyMessage(playerOid, currencyID, cost);
    	Boolean hasEnoughCurrency = Engine.getAgent().sendRPCReturnBoolean(msg);
    	return hasEnoughCurrency;
    }
	
	/**
	 * Message used to check if the player has enough of the specified currency
	 * @author Andrew Harrison
	 *
	 */
	public static class checkCurrencyMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public checkCurrencyMessage() {
            super();
        }
		public checkCurrencyMessage(OID oid, int currencyID, int count) {
			super(oid);
			setMsgType(MSG_TYPE_CHECK_CURRENCY);
			setCurrencyID(currencyID);
			setCount(count);
			Log.debug("ITEM: client checkCurrencyMessage hit 2");
		}
		
		int currencyID = -1;
		int count = 1;
		
		public int getCurrencyID() {
            return currencyID;
        }
        public void setCurrencyID(int currencyID) {
            this.currencyID = currencyID;
        }
        
        public int getCount() {
            return count;
        }
        public void setCount(int count) {
            this.count = count;
        }
	}
	
	/**
	 * Sends the AlterCurrencyMessage.
	 * @param oid
	 * @param currencyType
	 * @param delta
	 */
	public static void alterCurrency(OID oid, int currencyType, int delta) {
		alterCurrencyMessage msg = new alterCurrencyMessage(oid, currencyType, delta);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEMCLIENT: alterCurrencyMessage hit 1");
	}

	/**
	 * Message used to alter the amount of the specified currency a player has.
	 * @author Andrew
	 *
	 */
	public static class alterCurrencyMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public alterCurrencyMessage() {
            super();
        }
		public alterCurrencyMessage(OID oid, int currencyType, int delta) {
			super(oid);
			setMsgType(MSG_TYPE_ALTER_CURRENCY);
			setCurrencyType(currencyType);
			setDelta(delta);
			Log.debug("ITEMCLIENT: alterCurrencyMessage hit 2");
		}
		
		public int getCurrencyType() { return currencyType; }
		public void setCurrencyType(int currencyType) { this.currencyType = currencyType; }
		
		public int getDelta() { return delta; }
		public void setDelta(int delta) { this.delta = delta; }
		
		protected int currencyType;
		protected int delta;
	}
	
	/**
	 * Sends the EquippedItemUsedMessage.
	 * @param oid
	 * @param useType
	 * @param itemOid
	 */
	public static void equippedItemUsed(OID oid, String useType) {
		EquippedItemUsedMessage msg = new EquippedItemUsedMessage(oid, useType);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("ITEMCLIENT: EquippedItemUsedMessage hit 1");
	}
	
	/**
	 * Message used to alter the amount of the specified currency a player has.
	 * @author Andrew
	 *
	 */
	public static class EquippedItemUsedMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public EquippedItemUsedMessage() {
            super();
        }
		public EquippedItemUsedMessage(OID oid, String useType) {
			super(oid);
			setMsgType(MSG_TYPE_EQUIPPED_ITEM_USED);
			setUseType(useType);
			Log.debug("ITEMCLIENT: EquippedItemUsedMessage hit 2");
		}
		
		public String getUseType() { return useType; }
		public void setUseType(String useType) { this.useType = useType; }
		
		//public OID getItemOid() { return itemOid; }
		//public void setItemOid(OID itemOid) { this.itemOid = itemOid; }
		
		protected String useType;
		//protected OID itemOid;
	}
	
	/*
	 * Old Messages that are not currently used anymore.
	 */
	
	public static ArrayList<String> getAccountSkins(OID oid) {
		ExtensionMessage eMsg = new ExtensionMessage(MSG_TYPE_GET_SKINS, null, oid);
		return (ArrayList) Engine.getAgent().sendRPCReturnObject(eMsg);
	}
	
	public static int getAccountItemCount(OID oid, int itemID) {
		ExtensionMessage eMsg = new ExtensionMessage(MSG_TYPE_GET_ACCOUNT_ITEM_COUNT, null, oid);
		eMsg.setProperty("itemID", itemID);
		return Engine.getAgent().sendRPCReturnInt(eMsg);
	}
	
	/**
	 * Sends the CheckComponentMessage checking if the player has enough of the given item types.
	 * @param id
	 * @param components
	 * @param componentCounts
	 * @return
	 */
	public static Boolean checkComponents(OID id, LinkedList<Integer> components, 
			LinkedList<Integer> componentCounts){
    	CheckComponentMessage msg = new CheckComponentMessage(id, components, componentCounts);
    	Boolean hasRequiredComponents = (Boolean) Engine.getAgent().sendRPCReturnObject(msg);
    	return hasRequiredComponents;
    }
	
	/**
	 * Sends the CheckComponentMessage checking if the player has the specific items given.
	 * @param id
	 * @param reqComponentIDs
	 * @param reqStackSizes
	 * @param components
	 * @param componentCounts
	 * @return
	 */
	public static Boolean checkSpecificComponents(OID id, LinkedList<Integer> reqComponentIDs, LinkedList<Integer> reqStackSizes, LinkedList<Long> components, 
			LinkedList<Integer> componentCounts){
    	CheckComponentMessage msg = new CheckComponentMessage(id, reqComponentIDs, reqStackSizes, components, componentCounts);
    	Boolean hasRequiredComponents = (Boolean) Engine.getAgent().sendRPCReturnObject(msg);
    	return hasRequiredComponents;
    }
	
    public static class CheckComponentMessage extends SubjectMessage{
    	private static final long serialVersionUID = 1L;
    	public LinkedList<Integer> _reqComponents;
    	public LinkedList<Integer> _reqStackSizes;
    	public LinkedList<Long> _components;
    	public LinkedList<Integer> _componentCounts;
    	public OID _subject;
    	public boolean gridSystem = false;
    	
    	public CheckComponentMessage() {
    		super(MSG_TYPE_RETURNBOOLEAN_CHECK_COMPONENTS);
    	}
    	
    	public CheckComponentMessage(OID subject, LinkedList<Integer> componentIDs, LinkedList<Integer> stackSize, 
    			LinkedList<Long> components, LinkedList<Integer> componentCounts){
    		super(MSG_TYPE_RETURNBOOLEAN_CHECK_COMPONENTS, subject);
    		_reqComponents = componentIDs;
    		_reqStackSizes = stackSize;
    		_components = components;
    		_componentCounts = componentCounts;
    		_subject = subject;
    		gridSystem = true;
    	}
    	
    	public CheckComponentMessage(OID subject, 
    			LinkedList<Integer> components, LinkedList<Integer> componentCounts){
    		super(MSG_TYPE_RETURNBOOLEAN_CHECK_COMPONENTS, subject);
    		_reqComponents = components;
    		_reqStackSizes = componentCounts;
    		_subject = subject;
    		gridSystem = false;
    	}
    }

    public static final MessageType MSG_TYPE_AGIS_INV_FIND = MessageType.intern("ao.AGIS_INV_FIND");

    public static final MessageType MSG_TYPE_TRADE_START_REQ = MessageType.intern("ao.TRADE_START_REQ");
    public static final MessageType MSG_TYPE_TRADE_START = MessageType.intern("ao.TRADE_START");
    public static final MessageType MSG_TYPE_TRADE_COMPLETE = MessageType.intern("ao.TRADE_COMPLETE");
    public static final MessageType MSG_TYPE_TRADE_OFFER_REQ = MessageType.intern("ao.TRADE_OFFER_REQ");
    public static final MessageType MSG_TYPE_TRADE_OFFER_UPDATE = MessageType.intern("ao.TRADE_OFFER_UPDATE");
    public static final MessageType MSG_TYPE_ITEM_ACQUIRE_STATUS_CHANGE = MessageType.intern("ao.ITEM_ACQUIRE_STATUS_CHANGE");
    public static final MessageType MSG_TYPE_ITEM_EQUIP_STATUS_CHANGE = MessageType.intern("ao.ITEM_EQUIP_STATUS_CHANGE");
    public static final MessageType MSG_TYPE_REQ_OPEN_MOB = MessageType.intern("inventory.REQ_OPEN_MOB");
    public static final MessageType MSG_TYPE_REMOVE_SPECIFIC_ITEM = MessageType.intern("inventory.REMOVE_SPECIFIC_ITEM");
    public static final MessageType MSG_TYPE_REMOVE_GENERIC_ITEM = MessageType.intern("inventory.REMOVE_GENERIC_ITEM");
    public static final MessageType MSG_TYPE_GET_SPECIFIC_ITEM_DATA = MessageType.intern("inventory.GET_SPECIFIC_ITEM_DATA");
    public static final MessageType MSG_TYPE_GET_GENERIC_ITEM_DATA = MessageType.intern("inventory.GET_GENERIC_ITEM_DATA");
    public static final MessageType MSG_TYPE_GENERATE_ITEM = MessageType.intern("inventory.GENERATE_ITEM");
    public static final MessageType MSG_TYPE_PLACE_BAG = MessageType.intern("inventory.PLACE_BAG");
    public static final MessageType MSG_TYPE_MOVE_BAG = MessageType.intern("inventory.MOVE_BAG");
    public static final MessageType MSG_TYPE_REMOVE_BAG = MessageType.intern("inventory.REMOVE_BAG");
    public static final MessageType MSG_TYPE_MOVE_ITEM = MessageType.intern("inventory.MOVE_ITEM");
    public static final MessageType MSG_TYPE_LOOT_ITEM = MessageType.intern("inventory.LOOT_ITEM");
    public static final MessageType MSG_TYPE_LOOT_ALL = MessageType.intern("inventory.LOOT_ALL");
    public static final MessageType MSG_TYPE_GENERATE_LOOT = MessageType.intern("inventory.GENERATE_LOOT");
    public static final MessageType MSG_TYPE_GET_LOOT_LIST = MessageType.intern("inventory.GET_LOOT_LIST");
    public static final MessageType MSG_TYPE_GET_MERCHANT_LIST = MessageType.intern("inventory.GET_MERCHANT_LIST");
    public static final MessageType MSG_TYPE_PURCHASE_ITEM_FROM_MERCHANT = MessageType.intern("inventory.MSG_TYPE_PURCHASE_ITEM_FROM_MERCHANT");
    public static final MessageType MSG_TYPE_PURCHASE_ITEM = MessageType.intern("inventory.PURCHASE_ITEM");
    public static final MessageType MSG_TYPE_SELL_ITEM = MessageType.intern("inventory.SELL_ITEM");
    public static final MessageType MSG_TYPE_PICKUP_ITEM = MessageType.intern("inventory.PICKUP_ITEM");
    public static final MessageType MSG_TYPE_QUEST_ITEMS_LIST = MessageType.intern("inventory.QUEST_ITEMS_LIST");
    public static final MessageType MSG_TYPE_SEND_INV_UPDATE = MessageType.intern("inventory.SEND_INV_UPDATE");
    public static final MessageType MSG_TYPE_CHECK_CURRENCY = MessageType.intern("inventory.CHECK_CURRENCY");
    public static final MessageType MSG_TYPE_ALTER_CURRENCY = MessageType.intern("inventory.ALTER_CURRENCY");
    public static final MessageType MSG_TYPE_GET_SKINS = MessageType.intern("ao.GET_SKINS");
    public static final MessageType MSG_TYPE_PURCHASE_SKIN = MessageType.intern("ao.PURCHASE_SKIN");
    public static final MessageType MSG_TYPE_SET_WEAPON = MessageType.intern("ao.SET_WEAPON");
    public static final MessageType MSG_TYPE_SET_SKIN_COLOUR = MessageType.intern("ao.SET_SKIN_COLOUR");
    public static final MessageType MSG_TYPE_GET_ACCOUNT_ITEM_COUNT = MessageType.intern("ao.GET_ACCOUNT_ITEM_COUNT");
    public static final MessageType MSG_TYPE_ALTER_ITEM_COUNT = MessageType.intern("ao.ALTER_ITEM_COUNT");
    public static final MessageType MSG_TYPE_USE_ACCOUNT_ITEM = MessageType.intern("ao.USE_ACCOUNT_ITEM");
    public static final MessageType MSG_TYPE_ITEM_ACTIVATED = MessageType.intern("ao.ITEM_ACTIVATED");
    public static final MessageType MSG_TYPE_RETURNBOOLEAN_CHECK_COMPONENTS = MessageType.intern("ao.CHECK_COMPONENT");
    public static final MessageType MSG_TYPE_SET_AMMO = MessageType.intern("ao.SET_AMMO");
    public static final MessageType MSG_TYPE_EQUIPPED_ITEM_USED = MessageType.intern("ao.EQUIPPED_ITEM_USED");
    public static final MessageType MSG_TYPE_REPAIR_ITEMS = MessageType.intern("ao.REPAIR_ITEMS");
    // Mail Messages
    public static final MessageType MSG_TYPE_GET_MAIL = MessageType.intern("inventory.GET_MAIL");
    public static final MessageType MSG_TYPE_MAIL_READ = MessageType.intern("inventory.MAIL_READ");
    public static final MessageType MSG_TYPE_MAIL_TAKE_ITEM = MessageType.intern("inventory.MAIL_TAKE_ITEM");
    public static final MessageType MSG_TYPE_RETURN_MAIL = MessageType.intern("inventory.RETURN_MAIL");
    public static final MessageType MSG_TYPE_DELETE_MAIL = MessageType.intern("inventory.DELETE_MAIL");
    public static final MessageType MSG_TYPE_SEND_MAIL = MessageType.intern("inventory.SEND_MAIL");
    public static final MessageType MSG_TYPE_SEND_PURCHASE_MAIL = MessageType.intern("inventory.SEND_PURCHASE_MAIL");
    
    // Other/Admin
    public static final MessageType MSG_TYPE_RELOAD_ITEMS = MessageType.intern("ao.RELOAD_ITEMS");

    public static final String INV_METHOD_SLOT = "slot";
    public static final String MSG_INV_SLOT = "inv_slot";

    public final static byte tradeSuccess = (byte)1;
    public final static byte tradeCancelled = (byte)2;
    public final static byte tradeFailed = (byte)3;
    public final static byte tradeBusy = (byte)4;
}