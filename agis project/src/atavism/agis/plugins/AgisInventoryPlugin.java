package atavism.agis.plugins;

import atavism.agis.core.Agis;
import atavism.agis.core.TradeSession;
import atavism.agis.database.AccountDatabase;
import atavism.agis.database.ContentDatabase;
import atavism.agis.database.ItemDatabase;
import atavism.agis.database.MobDatabase;
import atavism.agis.objects.*;
import atavism.agis.objects.Currency;
import atavism.agis.plugins.AgisInventoryClient.CheckComponentMessage;
import atavism.agis.plugins.AgisInventoryClient.EquippedItemUsedMessage;
import atavism.agis.util.EquipHelper;
import atavism.agis.util.EventMessageHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.*;
import atavism.server.engine.*;
import atavism.server.math.Point;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.*;
import atavism.server.plugins.*;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.AORuntimeException;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.locks.Lock;

/**
 * The AgisInventoryPlugin is the manager of Inventory for players and mobs and all
 * related areas such as Trading, Looting, and Mail.  The Plugin creates and works with
 * the Inventory Sub Object, which contains the players inventory (their bags, mailbox
 * and eventually their bank).
 * 
 * The Plugin is set up with Hooks for many Inventory related messages and loads in the 
 * players inventory sub object to get the information requested or make the changes 
 * needed.
 * 
 * @author Andrew Harrison
 *
 */
public class AgisInventoryPlugin extends InventoryPlugin implements MessageCallback {

	/**
	 * Run on activation, this function sets up the Hooks and filters for the messages
	 * the plugin deals with.
	 */
    public void onActivate() {
        super.onActivate();
        
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_AGIS_INV_FIND, 
        		new AgisFindItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_TRADE_START_REQ,
                new TradeStartReqHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_TRADE_OFFER_REQ,
                new TradeOfferReqHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED, 
        		new DespawnedHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_REMOVE_GENERIC_ITEM, 
        		new RemoveGenericItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_REMOVE_SPECIFIC_ITEM, 
        		new RemoveSpecificItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GET_SPECIFIC_ITEM_DATA, 
        		new GetSpecificItemDataHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GET_GENERIC_ITEM_DATA, 
        		new GetGenericItemDataHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GENERATE_ITEM, 
        		new GenerateItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_PLACE_BAG, 
        		new PlaceBagHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_MOVE_BAG, 
        		new MoveBagHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_REMOVE_BAG, 
        		new RemoveBagHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_MOVE_ITEM, 
        		new MoveItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_LOOT_ITEM, 
        		new LootItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_LOOT_ALL, 
        		new LootAllHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GENERATE_LOOT, 
        		new GenerateLootHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GET_LOOT_LIST, 
        		new GetLootListHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GET_MERCHANT_LIST, 
        		new GetMerchantListHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_PURCHASE_ITEM, 
        		new PurchaseItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_SELL_ITEM, 
        		new SellItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_PICKUP_ITEM, 
        		new PickupItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_EQUIPPED_ITEM_USED, 
        		new EquippedItemUsedHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_REPAIR_ITEMS, 
        		new RepairItemsHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_SEND_INV_UPDATE, 
        		new SendInventoryUpdateHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_GET_MAIL, 
        		new GetMailHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_MAIL_READ, 
        		new MailReadHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_MAIL_TAKE_ITEM, 
        		new TakeMailItemHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_RETURN_MAIL, 
        		new ReturnMailHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_DELETE_MAIL, 
        		new DeleteMailHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_SEND_MAIL, 
        		new SendMailHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_SEND_PURCHASE_MAIL, 
        		new SendPurchaseMailHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_CHECK_CURRENCY, 
        		new CheckCurrencyHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_ALTER_CURRENCY, 
        		new AlterCurrencyHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_ALTER_ITEM_COUNT, 
        		new AlterItemCountHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_USE_ACCOUNT_ITEM, 
        		new UseAccountItemHook());
        
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CATEGORY_UPDATED, 
        		new CategoryUpdatedHook());
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_RELOAD_ITEMS, 
        		new ReloadItemsHook());
        
        getHookManager().addHook(AgisInventoryClient.MSG_TYPE_RETURNBOOLEAN_CHECK_COMPONENTS, 
        		new CheckComponentHook());
        
        // Hook to process login/logout messages
		getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
				new LoginHook());
		getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT,
				new LogoutHook());

        try {
        	// Filters for messages that sends a response back.
        	MessageTypeFilter filterNeedsResponse = new MessageTypeFilter();
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_AGIS_INV_FIND);
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_GET_SKINS);
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_GET_ACCOUNT_ITEM_COUNT);
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_GET_GENERIC_ITEM_DATA);
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_RETURNBOOLEAN_CHECK_COMPONENTS);
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_CHECK_CURRENCY);
        	filterNeedsResponse.addType(AgisInventoryClient.MSG_TYPE_PURCHASE_ITEM);
        	/* Long sub = */ Engine.getAgent().createSubscription(
                filterNeedsResponse, this, MessageAgent.RESPONDER);

        	// Filters for messages that don't send a response.
        	MessageTypeFilter filterNoResponse = new MessageTypeFilter();
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_TRADE_START_REQ);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_TRADE_OFFER_REQ);
        	filterNoResponse.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_REMOVE_GENERIC_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_REMOVE_SPECIFIC_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_GET_SPECIFIC_ITEM_DATA);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_GENERATE_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_PLACE_BAG);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_MOVE_BAG);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_REMOVE_BAG);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_MOVE_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_LOOT_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_LOOT_ALL);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_GENERATE_LOOT);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_GET_LOOT_LIST);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_GET_MERCHANT_LIST);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_SELL_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_PICKUP_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_EQUIPPED_ITEM_USED);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_REPAIR_ITEMS);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_SEND_INV_UPDATE);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_GET_MAIL);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_MAIL_READ);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_MAIL_TAKE_ITEM);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_RETURN_MAIL);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_DELETE_MAIL);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_SEND_MAIL);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_SEND_PURCHASE_MAIL);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_ALTER_CURRENCY);
        	//filterNoResponse.addType(AgisMobClient.MSG_TYPE_CATEGORY_UPDATED);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_RELOAD_ITEMS);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_PURCHASE_SKIN);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_SET_WEAPON);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_SET_SKIN_COLOUR);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_ALTER_ITEM_COUNT);
        	filterNoResponse.addType(AgisInventoryClient.MSG_TYPE_USE_ACCOUNT_ITEM);
        	/* Long sub = */ Engine.getAgent().createSubscription(filterNoResponse, this);
        	
        	// Create responder subscription
    	 	MessageTypeFilter filter2 = new MessageTypeFilter();
    	 	filter2.addType(LoginMessage.MSG_TYPE_LOGIN);
    	 	filter2.addType(LogoutMessage.MSG_TYPE_LOGOUT);
    	 	Engine.getAgent().createSubscription(filter2, this,
    	 			MessageAgent.RESPONDER);
        } catch (Exception e) {
            throw new AORuntimeException("activate failed", e);
        }
        
        // Load settings
        ContentDatabase cDB = new ContentDatabase(false);
        String bagCount = cDB.loadGameSetting("PLAYER_BAG_COUNT");
        if (bagCount != null)
        	INVENTORY_BAG_COUNT = Integer.parseInt(bagCount);
        String firstBagSize = cDB.loadGameSetting("PLAYER_DEFAULT_BAG_SIZE");
        if (firstBagSize != null)
        	INVENTORY_FIRST_BAG_SIZE = Integer.parseInt(firstBagSize);
        String sellFactor = cDB.loadGameSetting("SELL_FACTOR");
        if (sellFactor != null)
        	SELL_FACTOR = Float.parseFloat(sellFactor);
        String repairFactor = cDB.loadGameSetting("REPAIR_RATE");
        if (repairFactor != null)
        	REPAIR_RATE = Float.parseFloat(sellFactor);
        String corpseDuration = cDB.loadGameSetting("PLAYER_CORPSE_LOOT_DURATION");
        if (corpseDuration != null)
        	PLAYER_CORPSE_LOOT_DURATION = Integer.parseInt(corpseDuration);
        String corpseSafeDuration = cDB.loadGameSetting("PLAYER_CORPSE_SAFE_LOOT_DURATION");
        if (corpseSafeDuration != null)
        	PLAYER_CORPSE_SAFE_LOOT_DURATION = Integer.parseInt(corpseSafeDuration);
        String playerCorpseMobTemplate = cDB.loadGameSetting("PLAYER_CORPSE_MOB_TEMPLATE");
        if (playerCorpseMobTemplate != null)
        	PLAYER_CORPSE_MOB_TEMPLATE = Integer.parseInt(playerCorpseMobTemplate);
        String playerCorpseDropsEquipment = cDB.loadGameSetting("PLAYER_CORPSE_DROPS_EQUIPMENT");
        if (playerCorpseDropsEquipment != null)
        	PLAYER_CORPSE_DROPS_EQUIPMENT = Boolean.parseBoolean(playerCorpseDropsEquipment);
        
        cDB.loadEditorOptions();
        
        
        // Load items from the database
        ItemDatabase iDB = new ItemDatabase(false);
        loadItemsFromDatabase(iDB);
        iDB.close();
        
        MobDatabase mDB = new MobDatabase(false);
        loadLootTables(mDB);
        loadCurrencies(mDB);
        mDB.close();
        
        // Setup connection to admin database
        aDB = new AccountDatabase();
    }
    
    /**
     * Loads in item templates from the database and registers them.
     * @param iDB
     */
    public void loadItemsFromDatabase(ItemDatabase iDB) {
        ArrayList<Template> items = iDB.loadItemTemplates();
        for (Template tmpl: items)
        	ObjectManagerClient.registerTemplate(tmpl);
    }
    
    /**
     * Loads the items from the database in again. Not tested recently.
     * @author Andrew Harrison
     *
     */
    class ReloadItemsHook implements Hook {
		public boolean processMessage(atavism.msgsys.Message msg, int flags) {
			ExtensionMessage eMsg = (ExtensionMessage)msg;
			Log.debug("RELOAD: reloading items from database");
			ItemDatabase iDB = new ItemDatabase(false);
			loadItemsFromDatabase(iDB);
			iDB.close();
			OID objOid = eMsg.getSubject();
			WorldManagerClient.sendObjChatMsg(objOid, 2, "Reloading items from the database.");
		    return true;
		}
	}
    
    /**
     * Loads in the Loot Tables from the database and registers them
     * in the manager.
     * @param mDB
     */
    private void loadLootTables(MobDatabase mDB ) {
    	HashMap<Integer, LootTable> lootTables = mDB.loadLootTables(-1);
    	for (LootTable lTbl: lootTables.values()) {
    		Agis.LootTableManager.register(lTbl.getID(), lTbl);
        	Log.debug("LOOT: loaded loot Table: [" + lTbl.getName() + "]");
    	}
    }
    
    /**
     * Loads in Currencies from the Database and registers them
     * in the manager.
     * @param mDB
     */
    private void loadCurrencies(MobDatabase mDB) {
    	ArrayList<Currency> currencies = mDB.loadCurrencies(-1);
    	for (Currency currency: currencies) {
    		Agis.CurrencyManager.register(currency.getCurrencyID(), currency);
        	Log.debug("CURRENCY: currency Table: [" + currency.getCurrencyID() + ":" + currency.getCurrencyName() + "]");
    	}
    }

    /**
     * Returns the AgisItem Entity for the specified OID.
     * @param oid
     * @return
     */
    public static AgisItem getAgisItem(OID oid) {
        return (AgisItem)EntityManager.getEntityByNamespace(oid, Namespace.AGISITEM);
    }
    
    /**
     * Registers the AgisItem in the Item Namespace. Used when creating
     * the AgisItem SubObject.
     * @param item
     */
    public static void registerAgisItem(AgisItem item) {
        EntityManager.registerEntityByNamespace(item, Namespace.AGISITEM);
    }
    
    /**
     * Returns the InventoryInfo Entity for the specified OID. The InventoryInfo
     * contains the information about a player/mobs inventory such as what bags
     * and items they have.
     * @param oid
     * @return
     */
    public static InventoryInfo getInventoryInfo(OID oid) {
        return (InventoryInfo)EntityManager.getEntityByNamespace(oid, Namespace.BAG);
    }
    
    /**
     * Registers the InventoryInfo in the Item Namespace. Used when creating
     * the InventoryInfo SubObject for a player/mob.
     * @param iInfo
     */
    public static void registerInventoryInfo(InventoryInfo iInfo) {
    	EntityManager.registerEntityByNamespace(iInfo, Namespace.BAG);
    }
    
    /**
     * Returns the Bag Entity for the specified OID.
     * @param activatorOid
     * @return
     */
    public static Bag getBag(OID activatorOid) {
        return (Bag)EntityManager.getEntityByNamespace(activatorOid, Namespace.BAG);
    }
    
    /**
     * Registers the Bag Entity in the Bag Namespace. Used when a new
     * Bag is being created.
     * @param bag
     */
    public static void registerBag(Bag bag) {
    	EntityManager.registerEntityByNamespace(bag, Namespace.BAG);
    }

    /**
     * Creates the InventoryInfo for the for the specified player/mob. 
     * Takes in a template as well, applying the relevant properties from
     * the template to the InventoryInfo.
     * returns the bag, or null on failure
     */
    public SubObjData createInvSubObj(OID mobOid, Template template) {
    	InventoryInfo iInfo = new InventoryInfo(mobOid);
    	iInfo.setName(template.getName());
    	iInfo.setCurrentCategory(1);
    	Map<String, Serializable> props = template.getSubMap(Namespace.BAG);
    	if (props == null) {
    		Log.warn("createInvSubObj: no props in ns " + Namespace.BAG);
    		return null;
    	}

        Boolean persistent = (Boolean)template.get(
            Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_PERSISTENT);
        if (persistent == null)
            persistent = false;
        
        iInfo.setPersistenceFlag(persistent);

        // copy properties from template to object
    	for (Map.Entry<String, Serializable> entry : props.entrySet()) {
    	    String key = entry.getKey();
    	    Serializable value = entry.getValue();
    	    if (!key.startsWith(":")) {
    	    	iInfo.setProperty(key, value);
    	    }
    	}
        // register the entity
		registerInventoryInfo(iInfo);
        
		// Create the bags
		Log.debug("INV: template name: " + template.getName());
		if (template.getSubMap(InventoryClient.NAMESPACE).containsKey("isPlayer")) {
			Log.debug("INV: creating bags for player");
			createBags(iInfo, mobOid, true);
		} else {
			Log.debug("INV: creating bags for mob");
			createBags(iInfo, mobOid, false);
		}
        
        // Add any currencies the user doesn't know about yet
        for (int currencyID : Agis.CurrencyManager.keyList()) {
        	if (!iInfo.getCurrentCurrencies().containsKey(currencyID)) {
        		iInfo.addCurrency(currencyID, 0);
        	}
        }
		
		if (persistent)
			Engine.getPersistenceManager().persistEntity(iInfo);
		
		String invItems = (String) props.get(InventoryClient.TEMPL_ITEMS);
        return new SubObjData(Namespace.WORLD_MANAGER,
			      new CreateInventoryHook(mobOid, invItems));
    }
    
    /**
     * Creates a new set of Bags and inventory to be placed in an existing 
     * InventoryInfo.
     * @param iInfo
     * @param mobOid
     */
    protected void createBags(InventoryInfo iInfo, OID mobOid, boolean isPlayer) {
    	OID[] bags;
    	if (isPlayer) {
    		bags = new OID[INVENTORY_BAG_COUNT];
    	} else {
    		bags = new OID[1];
    	}
    	// create the first sub bag separately
        Bag subBag = createSubBag(iInfo.getOid(), Bag.BAG_USE_TYPE_STANDARD, INVENTORY_FIRST_BAG_SIZE);
        if (subBag == null) {
            return;
        }
        bags[0] = subBag.getOid();
        if (isPlayer) {
        	for (int subBagNum = 1; subBagNum < INVENTORY_BAG_COUNT; subBagNum++) {
        		if (Log.loggingDebug)
        			log.debug("createInvSubObj: creating sub bag, moboid=" + mobOid
                          + ", bag pos=" + subBagNum);
        		subBag = createSubBag(iInfo.getOid(), Bag.BAG_USE_TYPE_STANDARD, INVENTORY_OTHER_BAG_SIZE);
        		if (subBag == null) {
        			return;
        		}
        		bags[subBagNum] = subBag.getOid();
        	}
        }
        iInfo.setBags(bags);
        
        // Create an equipped items bag
        if (Log.loggingDebug)
            log.debug("createInvSubObj: creating sub bag, moboid=" + mobOid
                      + ", bag pos=" + INVENTORY_BAG_COUNT);
        subBag = createSubBag(iInfo.getOid(), Bag.BAG_USE_TYPE_EQUIPPED, EQUIP_SLOTS_COUNT);
        if (subBag == null) {
            return;
        }
        iInfo.setEquipmentItemBag(subBag.getOid());
    }
    
    /**
     * Creates a Bag that sits inside another Bag. A message isn't sent to the
     * worldmgr to set the forward reference.
     * @param ownerOid
     * @param numSlots
     * @param useType
     * @return
     */
    private Bag createSubBag(OID ownerOid, int useType, int numSlots) {
        // create the bag
        Bag bag = new Bag();
        bag.setOid(Engine.getOIDManager().getNextOid());
        bag.applySettings(numSlots, useType);

        // set back reference to the owner
        bag.setProperty(InventoryPlugin.INVENTORY_PROP_BACKREF_KEY, ownerOid);

        // set the forward reference on the parentBag
        //parentBag.putItem(parentBagSlotNum, bag.getOid());

        // bind the bag locally
        registerBag(bag);
        SubjectFilter wmFilter = new SubjectFilter(bag.getOid());
        wmFilter.addType(EnginePlugin.MSG_TYPE_SET_PROPERTY);
        wmFilter.addType(EnginePlugin.MSG_TYPE_GET_PROPERTY);
        /* Long sub = */ //Engine.getAgent().createSubscription(wmFilter, AgisInventoryPlugin.this);
        
        return bag;
    }
    
    /**
     * Hook for the CreateInventoryMessage. Calls createInventory with the
     * list of items to be given to the player/mob. Run by the createInvSubObject
     * method. 
     * @author Andrew Harrison
     *
     */
    protected class CreateInventoryHook implements Hook {
    	public CreateInventoryHook(OID masterOid, String invItems) {
    	    this.masterOid = masterOid;
    	    this.invItems = invItems;
    	}
    	protected OID masterOid;
    	protected String invItems;

    	public boolean processMessage(Message msg, int flags) {
            if (Log.loggingDebug)
                log.debug("CreateInventoryHook.processMessage: masterOid=" + masterOid + " invItems=" + invItems);
            InventoryInfo iInfo = getInventoryInfo(masterOid);

            if (invItems == null)
            	return true;
            if (invItems.equals("")) {
                return true;
            }
            createInventoryItems(masterOid, iInfo, invItems);
            return true;
    	}
    }
    
    /**
     * Generates the items requested and puts them in the players/mobs inventory.
     * Called when first creating an Inventory SubObject.
     * @param masterOid
     * @param iInfo
     * @param invItems A String with numbers representing the templateIDs of the items to be generated split by semicolons.
     */
    protected void createInventoryItems(OID masterOid, InventoryInfo iInfo, String invItems) {
    	for (String itemName : invItems.split(";")) {
        	boolean equip = false;
        	itemName = itemName.trim();
        	if (itemName.isEmpty())
        		continue;
        	if (itemName.startsWith("*")) {
        		itemName = itemName.substring(1);
        		equip = true;
        	}
        	int itemID = Integer.parseInt(itemName);
            if (Log.loggingDebug)
                log.debug("CreateInventoryHook.processMessage: creating item=" + itemName + " equip=" + equip);
            Template itemTemplate = new Template();
            itemTemplate.put(Namespace.OBJECT_MANAGER,
                    ObjectManagerClient.TEMPL_PERSISTENT,
                    iInfo.getPersistenceFlag());
            if (Log.loggingDebug)
                log.debug("CreateInventoryHook.processMessage: generating item=" + itemID);
            OID itemOid = ObjectManagerClient.generateObject(itemID, ObjectManagerPlugin.ITEM_TEMPLATE, itemTemplate);
            if (Log.loggingDebug)
                log.debug("CreateInventoryHook.processMessage: created item=" + itemOid);
            addItem(masterOid, iInfo.getOid(), itemOid);
            if (Log.loggingDebug)
                log.debug("CreateInventoryHook.processMessage: added item to inv=" + itemOid);
            if (equip) {
            	AgisItem item = getAgisItem(itemOid);
            	equipItem(item, masterOid, false);
            }
        }
    }
    
    /**
     * Handles the CategoryUpdatedMessage to indicate the player has changed world category.
     * This is used to change to or generate new data (Inventory in this case) to be used in the new
     * category
     * @author Andrew Harrison
     *
     */
    public class CategoryUpdatedHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            AgisMobClient.categoryUpdatedMessage pMsg = (AgisMobClient.categoryUpdatedMessage) msg;
            OID playerOid = pMsg.getSubject();
            int category = (Integer) pMsg.getProperty("category");
            Log.debug("CATEGORY: updating category for player " + playerOid + " and category: " + category);
            Lock lock = getObjectLockManager().getLock(playerOid);
            lock.lock();
            try {
                InventoryInfo iInfo = getInventoryInfo(playerOid);
                unequipOldItems(iInfo);
                saveInventory(iInfo, Namespace.BAG);
                unloadInventory(iInfo);
                if (iInfo.categoryUpdated(category)) {
                	createBags(iInfo, playerOid, true);
                	if (WorldManagerClient.getObjectInfo(playerOid).objType == ObjectTypes.player) {
                    	//int race = (Integer)EnginePlugin.getObjectProperty(mobOid, WorldManagerClient.NAMESPACE, "race");
                    	//int aspect = (Integer)EnginePlugin.getObjectProperty(mobOid, CombatClient.NAMESPACE, "aspect");
                    	//String startingItems = getStartingItems(race, aspect);
                    	createInventoryItems(playerOid, iInfo, "");
                    }
                } else {
                	loadInventory(iInfo);
                	for (OID bagOid : iInfo.getBags()) {
                		Bag bag = getBag(bagOid);
                		Log.debug("BAG: bag slots: " + bag.getNumSlots());
                	}
                }
                equipNewItems(iInfo);
                Log.debug("CATEGORY: updating category for player " + playerOid + " complete");
            }
            finally {
                lock.unlock();
            }
            sendBagInvUpdate(playerOid);
            return true;
        }
        
        private void unequipOldItems(InventoryInfo iInfo) {
        	OID bagOid = iInfo.getBags()[InventoryInfo.equipmentBag];
        	Bag equipBag = getBag(bagOid);
        	for (OID itemOID : equipBag.getItemsList()) {
        		if (itemOID == null)
        			continue;
        		AgisItem item = getAgisItem(itemOID);

        		lock.lock();
            	try {
            		// where is this currently equipped
            		EquipMap equipMap = getEquipMap(iInfo.getOid());
            		AgisEquipSlot slot = equipMap.getSlot(item.getMasterOid());
            		if (slot == null) {
            			// item is not currently equipped
            			Log.warn("AgisInventoryPlugin.unequipItem: item not equipped: item=" + item);
            			continue;
            		}
            		// remove the item from the map
            		equipMap.remove(slot);
            		if (Log.loggingDebug)
            			log.debug("AgisInventoryPlugin.unequipItem: removed DC for item:" + item); 
            	} finally {
            		lock.unlock();
            	}
            	AgisInventoryClient.itemEquipStatusChanged(iInfo.getOid(), item, false, item.getPrimarySlot().toString());
        		EquipHelper.updateDisplay(iInfo.getOid(), null, item.getPrimarySlot());
        	}
        }
        
        private void equipNewItems(InventoryInfo iInfo) {
        	OID bagOid = iInfo.getBags()[InventoryInfo.equipmentBag];
        	Bag equipBag = getBag(bagOid);
        	for (OID itemOID : equipBag.getItemsList()) {
        		if (itemOID == null)
        			continue;
        		AgisItem item = getAgisItem(itemOID);

        		// get the primary slot for the item
                AgisEquipSlot slot = item.getPrimarySlot();
                if (slot == null) {
                    Log.warn("AgisInventoryPlugin: slot is null for item: " + item);
                    continue;
                }

                EquipMap equipMap;
                lock.lock();
                try {
                    equipMap = getEquipMap(iInfo.getOid());
        	        // place object in slot
                    equipMap.put(slot, item.getMasterOid());
                } finally {
                    lock.unlock();
                }
                AgisInventoryClient.itemEquipStatusChanged(iInfo.getOid(), item, true, item.getPrimarySlot().toString());
                String displayVal = (String) item.getProperty("displayVal");
                EquipHelper.updateDisplay(iInfo.getOid(), displayVal, item.getPrimarySlot());
        	}
        }
    }

    /**
     * Handles the RemoveOrFindItemMessage, returning the OID of the item
     * found in the EquipSlot of the player/mob specified.
     *
     */
    class AgisFindItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            InventoryClient.RemoveOrFindItemMessage findMsg = (InventoryClient.RemoveOrFindItemMessage)msg;
            OID mobOid = findMsg.getSubject();
            String method = findMsg.getMethod();

            log.debug("AgisFindItemHook: got message");
            if (method.equals(AgisInventoryClient.INV_METHOD_SLOT)) {
            	AgisEquipSlot slot = (AgisEquipSlot)findMsg.getPayload();
            	OID resultOid = findItem(mobOid, slot);
            	Engine.getAgent().sendOIDResponse(findMsg, resultOid);
            }
            else {
            	Log.error("AgisFindItemHook: unknown method=" + method);
            }
            return true;
        }
    }

    /**
     * Calls the sendEquippedInvUpdate and the sendBagInvUpdate functions.
     */
    protected void sendInvUpdate(OID mobOid) {
        // Now send the equipped message as well
        sendEquippedInvUpdate(mobOid);
    	sendBagInvUpdate(mobOid);
    }
    
    /**
     * Handles the SendInventoryUpdateMessage, calling the sendBagInvUpdate function
     * for the specified player.
     * @author Andrew Harrison
     *
     */
    class SendInventoryUpdateHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            AgisInventoryClient.SendInventoryUpdateMessage findMsg = (AgisInventoryClient.SendInventoryUpdateMessage)msg;
	        OID mobOid = findMsg.getSubject();
	        sendBagInvUpdate(mobOid);
	        sendEquippedInvUpdate(mobOid);
	        return true;
	    }
    }
    
    /**
     * Sends down the Bags and Items found in the players Inventory.
     * @param mobOid
     */
    protected void sendBagInvUpdate(OID mobOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "BagInventoryUpdate");
		HashMap<Integer, Integer> itemCounts = new HashMap<Integer, Integer>();
    	
		int numItems = 0,unitsCount=0;
		//int numBags = 0;
		InventoryInfo iInfo = getInventoryInfo(mobOid);
		if (iInfo == null)
			return;
        OID[] items = iInfo.getBags();
        props.put("numBags", items.length);
		int  startingUnitIndex=4,endingUnitIndex=16;
        for (int bagPos = 0; bagPos < items.length; bagPos++) {
            OID subBagOid = items[bagPos];
            if (subBagOid == null) {
                log.error("sendInvUpdate: sub bag oid is null");
                continue;
            }
            Bag subBag = getBag(subBagOid);
            if (subBag == null) {
                log.error("sendInvUpdate: sub bag obj is null");
                props.put("bag_" + bagPos + "ID", 0);
                props.put("bag_" + bagPos + "Name", "");
                props.put("bag_" + bagPos + "NumSlots", 0);
                continue;
            }
            //sendInvUpdateHelper(invUpdateMsg, pos, subBag);
            props.put("bag_" + bagPos + "ID", subBag.getItemTemplateID());
            props.put("bag_" + bagPos + "Name", subBag.getName());
            props.put("bag_" + bagPos + "NumSlots", subBag.getNumSlots());
            OID[] itemsInBag = subBag.getItemsList();
            for (int itemPos = 0; itemPos < itemsInBag.length; itemPos++) {
            	boolean itemExists = true;
                // get the item
                OID oid = itemsInBag[itemPos];
                if (oid == null) {
                	itemExists = false;
                }
                AgisItem item = getAgisItem(oid);
                if (item == null) {
                    Log.warn("sendInvUpdateHelper: item is null, oid=" + oid);
                    itemExists = false;
                }
                if (itemExists) {
                	if (Log.loggingDebug)
                        log.debug("sendInvUpdateHelper: adding bagNum=" + bagPos + ", bagPos=" + itemPos +
                                  ", itemOid=" + oid + ", itemName=" + item.getName() + ",icon=" + item.getIcon());
					/***/
					int tempId = item.getTemplateID();
					if (tempId >startingUnitIndex && tempId < endingUnitIndex) unitsCount++;
					/***/
                	props.put("item_" + numItems + "TemplateID", item.getTemplateID());
                    props.put("item_" + numItems + "Name", item.getName());
                    props.put("item_" + numItems + "BaseName", item.getProperty("baseName"));
                    //props.put("item_" + numItems + "Description", description);
                    //props.put("item_" + numItems + "Icon", item.getIcon());
                    props.put("item_" + numItems + "Id", item.getOid());
                    props.put("item_" + numItems + "Count", item.getStackSize());
                    props.put("item_" + numItems + "BagNum", bagPos);
                    props.put("item_" + numItems + "SlotNum", itemPos);
                    props.put("item_" + numItems + "Bound", item.isPlayerBound());
                    if (item.getProperty("energyCost") != null) {
                    	props.put("item_" + numItems + "EnergyCost", item.getProperty("energyCost"));
                    } else {
                    	props.put("item_" + numItems + "EnergyCost", 0);
                    }
                    if (item.getProperty("durability") != null) {
                    	props.put("item_" + numItems + "Durability", item.getProperty("durability"));
                    	props.put("item_" + numItems + "MaxDurability", item.getProperty("maxDurability"));
                    } else {
                    	props.put("item_" + numItems + "MaxDurability", 0);
                    }
                    if (item.getProperty("resistanceStats") != null) {
                    	int numResist = 0;
                    	HashMap<String, Integer> resistances = (HashMap) item.getProperty("resistanceStats");
                    	for (String resistance: resistances.keySet()) {
                    		props.put("item_" + numItems + "Resist_" + numResist + "Name", resistance);
                    		props.put("item_" + numItems + "Resist_" + numResist + "Value", resistances.get(resistance));
                    		numResist++;
                    	}
                    	props.put("item_" + numItems + "NumResistances", numResist);
                    } else {
                    	props.put("item_" + numItems + "NumResistances", 0);
                    }
                    if (item.getProperty("bonusStats") != null) {
                    	int numStats = 0;
                    	HashMap<String, Integer> stats = (HashMap) item.getProperty("bonusStats");
                    	for (String statName: stats.keySet()) {
                    		props.put("item_" + numItems + "Stat_" + numStats + "Name", statName);
                    		props.put("item_" + numItems + "Stat_" + numStats + "Value", stats.get(statName));
                    		numStats++;
                    	}
                    	props.put("item_" + numItems + "NumStats", numStats);
                    } else {
                    	props.put("item_" + numItems + "NumStats", 0);
                    }
                    // If it is a weapon, add damage/speed stats
                    if (item.getItemType().equals("Weapon")) {
                    	props.put("item_" + numItems + "Delay", item.getProperty("delay"));
                    	props.put("item_" + numItems + "DamageType", item.getProperty("attackType"));
                    	props.put("item_" + numItems + "DamageValue", item.getProperty("damage"));
                    }
                    numItems++;
                    // Update the item counts map
                    if (itemCounts.containsKey(item.getTemplateID())) {
                    	itemCounts.put(item.getTemplateID(), itemCounts.get(item.getTemplateID()) + item.getStackSize());
                    } else {
                    	itemCounts.put(item.getTemplateID(), item.getStackSize());
                    }
                }
            }
        }
		boolean subjectIsPlayer = WorldManagerClient.getObjectInfo(mobOid).objType.equals(ObjectTypes.player);
    	if (subjectIsPlayer) EnginePlugin.setObjectProperty(mobOid,WorldManagerClient.NAMESPACE,"unitsCount",unitsCount);
        props.put("numItems", numItems);
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, mobOid, 
				mobOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
		// Also send an invUpdate message for the quest state etc.
		AgisInventoryClient.QuestItemsListMessage invUpdateMsg = new AgisInventoryClient.QuestItemsListMessage(mobOid, itemCounts);
		Engine.getAgent().sendBroadcast(invUpdateMsg);
		// Send currencies
        ExtendedCombatMessages.sendCurrencies(mobOid, iInfo.getCurrentCurrencies());
    }
    
    /**
     * Sends the equipped inventory bag information to the player.
     * @param mobOid
     */
    protected void sendEquippedInvUpdate(OID mobOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "EquippedInventoryUpdate");
    	
    	// go through each bag and place contents into inv update msg
		InventoryInfo iInfo = getInventoryInfo(mobOid);
		if (iInfo == null)
			return;
    	OID subBagOid = iInfo.getEquipmentItemBag();
    	if (subBagOid == null) {
    		log.error("sendInvUpdate: sub bag oid is null");
    	}
    	Bag subBag = getBag(subBagOid);
    	if (subBag == null) {
    		log.error("sendInvUpdate: sub bag obj is null");
    	}
    	
    	props.put("numSlots", subBag.getNumSlots());
    	
        for (int pos = 0; pos < subBag.getNumSlots(); pos++) {
            // get the item
            //Long oid = items[pos];
        	OID oid = subBag.getItem(pos);
            if (oid == null) {
            	props.put("item_" + pos + "Name", "");
            	continue;
            }
            AgisItem item = getAgisItem(oid);
            if (item == null) {
                Log.warn("sendInvUpdateHelper: item is null, oid=" + oid);
                props.put("item_" + pos + "Name", "");
                continue;
            }
            if (Log.loggingDebug)
                log.debug("sendEquippedUpdateHelper: " + ", itemOid=" + oid + ", itemName=" + item.getName() + ",icon=" + item.getIcon());
            //msg.addItem(bagNum, pos, oid, item.getName(), item.getIcon());
            props.put("item_" + pos + "Name", item.getName());
            props.put("item_" + pos + "BaseName", item.getProperty("baseName"));
            //props.put("item_" + numItems + "Description", description);
            //props.put("item_" + numItems + "Icon", item.getIcon());
            props.put("item_" + pos + "Id", item.getOid());
            props.put("item_" + pos + "Count", item.getStackSize());
            props.put("item_" + pos + "Bound", item.isPlayerBound());
            if (item.getProperty("energyCost") != null) {
            	props.put("item_" + pos + "EnergyCost", item.getProperty("energyCost"));
            } else {
            	props.put("item_" + pos + "EnergyCost", 0);
            }
            if (item.getProperty("durability") != null) {
            	props.put("item_" + pos + "Durability", item.getProperty("durability"));
            	props.put("item_" + pos + "MaxDurability", item.getProperty("maxDurability"));
            } else {
            	props.put("item_" + pos + "MaxDurability", 0);
            }
            if (item.getProperty("resistanceStats") != null) {
            	int numResist = 0;
            	HashMap<String, Integer> resistances = (HashMap) item.getProperty("resistanceStats");
            	for (String resistance: resistances.keySet()) {
            		props.put("item_" + pos + "Resist_" + numResist + "Name", resistance);
            		props.put("item_" + pos + "Resist_" + numResist + "Value", resistances.get(resistance));
            		numResist++;
            	}
            	props.put("item_" + pos + "NumResistances", numResist);
            } else {
            	props.put("item_" + pos + "NumResistances", 0);
            }
            if (item.getProperty("bonusStats") != null) {
            	int numStats = 0;
            	HashMap<String, Integer> stats = (HashMap) item.getProperty("bonusStats");
            	for (String statName: stats.keySet()) {
            		props.put("item_" + pos + "Stat_" + numStats + "Name", statName);
            		props.put("item_" + pos + "Stat_" + numStats + "Value", stats.get(statName));
            		numStats++;
            	}
            	props.put("item_" + pos + "NumStats", numStats);
            } else {
            	props.put("item_" + pos + "NumStats", 0);
            }
            // If it is a weapon, add damage/speed stats
            if (item.getItemType().equals("Weapon")) {
            	props.put("item_" + pos + "Delay", item.getProperty("delay"));
            	props.put("item_" + pos + "DamageType", item.getProperty("attackType"));
            	props.put("item_" + pos + "DamageValue", item.getProperty("damage"));
            }
        }
    	//sendInvUpdateHelper(invUpdateMsg, pos, subBag);
        
        // Send Ammo link
        int ammoItemID = -1;
        Integer ammoLoaded = (Integer)EnginePlugin.getObjectProperty(mobOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_LOADED);
        if (ammoLoaded != null)
        	ammoItemID = ammoLoaded;
        props.put("equippedAmmo", ammoItemID);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, mobOid, 
				mobOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    /**
     * Handles the GetGenericItemData Message. Returns the template for the
     * specified template ID.
     *
     */
    class GetGenericItemDataHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        ExtensionMessage getMsg = (ExtensionMessage)msg;
	        Template itemTmpl = null;
	        int itemID = (Integer) getMsg.getProperty("itemID");
	        itemTmpl = ObjectManagerClient.getTemplate(itemID, ObjectManagerPlugin.ITEM_TEMPLATE);
	        Engine.getAgent().sendObjectResponse(getMsg, itemTmpl);
	        return true;
	    }
    }
    
    /**
     * Handles the GetSpecificItemData Message. Sends down a message to the requesting
     * client with information about the requested items.
     *
     */
    class GetSpecificItemDataHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.getSpecificItemDataMessage getMsg = (AgisInventoryClient.getSpecificItemDataMessage)msg;
	        OID oid = getMsg.getSubject();
	        OID targetOid = (OID) getMsg.getProperty("targetOid");
	        ArrayList<Long> itemOids = (ArrayList) getMsg.getProperty("itemOids");

	        Map<String, Serializable> props = new HashMap<String, Serializable>();
			props.put("ext_msg_subtype", "TargetItemData");
	    	
			int numItems = 0;
			InventoryInfo iInfo = getInventoryInfo(targetOid);
			if (iInfo == null)
				return true;
	        OID[] items = iInfo.getBags();
	        for (int bagPos = 0; bagPos < items.length; bagPos++) {
	            OID subBagOid = items[bagPos];
	            if (subBagOid == null) {
	                log.error("sendInvUpdate: sub bag oid is null");
	                continue;
	            }
	            Bag subBag = getBag(subBagOid);
	            if (subBag == null) {
	                log.error("sendInvUpdate: sub bag obj is null");
	                continue;
	            }
	            //sendInvUpdateHelper(invUpdateMsg, pos, subBag);
	            OID[] itemsInBag = subBag.getItemsList();
	            for (int itemPos = 0; itemPos < itemsInBag.length; itemPos++) {
	            	boolean itemExists = true;
	                // get the item
	                //OID itemOid = itemsInBag[itemPos];
	                if (oid == null) {
	                	itemExists = false;
	                }
	                AgisItem item = getAgisItem(oid);
	                if (item == null) {
	                    Log.warn("sendInvUpdateHelper: item is null, oid=" + oid);
	                    itemExists = false;
	                }
	                if (itemExists && !props.containsValue(item.getName()) && itemOids.contains(item.getOid())) {
	                	if (Log.loggingDebug)
	                        log.debug("sendInvUpdateHelper: adding bagNum=" + bagPos + ", bagPos=" + itemPos +
	                                  ", itemOid=" + oid + ", itemName=" + item.getName() + ",icon=" + item.getIcon());
	                    props.put("item_" + numItems + "Name", item.getName());
	                    //props.put("item_" + numItems + "Description", description);
	                    //props.put("item_" + numItems + "Icon", item.getIcon());
	                    props.put("item_" + numItems + "Id", item.getOid());
	                    props.put("item_" + numItems + "Count", item.getStackSize());
	                    props.put("item_" + numItems + "BagNum", bagPos);
	                    props.put("item_" + numItems + "SlotNum", itemPos);
	                    /*if (item.getProperty("URL") != null) {
	                        props.put("item_" + numItems + "URL", item.getProperty("URL"));
	                    } else {
	                    	props.put("item_" + numItems + "URL", "");
	                    }*/
	                    if (item.getProperty("energyCost") != null) {
	                    	props.put("item_" + numItems + "EnergyCost", item.getProperty("energyCost"));
	                    }
	                    if (item.getProperty("resistanceStats") != null) {
	                    	int numResist = 0;
	                    	HashMap<String, Integer> resistances = (HashMap) item.getProperty("resistanceStats");
	                    	for (String resistance: resistances.keySet()) {
	                    		props.put("item_" + numItems + "Resist_" + numResist + "Name", resistance);
	                    		props.put("item_" + numItems + "Resist_" + numResist + "Value", resistances.get(resistance));
	                    		numResist++;
	                    	}
	                    	props.put("item_" + numItems + "NumResistances", numResist);
	                    } else {
	                    	props.put("item_" + numItems + "NumResistances", 0);
	                    }
	                    if (item.getProperty("bonusStats") != null) {
	                    	int numStats = 0;
	                    	HashMap<String, Integer> stats = (HashMap) item.getProperty("bonusStats");
	                    	for (String statName: stats.keySet()) {
	                    		props.put("item_" + numItems + "Stat_" + numStats + "Name", statName);
	                    		props.put("item_" + numItems + "Stat_" + numStats + "Value", stats.get(statName));
	                    		numStats++;
	                    	}
	                    	props.put("item_" + numItems + "NumStats", numStats);
	                    } else {
	                    	props.put("item_" + numItems + "NumStats", 0);
	                    }
	                    // If it is a weapon, add damage/speed stats
	                    if (item.getItemType().equals("Weapon")) {
	                    	props.put("item_" + numItems + "Speed", item.getProperty("speed"));
	                    	props.put("item_" + numItems + "DamageType", item.getProperty("attackType"));
	                    	props.put("item_" + numItems + "DamageValue", item.getProperty("damage"));
	                    }
	                    numItems++;
	                }
	            }
	        }
	    	
	        props.put("numItems", numItems);
	    	TargetedExtensionMessage TEmsg = new TargetedExtensionMessage(
					WorldManagerClient.MSG_TYPE_EXTENSION, oid, 
					oid, false, props);
			Engine.getAgent().sendBroadcast(TEmsg);
	        return true;
	    }
    }
    
    /**
     * Moves an item into the specified location.
     * @author Andrew Harrison
     *
     */
    class MoveItemHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage moveItemMsg = (ExtensionMessage)msg;
        	OID mobOid = moveItemMsg.getSubject();
	        int containerId = (Integer) moveItemMsg.getProperty("bagNum");
	        int slotId = (Integer) moveItemMsg.getProperty("slotNum");
	        OID itemOid = (OID) moveItemMsg.getProperty("itemOid");
	        int count = (Integer) moveItemMsg.getProperty("count");
	        Boolean swap = (Boolean) moveItemMsg.getProperty("swap");
	        if (swap != null && swap) {
	        	swapItemPositions(mobOid, itemOid, containerId, slotId);
	        	return true;
	        }
	        boolean fullStack = true;
	        
	        Log.debug("ITEM: got move item message with containerId: " + containerId + " and slot: " + slotId + " for item: " + itemOid);
	        InventoryInfo iInfo = getInventoryInfo(mobOid);
			if (iInfo == null || count == 0)
				return true;
	        OID[] subBags = iInfo.getBags();
	        
	        // Get item and check if we are moving the whole stack, or splitting it
    		AgisItem item = getAgisItem(itemOid);
    		Log.debug("MOVE: count: " + count + " with stackSize: " + item.getStackSize());
    		if (count < item.getStackSize()) {
    			fullStack = false;
    		}
    		
    		// If the container Id is not -1 then we need to place the item in the specified container/slot
	        if (containerId != -1) {
    			OID subBagOid = subBags[containerId];
        		Bag subBag = getBag(subBagOid);
        		OID[] itemsInBag = subBag.getItemsList();
        		OID oldBagID = (OID) item.getProperty(INVENTORY_PROP_BACKREF_KEY);
        		Log.debug("ITEM: old bag id: " + oldBagID);
        		Bag oldSubBag = subBag;
        		if (! oldBagID.equals(subBagOid)) {
        			oldSubBag = getBag(oldBagID);
            		Log.debug("ITEM: old bag is different to new bag: " + oldSubBag);
        		}
        		
        		// Make sure the slot the item will be placed in is empty
        		if (itemsInBag[slotId] != null) {
        			// Check in case it is an item of the same type and the items could be merged.
        			AgisItem oldItem = getAgisItem(itemsInBag[slotId]);
        			Log.debug("ITEM: old item: " + oldItem.getOid() + " new item: " + itemOid);
        			
        			if (oldItem.getName().equals(item.getName()) && !itemOid.equals(oldItem.getOid())) {
        				int stackSizeToAdd = item.getStackSize();
        				if (!fullStack)
        					stackSizeToAdd = count;
        				if ((oldItem.getStackSize() + stackSizeToAdd) <= oldItem.getStackLimit()) {
        					oldItem.alterStackSize(mobOid, stackSizeToAdd);
        					if (fullStack) {
        						removeItem(mobOid, itemOid, true);
        						//oldSubBag.removeItem(itemOid);
        					} else {
        						// If not full stack, only reduce the item count
        						item.alterStackSize(mobOid, -count);
        						Engine.getPersistenceManager().setDirty(item);
        					}
        					Engine.getPersistenceManager().setDirty(oldItem);
                			Engine.getPersistenceManager().setDirty(iInfo);
                			sendBagInvUpdate(mobOid);
                			return true;
        				}
        			}
        			if (!itemOid.equals(oldItem.getOid()))
        				ExtendedCombatMessages.sendErrorMessage(mobOid, "You must place the item in an empty slot.");
            		return true;
        		}
        		// Is the item equipped? If so, call unequipItem
        		if (isItemEquipped(item, mobOid)) {
        			unequipItem(item, mobOid, false);
        			return true;
        		}
        		
        		if (!fullStack) {
        			itemOid = generateItem(item.getTemplateID(), item.getName());
                	AgisItem newItem = getAgisItem(itemOid);
                	
                	if (newItem == null) {
                		return true;
                	}
                	
                	// Use set stack size as we don't need to trigger item gained/lost events
                	item.alterStackSize(mobOid, -count);
                	Engine.getPersistenceManager().setDirty(item);
                	item = newItem;
                	item.alterStackSize(mobOid, count-1);
        		} else {
        			// Remove item directly as the item is still on the player
        			oldSubBag.removeItem(itemOid);
        		}
        		
        		if (!placeItem(mobOid, mobOid, itemOid, containerId, slotId, !fullStack)) {
        			ExtendedCombatMessages.sendErrorMessage(mobOid, "Could not place item into destination slot");
            		return true;
        		}
        		// Clear the slot
        		
    		}
    		
    		sendBagInvUpdate(mobOid);
    		return true;
        }
    }
    
    /**
     * Swaps the bag/slots of two items. Takes in the itemOid of the item to move, then the bagNum and slotNum
     * to move it to, finds the item in that location then makes the switch.
     * @param playerOid
     * @param itemOid
     * @param bagNum2
     * @param slotNum2
     */
    protected void swapItemPositions(OID playerOid, OID itemOid, int bagNum2, int slotNum2) {
    	InventoryInfo iInfo = getInventoryInfo(playerOid);
		if (iInfo == null || itemOid == null)
			return;
        OID[] subBags = iInfo.getBags();
        
    	// Get the bag of the item being moved and the bag it is being moved to
    	Bag subBag1 = null;
    	int slotNum1 = -1;
    	AgisItem item1 = getAgisItem(itemOid);
    	for (int i = 0; i < subBags.length; i++) {
    		Bag subBag = getBag(subBags[i]);
    		if (subBag != null) {
    			if (subBag.findItem(itemOid) > 0) {
    				subBag1 = subBag;
    				slotNum1 = subBag.findItem(itemOid);
    				break;
    			}
    		}
    	}
    	
    	if (subBag1 == null || slotNum1 == -1) {
    		Log.error("AgisInventoryPlugin.swapItemPositions failed to find subBag for item: " + itemOid);
    		return;
    	}
    	
    	OID subBag2Oid = subBags[bagNum2];
    	Bag subBag2 = getBag(subBag2Oid);
    	OID itemOid2 = subBag2.getItem(slotNum2);
    	if (itemOid2 == null) {
    		Log.error("AgisInventoryPlugin.swapItemPositions no item found in bag: " + subBag2Oid + " slot: " + slotNum2);
    	}
    	AgisItem item2 = getAgisItem(itemOid2);
    	
    	// Now remove both items from their bags
    	if (!removeItemFromBag(playerOid, itemOid)) {
    		Log.error("AgisInventoryPlugin.swapItemPositions failed to remove item: " + itemOid);
    		return;
    	}
    	if (!removeItemFromBag(playerOid, itemOid2)) {
    		Log.error("AgisInventoryPlugin.swapItemPositions failed to remove item: " + itemOid2);
    		return;
    	}
    	
    	// Now put item 2 into slot 1
    	subBag1.putItem(slotNum1, itemOid2);
    	item2.setProperty(INVENTORY_PROP_BACKREF_KEY, subBag1.getOid());
        Engine.getPersistenceManager().setDirty(item2);
        
        // And put item 1 into slot 2
    	subBag2.putItem(slotNum2, itemOid);
    	item1.setProperty(INVENTORY_PROP_BACKREF_KEY, subBag2.getOid());
        Engine.getPersistenceManager().setDirty(item1);
    	
        Engine.getPersistenceManager().setDirty(iInfo);
    	
    	sendBagInvUpdate(playerOid);
    }
    
    /**
     * Generates the specified item and gives it to the specified player/mob. If the item has randomised stats
     * set to true, it will randomly generate the stats for the item.
     *
     */
    class GenerateItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.generateItemMessage genMsg = (AgisInventoryClient.generateItemMessage)msg;
	        OID oid = genMsg.getSubject();
	        int templateID = (Integer) genMsg.getProperty("itemID");
	        String itemName = (String) genMsg.getProperty("itemName");
	        int count = (Integer) genMsg.getProperty("count");
	        HashMap<String, Serializable> itemProps = (HashMap) genMsg.getProperty("itemProps");
	        if (itemName == null || itemName.equals("")) {
	        	Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
	        	itemName = tmpl.getName();
	        }
	        addItemFromTemplate(oid, templateID, count, itemProps);
	        //InventoryClient.addItem(oid, oid, oid, itemOid);
	        Log.debug("ITEM: finished generation and adding of item: " + templateID);
	        sendBagInvUpdate(oid);
	        return true;
	    }
    }
    
    /**
     * Creates a new AgisItem based on the templateID provided.
     * @param templateID
     * @param itemName
     * @return
     */
    private OID generateItem(int templateID, String itemName) {
    	Template itemTemplate = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
    	if (itemTemplate == null) {
    		Log.error("ITEM: generating item: " + itemName + " failed");
    		return null;
    	}
    	String templateName = itemTemplate.getName();
        Boolean randomisedStats = (Boolean) itemTemplate.get(InventoryClient.ITEM_NAMESPACE, "randomisedStats");
        if (randomisedStats != null && randomisedStats) {
        	// Randomise prefix and suffix and see if the template already exists
        	Log.debug("ITEM: generating new item: " + templateName + " which has randomised stats.");
        	ItemWeight prefix = null;
        	ItemWeight suffix = null;
        	
        	int roll = random.nextInt(100);
        	if (roll < 40) {
        		// Prefix only
        		roll = random.nextInt(itemPrefixes.size());
        		prefix = itemPrefixes.get(roll);
        		Log.debug("ITEM: prefix name: " + prefix.getItemWeightName());
        		templateName = prefix.getItemWeightName() + " " + templateName;
        	} else if (roll < 80) {
        		// Suffix only
        		roll = random.nextInt(itemSuffixes.size());
        		suffix = itemSuffixes.get(roll);
        		Log.debug("ITEM: suffix name: " + suffix.getItemWeightName());
        		templateName = templateName + " " + suffix.getItemWeightName();
        	} else {
        		// Both prefix and suffix
        		roll = random.nextInt(itemPrefixes.size());
        		prefix = itemPrefixes.get(roll);
        		roll = random.nextInt(itemSuffixes.size());
        		suffix = itemSuffixes.get(roll);
        		Log.debug("ITEM: prefix name: " + prefix.getItemWeightName() + "; suffix name: " + suffix.getItemWeightName());
        		templateName = prefix.getItemWeightName() + " " + templateName + " " + suffix.getItemWeightName();
        	}
        	
        	// Try get template, if successful, return
        	//Template baseTemplate = itemTemplate;
        	//itemTemplate = ObjectManagerClient.getTemplate(templateName);
        	//if (itemTemplate == null) {
        	Log.debug("ITEM: randomised template: " + templateName + " does not yet exist.");
        	createNewItemTemplate(itemTemplate, templateName, prefix, suffix);
        	//}
        }
        
        Template overrideTemplate = new Template();
        overrideTemplate.put(Namespace.OBJECT_MANAGER,
                ObjectManagerClient.TEMPL_PERSISTENT, true);
        OID itemOid = ObjectManagerClient.generateObject(templateID, ObjectManagerPlugin.ITEM_TEMPLATE, overrideTemplate);
        AgisItem item = getAgisItem(itemOid);
        item.setName(itemName);
        return itemOid;
    }
    
    /** 
     * Creates a new item template for an item using the given prefix and suffix item weights.
     * @param itemTemplate
     * @param newTemplateName
     * @param prefix
     * @param suffix
     * @return
     */
    private Template createNewItemTemplate(Template itemTemplate, String newTemplateName, 
    		ItemWeight prefix, ItemWeight suffix) {
    	Log.debug("ITEM: creating new item template with name: " + newTemplateName);
    	Template newItemTemplate = itemTemplate;
    	newItemTemplate.setName(newTemplateName);
    	newItemTemplate.setTemplateID(itemTemplate.getTemplateID() * -1);
    	float totalStatWeight = 0.0f;
    	double energyCost = 0.0;
    	HashMap<String, Integer> statWeights = new HashMap<String, Integer>();
    	// Set prefix/suffix data
    	if (prefix != null) {
    		totalStatWeight += prefix.getTotalStatWeight();
    		if (prefix.getStat1() != null && prefix.getWeight1() > 0) {
    			statWeights.put(prefix.getStat1(), prefix.getWeight1());
    		}
    		if (prefix.getStat2() != null && prefix.getWeight2() > 0) {
    			statWeights.put(prefix.getStat2(), prefix.getWeight2());
    		}
    		if (prefix.getStat3() != null && prefix.getWeight3() > 0) {
    			statWeights.put(prefix.getStat3(), prefix.getWeight3());
    		}
    	}
    	if (suffix != null) {
    		totalStatWeight += suffix.getTotalStatWeight();
    		if (suffix.getStat1() != null && suffix.getWeight1() > 0) {
    			if (statWeights.containsKey(suffix.getStat1())) {
    				statWeights.put(suffix.getStat1(), suffix.getWeight1() + statWeights.get(suffix.getStat1()));
    			} else {
    				statWeights.put(suffix.getStat1(), suffix.getWeight1());
    			}
    		}
    		if (suffix.getStat2() != null && suffix.getWeight2() > 0) {
    			if (statWeights.containsKey(suffix.getStat2())) {
    				statWeights.put(suffix.getStat2(), suffix.getWeight2() + statWeights.get(suffix.getStat2()));
    			} else {
    				statWeights.put(suffix.getStat2(), suffix.getWeight2());
    			}
    		}
    		if (suffix.getStat3() != null && suffix.getWeight3() > 0) {
    			if (statWeights.containsKey(suffix.getStat3())) {
    				statWeights.put(suffix.getStat3(), suffix.getWeight3() + statWeights.get(suffix.getStat3()));
    			} else {
    				statWeights.put(suffix.getStat3(), suffix.getWeight3());
    			}
    		}
    	}
    	// Stat Quantity = ((Item Quality * Slot Modifier * Grade Modifier)^1.7 / Total Stat Weight * Current Stat Weight)^(1/1.7)/StatMod *Round Down*
    	float itemQuality = (Integer) itemTemplate.get(InventoryClient.ITEM_NAMESPACE, "itemQuality");
    	String slot = (String) itemTemplate.get(InventoryClient.ITEM_NAMESPACE, "slot");
    	float slotModifier = getSlotModifier(slot);
    	int itemGrade = (Integer) itemTemplate.get(InventoryClient.ITEM_NAMESPACE, "itemGrade");
    	float gradeModifier = getGradeModifier(itemGrade);
    	//HashMap<String, Integer> itemStats = (HashMap) newItemTemplate.get(InventoryClient.ITEM_NAMESPACE, "bonusStats");
    	HashMap<String, Integer> itemStats = new HashMap<String, Integer>();
    	double totalStatsValue = Math.pow((itemQuality * slotModifier * gradeModifier), 1.7);
    	Log.debug("CALC: totalStatsValue: " + totalStatsValue);
    	for (String statName: statWeights.keySet()) {
    		float statMod = getStatModifier(statName);
    		Log.debug("CALC: itemQuality: " + itemQuality + "; slotMod: " + slotModifier + "; gradeModifier: " + gradeModifier + 
    				"; totalStatWeight: " + totalStatWeight + "; statWeight: " + (float)statWeights.get(statName));
    		double statCalc1 = totalStatsValue / totalStatWeight * (float)statWeights.get(statName);
    		Log.debug("CALC: statCalc1: " + statCalc1 + "; statMod: " + statMod);
    		double statValue = Math.floor(Math.pow(statCalc1, (1.0/1.7))/statMod);
    		Log.debug("CALC: final stat value for " + statName + " is: " + statValue);
    		itemStats.put(statName, (int)statValue);
    		energyCost += Math.pow(statValue * statMod, 1.7);
    	}
    	energyCost = Math.ceil(energyCost);
    	newItemTemplate.put(InventoryClient.ITEM_NAMESPACE, "bonusStats", itemStats);
    	newItemTemplate.put(InventoryClient.ITEM_NAMESPACE, "energyCost", (int) energyCost);
    	ObjectManagerClient.registerTemplate(newItemTemplate);
    	return newItemTemplate;
    }
    
    /**
     * Un-used at the moment. Eventually will be used to create an item to summon a pet.
     * @author Andrew
     *
     */
    class CreatePetItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.generateItemMessage genMsg = (AgisInventoryClient.generateItemMessage)msg;
	        OID oid = genMsg.getSubject();
	        String templateName = (String) genMsg.getProperty("itemName");
	        //Long itemOid = generateItem(templateName);
	        //InventoryClient.addItem(oid, oid, oid, itemOid);
	        Log.debug("ITEM: finished generation and adding of item: " + templateName);
	        return true;
	    }
    }
    
    /** 
     * Creates a new item template for an item using the given prefix and suffix item weights.
     * @param itemTemplate
     * @param newTemplateName
     * @param prefix
     * @param suffix
     * @return
     */
    private Template createPetItemTemplate(Template itemTemplate, String newTemplateName, 
    		String petRef) {
    	Log.debug("ITEM: creating new item template with name: " + newTemplateName);
    	Template newItemTemplate = itemTemplate;
    	newItemTemplate.setName(newTemplateName);
    	newItemTemplate.put(InventoryClient.ITEM_NAMESPACE, "petRef", petRef);
    	ObjectManagerClient.registerTemplate(newItemTemplate);
    	return newItemTemplate;
    }
    
    /**
     * Hook for the RemoveSpecificItem Message Removes the specified item (using its oid) from the 
     * specified player/mob. If the removeStack property is set to true it will remove the full 
     * item stack from the player.
     *
     */
    class RemoveSpecificItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.removeSpecificItemMessage removeMsg = (AgisInventoryClient.removeSpecificItemMessage)msg;
	        OID oid = removeMsg.getSubject();
	        HashMap<OID, Integer> itemsToRemove = removeMsg.getItemsToRemove();
	        boolean removeStack =  removeMsg.getRemoveStack();
	        
	        for(OID itemOid : itemsToRemove.keySet()) {
	        	removeSpecificItem(oid, itemOid, itemsToRemove.get(itemOid), removeStack);
	        }
	        
	    	//Send an inventory update
	    	sendBagInvUpdate(oid);
	        return true;
	    }
    }
    
    void removeSpecificItem(OID oid, OID itemOid, int numToRemove, boolean removeStack) {
    	InventoryInfo iInfo = getInventoryInfo(oid);
    	lock.lock();
    	try {
    	    AgisItem item = getAgisItem(itemOid);
    	    if (item == null)
    		    return;
    	    if (!removeStack) {
    	    	// Just reduce the stack size
    	    	item.alterStackSize(oid, -numToRemove);
    	    	Engine.getPersistenceManager().setDirty(item);
    			Engine.getPersistenceManager().setDirty(iInfo);
    			Log.debug("ITEM: reduced stack: " + itemOid + " of item type: " + item.getName() + " to size: " + item.getStackSize());
    	    }
    	    if (item.getStackSize() < 1 || removeStack) {
    	    	unequipItem(item, oid, false);
    	    	OID rootBagOid = oid;
	    	    if (rootBagOid == null) {
	    		    log.debug("removeItem: cant find rootBagOid");
	    		    return;
	    	    }
	    	    Boolean result = removeItemFromBag(rootBagOid, itemOid);
	    	    if (result)
	    	    	item.unacquired(rootBagOid);
	    	}
    	} finally {
    	    lock.unlock();
    	}
    }
    
    /**
     * Removes the generic item (using its name) from the specified player/mob. If the removeStack
     * property is set to true it will remove the full item stack from the player.
     *
     */
    class RemoveGenericItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.removeGenericItemMessage removeMsg = (AgisInventoryClient.removeGenericItemMessage)msg;
	        OID oid = removeMsg.getSubject();
	        HashMap<Integer, Integer> itemsToRemove = removeMsg.getItemsToRemove();
	        boolean removeStack = removeMsg.removeStack();
	        
	        for(int itemID : itemsToRemove.keySet()) {
	        	removeGenericItem(oid, itemID, itemsToRemove.get(itemID), removeStack);
	        }
	        
	    	//Send an inventory update
	        sendInvUpdate(oid);
	        return true;
	    }
    }
    
    void removeGenericItem(OID oid, int itemID, int numToRemove, boolean removeStack) {
    	InventoryInfo iInfo = getInventoryInfo(oid);
        lock.lock();
        try {
    	    ArrayList<OID> itemOids = findItemStacks(oid, itemID);
    	    for (OID itemOid: itemOids) {
    	    	AgisItem item = getAgisItem(itemOid);
    	    	Log.debug("ITEM: found stack: " + itemOid + " of item type: " + item.getName() + " to remove from.");
	    	    if (item == null)
	    	    	continue;
	    	    if (!removeStack) {
	    	    	// Just reduce the stack size
	    	    	int stackSize = item.getStackSize();
	    	    	item.alterStackSize(oid, -numToRemove);
	    	    	numToRemove = numToRemove - stackSize;
	    	    	Engine.getPersistenceManager().setDirty(item);
        			Engine.getPersistenceManager().setDirty(iInfo);
        			Log.debug("ITEM: reduced stack: " + itemOid + " of item type: " + item.getName() + " to size: " + item.getStackSize());
	    	    }
	    	    if (item.getStackSize() < 1 || removeStack) {
	    	    	unequipItem(item, oid, false);
	    	    	OID rootBagOid = oid;
		    	    if (rootBagOid == null) {
		    		    log.debug("removeItem: cant find rootBagOid");
		    		    continue;
		    	    }
		    	    Boolean result = removeItemFromBag(rootBagOid, itemOid);
		    	    if (result) {
		    	    	item.unacquired(rootBagOid);
		    	    }
		    	    Log.debug("ITEM: removing from stack: " + itemOid + " of item type: " + item.getName() + " had result: " + result);
    	    	}
	    	    if (numToRemove < 1)
	    	    	break;
    	    }
    	} finally {
    	    lock.unlock();
    	}
    }
    
    /**
     * Checks if the player has enough of the specified currency against the requested amount.
     * Sends a boolean response to indicate if the player has enough.
     * @author Andrew Harrison.
     *
     */
    class CheckCurrencyHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.checkCurrencyMessage purMsg = (AgisInventoryClient.checkCurrencyMessage)msg;
	        OID oid = purMsg.getSubject();
	        //OID mobOid = OID.fromLong((Long) purMsg.getProperty("mobOid"));
	        int currencyID = purMsg.getCurrencyID();
	        int cost = purMsg.getCount();
	        int currencyAmount = getMobCurrency(oid, currencyID);
	        
	        if (currencyAmount < cost) {
	        	// Send boolean response back saying it was a success
	        	Engine.getAgent().sendBooleanResponse(purMsg, false);
	        	Log.debug("CURRENCY: not enough money");
	        	return true;
	        }
	        Log.debug("CURRENCY: has enough money");
	        
        	// Send boolean response back saying it was a success
        	Engine.getAgent().sendBooleanResponse(purMsg, true);
	        return true;
	    }
    }
    
    /**
     * Gets the amount of the specified currency the mob specified has.
     * @param mobOid
     * @param currencyID
     * @return
     */
    public int getMobCurrency(OID mobOid, int currencyID) {
    	Log.debug("CURRENCY: getting mob currency: " + currencyID + " for mob: " + mobOid);
    	Currency c = Agis.CurrencyManager.get(currencyID);
    	if (c.getExternal()) {
    		OID accountID = (OID) EnginePlugin.getObjectProperty(mobOid, WorldManagerClient.NAMESPACE, WorldManagerClient.ACCOUNT_PROPERTY);
    		return aDB.getCharacterCoinAmount(accountID);
    	} else {
    		InventoryInfo iInfo = getInventoryInfo(mobOid);
    		return iInfo.getCurrencyAmount(currencyID, true);
    	}
    }
    
    /**
     * Changes the amount of the specified currency for the player 
     * by the specified amount.
     * @param mobOid
     * @param currencyID
     * @param delta
     */
    public void alterMobCurrency(OID mobOid, int currencyID, int delta) {
    	Currency c = Agis.CurrencyManager.get(currencyID);
    	Log.debug("CURRENCY: getting currency: " + currencyID);
    	if (c.getExternal()) {
    		OID accountID = (OID)EnginePlugin.getObjectProperty(mobOid, WorldManagerClient.NAMESPACE, WorldManagerClient.ACCOUNT_PROPERTY);
    		aDB.alterCharacterCoinAmount(accountID, delta);
    	} else {
    		InventoryInfo iInfo = getInventoryInfo(mobOid);
    		iInfo.alterCurrencyAmount(currencyID, delta);
    		Engine.getPersistenceManager().setDirty(iInfo);
    	}
    	InventoryInfo iInfo = getInventoryInfo(mobOid);
    	ExtendedCombatMessages.sendCurrencies(mobOid, iInfo.getCurrentCurrencies());
    }
    
    /**
     * Handles the request to get the Merchant List. Calls sendMerchantList.
     * @author Andrew
     *
     */
    class GetMerchantListHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        ExtensionMessage getMsg = (ExtensionMessage)msg;
	        OID oid = getMsg.getSubject();
	        int merchantTable = (Integer) getMsg.getProperty("merchantTable");
	        //sendMerchantList(oid, merchantTable);
	        return true;
	    }
    }
    
    /**
     * Handles the request to purchase an item. Checks if the merchant does
     * sell the item, then if the player has enough currency to purchase it. 
     * If the checks are met, the currency is deducted and the item is generated
     * and given to the player.
     * @author Andrew Harrison.
     *
     */
    class PurchaseItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.purchaseItemMessage purMsg = (AgisInventoryClient.purchaseItemMessage)msg;
	        OID oid = purMsg.getSubject();
	        //OID mobOid = OID.fromLong((Long) purMsg.getProperty("mobOid"));
	        int templateID = purMsg.getItemID();
	        int count = purMsg.getCount();
	        Log.debug("PURCHASE: attempting to purchase " + count + " of item " + templateID);
	        Map<String, Serializable> props = new HashMap<String, Serializable>();
			props.put("ext_msg_subtype", "item_purchase_result");
			Template itemTemplate = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
			props.put("itemName", itemTemplate.getName());
			TargetedExtensionMessage resultMsg = new TargetedExtensionMessage(
					WorldManagerClient.MSG_TYPE_EXTENSION, oid, 
					oid, false, props);
	        // First make sure the merchant can sell this item
	        //int tableNum = (Integer) EnginePlugin.getObjectProperty(mobOid, WorldManagerClient.NAMESPACE, "merchantTable");
	        /*int tableNum = (Integer) purMsg.getProperty("tableNum");
	        MerchantTable mt = merchantTables.get(tableNum);
			
	        if (!mt.getItems().contains(templateID)) {
	        	props.put("result", "no_item");
	        	Engine.getAgent().sendBroadcast(resultMsg);
	        	Log.error("MERCHANT: table " + tableNum + " does not contain item: " + templateID);
	        	return true;
	        }*/
	        // Now make sure the player can afford the item
	        Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
	        int purchaseCurrency = (Integer) tmpl.get(InventoryClient.ITEM_NAMESPACE, "purchaseCurrency");
	        int cost = (Integer) tmpl.get(InventoryClient.ITEM_NAMESPACE, "purchaseCost");
	        int currencyAmount = getMobCurrency(oid, purchaseCurrency);
	        Currency c = Agis.CurrencyManager.get(purchaseCurrency);
	        if (currencyAmount < (cost * count)) {
	        	props.put("result", "insufficient_funds");
	        	props.put("currency", c.getCurrencyName());
	        	Engine.getAgent().sendBroadcast(resultMsg);
	        	Log.debug("PURCHASE: not enough funds: " + currencyAmount + " for cost: " + (cost * count));
	        	// Send boolean response back saying it was a success
	        	Engine.getAgent().sendBooleanResponse(purMsg, false);
	        	return true;
	        }
	        Log.debug("PURCHASE: has enough money");
	        
	        // Check if the player has space
	        if (!hasSpace(oid, templateID, count, 0)) {
	        	props.put("result", "insufficient_space");
	        	Engine.getAgent().sendBroadcast(resultMsg);
	        	ExtendedCombatMessages.sendErrorMessage(oid, "You do not have enough space in your inventory to purchase that item.");
	        	// Send boolean response back saying it was a success
	        	Log.debug("PURCHASE: not enough space: ");
	        	Engine.getAgent().sendBooleanResponse(purMsg, false);
	        	return true;
	        }
	        
	        boolean itemAdded = addItemFromTemplate(oid, templateID, count, null);
	        //boolean itemAdded = InventoryClient.addItem(oid, oid, oid, itemOid);
	        if (itemAdded) {
	        	int delta = (-cost * count);
	        	alterMobCurrency(oid, purchaseCurrency, delta);
	        	sendBagInvUpdate(oid);
	        }
	        Log.debug("ITEM: finished generation and adding of item: " + templateID);
	        props.put("result", "success");
        	Engine.getAgent().sendBroadcast(resultMsg);
        	// Send boolean response back saying it was a success
        	Engine.getAgent().sendBooleanResponse(purMsg, true);
	        return true;
	    }
    }
    
    /**
     * Removes an item from a players inventory and increases the players matching currency type by the value of the item.
     * First checks if the item is an Account item - if so, sellAccountItem() is called;
     * @author Andrew Harrison
     *
     */
    class SellItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        ExtensionMessage removeMsg = (ExtensionMessage)msg;
	        OID oid = removeMsg.getSubject();
	        
	        String sellType = (String)removeMsg.getProperty("sellType");
	        if (sellType.equals("Account")) {
	        	int itemID = (Integer) removeMsg.getProperty("itemID");
	        	Log.debug("SELL: got account sell for item: " + itemID);
	        	//sellAccountItem(oid, itemID);
	        }
	        OID itemOid = (OID) removeMsg.getProperty("itemOid");
	        Log.debug("SELL: got sell for item: " + itemOid);
	        lock.lock();
	    	try {
	    	    AgisItem item = getAgisItem(itemOid);
	    	    if (item == null)
	    		    return true;
	    	    unequipItem(item, oid, false);
	    	    OID rootBagOid = oid;
		    	if (rootBagOid == null) {
		    		log.debug("removeItem: cant find rootBagOid");
		    		return true;
		    	}
		    	Log.debug("SELL: got root bag");
		    	if (item.getPurchaseCurrency() < 1) {
		    		EventMessageHelper.SendErrorEvent(oid, EventMessageHelper.CANNOT_SELL_ITEM, item.getTemplateID(), "");
		    		return true;
		    	}
		    	// Check if item is sellable
		    	boolean sellable = item.getBooleanProperty("sellable");
		    	if (!sellable) {
		    		EventMessageHelper.SendErrorEvent(oid, EventMessageHelper.CANNOT_SELL_ITEM, item.getTemplateID(), "");
		    		return true;
		    	}
		    	boolean removed = removeItemFromBag(rootBagOid, itemOid);
		    	if (removed) {
		    		Log.debug("SELL: removed Item");
		    		int delta = (int)(item.getPurchaseCost() / SELL_FACTOR);
		    		delta *= item.getStackSize();
		    		if (delta == 0 && item.getPurchaseCost() > 0)
		    			delta = 1;
		    		alterMobCurrency(oid, item.getPurchaseCurrency(), delta);
		    		item.unacquired(rootBagOid);
		    		//TODO: We should look at adding some list or something to hold sold items for buy back
		    	} else {
		    		Log.debug("SELL: remove failed");
		    	}
	    	} finally {
	    	    lock.unlock();
	    	}
	    	//Send an inventory update
	    	sendBagInvUpdate(oid);
	        return true;
	    }
    }
    
    /**
     * Deprecated. Handles the request to generate numerous of an item. This functionality is found in GenerateItemHook.
     */
    class AlterItemCountHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        ExtensionMessage purMsg = (ExtensionMessage)msg;
	        OID playerOid = purMsg.getSubject();
	        Log.debug("ALTER: got item alter message");
	        int templateID = (Integer) purMsg.getProperty("itemID");
	        int count = (Integer) purMsg.getProperty("count");
	        Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
			if (tmpl == null) {
				Log.error("PICKUP: item ID: " + templateID + " does not exist");
				return true;
			}
			
			addItemFromTemplate(playerOid, templateID, count, null);
			WorldManagerClient.sendObjChatMsg(playerOid, 2, "Received " + tmpl.getName() + " x" + count);
			
			sendBagInvUpdate(playerOid);
	        return true;
        }
    }
    
    /**
     * Generates the specified item and gives it to the specified player/mob. Should be called when a player
     * walks into (picks up) an item.
     *
     */
    class PickupItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        ExtensionMessage purMsg = (ExtensionMessage)msg;
	        OID oid = purMsg.getSubject();
	        int templateID = (Integer) purMsg.getProperty("itemID");
	        int count = (Integer) purMsg.getProperty("count");
	        int itemsAcquired = 0;
	        if (templateID == -1) {
	        	HashMap<Integer, Integer> lootTables = (HashMap) purMsg.getProperty("lootTables");
	        	Log.error("LOOT: num loot tables: " + lootTables.size());
	        	for (int lootTable: lootTables.keySet()) {
	        		int tableChance = lootTables.get(lootTable);
	        		int roll = random.nextInt(100);
	        		Log.error("LOOT: roll for loot table: " + lootTable + " is: " + roll + " with tablechance: " + tableChance);
	        		if (roll < tableChance) {
	        			// Check the loot table number
	    	        	Log.debug("LOOT: lootManager has: " + Agis.LootTableManager.getMap());
	    				LootTable lt = Agis.LootTableManager.get(lootTable);
	    				int itemNum = lt.getRandomItemNum();
	    				Log.debug("LOOT: randomised item number: " + itemNum);
	    				templateID = lt.getItems().get(itemNum);
	    				count = lt.getItemCounts().get(itemNum);
	        			if (accountItemAcquired(oid, templateID, count) == ItemAcquireResult.SUCCESS)
	        				itemsAcquired++;
	        		}
	        	}
	        } else {
	        	if (accountItemAcquired(oid, templateID, count) == ItemAcquireResult.SUCCESS)
	        		itemsAcquired++;
	        }
	        sendBagInvUpdate(oid);
		    if (itemsAcquired == 0) {
		    	// There was no items
		        ExtendedCombatMessages.sendCombatText(oid, "No items found", 9);
		    }
	        return true;
	    }
    }
    
    /**
     * Old function that should be deleted. Do not use.
     * @param oid
     * @param templateID
     * @param count
     * @return
     */
    protected ItemAcquireResult accountItemAcquired(OID oid, int templateID, int count) {
    	Log.error("PICKUP: got pickup message with player: " + oid + " and item: " + templateID);
		Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
		if (tmpl == null) {
			Log.error("PICKUP: item ID: " + templateID + " does not exist");
			return ItemAcquireResult.ITEM_DOESNT_EXIST;
		}
        
        // Check if the player has space
        if (!hasSpace(oid, templateID, count, 0)) {
        	ExtendedCombatMessages.sendErrorMessage(oid, "You do not have enough space in your inventory to receive that item.");
        	return ItemAcquireResult.STACK_LIMIT_REACHED;
        }
        
        Log.debug("ITEM: finished generation and adding of item: " + templateID);
        if (addItemFromTemplate(oid, templateID, count, null))
        	return ItemAcquireResult.SUCCESS;
        else
        	return ItemAcquireResult.UNKNOWN_FAILURE;
    }
    
    /**
     * Handles the EquippedItemUsedMessage. Rolls a chance to reduce the durability of an item. Should
     * be called when an item has been used by some process such as a combat ability, crafting, 
     * resource gathering etc.
     * @author Andrew Harrison
     *
     */
    class EquippedItemUsedHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	EquippedItemUsedMessage purMsg = (EquippedItemUsedMessage)msg;
	        OID oid = purMsg.getSubject();
	        
	        String useType = (String) purMsg.getUseType();
	        OID itemOID = null;
	        Random rand = new Random();
	        if (useType.equals("Attack")) {
	        	EquipMap equipMap = getEquipMap(oid);
	        	itemOID = equipMap.get(AgisEquipSlot.PRIMARYWEAPON);
	        	if (itemOID != null && rand.nextInt(100) < DURABILITY_LOSS_CHANCE_FROM_ATTACK)
	        		reduceDurabilityOfItem(oid, itemOID, true);
	        } else if (useType.equals("Defend")) {
	        	EquipMap equipMap = getEquipMap(oid);
	        	// Roll a chance for each item
	        	for(AgisEquipSlot slot : equipMap.map.keySet()) {
	        		if (!slot.equals(AgisEquipSlot.PRIMARYWEAPON)) {
	        			itemOID = equipMap.get(slot);
	    	        	if (itemOID != null && rand.nextInt(100) < DURABILITY_LOSS_CHANCE_FROM_DEFEND)
	    	        		reduceDurabilityOfItem(oid, itemOID, true);
	        		}
	        	}
	        } else if (useType.equals("Craft")) {
	        	EquipMap equipMap = getEquipMap(oid);
	        	itemOID = equipMap.get(AgisEquipSlot.PRIMARYWEAPON);
	        	if (itemOID != null && rand.nextInt(100) < DURABILITY_LOSS_CHANCE_FROM_CRAFT)
	        		reduceDurabilityOfItem(oid, itemOID, true);
	        } else if (useType.equals("Gather")) {
	        	EquipMap equipMap = getEquipMap(oid);
	        	itemOID = equipMap.get(AgisEquipSlot.PRIMARYWEAPON);
	        	if (itemOID != null && rand.nextInt(100) < DURABILITY_LOSS_CHANCE_FROM_GATHER)
	        		reduceDurabilityOfItem(oid, itemOID, true);
	        }
	        return true;
        }
    }
    
    void reduceDurabilityOfItem(OID playerOid, OID itemOid, boolean isWeapon) {
    	AgisItem item = getAgisItem(itemOid);
     	// Reduce the durability of the item
     	if (item.getProperty("durability") != null) {
     		int durability = (Integer)item.getProperty("durability");
     		durability--;
     		if (durability == 0) {
     			// Destroy item
     			/*unequipItem(item, playerOid, false);
	    	    OID rootBagOid = playerOid;
		    	if (rootBagOid == null) {
		    		log.debug("removeItem: cant find rootBagOid");
		    		return;
		    	}
		    	Boolean result = removeItemFromBag(rootBagOid, item.getOid());*/
     			item.setProperty("durability", 0);
     			AgisInventoryClient.itemEquipStatusChanged(playerOid, item, true, "");
     			
		    	ExtendedCombatMessages.sendErrorMessage(playerOid, "Your " + item.getName() + " broke.");
     		} else if (durability > 0) {
     			item.setProperty("durability", durability);
     			if (durability == 5) {
     				ExtendedCombatMessages.sendErrorMessage(playerOid, "Your " + item.getName() + "'s durability is getting low. It will break when it reaches 0.");
     			}
     		}
     		Engine.getPersistenceManager().setDirty(item);
     		sendInvUpdate(playerOid);
     		sendEquippedInvUpdate(playerOid);
     	}
    }
    
    class RepairItemsHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage purMsg = (ExtensionMessage)msg;
	        OID playerOid = purMsg.getSubject();
	        
	        boolean repairAll = (Boolean) purMsg.getProperty("repairAll");
	        if (repairAll) {
	        	ArrayList<AgisItem> items = new ArrayList<AgisItem>();
	        	InventoryInfo iInfo = getInventoryInfo(playerOid);
	        	OID[] subBags = iInfo.getBags();
	        	for (int pos = 0; pos < subBags.length; pos++) {
		        	OID subBag = subBags[pos];
		        	Bag bag = getBag(subBag);
		        	for (OID itemOid : bag.getItemsList()) {
		        		if (itemOid != null) {
		        			AgisItem item = getAgisItem(itemOid);
		        			if (item.getProperty("durability") != null) {
			        			items.add(item);
			        		}
		        		}
		        	}
	        	}
	        	
	        	EquipMap equipMap = getEquipMap(playerOid);
	        	for(AgisEquipSlot slot : equipMap.map.keySet()) {
	        		OID itemOid = equipMap.get(slot);
	    	        if (itemOid != null) {
	    	        	AgisItem item = getAgisItem(itemOid);
	    	        	if (item.getProperty("durability") != null) {
		        			items.add(item);
		        		}
	    	        }
	        	}
	        	
	        	boolean repairSuccessful = repairItems(playerOid, items);
	        	if (repairSuccessful) {
	        		EventMessageHelper.SendGeneralEvent(playerOid, EventMessageHelper.REPAIR_SUCCESSFUL, 0, "");
	        	}
	        } else {
	        	ArrayList<AgisItem> items = new ArrayList<AgisItem>();
	        	int numItems = (Integer)purMsg.getProperty("numItems");
	        	for (int i = 0; i < numItems; i++) {
	        		OID itemOid = (OID) purMsg.getProperty("itemOid" + i);
	        		AgisItem item = getAgisItem(itemOid);
	        		if (item.getProperty("durability") != null) {
	        			items.add(item);
	        		}
	        	}
	        	boolean repairSuccessful = repairItems(playerOid, items);
	        	if (repairSuccessful) {
	        		EventMessageHelper.SendGeneralEvent(playerOid, EventMessageHelper.REPAIR_SUCCESSFUL, 0, "");
	        	}
	        }
	        
	        sendInvUpdate(playerOid);
     		sendEquippedInvUpdate(playerOid);
	        
	        return true;
        }
    }
    
    boolean repairItems(OID playerOid, ArrayList<AgisItem> items) {
    	if (items.size() == 0) {
    		EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.NO_ITEM_DURABILITY, 0, "");
        	return false;
    	}
    	
        //TODO: work out some repair cost - Currently assuming all use the same currency
    	int purchaseCurrency = (Integer) items.get(0).getProperty("purchaseCurrency");
        int totalCost = 0;
        
        for (AgisItem item : items) {
        	int durability = (Integer)item.getProperty("durability");
        	int maxDurability = (Integer)item.getProperty("maxDurability");
        
        	float damagePercent = (float)durability / (float)maxDurability;
        
        	if (USE_FLAT_REPAIR_RATE) {
        		int grade = (Integer)item.getProperty("itemGrade");
        		int cost = 25 + (grade * 10);
        		//int cost = (Integer) item.getProperty("purchaseCost");
        		cost = (int)((float)cost * (1.0f - damagePercent));
        		totalCost += cost;
        	} else {
        		int cost = (Integer) item.getProperty("purchaseCost");
        		cost = (int)((float)cost * REPAIR_RATE * (1.0f - damagePercent));
        		totalCost += cost;
        	}
        }
        
        int currencyAmount = getMobCurrency(playerOid, purchaseCurrency);
        Log.debug("Currency: repair cost: " + totalCost + " against players currency: " + currencyAmount);
        if (currencyAmount < totalCost) {
        	EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.NOT_ENOUGH_CURRENCY, 0, "");
        	return false;
        }
        alterMobCurrency(playerOid, purchaseCurrency, -totalCost);
        
        // Set durability back to maximum
        for (AgisItem item : items) {
        	item.setProperty("durability", (Integer)item.getProperty("maxDurability"));
        
        	// If item is equipped, re-apply the equipped status
        	EquipMap equipMap = getEquipMap(playerOid);
        	for(AgisEquipSlot slot : equipMap.map.keySet()) {
        		OID equippedItem = equipMap.get(slot);
        		if (item.getOid().equals(equippedItem)) {
        			AgisInventoryClient.itemEquipStatusChanged(playerOid, item, true, "");
        		}
        	}
        
        	Engine.getPersistenceManager().setDirty(item);
        }
        
        return true;
    }
    
    /**
     * Handles the AlterCurrencyMessage. Alters a players amount of the currency specified.
     * @author Andrew Harrison
     *
     */
    class AlterCurrencyHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.alterCurrencyMessage alterMsg = (AgisInventoryClient.alterCurrencyMessage)msg;
	        OID oid = alterMsg.getSubject();
	        int currencyType = alterMsg.getCurrencyType();
	        int delta = alterMsg.getDelta();
	        alterMobCurrency(oid, currencyType, delta);
	        return true;
	    }
    }

    /**
     * handles requests to start a trading session
     *
     */
    class TradeStartReqHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage startMsg = (ExtensionMessage)msg;
            OID trader1Oid = (OID)startMsg.getProperty("requesterOid");
            OID trader2Oid = (OID)startMsg.getProperty("partnerOid");
            
            if (!CombatPlugin.isPlayerAlive(trader1Oid)) {
            	return true;
            } else if (!CombatPlugin.isPlayerAlive(trader2Oid)) {
            	return true;
            }

            Log.debug("TradeStartReqHook: trader1=" + trader1Oid + " trader2=" + trader2Oid);
            if (trader1Oid.equals(trader2Oid)) {
            	return true;
            }
            if (tradeSessionMap.containsKey(trader1Oid) || tradeSessionMap.containsKey(trader2Oid)) {
            	sendTradeComplete(trader1Oid, trader2Oid, AgisInventoryClient.tradeBusy);
            	return true;
            }
            TradeSession tradeSession = new TradeSession(trader1Oid, trader2Oid);
            tradeSessionMap.put(trader1Oid, tradeSession);
            tradeSessionMap.put(trader2Oid, tradeSession);
            sendTradeStart(trader1Oid, trader2Oid);
            sendTradeStart(trader2Oid, trader1Oid);
            return true;
        }
    }

    /**
     * send an ao.TRADE_COMPLETE message to trader1, telling it that a trade with trader2 has completed
     */
    protected static void sendTradeComplete(OID trader1, OID trader2, byte status) {
        Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "ao.TRADE_COMPLETE");
        props.put("status", status);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(AgisInventoryClient.MSG_TYPE_TRADE_COMPLETE,
                                                                    trader1, trader2, false, props);
        Engine.getAgent().sendBroadcast(msg);
    }

    /**
     * sends an ao.TRADE_START message to trader1 telling it that a trade has started with trader2
     */
    protected static void sendTradeStart(OID trader1, OID trader2) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "ao.TRADE_START");
        TargetedExtensionMessage msg = new TargetedExtensionMessage(AgisInventoryClient.MSG_TYPE_TRADE_START,
                                                                    trader1, trader2, false, props);
        Engine.getAgent().sendBroadcast(msg);
    }

    /**
     * handles requests to update an existing trading session
     *
     */
    class TradeOfferReqHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage tradeMsg = (ExtensionMessage)msg;
            OID trader1 = (OID)tradeMsg.getProperty("requesterOid");
            OID trader2 = (OID)tradeMsg.getProperty("partnerOid");

            Log.debug("TradeOfferReqHook: trader1=" + trader1 + " trader2=" + trader2);
            TradeSession tradeSession = tradeSessionMap.get(trader1);

            // fail if trade session doesn't exist or is invalid
            if ((tradeSession == null) || !tradeSession.isTrader(trader2)) {
            	sendTradeComplete(trader1, trader2, AgisInventoryClient.tradeFailed);
            	if (tradeSession != null) {
            		tradeSessionMap.remove(trader1);
            		OID partner = tradeSession.getPartnerOid(trader1);
            		tradeSessionMap.remove(partner);
            		sendTradeComplete(partner, trader1, AgisInventoryClient.tradeFailed);
            	}
            	return true;
            }

            List<OID> offer = (List<OID>)tradeMsg.getProperty("offerItems");
            HashMap<String, Integer> currencyOffer = (HashMap<String, Integer>)tradeMsg.getProperty("offerCurrencies");
            Log.debug("TradeOfferReqHook: got offerCurrencies");
            // if offer is cancelled or invalid, fail
            boolean cancelled = (Boolean)tradeMsg.getProperty("cancelled");
            if (cancelled || !validateTradeOffer(trader1, offer)) {
            	byte status = AgisInventoryClient.tradeFailed;
            	if (cancelled) {
            		status = AgisInventoryClient.tradeCancelled;
            	}
            	tradeSessionMap.remove(trader1);
            	tradeSessionMap.remove(trader2);
            	sendTradeComplete(trader1, trader2, status);
            	sendTradeComplete(trader2, trader1, status);
            	return true;
            }

            // update session with this offer
            boolean accepted = (Boolean)tradeMsg.getProperty("accepted");
            if (accepted) {
            	if (!validateTradeCurrencyOffer(trader1, currencyOffer)) {
            		accepted = false;
            		// Send down event to let the client know why it cancelled
            		EventMessageHelper.SendGeneralEvent(trader1, EventMessageHelper.INVALID_TRADE_CURRENCY, 0, "");
            	}
            }
            tradeSession.updateOffer(trader1, offer, currencyOffer, accepted);

            // if session is complete, then complete the trade
            if (tradeSession.isComplete()) {
            	tradeSessionMap.remove(trader1);
            	tradeSessionMap.remove(trader2);
            	sendTradeComplete(trader1, trader2, AgisInventoryClient.tradeSuccess);
            	sendTradeComplete(trader2, trader1, AgisInventoryClient.tradeSuccess);
            	completeTrade(tradeSession);
            	return true;
            }

            // otherwise, send trade updates to both traders
            sendTradeOfferUpdate(trader1, trader2, tradeSession);
            sendTradeOfferUpdate(trader2, trader1, tradeSession);
            return true;
        }
    }

    /**
     * Ensures the items offered in the Trade are valid. Checks there
     * are no doubles, and that the Trader does have the items.
     * @param trader
     * @param offer
     * @return
     */
    public boolean validateTradeOffer(OID trader, List<OID> offer) {
    	Set<OID> itemSet = new HashSet<OID>();

    	for (OID itemOid : offer) {
    	    // null is an empty slot in the offer
    	    if (itemOid == null) {
    	        continue;
    	    }
    	    // don't allow duplicate items in trade offer
    	    if (!itemSet.add(itemOid)) {
    	        return false;
    	    }
    	    
    	    // Verify the item is not bound to the player
    	    AgisItem item = getAgisItem(itemOid);
    	    if (item == null) {
    	    	return false;
    	    }
    	    if (!item.canBeTraded()) {
    	    	WorldManagerClient.sendObjChatMsg(trader, 2, "You cannot trade a bound item.");
    	    	return false;
    	    }
    	}

    	InventoryInfo iInfo = getInventoryInfo(trader);
		if (iInfo == null)
			return false;
        OID[] subBags = iInfo.getBags();

    	// go through all items trader has in inventory and remove from itemSet.
    	// anything left, the trader doesn't have.
    	for (OID subBagOid : subBags) {
    	    if (subBagOid != null) {
    		Bag subBag = getBag(subBagOid);
    		for (OID itemOid : subBag.getItemsList()) {
    		    itemSet.remove(itemOid);
    		}
    	    }
    	}

    	// if there are any items we didn't find, fail
    	if (!itemSet.isEmpty()) {
    	    return false;
    	}

    	return true;
    }
    
    /**
     * Ensures the items offered in the Trade are valid. Checks there
     * are no doubles, and that the Trader does have the items.
     * @param trader
     * @param offer
     * @return
     */
    public boolean validateTradeCurrencyOffer(OID trader, HashMap<String, Integer> currencyOffer) {
    	InventoryInfo iInfo = getInventoryInfo(trader);
		if (iInfo == null)
			return false;

    	// go through all currencies offered and ensure the player has enough
    	/*for (String currencyID : currencyOffer.keySet()) {
    	    if (currencyID != null && !currencyID.isEmpty()) {
    	    	int currencyAmount = iInfo.getCurrencyAmount(Integer.parseInt(currencyID), true);
    	    	if (currencyOffer.get(currencyID) > currencyAmount) {
    	    		currencyOffer.put(currencyID, currencyAmount);
    	    	}
    	    }
    	}*/
    	
    	int currencyType = -1;
        int currencyAmount = 0;
        for (String currency : currencyOffer.keySet()) {
        	int cType = Integer.parseInt(currency);
	        int amount = currencyOffer.get(currency);
	        Currency c = Agis.CurrencyManager.get(cType);
	        while (c.getCurrencyThatConvertsToThis() != null) {
	        	c = c.getCurrencyThatConvertsToThis();
	        	amount *= c.getConversionAmountReq();
	        }
	        currencyAmount += amount;
	        currencyType = c.getCurrencyID();
        }
        Log.debug("TRADE: checking trade currencies. Player is trading currency: " + currencyType + " x: " + currencyAmount);
        if (currencyType > 0 && currencyAmount > 0) {
        	// Does player have enough?
        	int playersCurrency = getMobCurrency(trader, currencyType);
        	Log.debug("TRADE: checking trade currencies. Player is trading: " + currencyAmount + " and has: " + playersCurrency);
        	if (playersCurrency < currencyAmount) {
        		return false;
        	}
        }
    	

    	return true;
    }

    /**
     * Sends down the updated list of offers to players involved in the Trade.
     * @param trader1
     * @param trader2
     * @param tradeSession
     */
    public static void sendTradeOfferUpdate(OID trader1, OID trader2, TradeSession tradeSession) {
        Boolean accepted1 = tradeSession.getAccepted(trader1);
        Boolean accepted2 = tradeSession.getAccepted(trader2);
        ArrayList<ArrayList> offer1 = sendTradeOfferUpdateHelper(trader1, tradeSession);
        ArrayList<ArrayList> offer2 = sendTradeOfferUpdateHelper(trader2, tradeSession);

        Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "ao.TRADE_OFFER_UPDATE");
        props.put("accepted1", accepted1);
        props.put("accepted2", accepted2);
        props.put("offer1", offer1);
        props.put("offer2", offer2);
        props.put("currencyOffer1", tradeSession.getCurrencyOffer(trader1));
        props.put("currencyOffer2", tradeSession.getCurrencyOffer(trader2));
        TargetedExtensionMessage msg = new TargetedExtensionMessage(AgisInventoryClient.MSG_TYPE_TRADE_OFFER_UPDATE,
                                                                    trader1, trader2, false, props);
        Engine.getAgent().sendBroadcast(msg);
    }

    /**
     * Helper function for the sendTradeOfferUpdate function. 
     * @param traderOid
     * @param tradeSession
     * @return
     */
    protected static ArrayList<ArrayList> sendTradeOfferUpdateHelper(OID traderOid, TradeSession tradeSession) {
    	ArrayList<ArrayList> offer = new ArrayList<ArrayList>();
        for (OID itemOid : tradeSession.getOffer(traderOid)) {
        	ArrayList<Object> info = new ArrayList<Object>();
	    	if ((itemOid == null)) {
                info.add(itemOid);
                info.add(-1);
                info.add(1);
                //info.add("");
                //info.add("");
	    	} else {
	    		AgisItem item = getAgisItem(itemOid);
                info.add(itemOid);
                info.add(item.getTemplateID());
                info.add(item.getStackSize());
                //info.add(item.getName());
                //info.add(item.getIcon());
	    	}
	    	offer.add(info);
        }
        return offer;
    }

    /**
     * Completes the Trade moving the items offered between the two players.
     * @param tradeSession
     */
    public void completeTrade(TradeSession tradeSession) {
        OID trader1Oid = tradeSession.getTrader1();
    	OID trader2Oid = tradeSession.getTrader2();
    	InventoryInfo iInfo1 = getInventoryInfo(trader1Oid);
    	InventoryInfo iInfo2 = getInventoryInfo(trader2Oid);
    	List<OID> offer1 = tradeSession.getOffer(trader1Oid);
    	List<OID> offer2 = tradeSession.getOffer(trader2Oid);
    
    	for (OID itemOid : offer1) {
    	    removeItem(trader1Oid, itemOid, true);
    	}
    	for (OID itemOid : offer2) {
    	    removeItem(trader2Oid, itemOid, true);
    	}
    	for (OID itemOid : offer1) {
    	    addItem(trader2Oid, iInfo2.getOid(), itemOid);
    	}
    	for (OID itemOid : offer2) {
    	    addItem(trader1Oid, iInfo1.getOid(), itemOid);
    	}
        sendInvUpdate(trader1Oid);
        sendInvUpdate(trader2Oid);
        
        // Move currencies
        HashMap<String, Integer> currencyOffer1 = tradeSession.getCurrencyOffer(trader1Oid);
        HashMap<String, Integer> currencyOffer2 = tradeSession.getCurrencyOffer(trader2Oid);
        
        // Transfer currency from trader 1 to trader 2
        int currencyType = -1;
        int currencyAmount = 0;
        for (String currency : currencyOffer1.keySet()) {
        	int cType = Integer.parseInt(currency);
	        int amount = currencyOffer1.get(currency);
	        Currency c = Agis.CurrencyManager.get(cType);
	        while (c.getCurrencyThatConvertsToThis() != null) {
	        	c = c.getCurrencyThatConvertsToThis();
	        	amount *= c.getConversionAmountReq();
	        }
	        currencyAmount += amount;
	        currencyType = c.getCurrencyID();
        }
        if (currencyType > 0 && currencyAmount > 0) {
        	alterMobCurrency(trader1Oid, currencyType, -currencyAmount);
        	alterMobCurrency(trader2Oid, currencyType, currencyAmount);
        }
        
        // Transfer currency from trader 2 to trader 1
        currencyType = -1;
        currencyAmount = 0;
        for (String currency : currencyOffer2.keySet()) {
        	int cType = Integer.parseInt(currency);
	        int amount = currencyOffer2.get(currency);
	        Currency c = Agis.CurrencyManager.get(cType);
	        while (c.getCurrencyThatConvertsToThis() != null) {
	        	c = c.getCurrencyThatConvertsToThis();
	        	amount *= c.getConversionAmountReq();
	        }
	        currencyAmount += amount;
	        currencyType = c.getCurrencyID();
        }
        if (currencyType > 0 && currencyAmount > 0) {
        	alterMobCurrency(trader2Oid, currencyType, -currencyAmount);
        	alterMobCurrency(trader1Oid, currencyType, currencyAmount);
        }
    }

    /**
     * Handles the DespawnedMessage which is sent when a WorldObject is despawned. 
     * Cancels any TradeSessions the player is involved in.
     */
    class DespawnedHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
    	    WorldManagerClient.DespawnedMessage despawnedMsg = (WorldManagerClient.DespawnedMessage)msg;
    	    OID oid = despawnedMsg.getSubject();
    	    TradeSession tradeSession = tradeSessionMap.get(oid);
    	    if (tradeSession != null) {
    	        OID trader1 = tradeSession.getTrader1();
    	        OID trader2 = tradeSession.getTrader2();
        		tradeSessionMap.remove(trader1);
        		tradeSessionMap.remove(trader2);
        		sendTradeComplete(trader1, trader2, AgisInventoryClient.tradeFailed);
        		sendTradeComplete(trader2, trader1, AgisInventoryClient.tradeFailed);
    	    }
    	    return true;
    	}
    }

    // HashMap storing all of the active TradeSessions.
    Map<OID, TradeSession> tradeSessionMap = new HashMap<OID, TradeSession>();

    /**
     * Creates an AgisItem from the Template specified.
     */
    protected SubObjData createItemSubObj(OID masterOid, Template template) {
        if (Log.loggingDebug)
            log.debug("createItemSubObj: creating item=" + template.getName()
		      + " masterOid=" + masterOid);
        AgisItem item = new AgisItem(masterOid);
        item.setName(template.getName());
        item.setTemplateID(template.getTemplateID());

        Map<String, Serializable> props = template.getSubMap(Namespace.AGISITEM);
        if (props == null) {
        	Log.warn("createItemSubObj: no props in ns " + Namespace.AGISITEM);
        	return null;
        }

        Boolean persistent = (Boolean)template.get(
            Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_PERSISTENT);
        if (persistent == null)
            persistent = false;
        item.setPersistenceFlag(persistent);

        // copy properties from template to object
        for (Map.Entry<String, Serializable> entry : props.entrySet()) {
        	String key = entry.getKey();
        	Serializable value = entry.getValue();
        	if (!key.startsWith(":")) {
        		item.setProperty(key, value);
        	}
        }
	
        // register the entity
        registerAgisItem(item);
        return new SubObjData();
    }

    /**
     * Loads the players inventory from the internal object database then sends
     * down the inventory information to their client.
     */
    protected void loadInventory(Entity e) {
    	InventoryInfo iInfo = (InventoryInfo) e;
        
        OID ownerOid = iInfo.getOid();

        boolean dirty = false;
    	for (OID subBagOid : iInfo.getBags()) {
    	    if (subBagOid != null) {
    	    	if (loadSubBag(subBagOid, iInfo))
    	    		dirty = true;
    	    }
    	}
    	
    	if (iInfo.getEquipmentItemBag() != null) {
    		if (loadSubBag(iInfo.getEquipmentItemBag(), iInfo))
	    		dirty = true;
    	}
        if (dirty)
            Engine.getPersistenceManager().setDirty(iInfo);
        
        // Add any currencies the user doesn't know about yet
        for (int currencyID : Agis.CurrencyManager.keyList()) {
        	if (!iInfo.getCurrentCurrencies().containsKey(currencyID)) {
        		iInfo.addCurrency(currencyID, 0);
        	}
        }
        ExtendedCombatMessages.sendCurrencies(ownerOid, iInfo.getCurrencies(iInfo.getCurrentCategory()));
    	sendInvUpdate(ownerOid);
        
        // Send the Equip message again for all equipped items to ensure the latest display and stats are set
    	OID subBagOid = iInfo.getEquipmentItemBag();
    	Bag subBag = getBag(subBagOid); 
    	if (subBag != null) {
    		for (OID itemOid : subBag.getItemsList()) {
    			if (itemOid != null) {
    				AgisItem item = getAgisItem(itemOid);
    				String displayVal = (String) item.getProperty("displayVal");
    		        EquipHelper.updateDisplay(ownerOid, displayVal, item.getPrimarySlot());
    		        AgisInventoryClient.itemEquipStatusChanged(ownerOid, item, true, item.getPrimarySlot().toString());
    			}
    		}
    	}
    	
    }

    /**
     * Loads a Bag that resides inside another Bag from the internal object database.
     * @param subBagOid
     * @param rootBag
     * @return
     */
    protected boolean loadSubBag(OID subBagOid, Entity rootBag) {
    	Bag subBag = (Bag) Engine.getDatabase().loadEntity(subBagOid,
                    Namespace.BAG);
    	registerBag(subBag);
        boolean dirty = false;
    	for (OID itemOid : subBag.getItemsList()) {
    	    if (itemOid != null) {
    	    	if (ObjectManagerClient.loadObject(itemOid) == null) {
    	    		// If we can't load the item, then delete reference
    	    		// to it from the bag.
                    Log.warn("loadSubBag: item "+itemOid+" does not exist, removing from bag "+subBagOid);
                    boolean rv = subBag.removeItem(itemOid);
                    if (rv)
                        dirty = true;
                }
    	    }
    	}
        return dirty;
    }
    
    protected void loadMailItems(OID oid) {
    	// Load mail items
    	InventoryInfo iInfo = getInventoryInfo(oid);
    	for (Mail m : iInfo.getMail()) {
    		if (m.getItems() != null) {
    			for (int i = 0; i < m.getItems().size(); i++) {
    				if (m.getItems().get(i) != null) {
    					ObjectManagerClient.loadObject(m.getItems().get(i));
    				}
    			}
    		}
    	}
    }

    /**
     * Unloads the players inventory object. If the root bag is dirty it is saved
     * before being unloaded.
     */
    protected void unloadInventory(Entity e) {
    	InventoryInfo iInfo = (InventoryInfo) e;
        OID ownerOid = iInfo.getOid();

        if (e.isDeleted())
            return;

        // If the root bag is dirty, then save immediately.  We can't
        // wait for the PersistenceManager because the sub bag entities
        // will be unregistered, so the save wouldn't do anything.
        if (Engine.getPersistenceManager().isDirty(e)) {
            Engine.getPersistenceManager().clearDirty(e);
            saveInventory(e,null);
        }

        Log.debug("unloadInventory: oid="+e.getOid()+" owner="+ownerOid);

        for (OID subBagOid : iInfo.getBags()) {
            if (subBagOid != null) {
                Bag subBag = getBag(subBagOid);
                if (subBag == null)
                    continue;
                for (OID itemOid : subBag.getItemsList()) {
                    if (itemOid == null)
                        continue;
                    if (Log.loggingDebug)
                        Log.debug("unloadInventory: bag oid="+e.getOid()+
                                " subbag="+subBagOid+ " item="+itemOid);
                    ObjectManagerClient.unloadObject(itemOid);
                }
                EntityManager.removeEntityByNamespace(subBagOid, Namespace.BAG);
            }
        }
        
        if (iInfo.getEquipmentItemBag() != null) {
            Bag subBag = getBag(iInfo.getEquipmentItemBag());
            if (subBag == null)
                return;
            for (OID itemOid : subBag.getItemsList()) {
                if (itemOid == null)
                    continue;
                if (Log.loggingDebug)
                    Log.debug("unloadInventory: bag oid="+e.getOid()+
                            " subbag="+iInfo.getEquipmentItemBag()+ " item="+itemOid);
                ObjectManagerClient.unloadObject(itemOid);
            }
            EntityManager.removeEntityByNamespace(iInfo.getEquipmentItemBag(), Namespace.BAG);
        }
    }

    /**
     * Deletes a InventoryInfo object from the internal object database.
     */
    protected void deleteInventory(Entity e) {
    	InventoryInfo iInfo = (InventoryInfo) e;
        OID ownerOid = iInfo.getOid();

        Log.debug("deleteInventory: oid="+e.getOid()+" owner="+ownerOid);

        for (OID subBagOid : iInfo.getBags()) {
            if (subBagOid != null) {
                Bag subBag = getBag(subBagOid);
                if (subBag == null)
                    continue;
                for (OID itemOid : subBag.getItemsList()) {
                    if (itemOid == null)
                        continue;
                    if (Log.loggingDebug)
                        Log.debug("deleteInventory: bag oid="+e.getOid()+
                                " subbag="+subBagOid+ " item="+itemOid);
                    ObjectManagerClient.deleteObject(itemOid);
                }
                subBag.setDeleted();
                EntityManager.removeEntityByNamespace(subBagOid, Namespace.BAG);
                Engine.getDatabase().deleteObjectData(subBagOid);
            }
        }
        
        if (iInfo.getEquipmentItemBag() != null) {
            Bag subBag = getBag(iInfo.getEquipmentItemBag());
            if (subBag == null)
                return;
            for (OID itemOid : subBag.getItemsList()) {
                if (itemOid == null)
                    continue;
                if (Log.loggingDebug)
                    Log.debug("deleteInventory: bag oid="+e.getOid()+
                            " subbag="+iInfo.getEquipmentItemBag()+ " item="+itemOid);
                ObjectManagerClient.deleteObject(itemOid);
            }
            subBag.setDeleted();
            EntityManager.removeEntityByNamespace(iInfo.getEquipmentItemBag(), Namespace.BAG);
            Engine.getDatabase().deleteObjectData(iInfo.getEquipmentItemBag());
        }
    }


    /**
     * Does nothing.
     */
    protected void loadItem(Entity e) {
    }

    /**
     * DOes nothing.
     */
    protected void unloadItem(Entity e) {
    }

    /**
     * Deletes an AgisItem from the internal object database.
     */
    protected void deleteItem(Entity item) {
        if (Log.loggingDebug)
            Log.debug("deleteItem: oid="+item.getOid());

        OID subBagOid = (OID)item.getProperty(INVENTORY_PROP_BACKREF_KEY);
        if (subBagOid != null) {
            if (removeItemFromBagHelper(subBagOid, item)) {
                Bag subBag = getBag(subBagOid);
                OID ownerOid = (OID)subBag.getProperty(INVENTORY_PROP_BACKREF_KEY);
                AgisItem aItem = (AgisItem)item;
                aItem.unacquired(ownerOid);
                InventoryInfo iInfo = getInventoryInfo(ownerOid);
                if (iInfo != null) {
                    Engine.getPersistenceManager().setDirty(iInfo);
                    sendInvUpdate(ownerOid);
                }
            }
        }

        // EnginePlugin DeleteSubObject handler has already removed
        // entity, so we just need to delete from data base.

        Engine.getDatabase().deleteObjectData(item.getOid());
    }

    /**
     * Saves an InventoryInfo object into the internal object database.
     */
    protected void saveInventory(Entity e, Namespace namespace) {
    	InventoryInfo iInfo = (InventoryInfo) e;
        if (Log.loggingDebug)
            log.debug("saveInventory: rootBag=" + iInfo.getOid());

    	for (OID subBagOid : iInfo.getBags()) {
    	    if (subBagOid != null) {
    	    	Bag subBag = getBag(subBagOid);
                if (subBag == null) {
                    log.error("saveInventory: subBag not found oid="+subBagOid);
                    continue;
                }
                if (Log.loggingDebug)
                    log.debug("saveInventory: subBag oid=" + subBag.getOid());
                Engine.getDatabase().saveObject(subBag, Namespace.BAG);

                for (OID itemOid : subBag.getItemsList()) {
                	if (itemOid != null) {
                        if (Log.loggingDebug)
                            log.debug("saveInventory: saving itemOid=" + itemOid);
                        ObjectManagerClient.saveObject(itemOid);
                        if (Log.loggingDebug)
                            log.debug("saveInventory: done saving itemOid=" + itemOid);
                	}
                }
    	    }
    	}
    	
    	// Save Equipped Items Bag
    	Bag subBag = getBag(iInfo.getEquipmentItemBag());
        if (subBag != null) {
        	if (Log.loggingDebug)
                log.debug("saveInventory: subBag oid=" + subBag.getOid());
            Engine.getDatabase().saveObject(subBag, Namespace.BAG);

            for (OID itemOid : subBag.getItemsList()) {
            	if (itemOid != null) {
                    if (Log.loggingDebug)
                        log.debug("saveInventory: saving itemOid=" + itemOid);
                    ObjectManagerClient.saveObject(itemOid);
                    if (Log.loggingDebug)
                        log.debug("saveInventory: done saving itemOid=" + itemOid);
            	}
            }
        } else {
            log.error("saveInventory: equippedItemsBag not found oid="+iInfo.getEquipmentItemBag());
        }
        
    	for (Mail m : iInfo.getMail()) {
    		if (m.getItems() != null) {
    			for (int i = 0; i < m.getItems().size(); i++) {
    				if (m.getItems().get(i) != null) {
    					ObjectManagerClient.saveObject(m.getItems().get(i));
    				}
    			}
    		}
    	}
    	// Save the actual InventoryInfo itself to make sure currencies are saved
    	Engine.getDatabase().saveObject(e, Namespace.BAG);
    }

    /**
     * Does nothing.
     */
    protected void saveItem(Entity e, Namespace namespace) {
    }
    
    /**
     * Places an item into the specified container and slot.
     * @param mobOid
     * @param rootBagOid
     * @param itemOid
     * @param containerNum
     * @param slotNum
     * @return
     */
    private boolean placeItem(OID mobOid, OID rootBagOid, OID itemOid, int containerNum, int slotNum, boolean newItem) {
    	InventoryInfo iInfo = getInventoryInfo(rootBagOid);
    	OID[] subBagOids = iInfo.getBags();
    	OID subBagOid = subBagOids[containerNum];
    	Bag subBag = getBag(subBagOid);
        if (subBag == null) {
            Log.warn("placeItem: did not find sub bag: " + subBagOid + " for bagoid=" + subBagOid);
            return false;
        }
        
        // get item
        AgisItem item = getAgisItem(itemOid);
        if (item == null) {
        	return false;
        }

        // add backref to item
        /*if (item.getProperty(INVENTORY_PROP_BACKREF_KEY) != null) {
            Log.warn("placeItem: item is already in a container, itemOid="
                    + item.getOid());
            return false;
        }*/
        // add item to bag
        boolean rv = subBag.putItem(slotNum, itemOid);
        if (Log.loggingDebug)
            log.debug("placeItem: adding to bag, rv=" + rv);

        if (rv) {
            item.setProperty(INVENTORY_PROP_BACKREF_KEY, subBagOid);
            item.acquired(mobOid);
        }

        // mark dirty
        Engine.getPersistenceManager().setDirty(item);
        Engine.getPersistenceManager().setDirty(iInfo);
        sendBagInvUpdate(mobOid);
        return rv;
    }
    
    /**
     * Adds the item to the player/mob specified. Existing items will be checked for to see if an item can be added
     * to an existing stack before a new item is made.
     * TODO: Merge some of this code into the addItem function, and call that from here (once the item has been generated).
     * @param mobOid: the object identifier of the player/mob to add the item to
     * @param itemID: the id of the template of the item to be added
     * @param count: how many of the item should be added
     * @return: a boolean indicating whether or not the item was successfully added.
     */
    private boolean addItemFromTemplate(OID mobOid, int itemID, int count, HashMap<String, Serializable> itemProps) {
    	lock.lock();
        try {
            // get bag
        	InventoryInfo iInfo = getInventoryInfo(mobOid);
            if (Log.loggingDebug)
                log.debug("addItem: found bag object: " + iInfo);
            
            Template tmpl = ObjectManagerClient.getTemplate(itemID, ObjectManagerPlugin.ITEM_TEMPLATE);
            String itemName = tmpl.getName();
            int stackLimit = (Integer) tmpl.get(InventoryClient.ITEM_NAMESPACE, "stackLimit");

            // Check if there are any existing stacks to add to
            ArrayList<OID> existingOids = findItemStacks(mobOid, itemID);
            if (existingOids.size() > 0) {
            	Log.debug("ITEM: user already has item " + itemID + ", see if we can add it to one of the stacks.");
            	for (OID existingOid: existingOids) {
            		AgisItem tempItem = getAgisItem(existingOid);
            		Log.debug("ITEM: stackSize " + tempItem.getStackSize() + ", stackLimit: " + tempItem.getStackLimit() + "for item: " + existingOid);
            		if((tempItem.getStackSize() + count) <= tempItem.getStackLimit()) {
            			// If the whole amonut fits into an existing stack, this is nice and easy
            			Log.debug("ITEM: increasing stack size for item: " + existingOid);
            			tempItem.alterStackSize(mobOid, count);
            			Engine.getPersistenceManager().setDirty(tempItem);
            			Engine.getPersistenceManager().setDirty(iInfo);
            			return true;
            		} else {
            			// Otherwise we add what we can to an existing stack and move onto the creation of new items
            			Log.debug("ITEM: increasing stack size to max for item: " + existingOid);
            			int stackSpace = tempItem.getStackLimit() - tempItem.getStackSize();
            			tempItem.alterStackSize(mobOid, stackSpace);
            			Engine.getPersistenceManager().setDirty(tempItem);
            			Engine.getPersistenceManager().setDirty(iInfo);
            			count = count - stackSpace;
            		}
            	}
            }
            // check each subbag and see if it can be added there
            OID[] subBags = iInfo.getBags();
            int stacksNeeded = ((count-1) / stackLimit) + 1;
            Log.debug("ITEM: there is no stacks to add to for item: " + itemID + " so going to create a new item. Stacks needed: " + stacksNeeded);
            for (int i = 0; i < stacksNeeded; i++) {
            	OID itemOid = generateItem(itemID, itemName);
    	        if (itemProps != null) {
    	        	AgisItem item = getAgisItem(itemOid);
    	        	for (String itemProp: itemProps.keySet()) {
    	        		item.setProperty(itemProp, itemProps.get(itemProp));
    	        	}
    	        }
	        	AgisItem item = getAgisItem(itemOid);
		        if (count > 1) {
		        	//int stackLimit = item.getStackLimit();
		        	if (count > stackLimit) {
		        		int stackSpace = item.getStackLimit() - item.getStackSize();
		        		item.alterStackSize(mobOid, stackSpace);
		        		count = count - stackLimit;
		        	} else {
		        		item.alterStackSize(mobOid, count-1);
		        		count = 0;
		        	}
		        } else {
		        	count = 0;
		        }
		        boolean stackAdded = false;
		        for (int pos = 0; pos < subBags.length; pos++) {
		        	OID subBag = subBags[pos];
	                if (addItemHelper(mobOid, subBag, -1, item)) {
			            Engine.getPersistenceManager().setDirty(iInfo);
			            stackAdded = true;
			            break;
	                }
	            }
		        if (stackAdded == false) {
		        	Log.error("ITEM: space for item: " + itemName + " was not found.");
		        	return false;
		        }
	        }
            if (count > 0)
            	return false;

            //ExtendedCombatMessages.sendErrorMessage(oid, "There is no space in your bags.");
            //return false;
        } finally {
            lock.unlock();
        }
    	return true;
    }

    /**
     * adds the item to the container item must not be in a container already
     * sets the item's containedBy backlink to the container
     */
    protected boolean addItem(OID mobOid, OID rootBagOid, OID itemOid) {
        lock.lock();
        try {
            // get bag
        	InventoryInfo iInfo = getInventoryInfo(mobOid);

            if (Log.loggingDebug)
                log.debug("addItem: found bag object: " + iInfo);

            // get item
            AgisItem item = getAgisItem(itemOid);
            /*if (item == null) {
            	item = getBag(itemOid);
            }*/
            if (item == null) {
                Log.warn("addItem: item is null: oid=" + itemOid);
                return false;
            }
            if (Log.loggingDebug)
                log.debug("addItem: found item: " + item);
            
            int stackSizeToAdd = item.getStackSize();
            
            // check to see if the player already has the item
            ArrayList<OID> existingOids = findItemStacks(mobOid, item.getTemplateID());
            if (existingOids.size() > 0) {
            	Log.debug("ITEM: user already has item " + item.getName() + ", see if we can add it to one of the stacks.");
            	for (OID existingOid: existingOids) {
            		AgisItem tempItem = getAgisItem(existingOid);
            		Log.debug("ITEM: stackSize " + tempItem.getStackSize() + ", stackLimit: " + tempItem.getStackLimit() + "for item: " + existingOid);
            		if((tempItem.getStackSize() + stackSizeToAdd) <= tempItem.getStackLimit()) {
            			Log.debug("ITEM: increasing stack size for item: " + existingOid);
            			tempItem.alterStackSize(mobOid, stackSizeToAdd);
            			Engine.getPersistenceManager().setDirty(tempItem);
            			Engine.getPersistenceManager().setDirty(iInfo);
            			return true;
            		}
            	}
            }

            // check each subbag and see if it can be added there
            Log.debug("ITEM: there is no stacks to add to for item: " + item.getName() + " so going to create a new item");
            OID[] subBags = iInfo.getBags();
            Log.debug("ITEM: bag count: " + subBags.length);
            for (int pos = 0; pos < subBags.length; pos++) {
            	OID subBag = subBags[pos];
                if (addItemHelper(mobOid, subBag, -1, item)) {
		            Engine.getPersistenceManager().setDirty(iInfo);
                    return true;
                }
            }
            ExtendedCombatMessages.sendErrorMessage(mobOid, "There is no space in your bags.");
            return false;
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Helper function for the addItem function. This is where special on-acquire item actions
     * should take place as it is where the item is actually added to the bag.
     * @param subBagOid
     * @param slotNum
     * @param item
     * @return
     */
    protected boolean addItemHelper(OID ownerOid, OID subBagOid, int slotNum, AgisItem item) {
    	// Run the acquire function on the item and if it returns true, destroy the item
    	if (item.acquired(ownerOid)) {
    		return true;
    	}
    	
        // get the bag object
        Bag subBag = getBag(subBagOid);
        if (subBag == null) {
            Log.warn("addItemHelper: did not find sub bag: " + subBagOid + " for bagoid=" + subBagOid);
            return false;
        }

        // add backref to item
        if (item.getProperty(INVENTORY_PROP_BACKREF_KEY) != null) {
            Log.warn("addItem: item is already in a container, itemOid="
                    + item.getOid());
            return false;
        }
        // add item to bag
        boolean rv = false;
        if (slotNum > 0) {
        	rv = subBag.putItem(slotNum, item.getOid());
        } else {
        	rv = subBag.addItem(item.getOid());
        }
        if (Log.loggingDebug)
            log.debug("addItem: adding to bag=" + subBag + " with slots=" + subBag.getNumSlots() + ", rv=" + rv);

        if (rv) {
            item.setProperty(INVENTORY_PROP_BACKREF_KEY, subBagOid);
        }

        // mark dirty
        Engine.getPersistenceManager().setDirty(item);
        return rv;
    }

    /**
     * Removes an item from a Bag.
     */
    protected boolean removeItemFromBag(OID rootBagOid, OID itemOid) {
        lock.lock();
        try {
            // get root bag
        	InventoryInfo iInfo = getInventoryInfo(rootBagOid);
            if (Log.loggingDebug)
                log.debug("removeItemFromBag: found root bag object: " + iInfo);

            // get item
            AgisItem item = getAgisItem(itemOid);
            if (item == null) {
                Log.warn("removeItemFromBag: item is null: oid=" + itemOid);
                return false;
            }
            if (Log.loggingDebug)
                log.debug("removeItemFromBag: found item: " + item);

            // check each subbag to find its container
            OID[] subBags = iInfo.getBags();
            for (int pos = 0; pos < subBags.length; pos++) {
            	OID subBag = subBags[pos];
                if (removeItemFromBagHelper(subBag, item)) {
                	Engine.getPersistenceManager().setDirty(iInfo);
                    return true;
                }
            }
            return false;
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Helper function for the removeItemFromBag function.
     * @param subBagOid
     * @param item
     * @return
     */
    protected boolean removeItemFromBagHelper(OID subBagOid, Entity item) {
        // get the bag object
        Bag subBag = getBag(subBagOid);
        if (subBag == null) {
            Log.warn("removeItemFromBagHelper: did not find sub bag: " + subBagOid);
            return false;
        }

        // does bag contain the item?
        Integer slotNum = subBag.findItem(item.getOid());
        if (slotNum == null) {
            if (Log.loggingDebug)
                log.debug("removeItemFromBagHelper: item not in bag itemOid=" + item.getOid() + " bagOid="+subBagOid);
            return false;
        }
        
        // found the item
        if (Log.loggingDebug)
            log.debug("removeItemFromBagHelper: found - slot=" + slotNum + ", itemOid=" + item.getOid());
        
        // remove item from bag - we separate the logic here from finding the item
        // because perhaps there was some other reason why the remove failed
        boolean rv = subBag.removeItem(item.getOid());
        if (rv == false) {
            if (Log.loggingDebug)
                log.debug("removeItemFromBagHelper: remove item failed");
            return false;
        }
        
        // remove the back reference
        item.setProperty(INVENTORY_PROP_BACKREF_KEY, null);

        // mark dirty
        Engine.getPersistenceManager().setDirty(item);
        if (Log.loggingDebug)
            log.debug("removeItemFromBagHelper: remove from bag, rv=" + rv);
        return rv;
    }
    
    /**
     * Called when an object is updated, sending information about the inventory
     * to the client.
     */
    public void updateObject(OID mobOid, OID target) {
        // This is a player-type mob if it's asking about itself, i.e., subject == target
        if (!mobOid.equals(target)) {
            if (Log.loggingDebug)
                log.debug("updateObject: obj is not a player, ignoring: " + mobOid);
            return;
        }
        if (Log.loggingDebug)
            log.debug("updateObject: obj is a player: " + mobOid);
        
        // send out inv update
        InventoryInfo iInfo = getInventoryInfo(mobOid);
        if (iInfo != null){
            Log.debug("AgisInventoryPlugin - sending inventory update");            
            sendInvUpdate(mobOid);
        }
        else 
            log.debug("updateObject: could not find entity in " + Namespace.BAG + " for mobOid " + mobOid);
        
        if (VendorPlugin.useVirtualCurrency()) {
            Log.debug("AgisInventoryPlugin - sending token balance");
            VendorClient.sendBalanceUpdate(mobOid, BillingClient
                    .getTokenBalance(mobOid));
        }
        return;
    }
    
    /**
     * Handles the GenerateLootMessage. Generates the loot for the specified mob
     * based on the loot tables assigned to the mob.
     * The GenerateLootMessage is usually sent when a mob dies.
     * @author Andrew
     *
     */
    class GenerateLootHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.generateLootMessage lootMsg = (AgisInventoryClient.generateLootMessage)msg;
	        OID mobOid = lootMsg.getSubject();
	        if (lootMsg.getIsPlayer()) {
	        	generatePlayerLoot(mobOid, lootMsg.getLoc());
	        	return true;
	        }
	        Log.debug("LOOT: Generating Loot for mob: " + mobOid);
	        // Calculate loot
	        HashMap<Integer, Integer> itemsToAdd = new HashMap<Integer, Integer>();
    		LinkedList<OID> itemsToLoot = new LinkedList<OID>();
        	HashMap<Integer, Integer> mobLootTables = (HashMap) EnginePlugin.getObjectProperty(mobOid, InventoryClient.NAMESPACE, "lootTables");
        	for (int lootTable: mobLootTables.keySet()) {
        		int tableChance = mobLootTables.get(lootTable);
        		int roll = AgisInventoryPlugin.random.nextInt(100);
        		Log.debug("LOOT: roll for loot table: " + lootTable + " is: " + roll + " with tablechance: " + tableChance);
        		if (roll < tableChance) {
        			Log.debug("LOOT: lootManager has: " + Agis.LootTableManager.getMap());
        			LootTable lt = Agis.LootTableManager.get(lootTable);
        			int itemNum = lt.getRandomItemNum();
        			Log.debug("LOOT: randomised item number: " + itemNum);
        			int templateID = lt.getItems().get(itemNum);
        			if (templateID > -1) {
        				int count = lt.getItemCounts().get(itemNum);
        				if (itemsToAdd.containsKey(templateID)) {
        					itemsToAdd.put(templateID, itemsToAdd.get(templateID) + count);
        				} else {
        					itemsToAdd.put(templateID, count);
        				}
        			}
        		}
        	}
        	
        	for (int templateID: itemsToAdd.keySet()) {
        		Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.ITEM_TEMPLATE);
				OID itemOid = generateItem(templateID, tmpl.getName());
			    boolean itemAdded = InventoryClient.addItem(mobOid, mobOid, mobOid, itemOid);
			    for (int i = 1; i < itemsToAdd.get(templateID); i++) {
			    	OID iOid = generateItem(templateID, tmpl.getName());
			    	boolean itemAdded2 = InventoryClient.addItem(mobOid, mobOid, mobOid, iOid);
			    	Log.debug("LOOT: adding item2: " + tmpl.getName() + " to mobs loot with result: " + itemAdded2);
			    }
			    Log.debug("LOOT: adding item: " + tmpl.getName() + " to mobs loot with result: " + itemAdded);
			    if (itemAdded)
			        itemsToLoot.add(itemOid);
        	}
        	
        	EnginePlugin.setObjectProperty(mobOid, InventoryClient.NAMESPACE, "loot", itemsToLoot);
        	Log.debug("Mob: " + mobOid + " now has loot: " + itemsToLoot);
    		return true;
        }
    }
    
    void generatePlayerLoot(OID playerOid, Point loc) {
    	Log.debug("LOOT: Generating Loot for player: " + playerOid);
    	if (PLAYER_CORPSE_LOOT_DURATION < 1) {
    		return;
    	}
    	Log.debug("LOOT: removing players equipped items? " + PLAYER_CORPSE_DROPS_EQUIPMENT);
    	// Remove players items and give them to the mob
    	LinkedList<OID> loot = new LinkedList<OID>(removeAllItems(playerOid));
    	
    	if (PLAYER_CORPSE_DROPS_EQUIPMENT) {
    		// Also remove all equipped gear
    		InventoryInfo iInfo = getInventoryInfo(playerOid);
    		Bag subBag = getBag(iInfo.getEquipmentItemBag());
    		EquipMap equipMap = getEquipMap(playerOid);
            if (subBag != null) {
                for (OID itemOid : subBag.getItemsList()) {
                	if (itemOid != null) {
                		AgisItem item = getAgisItem(itemOid);
                    	AgisEquipSlot slot = equipMap.getSlot(item.getMasterOid());
                		removeEquippedItem(playerOid, itemOid, slot);
                		equipMap.remove(slot);
                		AgisInventoryClient.itemEquipStatusChanged(playerOid, item, false, slot.toString());
        	            EquipHelper.updateDisplay(playerOid, null, slot);
                		loot.add(itemOid);
                	}
                }
            }
    	}
    	if (loot == null || loot.size() == 0)
    		return;
    	BehaviorTemplate bTmpl = new BehaviorTemplate();
    	bTmpl.setIsPlayerCorpse(true);
    	bTmpl.setOtherUse(playerOid.toString());
    	SpawnData sd = new SpawnData();
    	sd.setProperty("id", (int)playerOid.toLong());
		sd.setName(playerOid.toString());
		sd.setTemplateID(PLAYER_CORPSE_MOB_TEMPLATE);
		WorldManagerClient.ObjectInfo objInfo = WorldManagerClient.getObjectInfo(playerOid);
		sd.setInstanceOid(objInfo.instanceOid);
		sd.setLoc(loc);
		sd.setOrientation(objInfo.orient);
		sd.setNumSpawns(1);
		sd.setSpawnRadius(0);
		sd.setRespawnTime(-1);
		sd.setCorpseDespawnTime(0);
		sd.setProperty(AgisMobPlugin.BEHAVIOR_TMPL_PROP, bTmpl);
		sd.setProperty("loot", loot);
		sd.setProperty("corpseDuration", PLAYER_CORPSE_LOOT_DURATION);
		sd.setProperty("safeDuration", PLAYER_CORPSE_SAFE_LOOT_DURATION);
		AgisMobClient.spawnMob(sd);
		sendInvUpdate(playerOid);
    }
    
    /**
     * Handles the GetLootListMessage. Calls the sendLootList function and 
     * sends down a CoordinatedEffect to play a looting animation.
     */
    class GetLootListHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.getLootListMessage getMsg = (AgisInventoryClient.getLootListMessage)msg;
	        OID oid = getMsg.getSubject();
	        OID mobOid = (OID) getMsg.getProperty("mobOid");
	        
	        // Death check
	    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(oid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
	    	if (dead) {
	    		ExtendedCombatMessages.sendErrorMessage(oid, "You cannot peform that action when dead");
	    		return true;
	    	}
	    	
	        sendLootList(oid, mobOid);
	        // TODO: Replace this Coord Effect Code with just a name.
	        CoordinatedEffect cE = new CoordinatedEffect("LootEffect");
	    	cE.sendSourceOid(true);
	    	cE.sendTargetOid(true);
	    	cE.invoke(oid, oid);
	        return true;
	    }
    }
    
    /**
     * Sends down the list of loot from the specified mob to the requesting player.
     * @param oid
     * @param mobOid
     */
    public void sendLootList(OID oid, OID mobOid) {
    	LinkedList<OID> itemOids = (LinkedList) EnginePlugin.getObjectProperty(mobOid, InventoryClient.NAMESPACE, "loot");

        Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "LootList");
		props.put("lootTarget", mobOid);
    	Log.debug("LOOT: list to send down to client: " + itemOids);
		int numItems = 0;
		if (itemOids == null) {
			props.put("numItems", numItems);
	    	TargetedExtensionMessage TEmsg = new TargetedExtensionMessage(
					WorldManagerClient.MSG_TYPE_EXTENSION, oid, 
					oid, false, props);
			Engine.getAgent().sendBroadcast(TEmsg);
			return;
		}
		
		List<Integer> questItemReqs = QuestClient.getQuestItemReqs(oid);
        for (int itemPos = 0; itemPos < itemOids.size(); itemPos++) {
            boolean itemExists = true;
            // get the item
            OID itemOid = itemOids.get(itemPos);
            if (oid == null) {
                itemExists = false;
            }
            AgisItem item = getAgisItem(itemOid);
            if (item == null) {
                Log.warn("sendInvUpdateHelper: item is null, oid=" + itemOid);
                itemExists = false;
            }
            // Check to see if the player needs an item that is a quest item
            boolean itemNeeded = true;
    	    if(item.getItemType().equals("Quest")){
    	    	itemNeeded = false;
    	        Log.debug("ANDREW: found quest item " + item.getName() + ". does the player need it?");
    	        for (int i = 0; i < questItemReqs.size(); i++) {
    	        	Log.debug("ANDREW: checking questItem req: " + questItemReqs.get(i) + " against " + item.getTemplateID());
    	            if (questItemReqs.get(i) == item.getTemplateID()) {
    	            	itemNeeded = true;
    	            	Log.debug("ANDREW: found quest item - it was needed");
    	            }
    	        }
    	        
    	    }
            
            if (itemExists && itemOids.contains(item.getOid()) && itemNeeded) {
                if (Log.loggingDebug)
                    log.debug("sendInvUpdateHelper: adding bagPos=" + itemPos +
                              ", itemOid=" + oid + ", itemName=" + item.getName() + ",icon=" + item.getIcon());
                props.put("item_" + numItems + "Name", item.getName());
                props.put("item_" + numItems + "BaseName", item.getProperty("baseName"));
                //props.put("item_" + numItems + "Description", description);
                //props.put("item_" + numItems + "Icon", item.getIcon());
                props.put("item_" + numItems + "Id", item.getOid());
                props.put("item_" + numItems + "Count", item.getStackSize());
                props.put("item_" + numItems + "SlotNum", itemPos);
                if (item.getProperty("energyCost") != null) {
                    props.put("item_" + numItems + "EnergyCost", item.getProperty("energyCost"));
                } else {
                    props.put("item_" + numItems + "EnergyCost", 0);
                }
                if (item.getProperty("resistanceStats") != null) {
                    int numResist = 0;
                    HashMap<String, Integer> resistances = (HashMap) item.getProperty("resistanceStats");
                    for (String resistance: resistances.keySet()) {
                    	props.put("item_" + numItems + "Resist_" + numResist + "Name", resistance);
                    	props.put("item_" + numItems + "Resist_" + numResist + "Value", resistances.get(resistance));
                    	numResist++;
                    }
                    props.put("item_" + numItems + "NumResistances", numResist);
                } else {
                    props.put("item_" + numItems + "NumResistances", 0);
                }
                if (item.getProperty("bonusStats") != null) {
                    int numStats = 0;
                    HashMap<String, Integer> stats = (HashMap) item.getProperty("bonusStats");
                    for (String statName: stats.keySet()) {
                    	props.put("item_" + numItems + "Stat_" + numStats + "Name", statName);
                    	props.put("item_" + numItems + "Stat_" + numStats + "Value", stats.get(statName));
                    	numStats++;
                    }
                    props.put("item_" + numItems + "NumStats", numStats);
                } else {
                    props.put("item_" + numItems + "NumStats", 0);
                }
                // If it is a weapon, add damage/speed stats
                if (item.getItemType().equals("Weapon")) {
                    props.put("item_" + numItems + "Speed", item.getProperty("speed"));
                    props.put("item_" + numItems + "DamageType", item.getProperty("attackType"));
                    props.put("item_" + numItems + "DamageValue", item.getProperty("damage"));
                }
                numItems++;
            }
        }
        if (numItems == 0) {
        	/*TargetedPropertyMessage propMsg = new TargetedPropertyMessage(oid, mobOid);
            propMsg.setProperty("lootable", false);
            Engine.getAgent().sendBroadcast(propMsg);*/
            
            PropertyMessage propMsg2 = new PropertyMessage(mobOid, mobOid);
            propMsg2.setProperty("lootable", false);
            Engine.getAgent().sendBroadcast(propMsg2);
            Log.debug("LOOT: sending lootable = false");
            //WorldManagerClient.sendObjChatMsg(oid, 2, "There is nothing to loot");
        }
    	
        props.put("numItems", numItems);
    	TargetedExtensionMessage TEmsg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, oid, 
				oid, false, props);
		Engine.getAgent().sendBroadcast(TEmsg);
    }
    
    /**
     * Handles the LootAllMessage. Takes all lootasble items from the dead mob and places them into the looters bag.
     * @author Andrew
     */
    class LootAllHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.lootAllMessage lootMsg = (AgisInventoryClient.lootAllMessage)msg;
        	OID looterOid = lootMsg.getSubject();
        	OID mobOid = (OID) lootMsg.getProperty("mobOid");
        	
        	// Death check
	    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(looterOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
	    	if (dead) {
	    		ExtendedCombatMessages.sendErrorMessage(looterOid, "You cannot peform that action when dead");
	    		return true;
	    	}
	    	
        	OID accountId = (OID) EnginePlugin.getObjectProperty(looterOid, WorldManagerClient.NAMESPACE, WorldManagerClient.ACCOUNT_PROPERTY);
	        LinkedList<OID> loot = (LinkedList) EnginePlugin.getObjectProperty(mobOid, InventoryClient.NAMESPACE, "loot");
	        LinkedList<OID> leftOverLoot = new LinkedList<OID>();
	        for (int i = 0; i < loot.size(); i++) {
	        	OID itemOid = loot.get(i);
	        	boolean rv = removeItemFromBag(mobOid, itemOid);
	            log.debug("LOOT: removed oid=" + itemOid + ", rv=" + rv);
	            if (!rv) {
	            	//leftOverLoot.add(itemOid);
	            	continue;
	            }

	            AgisItem item = getAgisItem(itemOid);
	            if (item != null) {
	                ObjectManagerClient.setPersistenceFlag(itemOid,true);
	            } else {
	            	leftOverLoot.add(itemOid);
	            	continue;
	            }
	            HashMap<String, Serializable> logData = new HashMap<String, Serializable>();
		        logData.put("item", "Item attempt to loot : " + item.getName()+ " : OID " + itemOid + " : " );
		        logData.put("playerOid", looterOid);
		        
	            DataLoggerClient.logData("ITEM_LOOTED_EVENT", looterOid, mobOid, accountId, logData);

	            // add item to the looter's root bag
	            rv = addItem(looterOid, looterOid, itemOid);
	            log.debug("LOOT: addItem to looter, oid=" + itemOid + ", rv=" + rv);
	            if (!rv) {
	            	DataLoggerClient.logData("ITEM_LOOTED_FAILED_EVENT", looterOid, itemOid, accountId, logData);	
	            	addItem(mobOid, mobOid, itemOid);
	            	leftOverLoot.add(itemOid);
	            	continue;
	            }
	            
	        	String lootedItem = item.getName();
	        	if (lootedItem != null) {
	        		EventMessageHelper.SendInventoryEvent(looterOid, EventMessageHelper.ITEM_LOOTED, item.getTemplateID(), item.getStackSize(), null);
	        	}
	        	//WorldManagerClient.sendObjChatMsg(looterOid, 2, "You have looted: " + lootedItem);
	        }
	        EnginePlugin.setObjectProperty(mobOid, InventoryClient.NAMESPACE, "loot", leftOverLoot);
	        sendBagInvUpdate(looterOid);
    		sendLootList(looterOid, mobOid);
	        return true;
    	}
    }
    
    /**
     * used to loot objects from a mob, or some other top level container object, such
     * as a chest
     * @param looterOid player/mob that is looting
     * @param mobOid where you are looting from
     * @return success or failure
     */
    protected boolean lootAll(OID looterOid, OID mobOid) {
        log.debug("lootAll: looterOid=" + looterOid + ", mobOid=" + mobOid);
        
        Long rootLooterBagOid;
        lock.lock();
        try {
        }
        finally {
            lock.unlock();
        }
        EnginePlugin.setObjectPropertyNoResponse(mobOid, Namespace.WORLD_MANAGER, "lootable", Boolean.FALSE);

        sendInvUpdate(looterOid);
        return true;
    }
    
    /**
     * Takes the items from the dead mob and places them into the looters bag.
     * @author Andrew
     */
    class LootItemHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.lootItemMessage lootMsg = (AgisInventoryClient.lootItemMessage)msg;
        	OID looterOid = lootMsg.getSubject();
        	OID mobOid = (OID) lootMsg.getProperty("mobOid");
        	OID itemOid = (OID) lootMsg.getProperty("itemOid");
        	
        	// Death check
	    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(looterOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
	    	if (dead) {
	    		ExtendedCombatMessages.sendErrorMessage(looterOid, "You cannot peform that action when dead");
	    		return true;
	    	}
	    	
        	OID accountId = (OID) EnginePlugin.getObjectProperty(looterOid, WorldManagerClient.NAMESPACE, "accountId");
	        //Long rootLooterBagOid = looterOid;
	        LinkedList<OID> loot = (LinkedList) EnginePlugin.getObjectProperty(mobOid, InventoryClient.NAMESPACE, "loot");
        	// Give the player the item
            /*Template overrideTemplate = new Template();
        	overrideTemplate.put(Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_PERSISTENT, true);
        	Long itemOid = ObjectManagerClient.generateObject(itemName, overrideTemplate);
        	boolean rv = addItem(looterOid, rootLooterBag.getOid(), itemOid);*/
        	
        	// remove the item from the mobs root bag
            boolean rv = removeItemFromBag(mobOid, itemOid);
            log.debug("LOOT: removed oid=" + itemOid + ", rv=" + rv);
            if (!rv) {
            	return true;
            }

            AgisItem item = getAgisItem(itemOid);
            if (item != null)
                ObjectManagerClient.setPersistenceFlag(itemOid,true);
            else
            	return true;
            
            HashMap<String, Serializable> logData = new HashMap<String, Serializable>();
	        logData.put("item", "Item attempt to loot : " + item.getName()+ " : OID " + itemOid + " : " );
	        logData.put("playerOid", looterOid);
	        
            DataLoggerClient.logData("ITEM_LOOTED_EVENT", looterOid, mobOid, accountId, logData);

            // add item to the looter's root bag
            rv = addItem(looterOid, looterOid, itemOid);
            log.debug("LOOT: addItem to looter, oid=" + itemOid + ", rv=" + rv);
            if (!rv) {
            	DataLoggerClient.logData("ITEM_LOOTED_FAILED_EVENT", looterOid, itemOid, accountId, logData);
            	addItem(mobOid, mobOid, itemOid);
            	return true;
            }
            loot.remove(itemOid);
            EnginePlugin.setObjectProperty(mobOid, InventoryClient.NAMESPACE, "loot", loot);
        	String lootedItem = item.getName();
        	if (lootedItem != null) {
        		EventMessageHelper.SendInventoryEvent(looterOid, EventMessageHelper.ITEM_LOOTED, item.getTemplateID(), item.getStackSize(), null);
        	    //WorldManagerClient.sendObjChatMsg(looterOid, 2, "You have looted: " + lootedItem);
        	}
	        
        	sendBagInvUpdate(looterOid);
    		sendLootList(looterOid, mobOid);
    		return true;
        }
    }
    
    protected boolean lootAllHelper(OID looterOid, Bag looterRootBag, OID mobOid, Bag mobRootBag, Bag mobSubBag) {
        // assumes locking
        // process each item in bag
    	// First lets see what items a player needs for their quests
    	List<Integer> questItemReqs = QuestClient.getQuestItemReqs(looterOid);
    	Log.debug("ANDREW - got questItemReqs response: " + questItemReqs.toString());
    	
    	//Random loot hack!
    	// Lets random a number between 0 and the number of items, only that item will be added
    	int numItems = 2; // Start with 2 so that sometimes no item will be dropped
    	int equippedItems = 0;
    	for (int slotNum=0; slotNum < mobSubBag.getNumSlots(); slotNum++) {
            EquipMap emap = getEquipMap(mobOid);
            OID itemOid = mobSubBag.getItem(slotNum);
            if (itemOid == null) {
                log.debug("lootAllHelper: slotNum " + slotNum + " is empty");
                continue;
            } else if (emap.containsValue(itemOid)) {
            	equippedItems++;
            	continue;
            }
            numItems++;
    	}
    	Random rand = new Random();
    	int roll = rand.nextInt(numItems);
    	roll = roll + equippedItems;
    	Log.debug("ANDREW: roll is: " + roll + "; num items: " + numItems + "; equipped items: " + equippedItems);
    	
        for (int slotNum=0; slotNum < mobSubBag.getNumSlots(); slotNum++) {
            EquipMap emap = getEquipMap(mobOid);
            OID itemOid = mobSubBag.getItem(slotNum);
            if (itemOid == null ||  emap.containsValue(itemOid)) {
                log.debug("lootAllHelper: slotNum " + slotNum + " is empty");
                continue;
            }
            boolean questItem = false;
            
            log.debug("lootAllHelper: processing sub bags item slot="+slotNum+
                " oid=" + itemOid);
            
            // remove the item from the mobs root bag
            boolean rv = removeItemFromBag(mobRootBag.getOid(), itemOid);
            log.debug("lootAllHelper: removed oid=" + itemOid + ", rv=" + rv);
            if (! rv) {
                continue;
            }

            AgisItem item = getAgisItem(itemOid);
            if(item.getItemType().equals("Quest")){
                Log.debug("ANDREW - found quest item " + item.getName() + ". does the player need it?");
                int needed = 0;
                for (int i = 0; i < questItemReqs.size(); i++) {
                	if (questItemReqs.get(i) == item.getTemplateID())
                		needed = 1;
                }
                if (needed == 0)
                    continue;
                Log.debug("ANDREW - found quest item - it was needed");
                questItem = true;
            }
            if (item != null) {
                ObjectManagerClient.setPersistenceFlag(itemOid,true);
            } else {
                continue;
            }

            // add item to the looter's root bag
            if (slotNum != roll)
            	continue;
            rv = addItem(looterOid, looterRootBag.getOid(), itemOid);
            log.debug("lootAllHelper: addItem to looter, oid=" + itemOid + ", rv=" + rv);
            if (! rv) {
                continue;
            }
            WorldManagerClient.sendObjChatMsg(looterOid, 2, "You have recieved: " + item.getName());
        }
        log.debug("lootAllHelper: done processing subbag " + mobSubBag);
        return true;
    }

    /**
     * Returns whether or not the mob contains the given item
     * @param mobOid the player/mob to check against
     * @param itemOid the item you are checking against
     * @return true if mob contains item, false otherwise
     */
    protected boolean containsItem(OID mobOid, OID itemOid) {
        lock.lock();
        try {
            AgisItem item = getAgisItem(itemOid);
            if (item == null) {
                return false;
            }
            if (item.isDeleted()) {
                return false;
            }
            OID subBagOid = (OID)item.getProperty(INVENTORY_PROP_BACKREF_KEY);
            if (subBagOid == null) {
                return false;
            }
            
            // get the sub-bag
            Bag subBag = getBag(subBagOid);
            if (subBag == null) {
                return false;
            }
            
            // get the mob owner oid -- which matches the parent bag oid by convention
            OID rootBagOid = (OID)subBag.getProperty(INVENTORY_PROP_BACKREF_KEY);
            if (rootBagOid == null) {
                return false;
            }
            return (mobOid.equals(rootBagOid));
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * finds an item based on the template name
     */
    protected OID findItem(OID mobOid, int templateID) {
    	lock.lock();
    	try {
	    // find the mob's root bag
            if (Log.loggingDebug)
                log.debug("findItem: mob=" + mobOid + " template=" + templateID);
            OID rootBagOid = mobOid;
            if (rootBagOid == null) {
            	log.debug("findItem: cant find rootBagOid");
            	return null;
	    	}
            InventoryInfo iInfo = getInventoryInfo(rootBagOid);
	    	if (iInfo == null) {
	    		log.debug("findItem: could not find root bag");
				return null;
	    	}

	    	ArrayList<OID> resultList = new ArrayList<OID>();
	    	findItemHelper(mobOid, iInfo, templateID, resultList);
	    	return resultList.get(0);
		}
		finally {
	    	lock.unlock();
		}
    }
    
    /**
     * Finds all stacks of items matching the templateID that the player has.
     * @param mobOid
     * @param templateID
     * @return
     */
    protected ArrayList<OID> findItemStacks(OID mobOid, int templateID) {
    	lock.lock();
    	try {
    		// find the mob's root bag
            if (Log.loggingDebug)
                log.debug("findItem: mob=" + mobOid + " template=" + templateID);
            OID rootBagOid = mobOid;
	    	if (rootBagOid == null) {
	    		log.debug("findItem: cant find rootBagOid");
	    		return null;
	    	}
	    	InventoryInfo iInfo = getInventoryInfo(rootBagOid);
	    	if (iInfo == null) {
	    		log.debug("findItem: could not find root bag");
	    		return null;
	    	}

	    	ArrayList<OID> resultList = new ArrayList<OID>();
	    	findItemStacksHelper(mobOid, iInfo, templateID, resultList);
	    	return resultList;
    	}
    	finally {
    		lock.unlock();
    	}
    }
    
    /**
     * Helper function for the findItemStacks function.
     * @param mobOid
     * @param iInfo
     * @param templateID
     * @param resultList
     */
    protected void findItemStacksHelper(OID mobOid, InventoryInfo iInfo, int templateID, ArrayList<OID>resultList) {
    	OID[] subBags = iInfo.getBags();
        for (int i = 0; i < subBags.length; i++) {
        	OID subBagOid = subBags[i];
        	if (subBagOid == null)
        		continue;
        	Bag subBag = getBag(subBagOid);
        	for (OID itemOid : subBag.getItemsList()) {
        	    if (itemOid == null)
        		    continue;
        		AgisItem item = getAgisItem(itemOid);
        		if (templateID == item.getTemplateID()) {
        		    if (resultList.contains(itemOid))
        			    continue;
                    if (Log.loggingDebug)
                        log.debug("findItemHelper: adding item to resultList=" + itemOid);
        		    resultList.add(itemOid);
        		}
        	}
        }
    }

    /**
     * Finds items in the players inventory that match one of the template IDs specified.
     */
    protected ArrayList<OID> findItems(OID mobOid, ArrayList<Integer> templateList) {
    	lock.lock();
    	try {
	    // find the mob's root bag
            if (Log.loggingDebug)
                log.debug("findItem: mob=" + mobOid + " templateList=" + templateList);
            OID rootBagOid = mobOid;
	    	if (rootBagOid == null) {
	    		log.debug("findItem: cant find rootBagOid");
				return null;
	    	}
	    	InventoryInfo iInfo = getInventoryInfo(rootBagOid);
	    	if (iInfo == null) {
	    		log.debug("findItem: could not find root bag");
	    		return null;
	    	}

	    	ArrayList<OID> resultList = new ArrayList<OID>();
	    	for (int template : templateList) {
	    		findItemHelper(mobOid, iInfo, template, resultList);
	    	}
	    	return resultList;
    	}
    	finally {
	    	lock.unlock();
		}
    }

    /**
     * Helper function for the findItem and findItems functions.
     * @param mobOid
     * @param iInfo
     * @param templateID
     * @param resultList
     * @return
     */
    protected boolean findItemHelper(OID mobOid, InventoryInfo iInfo, int templateID, ArrayList<OID>resultList) {
    	OID[] subBags = iInfo.getBags();
        for (int i = 0; i < subBags.length; i++) {
        	OID subBagOid = subBags[i];
        	if (subBagOid == null)
        		continue;
        	Bag subBag = getBag(subBagOid);
        	for (OID itemOid : subBag.getItemsList()) {
        	    if (itemOid == null)
        		    continue;
        		AgisItem item = getAgisItem(itemOid);
        		if (templateID == item.getTemplateID()) {
        		    if (resultList.contains(itemOid))
        			    continue;
                    if (Log.loggingDebug)
                        log.debug("findItemHelper: adding item to resultList=" + itemOid);
        		    resultList.add(itemOid);
        		    return true;
        		}
        	}
        }
	    resultList.add(null);
	    return false;
    }

    /**
     * Finds an equipped item
     * @param mobOid
     * @param slot
     * @return
     */
    protected OID findItem(OID mobOid, AgisEquipSlot slot) {
	lock.lock();
	try {
	    EquipMap equipMap = getEquipMap(mobOid);
	    OID itemOid = equipMap.get(slot);
	    if (itemOid != null) {
	    	return equipMap.get(slot);
	    } else {
	    	return null;
	    }
	}
	finally {
	    lock.unlock();
	}
    }

    /**
     * Removes the specified item from the player. 
     */
    protected OID removeItem(OID mobOid, OID itemOid, boolean removeFromPlayer) {
    	lock.lock();
    	try {
    		AgisItem item = getAgisItem(itemOid);
    		if (item == null)
    			return null;
	    	unequipItem(item, mobOid, false);
	    	OID rootBagOid = mobOid;
	    	if (rootBagOid == null) {
		    log.debug("removeItem: cant find rootBagOid");
		    return null;
	    	}
	    	Boolean result = removeItemFromBag(rootBagOid, itemOid);
	    	if (result == true) {
	    		if (removeFromPlayer)
	    			item.unacquired(mobOid);
	    		return itemOid;
	    	} else {
	    		return null;
	    	}
        } finally {
    		lock.unlock();
		}
    }

    /**
     * Removes an item from the players inventory that matches the specified templateID
     */
    protected OID removeItem(OID mobOid, int template, boolean removeFromPlayer) {
    	lock.lock();
    	try {
    		OID itemOid = findItem(mobOid, template);
            if (Log.loggingDebug)
                log.debug("removeItem: mobOid=" + mobOid + " template=" + template + " ItemOid=" + itemOid);
            return removeItem(mobOid, itemOid, removeFromPlayer);
    	} finally {
    		lock.unlock();
    	}
    }

    /**
     * Removes all items from the players inventory that match one of the template IDs specified.
     */
    protected ArrayList<OID> removeItems(OID mobOid, ArrayList<Integer> templateList, boolean removeFromPlayer) {
    	lock.lock();
		try {
            if (Log.loggingDebug)
                log.debug("removeItems: mobOid=" + mobOid + " templateList=" + templateList);
            ArrayList<OID> itemList = findItems(mobOid, templateList);
            //if (itemList.contains(null)) {
            //    return null;
            //}
            for (OID itemOid : itemList) {
            	if (itemOid != null)
            		removeItem(mobOid, itemOid, removeFromPlayer);
            }
            // remove all the nulls from the list
            for (int i = itemList.size() - 1; i >= 0; i--) {
            	if (itemList.get(i) == null)
            		itemList.remove(i);
            }
            return itemList;
		} finally {
	    	lock.unlock();
		}
    }
    
    /**
     * Removes all items from the players inventory that match one of the template IDs specified.
     */
    protected ArrayList<OID> removeAllItems(OID mobOid) {
    	lock.lock();
		try {
            if (Log.loggingDebug)
                log.debug("removeAllItems: mobOid=" + mobOid);
            ArrayList<OID> itemList = new ArrayList<OID>();
            InventoryInfo iInfo = getInventoryInfo(mobOid);
            OID[] subBags = iInfo.getBags();
            for (int pos = 0; pos < subBags.length; pos++) {
            	if (pos >= INVENTORY_BAG_COUNT)
            		break;
            	Bag subBag = getBag(subBags[pos]);
            	for(OID itemOID : subBag.getItemsList()) {
            		if (itemOID != null)
            			itemList.add(itemOID);
            	}
            }
            for (OID itemOid : itemList) {
            	if (itemOid != null)
            		removeItem(mobOid, itemOid, true);
            }
            return itemList;
		} finally {
	    	lock.unlock();
		}
    }

    /**
     * Activates the item. Depending on the activation type the item has it may cause
     * the item to be equipped, start an ability, or do some other action.
     */
    protected boolean activateObject(OID objOid, OID activatorOid, OID targetOid) {
        AgisItem item = getAgisItem(objOid);
        if (item == null) {
            Log.warn("ActivateHook: item is null, oid=" + objOid);
            return false;
        }

        return item.activate(activatorOid, targetOid);
    }
    
    /**
     * Checks if the item specified is equipped by the player.
     * @param itemObj
     * @param activatorOid
     * @return
     */
    public boolean isItemEquipped(AOObject itemObj, OID activatorOid) {
    	AgisItem item = AgisItem.convert(itemObj);
    	AgisEquipSlot slot = item.getPrimarySlot();
    	if (slot == null) {
    		return false;
    	}
    	EquipMap equipMap = getEquipMap(activatorOid);

        // is the slot free?
        OID oItemOid = equipMap.get(slot);
        if (oItemOid != null && oItemOid.equals(item.getOid())) {
        	Log.debug("EQUIP: got isEquipped item: " + oItemOid);
        	return true;
        }
    	return false;
    }

    /**
     * Equips the item to its primary slot on the player/mob. The replace
     * parameter is used to unequip an existing item if there is one in the 
     * slot already. If it is set to false and there is an item in the slot, 
     * the new item will not be equipped.
     */
    public boolean equipItem(AOObject itemObj, OID activatorOid,
            boolean replace) {
        log.debug("AgisInventoryPlugin.equipItem: item=" + itemObj
                + ", activatorOid=" + activatorOid);

        AgisItem item = AgisItem.convert(itemObj);

        // is activator allowed to use the item
        // for now, ignore if the item has no callback
        PermissionCallback cb = item.permissionCallback();
        if ((cb != null) && (!cb.use(activatorOid))) {
            log.warn("permission callback failed");
            return false;
        }

        // get the primary slot for the item
        AgisEquipSlot slot = item.getPrimarySlot();
        if (slot == null) {
            Log.warn("AgisInventoryPlugin: slot is null for item: " + item);
            return false;
        }

        EquipMap equipMap;
        lock.lock();
        try {
            equipMap = getEquipMap(activatorOid);
            
            // is the slot free?
	        OID oItemOid = equipMap.get(slot);
	        log.debug("EQUIPITEM: item in primary slot: " + slot + " = " + oItemOid);
	        
	        if (oItemOid != null && !replace) {
	        	if (Log.loggingDebug)
                    log.debug("AgisInventoryPlugin: slot occupied and not set to replace - returning");
	        	return false;
	        }
            
	        // If the item is a two handed one, unequip the secondary weapon as well
            if (item.getProperty("slot") != null) {
            	String itemSlot = (String) item.getProperty("slot");
            	if (itemSlot.equals("Two Hand")) {
        	        OID oItemOid2 = equipMap.get(AgisEquipSlot.SECONDARYWEAPON);
        	        if (oItemOid2 != null) {
        		        AgisItem oItemObj = getAgisItem(oItemOid2);
        		        log.debug("EQUIPITEM: item = " + oItemObj);
                        if (Log.loggingDebug)
                            log.debug("AgisInventoryPlugin: slot occupied 2");
                        if (replace) {
                            unequipItem(oItemObj, activatorOid, false);
                        } else {
                        	//TODO: Re-equip the first item
                        	return false;
                        }
                    }
            	}
            }
           
            // If this item goes in the secondary weapon slot, check if the player has an two hand item
            if (slot.equals(AgisEquipSlot.SECONDARYWEAPON)) {
            	OID oItemOid3 = equipMap.get(AgisEquipSlot.PRIMARYWEAPON);
            	if (oItemOid3 != null) {
    		        AgisItem oItemObj = getAgisItem(oItemOid3);
    		        if (oItemObj.getProperty("slot") != null) {
    	            	String itemSlot = (String) oItemObj.getProperty("slot");
    	            	if (itemSlot.equals("Two Hand")) {
    	            		log.debug("EQUIPITEM: item = " + oItemObj);
    	                    if (Log.loggingDebug)
    	                        log.debug("AgisInventoryPlugin: slot occupied 3");
    	                    if (replace) {
    	                        unequipItem(oItemObj, activatorOid, false);
    	                    } else {
    	                    	return false;
    	                    }
    	            	}
    		        }
                }
            }
            
            Bag sourceBag = null;
	        int sourceSlot = -1;
            if (oItemOid != null) {
                // Get current bag and slot of the item we are equipping so the currently equipped item
                // can go in that slot
                InventoryInfo iInfo = getInventoryInfo(activatorOid);
                OID[] subBags = iInfo.getBags();
                for (int pos = 0; pos < subBags.length; pos++) {
                    Bag subBag = getBag(subBags[pos]);
                    Integer slotNum = subBag.findItem(item.getOid());
                    if (slotNum != null) {
                    	sourceBag = subBag;
                    	sourceSlot = slotNum;
                    }
                }
                
                if (sourceBag == null) {
                	// Somehow failed to find the bag where the equipped item is currently located
                	return false;
                }
            }
            
            // Remove the item from the normal bag and place it in the equipped bag
	        OID itemOid = removeItem(activatorOid, item.getOid(), false);
            
            // Unequip the item putting it into the source bag/slot
            if (oItemOid != null) {
            	AgisItem oItemObj = getAgisItem(oItemOid);
                if (Log.loggingDebug)
                    log.debug("AgisInventoryPlugin: slot occupied 4");
                if (!unequipItem(oItemObj, activatorOid, true, sourceBag, sourceSlot)) {
                	// The unequip failed, return the item back to it's original slot
                	log.error("AgisInventoryPlugin.equipItem - unequip failed, returning item to bag");
                	addItemHelper(activatorOid, sourceBag.getOid(), sourceSlot, item);
                	return false;
                }
            }
            
            // place it in the equipped bag
	        placeEquippedItem(activatorOid, itemOid, slot);
	        item.itemEquipped();
	        
	        // If there is an incompatible ammo type, remove it
	        Integer ammoTypeReq = (Integer) item.getProperty(AgisItem.AMMO_TYPE);
		    if (ammoTypeReq != null) {
		    	Integer ammoID = (Integer) EnginePlugin.getObjectProperty(activatorOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_LOADED);
		    	if (ammoID != null && ammoID > 0) {
		    		// get type of ammo and compare it to the req
		    		Template itemTemplate = ObjectManagerClient.getTemplate(ammoID, ObjectManagerPlugin.ITEM_TEMPLATE);
		    		Integer ammoType = (Integer) itemTemplate.get(InventoryClient.ITEM_NAMESPACE, AgisItem.AMMO_TYPE);
		    		if (ammoType != ammoTypeReq) {
		    			// Clear out ammo settings
		    			Log.debug("AMMO: got incompatible ammo: " + ammoType + "from ammoID: " + ammoID + " when " + ammoTypeReq + " is needed");
						EnginePlugin.setObjectPropertiesNoResponse(activatorOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_LOADED, -1);
				        EnginePlugin.setObjectPropertiesNoResponse(activatorOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_DAMAGE, 0);
		    		}
		    	}
		    }
	        
	        // place object in slot
            equipMap.put(slot, item.getMasterOid());
	        setDirty(activatorOid);
        } finally {
            lock.unlock();
        }
        AgisInventoryClient.itemEquipStatusChanged(activatorOid, item, true, slot.toString());

        if (Log.loggingDebug)
            log.debug("AgisInventoryPlugin: calling addDC, activatorOid="
                      + activatorOid + ", item=" + item);
        // update world manager's displaycontext for the obj
        //if (!addDC(activatorOid, item)) {
        //    Log.warn("AgisInventoryPlugin.equipItem: problem adding dc for item " + item);
        //}
        String displayVal = (String) item.getProperty("displayVal");
        EquipHelper.updateDisplay(activatorOid, displayVal, item.getPrimarySlot());
        
        sendInvUpdate(activatorOid);
        return true;
    }

    public boolean unequipItem(AOObject itemObj, OID activatorOid, boolean isReplaced) {
    	return unequipItem(itemObj, activatorOid, isReplaced, null, -1);
    }

    /**
     * Unequips the specified item. If isReplaced is set to true no display update
     * message will be sent as one will be sent when the new item is equipped.
     */
    public boolean unequipItem(AOObject itemObj, OID activatorOid, boolean isReplaced, Bag targetBag, int slotNum) {
        log.debug("AgisInventoryPlugin.unequipItem: item=" + itemObj
                + ", mobOid=" + activatorOid);

        AgisItem item = AgisItem.convert(itemObj);

        // is activator allowed to use the item
        // for now, ignore if the item has no callback
        PermissionCallback cb = item.permissionCallback();
        if ((cb != null) && (!cb.use(activatorOid))) {
            log.warn("callback failed");
            return false;
        }

        lock.lock();
        try {
            // where is this currently equipped
            EquipMap equipMap = getEquipMap(activatorOid);
            AgisEquipSlot slot = equipMap.getSlot(item.getMasterOid());
            if (slot == null) {
                // item is not currently equipped
                Log.warn("AgisInventoryPlugin.unequipItem: item not equipped: item=" + item);
                return false;
            }
            
            // First check if there is an open slot for the item
            InventoryInfo iInfo = getInventoryInfo(activatorOid);
            boolean hasSpace = hasSpace(activatorOid, item.getTemplateID(), 1, 0);
            
            if (hasSpace || (isReplaced && targetBag != null)) {
                // remove the item from the map
                equipMap.remove(slot);
	            // Remove the item from the equipped bag and place it back in a normal spot
	            // We probably should add in a check to make sure the bag has an empty spot to place the item in
	            removeEquippedItem(activatorOid, item.getOid(), slot);
	            if (targetBag != null) {
	            	addItemHelper(activatorOid, targetBag.getOid(), slotNum, item);
	            	Engine.getPersistenceManager().setDirty(iInfo);
	            } else {
	            	addItem(activatorOid, iInfo.getOid(), item.getOid());
	            }
	            setDirty(activatorOid);
	            AgisInventoryClient.itemEquipStatusChanged(activatorOid, item, false, slot.toString());
	            //if (!removeDC(mobOid, item)) {
	            //    Log.warn("AgisInventoryPlugin.unequipItem: problem removing dc for item " + item);
	            //}
	            if (!isReplaced)
	            	EquipHelper.updateDisplay(activatorOid, null, item.getPrimarySlot());
	            if (Log.loggingDebug)
	                log.debug("AgisInventoryPlugin.unequipItem: removed DC for item:" + item);
            }  
        } finally {
            lock.unlock();
        }
        
        if (!isReplaced)
        	sendInvUpdate(activatorOid);
        return true;
    }
    
    /**
     * Checks if the player has enough space in their inventory for the items being added.
     * @param activatorOid: the identifier of the person adding the items to their inventory
     * @param numItems: how many items are being added
     * @return: a boolean indicating whether or not the player has space
     */
    public boolean hasSpace(OID looterOid, int itemID, int count, int invType) {
    	lock.lock();
    	int spaceNeeded = 0;
    	int freeSpaces = 0;
        try {
            // get bag
            // find the looters root bag
        	OID rootLooterBagOid = looterOid;
            if (rootLooterBagOid == null) {
                log.debug("lootAll: cant find rootLooterBagOid");
                return false;
            }
            InventoryInfo iInfo = getInventoryInfo(looterOid);
            // Normal bag(s)
            if (invType == 0) {
        		// First check for existing stacks that we could add to
            	// Check if there are any existing stacks to add to
            	Template tmpl = ObjectManagerClient.getTemplate(itemID, ObjectManagerPlugin.ITEM_TEMPLATE);
            	int stackLimit = (Integer) tmpl.get(InventoryClient.ITEM_NAMESPACE, "stackLimit");
                ArrayList<OID> existingOids = findItemStacks(looterOid, itemID);
                if (existingOids.size() > 0) {
                	Log.debug("ITEM: user already has item " + itemID + ", see if we can add it to one of the stacks.");
                	for (OID existingOid: existingOids) {
                		AgisItem tempItem = getAgisItem(existingOid);
                		Log.debug("ITEM: stackSize " + tempItem.getStackSize() + ", stackLimit: " + tempItem.getStackLimit() + "for item: " + existingOid);
                		if(tempItem.getStackSize() < tempItem.getStackLimit()) {
                			Log.debug("ITEM: reducing count in hasSpace for item: " + itemID);
                			count = count - (tempItem.getStackLimit() - tempItem.getStackSize());
                			if (count <= 0) {
                				Log.debug("ITEM: hasSpace check has been fulfilled before creating new stacks for item: " + itemID);
                				count = 0;
                				break;
                			}
                		}
                	}
                }
                if (count > 0) {
                	spaceNeeded = ((count-1) / stackLimit) + 1;
        	        Log.debug("ITEM: item " + itemID + " needs " + spaceNeeded + " with a count of " + count + " and stackLimit of " + stackLimit);
                	// check each subbag and see if it can be added there
        	        OID[] subBags = iInfo.getBags();
                    for (int pos = 0; pos < subBags.length; pos++) {
                    	OID subBagOid = subBags[pos];
                        Bag subBag = getBag(subBagOid);
                        if (subBag == null) {
                            Log.warn("hasSpace: did not find sub bag: " + subBagOid + " for bagoid=" + subBagOid);
                            continue;
                        }
                        int numSlots = subBag.getNumSlots();
                        for (int i = 0; i < numSlots; i++) {
                        	if (subBag.getItem(i) == null) {
                        		Log.debug("hasSpace: bag has free space at spot: " + i);
                        		freeSpaces++;
                        		if (freeSpaces >= spaceNeeded)
                            	    return true;
                        	}	
                        }
                    }
                }
            }
    	} finally {
    		lock.unlock();
    	}
    	Log.debug("hasSpace: freeSpaces = " + freeSpaces + "; spaceNeeded = " + spaceNeeded);
    	if (freeSpaces >= spaceNeeded)
    	    return true;
    	else
    		return false;
    }
    
    /**
     * Places an equipped item in the correct slot that it belongs in
     * @param activatorOid
     * @param itemOid
     * @param slot
     * @return a boolean indicating whether or not the item was successfully placed
     */
    public boolean placeEquippedItem(OID activatorOid, OID itemOid, AgisEquipSlot slot) {
    	Log.debug("placeEquippedItem: slot = " + slot);
    	InventoryInfo iInfo = getInventoryInfo(activatorOid);
    	OID subBagOid = iInfo.getEquipmentItemBag();
    	Bag subBag = getBag(subBagOid); 
    	Entity item = getAgisItem(itemOid);
    	
    	//if (item.getProperty(INVENTORY_PROP_BACKREF_KEY) != null) {
        //    Log.warn("addItem: item is already in a container, itemOid="
        //            + item.getOid());
        //    return false;
        //}
        // add item to bag
    	int slotNum = -1; // Use some table of slot types to nums
    	if (slot.equals(AgisEquipSlot.PRIMARYWEAPON))
    		slotNum = 0;
    	else if (slot.equals(AgisEquipSlot.SECONDARYWEAPON))
    		slotNum = 1;
    	if (slot.equals(AgisEquipSlot.HEAD))
    		slotNum = 2;
    	else if (slot.equals(AgisEquipSlot.BACK) || slot.equals(AgisEquipSlot.CAPE))
    		slotNum = 3;
    	else if (slot.equals(AgisEquipSlot.SHIRT))
    		slotNum = 4;
    	else if (slot.equals(AgisEquipSlot.SHOULDER))
    		slotNum = 5;
    	else if (slot.equals(AgisEquipSlot.CHEST))
    		slotNum = 6;
    	else if (slot.equals(AgisEquipSlot.HANDS))
    		slotNum = 7;
    	else if (slot.equals(AgisEquipSlot.LEGS))
    		slotNum = 8;
    	else if (slot.equals(AgisEquipSlot.FEET))
    		slotNum = 9;
    	else if (slot.equals(AgisEquipSlot.NECK))
    		slotNum = 10;
    	else if (slot.equals(AgisEquipSlot.BELT))
    		slotNum = 11;
    	else if (slot.equals(AgisEquipSlot.PRIMARYRING))
    		slotNum = 12;
    	else if (slot.equals(AgisEquipSlot.SECONDARYRING))
    		slotNum = 13;
    	
    	if (slotNum == -1) {
    		Log.debug("placeEquippedItem: slot is -1");
    		return false;
    	}
    	boolean rv = subBag.putItem(slotNum, itemOid);
        if (Log.loggingDebug)
            log.debug("placeEquippedItem: adding to bag, rv=" + rv);

        if (rv) {
            item.setProperty(INVENTORY_PROP_BACKREF_KEY, subBagOid);
        }

        // mark dirty
        Engine.getPersistenceManager().setDirty(item);
        Engine.getPersistenceManager().setDirty(iInfo);
        return rv;
    	//return true;
    }
    
    /**
     * Removes an equipped item from the equipped items bag
     * @param activatorOid
     * @param itemOid
     * @param slot
     * @return a boolean indicating whether or not the item was successfully removed
     */
    public boolean removeEquippedItem(OID activatorOid, OID itemOid, AgisEquipSlot slot) {
    	InventoryInfo iInfo = getInventoryInfo(activatorOid);
    	OID subBagOid = iInfo.getEquipmentItemBag();
    	Bag subBag = getBag(subBagOid); 
    	Entity item = getAgisItem(itemOid);
    	
    	// does bag contain the item?
        Integer slotNum = subBag.findItem(item.getOid());
        if (slotNum == null) {
            if (Log.loggingDebug)
                log.debug("removeItemFromBagHelper: item not in bag itemOid=" + item.getOid() + " bagOid="+subBagOid);
            return false;
        }
        
        // found the item
        if (Log.loggingDebug)
            log.debug("removeItemFromBagHelper: found - slot=" + slotNum + ", itemOid=" + item.getOid());
        
        // remove item from bag - we seperate the logic here from finding the item
        // because perhaps there was some other reason why the remove failed
        boolean rv = subBag.removeItem(item.getOid());
        if (rv == false) {
            if (Log.loggingDebug)
                log.debug("removeItemFromBagHelper: remove item failed");
            return false;
        }
        
        // remove the back reference
        item.setProperty(INVENTORY_PROP_BACKREF_KEY, null);

        // mark dirty
        Engine.getPersistenceManager().setDirty(item);
        Engine.getPersistenceManager().setDirty(iInfo);
        if (Log.loggingDebug)
            log.debug("removeItemFromBagHelper: remove from bag, rv=" + rv);
        return rv;
    	//return true;
    }
    
    /*
     * Bag Functions
     */
    
    /**
     * Updates the properties for the bag in the specified location. It will get the Bag name and size etc from the item and set 
     * the bag to match those properties.
     * @param item: the item to place as a bag
     * @param mobOid: the oid of the player placing a bag
     * @param parentBagSlotNum: the slot to put the bag data in
     */
    private int changeBagInSlot(AgisItem item, OID mobOid, int parentBagSlotNum) {
    	if (parentBagSlotNum == 0) {
    		ExtendedCombatMessages.sendErrorMessage(mobOid, "You cannot swap out your Backpack.");
    		return -1;
    	}
    	// Verify the item is a Bag
    	Log.debug("BAG: changing bag in slot: " + parentBagSlotNum + " to item with type: " + item.getType());
    	if (!item.getItemType().equals("Bag")) {
    		return -1;
    	}
    	InventoryInfo iInfo = getInventoryInfo(mobOid);
		if (iInfo == null)
			return -1;
        OID[] subBags = iInfo.getBags();
    	OID subBagOid = subBags[parentBagSlotNum];
    	Bag subBag = getBag(subBagOid);
    	// If the new bag is of equal size or bigger, automatically move the items to it
    	Log.debug("BAG: bag has num slots: " + item.getProperty("numSlots"));
    	int newNumSlots = (Integer)item.getProperty("numSlots");
    	OID[] itemsInBag = subBag.getItemsList();
    	
    	Boolean result = removeItemFromBag(mobOid, item.getOid());
    	if (result == false) {
    		return -1;
    	}
    	
    	if (newNumSlots < subBag.getNumSlots()) {
    		// Make sure the bag is empty before swapping.
        	//TODO: Get the number of items in the old bag and see if they can be placed in the new one
        	//Long[] oldItems = new Long;
        	for (int i = 0; i < itemsInBag.length; i++) {
        		Log.debug("BAG: checking items in bag for swap. Item: " + itemsInBag[i]);
        		if (itemsInBag[i] != null) {
        			ExtendedCombatMessages.sendErrorMessage(mobOid, "This bag cannot hold all of the items in the bag you are currently using.");
            		return -1;
        		}
        	}
        	subBag.setNumSlots(newNumSlots);
    	} else {
    		subBag.setNumSlots(newNumSlots);
    		OID[] newItems = new OID[newNumSlots];
    		for (int i = 0; i < itemsInBag.length; i++)
    			newItems[i] = itemsInBag[i];
    		subBag.setItemsList(newItems);
    	}
    	
    	// get the item template id of the old bag so it can be recreated (if it is a valid item ID)
    	int oldBagID = subBag.getItemTemplateID();
    	// Now set the new settings
    	subBag.setName(item.getName());
    	subBag.setItemTemplateID(item.getTemplateID());
    	Engine.getPersistenceManager().setDirty(iInfo);
    	// return the old back ID
    	return oldBagID;
    }
    
    /**
     * Handles the placeBagMessage. Places the specified Bag object in the specified slot.
     *
     */
    class PlaceBagHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.placeBagMessage removeMsg = (AgisInventoryClient.placeBagMessage)msg;
	        OID oid = removeMsg.getSubject();
	        OID itemOid = (OID) removeMsg.getProperty("itemOid");
	        int bagSpotNum = (Integer) removeMsg.getProperty("bagSpotNum");
	        lock.lock();
	    	try {
	    	    AgisItem item = getAgisItem(itemOid);
	    	    if (item == null)
	    		    return true;
	    	    OID rootBagOid = oid;
		    	if (rootBagOid == null) {
		    		log.debug("placeBag: cant find rootBagOid");
		    		return true;
		    	}
		    	int oldBagID = changeBagInSlot(item, oid, bagSpotNum);
		    	if (oldBagID > 0) {
		    	    // Create an item to represent the old Bag
		    	    AgisInventoryClient.generateItem(oid, oldBagID, item.getName(), 1, null);
		    	}
	    	} finally {
	    	    lock.unlock();
	    	}
	    	sendBagInvUpdate(oid);
	        return true;
	    }
    }
    
    /**
     * Handles the MoveBagMessage. Swaps the properties for the bags in the 
     * specified locations. It will get the Bag name and size etc from the item 
     * and set the other bag to match those properties.
     * @author Andrew Harrison
     */
    class MoveBagHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.moveBagMessage genMsg = (AgisInventoryClient.moveBagMessage)msg;
        	OID mobOid = genMsg.getSubject();
	        int parentBagSlotNum = (Integer) genMsg.getProperty("bagSpotNum");
	        int newSlotNum = (Integer) genMsg.getProperty("newSpotNum");
        	if (parentBagSlotNum == 0 || newSlotNum == 0) {
        		ExtendedCombatMessages.sendErrorMessage(mobOid, "You cannot swap out your Backpack.");
        		return true;
    		}
        	InventoryInfo iInfo = getInventoryInfo(mobOid);
    		if (iInfo == null)
    			return true;
    		
            OID[] subBags = iInfo.getBags();
    		OID subBagOid1 = subBags[parentBagSlotNum];
    		Bag subBag1 = getBag(subBagOid1);
    		OID[] itemsInBag1 = subBag1.getItemsList();
    		int bagTemplate1 = subBag1.getItemTemplateID();
    		String bagName1 = subBag1.getName();
    		int bagNumSlots1 = subBag1.getNumSlots();
    		
    		// Prevent moving bags if they have any items in them
    		//TODO: get this working later - will require re-setting oid for each item
    		for (OID itemOid : itemsInBag1) {
    			if (itemOid != null) {
    				ExtendedCombatMessages.sendErrorMessage(mobOid, "You cannot move bags with items inside.");
    				return true;
    			}
    		}
    	
    		OID subBagOid2 = subBags[newSlotNum];
    		Bag subBag2 = getBag(subBagOid2);
    		OID[] itemsInBag2 = subBag2.getItemsList();
    		int bagTemplate2 = subBag2.getItemTemplateID();
    		String bagName2 = subBag2.getName();
    		int bagNumSlots2 = subBag2.getNumSlots();
    	
    		subBag1.setItemTemplateID(bagTemplate2);
    		subBag1.setName(bagName2);
    		subBag1.setNumSlots(bagNumSlots2);
    		subBag1.setItemsList(itemsInBag2);
    	
    		subBag2.setItemTemplateID(bagTemplate1);
    		subBag2.setName(bagName1);
    		subBag2.setNumSlots(bagNumSlots1);
    		subBag2.setItemsList(itemsInBag1);
    	
    		Engine.getPersistenceManager().setDirty(iInfo);
    		sendBagInvUpdate(mobOid);
    		return true;
        }
    }
    
    /**
     * Handles the RemoveBagMessage. Clears the bag data from the specified slot 
     * as if the bag was being removed. If a container and slot ID is given 
     * it will generate and place the bag item in that spot.
     * @author Andrew Harrison
     *
     */
    class RemoveBagHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
        	AgisInventoryClient.removeBagMessage genMsg = (AgisInventoryClient.removeBagMessage)msg;
        	OID mobOid = genMsg.getSubject();
	        int parentBagSlotNum = (Integer) genMsg.getProperty("bagSpotNum");
	        int containerId = (Integer) genMsg.getProperty("containerId");
	        int slotId = (Integer) genMsg.getProperty("slotId");
        	if (parentBagSlotNum == 0) {
        		ExtendedCombatMessages.sendErrorMessage(mobOid, "You cannot swap out your Backpack.");
        		return true;
    		}
        	InventoryInfo iInfo = getInventoryInfo(mobOid);
    		if (iInfo == null)
    			return true;
            OID[] subBags = iInfo.getBags();
    		OID subBagOid1 = subBags[parentBagSlotNum];
    		Bag subBag1 = getBag(subBagOid1);
    		OID[] itemsInBag1 = subBag1.getItemsList();
    		for (int i = 0; i < itemsInBag1.length; i++) {
        		Log.debug("BAG: checking items in bag for removal. Item: " + itemsInBag1[i]);
        		if (itemsInBag1[i] != null) {
        			ExtendedCombatMessages.sendErrorMessage(mobOid, "You cannot remove a bag that has items inside it.");
            		return true;
        		}
        	}
    		int bagID1 = subBag1.getItemTemplateID();
    		String bagName1 = subBag1.getName();
    		
    		// If the container Id is not -1 then we need to place the item in the specified container/slot
    		if (containerId != -1) {
    			OID subBagOid2 = subBags[containerId];
        		Bag subBag2 = getBag(subBagOid2);
        		OID[] itemsInBag2 = subBag2.getItemsList();
        		
        		// Make sure the slot the item will be placed in is empty
        		if (itemsInBag2[slotId] != null) {
        			ExtendedCombatMessages.sendErrorMessage(mobOid, "You must place the bag in an empty slot.");
            		return true;
        		}
        		
        		OID itemOid = generateItem(bagID1, bagName1);
        		if (!placeItem(mobOid, mobOid, itemOid, containerId, slotId, false)) {
        			ExtendedCombatMessages.sendErrorMessage(mobOid, "You must place the bag in an empty slot.");
            		return true;
        		}
    		}
    		
    		subBag1.setNumSlots(0);
    		subBag1.setName("");
    		subBag1.setItemTemplateID(-1);
    		Engine.getPersistenceManager().setDirty(iInfo);
    		sendBagInvUpdate(mobOid);
    		return true;
        }
    }
    
    // Log the login information and send a response
    class LoginHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LoginMessage message = (LoginMessage) msg;
            OID playerOid = message.getSubject();
            OID instanceOid = message.getInstanceOid();
            Log.debug("LoginHook: playerOid=" + playerOid + " instanceOid=" + instanceOid);
	        // Check for mail available here?
	        
	        aDB.checkCharacterPurchases(playerOid);
	        InventoryInfo iInfo = getInventoryInfo(playerOid);
	        iInfo.setMail(aDB.retrieveMail(playerOid));
	        Log.debug("MAIL: loaded " + iInfo.getMail().size() + " pieces of mail for player: " + playerOid);
	        loadMailItems(playerOid);
	        // Send down currencies
	        //ExtendedCombatMessages.sendCurrencies(playerOid, iInfo.getCurrentCurrencies());
	        
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            return true;
        }
    }

    // Log the logout information and send a response
    class LogoutHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LogoutMessage message = (LogoutMessage) msg;
            OID playerOid = message.getSubject();
            Log.debug("LOGOUT: inventory logout started for: " + playerOid);
            //Log.debug("LogoutHook: playerOid=" + playerOid);
            // Remove the entry from the hashmap
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            Log.debug("LOGOUT: inventory logout finished for: " + playerOid);
            return true;
        }
    }
    
    /*
     * Mail Hooks/Functions
     */
    
    /**
     * Handles the GetMailMessage. Calls sendMailList.
     *
     */
    class GetMailHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage getMsg = (ExtensionMessage)msg;
	        OID oid = getMsg.getSubject();
	        sendMailList(oid);
	        return true;
	    }
    }
    
    /**
     * Handles the MailReadMessage. Sets the specified piece of mail's read property
     * to true.
     * @author Andrew
     *
     */
    class MailReadHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage getMsg = (ExtensionMessage)msg;
	        OID oid = getMsg.getSubject();
	        int mailID = (Integer) getMsg.getProperty("mailID");
	        InventoryInfo iInfo = getInventoryInfo(oid);
	        ArrayList<Mail> mailList = iInfo.getMail();
	        Mail m = getMailByID(mailList, mailID);
	        Log.debug("Setting mail read with mailID: " + mailID);
	        m.setMailRead(true);
	        iInfo.setMail(mailList);
	        
	        // Put Read token in DB JSChasle
	        aDB.readMail(mailID);
	        
	        //WorldManagerClient.sendObjChatMsg(oid, 2, "Mail: " + mailID + " marked as read.");
	        sendMailList(oid);
	        return true;
	    }
    }
    
    /**
     * Handles the TakeMailItemMessage. Takes the item attached to the specified
     * piece of mail and puts it in the players inventory.
     * @author Andrew Harrison
     *
     */
    class TakeMailItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage getMsg = (ExtensionMessage)msg;
	        OID oid = getMsg.getSubject();
	        int mailID = (Integer) getMsg.getProperty("mailID");
	        int itemPos = (Integer) getMsg.getProperty("itemPos");
	        InventoryInfo iInfo = getInventoryInfo(oid);
	        Log.debug("MAIL: got take mail item with mailID: " + mailID);
	        ArrayList<Mail> mailList = iInfo.getMail();
	        Mail m = getMailByID(mailList, mailID);
	        Log.debug("MAIL: got take mail item with mailID: " + mailID);
	        if (itemPos == -1) {
	        	// We are dealing with currency
	        	alterMobCurrency(oid, m.getCurrencyType(), m.getCurrencyAmount());
	        	m.setCurrencyAmount(0);
	        	iInfo.setMail(mailList);
	        	aDB.takeMailCurrency(mailID);
	        	sendMailList(oid);
		        sendBagInvUpdate(oid);
		        return true;
	        } else if (m.getCoD()) {
	        	// Make sure the user has enough currency
	        	Currency c = Agis.CurrencyManager.get(m.getCurrencyType());
	        	if (c == null) {
	        		ExtendedCombatMessages.sendErrorMessage(oid, "Invalid Currency");
		        	return true;
	        	}
	        	if (getMobCurrency(oid, m.getCurrencyType()) < m.getCurrencyAmount()) {
	        		ExtendedCombatMessages.sendErrorMessage(oid, "You do not have enough " + c.getCurrencyName() + " to pay the CoD");
		        	return true;
	        	}
	        }
	        OID itemOID = m.getItems().get(itemPos);
	        AgisItem item = getAgisItem(itemOID);
	        Log.debug("MAIL: adding item: " + item);
	        if (item == null) {
	        	//TODO: handle this error
	        	Log.error("Mail item is null from mail: " + mailID);
	        	return true;
	        }
	        boolean itemAdded = addItem(oid, oid, item.getOid());
	        if (itemAdded) {
	        	Log.debug("MAIL: taken item: " + item);
	        	m.itemTaken(itemPos);
	        	iInfo.setMail(mailList);
	        	aDB.takeMailItem(mailID, itemPos, m.getCoD());
	        	if (m.getCoD()) {
	        		// Send mail to original sender with the currency amount
	        		String message = m.getRecipientName() + " has accepted your CoD request. Your payment is attached.";
	        		createAndSendMail(m.getRecipientOID(), m.getSenderOID(), m.getSenderName(), "CoD Payment", message,
	        				new ArrayList<OID>(), m.getCurrencyType(), m.getCurrencyAmount(), false);
	        		// Set the CoD flag to false and set currency to 0
	        		m.setCoD(false);
	        		m.setCurrencyAmount(0);
	        	}
	        }
	        
	        sendMailList(oid);
	        sendBagInvUpdate(oid);
	        return true;
	    }
    }
    
    /**
     * Handles the DeleteMailMessage. Deletes the specified piece of mail from
     * the players mailbox.
     * @author Andrew
     *
     */
    class ReturnMailHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage getMsg = (ExtensionMessage)msg;
	        OID oid = getMsg.getSubject();
	        int mailID = (Integer) getMsg.getProperty("mailID");
	        InventoryInfo iInfo = getInventoryInfo(oid);
	        ArrayList<Mail> mailList = iInfo.getMail();
	        Mail m = getMailByID(mailList, mailID);
	        Log.debug("Setting mail deleted with mail: " + mailID);
	        
	        // Delete the Mail (or set status to delete) in DB JSChasle
	        aDB.returnMail(m.getID(), m.getSenderOID(), m.getSenderName(), m.getRecipientOID(), 
	        		m.getRecipientName(), m.getSubject(), m.getCoD());
	        
	        mailList.remove(m);
	        iInfo.setMail(mailList);
	        sendMailList(oid);
	        return true;
	    }
    }
    
    /**
     * Handles the DeleteMailMessage. Deletes the specified piece of mail from
     * the players mailbox.
     * @author Andrew
     *
     */
    class DeleteMailHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage getMsg = (ExtensionMessage)msg;
	        OID oid = getMsg.getSubject();
	        int mailID = (Integer) getMsg.getProperty("mailID");
	        InventoryInfo iInfo = getInventoryInfo(oid);
	        ArrayList<Mail> mailList = iInfo.getMail();
	        Mail m = getMailByID(mailList, mailID);
	        Log.debug("Setting mail deleted with mail: " + mailID);
	        
	        // Delete the Mail (or set status to delete) in DB JSChasle
	        aDB.deleteMail(mailID);
	        
	        mailList.remove(m);
	        iInfo.setMail(mailList);
	        sendMailList(oid);
	        return true;
	    }
    }
    
    /**
     * Handles the SendMailMessage. Sends a piece of mail from the player to the
     * specified recipient.
     * @author Andrew
     *
     */
    class SendMailHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage sendMsg = (ExtensionMessage)msg;
            OID senderOid = sendMsg.getSubject();
            Log.debug("MAIL: got send mail");
	        String recipientName = (String) sendMsg.getProperty("recipient");
	        OID recipientOid = null;
	        //Need to check into the DB if the recipientName exist JSChasle (Done)
	        recipientOid = Engine.getDatabase().getOidByName(recipientName, WorldManagerClient.NAMESPACE);	        
	        if (recipientOid == null || recipientOid.equals(0l)) {
	        	ExtendedCombatMessages.sendErrorMessage(senderOid, "No character called '" + recipientName + "' exists.");
	        	return true;
	        }
	        Log.debug("MAIL: got valid recipient");
	        String subject = (String) sendMsg.getProperty("subject");
	        String message = (String) sendMsg.getProperty("message");
	        int numItems = (Integer) sendMsg.getProperty("numItems");
	        ArrayList<OID> items = new ArrayList<OID>();
	        for (int i = 0; i < numItems; i++) {
	        	items.add((OID) sendMsg.getProperty("item" + i));
	        }
	        Log.debug("MAIL: handled items");
	        
	        int numCurrencies = (Integer) sendMsg.getProperty("numCurrencies");
	        int currencyType = -1;
	        int currencyAmount = 0;
	        for (int i = 0; i < numCurrencies; i++) {
	        	int cType = (Integer) sendMsg.getProperty("currencyType" + i);
		        int amount = (Integer) sendMsg.getProperty("currencyAmount" + i);
		        Currency c = Agis.CurrencyManager.get(cType);
		        while (c.getCurrencyThatConvertsToThis() != null) {
		        	c = c.getCurrencyThatConvertsToThis();
		        	amount *= c.getConversionAmountReq();
		        }
		        currencyAmount += amount;
		        currencyType = c.getCurrencyID();
	        }
	        
	        boolean CoD = (Boolean) sendMsg.getProperty("CoD");
	        Log.debug("MAIL: handled currency with type: " + currencyType + " and amount: " + currencyAmount);
	        //OID mailOID = Engine.getOIDManager().getNextOid();
	        String result = createAndSendMail(senderOid, recipientOid, recipientName, subject, message, items, currencyType, currencyAmount, CoD);
        	// Send a message to the sender so the client knows the result of the sending
	        Map<String, Serializable> props = new HashMap<String, Serializable>();
        	props.put("ext_msg_subtype", "MailSendResult");
        	props.put("result", result);
        	TargetedExtensionMessage successMsg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, senderOid, 
				senderOid, false, props);
        	Engine.getAgent().sendBroadcast(successMsg);
	        
	        if (result.equals("Success")) {
	        	Log.debug("MAIL: mail sent");
	        } else {
	        	Log.warn("MAIL: mail was not sent to player: " + recipientOid);
	        }
			
	        return true;
	    }
    }
    
    /**
     * Creates a new mail item from the parameters given and sends it to the recipient. The created mail object
     * is saved to the database.
     * @param senderOid
     * @param recipientOid
     * @param recipientName
     * @param subject
     * @param message
     * @param items
     * @param currencyType
     * @param currencyAmount
     * @param CoD
     * @return
     */
    private String createAndSendMail(OID senderOid, OID recipientOid, String recipientName, String subject, String message, 
    		ArrayList<OID> items, int currencyType, int currencyAmount, boolean CoD) {
    	String senderName = WorldManagerClient.getObjectInfo(senderOid).name;
    	// Do a currency check first
    	if (currencyAmount > 0 && !CoD) {
        	Currency c = Agis.CurrencyManager.get(currencyType);
        	if (c == null) {
        		ExtendedCombatMessages.sendErrorMessage(senderOid, "Invalid Currency");
	        	return "Invalid Currency";
        	}
        	if (getMobCurrency(senderOid, currencyType) < currencyAmount) {
        		ExtendedCombatMessages.sendErrorMessage(senderOid, "You cannot send more " + c.getCurrencyName() + " than you have");
	        	return "Not enough Currency";
        	}
        }
        Log.debug("MAIL: creating mail");
    	Mail m = new Mail(-1, recipientOid, recipientName, senderOid, senderName, subject,
        		message, currencyType, currencyAmount, items, 1, CoD);
        aDB.addNewMail(m);
        
        // Check items are not bound to the player
        for(OID itemOid : items) {
        	if (itemOid == null) {
        		continue;
        	}
        	AgisItem item = getAgisItem(itemOid);
    	    if (item == null) {
    	    	return "Invalid Item";
    	    }
    	    if (!item.canBeTraded()) {
    	    	return "Cannot send bound Item";
    	    }
        }
        
        // Remove items and currency from senders inventory
        for(OID itemOid : items) {
        	if (itemOid != null) {
        		removeItem(senderOid, itemOid, true);
        	}
        }
        if (currencyType > 0 && currencyAmount > 0 && !CoD) {
        	alterMobCurrency(senderOid, currencyType, -currencyAmount);
        }
        
        Log.debug("MAIL: created mail object");
        InventoryInfo iInfo = getInventoryInfo(recipientOid);
        if (iInfo != null) {
        	iInfo.addMail(m);
	        WorldManagerClient.sendObjChatMsg(recipientOid, 2, "You have recieved new mail from " + senderName);
        }
        
        sendBagInvUpdate(senderOid);
        return "Success";
    }
    
    /**
     * Handles the SendPurchaseMailMessage. Creates a new mail object and sends 
     * it to the buyer.
     *
     */
    class SendPurchaseMailHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        AgisInventoryClient.sendPurchaseMailMessage getMsg = (AgisInventoryClient.sendPurchaseMailMessage)msg;
	        OID recipientOID = getMsg.getSubject();
	        String subject = "Item Shop Purchase";
	        String message = "Thank you for shopping at the Item Shop. Your purchase has been included in this mail.";
	        int itemID = (Integer) getMsg.getProperty("itemID");
	        Template tmpl = ObjectManagerClient.getTemplate(itemID, ObjectManagerPlugin.ITEM_TEMPLATE);
	        String itemName = tmpl.getName();
	        OID itemOID = generateItem(itemID, itemName);
	        //OID mailOID = Engine.getOIDManager().getNextOid();
	        String senderName = "The Item Shop";
	        int currencyType = 0;
	        int currencyAmount = 0;
	        ArrayList<OID> items = new ArrayList<OID>();
	        if (!itemOID.equals(0l)) {
	        	items.add(itemOID);
	        }
	        
	        boolean CoD = false;
	        Mail m = new Mail(-1, recipientOID, "", null, senderName, subject,
	        		message, currencyType, currencyAmount, items, 1, CoD);
	        InventoryInfo iInfo = getInventoryInfo(recipientOID);
	        iInfo.addMail(m);
	        sendMailList(recipientOID);
	        WorldManagerClient.sendObjChatMsg(recipientOID, 2, "You have recieved new mail from " + senderName);
	        return true;
	    }
    }
    
    /**
     * Simple function used to get a piece of mail from the list of mail items by
     * its ID.
     * @param mailList
     * @param mailID
     * @return
     */
    private Mail getMailByID(ArrayList<Mail> mailList, int mailID) {
    	for (Mail m : mailList) {
    		if (m.getID() == mailID)
    			return m;
    	}
    	return null;
    }
    
    /**
     * Sends down the list of mail a player has to their client.
     * @param playerOid
     */
    private void sendMailList(OID playerOid) {
    	// Check external mail database
    	aDB.checkCharacterPurchases(playerOid);
    	
    	InventoryInfo iInfo = getInventoryInfo(playerOid);
    	ArrayList<Mail> mailList = iInfo.getMail();
    	
        Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "MailList");
    	props.put("numMail", mailList.size());
        for (int pos = 0; pos < mailList.size(); pos++) {
        	Mail m = mailList.get(pos);
        	props.put("mail_" + pos + "ID", m.getID());
        	props.put("mail_" + pos + "SenderOid", m.getSenderOID());
        	props.put("mail_" + pos + "SenderName", m.getSenderName());
        	props.put("mail_" + pos + "Subject", m.getSubject());
        	props.put("mail_" + pos + "Message", m.getMessage());
        	props.put("mail_" + pos + "Read", m.getMailRead());
        	for (int i = 0; i < m.getItems().size(); i++) {
        		if (m.getItems().get(i) == null) {
        			props.put("mail_" + pos + "ItemTemplate" + i, -1);
                	props.put("mail_" + pos + "ItemName" + i, "");
                    props.put("mail_" + pos + "ItemCount" + i, 0);
                } else {
                	AgisItem item = getAgisItem(m.getItems().get(i));
                	props.put("mail_" + pos + "ItemTemplate" + i, item.getTemplateID());
                	props.put("mail_" + pos + "ItemName" + i, item.getName());
                    props.put("mail_" + pos + "ItemCount" + i, item.getStackSize());
                }
        	}
        	props.put("mail_" + pos + "NumItems", m.getItems().size());
        	props.put("mail_" + pos + "CurrencyType", m.getCurrencyType());
        	props.put("mail_" + pos + "CurrencyAmount", m.getCurrencyAmount());
        	props.put("mail_" + pos + "CoD", m.getCoD());
        }
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    /**
     * Hook for the CheckComponentMessage. Checks if the player has all the specified items 
     * in their inventory. Sends a Boolean response.
     * @author Andrew Harrison
     *
     */
    class CheckComponentHook implements Hook {
    	public boolean processMessage(Message arg0, int arg1) {	
    		CheckComponentMessage msg = (CheckComponentMessage)arg0;	
    		
    		// Group together the requirements
    		if (msg.gridSystem) {
    			HashMap<Integer, Integer> requirements = new HashMap<Integer, Integer>();
    			HashMap<Integer, Integer> itemTotals = new HashMap<Integer, Integer>();
    			for (int i = 0; i < msg._reqComponents.size(); i++) {
    				int reqComponent = msg._reqComponents.get(i);
    				int reqCount = msg._reqStackSizes.get(i);
    				if (!requirements.containsKey(reqComponent)) {
    					requirements.put(reqComponent, reqCount);
    					Log.debug("CHECK: set required itemID " + reqComponent + " to count " + reqCount);
    				} else {
    					reqCount += requirements.get(reqComponent);
    					requirements.put(reqComponent, reqCount);
    					Log.debug("CHECK: set required itemID " + reqComponent + " to count " + reqCount);
    				}
    				itemTotals.put(reqComponent, 0);
    			}
    		
    			HashMap<Integer, Integer> provided = new HashMap<Integer, Integer>();
    			HashMap<Long, Integer> providedIds = new HashMap<Long, Integer>();
    			for (int i = 0; i < msg._components.size(); i++) {
    				OID component = OID.fromLong(msg._components.get(i));
    				int providedItemID = getAgisItem(component).getTemplateID();
    				int count = msg._componentCounts.get(i);
    				if (!provided.containsKey(providedItemID)) {
    					provided.put(providedItemID, count);
    					providedIds.put(msg._components.get(i), count);
    					Log.debug("CHECK: set provided itemID " + providedItemID + " to count " + count);
    				} else {
    					count += provided.get(providedItemID);
    					provided.put(providedItemID, count);
    					providedIds.put(msg._components.get(i), count);
    					Log.debug("CHECK: set provided itemID " + providedItemID + " to count " + count);
    				}
    			}
    			
    			// Now verify the user has the items they said they did
    			for (Long providedId : providedIds.keySet()) {
    				OID component = OID.fromLong(providedId);
    				AgisItem tempItem = getAgisItem(component);
    				//Log.debug("CHECK: stackSize " + tempItem.getStackSize() + " for item: " + component + " with count: " + providedIds.get(providedId));
    				itemTotals.put(tempItem.getTemplateID(), itemTotals.get(tempItem.getTemplateID()) + tempItem.getStackSize());
    			}
    		
    			// Check each required item to see if the provided count matches
    			for (Integer itemID : requirements.keySet()) {
    				if (!provided.containsKey(itemID)) {
    					Log.debug("CHECK: itemID " + itemID + " was not found in the provided list");
    					Engine.getAgent().sendObjectResponse(msg, false);	
    					return true;	
    				} else if (provided.get(itemID) < requirements.get(itemID)) {
    					Log.debug("CHECK: itemID " + itemID + " required " + requirements.get(itemID) + " but player only provided " + provided.get(itemID));
    					Engine.getAgent().sendObjectResponse(msg, false);	
    					return true;
    				} else if (itemTotals.get(itemID) < requirements.get(itemID)) {
    					Log.debug("CHECK: itemID " + itemID + " required " + requirements.get(itemID) + " but player only had " + itemTotals.get(itemID));
    					Engine.getAgent().sendObjectResponse(msg, false);	
    					return true;
    				}
    			}
    			
    		} else {
    			ArrayList<Integer> test = new ArrayList<Integer>();
        		test.addAll(msg._reqComponents); 
        		HashMap<Integer, Integer> requirements = new HashMap<Integer, Integer>();
    			for (int i = 0; i < msg._reqComponents.size(); i++) {
    				int reqComponent = msg._reqComponents.get(i);
    				int reqCount = msg._reqStackSizes.get(i);
    				if (!requirements.containsKey(reqComponent)) {
    					requirements.put(reqComponent, reqCount);
    					Log.debug("CHECK: set required itemID " + reqComponent + " to count " + reqCount);
    				} else {
    					reqCount += requirements.get(reqComponent);
    					requirements.put(reqComponent, reqCount);
    					Log.debug("CHECK: set required itemID " + reqComponent + " to count " + reqCount);
    				}
    			}
    			
    			for (int itemID : requirements.keySet()) {
    				int count = requirements.get(itemID);
    				int amountFound = 0;
    				// Check if there are any existing stacks to add to
    	            ArrayList<OID> existingOids = findItemStacks(msg._subject, itemID);
    	            if (existingOids.size() > 0) {
    	            	Log.debug("CHECK: user has item " + itemID);
    	            	for (OID existingOid: existingOids) {
    	            		AgisItem tempItem = getAgisItem(existingOid);
    	            		Log.debug("CHECK: increasing amountFound for item: " + existingOid);
    	            		amountFound += tempItem.getStackSize();
    	            	}
    	            }
    	            
    	            if (amountFound < count) {
    	            	Log.debug("CHECK: did not find enough of the required item");
    	            	Engine.getAgent().sendObjectResponse(msg, false);
        				return true;
    	            }
    			}
        		/*Log.debug("CHECK: checking for items: " + test.toString());
    			List<OID> itemList = InventoryClient.findItems(msg._subject, test);
    			Log.debug("CHECK: found items: " + itemList.toString());
    			if(itemList == null || itemList.contains(null)){
    				Engine.getAgent().sendObjectResponse(msg, false);
    				return true;
    			}*/
    		}
    		Log.debug("CHECK: passed item check");
    		Engine.getAgent().sendObjectResponse(msg, true);	
    		return true;	
    	}
    }  
    
    /**
     * Don't use.
     * @author Andrew
     *
     */
    class UseAccountItemHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
	        /*ExtensionMessage eMsg = (ExtensionMessage)msg;
	        OID playerOid = eMsg.getSubject();
	        int itemID = (Integer) eMsg.getProperty("itemID");
	        Template itemTemplate = ObjectManagerClient.getTemplate(itemID, ObjectManagerPlugin.ITEM_TEMPLATE);*/
	        // 
	        return true;
	    }
    }

    /**
     * Helper class used to track what item can be equipped in what slot.
     * @author Andrew
     *
     */
    public static class EquipMap implements Serializable {

        public EquipMap() {
        }
        
        /**
         * returns the slot for item, can return null
         */
        public AgisEquipSlot getSlot(OID itemOid) {
            for (Map.Entry<AgisEquipSlot, OID> entry : map.entrySet()) {
                OID oItemOid = entry.getValue();
                if (oItemOid.equals(itemOid)) {
                    if (Log.loggingDebug)
                        log.debug("EquipMap.getSlot: found item=" + itemOid + " slot=" + entry.getKey());
                    return entry.getKey();
                }
            }
            if (Log.loggingDebug)
                log.debug("EquipMap.getSlot: item=" + itemOid + " slot=null");
            return null;
        }
        
        public OID get(AgisEquipSlot slot) {
            return map.get(slot);
        }
        
        public void put(AgisEquipSlot slot, OID longVal) {
        	Log.debug("SLOT: putting slot: " + slot);
            map.put(slot, longVal);
        }
        
        public void remove(AgisEquipSlot slot) {
        	Log.debug("SLOT: removing slot: " + slot);
            map.remove(slot);
        }

        public boolean containsValue(OID itemOid) {
            return map.containsValue(itemOid);
        }

        public HashMap<AgisEquipSlot, OID> getEquipMap() {
            return map;
        }

        public void setEquipMap(HashMap<AgisEquipSlot, OID> map) {
            this.map = map;
        }
        
        HashMap<AgisEquipSlot, OID> map = new HashMap<AgisEquipSlot, OID>();
        private static final long serialVersionUID = 1L;
    }

    /**
     * Gets the EquipMap for the player/mob.
     * @param activatorOid
     * @return
     */
    public EquipMap getEquipMap(OID activatorOid) {
        lock.lock();
        try {
        	InventoryInfo subObj = getInventoryInfo(activatorOid);
        	EquipMap map = (EquipMap) subObj.getProperty(EQUIP_MAP_PROP);
            if (map == null) {
                map = new EquipMap();
                subObj.setProperty(EQUIP_MAP_PROP, map);
                Engine.getPersistenceManager().setDirty(subObj);
            }
            return map;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Sets the InventoryInfo for the player to dirty causing it to be saved to the
     * internal object database.
     * @param mobOid
     */
    public void setDirty(OID mobOid) {
    	InventoryInfo subObj = getInventoryInfo(mobOid);
    	Engine.getPersistenceManager().setDirty(subObj);
    }

    // map (cache) of players base dc
    Map<OID, DisplayContext> baseDCMap = new HashMap<OID, DisplayContext>();
    public static final String EQUIP_MAP_PROP = "equipMap";
    
    public enum ItemAcquireResult {
    	SUCCESS,
    	INSUFFICIENT_CURRENCY,
    	STACK_LIMIT_REACHED,
    	ITEM_DOESNT_EXIST,
    	UNKNOWN_FAILURE
    }
    
    /**
     * Maps and constants
     */
    
    // Item Weights
    public static HashMap<String, Float> equipmentSlots = new HashMap<String, Float>();
    public static ArrayList<ItemWeight> itemPrefixes = new ArrayList<ItemWeight>();
    public static ArrayList<ItemWeight> itemSuffixes = new ArrayList<ItemWeight>();
    public static Random random = new Random();
    
    public static float getSlotModifier(String slot) {
    	if (slot.equals("Head"))
    		return 0.28f;
    	else if (slot.equals("Shoulder"))
    		return 0.14f;
    	else if (slot.equals("Chest"))
    		return 0.28f;
    	else if (slot.equals("Hands"))
    		return 0.14f;
    	else if (slot.equals("Waist"))
    		return 0.16f;
    	else if (slot.equals("Legs"))
    		return 0.27f;
    	else if (slot.equals("Feet"))
    		return 0.14f;
    	else if (slot.equals("Back"))
    		return 0.15f;
    	else if (slot.equals("Neck"))
    		return 0.16f;
    	else if (slot.equals("Ring"))
    		return 0.14f;
    	else if (slot.equals("Main Hand"))
    		return 0.25f;
    	else if (slot.equals("Off Hand"))
    		return 0.25f;
    	else if (slot.equals("Any Hand"))
    		return 0.25f;
    	else if (slot.equals("Two Hand"))
    		return 0.50f;
    	return 0.0f;
    }
    
    public static float getGradeModifier(int grade) {
    	if (grade == 1) 
    		return 0.667f;
    	else if (grade == 2) 
    		return 0.8f;
    	else if (grade == 3) 
    		return 1.0f;
    	else if (grade == 4) 
    		return 1.25f;
    	else if (grade == 5) 
    		return 1.55f;
    	else if (grade == 6) 
    		return 1.95f;
    	else
    		return 0.5f;
    }
    
    public static float getStatModifier(String statName) {
    	/*for (int i = 0; i < attributes.length; i++) {
    		if (attributes[i].equals(statName))
    			return 1.0f;
    	}
    	for (int i = 0; i < resists.length; i++) {
    		if (resists[i].equals(statName))
    			return 0.5f;
    	}
    	if (statName.equals("damage"))
    		return 2.0f;*/
    	return 1.0f;
    }
    
    public static List<Float> getArmourTypeModifier(String armourType) {
    	List<Float> armourModifiers = new LinkedList<Float>();
    	if (armourType.equals("Silk")) {
    		armourModifiers.add(0.1f);
    	    armourModifiers.add(0.9f);
    	} else if (armourType.equals("Wool")) {
    		armourModifiers.add(0.2f);
    	    armourModifiers.add(0.8f);
    	} else if (armourType.equals("Soft Leather")) {
    		armourModifiers.add(0.3f);
    	    armourModifiers.add(0.7f);
    	} else if (armourType.equals("Hard Leather")) {
    		armourModifiers.add(0.4f);
    	    armourModifiers.add(0.6f);
    	} else if (armourType.equals("Chain")) {
    		armourModifiers.add(0.5f);
    	    armourModifiers.add(0.5f);
    	} else if (armourType.equals("Scale")) {
    		armourModifiers.add(0.6f);
    	    armourModifiers.add(0.4f);
    	} else if (armourType.equals("Branded")) {
    		armourModifiers.add(0.7f);
    	    armourModifiers.add(0.3f);
    	} else if (armourType.equals("Plate")) {
    		armourModifiers.add(0.8f);
    	    armourModifiers.add(0.2f);
    	} else {
    		armourModifiers.add(0.5f);
    	    armourModifiers.add(0.5f);
    	}

    	return armourModifiers;
    }
    
    public static List<Float> getArmourSlotModifier(String armourSlot) {
    	List<Float> armourModifiers = new LinkedList<Float>();
    	if (armourSlot.equals("Head")) {
    		armourModifiers.add(0.12f);
    	    armourModifiers.add(0.11f);
    	} else if (armourSlot.equals("Chest")) {
    		armourModifiers.add(0.22f);
    	    armourModifiers.add(0.06f);
    	} else if (armourSlot.equals("Legs")) {
    		armourModifiers.add(0.21f);
    	    armourModifiers.add(0.06f);
    	} else if (armourSlot.equals("Hands")) {
    		armourModifiers.add(0.11f);
    	    armourModifiers.add(0.03f);
    	} else if (armourSlot.equals("Feet")) {
    		armourModifiers.add(0.11f);
    	    armourModifiers.add(0.03f);
    	} else if (armourSlot.equals("Shoulder")) {
    		armourModifiers.add(0.11f);
    	    armourModifiers.add(0.03f);
    	} else if (armourSlot.equals("Waist")) {
    		armourModifiers.add(0.02f);
    	    armourModifiers.add(0.14f);
    	} else if (armourSlot.equals("Back")) {
    		armourModifiers.add(0.05f);
    	    armourModifiers.add(0.10f);
    	} else if (armourSlot.equals("Neck")) {
    		armourModifiers.add(0.00f);
    	    armourModifiers.add(0.16f);
    	} else if (armourSlot.equals("Ring")) {
    		armourModifiers.add(0.00f);
    	    armourModifiers.add(0.14f);
    	} else {
    		armourModifiers.add(0.05f);
    	    armourModifiers.add(0.05f);
    	}

    	return armourModifiers;
    }
    
    public static int INVENTORY_BAG_COUNT = 4;
    public int INVENTORY_FIRST_BAG_SIZE = 16;
    public int INVENTORY_OTHER_BAG_SIZE = 0;
    public int EQUIP_SLOTS_COUNT = 15;
    public static int MAIL_ATTACHMENT_COUNT = 10;
    public static int MAIL_LIFE_DAYS = 30;
    public static int MAIL_COD_LIFE_DAYS = 3;
    public static float SELL_FACTOR = 4f; // Alters the amount you get for selling an item (item value / sellFactor)
    public static float REPAIR_RATE = 0.5f; // Alters the cost for repairing an item (item value / repairFactor)
    public int PLAYER_CORPSE_LOOT_DURATION = 0; // How long a lootable corpse of the player exists for
    public int PLAYER_CORPSE_SAFE_LOOT_DURATION = 0; // How long the corpse is only lootable by the player themselves
    public int PLAYER_CORPSE_MOB_TEMPLATE = 0;
    public boolean PLAYER_CORPSE_DROPS_EQUIPMENT = true;
    public int DURABILITY_LOSS_CHANCE_FROM_ATTACK = 10; // Out of 100
    public int DURABILITY_LOSS_CHANCE_FROM_DEFEND = 5;
    public int DURABILITY_LOSS_CHANCE_FROM_GATHER = 50;
    public int DURABILITY_LOSS_CHANCE_FROM_CRAFT = 50;
    
    boolean USE_FLAT_REPAIR_RATE = false;
    
    public static AccountDatabase aDB;

    static final Logger log = new Logger("AgisInventoryPlugin");
}
