package atavism.agis.plugins;

import atavism.agis.behaviors.*;
import atavism.agis.core.Agis;
import atavism.agis.database.AccountDatabase;
import atavism.agis.database.ContentDatabase;
import atavism.agis.database.ItemDatabase;
import atavism.agis.database.MobDatabase;
import atavism.agis.objects.*;
import atavism.agis.objects.AgisBasicQuest.CollectionGoal;
import atavism.agis.objects.AgisBasicQuest.KillGoal;
import atavism.agis.objects.Currency;
import atavism.agis.plugins.AgisMobClient.GetInstanceTemplateMessage;
import atavism.agis.plugins.GroupClient.GroupInfo;
import atavism.agis.util.EventMessageHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.agis.util.HelperFunctions;
import atavism.msgsys.*;
import atavism.server.engine.*;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.objects.*;
import atavism.server.plugins.*;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.ObjectInfo;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.*;

public class AgisMobPlugin extends MobManagerPlugin {

    /*
     * Properties
     */
    
    //protected static final Logger _log = new Logger("AgisMobPlugin");

    /*
     * Events
     */ 
    
    public void onActivate() {
        //super.onActivate();

        // register message hooks
    	getHookManager().addHook(AgisMobClient.MSG_TYPE_GET_INSTANCE_TEMPLATE,
        		new GetInstanceTemplateHook());
    	getHookManager().addHook(AgisMobClient.MSG_TYPE_SPAWN_INSTANCE_MOBS,
        		new SpawnInstanceMobsHook());
    	getHookManager().addHook(AgisMobClient.MSG_TYPE_SPAWN_MOB,
        		new SpawnMobHook());
    	getHookManager().addHook(AgisMobClient.MSG_TYPE_SPAWN_ARENA_CREATURE,
        		new SpawnArenaCreatureHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_SPAWN_PET,
        		new SpawnPetHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_TAME_BEAST,
                new TameBeastHook());
        // Hooks to process login/logout messages
        getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
 	           new LoginHook());
        getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT,
 	           new LogoutHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_SPAWNED,
				 new SpawnedHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED,
				 new DespawnedHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_INTERACT_WITH_OBJECT,
                new InteractWithObjectHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_GET_TEMPLATES,
        	   new GetTemplatesHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CREATE_MOB_SPAWN,
         	   new CreateMobSpawnHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CREATE_MOB,
          	   new CreateMobHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CREATE_FACTION,
           	   new CreateFactionHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CREATE_QUEST,
                new CreateQuestHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CREATE_LOOT_TABLE,
                new CreateLootTableHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_GET_ISLANDS_DATA,
                new GetIslandsHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_VERIFY_ISLAND_ACCESS,
                new VerifyIslandAccessHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_ENTER_WORLD,
                new EnterWorldHook());
        getHookManager().addHook(AgisWorldManagerClient.MSG_TYPE_CHANGE_INSTANCE, 
				new ChangeInstanceHook());
        getHookManager().addHook(InstanceClient.MSG_TYPE_LOAD_INSTANCE_BY_ID,
                new LoadIslandHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_REQUEST_DEVELOPER_ACCESS,
                new RequestIslandDeveloperAccessHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_CREATE_ISLAND,
                new CreateIslandHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_VIEW_MARKERS,
                new ViewSpawnMarkersHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_REQUEST_SPAWN_DATA,
                new RequestSpawnDataHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_EDIT_SPAWN_MARKER,
                new EditMobSpawnHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_DELETE_SPAWN_MARKER,
                new DeleteSpawnMarkerHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_SPAWN_DOME_MOB,
                new SpawnDomeMobHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_USE_TRAP_DOOR,
                new UseTrapDoorHook());
        getHookManager().addHook(InstanceClient.MSG_TYPE_INSTANCE_LOADED,
        		new LoadInstanceObjectsHook());
        getHookManager().addHook(AgisMobClient.MSG_TYPE_PLAY_COORD_EFFECT,
                new PlayCoordinatedEffectHook());
        
        // setup message filters
        MessageTypeFilter filter = new MessageTypeFilter();
        filter.addType(WorldManagerClient.MSG_TYPE_SPAWNED);
        filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
        filter.addType(AgisMobClient.MSG_TYPE_SPAWN_INSTANCE_MOBS);
        filter.addType(AgisMobClient.MSG_TYPE_SPAWN_MOB);
        filter.addType(AgisMobClient.MSG_TYPE_SPAWN_ARENA_CREATURE);
        filter.addType(AgisMobClient.MSG_TYPE_SPAWN_PET);
        filter.addType(AgisMobClient.MSG_TYPE_TAME_BEAST);
        filter.addType(AgisMobClient.MSG_TYPE_INTERACT_WITH_OBJECT);
        filter.addType(AgisMobClient.MSG_TYPE_GET_TEMPLATES);
        filter.addType(AgisMobClient.MSG_TYPE_CREATE_MOB_SPAWN);
        filter.addType(AgisMobClient.MSG_TYPE_CREATE_MOB);
        filter.addType(AgisMobClient.MSG_TYPE_CREATE_FACTION);
        filter.addType(AgisMobClient.MSG_TYPE_CREATE_QUEST);
        filter.addType(AgisMobClient.MSG_TYPE_CREATE_LOOT_TABLE);
        filter.addType(AgisMobClient.MSG_TYPE_GET_ISLANDS_DATA);
        filter.addType(AgisMobClient.MSG_TYPE_VERIFY_ISLAND_ACCESS);
        filter.addType(AgisMobClient.MSG_TYPE_ENTER_WORLD);
        filter.addType(AgisWorldManagerClient.MSG_TYPE_CHANGE_INSTANCE);
        filter.addType(AgisMobClient.MSG_TYPE_REQUEST_DEVELOPER_ACCESS);
        filter.addType(AgisMobClient.MSG_TYPE_CREATE_ISLAND);
        filter.addType(AgisMobClient.MSG_TYPE_VIEW_MARKERS);
        filter.addType(AgisMobClient.MSG_TYPE_REQUEST_SPAWN_DATA);
        filter.addType(AgisMobClient.MSG_TYPE_EDIT_SPAWN_MARKER);
        filter.addType(AgisMobClient.MSG_TYPE_DELETE_SPAWN_MARKER);
        filter.addType(AgisMobClient.MSG_TYPE_SPAWN_DOME_MOB);
        filter.addType(AgisMobClient.MSG_TYPE_USE_TRAP_DOOR);
        filter.addType(InstanceClient.MSG_TYPE_INSTANCE_LOADED);
        filter.addType(AgisMobClient.MSG_TYPE_SET_BLOCK);
        filter.addType(AgisMobClient.MSG_TYPE_PLAY_COORD_EFFECT);
        // Account login message - no idea if it should go here
        filter.addType(ProxyPlugin.MSG_TYPE_ACCOUNT_LOGIN);
        Engine.getAgent().createSubscription(filter, this);
        
        //setup responder message filters
        MessageTypeFilter responderFilter = new MessageTypeFilter();
        responderFilter.addType(LogoutMessage.MSG_TYPE_LOGOUT);
        responderFilter.addType(LoginMessage.MSG_TYPE_LOGIN);
        responderFilter.addType(InstanceClient.MSG_TYPE_LOAD_INSTANCE_BY_ID);
        responderFilter.addType(AgisMobClient.MSG_TYPE_GET_INSTANCE_TEMPLATE);
        Engine.getAgent().createSubscription(responderFilter, this, MessageAgent.RESPONDER);
        
        
        // Load settings
        ContentDatabase cDB = new ContentDatabase(false);
        cDB.loadEditorOptions();
        
        String bagCount = cDB.loadGameSetting("MOB_DEATH_EXP");
        if (bagCount != null)
        	MOB_DEATH_EXP = Boolean.parseBoolean(bagCount);

        // Load content that belongs to every category
        mobDataBase = new MobDatabase(true);
        patrolPoints = mobDataBase.loadPatrolPathPoints();
        loadCategoryContent(mobDataBase, BASE_CATEGORY);
        createFactories();
    	//loadSkins(new ItemDatabase());
        // Spawn data is now only loaded when an instance is loaded
        /*if (loaded) {
        	loaded = loadSpawnData();
        }*/ 
    	//loadInstances();
        loadInstanceTemplates();
    }
    
    private void loadCategoryContent(MobDatabase mDB, int categoryID) {
    	Log.debug("MOB: loading content for category: " + categoryID);
    	ContentCategory cc = new ContentCategory(categoryID);
    	HashMap<Integer, AgisBasicQuest> questMap =  mDB.loadQuests(categoryID);
    	for (int questID : questMap.keySet()) {
    		Agis.QuestManager.register(questID, questMap.get(questID));
    	}
    	// Factions
    	ArrayList<Faction> factions = mDB.loadFactions(categoryID);
    	for (Faction faction: factions) {
    		Agis.FactionManager.register(faction.getID(), faction);
        	Log.debug("MOB: loaded faction: [" + faction.getName() + "]");
    	}
    	// Mob Templates
    	ArrayList<Template> mobTemplates = mDB.loadMobTemplates(categoryID);
    	for (Template tmpl: mobTemplates) {
        	ObjectManagerClient.registerTemplate(tmpl);
        	Log.debug("MOB: loaded template: [" + tmpl.getName() + "]");
    	}
    	// Currencies
    	ArrayList<Currency> currencies = mDB.loadCurrencies(categoryID);
    	for (Currency currency: currencies) {
    		Agis.CurrencyManager.register(currency.getCurrencyID(), currency);
        	Log.debug("MOB: loaded currency: [" + currency.getCurrencyName() + "]");
    	}
    	// Loot Tables
        HashMap<Integer, LootTable> lootTables = mDB.loadLootTables(-1);
        for (LootTable lTbl: lootTables.values()) {
        	Agis.LootTableManager.register(lTbl.getID(), lTbl);
            Log.debug("LOOT: loaded loot Table: [" + lTbl.getName() + "]");
        }
        // Dialogues
        dialogues = mDB.loadDialogues();
    	contentCategories.put(categoryID, cc);
    	
    	/**
         * Loads in the merchant tables from the Database.
         * @param iDB
         */
    	ItemDatabase iDB = new ItemDatabase(false);
        merchantTables = iDB.loadMerchantTables();
        iDB.close();
        Log.debug("NPC: merchant tables: " + merchantTables);
    }
    
    private void createFactories() {
    	Log.debug("BEHAV: creating factory for Dot");
    	MobFactory cFactory = new MobFactory(500);
		// Add some behavs
		cFactory.addBehav(new BaseBehavior());
		DotBehavior behav = new DotBehavior();
        behav.setRadius(1500);
        cFactory.addBehav(behav);
		String factoryName = "DotFactory";
		Log.debug("BEHAV: registering factory for Dot");
		ObjectFactory.register(factoryName, cFactory);
    }
    
    private void loadBuildingGrids(MobDatabase mDB, String instanceName, OID instanceOID) {
    	/*buildingGrids.putAll(mDB.loadBuildingGrids(instanceName));
    	for (BuildingGrid grid : buildingGrids.values()) {
    		if (grid.getBuildings() != null && grid.getInstance().equals(instanceName)) {
    			grid.spawnBuildings(instanceOID);
    		}
    	}*/
    	resourceGrids.putAll(mDB.loadResourceGrids(instanceName));
    	for (ResourceGrid grid : resourceGrids.values()) {
    		if (grid.getCount() > 0 && grid.getInstance().equals(instanceName)) {
    			grid.spawnResource(instanceOID);
    		}
    	}
    }
    
    /*private void loadSkins(ItemDatabase iDB) {
    	skins = iDB.loadSkins();
    }*/
    
    /**
     * Reads in spawn data from the database. 
     */
    /*private boolean loadSpawnData() {
    	Log.debug("MOB: about to load in spawn data from the database");
    	MobDatabase mDB = new MobDatabase();
    	spawnInfo = mDB.loadSpawnData();
    	for (int spawnNum: spawnInfo.keySet()) {
    		SpawnData sd = spawnInfo.get(spawnNum);
    		String instanceType = sd.getStringProperty("instance");
    		if (instanceType != null) {
    			LinkedList<Integer> spawnInts;
    			if (instanceSpawns.get(instanceType) == null)
    				spawnInts = new LinkedList<Integer>();
    			else 
    				spawnInts = instanceSpawns.get(instanceType);
    			spawnInts.add(spawnNum);
    			instanceSpawns.put(instanceType, spawnInts);
    		}
    	}
    	Log.debug("MOB: finished loading in spawn data from the database");
    	return true;
    }*/
    
    /**
     * Reads in instance template data from the database. If the instance is set to be created on
     * server startup it will call loadInstance.
     */
    private void loadInstanceTemplates() {
    	Log.debug("MOB: about to load in island data from the database");
    	AccountDatabase aDB = new AccountDatabase();
    	instanceTemplates = aDB.loadInstanceTemplateData();
    	for (InstanceTemplate island : instanceTemplates.values()) {
    		Template tmpl = new Template(island.getName(), island.getID(), null);
    		tmpl.put(InstanceClient.NAMESPACE, InstanceClient.TEMPL_INSTANCE_TEMPLATE_NAME, tmpl.getName());
    		tmpl.put(InstanceClient.NAMESPACE, InstanceClient.TEMPL_INSTANCE_TEMPLATE_ID, island.getID());
			//tmpl.put(InstanceClient.NAMESPACE, InstanceClient.TEMPL_WORLD_FILE_NAME, island.getWorldFileName());
			tmpl.put(InstanceClient.NAMESPACE, InstanceClient.TEMPL_POPULATION_LIMIT, island.getPopulationLimit());
			tmpl.put(InstanceClient.NAMESPACE, "instanceType", island.getIslandType());
			tmpl.put(InstanceClient.NAMESPACE, "createOnStartup", island.getCreateOnStartup());
    		InstanceClient.registerInstanceTemplate(tmpl);
        	boolean createOnLoad = (Boolean) tmpl.get(InstanceClient.NAMESPACE, "createOnStartup");
        	int instanceType = (Integer) tmpl.get(InstanceClient.NAMESPACE, "instanceType");
        	// Read spawn data from the database
        	island.setSpawns(mobDataBase.loadInstanceSpawnData(island.getID()));
        	// Create if it is set to create on load
        	if (createOnLoad == true) {
        		loadInstance(tmpl.getTemplateID(), tmpl.getName(), instanceType, null);
        	}
        	
    	}
    	Log.debug("MOB: finished loading in island data from the database");
    }
    
    /**
     * Loads an instance using the given instance name. It will spawn mobs based on the spawns 
     * found in the database that match this instance
     * @param instanceName: the name of the instance template to use
     */
    private OID loadInstance(int templateID, String instanceName, int instanceType, OID groupOid) {
    	return loadInstance(templateID, instanceName, instanceType, groupOid, 0);
    }
    
    private OID loadInstance(int templateID, String instanceName, int instanceType, OID groupOid, int instanceNum) {
    	Log.debug("MOB: about to load instance: " + templateID + " and groupOid: " + groupOid);
    	// First check if the content category exists, if not load the data from the db
    	InstanceTemplate island = instanceTemplates.get(templateID);
    	if (!contentCategories.containsKey(island.getCategory())) {
    		loadCategoryContent(mobDataBase, island.getCategory());
    	}
    	Template overrideTemplate = new Template();
    	//TODO: come up with a way of naming instances
    	if(instanceNum > 0) {
    		instanceName = island.getName() + "_" + instanceNum;
    	} else if (instanceType == 0) {
    		instanceName = island.getName();
    	} else {
    		instanceName = island.getName() + numInstances;
    	}
    		
    	Log.debug("INSTANCE: creating instance with name: " + instanceName);
    	overrideTemplate.setName(instanceName);
    	overrideTemplate.put(Namespace.INSTANCE, InstanceClient.TEMPL_INSTANCE_NAME, island.getName());
    	OID instanceOid = InstanceClient.createInstance(templateID, overrideTemplate, groupOid);
    	
    	numInstances++;
    	// Load any building grids relating to this instance
    	//loadBuildingGrids(mobDataBase, templateName, instanceOid);
    	Log.debug("MOB: finished loading instance: " + templateID);
    	return instanceOid;
    }
    
    /**
     * Starts the spawning for an instance
     */
    class SpawnInstanceMobsHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.SpawnInstanceMobsMessage SPMsg = (AgisMobClient.SpawnInstanceMobsMessage) msg;
    	    
    	    SPMsg.tmpl.scheduleSpawnLoading(SPMsg.instanceOid);
    	    return true;
    	}
    }
    
    /**
     * Spawns a creature.
     */
    class SpawnMobHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.SpawnMobMessage SPMsg = (AgisMobClient.SpawnMobMessage) msg;
    	    SpawnData sd = SPMsg.sd;
    	    String factoryName = createMobFactory(sd);
    	    if (! factoryName.equals("")) {
    			sd.setFactoryName(factoryName);
        		MobManagerClient.createSpawnGenerator(sd);
    		}
    	    return true;
    	}
    }
    
    /**
     * Creates a new ObjectFactory using the data provided by the spawn data. The mob template name 
     * is obtained from a property in the spawn data.
     * @param sd: the Spawn Data to create an object factory for
     * @return String: the name of the object factory
     */
    public static String createMobFactory(SpawnData sd) {
    	int templateID = sd.getTemplateID();
    	Log.debug("MOB: creating mob factory for template: " + templateID);
    	Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.MOB_TEMPLATE);
    	if (tmpl == null) {
    		Log.error("MOB: template [" + templateID + "] doesn't exist.");
    		return "";
    	}
    	String meshName = "";
    	LinkedList<String> displays = (LinkedList) tmpl.get(WorldManagerClient.NAMESPACE, "displays");
    	if (displays.size() > 0) {
    		meshName = displays.get(0);
    		Log.debug("MOB: got display: " + meshName);
    	}
    	DisplayContext dc = new DisplayContext(meshName, true);
		dc.addSubmesh(new DisplayContext.Submesh("", ""));
	    tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
		tmpl.put(WorldManagerClient.NAMESPACE, "model", meshName); 
		ObjectManagerClient.registerTemplate(tmpl);
		
    	HashMap<String, Serializable> spawnProps = new HashMap<String, Serializable>();
    	MobFactory cFactory = new MobFactory(templateID);
		// Add some behavs
		cFactory.addBehav(new BaseBehavior());
		// Get properties from the template and add/edit behaviours to match
			BehaviorTemplate behavTmpl = (BehaviorTemplate) sd.getProperty("behaviourTemplate");
			int roamRadius = behavTmpl.getRoamRadius();
			if (roamRadius > 0) {
				Log.debug("BEHAV: about to add radius roam behaviour to mob: " + templateID);
				RadiusRoamBehavior rrBehav = new RadiusRoamBehavior();
				rrBehav.setRadius(roamRadius);
				rrBehav.setCenterLoc(sd.getLoc());
				rrBehav.setMovementSpeed(1.6f);
				cFactory.addBehav(rrBehav);
				Log.debug("BEHAV: adding radius roam behaviour to mob: " + templateID);
			}
			ArrayList<Point> patrolMarkers = behavTmpl.getPatrolPoints();
			if (patrolMarkers.size() > 0) {
				PatrolBehavior pBehav = new PatrolBehavior();
				for (int i = 0; i < patrolMarkers.size(); i++) {
					pBehav.addWaypoint(patrolMarkers.get(i));
					pBehav.addLingerTime(behavTmpl.getPatrolPauses().get(i));
				}
				pBehav.setMovementSpeed(1.6f);
				cFactory.addBehav(pBehav);
				Log.debug("BEHAV: adding patrol behaviour to mob: " + templateID);
			}
			boolean hasCombat = behavTmpl.getHasCombat();
			if (hasCombat) {
				Log.debug("BEHAV: about to add combat behaviour to mob: " + templateID);
				CombatBehavior cBehav = new CombatBehavior();
				HashMap<Integer, Integer> lootTables = (HashMap) tmpl.get(InventoryClient.NAMESPACE, "lootTables");
				cBehav.setLootTables(lootTables);
				cBehav.setCenterLoc(sd.getLoc());
				cBehav.setAggroRange(behavTmpl.getAggroRadius());
				cFactory.addBehav(cBehav);
				Log.debug("BEHAV: adding combat behaviour to mob: " + templateID);
			}
			ArrayList<Integer> startQuestList = behavTmpl.getStartsQuests();
			ArrayList<Integer> endQuestList = behavTmpl.getEndsQuests();
			ArrayList<Integer> startsDialoguesList = behavTmpl.getStartsDialogues();
			int merchantTable = behavTmpl.getMerchantTable();
			if (!startQuestList.isEmpty() || !endQuestList.isEmpty() || !startsDialoguesList.isEmpty() || (merchantTable > 0)) {
				ContentCategory cc = contentCategories.get(sd.getCategory());
				QuestBehavior qBehav = new QuestBehavior();
				for (int i: startQuestList) {
					if (Agis.QuestManager.get(i) != null) {
						qBehav.startsQuest(Agis.QuestManager.get(i));
					}
				}
				for (int i: endQuestList) {
					if (Agis.QuestManager.get(i) != null) {
						qBehav.endsQuest(Agis.QuestManager.get(i));
					}
				}
					
				for (int i: startsDialoguesList) {
					if (dialogues.containsKey(i)) {
						qBehav.startsDialogue(dialogues.get(i));
					}
				}
				
				if (merchantTable > 0) {
					qBehav.setMerchantTable(merchantTables.get(merchantTable));
					sd.setProperty("merchantTable", merchantTable);
				}
				cFactory.addBehav(qBehav);
			}
			Log.debug("BEHAV: passed quests: " + templateID);
			int questOpenLoot = behavTmpl.getQuestOpenLoot();
			if (questOpenLoot > 0) {
				Log.debug("OPEN: adding open behav to mob: " + templateID + "with loot table num: " + questOpenLoot);
				OpenBehavior oBehav = new OpenBehavior();
				LootTable lootTable = Agis.LootTableManager.get(questOpenLoot);
				if (lootTable == null) {
					Log.debug("OPEN: got null loot table");
				}
				Log.debug("OPEN: got loot table with num items: " + lootTable.getItems().size());
				oBehav.setItemsHeld(lootTable.getItems());
				oBehav.setItemLimit(1);
				cFactory.addBehav(oBehav);
				Log.debug("OPEN: added open behav to mob: " + templateID + "with item: " + lootTable.getItems().get(0));
			}
			if (behavTmpl.getIsChest()) {
				Log.debug("OPEN: adding chest behav to mob: " + templateID + "with loot tables");
				ChestBehavior oBehav = new ChestBehavior();
				/*LootTable lootTable = Agis.LootTableManager.get(chestOpenLoot);
				if (lootTable == null) {
					Log.debug("OPEN: got null loot table");
				}
				Log.debug("OPEN: got loot table with num items: " + lootTable.getItems().size());
				oBehav.setItemsHeld(lootTable.getItems());
				oBehav.setItemLimit(1);*/
				oBehav.setSingleItemPickup(false);
				cFactory.addBehav(oBehav);
				Log.debug("OPEN: added chest behav to mob: " + templateID);
			}
			int pickupItem = behavTmpl.getPickupItem();
			if (pickupItem > 0) {
				Log.debug("OPEN: adding pickup behav to mob: " + templateID + "with itemID: " + pickupItem);
				ChestBehavior oBehav = new ChestBehavior();
				ArrayList<Integer> itemList = new ArrayList<Integer>();
				itemList.add(pickupItem);
				oBehav.setItemsHeld(itemList);
				oBehav.setItemLimit(1);
				oBehav.setSingleItemPickup(true);
				cFactory.addBehav(oBehav);
				/*PickupReactionBehavior prBehav = new PickupReactionBehavior();
				prBehav.setItemID(pickupItem);
				prBehav.setRadius(2); // 2m
				cFactory.addBehav(prBehav);*/
				Log.debug("OPEN: added pickup behav to mob: " + templateID + "with item: " + pickupItem);
			}
			if (behavTmpl.getIsPlayerCorpse()) {
				PlayerCorpseBehavior pBehav = new PlayerCorpseBehavior();
				pBehav.addAcceptableTarget(OID.fromString(behavTmpl.getOtherUse()));
				pBehav.setLoot((LinkedList<OID>) sd.getProperty("loot"));
				pBehav.setCorpseDuration((Integer) sd.getProperty("corpseDuration"));
				pBehav.setSafeDuration((Integer) sd.getProperty("safeDuration"));
				pBehav.setSpawnerKey(sd.getName());
				pBehav.setCorpseOwner(OID.fromString(behavTmpl.getOtherUse()));
				cFactory.addBehav(pBehav);
				Log.debug("TEMP: PlayerCorpseBehaviour added");
			}
			if (behavTmpl.getBaseAction() != null) {
				sd.setProperty("baseAction", behavTmpl.getBaseAction());
			}
		spawnProps.put("weaponsSheathed", behavTmpl.getWeaponsSheathed());
		spawnProps.put("otherUse", behavTmpl.getOtherUse());
		sd.setProperty("props", spawnProps);
		//SynchroniseBehavior cBehav = new SynchroniseBehavior();
		//cBehav.setMasterOid(oid);
		//cFactory.addBehav(cBehav);
		String factoryName = templateID + "Factory" + numFactories;
		Log.debug("BEHAV: registering factory for mob: " + templateID);
		ObjectFactory.register(factoryName, cFactory);
		numFactories++;
		Log.debug("MOB: finished creating mob factory for template: " + templateID);
    	return factoryName;
    }
    
    /**
     * Sets the base model dc map for an NPC so they can have items equipped.
     */
    private void setBaseModel(Template tmpl, String gender) {
    	LinkedList<String> displayList = (LinkedList) tmpl.get(WorldManagerClient.NAMESPACE, "displays");
    	if (displayList != null) {
    		int displayNum = random.nextInt(displayList.size());
    		String display = displayList.get(displayNum);
    		Log.debug("NPC: using displayID: " + display);
		    Log.debug("MOB: chose display " + display);
		    //String raceGender = display.getRace() + display.getGender();
		    DisplayContext dc = new DisplayContext(display, true);
		    //dc.addSubmesh(new DisplayContext.Submesh("Feet-mesh.0", "BaseNPCTransparencyMaterial"));
    		dc.addSubmesh(new DisplayContext.Submesh("", ""));
    		//tmpl.put(WorldManagerClient.NAMESPACE, "model", prefabName);
		    tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
		    tmpl.put(WorldManagerClient.NAMESPACE, "playerAppearance", "NPC");
		    ObjectManagerClient.registerTemplate(tmpl);
    		return;
    	}
    }
    
    /**
     * Sets the display properties for the mob/npc based on their gender.
     * @param oid
     * @param gender
     */
    public static void setDisplay(OID oid, String gender) {
    	LinkedList<String> displayList = (LinkedList) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "displays");
    	Log.debug("DISPLAY: at setDisplay with mob " + oid + " which has displayList: " + displayList);
    	if (displayList != null) {
    		int displayNum = random.nextInt(displayList.size());
    		String display = displayList.get(displayNum);
    		Log.debug("DISPLAY: chose display " + display);
    		//String raceGender = display.getRace() + display.getGender();
    		DisplayContext dc = new DisplayContext(display, true);
    		//dc.addSubmesh(new DisplayContext.Submesh("Feet-mesh.0", "BaseNPCTransparencyMaterial"));
    		dc.addSubmesh(new DisplayContext.Submesh("", ""));
    		//WorldManagerClient.getObjectInfo(oid).
    		HashMap<String, Serializable> propMap = new HashMap<String, Serializable>();
    		propMap.put("aoobj.dc", dc);
    		String mobName = WorldManagerClient.getObjectInfo(oid).name;
    		Log.debug("DISPLAY: setting " + mobName + "'s gender as: " + gender + " with prefab: " + display);
    		//propMap.put("gender", gender);
    		//propMap.put("race", display.getRace());
    		//propMap.put("skinType", display.getSkinType());
    		//propMap.put("hairType", display.getHairType());
    		//propMap.put("hairColour", display.getHairColour());
    		//propMap.put("facialFeature", display.getFacialFeature());
    		//propMap.put("portrait", display.getPortrait());
    		EnginePlugin.setObjectProperties(oid, WorldManagerClient.NAMESPACE, propMap);
    		// Send the appearance afterwards just to make sure that the other properties are updated first
    		HashMap<String, Serializable> propMap2 = new HashMap<String, Serializable>();
    		propMap2.put("playerAppearance", "NPC");
    		EnginePlugin.setObjectProperties(oid, WorldManagerClient.NAMESPACE, propMap2);
    		Log.debug("DISPLAY: finished setting display " + display + " for mob: " + oid);
    		//ObjectManagerClient.registerTemplate(tmpl);
    		return;
    	}
    }
    
    /**
     * Randomises the appearance of a mobs template based on the properties set.
     * @param tmpl: the Template to randomise the appearance on
     */
    /*private void randomiseAppearance(Template tmpl) {
    	// First get the gender
    	String gender = (String) tmpl.get(WorldManagerClient.NAMESPACE, "gender");
    	if (gender.equals("Either")) {
    		// Randomise the gender
    		if (random.nextInt(2) == 0) {
    			gender = "Male";
    		} else {
    			gender = "Female";
    		}
    	} else if (gender.equals("Neither")) {
    		gender = "Male";
    	}
    	tmpl.put(WorldManagerClient.NAMESPACE, "gender", gender);
    	// Set the hair property
    	String hairMeshProperty = gender + "HairMeshes";
    	ArrayList<String> hairMeshes = (ArrayList) tmpl.get(WorldManagerClient.NAMESPACE, hairMeshProperty);
    	tmpl.put(WorldManagerClient.NAMESPACE, "hairMesh", hairMeshes.get(random.nextInt(hairMeshes.size())));
    	String hairColoursProperty = gender + "HairColours";
    	ArrayList<String> hairTextures = (ArrayList) tmpl.get(WorldManagerClient.NAMESPACE, hairColoursProperty);
    	tmpl.put(WorldManagerClient.NAMESPACE, "hairTexture", hairTextures.get(random.nextInt(hairTextures.size())));
    	String facialMeshProperty = gender + "FacialMeshes";
    	ArrayList<String> facialMeshes = (ArrayList) tmpl.get(WorldManagerClient.NAMESPACE, facialMeshProperty);
    	tmpl.put(WorldManagerClient.NAMESPACE, "facialMesh", facialMeshes.get(random.nextInt(facialMeshes.size())));
    	// Body textures
    	String bodyTexturesProperty = gender + "BodyColours";
    	ArrayList<String> bodyTextures = (ArrayList) tmpl.get(WorldManagerClient.NAMESPACE, bodyTexturesProperty);
    	tmpl.put(WorldManagerClient.NAMESPACE, "bodyTexture", bodyTextures.get(random.nextInt(bodyTextures.size())));
    	ObjectManagerClient.registerTemplate(tmpl);
    }*/
	
	/*private void createNewSpawn(String templateName, int spawnInfoID) {
		Template mobTemplate = ObjectManagerClient.getTemplate(templateName);
		//SpawnData sd = spawnInfo.
	}*/
    
    class LoginHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LoginMessage message = (LoginMessage) msg;
            OID playerOid = message.getSubject();
            EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "nonCombatPet", null);
            //CombatPet cPet = (CombatPet) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "CombatPet");
            EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "activePet", null);
            EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "combatPet", null);
        	/*if (cPet != null) {
        		cPet.loadPetData();
        	}*/
            
            // When logging in, make sure the players instance is not an arena one
            OID instanceOid = message.getInstanceOid();
    	    int instanceTemplateID = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
    	    InstanceTemplate island = instanceTemplates.get(instanceTemplateID);
    	    if (island.getIslandType() == ISLAND_TYPE_ARENA) {
    	    	LinkedList restoreStack = (LinkedList) EnginePlugin.getObjectProperty(
            			playerOid, Namespace.OBJECT_MANAGER,
                        ObjectManagerClient.TEMPL_INSTANCE_RESTORE_STACK);
            	restoreStack.pollLast(); // Get rid of the arena restore
            	// Now get the restore of where the player joined the arena
            	InstanceRestorePoint restorePoint = (InstanceRestorePoint) restoreStack.pollLast();
            	Log.debug("RESTORE: restorePoint: " + restorePoint);
            	OID defaultInstanceOid = InstanceClient.getInstanceOid(restorePoint.getInstanceID());
        		BasicWorldNode defaultLoc = new BasicWorldNode();
        		defaultLoc.setInstanceOid(defaultInstanceOid);
        		defaultLoc.setLoc(restorePoint.getLoc());
        		AOVector dir = new AOVector();
        		defaultLoc.setDir(dir);
        		// Save the restore stack
        		Log.debug("RESTORE: saving restore point stack: ");
        		EnginePlugin.setObjectProperty(
        				playerOid, Namespace.OBJECT_MANAGER,
                        ObjectManagerClient.TEMPL_INSTANCE_RESTORE_STACK, restoreStack);
        		InstanceClient.objectInstanceEntry(playerOid, defaultLoc, InstanceClient.InstanceEntryReqMessage.FLAG_NONE);
    	    }
            return true;
        }
    }

    // Log the logout information and send a response
    class LogoutHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LogoutMessage message = (LogoutMessage) msg;
            OID playerOid = message.getSubject();
            /*NonCombatPet oldPet = (NonCombatPet) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "nonCombatPet");
        	if (oldPet != null) {
        		oldPet.despawnPet();
        		Log.debug("PET: despawned old ncPet and now setting the player property to null");
        		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "nonCombatPet", null);
        	}
        	//CombatPet cPet = (CombatPet) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "CombatPet");
        	String petKey = (String) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "combatPet");
        	if (petKey != null) {
        		TamedPet pet = (TamedPet) ObjectManagerClient.loadObjectData(petKey);
        		pet.despawnPet();
        	}*/
            
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            return true;
        }
    }
    
    class SpawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.SpawnedMessage spawnedMsg = (WorldManagerClient.SpawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            
            ObjectInfo objInfo = WorldManagerClient.getObjectInfo(objOid);
            if (objInfo.objType == ObjectTypes.player) {
            	// Set the players world property
            	Log.debug("SPAWNED: setting world for player: " + objOid);
            	OID instanceOid = spawnedMsg.getInstanceOid();
        	    int world = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
        	    EnginePlugin.setObjectProperty(objOid, WorldManagerClient.NAMESPACE, "world", world);
        	    // Get the water height of the instance
        	    float globalWaterHeight = instanceTemplates.get(world).getGlobalWaterHeight();
        	    EnginePlugin.setObjectProperty(objOid, WorldManagerClient.NAMESPACE, "waterHeight", globalWaterHeight);
            } else if (objInfo.objType == ObjectTypes.mob) {
            	OID instanceOid = spawnedMsg.getInstanceOid();
            	if (instanceNavMeshes.containsKey(instanceOid)) {
            		ObjectStub objStub = (ObjectStub) EntityManager.getEntityByNamespace(objOid, Namespace.MOB);
            		//EntityManager.registerEntityByNamespace(obj, Namespace.MOB);
            		if (objStub != null && objStub.getWorldNode() != null) {
            			DetourActor actor = new DetourActor(objOid, objStub);
            			instanceNavMeshes.get(instanceOid).addActor(objOid, objInfo.loc, actor);
            		}
            	}
            }
            
            return true;
    	}
    }
    
    class DespawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.DespawnedMessage despawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
            OID objOid = despawnedMsg.getSubject();
            /*if (WorldManagerClient.getObjectInfo(objOid).objType == ObjectTypes.player) {
            	OID instanceOid = despawnedMsg.getInstanceOid();
        	    String world = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_NAME).templateName;
            }*/
            
            OID instanceOid = despawnedMsg.getInstanceOid();
            if (instanceNavMeshes.containsKey(instanceOid)) {
        		instanceNavMeshes.get(instanceOid).removeActor(objOid);
        	}
            
            return true;
    	}
    }
    
    /**
	 * Hook for the Harvest Resource Message. Used when a player attempts to start harvesting 
	 * from a ResourceNode.
	 * @author Andrew Harrison
	 *
	 */
	class InteractWithObjectHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            Log.debug("INTERACTIVE: got harvest resource message");
            OID playerOid = gridMsg.getSubject();
            OID instanceOid = WorldManagerClient.getObjectInfo(playerOid).instanceOid;
            int objectID = (Integer)gridMsg.getProperty("objectID");
            String state = (String)gridMsg.getProperty("state");
            if (interactiveObjects.containsKey(instanceOid)) {
            	Log.debug("INTERACTIVE: got obj instance: " + instanceOid + " looking for object: " + objectID);
            	if (interactiveObjects.get(instanceOid).containsKey(objectID)) {
            		Log.debug("INTERACTIVE: got resource"); 
            		interactiveObjects.get(instanceOid).get(objectID).tryUseObject(playerOid, state);
            	}
            }
            return true;
        }
	}
    
    /**
     * Spawns a creature in the specified arena. Gives the creature the arenaID so when it dies the system
     * can track it.
     */
    class SpawnArenaCreatureHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.spawnArenaCreatureMessage SPMsg = (AgisMobClient.spawnArenaCreatureMessage) msg;
    	    //Long duration = (Long) SPMsg.getProperty("duration");
    	    int arenaID = (Integer) SPMsg.getProperty("arenaID");
    	    OID instanceOid = (OID) SPMsg.getProperty("instanceOid");
    	    //String passiveEffect = (String) SPMsg.getProperty("passiveEffect");
    	    int spawnDataNum = (Integer) SPMsg.getProperty("spawnDataID");
    	    
    	    if (!spawnInfo.containsKey(spawnDataNum))
    	    {
    	    	// Load it in from the database
    	    	spawnInfo.put(spawnDataNum, mobDataBase.loadSpecificSpawnData(spawnDataNum));
    	    }
    	    SpawnData sd = spawnInfo.get(spawnDataNum);
    	    
    		String location = sd.getStringProperty("markerName");
    		if (location.equals("")) {
    			// location is a vector, no need to change it
    			int locX = sd.getIntProperty("locX");
    			int locY = sd.getIntProperty("locY");
    			int locZ = sd.getIntProperty("locZ");
    			Point p = new Point(locX, locY, locZ);
    			sd.setLoc(p);
    			int orientX = sd.getIntProperty("orientX");
    			int orientY = sd.getIntProperty("orientY");
    			int orientZ = sd.getIntProperty("orientZ");
    			int orientW = sd.getIntProperty("orientW");
    			Quaternion q = new Quaternion(orientX, orientY, orientZ, orientW);
    			sd.setOrientation(q);
    		} else {
    			// location is a marker, we will need to get its location
    			Marker m = InstanceClient.getMarker(instanceOid, location);
    			sd.setLoc(m.getPoint());
    			sd.setOrientation(m.getOrientation());
    		}
    		Log.debug("ARENA: finished location setting for spawn Num: " + spawnDataNum + " for arena id: " + arenaID);
    		sd.setInstanceOid(instanceOid);
    		String factoryName = createMobFactory(sd);
    		if (! factoryName.equals("")) {
    			sd.setFactoryName(factoryName);
    			sd.setProperty("arenaID", arenaID);
        		MobManagerClient.createSpawnGenerator(sd);
    		}
    	    return true;
    	}
    }
    
    public static boolean despawnArenaCreature(OID oid) {
    	ObjectStub obj = arenaSpawns.get(oid);
    	if (obj != null) {
    		obj.despawn();
    		ObjectManagerClient.unloadObject(oid);
    		arenaSpawns.remove(oid);
    		return true;
    	}
    	return false;
    }
    
    /**
     * Spawns a creature in the specified dome. Gives the creature the domeID so when it dies the system
     * can track it.
     */
    class SpawnDomeMobHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    ExtensionMessage SPMsg = (ExtensionMessage) msg;
    	    SpawnData sd = (SpawnData) SPMsg.getProperty("spawnData");
    	    int spawnType = (Integer) SPMsg.getProperty("spawnType");
    	    Log.error("DOME: got spawn dome mob message");
    	    int templateID = sd.getTemplateID();
        	Log.debug("MOB: creating mob factory for template: " + templateID);
        	Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.MOB_TEMPLATE);
        	if (tmpl == null) {
        		Log.error("MOB: template [" + templateID + "] doesn't exist.");
        		return true;
        	}
        	// First check if we randomise appearance, if so, randomise and update template appearance props
        	//boolean randomiseAppearance = (Boolean) tmpl.get(WorldManagerClient.NAMESPACE, "randomiseAppearance");
        	//if (randomiseAppearance) {
        	//	randomiseAppearance(tmpl);
        	//}
        	/*String gender = (String) tmpl.get(WorldManagerClient.NAMESPACE, "genderOptions");
        	Log.debug("DISPLAY: setting " + templateID + "'s spawnData gender as: " + gender);
        	sd.setProperty("genderOptions", gender);
        	if (gender.contains("Either")) {
        		if (random.nextInt(2) == 0)
        			gender = "Male";
        		else
        			gender = "Female";
        	}
        	setBaseModel(tmpl, gender);*/
        	//tmpl.put(WorldManagerClient.NAMESPACE, "gender", gender);
        	//setDisplay(tmpl, gender);
        	MobFactory cFactory = new MobFactory(templateID);
    		// Add some behavs
    		cFactory.addBehav(new BaseBehavior());
    		if (spawnType == -4 /*Dome.MOBTYPE_LOOT*/) {
    			/*PickupReactionBehavior behav = new PickupReactionBehavior(sd);
    			HashMap<Integer, Integer> lootTables = (HashMap)sd.getProperty("lootTables");
    			if (lootTables != null) {
    				behav.setLootTables(lootTables);
    			} else {
    				int itemID = (Integer)sd.getProperty("itemID");
    				behav.setItemID(itemID);
    			}
    			behav.setRadius(2); // 2m
    			cFactory.addBehav(behav);*/
    			HashMap<Integer, Integer> lootTables = (HashMap)sd.getProperty("lootTables");
    			tmpl.put(InventoryClient.NAMESPACE, "lootTables", lootTables);
    			LootBehavior lBehav = new LootBehavior(sd);
    			cFactory.addBehav(lBehav);
    		}
    		int roamRadius = (Integer) SPMsg.getProperty("roamRadius");
    		if (roamRadius > 0) {
    			RadiusRoamBehavior behav = new RadiusRoamBehavior();
    			behav.setRadius(roamRadius);
    			behav.setCenterLoc(sd.getLoc());
    			cFactory.addBehav(behav);
    		}
    		String factoryName = templateID + "Factory" + numFactories;
    		Log.debug("BEHAV: registering factory for mob: " + templateID);
    		ObjectFactory.register(factoryName, cFactory);
    		numFactories++;
    		sd.setFactoryName(factoryName);
        	MobManagerClient.createSpawnGenerator(sd);
        	Log.error("DOME: spawned dome mob");
    	    return true;
    	}
    }
    
    /*public class SpawnArenaObjects implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		AgisMobClient.spawnArenaObjectsMessage sMsg = (AgisMobClient.spawnArenaObjectsMessage) msg;
    		String worldName = (String) sMsg.getProperty("worldName");
    		String fileName = "$WORLD_DIR/" + worldName + "/DotLocations.csv";
            String worldFileName = FileUtil.expandFileName(fileName);
            Log.debug("ARENA: opening dotFile: " + worldFileName);
            
    		return true;
    	}
    }*/
    
    /**
     * Spawns a pet of the specified type for the specified player.
     */
    class SpawnPetHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.spawnPetMessage SPMsg = (AgisMobClient.spawnPetMessage) msg;
    	    OID oid = SPMsg.getSubject();
    	    Long duration = (Long) SPMsg.getProperty("duration");
    	    int petType = (Integer) SPMsg.getProperty("petType");
    	    int passiveEffect = (Integer) SPMsg.getProperty("passiveEffect");
    	    int skillType = (Integer) SPMsg.getProperty("skillType");
    	    
    	    if (petType == 2) {
    	    	int mobID = (Integer) SPMsg.getProperty("mobID");
    	    	spawnNonCombatPet(mobID, oid);
    	    } else if (petType == 3) {
    	    	int mobID = (Integer) SPMsg.getProperty("mobID");
    	    	spawnCombatPet(mobID, oid, duration, passiveEffect, skillType);
    	    } else if (petType == 4) {
    	    	String mobID = (String) SPMsg.getProperty("mobID");
    	    	spawnCapturedCombatPet(oid, mobID);
    	    }
			Log.debug("FACTION: update attitude completed");
    	    return true;
    	}
    }
    
    /**
     * Spawns a non combat pet that follows the owner around. If there is another non combat pet that the player
     * already has spawned, that pet will be despawned first.
     * @param templateName
     * @param ownerOid
     * @return
     */
    private boolean spawnNonCombatPet(int templateID, OID ownerOid) {
    	NonCombatPet oldPet = (NonCombatPet) EnginePlugin.getObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "nonCombatPet");
    	if (oldPet != null) {
    		oldPet.despawnPet();
    		// If we are trying to spawn the same pet, it means we are actually wanting to despawn the old one, so lets respawn now that it is done
    		if (oldPet.getMobTemplateID() == templateID) {
    			Log.debug("PET: despawned old ncPet and now setting the player property to null");
    			EnginePlugin.setObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "nonCombatPet", null);
    			return true;
    		}
    	}
    	Template tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.MOB_TEMPLATE);
    	String gender = (String) tmpl.get(WorldManagerClient.NAMESPACE, "genderOptions");
    	if (gender.equals("Either")) {
    		if (random.nextInt(2) == 0)
    			gender = "Male";
    		else
    			gender = "Female";
    	}
    	tmpl.put(WorldManagerClient.NAMESPACE, "gender", gender);
    	//setDisplay(tmpl, gender);
    	MobFactory cFactory = new MobFactory(templateID);
    	cFactory.addBehav(new BaseBehavior());
    	NonCombatPetBehavior ncpBehav = new NonCombatPetBehavior();
    	ncpBehav.setOwnerOid(ownerOid);
    	cFactory.addBehav(ncpBehav);
    	BasicWorldNode bwNode = WorldManagerClient.getWorldNode(ownerOid);
    	SpawnData spawnData = new SpawnData();
    	ObjectStub obj = null;
        obj = cFactory.makeObject(spawnData, bwNode.getInstanceOid(), bwNode.getLoc());
        obj.spawn();
        setDisplay(obj.getOid(), gender);
        InterpolatedWorldNode iwNode = obj.getWorldNode();
        Log.debug("PET: pet " + templateID + " spawned at: " + iwNode.getLoc() + " in instance: " + iwNode.getInstanceOid());
        Log.debug("PET: owner is at: " + bwNode.getLoc() + " in instance: " + bwNode.getInstanceOid());
        NonCombatPet ncPet = new NonCombatPet(templateID, obj.getOid(), true, ownerOid);
        // Save the NonCombatPet object to a player property
        EnginePlugin.setObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "nonCombatPet", ncPet);
    	return true;
    }
    
    /**
     * Spawns a combat pet that the owner can command. If the player already has a combat pet then it will just
     * call the summonPet function for that CombatPet instance, otherwise it will generate a new instance.
     * @param templateName
     * @param ownerOid
     * @return
     */
    private boolean spawnCombatPet(int mobID, OID ownerOid, Long duration, int passiveEffect, int skillType) {
    	Log.debug("PET: spawn combat pet hit with owner: " + ownerOid);
    	OID activePet = (OID) EnginePlugin.getObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "activePet");
    	if (activePet == null) {
    		CombatPet cPet = new CombatPet(mobID, ownerOid, duration, passiveEffect);
    	}
    	return true;
    }
    
    /**
     * Spawns a combat pet that the owner can command. If the player already has a combat pet then it will just
     * call the summonPet function for that CombatPet instance, otherwise it will generate a new instance.
     * @param templateName
     * @param ownerOid
     * @return
     */
    private boolean spawnCapturedCombatPet(OID ownerOid, String petRef) {
    	Log.debug("PET: spawn captured combat pet hit with owner: " + ownerOid + " and pet ref: " + petRef);
    	TamedPet pet = (TamedPet) ObjectManagerClient.loadObjectData(petRef);
    	if (pet != null) {
    		// Do a check to see if the owner has any existing pets
    		OID activePet = (OID) EnginePlugin.getObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "activePet");
    		String petKey = (String) EnginePlugin.getObjectProperty(ownerOid, WorldManagerClient.NAMESPACE, "combatPet");
    		if (activePet != null && petKey == null) {
    			// Shall we despawn the old pet, or tell the player they need to dismiss the old pet first?
    			WorldManagerClient.despawn(activePet);
    		}
        	if (petKey != null) {
        		TamedPet oldPet = (TamedPet) ObjectManagerClient.loadObjectData(petKey);
        		oldPet.despawnPet();
        		if (!oldPet.getName().equals(pet.getName())) {
        			pet.summonPet();
        		}
        	} else {
        		pet.summonPet();
        	}
    	}
    	return true;
    }
    
    /**
     * Tames a combat pet that the owner can command. If the player already has a combat pet then it will just
     * call the summonPet function for that CombatPet instance, otherwise it will generate a new instance.
     */
    class TameBeastHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.tameBeastMessage SPMsg = (AgisMobClient.tameBeastMessage) msg;
    	    OID oid = SPMsg.getSubject();
    	    OID mobOid = (OID) SPMsg.getProperty("mobOid");
    	    int skillType = (Integer) SPMsg.getProperty("skillType");
    	    Log.debug("PET: tame beast hook hit with target: " + mobOid);
    	    /*CombatPet oldPet = (CombatPet) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "CombatPet");
        	if (oldPet != null) {
        		Log.debug("PET: tame beast returning as player already has a pet");
        		return true;
        	}*/
        	int mobID = (Integer) EnginePlugin.getObjectProperty(mobOid, WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_ID);
        	String mobName = WorldManagerClient.getObjectInfo(mobOid).name;
        	String objectKey = generateObjectKey("pet");
        	TamedPet cPet = new TamedPet(objectKey, mobID, mobName, oid, skillType);
            cPet.setPersistenceFlag(true);
            ObjectManagerClient.saveObjectData(objectKey, cPet, WorldManagerClient.NAMESPACE);
            // Create a new item to store the object key
            String petItemName = "Whistle (for " + cPet.getMobName() + ")";
            HashMap<String, Serializable> itemProps = new HashMap<String, Serializable>();
            itemProps.put("petRef", objectKey);
            AgisInventoryClient.generateItem(oid, PET_WHISTLE, petItemName, 1, itemProps);
            // Save the NonCombatPet object to a player property
            //EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "CombatPet", cPet);
            ExtendedCombatMessages.sendAnouncementMessage(oid, "You have tamed a pet!", "");
    	    return true;
    	}
    }
    
    /**
     * Tames a combat pet that the owner can command. If the player already has a combat pet then it will just
     * call the summonPet function for that CombatPet instance, otherwise it will generate a new instance.
     */
    /*class SendPetCommandHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.sendPetCommandMessage SPMsg = (AgisMobClient.sendPetCommandMessage) msg;
    	    Long oid = SPMsg.getSubject();
    	    String command = (String) SPMsg.getProperty("command");
    	    Long targetOid = (Long) SPMsg.getProperty("targetOid");
    	    Log.debug("PET: send pet command hook hit with target: " + targetOid);
    	    CombatPet pet = (CombatPet) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "CombatPet");
        	if (pet != null) {
        		pet.handleCommand(command, targetOid);
        		return true;
        	} else {
        		String petKey = (String) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "activePet");
            	if (petKey != null) {
            		pet = (CombatPet) ObjectManagerClient.loadObjectData(petKey);
            		pet.handleCommand(command, targetOid);
            	}
        	}
        	
    	    return true;
    	}
    }*/
    
    class GetTemplatesHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage requestMessage = (ExtensionMessage) msg;
    	    OID oid = requestMessage.getSubject();
    	    String type = (String) requestMessage.getProperty("type");
    	    if (type.equals("quests")) {
    	    	sendQuestTemplates(oid);
    	    } else if (type.equals("quest")) {
    	    	int questID = (Integer)requestMessage.getProperty("ID");
    	    	sendQuestTemplate(oid, questID);
    	    } else if (type.equals("dialogues")) {
    	    	sendDialogueTemplates(oid);
    	    } else if (type.equals("merchantTables")) {
    	    	sendMerchantTables(oid);
    	    } else if (type.equals("mob")) {
    	    	sendMobTemplates(oid);
    	    } else if (type.equals("faction")) {
    	    	sendFactionTemplates(oid);
    	    } else if (type.equals("lootTables")) {
    	    	sendLootTables(oid);
    	    } else if (type.equals("patrol")) {
    	    	sendPatrolPoints(oid);
    	    }
    	    return true;
    	}
    }
    
    protected void sendMobTemplates(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "mobTemplates");
		//String world = (String) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
		//Island island = instanceTemplates.get(world);
    	ArrayList<HashMap<String, Serializable>> mobTemplates = mobDataBase.getMobTemplates(0 /*island.getCategory()*/, BASE_CATEGORY);
    	int pos = 0;
    	for (HashMap<String, Serializable> tmpl: mobTemplates) {
            props.put("mob_" + pos + "Name", tmpl.get("name"));
            props.put("mob_" + pos + "ID", tmpl.get("id"));
            props.put("mob_" + pos + "SubTitle", tmpl.get("subTitle"));
            props.put("mob_" + pos + "Species", tmpl.get("species"));
            props.put("mob_" + pos + "Subspecies", tmpl.get("subSpecies"));
            props.put("mob_" + pos + "Level", tmpl.get("level"));
            props.put("mob_" + pos + "Attackable", tmpl.get("attackable"));
            props.put("mob_" + pos + "MobType", tmpl.get("mobType"));
            props.put("mob_" + pos + "Faction", tmpl.get("faction"));
            props.put("mob_" + pos + "Gender", tmpl.get("gender"));
            props.put("mob_" + pos + "Scale", tmpl.get("scale"));
            //props.put("mob_" + pos + "AppearanceType", tmpl.get("appearanceType"));
            //props.put("mob_" + pos + "Appearance", tmpl.get("appearance"));
            LinkedList<String> displays = (LinkedList<String>) tmpl.get("displays");
            for (int i = 0; i < displays.size(); i++) {
            	props.put("mob_" + pos + "Display" + i, displays.get(i));
            }
            props.put("mob_" + pos + "NumDisplays", displays.size());
           // props.put("mob_" + pos + "Equipment", tmpl.get("equipment"));
			/*HashMap<Integer, Integer> lootTableMap = (HashMap<Integer, Integer>) tmpl.get("lootTables");
			int numLootTables = 0;
			for (int tableID : lootTableMap.keySet()) {
				props.put("mob_" + pos + "LootTable" + numLootTables, tableID);
				props.put("mob_" + pos + "LootTable" + numLootTables + "Chance", lootTableMap.get(tableID));
				numLootTables++;
			}
			props.put("mob_" + pos + "NumLootTables", numLootTables);*/
            pos++;
        }
    	props.put("numTemplates", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    protected void sendPatrolPoints(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "patrolPoints");
    	int pos = 0;
    	for (PatrolPoint point : patrolPoints.values()) {
    		if (point.startingPoint) {
    			props.put("patrol_" + pos + "Title", point.name);
    			props.put("patrol_" + pos + "Id", point.id);
    			pos++;
    		}
        }
    	props.put("numPatrols", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    protected void sendQuestTemplates(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "questTemplates");
		int world = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
		Map<Integer, AgisQuest> categoryQuests = Agis.QuestManager.getMap();
    	int pos = 0;
    	for (int key: categoryQuests.keySet()) {
            props.put("quest_" + pos + "Title", categoryQuests.get(key).getName());
            props.put("quest_" + pos + "Id", key);
            pos++;
        }
    	props.put("numTemplates", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    /**
     * Sends down the full details of a single quest
     * @param playerOid
     */
    protected void sendQuestTemplate(OID playerOid, int questID) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "questTemplate");
		int world = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
		InstanceTemplate island = instanceTemplates.get(world);
		AgisBasicQuest q = (AgisBasicQuest) Agis.QuestManager.get(questID);
        props.put("questTitle", q.getName());
        props.put("questId", questID);
        props.put("questLevel", q.getQuestLevelReq());
        props.put("questFaction", q.getFaction());
        if (q.getQuestPrereqs().size() > 0)
        	props.put("questPrereq", q.getQuestPrereqs().get(0));
        else
        	props.put("questPrereq", -1);
        props.put("questDescription", q.getDesc());
        props.put("questObjective", q.getObjective());
        props.put("questProgress", q.getProgressText());
        props.put("questCompletion", q.getCompletionText().get(0));
        //props.put("questExperience", q.getXpReward().get(0));
        int numItems = 0;
        if (q.getRewards().containsKey(0)) {
        	for (int itemID : q.getRewards().get(0).keySet()) {
        		props.put("questItem" + numItems, itemID);
        		numItems++;
        	}
        }
        props.put("questNumItems", numItems);
        numItems = 0;
        if (q.getRewardsToChoose().containsKey(0)) {
        	for (int itemID : q.getRewardsToChoose().get(0).keySet()) {
        		props.put("questItemToChoose" + numItems, itemID);
        		numItems++;
        	}
        }
        props.put("questNumItemsToChoose", numItems);
        
        int numObjectives = 0;
        for (KillGoal kGoal : q.getKillGoals()) {
        	props.put("questObjective" + numObjectives + "Type", "Kill");
        	props.put("questObjective" + numObjectives + "Target", kGoal.mobID);
        	props.put("questObjective" + numObjectives + "Text", kGoal.mobName);
        	props.put("questObjective" + numObjectives + "Count", kGoal.num);
        	numObjectives++;
        }
        for (CollectionGoal cGoal : q.getCollectionGoals()) {
        	props.put("questObjective" + numObjectives + "Type", "Collect");
        	props.put("questObjective" + numObjectives + "Target", cGoal.templateID);
        	props.put("questObjective" + numObjectives + "Text", cGoal.templateName);
        	props.put("questObjective" + numObjectives + "Count", cGoal.num);
        	numObjectives++;
        }
        props.put("questNumObjectives", numObjectives);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    protected void sendDialogueTemplates(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "dialogueTemplates");
		int world = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
    	int pos = 0;
    	for (int key: dialogues.keySet()) {
    		if (dialogues.get(key).getOpeningDialogue()) {
    			props.put("dialogue_" + pos + "Title", dialogues.get(key).getName());
    			props.put("dialogue_" + pos + "Id", key);
    			pos++;
    		}
        }
    	props.put("numTemplates", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    protected void sendMerchantTables(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "merchantTables");
		int world = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
		ItemDatabase mDB = new ItemDatabase(false);
		HashMap<Integer, MerchantTable> tables = mDB.loadMerchantTables();
		mDB.close();
    	int pos = 0;
    	for (int key: tables.keySet()) {
    		props.put("merchant_" + pos + "Title", tables.get(key).getName());
    		props.put("merchant_" + pos + "Id", key);
    		pos++;
        }
    	props.put("numTemplates", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    protected void sendFactionTemplates(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "factionTemplates");
		int world = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
		InstanceTemplate island = instanceTemplates.get(world);
		ArrayList<Faction> factions = mobDataBase.loadFactions(island.getCategory());
		factions.addAll(mobDataBase.loadFactions(BASE_CATEGORY));
    	int pos = 0;
    	for (Faction faction: factions) {
            props.put("faction_" + pos + "Name", faction.getName());
            props.put("faction_" + pos + "Id", faction.getID());
            props.put("faction_" + pos + "Group", faction.getGroup());
            props.put("faction_" + pos + "Category", faction.getCategory());
            props.put("faction_" + pos + "Static", faction.getIsPublic());
            pos++;
        }
    	props.put("numTemplates", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    protected void sendLootTables(OID playerOid) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "lootTables");
		int world = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
		InstanceTemplate island = instanceTemplates.get(world);
		HashMap<Integer, LootTable> lootTables = mobDataBase.loadLootTables(island.getCategory());
    	int pos = 0;
    	for (LootTable lTbl: lootTables.values()) {
            props.put("table_" + pos + "Name", lTbl.getName());
            props.put("table_" + pos + "ID", lTbl.getID());
            int dropPos = 0;
            for (int i = 0; i < lTbl.getItems().size(); i++) {
            	props.put("table_" + pos + "item" + i, lTbl.getItems().get(i));
            	props.put("table_" + pos + "itemCount" + i, lTbl.getItemCounts().get(i));
            	props.put("table_" + pos + "itemChance" + i, lTbl.getItemChances().get(i));
            	dropPos++;
            }
            props.put("table_" + pos + "NumDrops", dropPos);
            pos++;
        }
    	props.put("numTables", pos);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    class GetIslandsHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    AgisMobClient.getIslandsDataMessage SPMsg = (AgisMobClient.getIslandsDataMessage) msg;
    	    OID oid = SPMsg.getSubject();
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    sendIslandsData(oid, accountID);
    	    return true;
    	}
    }
    
    protected void sendIslandsData(OID playerOid, OID accountID) {
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "islandData");
		
    	int pos = 0;
    	for (int key: instanceTemplates.keySet()) {
    		InstanceTemplate island = instanceTemplates.get(key);
    		// Have to run some checks to see if the user is allowed to log into this island.
    		boolean hasAccess = true;
    		Log.debug("SendIslandData: checking access for account: " + accountID + ". island admin=" 
    				+ island.getAdministrator() + " and developers=" + island.getDevelopers());
    		if (!island.getIsPublic() && !accountHasDeveloperAccess(playerOid, accountID, island.getID()))
    			hasAccess = false;
    		
    		if (hasAccess)
    		{
    			props.put("island_" + pos + "Name", island.getName());
    			props.put("island_" + pos + "Public", island.getIsPublic());
    			if (island.getPassword().equals(""))
    				props.put("island_" + pos + "Password", false);
    			else
    				props.put("island_" + pos + "Password", true);
    			if (island.getAdministrator().equals(accountID)
    				|| island.getDevelopers().contains(accountID))
    				props.put("island_" + pos + "Developer", true);
    			else
    				props.put("island_" + pos + "Developer", false);
    			props.put("island_" + pos + "Rating", island.getRating());
    			props.put("island_" + pos + "Type", island.getIslandType());
    			props.put("island_" + pos + "Style", island.getStyle());
    			props.put("island_" + pos + "Description", island.getDescription());
    			pos++;
    		}
        }
    	props.put("numIslands", pos);
    	// Now work out whether the use can create an island. First get the # of islands they can have
    	// then look at how many islands they are administrator of.
    	AccountDatabase aDB = new AccountDatabase();
    	int numAvailable = aDB.getNumIslands(accountID);
    	numAvailable -= GetIslandsCreated(accountID);
    	props.put("islandsAvailable", numAvailable);
    	// If there is an island they can create then send down the list of templates
    	int numTemplates = 0;
    	if (numAvailable > 0) {
    		LinkedList<HashMap<String, Serializable>> templateIslands = aDB.loadTemplateIslands();
    		for (HashMap<String, Serializable> islandTemplate : templateIslands) {
    			props.put("template_" + numTemplates + "ID", islandTemplate.get("templateID"));
    			props.put("template_" + numTemplates + "Name", islandTemplate.get("name"));
    			props.put("template_" + numTemplates + "Size", islandTemplate.get("size"));
    			numTemplates++;
    		}
    	}
    	props.put("numTemplates", numTemplates);
    	
    	TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
    }
    
    public int GetIslandsCreated(OID accountOID)
    {
    	int islandsCreated = 0;
    	for (InstanceTemplate island : instanceTemplates.values())
    	{
    		if (island.getAdministrator().equals(accountOID))
    			islandsCreated++;
    	}
    	return islandsCreated;
    }
    
    class CreateIslandHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage verifyIslandAccessMessage = (ExtensionMessage) msg;
    	    OID oid = verifyIslandAccessMessage.getSubject();
    	    String template = (String) verifyIslandAccessMessage.getProperty("template");
    	    int templateID = (Integer) verifyIslandAccessMessage.getProperty("templateID");
    	    String islandName = (String) verifyIslandAccessMessage.getProperty("islandName");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    Log.debug("CreateIsland hit with template: " + template + " and islandName: " + islandName);
    	    
    	    TargetedExtensionMessage verifyResponse =
                new TargetedExtensionMessage(oid, oid);
    	    verifyResponse.setExtensionType("create_island_response");
            
            // Checks to make sure the user can create an island
            boolean canCreateIsland = true;
            String error = "";
            islandName.trim();
            // First name check:
            if (!HelperFunctions.isAlphaNumericWithSpacesAndApostrophes(islandName))
            {
            	canCreateIsland = false;
            	error = "Island name can only contain letters, numbers, apostrophes and spaces.";
            } else if (islandName.length() < 4)
            {
            	canCreateIsland = false;
            	error = "Island name must be longer than 3 characters.";
            } else if (islandName.length() > 24)
            {
            	canCreateIsland = false;
            	error = "Island name must be shorter than 24 characters.";
            }
            
            // Now account check
            AccountDatabase aDB = new AccountDatabase();
        	int numAvailable = aDB.getNumIslands(accountID);
        	numAvailable -= GetIslandsCreated(accountID);
        	if (numAvailable < 1) 
        	{
        		canCreateIsland = false;
            	error = "You must purchase access to another island before creating another one.";
        	}
            
        	// Now island name used check
            if (!aDB.getIslandName(islandName).isEmpty())
            {
            	canCreateIsland = false;
            	error = "Island name has already been used.";
            }
            
            if (canCreateIsland)
            {
            	String url = Engine.getProperty("atavism.create_island_url_remote");
            	HashMap<String, String> formData = new HashMap<String, String>();
            	formData.put("template", template);
            	formData.put("island_name", islandName);
            	HelperFunctions.sendHtmlForm(url, formData);
            	//url = Engine.getProperty("atavism.create_island_url_local");
            	//HelperFunctions.sendHtmlForm(url, formData);
            	if (!HelperFunctions.CopyTemplateFiles(template, islandName))
            	{
            		canCreateIsland = false;
            		error = "Island could not be copied. Please check you have a valid template and try again. If this error persists please contact a GM.";
            	} else
            	{
            		// Generate the database entry
            		InstanceTemplate island = new InstanceTemplate();
            		island.setAdministrator(accountID);
            		island.setCreateOnStartup(false);
            		island.setIsPublic(false);
            		island.setName(islandName);
            		island.setSize(1);
            		HashMap<String, HashMap<String, Float>> portals = aDB.loadIslandTemplatePortals(templateID);
            		aDB.writeIslandData(island, template);
            		island.setPortals(aDB.writePortalData(island.getID(), portals));
            		instanceTemplates.put(templateID, island);
            		Template tmpl = new Template(island.getName());
        			tmpl.put(InstanceClient.NAMESPACE, InstanceClient.TEMPL_INSTANCE_TEMPLATE_NAME, islandName);
        			tmpl.put(InstanceClient.NAMESPACE, "populationLimit", island.getPopulationLimit());
        			tmpl.put(InstanceClient.NAMESPACE, "instanceType", island.getIslandType());
        			tmpl.put(InstanceClient.NAMESPACE, "createOnStartup", island.getCreateOnStartup());
            		InstanceClient.registerInstanceTemplate(tmpl);
            		sendIslandsData(oid, accountID);
            		return true;
            	}
            }
            
            verifyResponse.setProperty("error", error);
            verifyResponse.setProperty("template", template);
            verifyResponse.setProperty("island_name", islandName);
            verifyResponse.setProperty("can_create", canCreateIsland);
            Engine.getAgent().sendBroadcast(verifyResponse);
    	    return true;
    	}
    }
    
    class VerifyIslandAccessHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage verifyIslandAccessMessage = (ExtensionMessage) msg;
    	    OID oid = verifyIslandAccessMessage.getSubject();
    	    int world = (Integer) verifyIslandAccessMessage.getProperty("world");
    	    String password = (String) verifyIslandAccessMessage.getProperty("password");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    Log.debug("VerifyIslandAccess hit with world: " + world);
    	    
    	    TargetedExtensionMessage verifyResponse =
                new TargetedExtensionMessage(oid, oid);
    	    verifyResponse.setExtensionType("world_access_response");
            
            // Determine whether the user has access to the specified island - and if yes, do they 
            // have developer access?
            boolean hasAccess = true;
            boolean isDeveloper = false;
            boolean isAdmin = false;
            InstanceTemplate island = instanceTemplates.get(world);
            if (!island.getIsPublic() && !island.getAdministrator().equals(accountID)
    				&& !island.getDevelopers().contains(accountID))
            {
            	hasAccess = false;
            }
            if (!island.getPassword().equals(password))
            	hasAccess = false;
            verifyResponse.setProperty("world", world);
            verifyResponse.setProperty("hasAccess", hasAccess);
            verifyResponse.setProperty("isDeveloper", isDeveloper);
            verifyResponse.setProperty("isAdmin", isAdmin);
            Engine.getAgent().sendBroadcast(verifyResponse);
    	    return true;
    	}
    }
    
    /**
     * 
     * @author Andrew
     *
     */
    class EnterWorldHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage enterWorldMessage = (ExtensionMessage) msg;
    	    OID oid = enterWorldMessage.getSubject();
    	    int world = (Integer) enterWorldMessage.getProperty("world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    Log.debug("EnterWorld hit with world: " + world);
            
            // Determine whether the user has access to the specified island
            boolean hasAccess = true;
            InstanceTemplate island = instanceTemplates.get(world);
            if (island == null)
            	return true;
            if (!island.getIsPublic() && !island.getAdministrator().equals(accountID)
    				&& !island.getDevelopers().contains(accountID))
            {
            	hasAccess = false;
            }
            
            if (hasAccess)
            {
            	BasicWorldNode node = new BasicWorldNode();
            	// Check for instance type
            	if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_WORLD) {
            		node = joinWorldInstance(island, oid, "spawn", null); 
            	} else if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_DUNGEON) {
            		node = joinDungeonInstance(island, oid, "spawn", null, false);
            	} else if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_GROUP_DUNGEON) {
            		node = joinDungeonInstance(island, oid, "spawn", null, true);
            	}
            	
            	if (node != null) {
            		InstanceClient.objectInstanceEntry(oid, node, InstanceClient.InstanceEntryReqMessage.FLAG_NONE);
            		//EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "world", world);
            		EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "category", island.getCategory());
            		//AgisMobClient.categoryUpdated(oid, island.getCategory());
            	}
            }
    	    return true;
    	}
    }
    
    /**
     * Handles a request to change instance to the specified instance at the location
     * of the specified marker. If no marker is specified, the player is teleported to 
     * the Vector passed in.
     * @author Andrew
     *
     */
    class ChangeInstanceHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		AgisWorldManagerClient.ChangeInstanceMessage spawnMsg = (AgisWorldManagerClient.ChangeInstanceMessage)msg;
    		OID playerOid = spawnMsg.getPlayerOid();
    		int instanceID = spawnMsg.getInstanceID();
    		String markerName = spawnMsg.getMarker();
    		Point loc = null;
    		if (markerName == null || markerName.equals("")) {
    			loc = spawnMsg.getLoc();
    		}
    		Log.debug("CHANGEI: player " + playerOid + " is changing instance to " + instanceID + " with marker " + markerName);
    		InstanceTemplate island = instanceTemplates.get(instanceID);
            BasicWorldNode node = new BasicWorldNode();
        	// Check for instance type
        	if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_WORLD) {
        		node = joinWorldInstance(island, playerOid, markerName, loc); 
        	} else if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_DUNGEON) {
        		node = joinDungeonInstance(island, playerOid, markerName, loc, false);
        	} else if (island.getIslandType() == InstanceTemplate.ISLAND_TYPE_GROUP_DUNGEON) {
        		node = joinDungeonInstance(island, playerOid, markerName, loc, true);
        	}
        	
        	if (node != null) {
        		InstanceClient.objectInstanceEntry(playerOid, node, InstanceClient.InstanceEntryReqMessage.FLAG_NONE);
        		//EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "world", world);
        		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "category", island.getCategory());
        		//AgisMobClient.categoryUpdated(playerOid, island.getCategory());
        	}	
    		return true;
    	}
    }
    
    protected BasicWorldNode joinWorldInstance(InstanceTemplate island, OID playerOid, String markerName, Point loc) {
    	BasicWorldNode node = new BasicWorldNode();
    	OID instanceOid = InstanceClient.getInstanceOid(island.getName());
    	String instanceName = island.getName();
    	if (instanceOid == null) {
    		instanceOid = loadInstance(island.getID(), island.getName(), island.getIslandType(), null);
    	    //Log.debug("CHANGEI: Instance name is wrong: " + world);
    	    //return true;
    		if (instanceOid == null)
    		{
    			Log.error("Could not get instance for world: " + island.getName());
    			return null;
    		}
    	}
    	// Check if there is space in the instance
    	
    	if (island.getPopulationLimit() > 0) {
    		int currentPopulation = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_PLAYER_POPULATION).playerPopulation;
    		Log.debug("INSTANCE: population for instance " + instanceOid + " is: " + currentPopulation + " against max: " + island.getPopulationLimit());
    		if (currentPopulation >= island.getPopulationLimit()) {
    			// Add _# to the name and see if it exists
    			int instanceNum = 1;
    			
    			while (true) {
    				instanceName = island.getName() + "_" + instanceNum;
    				instanceOid = InstanceClient.getInstanceOid(instanceName);
    				Log.debug("INSTANCE: oid for instance: " + instanceName + " is: " + instanceOid);
    				if (instanceOid != null) {
    					currentPopulation = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_PLAYER_POPULATION).playerPopulation;
    					Log.debug("INSTANCE: population for  instance is: " + currentPopulation + " against max: " + island.getPopulationLimit());
    					if (currentPopulation < island.getPopulationLimit()) {
    						break;
    					}
    				} else {
    					instanceOid = loadInstance(island.getID(), island.getName(), island.getIslandType(), null, instanceNum);
    					if (instanceOid != null) {
    						break;
    					}
    				}
    				instanceNum++;
    			}
    		}
    	}
    	
    	Marker spawn = null;
    	if (markerName != null)
    		spawn = InstanceClient.getMarker(instanceOid, markerName);
    	if (spawn != null) {
    		node.setOrientation(spawn.getOrientation());
        	node.setLoc(spawn.getPoint());
        	Log.debug("CHANGEI: using loc from marker: " + spawn.getPoint());
    	} else {
    		node.setLoc(loc);
    		Log.debug("CHANGEI: using loc: " + loc);
    	}
    	node.setInstanceOid(instanceOid);
    	AOVector direction = new AOVector();
    	node.setDir(direction);
    	Log.debug("CHANGEI: Instance name: " + instanceName + "; oid: " + instanceOid);
    	return node;
    }
    
    protected BasicWorldNode joinDungeonInstance(InstanceTemplate island, OID playerOid, String markerName, 
    		Point loc, boolean requiresGroup) {
    	BasicWorldNode node = new BasicWorldNode();
    	
    	Log.debug("CHANGEI: joining dungeon instance: " + island.getID() + " with player: " + playerOid);
    	// Get GroupOid
    	OID groupOid = playerOid;
    	GroupInfo gInfo = GroupClient.GetGroupMemberOIDs(playerOid);
    	if (gInfo.groupOid != null) {
    		groupOid = gInfo.groupOid;
    	} else if (requiresGroup) {
    		EventMessageHelper.SendErrorEvent(playerOid, EventMessageHelper.INSTANCE_REQUIRES_GROUP, 0, "");
    		return null;
    	}
    	
    	Log.debug("CHANGEI: got groupOid: " + groupOid);
    	
    	// Search for existing instances for the groupOid
    	OID instanceOid = InstanceClient.getInstanceOid(island.getID(), groupOid);
    	String instanceName = island.getName();
    	if (instanceOid == null) {
    		instanceOid = loadInstance(island.getID(), island.getName(), island.getIslandType(), groupOid);
    	    Log.debug("CHANGEI: No instance exists for group: " + groupOid);
    	    //return true;
    		if (instanceOid == null)
    		{
    			Log.error("Could not get instance for world: " + island.getName());
    			return null;
    		}
    	} else {
    		Log.debug("CHANGEI: got instance for groupOid: " + groupOid);
    	}
    	// Check if there is space in the instance
    	if (island.getPopulationLimit() > 0) {
    		int currentPopulation = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_PLAYER_POPULATION).playerPopulation;
    		if (currentPopulation >= island.getPopulationLimit()) {
    			Log.debug("CHANGEI: instanceOid is full: " + instanceOid);
    			// Add _# to the name and see if it exists
    			int instanceNum = 1;
    			
    			while (true) {
    				instanceName = island.getName() + "_" + instanceNum;
    				instanceOid = InstanceClient.getInstanceOid(instanceName);
    				if (instanceOid != null) {
    					currentPopulation = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_PLAYER_POPULATION).playerPopulation;
    					if (currentPopulation < island.getPopulationLimit()) {
    						break;
    					}
    				} else {
    					instanceOid = loadInstance(island.getID(), instanceName, island.getIslandType(), groupOid, instanceNum);
    					if (instanceOid != null) {
    						break;
    					}
    				}
    				instanceNum++;
    			}
    		}
    	}
    	
    	Marker spawn = null;
    	if (markerName != null)
    		spawn = InstanceClient.getMarker(instanceOid, markerName);
    	if (spawn != null) {
    		node.setOrientation(spawn.getOrientation());
        	node.setLoc(spawn.getPoint());
    	} else {
    		node.setLoc(loc);
    	}
    	node.setInstanceOid(instanceOid);
    	AOVector direction = new AOVector();
    	node.setDir(direction);
    	Log.debug("CHANGEI: Instance name: " + instanceName + "; oid: " + instanceOid);
    	return node;
    }
    
    class LoadIslandHook implements Hook{
        public boolean processMessage(Message msg, int flags) {
            GenericMessage loadIslandMsg = (GenericMessage) msg;
            int instanceID = (Integer) loadIslandMsg.getProperty("instanceID");
            OID groupOid = (OID) loadIslandMsg.getProperty("groupOid");
            Log.debug("LoadIslandHook hit with world: " + instanceID);
            InstanceTemplate island = instanceTemplates.get(instanceID);
            if (island == null)
            	Engine.getAgent().sendObjectResponse(msg, null);
            else {
            	OID instanceOid = loadInstance(instanceID, island.getName(), island.getIslandType(), groupOid);
            	Log.debug("LoadIslandHook instance loaded with oid: " + instanceOid);
            	Engine.getAgent().sendObjectResponse(msg, instanceOid);
            }
            return true;
        }
    }
    
    class RequestIslandDeveloperAccessHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage requestAccessMessage = (ExtensionMessage) msg;
    	    OID oid = requestAccessMessage.getSubject();
    	    OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
    	    String world = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_NAME).templateName;
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    Log.debug("RequestIslandDeveloperAccess hit with instanceOid: " + instanceOid + 
    	    		" world: " + world + " and account id: " + accountID);
            
            // Determine whether the user has access to the specified island - and if yes, do they 
            // have developer access?
            boolean hasAccess = true;
            InstanceTemplate island = instanceTemplates.get(world);
            if (island == null)
            	return true;
            if (!island.getIsPublic() && !island.getAdministrator().equals(accountID)
    				&& !island.getDevelopers().contains(accountID))
            {
            	hasAccess = false;
            }
            	
            TargetedExtensionMessage verifyResponse =
                    new TargetedExtensionMessage(oid, oid);
        	verifyResponse.setExtensionType("world_developer_response");
        	boolean isDeveloper = false;
            boolean isAdmin = false;
            if (island.getAdministrator().equals(accountID) && hasAccess)
            	isAdmin = true;
            if (island.getDevelopers().contains(accountID) && hasAccess)
            	isDeveloper = true;
        	verifyResponse.setProperty("isDeveloper", isDeveloper);
            verifyResponse.setProperty("isAdmin", isAdmin);
            Engine.getAgent().sendBroadcast(verifyResponse);
            if (isDeveloper)
            	sendIslandBuildingData(oid);
    	    return true;
    	}
    }
    
    /**
     * Sends data down about the island the user is currently building on. Contains information such as
     * number of spawns and the limit, whether the island is public etc.
     * @param oid
     */
    private void sendIslandBuildingData(OID oid) {
    	int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
	    Log.debug("SendIslandBuildingData hit with world: " + world);
        
        // Determine whether the user has developer access
	    InstanceTemplate island = instanceTemplates.get(world);
        if (island == null)
        	return;
        if (!accountHasDeveloperAccess(oid, accountID, world))
        	return;
               	
        TargetedExtensionMessage markerResponse =
                new TargetedExtensionMessage(oid, oid);
        markerResponse.setExtensionType("island_building_data");
        markerResponse.setProperty("name", island.getName());
        markerResponse.setProperty("isPublic", island.getIsPublic());
    	markerResponse.setProperty("content_packs", island.getContentPacks());
    	markerResponse.setProperty("subscription", island.getSubscriptionActive());
    	markerResponse.setProperty("numSpawns", mobDataBase.getSpawnCount(island.getID()));
    	
        Engine.getAgent().sendBroadcast(markerResponse);
    }
    
    class UpdatePortalHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage portalMsg = (ExtensionMessage)msg;
    		OID oid = OID.fromLong((Long)portalMsg.getProperty("playerOid"));
    		OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
    		int world = (Integer)  EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    		
    		if (!accountHasDeveloperAccess(oid, accountID, world))
    			return true;
    		
            int portalID = (Integer)portalMsg.getProperty("portalID");
            String portalName = (String)portalMsg.getProperty("portalName");
            Log.debug("UPDATEPORTAL: got update for portal: " + portalID);
            
    	   	AOVector loc = (AOVector) portalMsg.getProperty("loc");
    	   	Point p = new Point((int)loc.getX(), (int)loc.getY(), (int)loc.getZ());
    	   	Quaternion orient = (Quaternion) portalMsg.getProperty("orient");
    	   	int faction = (Integer) portalMsg.getProperty("faction");
    	   	HashMap<String, Float> portalProps = new HashMap<String, Float>();
    	   	portalProps.put("portalType", (float)1);
			portalProps.put("faction", (float)faction);
			portalProps.put("displayID", (float)LEGION_PORTAL_DISPLAY_ID);
			portalProps.put("locX", p.getX());
			portalProps.put("locY", p.getY());
			portalProps.put("locZ", p.getZ());
			portalProps.put("orientX", orient.getX());
			portalProps.put("orientY", orient.getY());
			portalProps.put("orientZ", orient.getZ());
			portalProps.put("orientW", orient.getW());
			portalProps.put("id", (float)portalID);
			InstanceTemplate island = instanceTemplates.get(world);
    	    island.updatePortal(portalName, portalProps);
			AccountDatabase aDB = new AccountDatabase();
			aDB.editPortalData(portalName, portalProps);
    	    return true;
    	}
    }
    
    class ViewSpawnMarkersHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage requestAccessMessage = (ExtensionMessage) msg;
    	    OID oid = requestAccessMessage.getSubject();
    	    sendSpawnMarkers(oid);
    	    return true;
    	}
    }
    
    private void sendSpawnMarkers(OID oid) {
    	int world = (Integer)  EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
	    Log.debug("SendSpawnMarkers hit with world: " + world);
        
        // Determine whether the user has developer access
	    InstanceTemplate itmpl = instanceTemplates.get(world);
        if (itmpl == null)
        	return;
        if (!accountHasDeveloperAccess(oid, accountID, world)) {
        	Log.error("User does not have permission to view spawns of world: " + world);
        	return;
        }
        	
        Log.debug("VIEW: sending spawns for player: " + oid);
        TargetedExtensionMessage markerResponse =
                new TargetedExtensionMessage(oid, oid);
        markerResponse.setExtensionType("add_visible_spawn_marker");
    	int numMarkers = 0;
    	
    	for (int id : instanceTemplates.get(world).getSpawns().keySet())
    	{
    		SpawnData sd = instanceTemplates.get(world).getSpawns().get(id);
    		if (sd.getLoc() == null) {
    			Log.error("SPAWN: no loc found for id: " + id);
    			continue;
    		}
    		markerResponse.setProperty("markerID_" + numMarkers, id);
    		markerResponse.setProperty("markerLoc_" + numMarkers, new AOVector(sd.getLoc()));
    		markerResponse.setProperty("markerOrient_" + numMarkers, sd.getOrientation());
    		numMarkers++;
    	}
    	markerResponse.setProperty("numMarkers", numMarkers);
        Engine.getAgent().sendBroadcast(markerResponse);
    }
    
    class RequestSpawnDataHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage requestMessage = (ExtensionMessage) msg;
    	    OID oid = requestMessage.getSubject();
    	    int spawnID = (Integer) requestMessage.getProperty("markerID");
    	    int world = (Integer)  EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    Log.debug("RequestSpawnData hit with world: " + world);
    	    
    	    // Determine whether the user has developer access
    	    InstanceTemplate island = instanceTemplates.get(world);
            if (island == null)
            	return true;
            if (!accountHasDeveloperAccess(oid, accountID, world))
            	return true;
    	    
            SpawnData sd = instanceTemplates.get(world).getSpawns().get(spawnID);
            sendSpawnData(oid, sd, spawnID);
    	    return true;
    	}
    }
    
    private void sendSpawnData(OID oid, SpawnData sd, int spawnID) {	
        TargetedExtensionMessage markerResponse =
                new TargetedExtensionMessage(oid, oid);
        markerResponse.setExtensionType("spawn_data");
        markerResponse.setProperty("spawnID", spawnID);
    	markerResponse.setProperty("numSpawns", sd.getNumSpawns());
    	markerResponse.setProperty("despawnTime", sd.getCorpseDespawnTime() / TIME_MULTIPLIER);
    	markerResponse.setProperty("respawnTime", sd.getRespawnTime() / TIME_MULTIPLIER);
    	markerResponse.setProperty("spawnRadius", sd.getSpawnRadius());
    	markerResponse.setProperty("mobTemplate", sd.getTemplateID());
    	BehaviorTemplate tmpl = (BehaviorTemplate)sd.getProperty(BEHAVIOR_TMPL_PROP);
    	markerResponse.setProperty("roamRadius", tmpl.getRoamRadius());
    	markerResponse.setProperty("patrolPath", tmpl.getPatrolPathID());
    	markerResponse.setProperty("hasCombat", tmpl.getHasCombat());
    	markerResponse.setProperty("merchantTable", tmpl.getMerchantTable());
    	markerResponse.setProperty("startsQuests", tmpl.getStartsQuests());
    	markerResponse.setProperty("endsQuests", tmpl.getEndsQuests());
    	markerResponse.setProperty("startsDialogues", tmpl.getStartsDialogues());
    	markerResponse.setProperty("pickupItem", tmpl.getPickupItem());
    	markerResponse.setProperty("isChest", tmpl.getIsChest());
        Engine.getAgent().sendBroadcast(markerResponse);
    }
    
    class CreateMobSpawnHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage spawnMsg = (ExtensionMessage)msg;
    		OID oid = OID.fromLong((Long)spawnMsg.getProperty("playerOid"));
    		OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
    		int world = (Integer)  EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    		
    		if (!accountHasDeveloperAccess(oid, accountID, world))
    			return true;
    		
    		
            int mobTemplate = (Integer)spawnMsg.getProperty("mobTemplate");
            Log.debug("CREATESPAWN: got spawn for template: " + mobTemplate);
            
            int respawnTime = (Integer) spawnMsg.getProperty("respawnTime");
            int despawnTime = (Integer) spawnMsg.getProperty("despawnTime");
            int numSpawns = (Integer) spawnMsg.getProperty("numSpawns");
            int spawnRadius = (Integer) spawnMsg.getProperty("spawnRadius");
            
    	   	AOVector loc = (AOVector) spawnMsg.getProperty("loc");
    	   	Point p = new Point(loc.getX(), loc.getY(), loc.getZ());
    	   	Quaternion orient = (Quaternion) spawnMsg.getProperty("orient");
    	   	
    	   	int roamRadius = (Integer) spawnMsg.getProperty("roamRadius");
    	   	int patrolPath = (Integer) spawnMsg.getProperty("patrolPath");
    	   	
    	   	int patrolPointsCount = (Integer) spawnMsg.getProperty("patrolPointsCount");
    	   	if (patrolPath < 1 && patrolPointsCount > 0) {
    	   		ArrayList<PatrolPoint> points = new ArrayList<PatrolPoint>();
    	   		for (int i = 0; i < patrolPointsCount; i++) {
        	   		float locX = (Float) spawnMsg.getProperty("patrolPoint" + i + "x");
        	   		float locY = (Float) spawnMsg.getProperty("patrolPoint" + i + "y");
        	   		float locZ = (Float) spawnMsg.getProperty("patrolPoint" + i + "z");
        	   		int lingerTime = (Integer) spawnMsg.getProperty("patrolPoint" + i + "linger");
        	   		PatrolPoint pp = new PatrolPoint(-1, new Point(locX, locY, locZ), lingerTime);
        	   		points.add(pp);
        	   	}
    	   		// Write patrol path to Database
    	   		boolean travelReverse = (Boolean) spawnMsg.getProperty("patrolPointsTravelReverse");
    	   		patrolPath = mobDataBase.writePatrolPath(points, travelReverse);
    	   		points.get(0).travelReverse = travelReverse;
    	   		for (int i = 0; i < points.size(); i++) {
    	   			patrolPoints.put(points.get(i).id, points.get(i));
    	   		}
    	   	}
    	   	
    	   	int startsQuestsCount = (Integer) spawnMsg.getProperty("startsQuestsCount");
    	   	ArrayList<Integer> startsQuests = new ArrayList<Integer>();
    	   	for (int i = 0; i < startsQuestsCount; i++) {
    	   		int startQuestID = (Integer) spawnMsg.getProperty("startsQuest" + i + "ID");
    	   		startsQuests.add(startQuestID);
    	   	}
    	   	int endsQuestsCount = (Integer) spawnMsg.getProperty("endsQuestsCount");
    	   	ArrayList<Integer> endsQuests = new ArrayList<Integer>();
    	   	for (int i = 0; i < endsQuestsCount; i++) {
    	   		int endQuestID = (Integer) spawnMsg.getProperty("endsQuest" + i + "ID");
    	   		endsQuests.add(endQuestID);
    	   	}
    	   	
    	   	int startsDialoguesCount = (Integer) spawnMsg.getProperty("startsDialoguesCount");
    	   	ArrayList<Integer> startsDialogues = new ArrayList<Integer>();
    	   	for (int i = 0; i < startsDialoguesCount; i++) {
    	   		int startDialogueID = (Integer) spawnMsg.getProperty("startsDialogue" + i + "ID");
    	   		startsDialogues.add(startDialogueID);
    	   	}
    	   	
    	   	int merchantTable = (Integer) spawnMsg.getProperty("merchantTable");
    	   	
    	   	int pickupItem = (Integer) spawnMsg.getProperty("pickupItem");
    	   	//int domeID = (Integer) spawnMsg.getProperty("domeID");
    	   	boolean chestLootTable = (Boolean) spawnMsg.getProperty("isChest");
    	   	
    	    SpawnData sd = new SpawnData();
    	    InstanceTemplate island = instanceTemplates.get(world);
    	    sd.setCategory(island.getCategory());
			sd.setLoc(p);
			sd.setOrientation(orient);
			sd.setInstanceOid(instanceOid);
			sd.setTemplateID(mobTemplate);
			sd.setNumSpawns(numSpawns);
			sd.setSpawnRadius(spawnRadius);
			sd.setRespawnTime(respawnTime * TIME_MULTIPLIER);
			sd.setCorpseDespawnTime(despawnTime * TIME_MULTIPLIER);
			//sd.setProperty("domeID", domeID);
			
			BehaviorTemplate behavTmpl = new BehaviorTemplate();
			behavTmpl.setRoamRadius(roamRadius);
			behavTmpl.setHasCombat(true);
			behavTmpl.setAggroRadius(0);
			behavTmpl.setStartsQuests(startsQuests);
			behavTmpl.setEndsQuests(endsQuests);
			behavTmpl.setStartsDialogues(startsDialogues);
			behavTmpl.setMerchantTable(merchantTable);
			Log.debug("MERCHANT: got tableID: " + merchantTable + " saved as: " + behavTmpl.getMerchantTable());
			behavTmpl.setPickupItem(pickupItem);
			behavTmpl.setIsChest(chestLootTable);
			behavTmpl.setPatrolPathID(patrolPath);
			mobDataBase.setPatrolPath(behavTmpl);
    		//int key = mDB.writeMobBehavior(behavTmpl);
			sd.setProperty(BEHAVIOR_TMPL_PROP, behavTmpl);
			int spawnID = mobDataBase.writeSpawnData(sd, p, orient, behavTmpl, island.getID());
			Log.debug("SPAWN: got spawnID: " + spawnID);
			behavTmpl.setID(spawnID);
    	    
			sd.setProperty("id", spawnID);
			String factoryName = createMobFactory(sd);
			Log.debug("SPAWN: factory Name: " + factoryName);
    		if (!factoryName.equals("")) {
    			sd.setFactoryName(factoryName);
        		MobManagerClient.createSpawnGenerator(sd);
        		Log.debug("SPAWN: created spawn generator: ");
    		}

    		instanceTemplates.get(world).getSpawns().put(spawnID, sd);
    		sendSpawnMarkerAdded(oid, spawnID, sd);
    	    return true;
    	}
    }
    
    class EditMobSpawnHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage editMsg = (ExtensionMessage)msg;
    		int spawnID = (Integer) editMsg.getProperty("markerID");
    		OID oid = editMsg.getSubject();
    		int world = (Integer)  EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    OID instanceOid2 = (OID) WorldManagerClient.getObjectInfo(oid).instanceOid;
    	    
    	    // Determine whether the user has developer access
    	    if (!accountHasDeveloperAccess(oid, accountID, world))
    	    	return true;
    	    
    	    // Delete the old spawn generator
    	    SpawnGenerator.removeSpawnGeneratorByID(instanceOid2, spawnID);
    		
    	    // Get the new properties and make the new one
    	    AOVector loc = (AOVector) editMsg.getProperty("loc");
    	    Point p = new Point((int)loc.getX(), (int)loc.getY(), (int)loc.getZ());
    	   	Quaternion orient = (Quaternion) editMsg.getProperty("orient");
            int mobTemplate = (Integer)editMsg.getProperty("mobTemplate");
            Log.debug("EDITSPAWN: got spawn for template: " + mobTemplate);
            int respawnTime = (Integer) editMsg.getProperty("respawnTime");
            int despawnTime = (Integer) editMsg.getProperty("despawnTime");
            int numSpawns = (Integer) editMsg.getProperty("numSpawns");
            int spawnRadius = (Integer) editMsg.getProperty("spawnRadius");
    	   	int roamRadius = (Integer) editMsg.getProperty("roamRadius");
    	   	int patrolPath = (Integer) editMsg.getProperty("patrolPath");
    	   	
    	   	int startsQuestsCount = (Integer) editMsg.getProperty("startsQuestsCount");
    	   	ArrayList<Integer> startsQuests = new ArrayList<Integer>();
    	   	for (int i = 0; i < startsQuestsCount; i++) {
    	   		int startQuestID = (Integer) editMsg.getProperty("startsQuest" + i + "ID");
    	   		startsQuests.add(startQuestID);
    	   	}
    	   	int endsQuestsCount = (Integer) editMsg.getProperty("endsQuestsCount");
    	   	ArrayList<Integer> endsQuests = new ArrayList<Integer>();
    	   	for (int i = 0; i < endsQuestsCount; i++) {
    	   		int endQuestID = (Integer) editMsg.getProperty("endsQuest" + i + "ID");
    	   		endsQuests.add(endQuestID);
    	   	}
    	   	
    	   	int startsDialoguesCount = (Integer) editMsg.getProperty("startsDialoguesCount");
    	   	ArrayList<Integer> startsDialogues = new ArrayList<Integer>();
    	   	for (int i = 0; i < startsDialoguesCount; i++) {
    	   		int startDialogueID = (Integer) editMsg.getProperty("startsDialogue" + i + "ID");
    	   		startsDialogues.add(startDialogueID);
    	   	}
    	   	int merchantTable = (Integer) editMsg.getProperty("merchantTable");
    	   	int pickupItem = (Integer) editMsg.getProperty("pickupItem");
    	   	boolean chestLootTable = (Boolean) editMsg.getProperty("isChest");

    	    OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
    	    String instanceName = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_NAME).name;
    	    Log.debug("Instance Name: " + instanceName);
    	    SpawnData sd = instanceTemplates.get(world).getSpawns().get(spawnID);
			sd.setLoc(p);
			sd.setOrientation(orient);
			sd.setInstanceOid(instanceOid);
			sd.setTemplateID(mobTemplate);
			sd.setNumSpawns(numSpawns);
			sd.setSpawnRadius(spawnRadius);
			sd.setRespawnTime(respawnTime * TIME_MULTIPLIER);
			sd.setCorpseDespawnTime(despawnTime * TIME_MULTIPLIER);
			
			BehaviorTemplate behavTmpl = (BehaviorTemplate)sd.getProperty("behaviourTemplate");
			behavTmpl.setRoamRadius(roamRadius);
			behavTmpl.setHasCombat(true);
			behavTmpl.setAggroRadius(0);
			behavTmpl.setStartsQuests(startsQuests);
			behavTmpl.setEndsQuests(endsQuests);
			behavTmpl.setStartsDialogues(startsDialogues);
			behavTmpl.setMerchantTable(merchantTable);
			behavTmpl.setPickupItem(pickupItem);
			behavTmpl.setIsChest(chestLootTable);
			behavTmpl.setPatrolPathID(patrolPath);
			mobDataBase.setPatrolPath(behavTmpl);
			sd.setProperty(BEHAVIOR_TMPL_PROP, behavTmpl);
			Log.debug("EDIT: " + behavTmpl.getID());
			mobDataBase.editSpawnData(sd, spawnID, p, orient, behavTmpl);
    	    
			sd.setProperty("id", spawnID);
			String factoryName = createMobFactory(sd);
    		if (! factoryName.equals("")) {
    			sd.setFactoryName(factoryName);
        		MobManagerClient.createSpawnGenerator(sd);
    		}

    		instanceTemplates.get(world).getSpawns().put(spawnID, sd);
    		sendSpawnMarkerAdded(oid, spawnID, sd);
    	    return true;
    	}
    }
    
    class DeleteSpawnMarkerHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage requestMessage = (ExtensionMessage) msg;
    	    OID oid = requestMessage.getSubject();
    	    int spawnID = (Integer) requestMessage.getProperty("markerID");
    	    int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    	    OID instanceOid2 = (OID) WorldManagerClient.getObjectInfo(oid).instanceOid;
    	    Log.debug("DeleteSpawnMarker hit with world: " + world);
    	    
    	    // Determine whether the user has developer access
    	    if (!accountHasDeveloperAccess(oid, accountID, world))
    	    	return true;
    	    
    	    instanceTemplates.get(world).getSpawns().remove(spawnID);
            SpawnGenerator.removeSpawnGeneratorByID(instanceOid2, spawnID);
            mobDataBase.deleteSpawnData(spawnID);
            sendSpawnMarkerDeleted(oid, spawnID);
    	    return true;
    	}
    }
    
    private void sendSpawnMarkerDeleted(OID oid, int spawnID) {
    	int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
	    Log.debug("SendSpawnMarkerDeleted hit with world: " + world);
        
        // Determine whether the user has developer access
	    InstanceTemplate island = instanceTemplates.get(world);
        if (island == null)
        	return;
        if (!accountHasDeveloperAccess(oid, accountID, world))
        	return;
        	
        TargetedExtensionMessage markerResponse =
                new TargetedExtensionMessage(oid, oid);
        markerResponse.setExtensionType("spawn_marker_deleted");
    	markerResponse.setProperty("spawnID", spawnID);
        Engine.getAgent().sendBroadcast(markerResponse);
    }
    
    private void sendSpawnMarkerAdded(OID oid, int spawnID, SpawnData sd) {
    	int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
	    Log.debug("SendSpawnMarkerDeleted hit with world: " + world);
        
        // Determine whether the user has developer access
	    InstanceTemplate island = instanceTemplates.get(world);
        if (island == null)
        	return;
        if (!accountHasDeveloperAccess(oid, accountID, world))
        	return;
        	
        TargetedExtensionMessage markerResponse =
                new TargetedExtensionMessage(oid, oid);
        markerResponse.setExtensionType("spawn_marker_added");
    	markerResponse.setProperty("markerID", spawnID);
    	markerResponse.setProperty("markerLoc", new AOVector(sd.getLoc()));
    	markerResponse.setProperty("markerOrient", sd.getOrientation());
        Engine.getAgent().sendBroadcast(markerResponse);
    }
    
    class CreateMobHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage spawnMsg = (ExtensionMessage)msg;
    		OID oid = OID.fromLong((Long)spawnMsg.getProperty("playerOid"));
    		int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    		OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    		
    		if (!accountHasDeveloperAccess(oid, accountID, world))
    			return true;
    		
    		InstanceTemplate island = instanceTemplates.get(world);
    		
    		boolean existingMob = false;
    		int templateID = (Integer)spawnMsg.getProperty("templateID");
            String name = (String)spawnMsg.getProperty("name");
            String subtitle = (String)spawnMsg.getProperty("subtitle");
            String species = (String)spawnMsg.getProperty("species");
            String subspecies = (String)spawnMsg.getProperty("subspecies");
            String gender = (String)spawnMsg.getProperty("gender");
            int displayCount = (Integer)spawnMsg.getProperty("displayCount");
            float scale = (Float)spawnMsg.getProperty("scale");
            int soundSet = 1;
            int level = (Integer)spawnMsg.getProperty("level");
            int animState = 1;
            int offset = 1900;
            int hitBox = 1500;
            int runThreshold = 2500;
            int health = 100;
            boolean attackable = (Boolean)spawnMsg.getProperty("attackable");
            int mobType = (Integer)spawnMsg.getProperty("mobType");
            int faction = (Integer)spawnMsg.getProperty("faction");
            String questCategory = "";
            LinkedList<Integer> displays = new LinkedList<Integer>();
            for (int i = 0; i < displayCount; i++) {
            	int displayID = (Integer)spawnMsg.getProperty("display" + i);
            	displays.add(displayID);
            }
            int numEquipment = (Integer)spawnMsg.getProperty("equipCount");
            LinkedList<Integer> equipIDs = new LinkedList<Integer>();
            for (int i = 0; i < numEquipment; i++) {
            	int equipID = (Integer)spawnMsg.getProperty("equip" + i + "ID");
            	equipIDs.add(equipID);
            }
            int numLootTables = (Integer)spawnMsg.getProperty("lootTableCount");
            HashMap<Integer, Integer> lootTables = new HashMap<Integer, Integer>();
            for (int i = 0; i < numLootTables; i++) {
            	int tableID = (Integer)spawnMsg.getProperty("lootTable" + i + "ID");
            	int tableChance = (Integer)spawnMsg.getProperty("lootTable" + i + "Chance");
            	lootTables.put(tableID, tableChance);
            }
            
            Template tmpl;
            String equipment = "";
            if (templateID == -1) {
            	templateID = mobDataBase.writeMobData(island.getCategory(), name, subtitle, mobType, soundSet, displays,
            		animState, scale, offset, hitBox, runThreshold, gender, level, attackable, faction, 
        			species, subspecies, questCategory);
            	if (templateID == -1) {
                	Log.error("MOB: Got error when writing mob data to the database");
                	return true;
                }
                //mDB.writeMobCombatData(templateID, health, "slash");
                // Equipment
                for (int equipID : equipIDs) {
                	equipment = equipment + "*" + equipID + "; ";
                	mobDataBase.writeMobEquipmentData(island.getCategory(), templateID, equipID);
                }
                // Loot Tables
                mobDataBase.writeMobLootTables(island.getCategory(), templateID, lootTables);
                tmpl = new Template(name, templateID, ObjectManagerPlugin.MOB_TEMPLATE);
            } else {
            	existingMob = true;
            	mobDataBase.editMobData(templateID, name, subtitle, mobType, soundSet, displays,
                		animState, scale, offset, hitBox, runThreshold, gender, level, attackable, faction, 
            			species, subspecies, questCategory);
            	//mDB.editMobCombatData(templateID, health, "slash");
            	// Do a check against the old equipment to see what needs added to the db and what needs deleted
            	tmpl = ObjectManagerClient.getTemplate(templateID, ObjectManagerPlugin.MOB_TEMPLATE);
            	String oldEquipment = (String)tmpl.get(InventoryClient.NAMESPACE, InventoryClient.TEMPL_ITEMS);
            	LinkedList<Integer> oldEquipIDs = new LinkedList<Integer>();
            	for (String itemName : oldEquipment.split(";")) {
                	boolean equip = false;
                	itemName = itemName.trim();
                	if (itemName.isEmpty())
                		continue;
                	if (itemName.startsWith("*")) {
                		itemName = itemName.substring(1);
                		equip = true;
                	}
                	int itemID = Integer.parseInt(itemName);
                	if (equip) {
                		oldEquipIDs.add(itemID);
                	}
            	}
            	for (int equipID : equipIDs) {
                	equipment = equipment + "*" + equipID + "; ";
                	if (!oldEquipIDs.contains(equipID))
                		mobDataBase.writeMobEquipmentData(island.getCategory(), templateID, equipID);
                }
            	for (int equipID : oldEquipIDs) {
            		if (!equipIDs.contains(equipID))
            			mobDataBase.deleteMobEquipmentData(templateID, equipID);
            	}
            	mobDataBase.writeMobLootTables(island.getCategory(), templateID, lootTables);
            }

			tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_ID, templateID);
			tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.mob);
			tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
			tmpl.put(WorldManagerClient.NAMESPACE, "subTitle", subtitle);
			
			tmpl.put(WorldManagerClient.NAMESPACE, "mobType", mobType);
			if (mobType == -1) {
				// It is an object - remove name display?
				tmpl.put(WorldManagerClient.NAMESPACE, "nameDisplay", false);
				tmpl.put(WorldManagerClient.NAMESPACE, "targetable", false);
			}
			tmpl.put(WorldManagerClient.NAMESPACE, "soundSet", soundSet);
			//String creatureDisplays = rs.getString("creatureDisplays");
			tmpl.put(WorldManagerClient.NAMESPACE, "displays", displays);
			tmpl.put(WorldManagerClient.NAMESPACE, "genderOptions", gender);
			
			tmpl.put(WorldManagerClient.NAMESPACE, "animationState", 1);
			AOVector v = new AOVector(scale, scale, scale);
			tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_SCALE, v);
			tmpl.put(WorldManagerClient.NAMESPACE, "overheadOffset", 1900);
			tmpl.put(WorldManagerClient.NAMESPACE, "hitBox", 1500);
			tmpl.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_RUN_THRESHOLD, 2.5f);
			//tmpl.put(CombatClient.NAMESPACE, "level", new AgisStat("level", level));
			tmpl.put(CombatClient.NAMESPACE, "attackable", attackable);
			tmpl.put(WorldManagerClient.NAMESPACE, "faction", faction);
			tmpl.put(WorldManagerClient.NAMESPACE, "species", species);
			tmpl.put(WorldManagerClient.NAMESPACE, "subSpecies", subspecies);
			putMobCombatStats(tmpl, level, health);
			tmpl.put(InventoryClient.NAMESPACE, InventoryClient.TEMPL_ITEMS, equipment);
			tmpl.put(InventoryClient.NAMESPACE, "lootTables", lootTables);
			
			ObjectManagerClient.registerTemplate(tmpl);
        	Log.debug("MOB: loaded template: [" + tmpl.getName() + "]");
        	
        	sendMobTemplates(oid);
        	if (existingMob)
        		SpawnGenerator.respawnMatchingMobs(instanceOid, templateID);
    	    return true;
    	}
    }
    
    protected void putMobCombatStats(Template tmpl, int level, int health) {
		String attackType = "slash";
		int exp_val = 100;
		// Now set the stats based on the stat variables above
		//float levelmod = 1.0f + (float)level / 40.0f;
		tmpl.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY, CombatPlugin.ATTACK_ABILITY);
		//tmpl.put(CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_REGEN_EFFECT, "regen effect");
		tmpl.put(CombatClient.NAMESPACE, "combat.mobflag", true);
		//tmpl.put(CombatClient.NAMESPACE, "level", new AgisStat("level", level));
		tmpl.put(CombatClient.NAMESPACE, "kill_exp", exp_val);
		tmpl.put(CombatClient.NAMESPACE, "weaponType", "Unarmed");
		tmpl.put(CombatClient.NAMESPACE, "attackType", attackType);
		// Other Stats
		LinkedList<Integer> effectsList = new LinkedList<Integer>();
		tmpl.put(CombatClient.NAMESPACE, "effects", effectsList);
    }
    
    class CreateFactionHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage spawnMsg = (ExtensionMessage)msg;
    		OID oid = OID.fromLong((Long)spawnMsg.getProperty("playerOid"));
    		int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    		
    		//if (!accountHasDeveloperAccess(oid, accountID, world))
    		//	return true;
    		
    		InstanceTemplate island = instanceTemplates.get(world);
    		
            String name = (String)spawnMsg.getProperty("name");
            String group = (String)spawnMsg.getProperty("subtitle");
            int defaultStance = (Integer)spawnMsg.getProperty("defaultStance");
            boolean isPublic = (Boolean)spawnMsg.getProperty("isPublic");
            
            int factionID = mobDataBase.writeFactionData(island.getCategory(), name, group, isPublic, defaultStance);
            if (factionID == -1) {
            	Log.error("MOB: Got error when writing faction data to the database");
            	//return true;
            }
            
            // Default Stances
            HashMap<Integer, Integer> defaultStances = new HashMap<Integer, Integer>();
            int numStances = (Integer)spawnMsg.getProperty("stanceCount");
            for (int i = 0; i < numStances; i++) {
            	int otherFaction = (Integer)spawnMsg.getProperty("faction" + i + "ID");
            	int stance = (Integer)spawnMsg.getProperty("faction" + i + "Stance");
            	mobDataBase.writeFactionStanceData(factionID, otherFaction, stance);
            	defaultStances.put(otherFaction, stance);
            }
            
            Faction faction = new Faction(factionID, name, group,
					island.getCategory());
            faction.setIsPublic(isPublic);
            faction.setDefaultStance(defaultStance);
            faction.setDefaultStances(defaultStances);
			
			Agis.FactionManager.register(faction.getID(), faction);

        	Log.debug("MOB: loaded faction: [" + faction.getName() + "]");
			EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "faction", Integer.valueOf(factionID));
        	sendFactionTemplates(oid);

    	    return true;
    	}
    }
    
    class CreateQuestHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage questMsg = (ExtensionMessage)msg;
            
            OID oid = OID.fromLong((Long)questMsg.getProperty("playerOid"));
            int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    		
    		if (!accountHasDeveloperAccess(oid, accountID, world))
    			return true;
            
            String title = (String)questMsg.getProperty("title");
            Log.debug("CREATEQUEST: got title: " + title);
            int questID = (Integer)questMsg.getProperty("ID");
            String description = (String)questMsg.getProperty("description");
            String objective = (String)questMsg.getProperty("objective");
            String progressText = (String)questMsg.getProperty("progressText");
            String completionText = (String)questMsg.getProperty("completionText");
            int faction = (Integer)questMsg.getProperty("faction");
            int numObjectives = (Integer)questMsg.getProperty("numObjectives");
            
            
            AgisBasicQuest q = new AgisBasicQuest();
			q.setName(title);
			q.setFaction(faction);
			q.setRepeatable(false);
			q.setSecondaryGrades(0);
			q.setDesc(description);
			q.setObjective(objective);
			q.setProgressText(progressText);
			q.setSecondaryGrades(1);
			// Requirements
			int level = (Integer)questMsg.getProperty("level");
			if (level < 2)
				level = 2;
			q.setQuestLevelReq(level - 2);
			int prereq = (Integer)questMsg.getProperty("prereq");
			
			InstanceTemplate island = instanceTemplates.get(world);
			// Split between create and edit
			if (questID == -1) {
				int totalTargets = 0;
	            for (int i = 0; i < numObjectives; i++) {
	            	int templateID = (Integer)questMsg.getProperty("objective" + i + "Target");
	        		String templateName = (String)questMsg.getProperty("objective" + i + "Text");
	        		int amount = (Integer)questMsg.getProperty("objective" + i + "Amount");
	            	String type = (String)questMsg.getProperty("objective" + i + "Type");
	            	if (type.equals("Kill")) {
	            		KillGoal kGoal = new KillGoal(0, templateID, templateName, amount);
	            		q.addKillGoal(kGoal);
	            	} else if (type.equals("Collect")) {
	            		CollectionGoal cGoal = new CollectionGoal(0, templateID, templateName, amount);
	            		q.addCollectionGoal(cGoal);
	            	}
	            	totalTargets += amount;
	            }
	            if (prereq != -1)
					q.addQuestPrereq(prereq);
				
				q.addDeliveryItem(-1);
				// Rewards
				q.setCompletionText(0, completionText);
				int exp = 30 + (10 + totalTargets * 10) * level;
				q.setXpReward(0, exp);
				int currencyType = (Integer)questMsg.getProperty("currencyType");
				int currencyAmount = (Integer)questMsg.getProperty("currencyAmount");
				q.setCurrencyReward(0, currencyType, currencyAmount);
				
				int reputationFaction = (Integer)questMsg.getProperty("reputationFaction");
				int reputationAmount = (Integer)questMsg.getProperty("reputationAmount");
				q.setRepReward(0, reputationFaction, reputationAmount);
				for (int i = 0; i < 4; i++) {
	            	int templateID = (Integer)questMsg.getProperty("itemReward" + i);
	            	if (templateID != -1)
	            		q.addReward(0, templateID, 1);
	            	templateID = (Integer)questMsg.getProperty("itemRewardToChoose" + i);
	            	if (templateID != -1)
	            		q.addRewardToChoose(0, templateID, 1);
	            }
				
				int key = mobDataBase.writeQuest(island.getCategory(), q);
				q.setID(key);
				if (key != -1) {
					Agis.QuestManager.register(key, q);
				}
			} else {
				// Editing an existing quest
				Log.debug("QUESTDB: editing quest=" + questID);
				q.clearGoals();
				int totalTargets = 0;
	            for (int i = 0; i < numObjectives; i++) {
	            	int templateID = (Integer)questMsg.getProperty("objective" + i + "Target");
	        		String templateName = (String)questMsg.getProperty("objective" + i + "Text");
	        		int amount = (Integer)questMsg.getProperty("objective" + i + "Amount");
	            	String type = (String)questMsg.getProperty("objective" + i + "Type");
	            	if (type.equals("Kill")) {
	            		KillGoal kGoal = new KillGoal(0, templateID, templateName, amount);
	            		q.addKillGoal(kGoal);
	            	} else if (type.equals("Collect")) {
	            		CollectionGoal cGoal = new CollectionGoal(0, templateID, templateName, amount);
	            		q.addCollectionGoal(cGoal);
	            	}
	            	totalTargets += amount;
	            }
	            q.getQuestPrereqs().clear();
	            if (prereq != -1)
					q.addQuestPrereq(prereq);
				
	            q.getDeliveryItems().clear();
				q.addDeliveryItem(-1);
				// Rewards
				q.setCompletionText(0, completionText);
				int exp = 30 + (10 + totalTargets * 10) * level;
				q.setXpReward(0, exp);
				int currencyType = (Integer)questMsg.getProperty("currencyType");
				int currencyAmount = (Integer)questMsg.getProperty("currencyAmount");
				q.setCurrencyReward(0, currencyType, currencyAmount);
				
				int reputationFaction = (Integer)questMsg.getProperty("reputationFaction");
				int reputationAmount = (Integer)questMsg.getProperty("reputationAmount");
				q.setRepReward(0, reputationFaction, reputationAmount);
				q.getRewards().clear();
				q.getRewardsToChoose().clear();
				for (int i = 0; i < 4; i++) {
	            	int templateID = (Integer)questMsg.getProperty("itemReward" + i);
	            	if (templateID != -1)
	            		q.addReward(0, templateID, 1);
	            	templateID = (Integer)questMsg.getProperty("itemRewardToChoose" + i);
	            	if (templateID != -1)
	            		q.addRewardToChoose(0, templateID, 1);
	            }
				mobDataBase.editQuest(questID, q);
				Log.debug("QUESTDB: quest edited");
				if (questID != -1) {
					Agis.QuestManager.remove(questID);
					Agis.QuestManager.register(questID, q);
				}
			}
			
            return true;
        }
    }
    
    class CreateLootTableHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage lootTableMsg = (ExtensionMessage)msg;
            
            OID oid = OID.fromLong((Long)lootTableMsg.getProperty("playerOid"));
            int world = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "world");
    	    OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
    		
    		if (!accountHasDeveloperAccess(oid, accountID, world))
    			return true;
            
            String name = (String)lootTableMsg.getProperty("name");
            Log.debug("CREATELOOT: got name: " + name);
            int tableID = (Integer)lootTableMsg.getProperty("ID");
            int numDrops = (Integer)lootTableMsg.getProperty("numDrops");
            ArrayList<Integer> items = new ArrayList<Integer>();
            ArrayList<Integer> dropChances = new ArrayList<Integer>();
            ArrayList<Integer> itemCounts = new ArrayList<Integer>();
            for (int i = 0; i < numDrops; i++) {
            	int itemID = (Integer)lootTableMsg.getProperty("drop" + i + "Item");
        		int chance = (Integer)lootTableMsg.getProperty("drop" + i + "Chance");
        		int count = 1; //(Integer)lootTableMsg.getProperty("drop" + i + "Count");
        		items.add(itemID);
        		dropChances.add(chance);
        		itemCounts.add(count);
            }
            LootTable lootTable = new LootTable();
            lootTable.setName(name);
			lootTable.setItems(items);
			lootTable.setItemChances(dropChances);
			lootTable.setItemCounts(itemCounts);
			
			InstanceTemplate island = instanceTemplates.get(world);
			// Split between create and edit
			if (tableID == -1) {
				int key = mobDataBase.writeLootTable(island.getCategory(), lootTable);
				lootTable.setID(key);
				if (key != -1) {
					Agis.LootTableManager.register(key, lootTable);
					for (int i = 0; i < lootTable.getItems().size(); i++) {
						int itemID = lootTable.getItems().get(i);
						int itemCount = lootTable.getItemCounts().get(i);
						int itemChance = lootTable.getItemChances().get(i);
						mobDataBase.writeLootTableDrops(key, itemID, itemCount, itemChance);
					}
				}
			} else {
				// Editing an existing quest
				mobDataBase.editLootTable(tableID, lootTable);
				for (int i = 0; i < lootTable.getItems().size(); i++) {
					int itemID = lootTable.getItems().get(i);
					int itemCount = lootTable.getItemCounts().get(i);
					int itemChance = lootTable.getItemChances().get(i);
					mobDataBase.writeLootTableDrops(tableID, itemID, itemCount, itemChance);
				}
				if (tableID != -1) {
					Agis.LootTableManager.register(tableID, lootTable);
				}
			}
			sendLootTables(oid);
            return true;
        }
    }
    
    public static boolean AccountIsAdmin(OID accountOID) {
    	Log.debug("About to check island admins: " + instanceTemplates);
    	for (InstanceTemplate island : instanceTemplates.values()) {
    		Log.debug("Checking island admin with account: " + accountOID + " and administrator: " + island.getAdministrator());
    		if (island.getAdministrator().equals(accountOID))
    			return true;
    	}
    	return false;
    }
    
    /**
     * Checks if the player has Developer Access to the world, allowing them to modify spawns, and other world objects.
     * @param accountID
     * @param world
     * @return
     */
    public static boolean accountHasDeveloperAccess(OID characterOID, OID accountID, int world) {
    	// First check if the account is an admin account
    	int adminLevel = (Integer) EnginePlugin.getObjectProperty(characterOID, WorldManagerClient.NAMESPACE, "adminLevel");
    	if (adminLevel == AgisLoginPlugin.ACCOUNT_ADMIN) {
    		return true;
    	}
    	// If not, check if they are on the list of island developers
    	InstanceTemplate island = instanceTemplates.get(world);
        if (island == null) {
        	Log.debug("ACCESS: world: " + world + " does not exist");
        	return false;
        }
        if (!island.getAdministrator().equals(accountID) && !island.getDevelopers().contains(accountID)) {
        	Log.debug("ACCESS: player " + accountID.toString() + " does not have access to world: " + world);
        	return false;
        }
        return true;
    }
	
	public static String generateObjectKey(String prefix) {
		Calendar currentTime = Calendar.getInstance();
		String objectKey = prefix + "_" + currentTime.getTimeInMillis();
		return objectKey;
	}
	
	/*
     * NavMesh Loading/Updating
     */
    
    /**
     * Loads in the NavMesh and other world information. Sends out the SpawnInstanceObjects message when finished.
     */
    class LoadInstanceObjectsHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		SubjectMessage message = (SubjectMessage) msg;
            OID instanceOid = message.getSubject();
    	    
    	    int instanceID = (Integer)InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
    	    String instanceName = instanceTemplates.get(instanceID).getName();
    	    InstanceNavMeshManager navMeshManager = new InstanceNavMeshManager(instanceName, instanceOid);
    	    instanceNavMeshes.put(instanceOid, navMeshManager);
    	    
    	    // Load in interactive objects
    	    ContentDatabase cDB = new ContentDatabase(false);
    	    interactiveObjects.put(instanceOid, cDB.loadInteractiveObjects(instanceID, instanceOid));
    	    
    	    InstanceTemplate iTmpl = AgisMobClient.getInstanceTemplate(instanceID);
            AgisMobClient.spawnInstanceObjects(iTmpl, instanceOid);
    	    return true;
    	}
    }
	
	/**
	 * Called when a player either leaves the dome region, or clicks "Leave Dome". Removes the player from the dome if
	 * they were in it.
	 * @author Andrew
	 *
	 */
	class UseTrapDoorHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("GRID: got harvest resource grid");
            boolean entering = (Boolean)gridMsg.getProperty("entering");
            if (entering) {
            	int tileID = (Integer)gridMsg.getProperty("trapDoorID");
            	int instanceID = (Integer)gridMsg.getProperty("instanceID");
            	BuildingGrid grid = buildingGrids.get(tileID);
            	int oldWorld = (Integer)EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "world");
            	Point loc = grid.getPosition();
            	loc.add(0f, 0.3f, 0f);
            	Log.debug("TRAP: grid loc is = " + loc);
            	String markerName = "spawn";
        		OID instanceOid = InstanceClient.getInstanceOid(instanceID);
            	Marker spawn = InstanceClient.getMarker(instanceOid, markerName);
            	if (EnterInstanceAtLoc(playerOid, instanceID, spawn.getPoint())) {
            		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "trapDoorInstance", oldWorld);
            		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "trapDoorLoc", loc);
            	}
            } else {
            	int instanceID = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "trapDoorInstance");
            	Point loc = (Point) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "trapDoorLoc");
            	EnterInstanceAtLoc(playerOid, instanceID, loc);
            }
            
            return true;
        }
	}
	
	boolean EnterInstanceAtLoc(OID oid, int instanceID, Point loc) {
		InstanceTemplate island = instanceTemplates.get(instanceID);
		OID instanceOid = InstanceClient.getInstanceOid(instanceID);
    	if (instanceOid == null) {
    		// First check to see if an island exists with this name and the instance needs to be made
    		instanceOid = loadInstance(island.getID(), island.getName(), island.getIslandType(), null);
    	    //Log.debug("CHANGEI: Instance name is wrong: " + world);
    	    //return true;
    		if (instanceOid == null)
    		{
    			Log.error("Could not get instance for world: " + instanceID);
    			return false;
    		}
    	}
    	Log.debug("CHANGEI: Instance id: " + instanceID + "; oid: " + instanceOid);
    	BasicWorldNode node = new BasicWorldNode();
    	node.setLoc(loc);
    	node.setInstanceOid(instanceOid);
    	AOVector direction = new AOVector();
    	node.setDir(direction);
    	InstanceClient.objectInstanceEntry(oid, node, InstanceClient.InstanceEntryReqMessage.FLAG_NONE);
    	EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "world", instanceID);
    	EnginePlugin.setObjectProperty(oid, WorldManagerClient.NAMESPACE, "category", island.getCategory());
    	//AgisMobClient.categoryUpdated(oid, island.getCategory());
    	return true;
	}
	
	class PlayCoordinatedEffectHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage gridMsg = (ExtensionMessage)msg;
        	OID playerOid = gridMsg.getSubject();
        	String coordEffect = (String)gridMsg.getProperty("coordEffect");
        	OID targetOid = null;
        	boolean hasTarget = (Boolean)gridMsg.getProperty("hasTarget");
        	if (hasTarget) {
        		long targetOID = (Long)gridMsg.getProperty("targetOid");
        		targetOid = OID.fromLong(targetOID);
        	}
        	// Send down coord effect
        	CoordinatedEffect effect = new CoordinatedEffect(coordEffect);
            effect.sendSourceOid(true);
            if (hasTarget) {
            	effect.sendTargetOid(true);
            	effect.invoke(playerOid, targetOid);
            } else {
            	effect.invoke(playerOid, playerOid);
            }
            return true;
        }
	}
	
	
	class GetInstanceTemplateHook implements Hook {
    	public boolean processMessage(Message arg0, int arg1) {	
    		GetInstanceTemplateMessage msg = (GetInstanceTemplateMessage)arg0;	
    		int instanceTemplateID = msg.instanceTemplateID;
    		
    		if (instanceTemplates.containsKey(instanceTemplateID)) {
    			Engine.getAgent().sendObjectResponse(msg, instanceTemplates.get(instanceTemplateID));	
    		} else {
    			Engine.getAgent().sendObjectResponse(msg, null);	
    		}
    		
    		return true;
    	}
    }  
	
	/**
	 * Gets the dialogue matching the specified dialogueID. Should only be used within the mobserver process.
	 * @param dialogueID
	 * @return
	 */
	public static Dialogue getDialogue(int dialogueID) {
		if (dialogues.containsKey(dialogueID)) {
			return dialogues.get(dialogueID);
		}
		return null;
	}
	
	/**
	 * Sets the Loot Object Template. If specified, the loot object template will be spawned with the mobs loot
	 * instead of the mob having the loot.
	 * @param prefab
	 */
	public static void setLootObjectTmpl(int tmpl) {
    	lootObjectTmpl = tmpl;
    }
	
	// If specified, this template will be spawned and given the mobs loot rather than the mob itself
    public static int lootObjectTmpl = -1;
    
    /**
	 * Sets the Loot Object Despawn Time.
	 * @param prefab
	 */
	public static void setLootObjectDespawn(int duration) {
		lootObjectDespawn = duration;
    }
	
	// If specified, this template will be spawned and given the mobs loot rather than the mob itself
    public static int lootObjectDespawn = 30;

    private MobDatabase mobDataBase;
	//public static HashMap<String, SmooSkin> skins = new HashMap<String, SmooSkin>();
	private static HashMap<Integer, SpawnData> spawnInfo = new HashMap<Integer, SpawnData>();
	private static HashMap<Integer, InstanceTemplate> instanceTemplates = new HashMap<Integer, InstanceTemplate>();
	private static HashMap<Integer, ContentCategory> contentCategories = new HashMap<Integer, ContentCategory>();
	public static HashMap<OID, ObjectStub> arenaSpawns = new HashMap<OID, ObjectStub>();
	private static HashMap<Integer, BuildingGrid> buildingGrids = new HashMap<Integer, BuildingGrid>();
	private static HashMap<Integer, ResourceGrid> resourceGrids = new HashMap<Integer, ResourceGrid>();
	private static HashMap<Integer, Dialogue> dialogues = new HashMap<Integer, Dialogue>();
	public static HashMap<Integer, MerchantTable> merchantTables = new HashMap<Integer, MerchantTable>();
	public static HashMap<Integer, PatrolPoint> patrolPoints = new HashMap<Integer, PatrolPoint>();
	
	private HashMap<OID, InstanceNavMeshManager> instanceNavMeshes = new HashMap<OID, InstanceNavMeshManager>();
	HashMap<OID, HashMap<Integer, InteractiveObject>> interactiveObjects = new HashMap<OID, HashMap<Integer, InteractiveObject>>();
	
	private static final int PET_WHISTLE = 12; // DEPRECATED
	private static final int BASE_CATEGORY = 0;
	public static final String BEHAVIOR_TMPL_PROP = "behaviourTemplate";
	private static final int LEGION_PORTAL_DISPLAY_ID = 27; // DEPRECATED
	public static final int PORTAL_Y_OFFSET = 0; //750;
	private static final int ISLAND_TYPE_WORLD = 0;
	private static final int ISLAND_TYPE_ARENA = 2;
	
	// Used to convert milliseconds to seconds and vice versa
	public static final int TIME_MULTIPLIER = 1000;
	
	public static boolean MOB_DEATH_EXP = true;
	//public static final float FOLLOW_DISTANCE = 1.5f;
	
	private static int numFactories = 0;
	private static int numInstances = 0;
	static Random random = new Random();
}