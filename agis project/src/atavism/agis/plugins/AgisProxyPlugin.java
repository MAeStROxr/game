package atavism.agis.plugins;

import atavism.agis.database.AccountDatabase;
import atavism.agis.database.AuthDatabase;
import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.ResponseMessage;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Hook;
import atavism.server.engine.OID;
import atavism.server.events.AuthorizedLoginEvent;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.network.ClientConnection;
import atavism.server.objects.Entity;
import atavism.server.objects.Player;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.ProxyPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.AORuntimeException;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * handles client traffic to the rest of the servers
 */
public class AgisProxyPlugin extends ProxyPlugin implements Runnable {
    
	public void onActivate() {
		super.onActivate();
		try {
		    log.debug("AgisProxyPlugin.onActivate()");
		    // register for msgtype->hooks
	 	    this.registerProxyHooks();
	 	    
	 	   MessageTypeFilter filter = new MessageTypeFilter();
		   filter.addType(GroupClient.MSG_TYPE_GET_FRIENDS);
		   filter.addType(GroupClient.MSG_TYPE_ADD_FRIEND);
		   Engine.getAgent().createSubscription(filter, this);
		   
		   MessageTypeFilter responderFilter = new MessageTypeFilter();
	       responderFilter.addType(GroupClient.MSG_TYPE_GET_PLAYER_BY_NAME); 
	       responderFilter.addType(LoginMessage.MSG_TYPE_LOGIN);
	       responderFilter.addType(LogoutMessage.MSG_TYPE_LOGOUT);
	       Engine.getAgent().createSubscription(responderFilter, this, MessageAgent.RESPONDER);

		}
		catch(Exception e) {
		    throw new AORuntimeException("onActivate failed", e);
		}
		
		aDB = new AccountDatabase();
		authDB = new AuthDatabase();
		
		Engine.getExecutor().scheduleAtFixedRate(this, 5, 15, TimeUnit.SECONDS);
	}
	
	protected void registerProxyHooks() {
	  	//getHookManager().addHook(CombatClient.MSG_TYPE_UPDATE_PLAYER_ATTITUDES,
	  	//			 new UpdatePlayerAttitudesHook());
	  	getHookManager().addHook(GroupClient.MSG_TYPE_GET_PLAYER_BY_NAME, new GetPlayerOidFromNameHook());
	  	getHookManager().addHook(GroupClient.MSG_TYPE_GET_FRIENDS, new GetFriendsHook());
	  	getHookManager().addHook(GroupClient.MSG_TYPE_ADD_FRIEND, new AddFriendHook());
	  	getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN, new LoginHook());
	  	getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT, new LogoutHook());
	}
	
	@Override
	public void run() {
		Log.debug("AUTH: updating server status");
		authDB.sendServerStatusUpdate(getOids().size());
	}
	
	/**
     * process login message from the client.
     */
    protected boolean processLogin(ClientConnection con, AuthorizedLoginEvent loginEvent) {

        if (! super.processLogin(con, loginEvent))
	    return false;
        
        OID oid = loginEvent.getOid();
        Log.debug("LOGIN: login oid: " + oid);
        //Long accountID = getAccountId(oid);
        OID accountID = (OID) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "accountId");
        //Entity banList = ObjectManagerClient.loadObjectData("banList");
    	//HashMap<Long, Long> bannedUsers = (HashMap) banList.getProperty("bannedUsers");
        if (bannedUsers.containsValue(accountID)) {
        	// Check to see if time is up
        	Calendar c = Calendar.getInstance();
        	Long currentTime = c.getTimeInMillis();
        	if (currentTime.compareTo(bannedUsers.get(accountID)) > 0) {
        		removeAccountFromBanList(accountID);
        	} else {
        		Log.debug("LOGIN: user " + oid + " tried to login but they are banned, so closing connection");
            	con.close();
        	}
        }
        
        // get friends list and add them to the reverse map
        
        // now get the entry for this player and send a message to each person in the list
        // letting them know the player is now online
        
        // Example: get the inventory
        String IPaddress = con.IPAndPort();
        Log.debug("LOGIN: login IPAddress: " + con.toString());
        
        //Events.loginToWorld(oid, IPaddress);
        HashMap<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ipaddress", IPaddress);
        DataLoggerClient.logData("PLAYER_LOGGED_IN_EVENT", oid, null, accountID, props);
        Log.debug("LOGIN: sent login event");

        return true;
    }
    
    // Log the login information and send a response
    class LoginHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LoginMessage message = (LoginMessage) msg;
            OID playerOid = message.getSubject();
            OID instanceOid = message.getInstanceOid();
            Log.debug("LoginHook: playerOid=" + playerOid + " instanceOid=" + instanceOid);
            String playerName = WorldManagerClient.getObjectInfo(playerOid).name;
            LinkedList<OID> friendsOf = aDB.getFriendsOf(playerOid);
            // For each friend of, check if they are online then alert them to the fact their
            // friend has logged in.
            for (OID friendOid : friendsOf) {
            	if (getOids().contains(friendOid)) {
            		WorldManagerClient.sendObjChatMsg(friendOid, 2, playerName + " has logged in.");
            		SendFriendsList(friendOid);
            	}
            }
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            
            SendFriendsList(playerOid);
            WorldManagerClient.sendObjChatMsg(playerOid, 2, "Welcome to Smoo Online!");
            //sendPlayersOnline(false);
            authDB.sendServerStatusUpdate(getOids().size());
            return true;
        }
    }
    
    // Log the logout information and send a response
    class LogoutHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LogoutMessage message = (LogoutMessage) msg;
            OID playerOid = message.getSubject();
            Log.debug("LOGOUT: proxy logout started for: " + playerOid);
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            //sendPlayersOnline(true);
            authDB.sendServerStatusUpdate(getOids().size());
            
            LinkedList<OID> friendsOf = aDB.getFriendsOf(playerOid);
            // For each friend of, check if they are online then alert them to the fact their
            // friend has logged in.
            String playerName = WorldManagerClient.getObjectInfo(playerOid).name;
            for (OID friendOid : friendsOf) {
            	if (getOids().contains(friendOid)) {
            		WorldManagerClient.sendObjChatMsg(friendOid, 2, playerName + " has logged out.");
            		SendFriendsList(friendOid);
            	}
            }
            Log.debug("LOGOUT: proxy logout finished for: " + playerOid);
            return true;
        }
    }
    
    protected void sendPlayersOnline(boolean loggingOut) {
    	int playersOnline = playerManager.getPlayerCount();
    	if (loggingOut)
    		playersOnline -= 1;
    	// Send this message to every player (probably isn't very efficient)
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "players_online");
		props.put("online", playersOnline);
    	for (OID oid : super.getPlayerOids()) {
    		TargetedExtensionMessage msg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, oid, 
    				oid, false, props);
    		Engine.getAgent().sendBroadcast(msg);
    	}
    }
    
    /**
     * Used to get a Set of all players logged in.
     * @return
     */
    public Set<OID> getOids() {
    	return super.getPlayerOids();
    }
    
    class GetPlayerOidFromNameHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    GroupClient.getPlayerByNameMessage GPBNMsg = (GroupClient.getPlayerByNameMessage) msg;
    	    String playerName = (String) GPBNMsg.getProperty("inviteeName");
    	    Log.debug("PROXY: getting player oid from name " + playerName);
    	    OID oid = getPlayerOid(playerName);
    	    Engine.getAgent().sendOIDResponse(msg, oid);
    	    return true;
    	}
    }
    
    public OID getPlayerOid(String name) {
    	List<Player> players = new ArrayList<Player>(playerManager.getPlayerCount());
    	playerManager.getPlayers(players);
    	Log.debug("PROXY: searching for player oid from name: " + name + " and numPlayers: " + playerManager
                .getPlayerCount());
        for (Player player : players) {
            if (name.equals(player.getName()))
            	return player.getOid();
        }
    	Log.debug("PROXY: found no oid for player " + name);
    	return null;
    }
    
    public boolean isPlayerOnline(OID oid) {
    	List<Player> players = new ArrayList<Player>(playerManager.getPlayerCount());
    	playerManager.getPlayers(players);
        for (Player player : players) {
        	if (player.getOid().equals(oid))
        		return true;
        }
    	return false;
    }
    
    protected static void removeAccountFromBanList(OID accountID) {
    	Entity banList = ObjectManagerClient.loadObjectData("banList");
    	bannedUsers.remove(accountID);
    	banList.setProperty("bannedUsers", (Serializable) bannedUsers);
    	Log.debug("BAN: removed user: " + accountID + ". Banlist = " + banList);
    	ObjectManagerClient.saveObjectData("banList",
    			banList, WorldManagerClient.NAMESPACE);
    }
    
    public class AddFriendHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage spawnMsg = (ExtensionMessage)msg;
    		OID playerOid = (OID) spawnMsg.getProperty("playerOid");
    		String friendName = (String) spawnMsg.getProperty("friend");
    		Log.debug("FRIEND: adding friend " + friendName + " to " + playerOid);
    		// Check to make sure the friend does exist
    		OID friendOID = getPlayerOid(friendName);
    		if (friendOID == null) {
    			WorldManagerClient.sendObjChatMsg(playerOid, 2, friendName + " could not be found.");
    			return true;
    		}
    		// Check to make sure they do not already have this player as a friend
    		if (aDB.getFriends(playerOid).containsKey(friendOID)) {
    			WorldManagerClient.sendObjChatMsg(playerOid, 2, friendName + " is already your friend.");
    			return true;
    		}
    		// Finally, add friend
    		aDB.addFriend(playerOid, friendOID, friendName);
    		WorldManagerClient.sendObjChatMsg(playerOid, 2, friendName + " added to friends.");
    		SendFriendsList(playerOid);
            return true;
        }
    }
    
    public class GetFriendsHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	ExtensionMessage getMsg = (ExtensionMessage)msg;
    		OID playerOid = OID.fromLong((Long)getMsg.getProperty("playerOid"));
    		SendFriendsList(playerOid);
    		sendPlayersOnline(false);
    		return true;
        }
    }
    
    public void SendFriendsList(OID playerOid) {
    	Log.debug("FRIENDS: sending friends list for player: " + playerOid);
    	HashMap<OID, String> friends = aDB.getFriends(playerOid);
    	Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "friends_list");
		int numFriends = 0;
		for (OID friendOID : friends.keySet()) {
			props.put("friend" + numFriends + "Oid", friendOID);
			props.put("friend" + numFriends + "Name", friends.get(friendOID));
			props.put("friend" + numFriends + "Status", isPlayerOnline(friendOID));
			numFriends++;
		}
		props.put("numFriends", numFriends);
		TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("FRIENDS: sent friends list for player: " + playerOid + " with props: " + props);
    }

    public static HashMap<OID, Long> bannedUsers = new HashMap<OID, Long>();
    // Keeps track of who to notify when a player logs in
    public static HashMap<OID, ArrayList<OID>> friendReverseMap = new HashMap<OID, ArrayList<OID>>();
    // Keeps track of who to send chat messages to
    public static HashMap<String, ArrayList<OID>> chatChannelSubscribers = new HashMap<String, ArrayList<OID>>();
    
    AccountDatabase aDB;
    AuthDatabase authDB;
  
}
