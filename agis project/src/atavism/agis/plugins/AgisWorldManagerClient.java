package atavism.agis.plugins;

import atavism.msgsys.GenericMessage;
import atavism.msgsys.MessageType;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.messages.PropertyMessage;
import atavism.server.util.Log;


/**
 * AGIS-specific calls for sending/getting messages to the AgisWorldManagerPlugin
 */
public class AgisWorldManagerClient {
	
	/**
	 * Sends the SetMovementStateMessage.
	 * @param oid
	 * @param itemOid
	 * @param bagSpotNum
	 */
	public static void sendSetMovementStateMessage(OID oid, int movementState) {
		SetMovementStateMessage msg = new SetMovementStateMessage(oid, movementState);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("MOVEMENT: setMovementState hit 2");
	}

	/**
	 * Message used to put a bag item from the players inventory into a bag slot. 
	 * Do not use this to move a bag from one slot to another.
	 * @author Andrew Harrison
	 *
	 */
	public static class SetMovementStateMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public SetMovementStateMessage() {
            super();
        }
		public SetMovementStateMessage(OID oid, int movementState) {
			super(oid);
			setMsgType(MSG_TYPE_SET_MOVEMENT_STATE);
			setMovementState(movementState);
			Log.debug("MOVEMENT: setMovementState hit 1");
		}
		
		public void setMovementState(int state) {
			this.movementState = state;
		}
		public int getMovementState() {
			return this.movementState;
		}
		int movementState;
	}
	
	/**
	 * Sends the WaterRegionTransitionMessage.
	 * @param oid
	 * @param itemOid
	 * @param bagSpotNum
	 */
	public static void sendWaterRegionTransitionMessage(OID oid, int regionID, boolean entering) {
		WaterRegionTransitionMessage msg = new WaterRegionTransitionMessage(oid, regionID, entering);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("MOVEMENT: setMovementState hit 2");
	}

	/**
	 * Message used to put a bag item from the players inventory into a bag slot. 
	 * Do not use this to move a bag from one slot to another.
	 * @author Andrew Harrison
	 *
	 */
	public static class WaterRegionTransitionMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public WaterRegionTransitionMessage() {
            super();
        }
		public WaterRegionTransitionMessage(OID oid, int regionID, boolean entering) {
			super(oid);
			setMsgType(MSG_TYPE_WATER_REGION_TRANSITION);
			setRegionID(regionID);
			entering(entering);
			Log.debug("MOVEMENT: setMovementState hit 1");
		}
		
		public void setRegionID(int regionID) {
			this.regionID = regionID;
		}
		public int getRegionID() {
			return this.regionID;
		}
		int regionID;
		
		public void entering(boolean entering) {
			this.entering = entering;
		}
		public boolean entering() {
			return this.entering;
		}
		boolean entering;
	}
	
	/**
	 * Sends the ChangeInstanceMessage.
	 * @param oid
	 * @param instanceID
	 * @param loc
	 */
	public static void sendChangeInstance(OID playerOid, int instanceID, Point loc) {
		ChangeInstanceMessage msg = new ChangeInstanceMessage(playerOid, instanceID, null, loc);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("INSTANCE: ChangeInstance hit 2");
	}

	public static class ChangeInstanceMessage extends GenericMessage {
		private static final long serialVersionUID = 1L;
        public ChangeInstanceMessage() {
            super();
        }
		public ChangeInstanceMessage(OID playerOid, int instanceID, String marker, Point loc) {
			setMsgType(MSG_TYPE_CHANGE_INSTANCE);
			setPlayerOid(playerOid);
			setInstanceID(instanceID);
			setMarker(marker);
			setLoc(loc);
			Log.debug("INSTANCE: ChangeInstance hit 1");
		}
		
		public void setPlayerOid(OID playerOid) {
			this.playerOid = playerOid;
		}
		public OID getPlayerOid() {
			return this.playerOid;
		}
		OID playerOid;
		
		public void setInstanceID(int instanceID) {
			this.instanceID = instanceID;
		}
		public int getInstanceID() {
			return this.instanceID;
		}
		int instanceID;
		
		public void setMarker(String marker) {
			this.marker = marker;
		}
		public String getMarker() {
			return this.marker;
		}
		String marker;
		
		public void setLoc(Point loc) {
			this.loc = loc;
		}
		public Point getLoc() {
			return this.loc;
		}
		Point loc;
	}
	
    public static final MessageType MSG_TYPE_SET_MOVEMENT_STATE = MessageType.intern("ao.SET_MOVEMENT_STATE");
    public static final MessageType MSG_TYPE_WATER_REGION_TRANSITION = MessageType.intern("ao.WATER_REGION_TRANSITION");
    public static final MessageType MSG_TYPE_SET_UNDERWATER = MessageType.intern("ao.SET_UNDERWATER");
    public static final MessageType MSG_TYPE_CHANGE_INSTANCE = MessageType.intern("ao.CHANGE_INSTANCE");
}