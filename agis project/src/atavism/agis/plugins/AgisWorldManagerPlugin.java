package atavism.agis.plugins;

import atavism.agis.database.ContentDatabase;
import atavism.agis.objects.*;
import atavism.agis.plugins.AgisWorldManagerClient.SetMovementStateMessage;
import atavism.agis.plugins.AgisWorldManagerClient.WaterRegionTransitionMessage;
import atavism.agis.util.AgisDisplayContext;
import atavism.msgsys.*;
import atavism.server.engine.*;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.*;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.ObjectInfo;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedPropertyMessage;
import atavism.server.plugins.WorldManagerPlugin;
import atavism.server.util.Log;
import atavism.server.util.Points;

import java.io.Serializable;
import java.util.*;

/**
 * handles client traffic to the rest of the servers
 */
public class AgisWorldManagerPlugin extends WorldManagerPlugin {

    public AgisWorldManagerPlugin()
    {
        super();
        propertyExclusions.add(AgisObject.baseDCKey);
    }
    
    public void onActivate() {
		super.onActivate();
		
		// Create responder subscription
	 	MessageTypeFilter filter = new MessageTypeFilter();
	 	filter.addType(InstanceClient.MSG_TYPE_INSTANCE_LOADED);
	 	filter.addType(AgisMobClient.MSG_TYPE_ADD_TARGET_TO_CHECK);
	 	filter.addType(AgisMobClient.MSG_TYPE_REMOVE_TARGET_TO_CHECK);
	 	filter.addType(AgisWorldManagerClient.MSG_TYPE_WATER_REGION_TRANSITION);
	 	filter.addType(AgisWorldManagerClient.MSG_TYPE_SET_MOVEMENT_STATE);
	 	//filter.addType(AgisMobClient.MSG_TYPE_SET_UNDERWATER);
	 	filter.addType(WorldManagerClient.MSG_TYPE_SPAWNED);
	 	filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
	 	Engine.getAgent().createSubscription(filter, this);
		MessageTypeFilter filter2 = new MessageTypeFilter();
	 	filter2.addType(CombatClient.MSG_TYPE_GET_AOE_TARGETS);
	 	filter2.addType(LoginMessage.MSG_TYPE_LOGIN);
	 	//filter2.addType(LogoutMessage.MSG_TYPE_LOGOUT);
	 	Engine.getAgent().createSubscription(filter2, this, MessageAgent.RESPONDER);
	 	
	 	serverStartTime = System.currentTimeMillis();
	}

    protected void registerHooks() {
        super.registerHooks();
        getHookManager().addHook(InstanceClient.MSG_TYPE_INSTANCE_LOADED,
        						new LoadInstanceObjectsHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_SETWNODE_REQ,
                                new SetWNodeReqHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_UPDATEWNODE_REQ,
                                new UpdateWNodeReqHook());
        getHookManager().addHook(EnginePlugin.MSG_TYPE_SET_PROPERTY,
                                new NoMovePropertyHook());
        getHookManager().addHook(EnginePlugin.MSG_TYPE_SET_PROPERTY_NONBLOCK,
                                new NoMovePropertyHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_REPARENT_WNODE_REQ,
				 				new ReparentWNodeReqHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_SPAWNED,
			     				new SpawnedHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED,
 								new DespawnedHook());
        getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
  	           					new LoginHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_GET_AOE_TARGETS,
				 				new GetTargetsInAreaHook());
        getHookManager().addHook(AgisWorldManagerClient.MSG_TYPE_WATER_REGION_TRANSITION, 
				new WaterRegionTransitionHook());
        getHookManager().addHook(AgisWorldManagerClient.MSG_TYPE_SET_MOVEMENT_STATE, 
        						new SetMovementStateHook());
        /*getHookManager().addHook(AgisMobClient.MSG_TYPE_SET_UNDERWATER, 
								new SetUnderwaterHook());*/
    }

    /**
     * Override this method to change what kind of object is created
     * for the sub object hook.
     * @return AOObject representing the generated sub-object.
     */
    protected AOObject generateWorldManagerSubObject(Template template,
            OID masterOid)
    {
        // get the object type
        ObjectType objType = (ObjectType) template.get(Namespace.WORLD_MANAGER,
           WorldManagerClient.TEMPL_OBJECT_TYPE);
        AOObject obj = null;

        // generate the subobject
        if (Log.loggingDebug) {
            Log.debug("AgisWorldManagerPlugin: generateWorldManagerSubObject: objectType=" + objType + ", template=" + template);
        }
        if (objType == null) {
            Log.warn("AgisWorldManagerPlugin: generateSubObject: no object type, using structure");
            obj = new AgisObject(masterOid);
            obj.setType(ObjectTypes.structure);
        }
        else if (objType == ObjectTypes.mob || objType == ObjectTypes.player) {
            obj = new AgisMob(masterOid);
            obj.setType(objType);
        }
        else if (objType == ObjectTypes.structure) {
            obj = new AgisObject(masterOid);
            obj.setType(ObjectTypes.structure);
        }
        else if (objType == ObjectTypes.light) {
            Light l = new Light(masterOid);
            LightData ld = (LightData)template.get(Namespace.WORLD_MANAGER,
                Light.LightDataPropertyKey);
            l.setLightData(ld);
            obj = l;
        }
        else {
            obj = new AgisObject(masterOid);
            obj.setType(objType);
        }

        Map<String, Serializable> props = template.getSubMap(Namespace.WORLD_MANAGER);
        if (props == null) {
        	Log.warn("AgisWorldManagerPlugin.generateSubObject: no props in ns "
		     + Namespace.WORLD_MANAGER);
        	return null;
        }

        // copy properties from template to object
        for (Map.Entry<String, Serializable> entry : props.entrySet()) {
        	String key = entry.getKey();
        	Serializable value = entry.getValue();
        	if (!key.startsWith(":")) {
        		obj.setProperty(key, value);
        	}
        }
	
        if (obj.isUser() || obj.isMob() || obj.isStructure()) {
        	AgisObject agisObj = (AgisObject)obj;

        	// set the base display context for the object
        	DisplayContext dc = (DisplayContext)props.get(WorldManagerClient.TEMPL_DISPLAY_CONTEXT);
        	if (dc == null) {
                if (objType != ObjectTypes.terrainDecal)
                    Log.warn("AgisWorldManagerPlugin.generateSubObject: obj has no display context, oid="+masterOid);
        	} else {
        		dc = (DisplayContext)dc.clone();
        		dc.setObjRef(agisObj.getOid());
        		agisObj.baseDC(dc);
        		agisObj.displayContext(dc);
        	}
        }

        return obj;
    }
    
    /**
     * Loads in the NavMesh and other world information. Sends out the SpawnInstanceObjects message when finished.
     */
    class LoadInstanceObjectsHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		SubjectMessage message = (SubjectMessage) msg;
            OID instanceOid = message.getSubject();
    	    
    	    int instanceID = (Integer)InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
    	    
    	    // Load in interactive objects
    	    ContentDatabase cDB = new ContentDatabase(false);
    	    regions.put(instanceOid, cDB.loadRegions(instanceID, instanceOid));
    	    
    	    // Load in global water height
    	    
    	    return true;
    	}
    }
    
    class SetWNodeReqHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            WorldManagerClient.SetWorldNodeReqMessage setNodeMsg =
                (WorldManagerClient.SetWorldNodeReqMessage) msg;

            BasicWorldNode wnode =
                (BasicWorldNode)setNodeMsg.getWorldNode();
            OID oid = setNodeMsg.getSubject();
            Entity entity = getWorldManagerEntity(oid);

            boolean rv = false;

            do {
                if (entity == null) {
                    log.warn("SetWNodeReqHook: cannot find entity oid="+oid);
                    break;
                }

                if (entity instanceof AOObject) {
                    AOObject obj = (AOObject) entity;
                    if (obj.worldNode().isSpawned()) {
                        log.warn("SetWNodeReqHook: cannot set worldnode, object currently spawned oid="+oid);
                        break;
                    }

                    // If the new world node does not have orientation then
                    // keep the existing orientation.
                    Quaternion currentOrient = null;
                    if (obj.worldNode() != null)
                        currentOrient = obj.worldNode().getOrientation();
                    WMWorldNode newWnode = new WMWorldNode(wnode);
                    if (newWnode.getOrientation() == null)
                        newWnode.setOrientation(currentOrient);
                    newWnode.setPerceptionRadius(
                        ((WMWorldNode)obj.worldNode()).getPerceptionRadius());
                    if (Log.loggingDebug)
                        log.debug("SetWNodeReqHook: obj=" + obj +
                                ", newWnode=" + newWnode +
                                ", perceiver=" + obj.perceiver());

                    obj.worldNode(newWnode);
                    newWnode.setObject(obj);
                    if ((setNodeMsg.getFlags() & WorldManagerClient.SAVE_NOW) != 0)
                        Engine.getPersistenceManager().persistEntity(obj);
                    else
                        Engine.getPersistenceManager().setDirty(obj);

                    if (Log.loggingDebug)
                        log.debug("SetWNodeReqHook: done oid=" + oid +
                                  ", wnode=" + obj.worldNode());

                    rv = true;
                }
                else {
                    log.debug("SetWNodeReqHook: not aoobject oid="+oid);
                }
                break;
            } while (false);

            Engine.getAgent().sendBooleanResponse(msg, rv);

            return true;
        }
    }

    /**
     * Handles the UpdateWorldNodeReq Message. Called when a mob/player is requesting to 
     * update its world node (it moved or is moving now)
     */
    class UpdateWNodeReqHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            WorldManagerClient.UpdateWorldNodeReqMessage updateMsg = (WorldManagerClient.UpdateWorldNodeReqMessage) msg;
            BasicWorldNode wnode = updateMsg.getWorldNode();
            OID masterOid = updateMsg.getSubject();
            Entity entity = getWorldManagerEntity(masterOid);
            Boolean updateOverride = updateMsg.getOverride();
            
            if (entity == null) {
                log.error("UpdateWNodeReqHook: could not find entity, masterOid=" + masterOid);
                return false;
            }
            // log.debug("UpdateWNodeReqHook: entity=" + entity);

            if (!(entity instanceof AOObject)) {
                log.error("UpdateWNodeReqHook: entity is not an obj: " + entity);
                return false;
            }

            // FIXME/TODO: check if already spawned
            // ((AOObject)entity).getWorldNode().isSpawned();

            // loc from locatable
            // orient from wnode

            // get the object's current world node
            AOObject obj = (AOObject) entity;
            InterpolatedWorldNode curWnode = (InterpolatedWorldNode) obj.worldNode();

            // check for restrictions
            boolean nomove = obj.getBooleanProperty(WorldManagerClient.WORLD_PROP_NOMOVE);
            boolean noturn = obj.getBooleanProperty(WorldManagerClient.WORLD_PROP_NOTURN);
            boolean sendCorrection = false;

            // Get the current location before setting the direction.
            // This will complete the interpolation based on the old
            // direction.
            Point oldLoc = curWnode.getLoc();

            if (Log.loggingDebug)
                Log.debug("UpdateWNodeReqHook: oldLoc="+oldLoc +
                    " nomove="+nomove+ " noturn="+noturn);

            BasicWorldNode newNode = new BasicWorldNode(curWnode);

            // update the object's current world node
            Quaternion orient = wnode.getOrientation();
            if (orient != null) {
                if (!updateOverride && noturn) {
                    if (!curWnode.getOrientation().equals(orient)) {
                        orient = curWnode.getOrientation();
                        newNode.setOrientation(orient);
                        sendCorrection = true;
                    }
                } else {
                    if (updateOverride) {
                        newNode.setOrientation(orient);
                        sendCorrection = true;
                    }
                    curWnode.setOrientation(orient);
                }
            }

            AOVector dir = wnode.getDir();
            if (dir != null) {
                if (nomove) {
                    if (!dir.isZero()) {
                        dir = new AOVector(0,0,0);
                        newNode.setDir(dir);
                        sendCorrection = true;
                    }
                }
                curWnode.setDir(dir);
            }

            Point newLoc = wnode.getLoc();
            if (Log.loggingDebug) {
                Log.debug("UpdateWNodeReqHook: masterOid " + masterOid + ", oldLoc " + oldLoc + ", newLoc " + newLoc + ", override " + updateOverride);
            }
            if (newLoc != null) {
                if (!updateOverride && nomove) {
                    if (Point.distanceTo(oldLoc, newLoc) > 0) {
                        newLoc = oldLoc;
                        newNode.setLoc(newLoc);
                        sendCorrection = true;
                    }
                } else if (!updateOverride && !Points.isClose(oldLoc, newLoc, World.getLocTolerance())) {
                	// 05/23/07 Stryker Correct with the last location we got from the client, 
                	// rather than the last interpolated location, since the last interpolated 
                	// location may end up transporting the player inside a collision volume.
                	newLoc = curWnode.getRawLoc();
                	newNode.setLoc(newLoc);
                    sendCorrection = true;
                } else {
                	if (updateOverride) {
                        newNode.setLoc(newLoc);
                        sendCorrection = true;
                    }
                }
            }

            if (Log.loggingDebug) {
                log.debug("UpdateWNodeReqHook: set world node, entity=" + entity + ", new wnode=" + curWnode);
            }

            if (sendCorrection) {
                if (Log.loggingDebug) {
                    log.debug("UpdateWNodeReqHook: sending world node correction " + newNode);
                }
                WorldManagerClient.correctWorldNode(masterOid, newNode);
            }

            if (updateMsg.getPreMessage() != null) {
                Engine.getAgent().sendBroadcast(updateMsg.getPreMessage());
            }
            if (!newLoc.equals(oldLoc)) {
            	curWnode.setLoc(newLoc);
            }
            if (updateMsg.getPostMessage() != null) {
                Engine.getAgent().sendBroadcast(updateMsg.getPostMessage());
            }
            
            if (Point.distanceTo(oldLoc, newLoc) > 0.1f) {
            	//Send some message to interrupt any actions that are currently in casting
                CombatClient.interruptAbilityMessage interruptMsg = new CombatClient.interruptAbilityMessage(masterOid);
                Engine.getAgent().sendBroadcast(interruptMsg);
            }

            // make a wnodeupdatemsg - make a copy of the curWnode
            // but as basic world node since the WMWNode currently
            // has some serialization problems
            BasicWorldNode updateNode = new BasicWorldNode(curWnode);
            WorldManagerClient.UpdateWorldNodeMessage upMsg = new WorldManagerClient.UpdateWorldNodeMessage(masterOid, updateNode);
            Engine.getAgent().sendBroadcast(upMsg);
            return true;
        }
    }

    class ReparentWNodeReqHook implements Hook {
	public boolean processMessage(Message msg, int flags) {
	    WorldManagerClient.ReparentWNodeReqMessage rMsg = (WorldManagerClient.ReparentWNodeReqMessage) msg;

	    OID oid = rMsg.getSubject();
	    OID parentOid = rMsg.getParentOid();

            if (Log.loggingDebug)
                log.debug("ReparentWNodeReqHook: oid=" + oid + " parent=" + parentOid);
            // Entity entity = Entity.getEntity(oid);
            Entity entity = getWorldManagerEntity(oid);
	    InterpolatedWorldNode parentWnode = null;

            if (entity == null) {
                log.error("ReparentWNodeReqHook: could not find entity: " + oid);
                return false;
            }
            if (!(entity instanceof AOObject)) {
                log.error("ReparentWNodeReqHook: entity is not an obj: " + entity);
                return false;
            }

            // get the object's current world node
            AOObject obj = (AOObject) entity;
            InterpolatedWorldNode wnode = (InterpolatedWorldNode) obj.worldNode();

	    if (parentOid != null) {
		// Entity parent = Entity.getEntity(parentOid);
		Entity parent = getWorldManagerEntity(parentOid);
		if (parent == null) {
		    log.error("ReparentWNodeReqHook: could not find parent: " + parent);
		    return false;
		}
		if (!(parent instanceof AOObject)) {
		    log.error("ReparentWNodeReqHook: parent is not an obj: " + parent);
		    return false;
		}
		AOObject parentObj = (AOObject) parent;
		parentWnode = (InterpolatedWorldNode) parentObj.worldNode();
	    }

	    InterpolatedWorldNode oldParentWnode = (InterpolatedWorldNode)wnode.getParent();
	    if (oldParentWnode != null) {
		oldParentWnode.removeChild(wnode);
	    }
	    wnode.setParent(parentWnode);
	    if (parentWnode != null) {
		parentWnode.addChild(wnode);
		wnode.setLoc(parentWnode.getLoc());
		wnode.setDir(parentWnode.getDir());
		wnode.setOrientation(parentWnode.getOrientation());
	    }

	    BasicWorldNode bwnode = new BasicWorldNode(wnode);
	    WorldManagerClient.UpdateWorldNodeMessage updateMsg =
		new WorldManagerClient.UpdateWorldNodeMessage(oid, bwnode);
	    Engine.getAgent().sendBroadcast(updateMsg);
	    WorldManagerClient.WorldNodeCorrectMessage correctMsg =
		new WorldManagerClient.WorldNodeCorrectMessage(oid, bwnode);
	    Engine.getAgent().sendBroadcast(correctMsg);
	    return true;
	}
    }

    class NoMovePropertyHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            EnginePlugin.SetPropertyMessage rMsg = (EnginePlugin.SetPropertyMessage) msg;

            OID oid = rMsg.getSubject();
            if (rMsg.containsKey(WorldManagerClient.WORLD_PROP_NOMOVE)) {
                Boolean noMove = (Boolean)rMsg.getProperty(WorldManagerClient.WORLD_PROP_NOMOVE);
                AOObject obj = (AOObject)getWorldManagerEntity(oid);
                // If we're stopping, and we're a mob, and we have a MobPath
                if (noMove == true && obj != null && obj.getType().isMob() &&
                        obj.getProperty(WorldManagerClient.MOB_PATH_PROPERTY) != null) {
                    log.debug("NoMovePropertyHook: stopping object");

                    WorldManagerClient.MobPathCorrectionMessage correction = new 
                        WorldManagerClient.MobPathCorrectionMessage(oid,  System.currentTimeMillis(), "linear", 0, 
                            "", new LinkedList<Point>());
                    Engine.getAgent().sendBroadcast(correction);

                    WorldManagerClient.MobPathMessage cancellation = new 
                        WorldManagerClient.MobPathMessage(oid,  System.currentTimeMillis(), "linear", 0, 
                            "", new LinkedList<Point>());
                    Engine.getAgent().sendBroadcast(cancellation);
                    
                    BasicWorldNode wnode = obj.baseWorldNode();
                    wnode.setDir(new AOVector(0,0,0));

                    // make a wnodeupdatemsg - make a copy of the curWnode
                    // but as basic world node since the WMWNode currently
                    // has some serialization problems
                    WorldManagerClient.UpdateWorldNodeMessage upMsg =
                        new WorldManagerClient.UpdateWorldNodeMessage(oid, wnode);
                    Engine.getAgent().sendBroadcast(upMsg);
                }
            }
            return true;
        }
    }
    
    /**
	 * The hook for when players login. This will reset their arenaID (in case there was a 
	 * server crash) and teleport them back to the original world.
	 *
	 */
    class LoginHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LoginMessage message = (LoginMessage) msg;
            OID playerOid = message.getSubject();
            //OID instanceOid = message.getInstanceOid();
            // Make sure the model is set correctly
    	    //String race = (String) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "race");
    	    //String newMesh = getDisplayMesh(race);
    	    //DisplayContext dc = new DisplayContext(playerOid, newMesh);
    	    //sendDCMessage(playerOid, dc, true);
    	    //AOObject obj = (AOObject) getWorldManagerEntity(playerOid);
    	    //obj.displayContext(dc);
            
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            return true;
        }
    }
    
	/**
	 * The hook for when players logout (or disconnect). This will remove the player from
	 * any arenas and queues they are in.
	 *
	 */
    class LogoutHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LogoutMessage message = (LogoutMessage) msg;
            OID playerOid = message.getSubject();
            Log.debug("LogoutHook: playerOid=" + playerOid);
            
            // Check if they are in an arena
            int arenaID = -1;
            try {
            	arenaID = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "arenaID");
            } catch (NullPointerException e) {
            	Log.warn("ARENA PLUGIN: player " + playerOid + " does not have an arenaID property");
            }
            if (arenaID != -1)
                ArenaClient.removePlayer(playerOid);
            // Remove the player from any queues they may have been in
            ArrayList<ArenaQueue> queues = ArenaPlugin.getArenaQueues();
            //ArenaClient.leaveQueue(playerOid, arenaType);
            for (int k = 0; k < queues.size(); k++) {
				queues.get(k).removePlayer(playerOid);
			}
            
            // Check if they were in a Duel Challenge
            int challengeID = -1;
            try {
            	challengeID = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "duelChallengeID");
            } catch (NullPointerException e) {
            }
            if (challengeID != -1) {
            	//Map<Integer, DuelChallenge> duelChallenges = ArenaPlugin.getDuelChallenges();
            	//DuelChallenge challenge = duelChallenges.get(challengeID);
            	//challenge.playerDeclined(playerOid);
            	EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "factionOverride", "");
            	String name = WorldManagerClient.getObjectInfo(playerOid).name;
            	Log.debug("ARENA PLUGIN: removing player who is logging out from their duel");
            	ArenaClient.duelChallengeDisconnect(playerOid, name, challengeID);
            }
            
            // Check if they were in a Duel Challenge
            Log.debug("ARENA PLUGIN: checking duelID for player logging out");
            int duelID = -1;
            try {
            	duelID = (Integer) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DUEL_ID);
            } catch (NullPointerException e) {
            }
            Log.debug("ARENA PLUGIN: checking duelID for player logging out; ID is " + duelID);
            if (duelID != -1) {
            	//Map<Integer, Duel> duels = ArenaPlugin.getDuels();
            	//Duel duel = duels.get(duelID);
            	EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "factionOverride", "");
            	String name = WorldManagerClient.getObjectInfo(playerOid).name;
            	Log.debug("ARENA PLUGIN: removing player who is logging out from their duel");
            	ArenaClient.duelDisconnect(playerOid, name, duelID);
            }
            
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            return true;
        }
    }
    
    class SpawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.SpawnedMessage spawnedMsg = (WorldManagerClient.SpawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            OID instanceOid = spawnedMsg.getInstanceOid();
            
            ObjectInfo objInfo = WorldManagerClient.getObjectInfo(objOid);
            if (objInfo.objType == ObjectTypes.player) {
            	// Set the players world property
            	/*Log.debug("SPAWNED: setting world for player: " + objOid);
            	OID instanceOid = WorldManagerClient.getObjectInfo(objOid).instanceOid;
        	    String world = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_NAME).templateName;
        	    EnginePlugin.setObjectProperty(objOid, WorldManagerClient.NAMESPACE, "world", world);*/
            	
            	// Send down server current time
            	long timeDif = (int)(System.currentTimeMillis() - serverStartTime) / 1000;
            	Map<String, Serializable> props = new HashMap<String, Serializable>();
        		props.put("ext_msg_subtype", "server_time");
        		props.put("server_time_running", timeDif);
        		Calendar cal = Calendar.getInstance();
        		props.put("year", cal.get(Calendar.YEAR));
        		props.put("month", cal.get(Calendar.MONTH));
        		props.put("day", cal.get(Calendar.DAY_OF_MONTH));
        		props.put("hour", cal.get(Calendar.HOUR_OF_DAY));
        		props.put("minute", cal.get(Calendar.MINUTE));
        		props.put("second", cal.get(Calendar.SECOND));

        		TargetedExtensionMessage teMsg = new TargetedExtensionMessage(
        				WorldManagerClient.MSG_TYPE_EXTENSION, objOid, 
        				objOid, false, props);
        		Engine.getAgent().sendBroadcast(teMsg);
            }
            
            if (regions.containsKey(instanceOid)) {
            	Point p = WorldManagerClient.getObjectInfo(objOid).loc;
            	for (VolumetricRegion region : regions.get(instanceOid).values()) {
            		float distance = Point.distanceToXZ(p, new Point(region.getLoc()));
            		if (distance < region.getReactionRadius()) {
            			region.addPlayer(objOid);
            		}
            	}
            }
            
            return true;
    	}
    }
    
    /**
	 * Called when a player or mob despawns. 
	 */
	class DespawnedHook implements Hook	 {
		public boolean processMessage(Message msg, int flags) {
			WorldManagerClient.DespawnedMessage despawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
			OID objOid = despawnedMsg.getSubject();
			OID instanceOid = despawnedMsg.getInstanceOid();
			
			if (regions.containsKey(instanceOid)) {
				for (VolumetricRegion region : regions.get(instanceOid).values()) {
					region.removePlayer(objOid);
				}
			}
			return true;
		}
	}
	
	/**
     * Updates the movement state for the player/mob specified. Will update the follow terrain
     * property as well based on the new movement state.
     * @author Andrew
     *
     */
    class WaterRegionTransitionHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		WaterRegionTransitionMessage spawnMsg = (WaterRegionTransitionMessage)msg;
    		OID playerOid = spawnMsg.getSubject();
    		Integer waterRegion = spawnMsg.getRegionID();
    		Log.debug("WATER: handling water region transition for region " + waterRegion + " for " + playerOid);
    		//LinkedList<Integer> waterRegions = (LinkedList<Integer>) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "waterRegions");
    		//if (waterRegions == null)
    		//	waterRegions = new LinkedList<Integer>();
    		
    		if (spawnMsg.entering() /*&& !waterRegions.contains(waterRegion)*/) {
    			Log.debug("WATER: player entering new waterRegion");
    			//if (waterRegions.size() == 0) {
    				Log.debug("WATER: setting player to swimming");
    				EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, PROP_MOVEMENT_STATE, MOVEMENT_STATE_SWIMMING);
    				boolean followTerrain = false;
    				EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, PROP_FOLLOW_TERRAIN, followTerrain);
    				Entity entity = getWorldManagerEntity(playerOid);
    	    		// get the object's current world node
    	            AOObject obj = (AOObject) entity;
    	            InterpolatedWorldNode curWnode = (InterpolatedWorldNode) obj.worldNode();
    	            curWnode.setFollowsTerrain(followTerrain);
    			//}
    			//waterRegions.add(waterRegion);
    		} else if (!spawnMsg.entering()) {
    			//waterRegions.remove(waterRegion);
    			Log.debug("WATER: player leaving waterRegion");
    			//if (waterRegions.size() == 0) {
    				Log.debug("WATER: setting player to walking");
    				EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, PROP_MOVEMENT_STATE, MOVEMENT_STATE_RUNNING);
    				boolean followTerrain = true;
    				EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, PROP_FOLLOW_TERRAIN, followTerrain);
    				Entity entity = getWorldManagerEntity(playerOid);
    	    		// get the object's current world node
    	            AOObject obj = (AOObject) entity;
    	            InterpolatedWorldNode curWnode = (InterpolatedWorldNode) obj.worldNode();
    	            curWnode.setFollowsTerrain(followTerrain);
    			//}
    		}
    		//EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "waterRegions", waterRegions);
            
    		return true;
    	}
    }
    
    /**
     * Updates the movement state for the player/mob specified. Will update the follow terrain
     * property as well based on the new movement state.
     * @author Andrew
     *
     */
    class SetMovementStateHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		SetMovementStateMessage spawnMsg = (SetMovementStateMessage)msg;
    		OID playerOid = spawnMsg.getSubject();
    		int movementState = spawnMsg.getMovementState();
    		Log.debug("STATE: setting movement state to " + movementState + " for " + playerOid);
    		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, PROP_MOVEMENT_STATE, movementState);
    		// Set the follow terrain property based on the movement state
    		boolean followTerrain = true;
    		if (movementState != 1) {
    			followTerrain = false;
    		}
    		EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, PROP_FOLLOW_TERRAIN, followTerrain);
    		Entity entity = getWorldManagerEntity(playerOid);
    		// get the object's current world node
            AOObject obj = (AOObject) entity;
            InterpolatedWorldNode curWnode = (InterpolatedWorldNode) obj.worldNode();
            curWnode.setFollowsTerrain(followTerrain);
            
    		return true;
    	}
    }
    
    /**
     * Updates the movement state for the player/mob specified. Will update the follow terrain
     * property as well based on the new movement state.
     * @author Andrew
     *
     */
    class SetUnderwaterHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage spawnMsg = (ExtensionMessage)msg;
    		OID playerOid = spawnMsg.getSubject();
    		boolean underwater = (Boolean) spawnMsg.getProperty("underwater");
    		Log.debug("STATE: setting underwater to " + underwater + " for " + playerOid);
    		EnginePlugin.setObjectProperty(playerOid, CombatClient.NAMESPACE, "underwater", underwater);
            //CombatPlugin.getCombatInfo(playerOid);
    		return true;
    	}
    }
    
    /**
	 * Used for AoE abilities to find out which mobs/players are in an area.
	 *
	 */
    class GetTargetsInAreaHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	CombatClient.getAoeTargetsMessage message = (CombatClient.getAoeTargetsMessage) msg;
        	OID subjectOid = message.getSubject();
            
            BasicWorldNode subjectWorldNode = ((AOObject)getWorldManagerEntity(subjectOid)).baseWorldNode();
            OID instanceOid = subjectWorldNode.getInstanceOid();
            List<OID> objectsIn = null;
            if (instanceOid != null) {
                objectsIn = getInstanceObjectsIn(instanceOid, message.getLoc(), message.getRadius(), message.getObjectType());
            } else {
                objectsIn = getObjectsIn(message.getLoc(), message.getRadius(), message.getObjectType());
            }
            Engine.getAgent().sendObjectResponse(message, objectsIn);

            return true;
        }
    }
    
    private List<OID> getInstanceObjectsIn(OID instanceOid, Point loc, Integer radius, ObjectType objectType) {
    	Entity[] entities = EntityManager.getAllEntitiesByNamespace(Namespace.WORLD_MANAGER);
    	List<OID> objectsIn = new ArrayList<OID>();
    	if (objectType != null) {
    		for (Entity entity : entities) {
    			OID entityOid = entity.getOid();
    			AOObject obj = (AOObject)getWorldManagerEntity(entityOid);
    			if(obj != null){
    				BasicWorldNode entityWorldNode = obj.baseWorldNode();
    				if ((entity.getType().getTypeId() == objectType.getTypeId()) && (instanceOid.equals(entityWorldNode.getInstanceOid()))) {
    					Log.debug("[CYC][1] entityType: " + entity.getType() + ", objectType: " + objectType);
    					Point entityLoc = entityWorldNode.getLoc();
    					Log.debug("[CYC][1] loc: " + loc + ", entityLoc: " + entityLoc + ", entityName: " + entity.getName());
    					if (Math.round(Point.distanceToXZ(loc, entityLoc)) <= radius) {
    						objectsIn.add(entityOid);
    					}
    					Log.debug("[CYC][1] distance: " + Math.round(Point.distanceToXZ(loc, entityLoc)) + ", radius: " + radius);
    				}
    			}
    		}
    	} else {
    		for (Entity entity : entities) {
    			OID entityOid = entity.getOid();
    			BasicWorldNode entityWorldNode = ((AOObject)getWorldManagerEntity(entityOid)).baseWorldNode();
    			if (instanceOid.equals(entityWorldNode.getInstanceOid())) {
    				Log.debug("[CYC][2] entityType: " + entity.getType());
    				Point entityLoc = entityWorldNode.getLoc();
    				Log.debug("[CYC][2] loc: " + loc + ", entityLoc: " + entityLoc + ", entityName: " + entity.getName());
    				if (Math.round(Point.distanceToXZ(loc, entityLoc)) <= radius) {
    					objectsIn.add(entityOid);
    				}
    				Log.debug("[CYC][2] distance: " + Math.round(Point.distanceToXZ(loc, entityLoc)) + ", radius: " + radius);
    			}
    		}
    	}
    	return objectsIn;
    }
    
    private List<OID> getObjectsIn(Point loc, Integer radius, ObjectType objectType) {
    	Entity[] entities = EntityManager.getAllEntitiesByNamespace(Namespace.WORLD_MANAGER);
    	List<OID> objectsIn = new ArrayList<OID>();
    	if (objectType != null) {
    		for (Entity entity : entities) {
    			if (entity.getType().getTypeId() == objectType.getTypeId()) {
    				OID entityOid = entity.getOid();
    				BasicWorldNode entityWorldNode = ((AOObject)getWorldManagerEntity(entityOid)).baseWorldNode();
    				Point entityLoc = entityWorldNode.getLoc();
    				if (Math.round(Point.distanceToXZ(loc, entityLoc)) <= radius) {
    					objectsIn.add(entityOid);
    				}
    			}
    		}
    	} else {
    		for (Entity entity : entities) {
    			OID entityOid = entity.getOid();
    			BasicWorldNode entityWorldNode = ((AOObject)getWorldManagerEntity(entityOid)).baseWorldNode();
    			Point entityLoc = entityWorldNode.getLoc();
    			if (Math.round(Point.distanceToXZ(loc, entityLoc)) <= radius) {
    				objectsIn.add(entityOid);
    			}
    		}
    	}
    	return objectsIn;
    }

    // start running at 2.0m/sec
    public static Float defaultRunThreshold = 2.0f;
    
    /**
     * creates a DisplayContextMessage with notifyOid set as its MSG_OID.
     */
    protected void sendDCMessage(AOObject obj, boolean forceInstantLoad) {
        if (Log.loggingDebug)
            log.debug("sendDCMessage: obj=" + obj);

        if (!(obj instanceof AgisObject)) {
//            log.error("sendDCMessage: not a agisobj: " + obj);
            return;
        }
        DisplayContext dc = obj.displayContext();
        sendDCMessage(obj.getOid(), dc, forceInstantLoad);
    }
    
    /**
     * creates a DisplayContextMessage with notifyOid set as its MSG_OID.
     */
    protected void sendDCMessage(OID oid, DisplayContext dc, boolean forceInstantLoad) {
        if (Log.loggingDebug)
            log.debug("sendDCMessage: obj=" + oid);

        if (dc == null) {
            log.warn("sendDCMessage: obj has no dc: " + oid);
            return;
        }

        WorldManagerClient.DisplayContextMessage dcMsg = new WorldManagerClient.DisplayContextMessage(oid, dc);
        dcMsg.setForceInstantLoad(forceInstantLoad);
        Engine.getAgent().sendBroadcast(dcMsg);
    }

    /**
     * sends over health, int, str, etc.
     */
    protected void sendPropertyMessage(OID notifyOid, AOObject updateObj) {
        if (! (updateObj instanceof AgisObject)) {
            if (Log.loggingDebug)
                log.debug("AgisWorldManagerPlugin.sendPropertyMessage: skipping, obj is not agisobject: "
		          + updateObj);
            return;
        }
        AgisObject mObj = (AgisObject) updateObj;
        OID updateOid = updateObj.getMasterOid();

    	PropertyMessage propMessage = new PropertyMessage(updateOid, notifyOid);
    	for (String key : mObj.getPropertyMap().keySet()) {
            if (propertyExclusions.contains(key))
                continue;
            propMessage.setProperty((String)key, mObj.getProperty(key));
    	}

        // send the message
    	Log.debug("AgisWorldManagerPlugin.sendPropertyMessage: sending property message for obj=" + updateObj + " to=" + notifyOid + " msg=" + propMessage);
        Engine.getAgent().sendBroadcast(propMessage);
    }

    protected void sendTargetedPropertyMessage(OID targetOid,
        AOObject updateObj)
    {
        if (! (updateObj instanceof AgisObject)) {
            if (Log.loggingDebug)
                log.debug("AgisWorldManagerPlugin.sendTargetedPropertyMessage: skipping, obj is not agisobject: "
		          + updateObj);
            return;
        }
        AgisObject mObj = (AgisObject) updateObj;
        OID updateOid = updateObj.getMasterOid();

        TargetedPropertyMessage propMessage =
            new TargetedPropertyMessage(targetOid, updateOid);
        for (String key : mObj.getPropertyMap().keySet()) {
            if (propertyExclusions.contains(key))
                continue;
            propMessage.setProperty((String)key, mObj.getProperty(key));
        }

        // send the message
        Log.debug("AgisWorldManagerPlugin.sendTargetedPropertyMessage: subject=" + updateObj + " target=" + targetOid + " msg=" + propMessage);
        Engine.getAgent().sendBroadcast(propMessage);
    }

    /**
     * gets the current display context - used in the base world manager plugin
     * when it needs to send the display context to the proxy - this gets called
     * by the wmgr via the proxy upon logging in
     */
    protected DisplayContext getDisplayContext(OID objOid) {
	Entity entity = getWorldManagerEntity(objOid);
        if (entity == null) {
            return null;
        }
        if (!(entity instanceof AOObject)) {
            return null;
        }
        AOObject obj = (AOObject) entity;
        if (!(obj instanceof AgisObject)) {
            // its base object type, just send over its stored
            // displaycontext
            return obj.displayContext();
        }

        DisplayContext dc = AgisDisplayContext.createFullDisplayContext((AgisObject) obj);
        /*if (Log.loggingDebug)
            log.debug("AgisWorldManagerPlugin: get dc = " + dc);*/
        return dc;
    }
    
    HashMap<OID, Float> instanceGlobalWaterHeights = new HashMap<OID, Float>();
    HashMap<OID, HashMap<Integer, VolumetricRegion>> regions = new HashMap<OID, HashMap<Integer, VolumetricRegion>>();
    protected int waterHeight = Integer.MIN_VALUE;
    
    private long serverStartTime;
    private long currentSecondsRunning = 0;
    
    public static final int MOVEMENT_STATE_RUNNING = 1;
    public static final int MOVEMENT_STATE_SWIMMING = 2;
    public static final int MOVEMENT_STATE_FLYING = 3;
    
    public static final String PROP_FOLLOW_TERRAIN = "follow_terrain";
    public static final String PROP_MOVEMENT_STATE = "movement_state";
    public static final String PROP_MOVEMENT_SPEED = "movement_speed";
    public static final String PROP_ACTION_STATE = "action_state";
    
}
