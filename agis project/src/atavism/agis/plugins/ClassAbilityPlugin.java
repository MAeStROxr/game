/**
 *
 */
package atavism.agis.plugins;

import atavism.agis.core.Agis;
import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisSkill;
import atavism.agis.database.CombatDatabase;
import atavism.agis.database.ContentDatabase;
import atavism.agis.objects.*;
import atavism.agis.plugins.ClassAbilityClient.RewardExpForKillMessage;
import atavism.agis.plugins.CombatClient.AbilityUpdateMessage.Entry;
import atavism.agis.util.EventMessageHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageTypeFilter;
import atavism.server.engine.*;
import atavism.server.objects.Entity;
import atavism.server.objects.EntityManager;
import atavism.server.objects.Template;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.*;


public class ClassAbilityPlugin extends EnginePlugin {

	/**
     * Holds the list of professions
     */
    protected static HashMap<String, ProfessionObject> professions = new HashMap<String, ProfessionObject>();

    /*
     * This will hold the keying between ability start and ability completion
     */
    HashMap<Integer, ArrayList<OID>> playerabilitykey = new HashMap<Integer, ArrayList<OID>>();

    protected static Map<String, AgisStatDef> statDefMap = new HashMap<String, AgisStatDef>();

    public ClassAbilityPlugin() {
        super("ClassAbility");
        setPluginType("ClassAbility");
    }

    private static final Logger log = new Logger("ClassAbility");

    public void onActivate() {
        if (Log.loggingDebug)
            log.debug(this.getName() + " OnActivate Started");
        super.onActivate();
        if (Log.loggingDebug)
            log.debug(this.getName() + " base class onActivate ran");
        registerHooks();
        if (Log.loggingDebug)
            log.debug(this.getName() + " registered hooks");
        MessageTypeFilter filter = new MessageTypeFilter();
        filter.addType(CombatClient.MSG_TYPE_SKILL_UPDATE);
        filter.addType(CombatClient.MSG_TYPE_ABILITY_UPDATE);
        //filter.addType(CombatClient.MSG_TYPE_ABILITY_PROGRESS);
        filter.addType(CombatClient.MSG_TYPE_START_ABILITY);
        filter.addType(ClassAbilityClient.MSG_TYPE_HANDLE_EXP);
        filter.addType(CombatClient.MSG_TYPE_ALTER_EXP);
        filter.addType(CombatClient.MSG_TYPE_COMBAT_ABILITY_USED);
	    filter.addType(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_INCREASE);
	    filter.addType(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_DECREASE);
	    filter.addType(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_RESET);
	    filter.addType(ClassAbilityClient.MSG_TYPE_LEVEL_CHANGE);
	    filter.addType(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_ALTER_CURRENT);
	    filter.addType(ClassAbilityClient.MSG_TYPE_PURCHASE_SKILL_POINT);
	    filter.addType(ClassAbilityClient.MSG_TYPE_SET_SKILL_STATE);
	    filter.addType(ClassAbilityClient.MSG_TYPE_LEARN_ABILITY);
        filter.addType(CombatClient.MSG_TYPE_UPDATE_ACTIONBAR);
        Engine.getAgent().createSubscription(filter, this);
        
        // Create responder subscription
  	    MessageTypeFilter filter2 = new MessageTypeFilter();
  	    //filter2.addType(LoginMessage.MSG_TYPE_LOGIN);
  	    //filter2.addType(LogoutMessage.MSG_TYPE_LOGOUT);
  	    filter2.addType(ClassAbilityClient.MSG_TYPE_COMBAT_GET_SKILL);
  	    filter2.addType(ClassAbilityClient.MSG_TYPE_COMBAT_GET_PLAYER_SKILL_LEVEL);
  	    Engine.getAgent().createSubscription(filter2, this,
  	       MessageAgent.RESPONDER);

        registerLoadHook(Namespace.CLASSABILITY, new ClassAbilityLoadHook());
        registerSaveHook(Namespace.CLASSABILITY, new ClassAbilitySaveHook());

        this.registerPluginNamespace(ClassAbilityClient.NAMESPACE, new ClassAbilitySubObjectHook());

        // Get the ability list from the database.
        //this.buildAbilitiesList();

        if (Log.loggingDebug)
            log.debug(this.getName() + " activated");
        
        loadProgressionData();
    }

    public void registerHooks() {
        getHookManager().addHook(CombatClient.MSG_TYPE_ABILITY_UPDATE, new ClassAbilityAddAbilityHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_ABILITY_PROGRESS, new ClassAbilityAbilityProgressHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_START_ABILITY, new ClassAbilityStartAbilityHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_HANDLE_EXP, new ClassAbilityHandleXpHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_ALTER_EXP, new AlterExpHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_COMBAT_ABILITY_USED, new AbilityUsedHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_PURCHASE_SKILL_POINT, new PurchaseSkillPointHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_SET_SKILL_STATE, new SetSkillStateHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_INCREASE, new SkillIncreaseHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_DECREASE, new SkillDecreaseHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_RESET, new SkillResetHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_LEVEL_CHANGE, new LevelChangeHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_COMBAT_SKILL_ALTER_CURRENT, new IncreaseSkillCurrentHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_COMBAT_GET_SKILL, new SkillGetHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_COMBAT_GET_PLAYER_SKILL_LEVEL, new GetSkillLevelHook());
        getHookManager().addHook(ClassAbilityClient.MSG_TYPE_LEARN_ABILITY, new LearnAbilityHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_UPDATE_ACTIONBAR, new UpdateActionBarHook());
    }
    
    void loadProgressionData() {
    	CombatDatabase cDB = new CombatDatabase(false);
    	// Level xp amounts
        levelXpRequirements = cDB.loadLevelExpRequirements();
        cDB.close();
        // Stat progression
        ContentDatabase ctDB = new ContentDatabase(false);
        characterTemplates = ctDB.loadCharacterFactoryTemplates();
        ctDB.close();
        
        String useSkillPurchasePoints = ctDB.loadGameSetting("USE_SKILL_PURCHASE_POINTS");
		if (useSkillPurchasePoints != null)
			USE_SKILL_PURCHASE_POINTS = Boolean.parseBoolean(useSkillPurchasePoints);
		String skillPointsPerLevel = ctDB.loadGameSetting("SKILL_POINTS_GIVEN_PER_LEVEL");
		if (skillPointsPerLevel != null)
			SKILL_POINTS_GIVEN_PER_LEVEL = Integer.parseInt(skillPointsPerLevel);
		String useSkillMax = ctDB.loadGameSetting("USE_SKILL_MAX");
		if (useSkillMax != null)
			USE_SKILL_MAX = Boolean.parseBoolean(useSkillMax);
		String skillStartingMax = ctDB.loadGameSetting("SKILL_STARTING_MAX");
		if (skillStartingMax != null)
			SKILL_STARTING_MAX = Integer.parseInt(skillStartingMax);
		String expMaxLevelDifference = ctDB.loadGameSetting("EXP_MAX_LEVEL_DIFFERENCE");
		if (expMaxLevelDifference != null)
			EXP_MAX_LEVEL_DIFFERENCE = Integer.parseInt(expMaxLevelDifference);
    }

    public class ClassAbilitySubObjectHook extends GenerateSubObjectHook {

        public ClassAbilitySubObjectHook(){ super(ClassAbilityPlugin.this); }

        public SubObjData generateSubObject(Template template, Namespace namespace, OID masterOid) {
            if (Log.loggingDebug)
                log.debug("GenerateSubObjectHook: masterOid=" + masterOid
                          + ", template=" + template);

            if(masterOid == null) {
                log.error("GenerateSubObjectHook: no master oid");
                return null;
            }

            if (Log.loggingDebug)
                log.debug("GenerateSubObjectHook: masterOid="+masterOid+", template="+template);

            Map<String, Serializable> props = template.getSubMap(ClassAbilityClient.NAMESPACE);

            if (props == null) {
                Log.warn("GenerateSubObjectHook: no props in ns "
                         + ClassAbilityClient.NAMESPACE);
                return null;
            }

            // generate the subobject
            ClassAbilityObject tinfo = new ClassAbilityObject(masterOid);
            tinfo.setName(template.getName());

            Boolean persistent = (Boolean)template.get(Namespace.OBJECT_MANAGER,
                                                       ObjectManagerClient.TEMPL_PERSISTENT);
            if (persistent == null)
                persistent = false;
            tinfo.setPersistenceFlag(persistent);

            // copy properties from template to object
            for (Map.Entry<String, Serializable> entry : props.entrySet()) {
                String key = entry.getKey();
                Serializable value = entry.getValue();
                if (!key.startsWith(":")) {
                    tinfo.setProperty(key, value);
                }
            }

            tinfo.setPlayerClass((String)tinfo.getProperty("class"));

            if (Log.loggingDebug)
                log.debug("GenerateSubObjectHook: created entity " + tinfo);

            // register the entity
            EntityManager.registerEntityByNamespace(tinfo, ClassAbilityClient.NAMESPACE);

            //send a response message
            return new SubObjData();
        }
    }

    public class ClassAbilityAddAbilityHook implements Hook {
        public boolean processMessage(Message msg, int flags) {

            CombatClient.AbilityUpdateMessage reqMsg = (CombatClient.AbilityUpdateMessage) msg;
            // extract the base information
            List<Entry> skilllist = reqMsg.getAbilities();
            OID oid = reqMsg.getSubject();

            ClassAbilityObject caobj = (ClassAbilityObject)EntityManager.getEntityByNamespace(oid, ClassAbilityClient.NAMESPACE);
            if (caobj == null)
                return true;
            String playerclass = caobj.getPlayerClass();

            for (Entry e : skilllist) {

                log.debug("Adding ability to the player: " + oid + " ability: " + e.abilityID);

                // do a couple of double checks..
                if (Agis.AbilityManager.keySet().contains(e.abilityID)) {
                    // okay it is a valid system skill, now make sure it is in our list
                    // but first we need the player's object for classability

                    if (playerclass == null) {
                        // they didn't define a class for this character, so we can't track stats for them
                        log.warn("They didn't define a class type for this player...");
                        return true;
                    }

                    // now use the player's class to get the list of skills avaiable
                    if (professions.get(playerclass).hasAbility(e.abilityID)) {
                        if (caobj.getProperty(e.abilityID + "_exp") == null) {
                            // this is valid skill for tracking experience. So generate an agisstat for this skill
                            createStats(caobj, Agis.AbilityManager.get(e.abilityID), professions.get(playerclass).getAbility(e.abilityID).getExperiencePerUse());
                        }
                    }
                }
            }
            // return true so that the chain continues if there is more
            return true;
        }
    }

    /**
     * This method creates stats based on the passed in skill, and assigns them to the player via the
     * created players' ClassAbilityObject.
     *
     * @param caobj
     * @param skill
     * @param xp_use
     */
    public static void createStats(ClassAbilityObject caobj, AgisSkill skill, Integer xp_use) {
        AgisStat tmp_exp = new AgisStat(skill.getName() + "_exp");
        tmp_exp.min = tmp_exp.current = tmp_exp.base = 0 ;
        tmp_exp.max = skill.getBaseExpThreshold();

        AgisStat tmp_rank = new AgisStat(skill.getName() + "_rank");
        tmp_rank.min = tmp_rank.current = tmp_rank.base = 0;
        tmp_rank.max = skill.getMaxRank();

        caobj.setProperty(skill.getName() + "_exp", tmp_exp);
        caobj.setProperty(skill.getName() + "_rank", tmp_rank);
        caobj.setProperty(skill.getName(), xp_use);
    }

    /**
     * This method creates stats based on the passed in ability, and assigns them to the player via the
     * created players' ClassAbilityObject.
     *
     * @param caobj
     * @param ability
     * @param xp_use
     */
    public static void createStats(ClassAbilityObject caobj, AgisAbility ability, Integer xp_use) {
        AgisStat tmp_exp = new AgisStat(ability.getName() + "_exp");
        tmp_exp.min = tmp_exp.current = tmp_exp.base = 0 ;
        tmp_exp.max = ability.getBaseExpThreshold(); // TODO: Replace this value with a modifiable one

        AgisStat tmp_rank = new AgisStat(ability.getName() + "_rank");
        tmp_rank.min = tmp_rank.current = tmp_rank.base = 0;
        tmp_rank.max = ability.getMaxRank(); // TODO: Replace this value with a modifiable one

        caobj.setProperty(ability.getName() + "_exp", tmp_exp);
        caobj.setProperty(ability.getName() + "_rank", tmp_rank);
        caobj.setProperty(ability.getName(), xp_use);
    }

    /**
     * Register the stat with the specific player, since only the player themselves have to be aware of what stats they
     * should be paying attention to.
     *
     * @param stat
     */
    public static void registerStat(AgisStatDef stat) {
        registerStat(stat, new String[0]);
    }

    public static void registerStat(AgisStatDef stat, String... dependencies) {
        String statName = stat.getName();
        if (!statDefMap.containsKey(statName)) {
            statDefMap.put(statName, stat);
            for (String depName : dependencies) {
                AgisStatDef depStat = statDefMap.get(depName);
                if (depStat != null) {
                    depStat.addDependent(stat);
                } else {
                    Log.error("no stat definition for dependency " + depName
                              + " of stat " + statName);
                }
            }
        }
    }

    /**
     * This method allows registering a profession.
     *
     * @param profession
     */
    public static void registerProfession(ProfessionObject profession) {
        log.debug("Registering Profession: " + profession);
        professions.put(profession.getName(), profession);
    }

    public static AgisStatDef lookupStatDef(String name) {
        return statDefMap.get(name);
    }

    public static void sendSkillUpdate(CombatInfo info) {
        // extract the base information
       // SkillInfo skillInfo = info.getCurrentSkillInfo();
        OID oid = info.getOwnerOid();

        //Setup message to send to client
        TargetedExtensionMessage updateMsg = new TargetedExtensionMessage(info.getOwnerOid());
        updateMsg.setExtensionType("ao.SKILL_UPDATE");

        ClassAbilityObject caobj = (ClassAbilityObject)EntityManager.getEntityByNamespace(oid, ClassAbilityClient.NAMESPACE);
        if (caobj == null)
            return;
        //String playerclass = caobj.getPlayerClass();

        //Create xp and rank stats for any new skills
        /*for (int skillID : skillInfo.getSkills().keySet()){

            log.debug("Adding skill to the player: " + oid + " skill: " + skillInfo);

            // do a couple of double checks..
            if (Agis.SkillManager.keySet().contains(skillID)){
                // okay it is a valid system skill, now make sure it is in our list
                // but first we need the player's object for classability

                if (playerclass == null){
                    // they didn't define a class for this character, so we can't track stats for them
                    log.warn("They didn't define a class type for this player...");
                    return;
                }
                log.debug(professions.get(playerclass).toString());
                // now use the player's class to get the list of skills avaiable
                if (professions.get(playerclass).hasSkill(skillID)){
                    if (caobj.getProperty(skillID + "_exp") == null){
                        // this is valid skill for tracking experience. So generate an agisstat for this skill
                        //createStats(caobj, Agis.SkillManager.get(skillID), 
                        //		professions.get(playerclass).getSkill(skillID).getExperiencePerUse());
                    }
                }

                //Add skill to update message - Skill Name and Current Rank
                HashMap<String,Serializable> skillData = new HashMap<String,Serializable> ();
                skillData.put("id",skillID);
                skillData.put("rank",((AgisStat)caobj.getProperty(skillID + "_rank")).current);
                updateMsg.setProperty("Skill_"+skillID, skillData);
            }
        }*/

        //Send update to the client on new skills
        Engine.getAgent().sendBroadcast(updateMsg);
    }

    class ClassAbilityStartAbilityHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            CombatClient.StartAbilityMessage abilityMsg = (CombatClient.StartAbilityMessage) msg;
            OID oid = abilityMsg.getSubject();
            int abilityID = abilityMsg.getAbilityID();

            log.debug("Processing Start Ability Message: " + oid + ", ability: " + abilityID);

            // Check to see the ability is already in the hashmap
            if (playerabilitykey.containsKey(abilityID)) {
                // put the player in if not in there
                if (!playerabilitykey.get(abilityID).contains(oid)) {
                    playerabilitykey.get(abilityID).add(oid);
                }
            }
            else {
                // need to add the ability and the player
                ArrayList<OID> a = new ArrayList<OID>();
                a.add(oid);
                playerabilitykey.put(abilityID, a);
            }

            log.debug("PlayerAbilityKey list for " + abilityID + " : [ " + playerabilitykey.get(abilityID).toString() + " ]");

            return true;
        }
    }

    class ClassAbilityAbilityProgressHook implements Hook{
        public boolean processMessage(Message msg, int flags) {
            // Convert the message, then get the pertinent information
            CombatClient.AbilityProgressMessage abilityMsg = (CombatClient.AbilityProgressMessage) msg;
            int abilityID = abilityMsg.getAbilityID();
            String state = abilityMsg.getState();

            log.debug("Processing Progress Ability Message: " + state + ":" + AgisAbility.ActivationState.COMPLETED +", ability: " + abilityID);

            if (state.equals(AgisAbility.ActivationState.COMPLETED.toString())) {
                // now we need to find a matching oid from our key list
                ArrayList<OID> oids = playerabilitykey.get(abilityID);
                if (oids == null){ return true; }       // there are no oids associated with this ability
                log.debug("Getting OIDS: [ " + oids.toString() + " ]");
                for( OID oid : oids) {
                    // now load the combat namespace for this oid so we can find out if the current state is completed or not
                    CombatInfo ci = (CombatInfo)EntityManager.getEntityByNamespace(oid, CombatClient.NAMESPACE);
                    if (ci == null)
                        continue;
                    log.debug("Checking the current action state for " + ci.getName() +  " ( " + ci.getOid() + " ) : " + ci.getCurrentAction());
                    if (ci.getCurrentAction() == null) {
                        // we found one that is completed so pull in the classabilityobject and complete the task
                        ClassAbilityObject caobj = (ClassAbilityObject)EntityManager.getEntityByNamespace(oid, ClassAbilityClient.NAMESPACE);
                        AgisAbility ability = professions.get(caobj.getPlayerClass()).getAbility(abilityID);

                        if (ability != null) { // this is not a valid profession ability for this player
                            //Get associated skill information
                            AgisSkill skill = ability.getRequiredSkill();
                            if(skill != null){
                                // Make sure the player's profession has this skill
                                if(professions.get(caobj.getPlayerClass()).getSkillMap().get(skill.getName()) != null)
                                    //Update skill
                                    caobj.updateBaseStat(skill.getID(), skill.getExperiencePerUse());
                            }
                                 // now increment the ability
                                 caobj.updateBaseStat(abilityID, ability.getExperiencePerUse());
                            }
                        }
                    }
                }

            return true;
        }
    }

    class ClassAbilityLoadHook implements LoadHook {
        public void onLoad(Entity e) {

        }
    }

    class ClassAbilitySaveHook implements SaveHook {
        public void onSave(Entity e, Namespace namespace) {
        }
    }

    public static ClassAbilityObject getClassAbilityObject(OID oid) {
        log.debug("Checking the data for oid: " + oid);
        Entity entity = EntityManager.getEntityByNamespace(oid, Namespace.CLASSABILITY);
        log.debug("What is this entity type? " + entity.getType() + " and Name? " + entity.getName() + " and OID: " + entity.getOid());
        return (ClassAbilityObject)entity;
    }

    public class ClassAbilityHandleXpHook implements Hook{
    	public boolean processMessage(Message msg, int flags) {
    		RewardExpForKillMessage xpUpdateMsg = (RewardExpForKillMessage)msg;
    		
    		CombatInfo target = CombatPlugin.getCombatInfo(xpUpdateMsg.getTargetOid());
    		if (target == null)
    			return true;
    		
    		if (xpUpdateMsg.getAttackers().size() > 1) {
    			int totalDamage = 0;
    			for (int damage : xpUpdateMsg.getAttackers().values()) {
    				totalDamage += damage;
    			}
    			
    			float percentPerDamage = 1 / (float)totalDamage;
    			for (OID attackerOid : xpUpdateMsg.getAttackers().keySet()) {
    				rewardExpForKill(target, attackerOid, percentPerDamage * (float)xpUpdateMsg.getAttackers().get(attackerOid));
    			}
    		} else {
    			OID attackerOid = xpUpdateMsg.getAttackers().keySet().iterator().next();
    			rewardExpForKill(target, attackerOid, 1f);
    		}
        
    		return true;
    	}
    }
    
    /**
     * This function is used when a target dies to give the player XP from the kill.
     *
     * It is in the CombatAPI to allow for modification as systems are added to the combat.
     *
     * @param attacker
     * @param target
     * @param numAttackers
     */
    public static void rewardExpForKill(CombatInfo target, OID attackerOid, float expPercent) {
        // pull the xp value from the target
        Integer xpPercent = (Integer)target.getProperty(KILL_EXP_STAT);
        
        int targetLevel = target.statGetCurrentValue("level");
        log.debug("ANDREW - handle xp");

        if (xpPercent == null){ return; } // this target has no xp to gain
        LinkedList<OID> handledOids = new LinkedList<OID>();
                  
        CombatInfo attacker = CombatPlugin.getCombatInfo(attackerOid);
        if (attacker.isMob())
        	return;
            
        // If the attacker is grouped then all group members should get xp also
        HashSet<OID> groupMembers = new HashSet<OID>();
        if (attacker.isGrouped()){
            groupMembers = GroupClient.GetGroupMemberOIDs(attackerOid).memberOidSet;
            expPercent = (expPercent / (float)groupMembers.size()) + 0.1f;
            log.debug("GROUP: xp hit, group members: " + groupMembers);
            for(OID groupMemberOid : groupMembers){
                // If the group member is in the attackers list or already handled list, 
                //  then do not set their xp here
                if(!handledOids.contains(groupMemberOid)){
                    CombatInfo groupMember = CombatPlugin.getCombatInfo(groupMemberOid);
                     log.debug("GROUP: xp hit, giving exp to: " + groupMember.getOwnerOid());
                    // now apply the value to the group member
                    //groupMember.statModifyBaseValue(EXPERIENCE_STAT, xpPercent);
                     int attackerLevel = attacker.statGetCurrentValue(LEVEL_STAT);
                     if (attackerLevel - targetLevel <= EXP_MAX_LEVEL_DIFFERENCE) {
                    	 double xpval = (65* Math.pow(1.1, targetLevel))*(Math.pow(0.95, Math.abs(targetLevel-attackerLevel))) * expPercent;
                    	 int xpValInt = (int) xpval;
                    	 giveExp(groupMemberOid, xpValInt);
                     }
                    // Add to handled list to ensure the OID is not processed more than once
                    handledOids.add(groupMemberOid);
                    //ClassAbilityClient.sendXPUpdate(groupMemberOid, EXPERIENCE_STAT, groupMember.statGetCurrentValue(EXPERIENCE_STAT));            
                }
            }
        } else {
        	log.debug("ANDREW - handle xp 2");
        	int attackerLevel = attacker.statGetCurrentValue(LEVEL_STAT);
            double xpval = (65* Math.pow(1.1, targetLevel))*(Math.pow(0.95, Math.abs(targetLevel-attackerLevel))) * expPercent;
            int xpValInt = (int) xpval;
            giveExp(attackerOid, xpValInt);
        }
    }
    
    static private void giveExp(OID oid, int exp) {
    	Log.debug("EXP: exp alter hit, oid: " + oid + "; xp amount: " + exp);
	    CombatInfo info = CombatPlugin.getCombatInfo(oid);
	    int level = info.statGetCurrentValue("level");
	    if (!levelXpRequirements.containsKey(level) || exp == 0) {
	    	return;
	    }
	    int curXP = info.statGetCurrentValue(EXPERIENCE_STAT);
        int maxXP = levelXpRequirements.get(level);
        curXP = curXP + exp;
        
        WorldManagerClient.sendObjChatMsg(oid, 2, "You have received: " + exp + " experience points.");
        while (curXP > maxXP) {
        	// level up!
        	curXP = curXP - maxXP;
        	level = level + 1;
        	handleLevelUp(info, level);
        	maxXP = levelXpRequirements.get(level);
        }
        info.statSetBaseValue("experience", curXP);
        Log.debug("EXP: experience stat is now: " + info.statGetBaseValue("experience") + ", " + curXP);
        
        String abilityEvent = EventMessageHelper.COMBAT_EXP_GAINED;
        EventMessageHelper.SendCombatEvent(info.getOwnerOid(), info.getOwnerOid(), abilityEvent, -1, -1, exp, -1);
    }
    
    /**
     * This method calculates what a players stats should be based on their new level
     * @param player - the combat Info of the player levelling up
     * @param newLevel - the level the player is now
     */
    public static void handleLevelUp(CombatInfo player, int newLevel) {
    	Log.debug("ANDREW - level up. new level: " + newLevel);
    	if (levelXpRequirements.containsKey(newLevel)) {
    		player.statSetBaseValue(EXPERIENCE_MAX_STAT, levelXpRequirements.get(newLevel));
    	}
    	
    	// Get the character template based on race and class
    	int race = (Integer)EnginePlugin.getObjectProperty(player.getOwnerOid(), WorldManagerClient.NAMESPACE, "race");
    	int aspect = player.aspect();
    	CharacterTemplate tmpl = characterTemplates.get(race + " " + aspect);
    	// Increment their level and recalculate their stats
    	//player.statModifyBaseValue("dmg-base", 1);
    	player.statSetBaseValue("level", newLevel);
    	calculatePlayerStats(player, tmpl);
    	
    	//TODO: set vitality stats to max on level up?
    	
    	ClassAbilityClient.levelChange(player.getOwnerOid(), newLevel);

    	WorldManagerClient.sendObjChatMsg(player.getOwnerOid(), 2,
			    "Congratulations, you have reached level " + newLevel + "!");
    	
    	CoordinatedEffect cE = new CoordinatedEffect("LevelUpEffect");
    	cE.sendSourceOid(true);
    	cE.invoke(player.getOwnerOid(), player.getOwnerOid());
    }
    
    public static void calculatePlayerStats(CombatInfo info, CharacterTemplate tmpl) {
    	int level = info.statGetBaseValue("level");
    	// Update every stat based on the progression data for each stat the template has
    	for (String stat : tmpl.getStartingStats().keySet()) {
    		if (CombatPlugin.statDefMap.get(stat) instanceof VitalityStatDef)
    			continue;
    		int baseValue = tmpl.getStartingStats().get(stat).baseValue;
    		float levelIncrease = tmpl.getStartingStats().get(stat).levelIncrease;
    		float levelPercentIncrease = tmpl.getStartingStats().get(stat).levelPercentIncrease;
    		float value = baseValue + ((level-1) * levelIncrease);
			value += value * ((level-1) * (levelPercentIncrease / 100));
			
			if (info.getProperty(stat) == null) {
				// Add the stat to the player
				AgisStat newStat = new AgisStat(stat);
				info.setProperty(stat, newStat);
			}
			info.statSetBaseValue(stat, (int)value);
    	}
    	if (levelXpRequirements.containsKey(level)) {
    		info.statSetBaseValue(EXPERIENCE_MAX_STAT, levelXpRequirements.get(level));
    	}
    }

    /**
     * This method handles leveling the player profession based on the level that they have reached.
     *
     * Modifications are based on the leveling map for the profession in question as well as the stat's
     * own leveling map, if applicable.
     *
     * @param player
     * @param lvl
     */
    public static void handleLevelingProfessionPlayer(CombatInfo player, int lvl) {
        // first we need to get a hold of the players profession if it exists
        String profession = null;
        try {
            profession = (String)EnginePlugin.getObjectProperty(player.getOid(), Namespace.WORLD_MANAGER, "class");
        } catch(Exception e)  {

        }
        if (profession == null) {
            return;
        }
        ProfessionObject po = professions.get(profession);

        // now now get the leveling map for the profession
        LevelingMap lm = po.getLevelingMap();

        // now go through every proprety of the combatinfo and check to see if it is an agis stat or not
        for (String propname : player.getPropertyMap().keySet()) {
            // check to make sure we aren't dealing with the experience stat (as that controls leveling)
            // that this is indeed a base stat, and that it is also an instance of the AgisStat (in case something
            // went wrong and it was never created as a AgisStat).
            if (!propname.equals("experience") && po.isBaseStat(propname) && player.getProperty(propname) instanceof AgisStat) {
                // this is an agis stat so this is something maybe modified according to the profession map
                // do initial modification of full stats based on leveling map 0
                log.debug("Leveling up stat " + propname);
                AgisStat stat = (AgisStat)player.getProperty(propname);
                // this update call could be done when stats are finishing being setup. so bypass..
                log.debug("Leveling up " + propname + " : " + stat);
                int checker = stat.base; // setup a catch to see if we need to mark it as dirty or not at the end

                log.debug("Base " + propname + " base stat: " + stat.base);
                stat.base += (new Float((stat.base * lm.getLevelPercentageModification(0)))).intValue() + lm.getLevelFixedAmountModification(0);
                log.debug(propname + " base stat after global modification: " + stat.base);

                // check to see if there is a leveling map for this level
                if (lm.hasLevelModification(lvl)) {
                    // now apply it to the stats as well
                    stat.base += (new Float((stat.base * lm.getLevelPercentageModification(lvl)))).intValue() + lm.getLevelFixedAmountModification(lvl);
                    log.debug(propname + " base stat after level modification: " + stat.base);
                }

                // now to find out if this stat has it's own level map
                if (po.hasStatLevelModification(propname, lvl)) {
                    // we do have a level modification for this stat, so lets make it happen
                    LevelingMap statlm = po.getStatsLevelingMap(propname);
                    stat.base += (new Float((stat.base * statlm.getLevelPercentageModification(lvl)))).intValue() + statlm.getLevelFixedAmountModification(lvl);
                    log.debug(propname + " base stat after stat modification: " + stat.base);
                }

                log.debug(propname + " checking comparison: " + checker + " : " + stat.base);
                if (checker != stat.base) {
                    stat.current = stat.max = stat.base;
                    // the stat has changed value
                    player.setProperty(propname, stat);
                    stat.setDirty(true);
                    log.debug(propname + " updating base stat def");
                    CombatPlugin.getBaseStatDef(propname).update(stat, player);
                }
            }
        }
    }

    public static void handleSkillAbilityRanking(ClassAbilityObject player, int statid, int lvl) {
        /*LevelingMap lm;

        // In this case the skill/ability itself stores the leveling map so grab it from the stat.
        if (Agis.SkillManager.get(statid) != null){
            //lm = Agis.SkillManager.get(statid).getLevelingMap();
        }
        else if (Agis.AbilityManager.get(statid) != null) {
            lm = Agis.AbilityManager.get(statid).getLevelingMap();
        }
        else {
            // Nothing was found, so do nothing..
            return;
        }

        // Now grab the stat that is to be modified
        AgisStat stat = (AgisStat)player.getProperty(statid + "_exp");

        // this update call could be done when stats are finishing being setup. so bypass..
        log.debug("Leveling up " + statid + " : " + stat);
        int checker = stat.max; // setup a catch to see if we need to mark it as dirty or not at the end

        log.debug("Max " + statid + " max stat: " + stat.max);
        stat.max += (new Float((stat.max * lm.getLevelPercentageModification(0)))).intValue() + lm.getLevelFixedAmountModification(0);
        log.debug(statid + " max stat after global modification: " + stat.max);

        // check to see if there is a leveling map for this level
        if (lm.hasLevelModification(lvl)) {
            // now apply it to the stats as well
            stat.max += (new Float((stat.max * lm.getLevelPercentageModification(lvl)))).intValue() + lm.getLevelFixedAmountModification(lvl);
            log.debug(statid + " max stat after level modification: " + stat.max);
        }

        log.debug(statid + " checking comparison: " + checker + " : " + stat.max);
        if (checker != stat.max) {
            // the stat has changed value
            player.setProperty(statid+"_exp", stat);
            stat.setDirty(true);
            log.debug(statid + " updating base stat def");
            ClassAbilityPlugin.lookupStatDef(statid+"_exp").update(stat, player);
        }*/
    	return;
    }
    
    class AlterExpHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    CombatClient.alterExpMessage QERMsg = (CombatClient.alterExpMessage) msg;
    	    OID oid = QERMsg.getSubject();
    	    int xpReward = QERMsg.getXpAmount();
    	    giveExp(oid, xpReward);
    	    return true;
    	}
    }
    
    /*
     * Skill Progression Code.
     * This may be moved later on.
     */
    
    class PurchaseSkillPointHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage eMsg = (ExtensionMessage) msg;
            OID oid = eMsg.getSubject();
            Log.debug("SKILL: got purchase skill point for: " + oid);
            CombatInfo info = CombatPlugin.getCombatInfo(oid);
            SkillInfo sInfo = info.getCurrentSkillInfo();
            // work out cost based on how many points the player has bought
            int totalPoints = sInfo.getPointsSpent() + sInfo.getSkillPoints();
            int cost = GetSkillPointCost(totalPoints);
            // Does the player have enough xp to buy a point?
            int curXP = info.statGetCurrentValue("experience");
            if (curXP < cost) {
            	Log.debug("SKILL: player: " + oid + " does not have enough experience to purchase a skill point");
            	return true;
            }
            sInfo.setSkillPoints(sInfo.getSkillPoints() + 1);
            info.statSetBaseValue("experience", curXP - cost);
            Log.debug("SKILL: player: " + oid + " has purchased a skill point");
            ExtendedCombatMessages.sendSkills(oid, info.getCurrentSkillInfo());
            return true;
    	}
    }
    
    public static int GetSkillPointCost(int pointsPurchased) {
    	return pointsPurchased * 100 + 100;
    }
    
    class SetSkillStateHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		ExtensionMessage eMsg = (ExtensionMessage) msg;
            OID oid = eMsg.getSubject();
            Log.debug("SKILL: got set skill state for: " + oid);
            int skillID = (Integer) eMsg.getProperty("skillID");
            int state = (Integer) eMsg.getProperty("state");
            CombatInfo info = CombatPlugin.getCombatInfo(oid);
            SkillInfo sInfo = info.getCurrentSkillInfo();
            SkillData skillData = sInfo.getSkills().get(skillID);
            skillData.setState(state);
            Engine.getPersistenceManager().setDirty(info);
            
            Log.debug("SKILL: player: " + oid + " has changed the state of a skill to: " + skillData.getState());
            ExtendedCombatMessages.sendSkills(oid, info.getCurrentSkillInfo());
            return true;
    	}
    }
    
    class SkillIncreaseHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.skillIncreasedMessage EBMsg = (ClassAbilityClient.skillIncreasedMessage) msg;
    		OID oid = EBMsg.getSubject();
    		// Check if the server is set to use Skill Points
    		if (!USE_SKILL_PURCHASE_POINTS) {
    			Log.error("Player " + oid + " has tried to use skill points, but this feature is not activated");
    			return true;
    		}
    	    
    	    int skillType = EBMsg.getSkillID();
    	    Log.debug("SKILL: got SkillIncreaseMessage with skill: " + skillType);
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    SkillInfo.increaseSkill(cInfo.getCurrentSkillInfo(), skillType, cInfo.aspect(), cInfo);
    	    ExtendedCombatMessages.sendSkills(oid, cInfo.getCurrentSkillInfo());
    		return true;
    	}
    }
    
    class SkillDecreaseHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.skillDecreasedMessage EBMsg = (ClassAbilityClient.skillDecreasedMessage) msg;
    	    OID oid = EBMsg.getSubject();
    	    int skillType = (Integer) EBMsg.getProperty("skillType");
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    SkillInfo.decreaseSkill(cInfo.getCurrentSkillInfo(), skillType, cInfo.aspect(), cInfo);
    	    ExtendedCombatMessages.sendSkills(oid, cInfo.getCurrentSkillInfo());
    	    return true;
    	}
    }
    
    class AbilityUsedHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    	    CombatClient.abilityUsedMessage EBMsg = (CombatClient.abilityUsedMessage) msg;
    	    OID oid = EBMsg.getSubject();
    	    int skillType = (Integer) EBMsg.getProperty("skillType");
    	    Log.debug("SKILL: Ability used of skill type: " + skillType);
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    SkillInfo.skillUpAttempt(cInfo.getCurrentSkillInfo(), skillType, cInfo);
    	    ExtendedCombatMessages.sendSkills(oid, cInfo.getCurrentSkillInfo());
    	    return true;
    	}
    }
    
    class SkillResetHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.skillResetMessage EBMsg = (ClassAbilityClient.skillResetMessage) msg;
    	    OID oid = EBMsg.getSubject();
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    SkillInfo.resetSkills(cInfo.getCurrentSkillInfo(), cInfo);
    	    ExtendedCombatMessages.sendSkills(oid, cInfo.getCurrentSkillInfo());
    		return true;
    	}
    }
    
    class SkillGetHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.skillGetMessage EBMsg = (ClassAbilityClient.skillGetMessage) msg;
    		int templateNum = (Integer) EBMsg.getProperty("templateNum");
    		Log.debug("ANDREW: Getting skill template");
    		Engine.getAgent().sendObjectResponse(msg, Agis.SkillManager.get(templateNum));
    		return true;
    	}
    }
    
    /**
     * Hook for the GetPlayerSkillLevelMessage. Gets the level of the specified skill for the player in question
     * and sends it back to the remote call procedure that sent the message out.
     * @author Andrew Harrison
     *
     */
    class GetSkillLevelHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.GetPlayerSkillLevelMessage EBMsg = (ClassAbilityClient.GetPlayerSkillLevelMessage) msg;
    		Log.debug("SKILL: got GetPlayerSkillLevelMessage");
    		OID oid = EBMsg.getSubject();
    	    int skillType = EBMsg.getSkillType();
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    int skillLevel = -1;
    	    if (cInfo.getCurrentSkillInfo().getSkills().containsKey(skillType)) {
    	    	skillLevel = cInfo.getCurrentSkillInfo().getSkills().get(skillType).getSkillLevel();
    	    }
    		
    		Engine.getAgent().sendIntegerResponse(msg, skillLevel);
    		return true;
    	}
    }
    
    class LevelChangeHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.levelChangeMessage EBMsg = (ClassAbilityClient.levelChangeMessage) msg;
    		OID oid = EBMsg.getSubject();
    		int newLevel = EBMsg.getLevel();
    		CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    		if (USE_SKILL_PURCHASE_POINTS) {
    			SkillInfo.levelChanged(cInfo.getCurrentSkillInfo(), cInfo, newLevel);
    		}
    	    ExtendedCombatMessages.sendSkills(oid, cInfo.getCurrentSkillInfo());
            return true;
    	}
    }
    
    class IncreaseSkillCurrentHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.skillAlterCurrentMessage EBMsg = (ClassAbilityClient.skillAlterCurrentMessage) msg;
    	    OID oid = EBMsg.getSubject();
    	    int skillType = EBMsg.getSkillType();
    	    int alterValue = EBMsg.getAlterAmount();
    	    Log.debug("SKILL: Ability used of skill type: " + skillType);
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    //SkillInfo.increaseSkillCurrent(cInfo.getCurrentSkillInfo(), skillType, alterValue, cInfo);
    	    for (int i = 0; i < alterValue; i++) {
    	    	SkillInfo.increaseSkill(cInfo.getCurrentSkillInfo(), skillType, cInfo.aspect(), cInfo);
    	    }
    	    ExtendedCombatMessages.sendSkills(oid, cInfo.getCurrentSkillInfo());
    	    return true;
    	}
    }
    
    /**
     * Adds the given ability to the players list of known abilities.
     * @author Andrew Harrison
     *
     */
    class LearnAbilityHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ClassAbilityClient.LearnAbilityMessage laMsg = (ClassAbilityClient.LearnAbilityMessage) msg;
    	    OID oid = laMsg.getSubject();
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    SkillInfo.learnAbility(cInfo, laMsg.getAbilityID());
    	    return true;
    	}
    }
    
    /**
     * Hook for the UpdateActionBarMessage. Attempts to add the new action to the players
     * action bar then sends down the action bar info to the player.
     * @author Andrew Harrison
     *
     */
    class UpdateActionBarHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		CombatClient.updateActionBarMessage UABMsg = (CombatClient.updateActionBarMessage) msg;
    	    OID oid = UABMsg.getSubject();
    	    int actionPosition = (Integer) UABMsg.getProperty("actionPosition");
    	    String newAction = (String) UABMsg.getProperty("newAction");
    	    CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
    	    ArrayList<String> actions = cInfo.getCurrentActions();
    	    while (actions.size() <= actionPosition) {
    	    	actions.add("");
    	    }
    	    actions.set(actionPosition, newAction);
    	    cInfo.setCurrentActions(actions);
    	    ExtendedCombatMessages.sendActions(oid, actions);
    	    return true;
    	}
    }
    
    public static int GetStartingXpReq() {
    	if (levelXpRequirements.containsKey(1))
    		return levelXpRequirements.get(1);
    	else
    		return 0;
    }
    
    public static CharacterTemplate getCharacterTemplate(String key) {
    	return characterTemplates.get(key);
    }
    
    static HashMap<Integer, Integer> levelXpRequirements = new HashMap<Integer, Integer>();
    static Map<String, CharacterTemplate> characterTemplates = new HashMap<String, CharacterTemplate>();
    
    public static final int SKILL_GAIN_RATE = 1;
    //public static final int SKILL_MAX = 15;
    public static final int POINTS_PER_SKILL_LEVEL = 10;
    public static final int MAX_SKILL_ABILITIES = 10;
    public static int TOTAL_SKILL_MAX = 1000000;
    
    public static boolean USE_SKILL_PURCHASE_POINTS = true;
    public static int SKILL_POINTS_GIVEN_PER_LEVEL = 3;
    public static boolean USE_SKILL_MAX = true;
    public static int SKILL_STARTING_MAX = 5;
    public static int EXP_MAX_LEVEL_DIFFERENCE = 10;
    
    //Define combat related string constants
    public static final String KILL_EXP_STAT = "kill_exp";
    public static final String EXPERIENCE_STAT = "experience";
    public static final String EXPERIENCE_MAX_STAT = "experience-max";
    public static final String LEVEL_STAT =  "level";
}
