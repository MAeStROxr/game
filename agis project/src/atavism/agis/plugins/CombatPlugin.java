package atavism.agis.plugins;

import atavism.agis.core.Agis;
import atavism.agis.core.AgisAbility;
import atavism.agis.core.AgisEffect;
import atavism.agis.core.AgisEffect.EffectState;
import atavism.agis.database.CombatDatabase;
import atavism.agis.database.ContentDatabase;
import atavism.agis.effects.MountEffect;
import atavism.agis.objects.*;
import atavism.agis.plugins.GroupClient.GroupInfo;
import atavism.agis.util.EquipHelper;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.ResponseMessage;
import atavism.server.engine.*;
import atavism.server.math.Point;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.Entity;
import atavism.server.objects.EntityManager;
import atavism.server.objects.Marker;
import atavism.server.objects.Template;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.ObjectInfo;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.AORuntimeException;
import atavism.server.util.LockFactory;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

//
// the combat plugin tracks autoattacks and resolves combat messages
//
public class CombatPlugin extends atavism.server.engine.EnginePlugin {

	public CombatPlugin() {
		super(COMBAT_PLUGIN_NAME);
		setPluginType("Combat");
	}

	public static String COMBAT_PLUGIN_NAME = "Combat";

	protected static final Logger log = new Logger("CombatPlugin");
	protected static Lock lock = LockFactory.makeLock("CombatPlugin");

	public void onActivate() {
		try {
			log.debug("CombatPlugin.onActivate()");
			// register for msgtype->hooks
			registerHooks();

			// subscribe to auto attack messages
			MessageTypeFilter filter = new MessageTypeFilter();
			filter.addType(CombatClient.MSG_TYPE_SET_COMBAT_INFO_STATE);
			filter.addType(CombatClient.MSG_TYPE_AUTO_ATTACK);
			filter.addType(CombatClient.MSG_TYPE_START_ABILITY);
			filter.addType(CombatClient.MSG_TYPE_RELEASE_OBJECT);
			filter.addType(WorldManagerClient.MSG_TYPE_UPDATE_OBJECT);
			filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
			filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
			filter.addType(WorldManagerClient.MSG_TYPE_SPAWNED);
			filter.addType(CombatClient.MSG_TYPE_ADD_SKILL);
			filter.addType(AgisInventoryClient.MSG_TYPE_ITEM_ACQUIRE_STATUS_CHANGE);
			filter.addType(AgisInventoryClient.MSG_TYPE_ITEM_EQUIP_STATUS_CHANGE);
			filter.addType(CombatClient.MSG_TYPE_COMBAT_STOP_AUTO_ATTACK);
			filter.addType(CombatClient.MSG_TYPE_TARGET_TYPE);
			filter.addType(ArenaClient.MSG_TYPE_REMOVE_EFFECTS);
			filter.addType(CombatClient.MSG_TYPE_APPLY_EFFECT);
			filter.addType(CombatClient.MSG_TYPE_REMOVE_EFFECT);
			filter.addType(CombatClient.MSG_TYPE_REMOVE_BUFF);
			filter.addType(AgisMobClient.MSG_TYPE_UPDATE_PET_STATS);
			filter.addType(CombatClient.MSG_TYPE_UPDATE_BREATH);
			filter.addType(CombatClient.MSG_TYPE_UPDATE_FATIGUE);
			filter.addType(CombatClient.MSG_TYPE_UPDATE_HEALTH_PROPS);
			filter.addType(CombatClient.MSG_TYPE_REGEN_HEALTH_MANA);
			filter.addType(CombatClient.MSG_TYPE_DISMOUNT);
			/* Long sub = */Engine.getAgent().createSubscription(filter, this);

			// Create responder subscription
			MessageTypeFilter filter2 = new MessageTypeFilter();
			filter2.addType(LoginMessage.MSG_TYPE_LOGIN);
			// filter2.addType(LogoutMessage.MSG_TYPE_LOGOUT);
			filter2.addType(CombatClient.MSG_TYPE_GET_PLAYER_STAT_VALUE);
			Engine.getAgent().createSubscription(filter2, this,
					MessageAgent.RESPONDER);

			registerLoadHook(Namespace.COMBAT, new CombatLoadHook());
			registerUnloadHook(Namespace.COMBAT, new CombatUnloadHook());
			registerSaveHook(Namespace.COMBAT, new CombatSaveHook());

			registerPluginNamespace(Namespace.COMBAT,
					new CombatPluginGenerateSubObjectHook());
		} catch (Exception e) {
			throw new AORuntimeException("onActivate failed", e);
		}

		// Load items from the database
		loadCombatDataFromDatabase();

		// Load settings
		ContentDatabase cDB = new ContentDatabase(false);
		String rangeCheckVertical = cDB.loadGameSetting("RANGE_CHECK_VERTICAL");
		if (rangeCheckVertical != null)
			RANGE_CHECK_VERTICAL = Boolean.parseBoolean(rangeCheckVertical);
		String maxSkillLevel = cDB.loadGameSetting("TOTAL_SKILL_MAX");
		if (maxSkillLevel != null)
			ClassAbilityPlugin.TOTAL_SKILL_MAX = Integer.parseInt(maxSkillLevel);
		String spiritEffect = cDB.loadGameSetting("SPIRIT_EFFECT");
		if (spiritEffect != null)
			SPIRIT_EFFECT = Integer.parseInt(spiritEffect);
		String releaseOnLogin = cDB.loadGameSetting("RELEASE_ON_LOGIN");
		if (releaseOnLogin != null)
			RELEASE_ON_LOGIN = Boolean.parseBoolean(releaseOnLogin);
		String magicalAttacksUseWeaponDamage = cDB.loadGameSetting("MAGICAL_ATTACKS_USE_WEAPON_DAMAGE");
		if (magicalAttacksUseWeaponDamage != null)
			MAGICAL_ATTACKS_USE_WEAPON_DAMAGE = Boolean.parseBoolean(magicalAttacksUseWeaponDamage);
		String expBasedOnDamageDealt = cDB.loadGameSetting("EXP_BASED_ON_DAMAGE_DEALT");
		if (expBasedOnDamageDealt != null)
			EXP_BASED_ON_DAMAGE_DEALT = Boolean.parseBoolean(expBasedOnDamageDealt);
		String weaponReqUsesSharedTypes = cDB.loadGameSetting("WEAPON_REQ_USES_SHARED_TYPES");
		if (expBasedOnDamageDealt != null)
			WEAPON_REQ_USES_SHARED_TYPES = Boolean.parseBoolean(weaponReqUsesSharedTypes);

		// Start combat stat tick
		// Engine.getExecutor().scheduleAtFixedRate(statTick, 10, 1,
		// TimeUnit.SECONDS);
	}

	public void loadCombatDataFromDatabase() {
		CombatDatabase cDB = new CombatDatabase(false);
		if (STAT_LIST == null) {
			loadStatData(cDB);
		}
		// Load effects
		ArrayList<AgisEffect> effects = cDB.loadCombatEffects();
		for (AgisEffect effect : effects)
			Agis.EffectManager.register(effect.getID(), effect);
		// Abilities
		ArrayList<AgisAbility> abilities = cDB.loadAbilities();
		for (AgisAbility ability : abilities) {
			Agis.AbilityManager.register(ability.getID(), ability);
			Log.debug("ABILITY: added " + ability.getName()
					+ " to the database.");
		}
		// Skills
		HashMap<Integer, SkillTemplate> skillTemplates = cDB.loadSkills();
		for (SkillTemplate tmpl : skillTemplates.values())
			Agis.SkillManager.register(tmpl.getSkillID(), tmpl);

		cDB.close();

		ContentDatabase ctDB = new ContentDatabase(false);
		graveyards = ctDB.loadGraveyards();
	}

	private void loadStatData(CombatDatabase cDB) {
		// Load stats
		STAT_LIST = cDB.LoadStats();
		// Load Damage Types
		DAMAGE_TYPES = cDB.LoadDamageTypes();
	}

	// how to process incoming messages
	protected void registerHooks() {
		getHookManager().addHook(CombatClient.MSG_TYPE_GET_PLAYER_STAT_VALUE,
				new GetPlayerStatValueHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_SET_COMBAT_INFO_STATE,
				new UpdateCombatInfoStateHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_AUTO_ATTACK,
				new AutoAttackHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_START_ABILITY,
				new StartAbilityHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_RELEASE_OBJECT,
				new ReleaseObjectHook());
		getHookManager().addHook(WorldManagerClient.MSG_TYPE_UPDATE_OBJECT,
				new UpdateObjectHook());
		getHookManager().addHook(PropertyMessage.MSG_TYPE_PROPERTY,
				new PropertyHook());
		getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED,
				new DespawnedHook());
		getHookManager().addHook(WorldManagerClient.MSG_TYPE_SPAWNED,
				new SpawnedHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_TARGET_TYPE,
				new TargetTypeUpdateHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_ADD_SKILL,
				new AddSkillHook());
		getHookManager().addHook(
				AgisInventoryClient.MSG_TYPE_ITEM_ACQUIRE_STATUS_CHANGE,
				new ItemAcquireStatusChangeHook());
		getHookManager().addHook(
				AgisInventoryClient.MSG_TYPE_ITEM_EQUIP_STATUS_CHANGE,
				new ItemEquipStatusChangeHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_COMBAT_STOP_AUTO_ATTACK,
				new StopAutoAttackHook());
		getHookManager().addHook(ArenaClient.MSG_TYPE_REMOVE_EFFECTS,
				new RemoveArenaEffectsHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_APPLY_EFFECT,
				new ApplyEffectHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_REMOVE_EFFECT,
				new RemoveEffectHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_REMOVE_BUFF,
				new RemoveBuffHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_UPDATE_HEALTH_PROPS,
				new SetHealthPropertiesHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_REGEN_HEALTH_MANA,
				new RegenerateHealthHook());
		getHookManager().addHook(CombatClient.MSG_TYPE_DISMOUNT,
				new DismountHook());

		// Hooks to process login/logout messages
		getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN, new LoginHook());
		getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT,
				new LogoutHook());
	}

	public static void resolveAutoAttack(CombatInfo info) {
		if (Log.loggingDebug)
			log.debug("CombatPlugin.resolveAutoAttack: info=" + info);
		OID targetOid = info.getAutoAttackTarget();
		CombatInfo target = getCombatInfo(targetOid);
		if (target == null) {
			return;
		}

		int abilityID = (Integer) info
				.getProperty(CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY);
		if (abilityID < 1) {
			return;
		}

		int autoattack = abilityID;
		if (!info.isUser()) {
			// Intercept here and check to see if the mob should use a different
			// ability - this is only temp
			// int abilityID = (Integer) info.getProperty("abilities");
			// int healAbilityID = (Integer) info.getProperty("HealAbilities");
			/*
			 * int maxHealth = info.statGetCurrentValue("health-max"); int
			 * curHealth = info.statGetCurrentValue("health"); float
			 * healthPercent = (float)curHealth / (float)maxHealth;
			 */

			boolean useHeal = false;
			boolean useAbility = false;
			/*
			 * if (healAbilityID != null && healthPercent < 0.3) { // Get the
			 * ability, then check the cooldown AgisAbility ability =
			 * Agis.AbilityManager.get(healAbilities); Map<String, Cooldown>
			 * cooldowns = ability.getCooldownMap(); Set<Map.Entry<String,
			 * Cooldown>> cooldownSet = cooldowns.entrySet(); boolean
			 * abilityReady = true; for (Map.Entry<String, Cooldown> entry :
			 * cooldownSet) { String cooldownName = (String) entry.getKey();
			 * Cooldown.State cdState = info.getCooldownState(cooldownName); if
			 * (cdState != null) {
			 * Log.debug("ANDREW - HealAbility is currently on cooldown");
			 * abilityReady = false; } } if (abilityReady == true) useHeal =
			 * true; } if (abilities != null && useHeal == false) { // Get the
			 * ability, then check the cooldown AgisAbility ability =
			 * Agis.AbilityManager.get(abilities); Map<String, Cooldown>
			 * cooldowns = ability.getCooldownMap(); Set<Map.Entry<String,
			 * Cooldown>> cooldownSet = cooldowns.entrySet(); boolean
			 * abilityReady = true; for (Map.Entry<String, Cooldown> entry :
			 * cooldownSet) { String cooldownName = (String) entry.getKey();
			 * Cooldown.State cdState = info.getCooldownState(cooldownName); if
			 * (cdState != null) {
			 * Log.debug("ANDREW - Ability is currently on cooldown");
			 * abilityReady = false; } } if (abilityReady == true) { Random
			 * random = new Random(); int rand = random.nextInt(100); if (rand >
			 * 20) useAbility = true; } }
			 * 
			 * if (useHeal == true) abilityName = healAbilities; else if
			 * (useAbility == true) abilityName = abilities;
			 */

		}

		AgisAbility ability = Agis.AbilityManager.get(abilityID);
		if (Log.loggingDebug)
			log.debug("CombatPlugin.resolveAutoAttack: abilityID " + abilityID
					+ ", ability " + ability);
		if (abilityID != autoattack) {
			String casterName = WorldManagerClient.getObjectInfo(info
					.getOwnerOid()).name;
			WorldManagerClient.sendObjChatMsg(target.getOwnerOid(), 0,
					casterName + " begins casting " + ability.getName());
		}

		// Try make the attacker (only for mobs) face their target
		/*
		 * if (!info.isUser()) { InterpolatedWorldNode wnode =
		 * info.getWorldNode(); //Quaternion direction = null;
		 * //wnode.setOrientation(direction); }
		 */

		/*int duelID = -1;
		int duelID2 = -1;
		if (info.isUser() && target.isUser()) {
			try {
				duelID = (Integer) target.getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
				duelID2 = (Integer) info.getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
			} catch (NullPointerException e) {
			}
		}
		if (duelID == duelID2)
			ability.setDuelID(duelID);
		else
			ability.setDuelID(-1);*/

		Log.debug("AUTO: calling auto attack");
		AgisAbility.startAbility(ability, info, target, null);
		Log.debug("AUTO: finished auto attack");
		if (MOBS_STOP_TO_ATTACK && !info.isUser()) {
			CombatClient.sendAutoAttackCompleted(info.getOwnerOid());
		}
	}

	public static void sendAbilityUpdate(CombatInfo info) {
		if (Log.loggingDebug)
			log.debug("CombatPlugin: sending AbilityUpdate for obj=" + info);
		CombatClient.AbilityUpdateMessage msg = new CombatClient.AbilityUpdateMessage(
				info.getOwnerOid(), info.getOwnerOid());
		for (int abilityID : info.getCurrentAbilities()) {
			AgisAbility ability = Agis.AbilityManager.get(abilityID);
			if (Log.loggingDebug)
				log.debug("CombatPlug: adding ability to message. ability="
						+ ability);
			msg.addAbility(ability.getID(), ability.getIcon(), "");
		}
		Engine.getAgent().sendBroadcast(msg);
	}

	public static CombatInfo getCombatInfo(OID oid) {
		return (CombatInfo) EntityManager.getEntityByNamespace(oid,
				Namespace.COMBAT);
	}

	public static void registerCombatInfo(CombatInfo cinfo) {
		EntityManager.registerEntityByNamespace(cinfo, Namespace.COMBAT);
	}

	class CombatLoadHook implements LoadHook {
		public void onLoad(Entity e) {
			CombatInfo info = (CombatInfo) e;
			// Clear the target lists
			info.setAttackableTargets(new HashMap<OID, TargetInfo>());
			info.setFriendlyTargets(new HashMap<OID, TargetInfo>());
			AgisEffect.removeNonContinuousEffects(info, true);
			if (info.dead()) {
				sendDeathBox(info.getOwnerOid());
			}
			// Calculate their base stats to ensure they are correct
			int race = (Integer) EnginePlugin.getObjectProperty(
					info.getOwnerOid(), WorldManagerClient.NAMESPACE, "race");
			int aspect = info.aspect();
			CharacterTemplate tmpl = ClassAbilityPlugin
					.getCharacterTemplate(race + " " + aspect);
			ClassAbilityPlugin.calculatePlayerStats(info, tmpl);
			for (SkillData sData : info.getCurrentSkillInfo().getSkills()
					.values()) {
				SkillInfo.applyStatModifications(info,
						Agis.SkillManager.get(sData.getSkillID()),
						sData.getSkillLevel());
			}

			// Apply stat shift values again
			for (String statName : info.getVitalityStats().keySet()) {
				AgisStat stat = (AgisStat) info.getProperty(statName);
				VitalityStatDef statDef = (VitalityStatDef) CombatPlugin
						.lookupStatDef(statName);
				stat.setBaseShiftValue(statDef.getShiftValue(),
						statDef.getReverseShiftValue());
				
				// Set the canExceedMax property
				if (lookupStatDef(stat.getName()).getCanExceedMax()) {
					stat.setCanExceedMax(true);
					Log.debug("MAX: canExceedMax for stat: " + stat.getName());
				}
			}

			// Send down the messages about the players current abilities/skills
			// etc.
			Log.debug("COMBAT: Sending combat messages");
			ExtendedCombatMessages.sendAbilities(info.getOwnerOid(),
					info.getCurrentAbilities());
			ExtendedCombatMessages.sendActions(info.getOwnerOid(),
					info.getCurrentActions());
			ExtendedCombatMessages.sendSkills(info.getOwnerOid(),
					info.getCurrentSkillInfo());

			// Add to stat tick
			CombatStatTick statTick = new CombatStatTick(info);
			// statTick.AddCombatInfo(info);
			Engine.getExecutor().schedule(statTick, 10, TimeUnit.SECONDS);
			statTicks.put(info.getOwnerOid(), statTick);
		}
	}

	class CombatUnloadHook implements UnloadHook {
		public void onUnload(Entity e) {
			CombatInfo info = (CombatInfo) e;
			Log.debug("COMBAT: Unloading CombatInfo for Entity: " + e.getOid());
			// Remove to stat tick
			// statTick.RemoveCombatInfo(info);
			statTicks.get(info.getOwnerOid()).disable();
			AgisEffect.removeNonContinuousEffects(info, false);
		}
	}

	class CombatSaveHook implements SaveHook {
		public void onSave(Entity e, Namespace namespace) {
		}
	}

	/**
	 * Creates the CombatInfo subObject for the mob/player characters and
	 * initialises the properties including the combat stats.
	 * 
	 * @author Andrew Harrison
	 * 
	 */
	class CombatPluginGenerateSubObjectHook extends GenerateSubObjectHook {
		public CombatPluginGenerateSubObjectHook() {
			super(CombatPlugin.this);
		}

		public SubObjData generateSubObject(Template template,
				Namespace namespace, OID masterOid) {
			if (Log.loggingDebug)
				log.debug("GenerateSubObjectHook: masterOid=" + masterOid
						+ ", template=" + template);

			Map<String, Serializable> props = template
					.getSubMap(Namespace.COMBAT);
			if (props == null) {
				Log.warn("GenerateSubObjectHook: no props in ns "
						+ Namespace.COMBAT);
				return null;
			}

			// generate the subobject
			CombatInfo cinfo = new CombatInfo(masterOid,
					template.getTemplateID());
			cinfo.setName(template.getName());
			cinfo.setCurrentCategory(1);

			Boolean persistent = (Boolean) template.get(
					Namespace.OBJECT_MANAGER,
					ObjectManagerClient.TEMPL_PERSISTENT);
			if (persistent == null)
				persistent = false;
			cinfo.setPersistenceFlag(persistent);

			int minlevel = 1;
			int maxlevel = 1;
			// copy properties from template to object
			for (Map.Entry<String, Serializable> entry : props.entrySet()) {
				String key = entry.getKey();
				Serializable value = entry.getValue();
				if (!key.startsWith(":")) {
					cinfo.setProperty(key, value);
					Log.debug("COMBAT: added property: " + key);
					if (key.equals("minLevel")) {
						// Grab the level value for the starting stats
						// calculation
						int stat = (Integer) value;
						minlevel = stat;
					} else if (key.equals("maxLevel")) {
						// Grab the level value for the starting stats
						// calculation
						int stat = (Integer) value;
						maxlevel = stat;
					}
				}
			}

			Random rand = new Random();
			int level = minlevel + rand.nextInt(maxlevel - minlevel + 1);

			// Get any stat overrides - these are for mob templates
			HashMap<String, Integer> statOverrides = null;
			if (props.containsKey(":statOverrides"))
				statOverrides = (HashMap) props.get(":statOverrides");

			// Get the starting stats and values
			HashMap<String, AgisStat> statMap = getStartingStats(level,
					cinfo.isMob(), statOverrides);

			// Insert properties for public stats
			for (AgisStat stat : statMap.values()) {
				cinfo.setProperty(stat.getName(), stat);
			}

			// Make sure no stats are missing
			for (Map.Entry<String, AgisStatDef> statEntry : statDefMap
					.entrySet()) {
				String statName = statEntry.getKey();
				AgisStat stat = (AgisStat) cinfo.getProperty(statName);
				if (stat == null) {
					Log.debug("STAT: stat is null - " + statName);
					stat = new AgisStat(statName);
					cinfo.setProperty(statName, stat);
				}
			}

			// Run the update for base stats before running through vitality
			// stats so the
			// starting values will be correct
			for (AgisStatDef statDef : baseStats) {
				String statName = statDef.getName();
				AgisStat stat = (AgisStat) cinfo.getProperty(statName);
				Log.warn("STAT: updating stat: " + stat);
				statDef.update(stat, cinfo);
			}

			// Look through all stats and grab all vitality stats to apply a few
			// extra settings
			for (Map.Entry<String, AgisStatDef> statEntry : statDefMap
					.entrySet()) {
				String statName = statEntry.getKey();
				AgisStat stat = (AgisStat) cinfo.getProperty(statName);

				// If the stat is a VitalityStatDef (or child) then add the stat
				// to the statDef
				// and set the base value
				if (lookupStatDef(stat.getName()) instanceof VitalityStatDef) {
					VitalityStatDef statDef = (VitalityStatDef) lookupStatDef(stat
							.getName());
					// Set it to the start percent
					int startingValue = 1;
					startingValue = statDef.getStartingValue(cinfo);
					cinfo.statSetBaseValue(statName, startingValue);
					// stat.setBase(startingValue);

					// Add the vitality stat listing to the Combat Info
					Log.debug("STAT: adding vitality stat with shift inteval: "
							+ statDef.getShiftInterval());
					if ((statDef.checkShiftTarget(cinfo))
							&& statDef.getShiftInterval() > 0) {
						cinfo.addVitalityStat(stat, statDef.getShiftInterval());
					}
				}
			}

			if (Log.loggingDebug)
				log.debug("GenerateSubObjectHook: created entity " + cinfo);

			// register the entity
			registerCombatInfo(cinfo);

			// If the object is a players character create the skillInfo
			if (cinfo.isUser()) {
				ArrayList<Integer> skills = (ArrayList) props
						.get(":startingSkills");
				createNewSkillInfo(cinfo, masterOid, skills);
			}

			if (persistent)
				Engine.getPersistenceManager().persistEntity(cinfo);

			// Add to stat tick
			CombatStatTick statTick = new CombatStatTick(cinfo);
			// statTick.AddCombatInfo(cinfo);
			Engine.getExecutor().schedule(statTick, 10, TimeUnit.SECONDS);
			statTicks.put(masterOid, statTick);

			// send a response message
			return new SubObjData();
		}
	}

	/**
	 * Creates the skillInfo object for the CombatInfo object based on the skill
	 * templates loaded in from the database.
	 * 
	 * @param info
	 * @param mobOid
	 * @param race
	 */
	protected void createNewSkillInfo(CombatInfo info, OID mobOid,
			ArrayList<Integer> skills) {
		// First get the skills the character will get
		SkillInfo skillInfo = new SkillInfo(info.getCurrentCategory());
		ArrayList<Integer> abilities = new ArrayList<Integer>();
		ArrayList<String> actions = new ArrayList<String>();
		// First give the players auto attack
		abilities.add(PLAYER_ATTACK_ABILITY);
		for (int skill : skills) {
			Log.debug("SKILL: adding skill: " + skill);
			SkillTemplate tmpl = Agis.SkillManager.get(skill);
			Log.debug("SKILL: 1 adding skill: " + tmpl.getSkillName());
			skillInfo.addSkill(tmpl);
			Log.debug("SKILL: 2 adding skill: " + skill);
			ArrayList<Integer> abilityIDs = tmpl.getStartAbilityIDs();
			Log.debug("SKILL: got " + abilityIDs.size() + " abilities");
			for (int ability : abilityIDs) {
				abilities.add(ability);
				actions.add("a" + ability);
			}
		}
		// Store the information in the combat info
		info.setCurrentAbilities(abilities);
		info.setCurrentActions(actions);
		info.setCurrentSkillInfo(skillInfo);
		// Send down the messages about the players current abilities/skills
		// etc.
		Log.debug("AJ: Sending combat messages");
		ExtendedCombatMessages.sendAbilities(info.getOwnerOid(),
				info.getCurrentAbilities());
		ExtendedCombatMessages.sendActions(info.getOwnerOid(),
				info.getCurrentActions());
		ExtendedCombatMessages.sendSkills(info.getOwnerOid(),
				info.getCurrentSkillInfo());
	}

	/**
	 * Hook for the GetPlayerStatValueMessage. Gets the value of the specified
	 * stat for the player in question and sends it back to the remote call
	 * procedure that sent the message out.
	 * 
	 * @author Andrew Harrison
	 * 
	 */
	class GetPlayerStatValueHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.GetPlayerStatValueMessage EBMsg = (CombatClient.GetPlayerStatValueMessage) msg;
			Log.debug("SKILL: got GetPlayerSkillLevelMessage");
			OID oid = EBMsg.getSubject();
			String statName = EBMsg.getStatName();
			CombatInfo cInfo = CombatPlugin.getCombatInfo(oid);
			int statValue = -1;
			if (lookupStatDef(statName) != null) {
				statValue = cInfo.statGetCurrentValue(statName);
			}

			Engine.getAgent().sendIntegerResponse(msg, statValue);
			return true;
		}
	}

	class AutoAttackHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.AutoAttackMessage autoAtkMsg = (CombatClient.AutoAttackMessage) msg;
			OID oid = autoAtkMsg.getSubject();
			CombatInfo obj = getCombatInfo(oid);
			if (obj == null)
				return true;
			OID targetOid = autoAtkMsg.getTargetOid();
			CombatInfo target = getCombatInfo(targetOid);
			Boolean status = autoAtkMsg.getAttackStatus();
			Lock objLock = obj.getLock();
			Lock targetLock = null;
			if (target != null)
				targetLock = target.getLock();

			try {
				objLock.lock();
				while ((targetLock != null) && !targetLock.tryLock()) {
					objLock.unlock();
					Thread.yield();
					objLock.lock();
				}

				if (Log.loggingDebug)
					log.debug("AutoAttackHook.processMessage: oid=" + oid
							+ ", targetOid=" + targetOid + ", status=" + status);

				if (!status || obj.dead() || (target == null) || target.dead()) {
					obj.stopAutoAttack();
					if (target != null) {
						OID tagOwner = (OID) target
								.getProperty(CombatInfo.COMBAT_TAG_OWNER);
						if (tagOwner != null && tagOwner.equals(oid)) {
							// You owned the tag, but now you have stopped so
							// lets clear the ownership
							OID newOwner = null;
							if (getAttackers(targetOid) != null) {
								for (OID attacker : getAttackers(targetOid)) {
									if (!attacker.equals(oid))
										newOwner = attacker;
								}
								target.setProperty(CombatInfo.COMBAT_TAG_OWNER,
									newOwner);
							}
						}
					}
					Long petOwner = (Long) obj.getProperty("petOwner");
					if (petOwner != null) {
						AgisMobClient.petTargetLost(obj.getOid());
					}

				} else {
					obj.setAutoAttack(targetOid);
				}
				return true;
			} finally {
				if (targetLock != null)
					targetLock.unlock();
				objLock.unlock();
			}
		}
	}

	class StopAutoAttackHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.stopAutoAttackMessage EBMsg = (CombatClient.stopAutoAttackMessage) msg;
			OID oid = EBMsg.getSubject();
			Log.debug("COMBATPLUGIN: stop autoAttack caught: " + EBMsg);
			CombatInfo info = getCombatInfo(oid);
			info.stopAutoAttack();
			return true;
		}
	}

	class StartAbilityHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.StartAbilityMessage abilityMsg = (CombatClient.StartAbilityMessage) msg;
			OID oid = abilityMsg.getSubject();
			OID targetOid = abilityMsg.getTargetOid();
			int abilityID = abilityMsg.getAbilityID();
			Point loc = abilityMsg.getLocation();
			AgisItem item = (AgisItem) abilityMsg.getItem();

			log.debug("StartAbilityHook.processMessage: oid=" + oid
					+ ", targetOid=" + targetOid + " ability=" + abilityID
					+ ", loc=" + loc);

			CombatInfo obj = getCombatInfo(oid);
			CombatInfo target = obj;
			if (targetOid.toLong() != -1)
            	target = getCombatInfo(targetOid);
			AgisAbility ability = Agis.AbilityManager.get(abilityID);

			/*int duelID = -1;
			int duelID2 = -1;
			if (obj.isUser() && target.isUser()) {
				try {
					duelID = (Integer) target.getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
					duelID2 = (Integer) obj.getProperty(CombatInfo.COMBAT_PROP_DUEL_ID);
				} catch (NullPointerException e) {
				}
			}

			if (duelID == duelID2) {
				ability.setDuelID(duelID);
			} else {
				ability.setDuelID(-1);
			}*/

			// AgisAbility.startAbility(ability, obj, target, null);
			AgisAbility.startAbility(ability, obj, target, item, loc);
			return true;
		}
	}

	class ReleaseObjectHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.ReleaseObjectMessage releaseMsg = (CombatClient.ReleaseObjectMessage) msg;
			OID oid = releaseMsg.getSubject();

			if (Log.loggingDebug)
				log.debug("ReleaseObjectHook.processMessage: oid=" + oid);

			CombatInfo info = getCombatInfo(oid);
			if (info == null)
				return true;
			if (!info.dead())
				return true;
			info.setCombatState(false);
			if (releaseMsg.turnToSpirit() && info.getState() != null
					&& info.getState().equals(CombatInfo.COMBAT_STATE_SPIRIT)) {
				// If player is already a spirit and they are trying to turn
				// into a spirit - return
				return true;
			}
			Log.debug("RELEASE: 5");

			EnginePlugin.setObjectPropertiesNoResponse(info.getOwnerOid(),
					Namespace.WORLD_MANAGER,
					WorldManagerClient.WORLD_PROP_NOMOVE, new Boolean(false),
					WorldManagerClient.WORLD_PROP_NOTURN, new Boolean(false));
			Log.debug("RELEASE: 7");

			AgisInventoryClient.generatePlayerLoot(oid,
					WorldManagerClient.getObjectInfo(oid).loc);
			
			if (!releaseMsg.turnToSpirit()) {
				// Handle respawn positioning
				info.clearState(CombatInfo.COMBAT_STATE_SPIRIT);
				relocateReleasedPlayer(oid);
				// Do this after relocating so mobs don't aggro from where the player died
				info.setDeadState(false);
				setReleaseStatValues(info);
			} else {
				info.setState(CombatInfo.COMBAT_STATE_SPIRIT);
				if (SPIRIT_EFFECT > 0)
					AgisEffect.applyEffect(Agis.EffectManager.get(SPIRIT_EFFECT), info, info, -1);
			}

			return true;
		}
	}

	public static void setReleaseStatValues(CombatInfo info) {
		for (Map.Entry<String, AgisStatDef> statEntry : statDefMap.entrySet()) {
			String statName = statEntry.getKey();
			// AgisStatDef statDef = statEntry.getValue();

			AgisStat stat = (AgisStat) info.getProperty(statName);
			// If the stat is a VitalityStatDef (or child) then add the stat to
			// the statDef
			if (lookupStatDef(stat.getName()) instanceof VitalityStatDef) {
				VitalityStatDef statDef = (VitalityStatDef) lookupStatDef(stat
						.getName());
				if (statDef.getReleaseResetPercent() == -1)
					continue;
				// Reset to set percent
				int maxval = info.statGetMaxValue(statName);
				int currentval = info.statGetCurrentValue(statName);
				currentval = maxval * statDef.getReleaseResetPercent() / 100;
				Log.debug("RELEASE: setting vitality stat: " + statName
						+ " to current: " + currentval);
				info.statSetBaseValue(statName, currentval);
			}
		}
	}

	public static void setDeathStatValues(CombatInfo info) {
		for (Map.Entry<String, AgisStatDef> statEntry : statDefMap.entrySet()) {
			String statName = statEntry.getKey();
			// AgisStatDef statDef = statEntry.getValue();

			AgisStat stat = (AgisStat) info.getProperty(statName);
			// If the stat is a VitalityStatDef (or child) then add the stat to
			// the statDef
			if (lookupStatDef(stat.getName()) instanceof VitalityStatDef) {
				VitalityStatDef statDef = (VitalityStatDef) lookupStatDef(stat
						.getName());
				if (statDef.getDeathResetPercent() == -1)
					continue;
				// Reset to set percent
				int maxval = info.statGetMaxValue(statName);
				int currentval = info.statGetCurrentValue(statName);
				currentval = maxval * statDef.getDeathResetPercent() / 100;
				Log.debug("DEATH: setting vitality stat: " + statName
						+ " to current: " + currentval);
				info.statSetBaseValue(statName, currentval);
			}
		}
	}

	/**
	 * Calculates where the player should be moved to upon releasing. Takes into
	 * account variables such as their race, current zone/subzone, and whether
	 * or not they are in an arena.
	 * 
	 * @param oid
	 *            : the oid of the player who has released.
	 */
	private void relocateReleasedPlayer(OID oid) {
		Log.debug("RELEASE: moving player: " + oid);
		/*
		 * int arenaID = (Integer) EnginePlugin.getObjectProperty(oid,
		 * Namespace.WORLD_MANAGER, "arenaID"); if (arenaID != -1) {
		 * arenaRelease(oid); return; //if (relocated) // return; }
		 */
		BasicWorldNode wnode = WorldManagerClient.getWorldNode(oid);

		OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
		int instanceID = InstanceClient.getInstanceInfo(instanceOid,
				InstanceClient.FLAG_TEMPLATE_ID).templateID;
		float distanceToClosestGraveyard = Float.MAX_VALUE;
		Graveyard closestGraveyard = null;
		HashMap<Integer, PlayerFactionData> pfdMap = (HashMap) EnginePlugin
				.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "factionData");
		if (graveyards.containsKey(instanceID)) {
			for (Graveyard gy : graveyards.get(instanceID)) {
				// Check if graveyard is compatible for the players faction/reputation
				int playerFaction = (Integer) EnginePlugin.getObjectProperty(oid,
					WorldManagerClient.NAMESPACE, "faction");
				if (playerFaction != gy.getFactionReq()) {
					// If the players faction is not the same as the graveyards,
					// make sure they have high enough rep to use it
					if (!pfdMap.containsKey(gy.getFactionReq())) {
						// TODO: Player hasn't met the faction yet. Should do a
						// check for default stance - will do this later
						continue;
					}
					PlayerFactionData pfd = pfdMap.get(gy.getFactionReq());
					Log.debug("FACTION: got faction from players FactionDataMap");
					int reputation = pfd.getReputation();
					if (FactionPlugin.calculateStanding(reputation) < gy.getFactionRepReq()) {
						continue;
					}
				}

				float distance = Point.distanceTo(gy.getLoc(), wnode.getLoc());
				if (distance < distanceToClosestGraveyard) {
					closestGraveyard = gy;
					distanceToClosestGraveyard = distance;
				}
			}
		}

		if (closestGraveyard != null) {
			wnode.setLoc(closestGraveyard.getLoc());
		} else {
			Marker marker = InstanceClient.getMarker(instanceOid, "spawn");
			wnode.setLoc(marker.getPoint());
		}

		WorldManagerClient.updateWorldNode(oid, wnode, true);
	}
	
	/**
	 * Removes the Spirit Effect from the specified player if they have one.
	 * 
	 * @param oid
	 */
	private void removeSpiritEffect(OID oid) {
		// Remove the mount effect from the player
		CombatInfo player = getCombatInfo(oid);
		EffectState spiritEffect = null;
		for (EffectState state : player.getCurrentEffects()) {
			Log.debug("SPIRIT: found effect: " + state.getEffectID());
			if (state.getEffect() != null && state.getEffectID() == SPIRIT_EFFECT) {
				spiritEffect = state;
				Log.debug("SPIRIT: found effect to remove: " + spiritEffect.getEffectID());
				break;
			}
		}

		if (spiritEffect != null) {
			AgisEffect.removeEffect(spiritEffect, player);
		}
	}

	/**
	 * Deals with players who have released while in an arena. It determines
	 * their team and will teleport them to the appropriate graveyard.
	 * 
	 * @param oid
	 *            : the oid of the player who has released.
	 */
	private boolean arenaRelease(OID oid) {
		String factionOverride = (String) EnginePlugin.getObjectProperty(oid,
				WorldManagerClient.NAMESPACE, "factionOverride");
		if (factionOverride == null || factionOverride.equals("")) {
			Log.debug("RELEASE: factionOverride was emtpy for player: " + oid);
			return false;
		}
		// TODO: Fix the way I get the team number of the player
		int teamStringLoc = factionOverride.lastIndexOf("_");
		String teamNum = factionOverride.substring(teamStringLoc + 5);
		String markerName = "death_" + teamNum;
		Log.debug("RELEASE: Player is in arena and on team " + teamNum);
		OID instanceOid = WorldManagerClient.getObjectInfo(oid).instanceOid;
		BasicWorldNode wnode = new BasicWorldNode();
		Marker marker = InstanceClient.getMarker(instanceOid, markerName);
		if (marker == null) {
			Log.debug("RELEASE: arena marker not found: " + markerName);
			return false;
		}
		Log.debug("RELEASE: arena marker found: " + markerName);
		wnode.setLoc(marker.getPoint());
		WorldManagerClient.updateWorldNode(oid, wnode, true);
		return true;
	}

	/**
	 * This will handle the death of a player or mob. It may try to activate any
	 * after-death effects and it will also send out messages for other
	 * death-related events.
	 * 
	 * @param obj
	 *            : the combatInfo object for the player or mob that died
	 */
	public static void handleDeath(CombatInfo obj) {
		// TODO: Check for any death-related abilities that may prevent death
		// from occurring

		// If we have reached this point, the unit will be set as dead
		obj.setDeadState(true);
		OID oid = obj.getOwnerOid();
		// sendDeathBox(oid);
		AgisEffect.removeNonPassiveEffects(obj);
		Log.debug("DEATH: effects removed");
		// OID killerOid = lastAttackerMap.get(oid);
		// Log.debug("DEATH: sending out Arena client death related messages for object: "
		// + oid);
		// ArenaClient.arenaDeath(killerOid, oid);
		// Now set the unit as lootable, if applicable
		// Log.debug("DEATH: sent Arena client death related messages for object: "
		// + oid);
		if (obj.isMob()) {
			/*
			 * OID petOwner = (OID) obj.getProperty("petOwner"); if (petOwner !=
			 * null) { // It's a pet so we don't want to set it as lootable
			 * return; }
			 */
			/*
			 * OID tagOwnerOid = (OID)
			 * obj.getProperty(CombatInfo.COMBAT_TAG_OWNER);
			 * Log.debug("DEATH: setting mob " + oid +
			 * " as lootable for player: " + tagOwnerOid);
			 * AgisInventoryClient.generateLoot(oid); TargetedPropertyMessage
			 * propMsg = new TargetedPropertyMessage(tagOwnerOid, oid);
			 * propMsg.setProperty("lootable", true);
			 * Engine.getAgent().sendBroadcast(propMsg); ExtensionMessage
			 * xpUpdateMsg = new
			 * ExtensionMessage(ClassAbilityClient.MSG_TYPE_HANDLE_EXP,
			 * "ao.HANDLE_EXP", oid); xpUpdateMsg.setProperty("attackers",
			 * tagOwnerOid); Engine.getAgent().sendBroadcast(xpUpdateMsg);
			 */
		} else {
			ArenaClient.duelDefeat(oid);
			OID accountId = (OID) EnginePlugin.getObjectProperty(oid,
					WorldManagerClient.NAMESPACE, "accountId");
			DataLoggerClient.logData("PLAYER_DIED", oid, null, accountId, null);
		}
	}

	/**
	 * Sends a message to the client to display a death (release) box. Depending
	 * on certain circumstances the box may have different options, or not
	 * displayed at all.
	 * 
	 * @param oid
	 */
	private static void sendDeathBox(OID oid) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "deathBox");

		props.put("boxType", "normal");
		// Check if they are in an arena
		int arenaID = -1;
		try {
			arenaID = (Integer) EnginePlugin.getObjectProperty(oid,
					WorldManagerClient.NAMESPACE, "arenaID");
		} catch (NullPointerException e) {
		}
		if (arenaID != -1) {
			props.put("boxType", "arena");
		}

		TargetedExtensionMessage msg = new TargetedExtensionMessage(
				WorldManagerClient.MSG_TYPE_EXTENSION, oid, oid, false, props);
		Engine.getAgent().sendBroadcast(msg);
	}

	void SpawnLootObject() {

	}

	/**
	 * Hook for the Dismount Extension Message. Will attempt to remove a
	 * MountEffect from the player (resulting in them being dismounted).
	 * 
	 * @author Andrew Harrison
	 * 
	 */
	class DismountHook implements Hook {
		public boolean processMessage(atavism.msgsys.Message msg, int flags) {
			ExtensionMessage eMsg = (ExtensionMessage) msg;
			OID playerOid = eMsg.getSubject();
			Log.debug("MOUNT: got dismount message");
			dismountPlayer(playerOid);

			return true;
		}
	}

	/**
	 * Removes a MountEffect from the specified player if they have one. The
	 * result is the player being dismounted.
	 * 
	 * @param oid
	 */
	private void dismountPlayer(OID oid) {
		// Remove the mount effect from the player
		CombatInfo player = getCombatInfo(oid);
		EffectState mountEffect = null;
		for (EffectState state : player.getCurrentEffects()) {
			Log.debug("MOUNT: found effect: " + state.getEffectID());
			if (state.getEffect() != null
					&& state.getEffect() instanceof MountEffect) {
				mountEffect = state;
				Log.debug("MOUNT: found effect to remove: "
						+ mountEffect.getEffectID());
				break;
			}
		}

		if (mountEffect != null) {
			AgisEffect.removeEffect(mountEffect, player);
		}
	}

	class UpdateCombatInfoStateHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.SetCombatInfoStateMessage stateMsg = (CombatClient.SetCombatInfoStateMessage) msg;

			OID subjectOid = stateMsg.getSubject();
			// is the update object spawned?
			CombatInfo info = getCombatInfo(subjectOid);
			if (info == null) {
				return false;
			}

			String state = stateMsg.getState();
			if (!stateMsg.getClearState()) {
				Log.debug("STATE: setting combat info state to: " + state);
				info.setState(state);
			} else {
				Log.debug("STATE: clearing combat info state: " + state);
				info.clearState(state);
			}

			return true;
		}
	}

	class UpdateObjectHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			WorldManagerClient.UpdateMessage updateReq = (WorldManagerClient.UpdateMessage) msg;
			OID subjectOid = updateReq.getSubject();
			OID targetOid = updateReq.getTarget();

			// is the update object spawned?
			CombatInfo info = getCombatInfo(subjectOid);
			if (info == null) {
				return false;
			}

			// send over properties
			if (Log.loggingDebug)
				log.debug("UpdateObjectHook.processMessage: sending properties for subjectOid="
						+ subjectOid);

			WorldManagerClient.TargetedPropertyMessage propMessage = new WorldManagerClient.TargetedPropertyMessage(
					targetOid, subjectOid);
			for (Map.Entry<String, Serializable> kvp : info.getPropertyMap()
					.entrySet()) {
				if (!(kvp.getValue() instanceof AgisStat))
					propMessage.setProperty(kvp.getKey(), kvp.getValue(), true);
			}

			// Comment out next two lines for LES world
			Engine.getAgent().sendBroadcast(propMessage);
			info.statSendUpdate(true, targetOid);

			// Abilities are only sent to the player themselves
			// if (subjectOid.equals(targetOid))
			// ClassAbilityPlugin.sendSkillUpdate(info);
			// sendAbilityUpdate(info);

			return true;
		}
	}

	/**
	 * TODO: Move this code to CombatBehavior
	 * 
	 * @author Andrew
	 * 
	 */
	class PropertyHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			PropertyMessage propMsg = (PropertyMessage) msg;
			OID objOid = propMsg.getSubject();

			Boolean dead = (Boolean) propMsg.getProperty(CombatInfo.COMBAT_PROP_DEADSTATE);

			if (dead != null && dead) {
				CombatInfo obj = getCombatInfo(objOid);
				obj.stopAutoAttack();

				Set<OID> attackers = getAttackers(objOid);

				if (attackers != null) {
					clearAttackers(objOid);
					// Send message to ClassAbilityPlugin to handle xp gain
					if (!obj.isMob()) {
						return true;
					}
					String name = obj.getName();
					// int mobID = -1;
					Integer mobID = (Integer) EnginePlugin.getObjectProperty(
							obj.getOwnerOid(), WorldManagerClient.NAMESPACE,
							WorldManagerClient.TEMPL_ID);
					Log.debug("DEATH: mob " + mobID);
					LinkedList<String> questCategories = (LinkedList<String>) EnginePlugin
							.getObjectProperty(obj.getOwnerOid(), WorldManagerClient.NAMESPACE,
									"questCategories");
					if (questCategories == null)
						Log.debug("QuestCategories is null");
					else
						Log.debug("QuestCategories = " + questCategories);

	        		ArrayList<OID> groupsAlreadyChecked = new ArrayList<OID>();
					for (OID attacker : attackers) {
						CombatInfo info = getCombatInfo(attacker);
						if (info != null) {
							info.stopAutoAttack();
						}
						
						// Get group members that are close by and send message for them as well
						if (info.isGrouped()) {
							// Skip any group members where the group has already had a Mob Death message sent for it
							GroupInfo gInfo = GroupClient.GetGroupMemberOIDs(attacker);
							if (gInfo.groupOid != null) {
								if (groupsAlreadyChecked.contains(gInfo.groupOid)) {
		        					continue;
		        				} else {
		        					groupsAlreadyChecked.add(gInfo.groupOid);
		        				}
							}
							ObjectInfo objInfo = WorldManagerClient.getObjectInfo(attacker);
							for (OID groupMember : gInfo.memberOidSet) {
								Log.debug("GROUP: checking group member to send quest mob death: " + groupMember);
								if (!groupMember.equals(attacker)) {
									// Distance check
									ObjectInfo memberInfo = WorldManagerClient.getObjectInfo(groupMember);
									if (!memberInfo.instanceOid.equals(objInfo.instanceOid)) {
										Log.debug("GROUP: group member is not in the same instance");
										continue;
									}
									if (Point.distanceTo(memberInfo.loc, objInfo.loc) > 100) {
										Log.debug("GROUP: group member is not close enough: " + Point.distanceTo(memberInfo.loc, objInfo.loc));
										continue;
									}
								}
								
								CombatClient.QuestMobDeath QMDmsg = new CombatClient.QuestMobDeath(
										groupMember, mobID, name, questCategories);
								Engine.getAgent().sendBroadcast(QMDmsg);
							}
						} else {
							CombatClient.QuestMobDeath QMDmsg = new CombatClient.QuestMobDeath(
									attacker, mobID, name, questCategories);
							Engine.getAgent().sendBroadcast(QMDmsg);
						}
						
					}
				}
			} else if (dead != null && !dead) {
				removeSpiritEffect(objOid);
			}

			return true;
		}
	}

	/**
	 * Sets combat state to false upon despawning
	 */
	class DespawnedHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			WorldManagerClient.DespawnedMessage despawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
			OID objOid = despawnedMsg.getSubject();
			CombatInfo obj = getCombatInfo(objOid);
			if (obj == null)
				return false;
			if (Log.loggingDebug)
				log.debug("DespawnedHook: got a despawned message for oid="
						+ objOid);
			if (obj != null)
				obj.setCombatState(false);
			return true;
		}
	}

	class SpawnedHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			WorldManagerClient.SpawnedMessage spawnedMsg = (WorldManagerClient.SpawnedMessage) msg;
			OID objOid = spawnedMsg.getSubject();
			CombatInfo info = getCombatInfo(objOid);
			if (info != null && info.isUser()) {
				// Send down the messages about the players current
				// abilities/skills etc.
				Log.debug("AJ: Sending combat messages");
				ExtendedCombatMessages.sendAbilities(info.getOwnerOid(),
						info.getCurrentAbilities());
				ExtendedCombatMessages.sendActions(info.getOwnerOid(),
						info.getCurrentActions());
				ExtendedCombatMessages.sendSkills(info.getOwnerOid(),
						info.getCurrentSkillInfo());
			}
			return true;
		}
	}

	/**
	 * Called when a player enters a combat area and their health properties
	 * need to be set. Also called when a player levels up.
	 * 
	 * @author Andrew
	 * 
	 */
	class SetHealthPropertiesHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage spawnedMsg = (ExtensionMessage) msg;
			OID objOid = spawnedMsg.getSubject();
			CombatInfo info = getCombatInfo(objOid);
			if (info.isUser()) {
				// Set players health based on their level
				int level = (Integer) info.getProperty("level");
				int health = 20 + ((level - 1) * 2);
				info.statSetBaseValue("health", health);
				info.statSetBaseValue("health-max", health);
			}
			return true;
		}
	}

	/**
	 * Called when a player enters a combat area and their health properties
	 * need to be set. Also called when a player levels up.
	 * 
	 * @author Andrew
	 * 
	 */
	class RegenerateHealthHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage healthMsg = (ExtensionMessage) msg;
			OID objOid = healthMsg.getSubject();
			CombatInfo info = getCombatInfo(objOid);
			int health = info
					.statGetCurrentValue(CombatInfo.COMBAT_PROP_HEALTH);
			int amount = (Integer) healthMsg.getProperty("amount");
			int healthMax = info.statGetCurrentValue("health-max");
			health += amount;
			if (health > healthMax) {
				health = healthMax;
			}
			info.statSetBaseValue(CombatInfo.COMBAT_PROP_HEALTH, health);
			/*
			 * if (health < healthMax) { // If health is lower than max, regen
			 * some CombatClient.startAbility(200, objOid, objOid, null); }
			 */
			return true;
		}
	}

	class TargetTypeUpdateHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.TargetTypeMessage ttMsg = (CombatClient.TargetTypeMessage) msg;
			OID objOid = ttMsg.getSubject();
			int targetType = (Integer) ttMsg.getTargetType();
			String species = (String) ttMsg.getSpecies();
			OID target = (OID) ttMsg.getTargetOid();
			CombatInfo info = getCombatInfo(objOid);
			if (info == null)
				return true;
			Log.debug("TARGET: got target type update. Setting target: "
					+ target + " to type: " + targetType + " for mob: "
					+ objOid);
			// First remove target from all sets
			info.removeAttackableTarget(target);
			info.removeFriendlyTarget(target);
			// Create new target Info
			TargetInfo tInfo = new TargetInfo();
			tInfo.setOid(target);
			tInfo.setSpecies(species);
			if (targetType == FactionPlugin.Attackable) {
				info.addAttackableTarget(target, tInfo);
			} else if (targetType == FactionPlugin.Healable) {
				info.addFriendlyTarget(target, tInfo);
			}
			return true;
		}
	}

	public static void addAttacker(OID target, OID attacker) {
		lock.lock();
		try {
			Set<OID> attackers = autoAttackReverseMap.get(target);
			if (attackers == null) {
				attackers = new HashSet<OID>();
				autoAttackReverseMap.put(target, attackers);
			}
			attackers.add(attacker);
			lastAttackerMap.put(target, attacker);
		} finally {
			lock.unlock();
		}
		// Check if the mobs tagOwner property is set
		CombatInfo info = getCombatInfo(target);

		if (info.isMob()) {
			OID tagOwner = (OID) info.getProperty(CombatInfo.COMBAT_TAG_OWNER);
			if (tagOwner == null) {
				info.setProperty(CombatInfo.COMBAT_TAG_OWNER, attacker);
			}
		}
	}

	public static void removeAttacker(OID target, OID attacker) {
		lock.lock();
		try {
			Set<OID> attackers = autoAttackReverseMap.get(target);
			if (attackers != null) {
				attackers.remove(attacker);
				if (attackers.isEmpty()) {
					autoAttackReverseMap.remove(target);
				}
			}
		} finally {
			lock.unlock();
		}
	}

	public static Set<OID> getAttackers(OID target) {
		lock.lock();
		try {
			return autoAttackReverseMap.get(target);
		} finally {
			lock.unlock();
		}
	}

	public static void clearAttackers(OID target) {
		lock.lock();
		try {
			autoAttackReverseMap.remove(target);
		} finally {
			lock.unlock();
		}
	}

	protected static Map<OID, Set<OID>> autoAttackReverseMap = new HashMap<OID, Set<OID>>();
	protected static Map<OID, OID> lastAttackerMap = new HashMap<OID, OID>();

	public static void registerStat(AgisStatDef stat) {
		registerStat(stat, false, new String[0]);
	}

	public static void registerStat(AgisStatDef stat, boolean isPublic) {
		registerStat(stat, isPublic, new String[0]);
	}

	public static void registerStat(AgisStatDef stat, boolean isPublic,
			String... dependencies) {
		String statName = stat.getName();
		if (statDefMap.containsKey(statName)) {
			throw new AORuntimeException("stat already defined");
		}
		statDefMap.put(statName, stat);
		if (dependencies.length == 0) {
			baseStats.add(stat);
		}
		for (String depName : dependencies) {
			AgisStatDef depStat = statDefMap.get(depName);
			if (depStat != null) {
				depStat.addDependent(stat);
			} else {
				Log.error("no stat definition for dependency " + depName
						+ " of stat " + statName);
			}
		}
		if (isPublic)
			publicStats.add(statName);
	}

	public static AgisStatDef lookupStatDef(String name) {
		return statDefMap.get(name);
	}

	protected static Map<String, AgisStatDef> statDefMap = new HashMap<String, AgisStatDef>();
	protected static Set<AgisStatDef> baseStats = new HashSet<AgisStatDef>();
	public static Set<String> publicStats = new HashSet<String>();

	// Process Skill training message
	class AddSkillHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage reqMsg = (ExtensionMessage) msg;

			applySkillTraining((OID) reqMsg.getProperty("playerOid"),
					(Integer) reqMsg.getProperty("skill"));

			return true;
		}
	}

	class AddAbilityHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage reqMsg = (ExtensionMessage) msg;

			CombatInfo player = getCombatInfo(reqMsg.getSubject());
			// player.addAbility((Integer)reqMsg.getProperty("abilityID"));
			// sendAbilityUpdate(player);
			return true;
		}
	}

	class GetAbilityHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage getMsg = (ExtensionMessage) msg;

			AgisAbility ability = Agis.AbilityManager.get((Integer) getMsg
					.getProperty("abilityID"));
			HashMap<String, String> abilityInfo = new HashMap<String, String>();
			abilityInfo.put("name", ability.getName());
			abilityInfo.put("id", "" + ability.getID());
			abilityInfo.put("icon", ability.getIcon());
			Engine.getAgent().sendObjectResponse(msg, abilityInfo);

			return true;
		}
	}

	// Add the Skill to the player object and notify the client of the updates
	public void applySkillTraining(OID playerOid, int skill) {
		Log.debug("CombatPlugin.applySkillTraining : skill = " + skill);
		CombatInfo player = (CombatInfo) getCombatInfo(playerOid);
		// Only add the skill to the player if he does not already have that
		// skill

		/*
		 * if (!player.getCurrentSkillInfo().getSkills().containsKey(skill)) {
		 * player.addSkill(skill); // Also adds default ability
		 * ClassAbilityPlugin.sendSkillUpdate(player); // Handle skill updates
		 * // in ClassAbilityPlugin sendAbilityUpdate(player); // Move to
		 * ClassAbilityPlugin? } else { Map<String, Serializable> props = new
		 * HashMap<String, Serializable>(); props.put("ext_msg_subtype",
		 * "ao.TRAINING_FAILED"); props.put("playerOid", playerOid);
		 * props.put("reason",
		 * "You cannot train any further in the selected skill.");
		 * 
		 * TargetedExtensionMessage msg = new TargetedExtensionMessage(
		 * CombatClient.MSG_TYPE_TRAINING_FAILED, playerOid, playerOid, false,
		 * props); Engine.getAgent().sendBroadcast(msg); }
		 */
	}

	public static AgisStatDef getBaseStatDef(String name) {
		for (AgisStatDef statdef : baseStats) {
			if (statdef.getName().equals(name)) {
				return statdef;
			}
		}
		return null;
	}

	class RemoveArenaEffectsHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ArenaClient.removeEffectsMessage EBMsg = (ArenaClient.removeEffectsMessage) msg;
			OID oid = EBMsg.getSubject();
			String type = (String) EBMsg.getProperty("type");
			int ID = (Integer) EBMsg.getProperty("ID");
			CombatInfo info = getCombatInfo(oid);
			// TODO: Deal with this later
			return true;
		}
	}

	/**
	 * Handles the EquipBonusMessage which is sent when a player/mob equips or
	 * unequips an item. Calls the UpdateEquiperStats which updates the stats
	 * for the player/mob based on the item equipped or unequipped.
	 * 
	 * @author Andrew Harrison
	 * 
	 */
	class ItemAcquireStatusChangeHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			AgisInventoryClient.ItemAcquiredStatusMessage EBMsg = (AgisInventoryClient.ItemAcquiredStatusMessage) msg;
			OID oid = EBMsg.getSubject();
			Log.debug("ITEM ACQUIRE CHANGE: item " + EBMsg.getItem()
					+ " was acquired? " + EBMsg.getAcquired());
			CombatInfo info = getCombatInfo(oid);
			EquipHelper.ItemAcquiredStatHelper(oid, EBMsg.getItem(),
					EBMsg.getAcquired(), info);
			return true;
		}
	}

	/**
	 * Handles the EquipBonusMessage which is sent when a player/mob equips or
	 * unequips an item. Calls the UpdateEquiperStats which updates the stats
	 * for the player/mob based on the item equipped or unequipped.
	 * 
	 * @author Andrew Harrison
	 * 
	 */
	class ItemEquipStatusChangeHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			AgisInventoryClient.ItemEquipStatusMessage EBMsg = (AgisInventoryClient.ItemEquipStatusMessage) msg;
			OID oid = EBMsg.getSubject();
			Log.debug("ITEM EQUIP CHANGE: " + EBMsg);
			CombatInfo info = getCombatInfo(oid);
			EquipHelper.UpdateEquiperStats(oid, EBMsg.getItem(),
					EBMsg.getEquipped(), info);
			return true;
		}
	}

	class ApplyEffectHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.applyEffectMessage upsMsg = (CombatClient.applyEffectMessage) msg;
			OID oid = upsMsg.getSubject();
			int effectID = (Integer) upsMsg.getProperty("effectID");

			Log.debug("COMBATPLUGIN: about to apply effect: " + effectID
					+ " to object: " + oid);
			CombatInfo info = getCombatInfo(oid);
			AgisEffect effect = Agis.EffectManager.get(effectID);
			Map<String, Integer> params = new HashMap<String, Integer>();
			params.put("skillType", -1);
			params.put("hitRoll", 50);
			AgisEffect.applyEffect(effect, info, info, -1, params);
			return true;
		}
	}

	class RemoveEffectHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			CombatClient.removeEffectMessage upsMsg = (CombatClient.removeEffectMessage) msg;
			OID oid = upsMsg.getSubject();
			int effectID = upsMsg.getEffectID();

			Log.debug("COMBATPLUGIN: about to remove effect: " + effectID
					+ " from object: " + oid);
			CombatInfo info = getCombatInfo(oid);
			AgisEffect.removeEffectByID(info, effectID);
			return true;
		}
	}
	
	/**
	 * Handles a message from the player when they want to remove one of their buffs.
	 * @author Andrew Harrison
	 *
	 */
	class RemoveBuffHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage rmMsg = (ExtensionMessage) msg;
			
			OID oid = rmMsg.getSubject();
			CombatInfo info = getCombatInfo(oid);

			int effectID = (Integer) rmMsg.getProperty("effectID");
			int pos = (Integer) rmMsg.getProperty("pos");
			
			ArrayList<EffectState> playerEffects = new ArrayList<EffectState>(info.getEffects(1));
			//Log.debug("REMOVE: playerEffect at pos: " + 0 + " is: " + playerEffects.get(pos).getEffectID() + " against " + effectID);
			if (playerEffects.get(pos).getEffectID() == effectID && playerEffects.get(pos).getEffect().isBuff()) {
				AgisEffect.removeEffect(playerEffects.get(pos));
			}

			return true;
		}
	}

	// Log the login information and send a response
	class LoginHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			LoginMessage message = (LoginMessage) msg;
			OID playerOid = message.getSubject();
			OID instanceOid = message.getInstanceOid();
			Log.debug("LoginHook: playerOid=" + playerOid + " instanceOid="
					+ instanceOid);

			// Do something
			CombatInfo info = getCombatInfo(playerOid);
			/*
			 * if (info != null) { AgisEffect regenEffect =
			 * Agis.EffectManager.get(HEALTH_REGEN_EFFECT); AgisEffect
			 * regenEffect2 = Agis.EffectManager.get(ENERGY_REGEN_EFFECT);
			 * startRegen(info, "health", regenEffect); startRegen(info, "mana",
			 * regenEffect2);
			 * 
			 * }
			 */
			
			if (info.dead()) {
				CombatClient.releaseObject(playerOid, false);
			}
			
			EnginePlugin.setObjectProperty(playerOid, CombatClient.NAMESPACE,
					"combatstate", false);
			EnginePlugin.setObjectPropertyNoResponse(playerOid,
					WorldManagerClient.NAMESPACE, "weaponsSheathed", true);

			// Update the attitudes of all players around this one
			// TODO: addPlayerAttitudes(playerOid);?
			Engine.getAgent().sendResponse(new ResponseMessage(message));
			return true;
		}
	}

	// Log the logout information and send a response. No longer used.
	class LogoutHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			LogoutMessage message = (LogoutMessage) msg;
			OID playerOid = message.getSubject();
			Log.debug("LOGOUT: combat logout started for: " + playerOid);
			// removeBreathEffect(playerOid);
			// removeFatigueEffect(playerOid);
			// Log.debug("LogoutHook: playerOid=" + playerOid);
			// CombatInfo info = getCombatInfo(playerOid);
			// Do something
			Engine.getAgent().sendResponse(new ResponseMessage(message));
			Log.debug("LOGOUT: combat logout finished for: " + playerOid);
			return true;
		}
	}

	/**
	 * Sets up the starting stats for players and mobs. I'm not totally happy
	 * with this setup.
	 * 
	 * @param level
	 * @param isMob
	 * @param baseHealth
	 * @param healthMod
	 * @param baseMana
	 * @param manaMod
	 * @return
	 */
	public HashMap<String, AgisStat> getStartingStats(int level, boolean isMob,
			HashMap<String, Integer> statOverrides) {
		HashMap<String, AgisStat> statMap = new HashMap<String, AgisStat>();
		// Other Stats
		int dmg_dealt_mod = 0;
		int dmg_taken_mod = 0;
		// int attack_speed = 2000;
		//int dmg_base = 20;
		statMap.put("dmg-dealt-mod", new AgisStat("dmg-dealt-mod",
				dmg_dealt_mod));
		statMap.put("dmg-taken-mod", new AgisStat("dmg-taken-mod",
				dmg_taken_mod));

		// statMap.put("attack_speed", new AgisStat("attack_speed",
		// attack_speed));
		statMap.put("experience-max", new AgisStat("experience-max",
				ClassAbilityPlugin.GetStartingXpReq()));
		statMap.put("experience", new AgisStat("experience", 0));
		statMap.put(LEVEL_STAT, new AgisStat(LEVEL_STAT, level));
		// Mobs stats are set by default with override stats being used to alter
		// values for an indivdual mob
		if (isMob) {
			if (STAT_LIST == null) {
				CombatDatabase cDB = new CombatDatabase(false);
				loadStatData(cDB);
				cDB.close();
			}
			for (String stat : STAT_LIST) {
				AgisStatDef statDef = statDefMap.get(stat);
				float value = statDef.getMobStartingValue()
						+ ((level - 1) * statDef.getMobLevelIncrease());
				if (statOverrides != null && statOverrides.containsKey(stat)) {
					value = statOverrides.get(stat);
					Log.debug("STAT: using mob stat override for stat: " + stat
							+ " with value: " + value);
				} else {
					value += value
							* ((level - 1) * (statDef
									.getMobLevelPercentIncrease() / 100));
				}
				// statMap.put(stat, new AgisStat(stat, 15 + level * 5));
				if (DAMAGE_TYPES.containsValue(stat)) {
					statMap.put(stat, new AgisStat(stat, (int) value));
				} else {
					statMap.put(stat, new AgisStat(stat, (int) value));
				}
				Log.debug("STAT: gave mob stat: " + stat + " with value: "
						+ value);
			}

			// dmg_base = (int)((10 + level * 3) *
			// ((float)attack_speed/1000.0f));
			// statMap.put("dmg-base", new AgisStat("dmg-base", dmg_base));
		} else {
			// Still need to do health and mana calculations for player
			// characters

			statMap.put("dmg-base", new AgisStat("dmg-base", 0));
		}
		return statMap;
	}

	// Temporary tick system
	class CombatStatTick implements Runnable {

		public CombatStatTick(CombatInfo info) {
			this.info = info;
			oid = info.getOwnerOid();
		}

		CombatInfo info;
		OID oid;
		boolean active = true;

		public void run() {
			// for (CombatInfo info : activeInfos) {
			if (info != null && active) {
				info.runCombatTick();
				Engine.getExecutor().schedule(this, 1, TimeUnit.SECONDS);
			} else {
				statTicks.remove(oid);
			}
			// }
		}

		public void disable() {
			active = false;
			Log.debug("STAT: disabling vitality stat ticks");
		}

		/*
		 * public void AddCombatInfo(CombatInfo info) { activeInfos.add(info); }
		 * public void RemoveCombatInfo(CombatInfo info) {
		 * activeInfos.remove(info); } ArrayList<CombatInfo> activeInfos = new
		 * ArrayList<CombatInfo>();
		 */
	}
	
	public static boolean isPlayerAlive(OID playerOid) {
		// Dead check
    	Boolean dead = (Boolean) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
    	if (dead != null && dead) {
    		return false;
    	} else {
    		return true;
    	}
	}

	// CombatStatTick statTick = new CombatStatTick();
	HashMap<OID, CombatStatTick> statTicks = new HashMap<OID, CombatStatTick>();

	// Graveyards
	HashMap<Integer, ArrayList<Graveyard>> graveyards = new HashMap<Integer, ArrayList<Graveyard>>();

	// Static settings
	public static final int STAT_BASE = 100;
	public static final int HEALTH_BASE = 500;
	public static final int MANA_BASE = 10;
	public static final int FLATRESIST_BASE = 0;
	public static final int PERCENTRESIST_BASE = 0;

	public static String DAMAGE_DEALT_MODIFIER = "dmg-dealt-mod";
	public static String DAMAGE_TAKEN_MODIFIER = "dmg-taken-mod";
	public static String ATTACK_SPEED_STAT = "attack_speed";
	// These stat mappings are loaded in from the database
	public static String PHYSICAL_POWER_STAT = null;
	public static String MAGICAL_POWER_STAT = null;
	public static String PHYSICAL_ACCURACY_STAT = null;
	public static String MAGICAL_ACCURACY_STAT = null;
	public static String WEIGHT_STAT = null;
	public static String LEVEL_STAT = "level";

	// Maps damage types to the stat that is used to reduce damage taken
	public static HashMap<String, String> DAMAGE_TYPES;
	public static LinkedList<String> STAT_LIST;

	public static final String PROP_HITBOX = "hitBox";

	public static boolean RANGE_CHECK_VERTICAL = false;
	public static boolean MOBS_STOP_TO_ATTACK = false;
	public static int SPIRIT_EFFECT = -1;
	public static boolean RELEASE_ON_LOGIN = true;
	public static boolean MAGICAL_ATTACKS_USE_WEAPON_DAMAGE = false;
	public static boolean EXP_BASED_ON_DAMAGE_DEALT = true;
	public static boolean WEAPON_REQ_USES_SHARED_TYPES = true;

	// Settings below here to be deleted
	public static final int ATTACK_ABILITY = 1;
	public static final int PLAYER_ATTACK_ABILITY = 1;

	public static final int DEFAULT_MOVEMENT_SPEED = 7;
}
