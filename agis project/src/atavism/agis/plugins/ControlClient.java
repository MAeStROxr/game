package atavism.agis.plugins;


import atavism.msgsys.MessageType;
import atavism.server.engine.OID;
import mygame.Proto;
import mygame.proto.Control;
import mygame.proto.ControlPlayerMsg;
import mygame.proto.ControlPointMsg;

import java.util.Set;
import java.util.function.BiConsumer;

public class ControlClient {

	public static void sendTargetedControlPointMsg(ControlPointMsg pointMsg,OID targetOID) {
		Proto.sendExtensionFlatbuffer(pointMsg.getByteBuffer(),"ControlPointMsg",Control.name(Control.CONTROL_POINT),targetOID,OID.fromLong(pointMsg.subjectOID()));


	}

	public static void sendControlPointMsg(ControlPointMsg pointMsg, Set<OID> targets) {
		for (OID playerOid : targets) {
			sendTargetedControlPointMsg(pointMsg, playerOid);
		}
	}


	public static void sendControlPointMsg(ControlPointMsg pointMsg, Set<OID> targets, BiConsumer<OID,ControlPointMsg> changer) {
		for (OID playerOid : targets) {
			changer.accept(playerOid,pointMsg);
			ControlClient.sendTargetedControlPointMsg(pointMsg, playerOid);
		}
	}

	public static void sendTargetedControlPlayerMsg(ControlPlayerMsg controlPlayerMsg,OID target) {
		Proto.sendExtensionFlatbuffer(controlPlayerMsg.getByteBuffer(),"ControlPlayerMsg",Control.name(Control.PLAYER_CONTROL),
				target,OID.fromLong(controlPlayerMsg.subjectOID()));
	}

	public static void sendControlPlayerMsg(ControlPlayerMsg controlPlayerMsg,Set<OID> targets) {
		for (OID playerOid : targets) {
			Proto.sendExtensionFlatbuffer(controlPlayerMsg.getByteBuffer(), "ControlPlayerMsg", Control.name(Control.PLAYER_CONTROL),
					playerOid, OID.fromLong(controlPlayerMsg.subjectOID()));
		}
	}

	public static void sendControlPlayerMsg(ControlPlayerMsg controlPlayerMsg,Set<OID> targets, BiConsumer<OID,ControlPlayerMsg> changer) {
		for (OID playerOid : targets) {
			changer.accept(playerOid,controlPlayerMsg);
			Proto.sendExtensionFlatbuffer(controlPlayerMsg.getByteBuffer(), "ControlPlayerMsg", Control.name(Control.PLAYER_CONTROL),
					playerOid, OID.fromLong(controlPlayerMsg.subjectOID()));
		}
	}

	public static final MessageType MSG_TYPE_CONTROL_POINT = MessageType.intern("control."+Control.name(Control.CONTROL_POINT));

	public static final MessageType MSG_TYPE_PLAYER_CONTROL = MessageType
			.intern("control."+Control.name(Control.PLAYER_CONTROL));





}
