package atavism.agis.plugins;

import atavism.msgsys.Message;
import atavism.msgsys.MessageTypeFilter;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Hook;
import atavism.server.engine.OID;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.util.LockFactory;
import atavism.server.util.Logger;
import mygame.Proto;
import mygame.control.ControlPlayer;
import mygame.control.ControlPlayerManager;
import mygame.control.ControlPointManager;
import mygame.proto.ControlPlayerCmd;
import mygame.proto.ControlPlayerMsg;
import mygame.proto.ControlPointMsg;
import mygame.proto.PointCmd;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;

public class ControlPlugin extends EnginePlugin {
	protected static final Logger log = new Logger("ControlPlugin");
	protected static Lock lock = LockFactory.makeLock("ControlPlugin");
	ControlPointManager pointManager = new ControlPointManager();
	ControlPlayerManager playerManager = new ControlPlayerManager();
	Proto proto  = new Proto();
	public ControlPlugin() {
		super("ControlPlugin");

		setPluginType("ControlPlugin");
		log.debug("ControlPlugin Constructor\n");
		pointManager.setPlayerManager(playerManager);
		pointManager.loadControl();
		pointManager.startTasks();
	}

	public static String TACTICAL_PLUGIN_NAME = "ControlPoint";
	

	public String getName() {
		return TACTICAL_PLUGIN_NAME;
	}



	public void onActivate() {


		log.debug("ControlPlugin.onActivate()\n"+playerManager.getRoot().printSubTree());
		// getHookManager().addHook(ControlClient.MSG_TYPE_PLAYER_ADD, new
		// AddPlayerHook());
		getHookManager().addHook(ControlClient.MSG_TYPE_CONTROL_POINT, new ControlPointHook());
		getHookManager().addHook(ControlClient.MSG_TYPE_PLAYER_CONTROL, new PlayerControl());
		getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
				new LoginHook());
		getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT,
				new LogoutHook());
		MessageTypeFilter filter = new MessageTypeFilter();
		filter.addType(ControlClient.MSG_TYPE_CONTROL_POINT);
		filter.addType(LoginMessage.MSG_TYPE_LOGIN);
		filter.addType(LogoutMessage.MSG_TYPE_LOGOUT);
		filter.addType(ControlClient.MSG_TYPE_PLAYER_CONTROL);
		Engine.getAgent().createSubscription(filter, this);


	}



	class LoginHook implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags){
			LoginMessage message = (LoginMessage) msg;
			OID playerOid = message.getSubject();
			String name = message.getPlayerName();
			log.debug("Control Login:"+name+" "+playerOid);
			playerManager.addPlayer(playerOid,new ControlPlayer(playerOid,message.getPlayerName()));
			playerManager.loginPlayer(playerManager.getPlayer(playerOid));
			return true;
		}
	}
	class LogoutHook implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags) {
			LogoutMessage logoutMsg = (LogoutMessage)msg;
			OID playerOid = logoutMsg.getSubject();
			log.debug("Control Logout:"+logoutMsg.getPlayerName()+" "+playerOid);
			playerManager.logoutPlayer(playerManager.getPlayer(playerOid));
			return true;
		}
	}
	class ControlPointHook implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage gridMsg = (ExtensionMessage) msg;
			ByteBuffer bb = Proto.getProtoMessage(gridMsg,"ControlPointMsg");
			ControlPointMsg pointMsg = ControlPointMsg.getRootAsControlPointMsg(bb);
			log.debug("Control Point:"+pointMsg.OID()+" cmd:"+pointMsg.cmd()+
					PointCmd.name(pointMsg.cmd())+" subjectOid:"+pointMsg.subjectOID());
			pointManager.processPointMsg(pointMsg);
			return true;
		}
	}

	

	class PlayerControl implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage gridMsg = (ExtensionMessage) msg;
			ByteBuffer bb = Proto.getProtoMessage(gridMsg,"ControlPlayerMsg");
			ControlPlayerMsg playerMsg = ControlPlayerMsg.getRootAsControlPlayerMsg(bb);
			log.debug("Control Player:"+playerMsg.subjectOID()+" cmd:"+ ControlPlayerCmd.name(playerMsg.cmd()));
			playerManager.processPlayerMsg(playerMsg);
			return true;
		}
	}



}
