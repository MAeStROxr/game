package atavism.agis.plugins;

import atavism.agis.core.Agis;
import atavism.agis.database.ContentDatabase;
import atavism.agis.database.ItemDatabase;
import atavism.agis.objects.*;
import atavism.agis.plugins.CraftingClient.CreateResourceNodeFromMobMessage;
import atavism.agis.plugins.CraftingClient.DestroyMobResourceNodeMessage;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.SubjectMessage;
import atavism.server.engine.*;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.objects.ObjectTypes;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.ObjectInfo;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Plugin for managing Crafting and Resource gathering.
 * @author Andrew Harrison
 *
 */
public class CraftingPlugin extends EnginePlugin {
	
	public CraftingPlugin()
	{
		super(CRAFTING_PLUGIN_NAME);
		setPluginType("Crafting");
	}
	
	public static String CRAFTING_PLUGIN_NAME = "CraftingPlugin";
	
	public String getName() {
		return CRAFTING_PLUGIN_NAME;
	}

	public void onActivate() {
		Log.debug("CraftingPlugin.onActivate()");
		registerHooks();
		
		MessageTypeFilter filter = new MessageTypeFilter();
		filter.addType(WorldManagerClient.MSG_TYPE_SPAWNED);
        filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
		filter.addType(InstanceClient.MSG_TYPE_INSTANCE_LOADED);
		filter.addType(CraftingClient.MSG_TYPE_CREATE_RESOURCE_NODE_FROM_MOB);
		filter.addType(CraftingClient.MSG_TYPE_DESTROY_MOB_RESOURCE_NODE);
		filter.addType(CraftingClient.MSG_TYPE_HARVEST_RESOURCE);
		filter.addType(CraftingClient.MSG_TYPE_GATHER_RESOURCE);
		filter.addType(CraftingClient.MSG_TYPE_CRAFTING_CRAFT_ITEM);
		filter.addType(CraftingClient.MSG_TYPE_CRAFTING_GRID_UPDATED);
		filter.addType(CraftingClient.MSG_TYPE_GET_BLUEPRINTS);
		Engine.getAgent().createSubscription(filter, this);
		Log.debug("CRAFTING: completed Plugin activation");
		
		ItemDatabase iDB = new ItemDatabase(false);
		recipes = iDB.loadCraftingRecipes();
		cDB = new ContentDatabase(true);
		String resourceDropsOnFail = cDB.loadGameSetting("RESOURCE_DROPS_ON_FAIL");
        if (resourceDropsOnFail != null)
        	RESOURCE_DROPS_ON_FAIL = Boolean.parseBoolean(resourceDropsOnFail);
        String autoPickupResources = cDB.loadGameSetting("AUTO_PICKUP_RESOURCES");
        if (autoPickupResources != null)
        	AUTO_PICKUP_RESOURCES = Boolean.parseBoolean(autoPickupResources);
        String resourceGatherDistance = cDB.loadGameSetting("RESOURCE_GATHER_DISTANCE");
        if (resourceGatherDistance != null)
        	RESOURCE_GATHER_DISTANCE = Integer.parseInt(resourceGatherDistance);
        String gatherCanFail = cDB.loadGameSetting("RESOURCE_GATHER_CAN_FAIL");
        if (gatherCanFail != null)
        	RESOURCE_GATHER_CAN_FAIL = Boolean.parseBoolean(gatherCanFail);
        String skinningSkillID = cDB.loadGameSetting("SKINNING_SKILL_ID");
        if (skinningSkillID != null)
        	SKINNING_SKILL_ID = Integer.parseInt(skinningSkillID);
        String skinningWeaponReq = cDB.loadGameSetting("SKINNING_WEAPON_REQ");
        if (skinningWeaponReq != null)
        	SKINNING_WEAPON_REQ = skinningWeaponReq;
        String resourceCountIsLootCount = cDB.loadGameSetting("RESOURCE_COUNT_IS_LOOT_COUNT");
        if (resourceCountIsLootCount != null)
        	RESOURCE_COUNT_IS_LOOT_COUNT = Boolean.parseBoolean(resourceCountIsLootCount);
        String useResourceGroups = cDB.loadGameSetting("USE_RESOURCE_GROUPS");
        if (useResourceGroups != null)
        	USE_RESOURCE_GROUPS = Boolean.parseBoolean(useResourceGroups);
        String resourceGroupSize = cDB.loadGameSetting("RESOURCE_GROUP_SIZE");
        if (resourceGroupSize != null)
        	RESOURCE_GROUP_SIZE = Integer.parseInt(resourceGroupSize);
	}
	
	protected void registerHooks(){
		getHookManager().addHook(WorldManagerClient.MSG_TYPE_SPAWNED,
				 new SpawnedHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED,
				 new DespawnedHook());
		getHookManager().addHook(InstanceClient.MSG_TYPE_INSTANCE_LOADED,
        		new InstanceLoadedHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_CREATE_RESOURCE_NODE_FROM_MOB,
                new CreateResourceNodeFromMobHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_DESTROY_MOB_RESOURCE_NODE,
                new DestroyResourceNodeHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_HARVEST_RESOURCE,
                new HarvestResourceHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_GATHER_RESOURCE,
                new GatherResourceHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_CRAFTING_CRAFT_ITEM,
				new CraftItemHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_CRAFTING_GRID_UPDATED,
				new CraftingGridUpdatedHook());
		getHookManager().addHook(CraftingClient.MSG_TYPE_GET_BLUEPRINTS,
				new SendBlueprintHook());
	}
	
	class SpawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.SpawnedMessage spawnedMsg = (WorldManagerClient.SpawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            if (WorldManagerClient.getObjectInfo(objOid).objType == ObjectTypes.player) {
            	// Set the players world property
            	Log.debug("SPAWNED: getting claims for player: " + objOid);
            	OID instanceOid = spawnedMsg.getInstanceOid();
            	Point p = WorldManagerClient.getObjectInfo(objOid).loc;
            	if (CraftingPlugin.USE_RESOURCE_GROUPS) {
            		for (ResourceNodeGroup nodeGroup : resourceNodeGroups.get(instanceOid).values()) {
            	    	float distance = Point.distanceToSquaredXZ(p, new Point(nodeGroup.getLoc()));
            	    	if (distance < 100000) {
            	    		nodeGroup.addPlayer(objOid);
            	    	}
            	    }
            	} else {
            		for (ResourceNode rNode : resourceNodes.get(instanceOid).values()) {
            	    	float distance = Point.distanceToSquaredXZ(p, new Point(rNode.getLoc()));
            	    	if (distance < 100000) {
            	    		rNode.addPlayer(objOid);
            	    	}
            	    }
            	}
        	    
        	    // Clear current task
    			EnginePlugin.setObjectProperty(objOid, WorldManagerClient.NAMESPACE, "currentTask", "");
            }
            
            
            
            return true;
    	}
    }
	
	class DespawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.DespawnedMessage spawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            ObjectInfo objInfo = WorldManagerClient.getObjectInfo(objOid);
            if (objInfo != null && objInfo.objType == ObjectTypes.player) {
            	OID instanceOid = spawnedMsg.getInstanceOid();
        	    // Remove the player from the resource Nodes in the instance they despawned from
        	    for (ResourceNode rNode : resourceNodes.get(instanceOid).values()) {
        	    	rNode.removePlayer(objOid);
        	    }
            } else if (objInfo != null && objInfo.objType == ObjectTypes.mob) {
            	OID instanceOid = spawnedMsg.getInstanceOid();
            	int nodeID = (int)(objOid.toLong() * -1);
            	if (resourceNodes.get(instanceOid).containsKey(nodeID)) {
            		Log.debug("RESOURCE: despawning mobs resource node: " + nodeID);
            		resourceNodes.get(instanceOid).get(nodeID).despawnResource();
            		resourceNodes.get(instanceOid).remove(nodeID);
            	}
            }
            
            return true;
    	}
    }
	
	/**
     * Hook for the InstanceLoadedMessage. The message is sent when an instance is ready for objects
     * to be loaded in and this hook will load the resource nodes for the instance.
     */
	class InstanceLoadedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		SubjectMessage message = (SubjectMessage) msg;
            OID instanceOid = message.getSubject();
            Log.debug("VOXEL: got instance loaded message with oid: " + instanceOid);
            int instanceID = (Integer)InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
            resourceNodes.put(instanceOid, cDB.loadResourceNodes(instanceID, instanceOid, resourceNodeGroups));
    		return true;
    	}
	}
	
	class CreateResourceNodeFromMobHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	CreateResourceNodeFromMobMessage gridMsg = (CreateResourceNodeFromMobMessage)msg;
            Log.debug("RESOURCE: got create resource node message");
            OID mobOid = gridMsg.getSubject();
            if (mobOid == null)
            	return true;
            
            int lootTable = gridMsg.getLootTable();
            int skillLevelReq = gridMsg.getSkillLevelReq();
            float harvestTime = gridMsg.getHarvestTime();
            
            int nodeID = (int)(mobOid.toLong() * -1);
            BasicWorldNode mobWNode = WorldManagerClient.getWorldNode(mobOid);
            ResourceNode resourceNode = new ResourceNode(nodeID, new AOVector(mobWNode.getLoc()), mobWNode.getInstanceOid());
            resourceNode.setName("mob");
			resourceNode.setGameObject("");
			resourceNode.setCoordEffect("SkinningEffect");
			resourceNode.setSkill(SKINNING_SKILL_ID);
			resourceNode.setSkillLevelReq(skillLevelReq);
			resourceNode.setSkillLevelMax(skillLevelReq+20);
			resourceNode.setWeaponReq(SKINNING_WEAPON_REQ);
			resourceNode.setEquippedReq(false);
			resourceNode.setRespawnTime(-1); // Set to -1 so it wont respawn
			resourceNode.setHarvestCount(1);
			resourceNode.setHarvestTimeReq(harvestTime);
			
			// Add resource drops
			Log.debug("LOOT: lootManager has: " + Agis.LootTableManager.getMap());
			LootTable lt = Agis.LootTableManager.get(lootTable);
			for (int i = 0; i < lt.getItems().size(); i++) {
				int templateID = lt.getItems().get(i);
				if (templateID > -1) {
					int count = lt.getItemCounts().get(i);
					resourceNode.AddResourceDrop(templateID, count, count, lt.getItemChances().get(i));
				}
			}
            
			resourceNode.setMobAsSkinnable(mobOid);
			// Save resource node
            resourceNodes.get(mobWNode.getInstanceOid()).put(nodeID, resourceNode);
            
            // Set mob as skinnable
            EnginePlugin.setObjectProperty(mobOid, Namespace.WORLD_MANAGER, "skinnableLevel", skillLevelReq);
            return true;
        }
	}
	
	class DestroyResourceNodeHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	DestroyMobResourceNodeMessage gridMsg = (DestroyMobResourceNodeMessage)msg;
            Log.debug("RESOURCE: got destroy resource message");
            
            OID mobOid = gridMsg.getSubject();
            if (mobOid == null)
            	return true;
            
            OID instanceOid = gridMsg.getInstanceOid();
            
            int nodeID = (int)(mobOid.toLong() * -1);
            resourceNodes.get(instanceOid).get(nodeID).despawnResource();
            resourceNodes.get(instanceOid).remove(nodeID);
            
            return true;
        }
	}
    
    /**
	 * Hook for the Harvest Resource Message. Used when a player attempts to start harvesting 
	 * from a ResourceNode.
	 * @author Andrew Harrison
	 *
	 */
	class HarvestResourceHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            Log.debug("RESOURCE: got harvest resource message");
            OID playerOid = gridMsg.getSubject();
            OID instanceOid = WorldManagerClient.getObjectInfo(playerOid).instanceOid;
            int resourceID = (Integer)gridMsg.getProperty("resourceID");
            if (resourceNodes.containsKey(instanceOid)) {
            	Log.debug("RESOURCE: got resource instance: " + instanceOid + " looking for node: " + resourceID);
            	if (resourceNodes.get(instanceOid).containsKey(resourceID)) {
            		Log.debug("RESOURCE: got resource"); 
            		resourceNodes.get(instanceOid).get(resourceID).tryHarvestResources(playerOid);
            	}
            }
            return true;
        }
	}
	
	/**
	 * Hook for the Gather Resource Message. Used when a player is gathering one (or all) of the items they 
	 * have harvested from the ResourceNode.
	 * @author Andrew Harrison
	 *
	 */
	class GatherResourceHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            Log.debug("RESOURCE: got gather resource message");
            OID playerOid = gridMsg.getSubject();
            OID instanceOid = WorldManagerClient.getObjectInfo(playerOid).instanceOid;
            int resourceID = (Integer)gridMsg.getProperty("resourceID");
            boolean gatherAll = (Boolean)gridMsg.getProperty("gatherAll");
            if (resourceNodes.containsKey(instanceOid)) {
            	Log.debug("RESOURCE: got resource instance: " + instanceOid + " looking for node: " + resourceID);
            	if (resourceNodes.get(instanceOid).containsKey(resourceID)) {
            		Log.debug("RESOURCE: got resource"); 
            		if (gatherAll) {
            			resourceNodes.get(instanceOid).get(resourceID).gatherAllItems(playerOid);
            		} else {
            			int itemID = (Integer)gridMsg.getProperty("itemID");
            			resourceNodes.get(instanceOid).get(resourceID).gatherItem(playerOid, itemID);
            		}
            	}
            }
            return true;
        }
	}
	
	/**
	 * Hook for the Craft Item Message. Calls either DoGridCraft() or DoStandardCraft()
	 * based on the GRID_BASED_CRAFTING value.
	 * @author Andrew
	 *
	 */
	class CraftItemHook implements Hook {
		public boolean processMessage(atavism.msgsys.Message msg, int flags) {
			ExtensionMessage eMsg = (ExtensionMessage)msg;
			OID playerOid = eMsg.getSubject();
			
			int recipeId = (Integer) eMsg.getProperty("RecipeId");
			String stationType = (String) eMsg.getProperty("stationType");
			int recipeItemID = (Integer) eMsg.getProperty("recipeItemID");
			
			// Death check
	    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
	    	if (dead) {
	    		ExtendedCombatMessages.sendErrorMessage(playerOid, "You cannot peform that action when dead");
	    		return true;
	    	}
	    	
	    	// Busy check
	    	String currentTask = (String)EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "currentTask");
	    	if (currentTask != null && !currentTask.equals("")) {
	    		// Player is currently on another task
	    		return true;
	    	}
			
			if (GRID_BASED_CRAFTING) {
				DoGridCraft(playerOid, recipeId, recipeItemID, stationType, eMsg);
			} else {
				DoStandardCraft(playerOid, recipeId, stationType, eMsg);
			}
			
		    return true;
		}
	}
	
	/**
	 * Attempts to craft an item based on the grid based crafting system.
	 * @param playerOid
	 * @param recipeId
	 * @param recipeItemID
	 * @param stationType
	 * @param eMsg
	 */
	void DoGridCraft(OID playerOid, int recipeId, int recipeItemID, String stationType, ExtensionMessage eMsg) {
		LinkedList<Long> components = null;
		LinkedList<Integer> componentCounts = null;
		CraftingRecipe recipe = null;
		
		if (recipeItemID > 0) {
			CraftingRecipe resultRecipe = null;
			for (CraftingRecipe tempRecipe : recipes.values()) {
				if (tempRecipe.getRecipeItemId() == recipeItemID) {
					resultRecipe = tempRecipe;
				}
			}
			Log.debug("CRAFTING: resultRecipe: " + resultRecipe);
			// Load components in 
			LinkedList<Integer> reqComponents = resultRecipe.getRequiredItems();
			componentCounts = resultRecipe.getRequiredItemCounts();
			if(AgisInventoryClient.checkComponents(playerOid, reqComponents, componentCounts)
				&& resultRecipe.getStationReq().equals(stationType))
			{
				recipe = resultRecipe;
				Map<String, Serializable> props = new HashMap<String, Serializable>();
				props.put("ext_msg_subtype", "CraftingMsg");
				props.put("PluginMessageType", "CraftingStarted");
				props.put("creationTime", recipe.getCreationTime());
				TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
						playerOid, false, props);
				
				Engine.getAgent().sendBroadcast(playerMsg);
				
				CraftingTask task = new CraftingTask(recipe, playerOid, recipeId);
				Engine.getExecutor().schedule(task, recipe.getCreationTime(), TimeUnit.SECONDS);
				
				// Play coord effect
				String coordEffect = (String) eMsg.getProperty("coordEffect");
				task.PlayCoordinatedEffect(coordEffect);
				
				// Delete recipe
				if (DELETE_CRAFTING_RECIPE_ON_USE)
					AgisInventoryClient.removeGenericItem(playerOid, recipeItemID, false, 1);
				return;
			}
		} else {
			Log.debug("CRAFTING: got craft item message with recipeID: " + recipeId);
			if (!recipes.containsKey(recipeId)) {
				return;
			}
			
			recipe = recipes.get(recipeId);
			Log.debug("CRAFTING: got recipe: " + recipe);
			LinkedList<Integer> componentIDs = recipe.getRequiredItems();
			LinkedList<Integer> componentReqCounts = recipe.getRequiredItemCounts();
			
			if (!recipe.getStationReq().equals(stationType) && !recipe.getStationReq().contains("none")) {
				return;
			}
			
			// Skill Level Check
			if (recipe.getSkillID() > 0) {
				int playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, recipe.getSkillID());
				Log.debug("CRAFTING: checking skill: " + recipe.getSkillID() + " against playerSkillLevel: " + playerSkillLevel);
				if (playerSkillLevel < recipe.getRequiredSkillLevel()) {
					return;
				}
			}
			
			components = (LinkedList<Long>) eMsg.getProperty("components");
			componentCounts = (LinkedList<Integer>) eMsg.getProperty("componentCounts");
			
			LinkedList<CraftingComponent> craftingComponents = new LinkedList<CraftingComponent>();
			for (int i = 0; i < componentIDs.size(); i++) {
				craftingComponents.add(new CraftingComponent("", componentCounts.get(i), componentIDs.get(i)));
			}
			
			if(AgisInventoryClient.checkSpecificComponents(playerOid, componentIDs, componentReqCounts, components, componentCounts))
			{
				Map<String, Serializable> props = new HashMap<String, Serializable>();
				props.put("ext_msg_subtype", "CraftingMsg");
				props.put("PluginMessageType", "CraftingStarted");
				props.put("creationTime", recipe.getCreationTime());
				TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
						playerOid, false, props);
				
				Engine.getAgent().sendBroadcast(playerMsg);
				
				// Set component counts so only the ones required are used
				for (int i = 0; i < componentReqCounts.size(); i++) {
					componentCounts.set(i, componentReqCounts.get(i));
				}
				
				CraftingTask task = new CraftingTask(recipe, components, componentCounts, playerOid, recipeId);
				Engine.getExecutor().schedule(task, recipe.getCreationTime(), TimeUnit.SECONDS);
				
				// Play coord effect
				String coordEffect = (String) eMsg.getProperty("coordEffect");
				task.PlayCoordinatedEffect(coordEffect);
				return;
			}
		}
		
		Log.debug("CRAFTING PLUGIN: Player doesn't have the required Components in their Inventory");
			
		Map<String, Serializable> props = new HashMap<String, Serializable>();
		props.put("ext_msg_subtype", "CraftingMsg");
		props.put("PluginMessageType", "CraftingFailed");
		props.put("ErrorMsg", "You do not have the required Components to craft this Recipe!");
		TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
				playerOid, false, props);
			
		Engine.getAgent().sendBroadcast(playerMsg);
	}
	
	/**
	 * Attempts to craft an item based on the standard crafting system (player doesn't need to place items in a grid).
	 * @param playerOid
	 * @param recipeId
	 * @param stationType
	 * @param eMsg
	 */
	void DoStandardCraft(OID playerOid, int recipeId, String stationType, ExtensionMessage eMsg) {
		String recipeName = (String) eMsg.getProperty("ItemName");
		Log.debug("CRAFTING: got recipe: " + recipeName);
		
		LinkedList<Integer> componentIds = new LinkedList<Integer>(); //(LinkedList<Integer>) cimsg.getProperty("ItemIds");
		LinkedList<Integer> componentStacks = (LinkedList<Integer>) eMsg.getProperty("ItemStacks");
		
		CraftingRecipe recipe = recipes.get(recipeId);
		
		if(recipe != null)
		{
			if (!recipe.getStationReq().equals(stationType)) {
				//TODO: maybe send error message?
				return;
			}
			
			LinkedList<LinkedList<CraftingComponent>> components = recipe.getRequiredCraftingComponents();
			for (int i = 0; i < components.size(); i++) {
				for (int j = 0; j < components.get(j).size(); j++) {
					componentIds.add(components.get(i).get(j).getItemId());
				}
			}
			
			// Skill Level Check
			int playerSkillLevel = ClassAbilityClient.getPlayerSkillLevel(playerOid, recipe.getSkillID());
			Log.debug("CRAFTING: checking skill: " + recipe.getSkillID() + " against playerSkillLevel: " + playerSkillLevel);
			if (playerSkillLevel < recipe.getSkillID()) {
				ExtendedCombatMessages.sendErrorMessage(playerOid, "You do not have the skill level required to craft this Resource Node");
				return;
			}
			
			if(AgisInventoryClient.checkComponents(playerOid, componentIds, componentStacks))
			{
				Map<String, Serializable> props = new HashMap<String, Serializable>();
				props.put("ext_msg_subtype", "CraftingMsg");
				props.put("PluginMessageType", "CraftingStarted");
				props.put("creationTime", recipe.getCreationTime());
				TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
						playerOid, false, props);
				
				Engine.getAgent().sendBroadcast(playerMsg);
				
				CraftingTask task = new CraftingTask(recipe, playerOid, recipeId);
				Engine.getExecutor().schedule(task, recipe.getCreationTime(), TimeUnit.SECONDS);
				
				// Play coord effect
				String coordEffect = (String) eMsg.getProperty("coordEffect");
				task.PlayCoordinatedEffect(coordEffect);
				return;
			}
			Log.debug("CRAFTING PLUGIN: User doesn't have the required Components in their Inventory!");
			
			Map<String, Serializable> props = new HashMap<String, Serializable>();
			props.put("ext_msg_subtype", "CraftingMsg");
			props.put("PluginMessageType", "CraftingFailed");
			props.put("ErrorMsg", "You do not have the required Components to craft this Recipe!");
			TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
					playerOid, false, props);
			
			Engine.getAgent().sendBroadcast(playerMsg);
			
			return;
		}
	}
	
	/**
	 * Hook for the Crafting Grid Updated Message. Checks to see if the items the player has placed
	 * in the grid match up with any recipies. If so, it sends down the result to the player.
	 * @author Andrew Harrison
	 *
	 */
	class CraftingGridUpdatedHook implements Hook {
		public boolean processMessage(atavism.msgsys.Message msg, int flags) {
			ExtensionMessage cimsg = (ExtensionMessage)msg;
			OID playerOid = cimsg.getSubject();
			
			Log.debug("CRAFTING: got grid updated message");
			
			// Death check
	    	boolean dead = (Boolean) EnginePlugin.getObjectProperty(playerOid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE);
	    	if (dead) {
	    		ExtendedCombatMessages.sendErrorMessage(playerOid, "You cannot peform that action when dead");
	    		return true;
	    	}
			
			int gridSize = (Integer) cimsg.getProperty("gridSize");
			LinkedList<Integer> componentIDs = (LinkedList<Integer>) cimsg.getProperty("componentIDs");
			LinkedList<Integer> componentCounts = (LinkedList<Integer>) cimsg.getProperty("componentCounts");
			String stationType = (String) cimsg.getProperty("stationType");
			int recipeItemID = (Integer) cimsg.getProperty("recipeItemID");
			
			LinkedList<CraftingComponent> craftingComponents = new LinkedList<CraftingComponent>();
			CraftingRecipe foundRecipe = null;
			
			if (recipeItemID > 0) {
				CraftingRecipe resultRecipe = null;
				for (CraftingRecipe recipe : recipes.values()) {
					if (recipe.getRecipeItemId() == recipeItemID) {
						resultRecipe = recipe;
					}
				}
				Log.debug("CRAFTING: resultRecipe: " + resultRecipe);
				// Load components in 
				LinkedList<Integer> reqComponentIDs = resultRecipe.getRequiredItems();
				LinkedList<Integer> componentReqCounts = resultRecipe.getRequiredItemCounts();
				if(AgisInventoryClient.checkComponents(playerOid, reqComponentIDs, componentReqCounts)
					&& resultRecipe.getStationReq().equals(stationType))
				{
					foundRecipe = resultRecipe;
				}
			} else {
				for (int i = 0; i < componentIDs.size(); i++) {
					craftingComponents.add(new CraftingComponent("", componentCounts.get(i), componentIDs.get(i)));
				}
				
				Log.debug("CRAFTING: checking recipes");
				
				// Split the component list into rows
				LinkedList<LinkedList<CraftingComponent>> componentRows = new LinkedList<LinkedList<CraftingComponent>>();
				for (int i = 0; i < gridSize; i++) {
					LinkedList<CraftingComponent> componentRow = new LinkedList<CraftingComponent>();
					for (int j = 0; j < gridSize; j++) {
						componentRow.add(craftingComponents.get(i*gridSize+j));
						Log.debug("CRAFTING: adding item: " + craftingComponents.get(i*gridSize+j).getItemId() + " to row: " + i + " in column: " + j);
					}
					componentRows.add(componentRow);
				}
				
				for (CraftingRecipe recipe : recipes.values()) {
					if (recipe.DoesRecipeMatch(componentRows, stationType)) {
						foundRecipe = recipe;
						break;
					}
				}
			}
			
			Log.debug("CRAFTING: found recipe: " + foundRecipe);
			Map<String, Serializable> props = new HashMap<String, Serializable>();
			props.put("ext_msg_subtype", "CraftingGridMsg");
			if (foundRecipe != null) {
				props.put("recipeID", foundRecipe.getID());
				props.put("recipeName", foundRecipe.getName());
				props.put("recipeItem", foundRecipe.getRecipeItemId());
				props.put("numResults", foundRecipe.getResultItemIds().size());
				for (int i = 0; i < foundRecipe.getResultItemIds().size(); i++) {
					props.put("resultItem" + i, foundRecipe.getResultItemIds().get(i));
					props.put("resultItemCount" + i, foundRecipe.getResultItemCounts().get(i));
				}
			} else {
				props.put("recipeID", -1);
				props.put("recipeName", "");
				props.put("recipeItem", -1);
				props.put("numResults", 0);
			}
			
			TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
					playerOid, false, props);
			
			Engine.getAgent().sendBroadcast(playerMsg);
			
		    return true;
		}
	}
	
	/**
	 * Hook for the Get Blueprints Message. Sends the Blueprint information to the client for the 
	 * recipes provided.
	 * @author Andrew Harrison
	 *
	 */
	class SendBlueprintHook implements Hook {
		public boolean processMessage(atavism.msgsys.Message msg, int flags) {
			ExtensionMessage cimsg = (ExtensionMessage)msg;
			OID playerOid = cimsg.getSubject();
			Log.debug("CRAFTING: got getBlueprintMessage");
			
			int numRecipes = (Integer) cimsg.getProperty("numRecipes");
			LinkedList<Integer> recipeIDs = new LinkedList<Integer>();
			
			for (int i = 0; i < numRecipes; i++) {
				recipeIDs.add((Integer) cimsg.getProperty("recipe" + i));
			}
			
			Map<String, Serializable> props = new HashMap<String, Serializable>();
			props.put("ext_msg_subtype", "BlueprintMsg");
			
			int blueprintNum = 0;
			for (int recipeId : recipeIDs) {
				CraftingRecipe recipe = recipes.get(recipeId);
				Log.debug("CRAFTING: got crafting recipe: " + recipe);
				if (recipe != null) {
					props.put("recipeID" + blueprintNum, recipe.getID());
					props.put("numResults", recipe.getResultItemIds().size());
					for (int i = 0; i < recipe.getResultItemIds().size(); i++) {
						props.put("resultItem" + i, recipe.getResultItemIds().get(i));
						props.put("resultItemCount" + i, recipe.getResultItemCounts().get(i));
					}
					props.put("recipeItemID" + blueprintNum, recipe.getRecipeItemId());
					props.put("station" + blueprintNum, recipe.getStationReq());
					int row = 0;
					for (LinkedList<CraftingComponent> recipeRow : recipe.getRequiredCraftingComponents()) {
						int column = 0;
						for (CraftingComponent component : recipeRow) {
							props.put("item" + blueprintNum + "_" + row + "_" + column, component.getItemId());
							column++;
						}
						props.put("numColumns" + blueprintNum + "_" + row, column);
						row++;
					}
					props.put("numRows" + blueprintNum, row);
					blueprintNum++;
				}
			}
			props.put("numBlueprints", blueprintNum);
			
			TargetedExtensionMessage playerMsg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
					playerOid, false, props);
			
			Engine.getAgent().sendBroadcast(playerMsg);
			
			return true;
		}
	}
	
	ContentDatabase cDB;
	
	public static int GRID_SIZE = 4;
	boolean GRID_BASED_CRAFTING = true;
	public static boolean RESOURCE_DROPS_ON_FAIL = true;
	public static boolean GAIN_SKILL_AFTER_MAX = true;
	public static boolean AUTO_PICKUP_RESOURCES = false;
	public static int RESOURCE_GATHER_DISTANCE = 4;
	public static boolean RESOURCE_GATHER_CAN_FAIL = false;
	public static boolean DELETE_CRAFTING_RECIPE_ON_USE = true;
	public static boolean RESOURCE_COUNT_IS_LOOT_COUNT = false;
	
	public static boolean USE_RESOURCE_GROUPS = true;
	public static int RESOURCE_GROUP_SIZE = 100;
	
	public static int SKINNING_SKILL_ID = -1;
	public static String SKINNING_WEAPON_REQ = "";
	static HashMap<Integer, CraftingRecipe> recipes = new HashMap<Integer, CraftingRecipe>();
	HashMap<OID, HashMap<Integer, ResourceNode>> resourceNodes = new HashMap<OID, HashMap<Integer, ResourceNode>>();
	HashMap<OID, HashMap<String, ResourceNodeGroup>> resourceNodeGroups = new HashMap<OID, HashMap<String, ResourceNodeGroup>>();
	
	public static String TASK_CRAFTING = "crafting";
	public static String TASK_GATHERING = "gathering";
	
}
