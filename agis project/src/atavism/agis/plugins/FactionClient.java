package atavism.agis.plugins;

import atavism.msgsys.MessageType;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.messages.PropertyMessage;
import atavism.server.util.Log;

//
// client for sending/getting messages to the FactionPlugin
//
public class FactionClient {
    private FactionClient() {
    }
    
    // get a mobs reaction towards a player
    public static void getAttitude(OID oid, OID targetOid) {
    	getAttitudeMessage msg = new getAttitudeMessage(oid, targetOid);
        Engine.getAgent().sendBroadcast(msg);
    }
	
	public static class getAttitudeMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public getAttitudeMessage() {
            super();
        }
        
        public getAttitudeMessage(OID oid, OID targetOid) {
        	super(oid);
        	setProperty("target", targetOid);
        	setMsgType(MSG_TYPE_GET_REACTION);
        	Log.debug("COMBAT CLIENT: getAttitudeMessage hit 1");
        }
	}
	
	
	// get a mobs reaction towards a player
    public static void alterReputation(OID oid, int faction, int repChange,boolean replaceRep) {
    	AlterReputationMessage msg = new AlterReputationMessage(oid, faction, repChange,replaceRep);
        Engine.getAgent().sendBroadcast(msg);
    }

    // get a mobs reaction towards a player
    public static void alterReputation(OID oid, int faction, int repChange) {
        AlterReputationMessage msg = new AlterReputationMessage(oid, faction, repChange,false);
        Engine.getAgent().sendBroadcast(msg);
    }
	
	public static class AlterReputationMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public AlterReputationMessage() {
            super();
        }
        
        public AlterReputationMessage(OID oid, int faction, int repChange) {
        	super(oid);
        	setFaction(faction);
        	setRepChange(repChange);
        	setMsgType(MSG_TYPE_ALTER_REPUTATION);
        	Log.debug("FACTION CLIENT: AlterReputationMessage hit 1");
        }

        public AlterReputationMessage(OID oid, int faction, int repChange, boolean replaceRep) {
            super(oid);
            setFaction(faction);
            setRepChange(repChange);
            setReplaceRep(replaceRep);
            setMsgType(MSG_TYPE_ALTER_REPUTATION);
            Log.debug("FACTION CLIENT: AlterReputationMessage hit 1");
        }
        
        public void setFaction(int faction) {
        	this.faction = faction;
        }
        public int getFaction() {
        	return faction;
        }
        int faction;
        
        public void setRepChange(int repChange) {
        	this.repChange = repChange;
        }
        public int getRepChange() {
        	return repChange;
        }
        int repChange;

        public void setReplaceRep(boolean replaceRep) {
            this.replaceRep = replaceRep;
        }
        public boolean getReplaceRep() {
            return replaceRep;
        }
        boolean replaceRep;
	}


	
	public static final MessageType MSG_TYPE_GET_REACTION = MessageType.intern("faction.GET_REACTION");
	public static final MessageType MSG_TYPE_ALTER_REPUTATION = MessageType.intern("faction.ALTER_REPUTATION");
	public static final MessageType MSG_TYPE_UPDATE_PVP_STATE = MessageType.intern("faction.UPDATE_PVP_STATE");
}