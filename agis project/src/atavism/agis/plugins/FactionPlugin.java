package atavism.agis.plugins;

import atavism.agis.core.Agis;
import atavism.agis.database.ContentDatabase;
import atavism.agis.objects.Faction;
import atavism.agis.objects.PlayerFactionData;
import atavism.agis.util.EventMessageHelper;
import atavism.msgsys.FilterUpdate;
import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageTypeFilter;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Hook;
import atavism.server.engine.OID;
import atavism.server.messages.*;
import atavism.server.objects.ObjectTracker;
import atavism.server.objects.ObjectTypes;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedPropertyMessage;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Handles faction and attitude related messages, calculates the interaction
 * state between the objects involved and sends out the messages to alert
 * other parts of the server of the interaction state.
 * @author Andrew
 *
 */
public class FactionPlugin extends EnginePlugin {

	public FactionPlugin() {
        super("Faction");
        setPluginType("Faction");
    }
	
	public String getName()
	{
		return FACTION_PLUGIN_NAME;
	}
	
    /*
     * Properties
     */
	private static final Logger log = new Logger("Faction");
	public static String FACTION_PLUGIN_NAME = "FactionPlugin";

    /*
     * Events
     */ 
    
    public void onActivate() {
        //super.onActivate();

        // register message hooks
    	getHookManager().addHook(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS, 
                new AttitudeGetHook());
    	getHookManager().addHook(WorldManagerClient.MSG_TYPE_SPAWNED,
				 new SpawnedHook());
    	getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED,
				 new DespawnedHook());
    	getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT,
 	           new LogoutHook());
    	getHookManager().addHook(PropertyMessage.MSG_TYPE_PROPERTY,
  	           new PropertyHook());
    	getHookManager().addHook(FactionClient.MSG_TYPE_UPDATE_PVP_STATE,
                new SetPvPHook());
    	getHookManager().addHook(FactionClient.MSG_TYPE_ALTER_REPUTATION,
                new AlterReputationHook());
		//getHookManager().addHook(FactionClient.MSG_TYPE_GET_REACTION,
		//		new GetReputationHook());
    	getHookManager().addHook(WorldManagerClient.MSG_TYPE_PERCEPTION_INFO,
                new PerceptionHook());

        // setup message filters
        MessageTypeFilter filter = new MessageTypeFilter();
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        filter.addType(CombatClient.MSG_TYPE_FACTION_UPDATE);
        filter.addType(WorldManagerClient.MSG_TYPE_SPAWNED);
        filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter.addType(FactionClient.MSG_TYPE_UPDATE_PVP_STATE);
        filter.addType(FactionClient.MSG_TYPE_ALTER_REPUTATION);
		filter.addType(FactionClient.MSG_TYPE_GET_REACTION);
        Engine.getAgent().createSubscription(filter, this);
        
        //setup responder message filters
        /*MessageTypeFilter responderFilter = new MessageTypeFilter();
        responderFilter.addType(LogoutMessage.MSG_TYPE_LOGOUT);
        //responderFilter.addType(LoginMessage.MSG_TYPE_LOGIN);
        Engine.getAgent().createSubscription(responderFilter, this, MessageAgent.RESPONDER);*/
        
        perceptionFilter = new PerceptionFilter();
        perceptionFilter.addType(WorldManagerClient.MSG_TYPE_PERCEPTION_INFO);
        perceptionFilter.setMatchAllSubjects(true);
        //List<ObjectType> subjectTypes = new ArrayList<ObjectType>(1);
        //subjectTypes.add(ObjectTypes.player);
        //perceptionFilter.setSubjectObjectTypes(subjectTypes);
        PerceptionTrigger perceptionTrigger = new PerceptionTrigger();
        perceptionSubId = Engine.getAgent().createSubscription(
            perceptionFilter, this, MessageAgent.NO_FLAGS, perceptionTrigger);
        
        
        // Load settings
     	ContentDatabase cDB = new ContentDatabase(false);
     	String aggroRadius = cDB.loadGameSetting("AGGRO_RADIUS");
     	if (aggroRadius != null)
     		AGGRO_RADIUS = Integer.parseInt(aggroRadius);
    }
    
    class LogoutHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LogoutMessage message = (LogoutMessage) msg;
            //OID playerOid = message.getSubject();
            
            // Find all of the objects the player had in range then remove reactionMappings and target settings
            return true;
        }
    }
    
    class SpawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.SpawnedMessage spawnedMsg = (WorldManagerClient.SpawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            if (WorldManagerClient.getObjectInfo(objOid).objType == ObjectTypes.player) {
            	// Create a tracker node
            	Log.debug("SPAWNED: creating tracker for player: " + objOid);
            	if (perceptionFilter.addTarget(objOid)) {
                    FilterUpdate filterUpdate = new FilterUpdate(1);
                    filterUpdate.addFieldValue(PerceptionFilter.FIELD_TARGETS, objOid);
                    Engine.getAgent().applyFilterUpdate(perceptionSubId, filterUpdate);
                }
            }
            
            return true;
    	}
    }
    
    class DespawnedHook implements Hook	 {
		public boolean processMessage(Message msg, int flags) {
			WorldManagerClient.DespawnedMessage despawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
			OID objOid = despawnedMsg.getSubject();
			if (perceptionFilter.hasTarget(objOid)) {
	            perceptionFilter.removeTarget(objOid);
	            FilterUpdate filterUpdate = new FilterUpdate(1);
	            filterUpdate.removeFieldValue(PerceptionFilter.FIELD_TARGETS, objOid);
	            Engine.getAgent().applyFilterUpdate(perceptionSubId,
	                filterUpdate);
	        }
			removeObjectInRange(objOid, despawnedMsg.getInstanceOid());
			return true;
		}
	}
    
    /**
     * PerceptionHook processes PerceptionMessages, which incorporate lists
     * of perceived objects gained and lost by the target object.
     */
    class PerceptionHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            PerceptionMessage perceptionMessage = (PerceptionMessage)msg;
            OID perceiverOid = perceptionMessage.getTarget();
            List<PerceptionMessage.ObjectNote> gain = perceptionMessage.getGainObjects();
            List<PerceptionMessage.ObjectNote> lost = perceptionMessage.getLostObjects();
            if (Log.loggingDebug)
                Log.debug("FactionPlugin.PerceptionHook.processMessage: perceiverOid " + perceiverOid + " " +
                        ((gain==null)?0:gain.size()) + " gain and " +
                        ((lost==null)?0:lost.size()) + " lost");

            if (gain != null) {
                for (PerceptionMessage.ObjectNote note : gain)
                    processNote(perceiverOid, note, true);
            }
            if (lost != null) {
                for (PerceptionMessage.ObjectNote note : lost)
                    processNote(perceiverOid, note, false);
            }
            return true;
        }
        
        protected void processNote(OID perceiverOid, PerceptionMessage.ObjectNote note, boolean add) {
            OID perceivedOid = note.getSubject();
            if (add) {
            	Log.debug("FACTION: calculating states between perceived nodes: " + perceiverOid + " - " + perceivedOid);
            	calculateInteractionState(perceiverOid, perceivedOid);
            	calculateInteractionState(perceivedOid, perceiverOid);
            	addObjectInRange(perceiverOid, perceivedOid);
            } else {
            	synchronized (objectsInRange) {
		    		if (objectsInRange.containsKey(perceiverOid)) {
		    			objectsInRange.get(perceiverOid).remove(perceivedOid);
		    		}
		    		if (objectsInRange.containsKey(perceivedOid)) {
		    			objectsInRange.get(perceivedOid).remove(perceiverOid);
		    		}
				}
            }
            //}
        }
    }
    
    class SetPvPHook implements Hook {
		public boolean processMessage(atavism.msgsys.Message msg, int flags) {
			ExtensionMessage cimsg = (ExtensionMessage)msg;
			OID playerOid = cimsg.getSubject();
			Log.debug("PVP: got getBlueprintMessage");
			
			boolean pvpState = (Boolean) cimsg.getProperty("pvpState");
			if (pvpState) {
				EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "temporaryFaction", "pvp_" + playerOid.toString());
			} else {
				EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "temporaryFaction", "");

			}
			
			return true;
		}
    }
    
    /**
     * Adds an object in range to the objects in range Map.
     * @param subject
     * @param target
     */
    private void addObjectInRange(OID subject, OID target) {
    	synchronized (objectsInRange) {
    		if (objectsInRange.containsKey(subject)) {
    			if (!objectsInRange.get(subject).contains(target))
    				objectsInRange.get(subject).add(target);
    		} else {
    			ArrayList<OID> targets = new ArrayList<OID>();
    			targets.add(target);
    			objectsInRange.put(subject, targets);
    		}
    	}
    }
    
    /**
     * Removes an object from objectsInRange map.
     * @param subject
     * @param instanceOID
     */
    private void removeObjectInRange(OID subject, OID instanceOID) {
    	synchronized (objectsInRange) {
    		if (objectsInRange.containsKey(subject)) {
    			for (OID target : objectsInRange.get(subject)) {
    				if (objectsInRange.containsKey(target)) {
    					objectsInRange.get(target).remove(subject);
    				}
    			}
    			objectsInRange.remove(subject);
    		}
    	}
    }
    
    class PropertyHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			PropertyMessage propMsg = (PropertyMessage) msg;
			OID objOid = propMsg.getSubject();
			boolean factionPresent=false;
			try {
				Integer faction = (Integer) propMsg.getProperty("faction");
				if (faction!=null) factionPresent=true;
			} catch (java.lang.ClassCastException e){
				log.debug(e.getMessage());
				String faction = (String) propMsg.getProperty("faction");
				if (faction!=null) factionPresent=true;
			}
			String temporaryFaction = (String)propMsg.getProperty("temporaryFaction");
			if (factionPresent || temporaryFaction != null) {
				Log.debug("PROPERTY: got faction property update for subject: " + objOid + 
						" who has objectsInRange: " + objectsInRange.get(objOid));
				// There has been a faction change, we need to re-check the interaction state
				// between the subject and all objects in their range.
				if (objectsInRange.containsKey(objOid)) {
					ArrayList<OID> objectsToRecalculate = new ArrayList<OID>();
					objectsToRecalculate.addAll(objectsInRange.get(objOid));
	    			for (OID target : objectsToRecalculate) {
	    				calculateInteractionState(objOid, target);
	    				calculateInteractionState(target, objOid);
	    			}
				}
				return true;
            }
			Integer domeID = (Integer)propMsg.getProperty("domeID");
			if (domeID != null) {
				Log.debug("PROPERTY: got domeID property update for subject: " + objOid + 
						" who has objectsInRange: " + objectsInRange.get(objOid));
				// There has been a faction change, we need to re-check the interaction state
				// between the subject and all objects in their range.
				if (objectsInRange.containsKey(objOid)) {
					ArrayList<OID> objectsToRecalculate = new ArrayList<OID>();
					objectsToRecalculate.addAll(objectsInRange.get(objOid));
	    			for (OID target : objectsToRecalculate) {
	    				calculateInteractionState(objOid, target);
	    				calculateInteractionState(target, objOid);
	    			}
				}
				return true;
            }
            return true;
        }
    }
    
    class AttitudeGetHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage)msg;
    	    OID subjectOid = nMsg.getSubject();
    	    OID targetOid = nMsg.getTarget();
			Log.debug("FACTION: get Attitude caught: " + nMsg);
			if (nMsg.getInRadius()) {
				calculateInteractionState(subjectOid, targetOid);
				calculateInteractionState(targetOid, subjectOid);
			} else {
				Log.debug("FACTION: target: " + targetOid + " is no longer in radius of " + subjectOid);
				synchronized (objectsInRange) {
		    		if (objectsInRange.containsKey(subjectOid)) {
		    			objectsInRange.get(subjectOid).remove(targetOid);
		    		}
		    		if (objectsInRange.containsKey(targetOid)) {
		    			objectsInRange.get(targetOid).remove(subjectOid);
		    		}
				}
			}
			Log.debug("FACTION: get Attitude completed");
    	    return true;
    	}
    }
    
    class AlterReputationHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		FactionClient.AlterReputationMessage nMsg = (FactionClient.AlterReputationMessage)msg;
    	    OID subjectOid = nMsg.getSubject();
    	    int faction = nMsg.getFaction();
    	    int repChange = nMsg.getRepChange();
			Log.debug("FACTION: AlterReputation caught: " + nMsg);
			// Get the Player Faction Data for this faction
	    	HashMap<Integer, PlayerFactionData> pfdMap = (HashMap) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "factionData");
			int subjectFaction = (Integer) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "faction");
			if (!pfdMap.containsKey(faction)) {
    			// Player has not yet met this faction
    			Log.debug("FACTION: player " + subjectOid + " has not met faction " + faction);
    			Faction newFaction = Agis.FactionManager.get(faction);

    			pfdMap.put(faction, Faction.addFactionToPlayer(subjectOid, newFaction, subjectFaction));

    		}
	    	
    		PlayerFactionData pfd = pfdMap.get(faction);
			boolean replaceRep = nMsg.getReplaceRep();
			if (replaceRep) pfd.setReputation(repChange);
    		else pfd.updateReputation(repChange);

    		pfdMap.put(faction, pfd);
			EnginePlugin.setObjectPropertyNoResponse(subjectOid, WorldManagerClient.NAMESPACE, "factionData", pfdMap);
			
			EventMessageHelper.SendGeneralEvent(subjectOid, EventMessageHelper.REPUTATION_CHANGED, faction, "" + repChange);
			
			Log.debug("FACTION: get Attitude completed");
    	    return true;
    	}
    }

/*
	class GetReputationHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			FactionClient.AlterReputationMessage nMsg = (FactionClient.AlterReputationMessage)msg;
			OID subjectOid = nMsg.getSubject();
			int faction = nMsg.getFaction();
			int repChange = nMsg.getRepChange();
			Log.debug("FACTION: AlterReputation caught: " + nMsg);
			// Get the Player Faction Data for this faction
			HashMap<Integer, PlayerFactionData> pfdMap = (HashMap) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "factionData");
			if (!pfdMap.containsKey(faction)) {
				// Player has not yet met this faction
				Log.debug("FACTION: player " + subjectOid + " has not met faction " + faction);
				Faction newFaction = Agis.FactionManager.get(faction);
				int subjectFaction = (Integer) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "faction");
				pfdMap.put(faction, Faction.addFactionToPlayer(subjectOid, newFaction, subjectFaction));
			}

			PlayerFactionData pfd = pfdMap.get(faction);
			pfd.updateReputation(repChange);
			pfdMap.put(faction, pfd);
			EnginePlugin.setObjectPropertyNoResponse(subjectOid, WorldManagerClient.NAMESPACE, "factionData", pfdMap);

			EventMessageHelper.SendGeneralEvent(subjectOid, EventMessageHelper.REPUTATION_CHANGED, faction, "" + repChange);

			Log.debug("FACTION: get Attitude completed");
			return true;
		}
	}*/



	/**
     * Calculates the interaction state between two objects. Both how the target will react to the subject and
     * what the subject can do to the target are calculated with the results being sent out for other
     * areas of the server to use.
     * 
     * Smoo Online uses the following flow to determine interaction states:
     * 1: Temporary factions - if both subject and target have temp factions then if they are the same, they are friendly,
     * if different - hated
     * 2: Group ID - if both users have a group ID and they are the same, they are instantly friendly
     * 3: Dome ID - if both users have a dome ID then they are hated
     * 4: Else - set to null
     */
    private void calculateInteractionState(OID subjectOid, OID targetOid) {
		boolean subjectIsPlayer = WorldManagerClient.getObjectInfo(subjectOid).objType.equals(ObjectTypes.player);
		//boolean targetIsPlayer = target.getType().equals(ObjectTypes.player);
		int subjectFaction = -1;
		int targetFaction = -1;
		try {
			subjectFaction = (Integer) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "faction");
			targetFaction = (Integer) EnginePlugin.getObjectProperty(targetOid, WorldManagerClient.NAMESPACE, "faction");
		} catch (NullPointerException e) {
			Log.debug("FACTION: subject or target faction was null");
			return;
		}
		String subjectTempFaction = (String) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "temporaryFaction");
    	String targetTempFaction = (String) EnginePlugin.getObjectProperty(targetOid, WorldManagerClient.NAMESPACE, "temporaryFaction");
    	// First, how will the target react to the subject - Friendly: 1, neutral: 0, 
		// or aggressive: -1 (likely to attack if the subject gets close)
    	int reaction = Neutral;
		// Check for temporary factions
    	if (subjectTempFaction != null && !subjectTempFaction.equals("") 
    			&& targetTempFaction != null && !targetTempFaction.equals("")) {
    		if (subjectTempFaction.equals(targetTempFaction)) {
    			reaction = Friendly;
    		} else {
    			reaction = Hated;
    		}
    	} else {
    		reaction = determineFactionStanding(subjectOid, targetOid, subjectFaction, targetFaction);
    		//reaction = determineInteractionState(subjectOid, targetOid);
    	}
    	// Send reaction message to the subject so they know what to expect from the target
    	if (subjectIsPlayer)
    		sendTargetReaction(subjectOid, targetOid, reaction);
    	
		// Now what can the subject do to the target.
		int standing = -1;
		boolean needsFactionCheck = true;
		boolean canBeHealed = true;
		// If the target is in a duel/arena etc. then we can't heal them unless we are in the same temp faction
		if (targetTempFaction != null && !targetTempFaction.equals("")) {
			if (subjectTempFaction != null && !subjectTempFaction.equals("")) {
				if (targetTempFaction.equals(subjectTempFaction)) {
					standing = Friendly;
					needsFactionCheck = false;
				} else {
					standing = Hated;
					needsFactionCheck = false;
				}
			}
			canBeHealed = false;
		}
		if (needsFactionCheck) {
			standing = determineFactionStanding(targetOid, subjectOid, targetFaction, subjectFaction);
			//standing = determineInteractionState(subjectOid, targetOid);
		}
		int targetType = Neither;
		
		if (!canBeHealed && standing == Friendly)
			targetType = Neither;
		else if (standing > Neutral)
			targetType = Healable; // Healable
		else /*if (standing != Neutral)*/
			targetType = Attackable; // Attackable 
		// Send target type message
		sendTargetUpdate(subjectOid, targetOid, targetType, standing, subjectIsPlayer);

		// Finally add the pairing to objects in range
		addObjectInRange(subjectOid, targetOid);
    }
    
    /**
     * Sends a targeted property message to the matching client letting the user know how
     * a target is likely to react to them.
     * @param subjectOid
     * @param targetOid
     * @param reaction
     */
    private void sendTargetReaction(OID subjectOid, OID targetOid, int reaction) {
    	TargetedPropertyMessage propMsg = new TargetedPropertyMessage(subjectOid, targetOid);
        propMsg.setProperty("reaction", reaction);
        Engine.getAgent().sendBroadcast(propMsg);
        Log.debug("ATTITUDE: setting reaction for target: " + targetOid + " against: " + subjectOid + " to " + reaction);
    }
    
    /**
     * Collects details about the target and sends out the target update message.
     * @param subjectOid
     * @param targetOid
     * @param targetType
     */
    private void sendTargetUpdate(OID subjectOid, OID targetOid, int targetType, int standing,
    		boolean subjectIsPlayer) {
    	CombatClient.setTargetType(subjectOid, targetOid, targetType, "");
    	TargetedPropertyMessage propMsg = new TargetedPropertyMessage(subjectOid, targetOid);
    	propMsg.setProperty("targetType", targetType);
        Engine.getAgent().sendBroadcast(propMsg);
        Log.debug("ATTITUDE: sent target type update for subject: " + subjectOid + " who is player? " + 
        		subjectIsPlayer + " and has standing: " + standing + " towards target: " + targetOid);
        if (!subjectIsPlayer) {
        	OID instanceOid = WorldManagerClient.getObjectInfo(subjectOid).instanceOid;
        	if (standing < Neutral) {
        		MobManagerPlugin.getTracker(instanceOid).addAggroRadius(subjectOid, targetOid, AGGRO_RADIUS);
        		//Log.error("ATTITUDE: setting mob " + subjectOid + " to aggro against player: " + targetOid);
        	} else {
        		MobManagerPlugin.getTracker(instanceOid).removeAggroRadius(subjectOid, targetOid);
        	}
        }
    }
    
    /**
     * If grouped, then friendly. If both have Dome IDs, then they are hated. Else, neutral
     * @param subjectOid
     * @param targetOid
     * @return
     */
    private int determineInteractionState(OID subjectOid, OID targetOid) {
    	//TODO: group ID detection
    	Integer domeID = (Integer) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "domeID");
    	Integer targetDomeID = (Integer) EnginePlugin.getObjectProperty(targetOid, WorldManagerClient.NAMESPACE, "domeID");
    	if (domeID != null && domeID != -1 && targetDomeID != null && targetDomeID != -1) {
    		boolean subjectIsPlayer = WorldManagerClient.getObjectInfo(subjectOid).objType.equals(ObjectTypes.player);
    		boolean targetIsPlayer = WorldManagerClient.getObjectInfo(targetOid).objType.equals(ObjectTypes.player);
    		if (!subjectIsPlayer && !targetIsPlayer) {
    			// both are mobs, so they are friendly
    			return Friendly;
    		}
    		return Hated;
    	} else {
    		return Neutral;
    	}
    }
    
    /**
     * Returns the faction standing between the targets faction and the subject.
     * @param subjectOid
     * @param targetOid
     * @return
     */


    public  int determineFactionStanding(OID subjectOid, OID targetOid, int subjectFaction, int targetFaction) {
    	// Firstly - if the factions are the same, return friendly
    	// TODO: remove the < 1 check
    	if (((subjectFaction!=1) && subjectFaction == targetFaction) || subjectFaction < 1 || targetFaction < 1)
    		return Friendly;
    	boolean subjectIsPlayer = WorldManagerClient.getObjectInfo(subjectOid).objType.equals(ObjectTypes.player);
		boolean targetIsPlayer = WorldManagerClient.getObjectInfo(targetOid).objType.equals(ObjectTypes.player);
    	Log.debug("FACTI: subjectIsPlayer: " + subjectIsPlayer + ", subjectFaction = " + subjectFaction + ", target = " + targetFaction);
    	if (subjectIsPlayer) {
    		Faction newFaction = Agis.FactionManager.get(targetFaction);
    		if (!newFaction.getIsPublic()) {
    			return calculateStanding(newFaction.getDefaultReputation(subjectFaction));
    		}
    		// We get the players faction data
    		HashMap<Integer, PlayerFactionData> pfdMap = (HashMap) EnginePlugin.getObjectProperty(subjectOid, WorldManagerClient.NAMESPACE, "factionData");
    		if (!pfdMap.containsKey(targetFaction)) {
    			// Player has not yet met this faction
    			Log.debug("FACTION: player " + subjectOid + " has not met faction " + targetFaction);
    			pfdMap.put(targetFaction, Faction.addFactionToPlayer(subjectOid, newFaction, subjectFaction));
    		}
    		Log.debug("FACTION: getting target faction: " + targetFaction + " from players FactionDataMap");
    		PlayerFactionData pfd = pfdMap.get(targetFaction);
    		Log.debug("FACTION: got faction from players FactionDataMap");
    		int reputation = pfd.getReputation();
    		Log.debug("FACTION: players reputation with faction in question: " + reputation);
			return calculateStanding(reputation);
    	} else if (targetIsPlayer) {
    		Faction newFaction = Agis.FactionManager.get(subjectFaction);
    		if (!newFaction.getIsPublic()) {
    			return calculateStanding(newFaction.getDefaultReputation(targetFaction));
    		}
    		// We get the players faction data
    		HashMap<Integer, PlayerFactionData> pfdMap = (HashMap) EnginePlugin.getObjectProperty(targetOid, WorldManagerClient.NAMESPACE, "factionData");
    		if (!pfdMap.containsKey(subjectFaction)) {
    			// Player has not yet met this faction
    			Log.debug("FACTION: player " + subjectOid + " has not met faction " + subjectFaction);
    			pfdMap.put(subjectFaction, Faction.addFactionToPlayer(targetOid, newFaction, targetFaction));
    		}
    		Log.debug("FACTION: getting subject faction: " + subjectFaction + " from players FactionDataMap");
    		PlayerFactionData pfd = pfdMap.get(subjectFaction);
    		Log.debug("FACTION: got faction from players FactionDataMap");
    		int reputation = pfd.getReputation();
    		Log.debug("FACTION: players reputation with faction in question: " + reputation);
			return calculateStanding(reputation);
    	} else {
    		Faction newFaction = Agis.FactionManager.get(targetFaction);
    		int reputation = newFaction.getDefaultReputation(subjectFaction);
    		return calculateStanding(reputation);
    	}
    }
    
    public static int calculateStanding(int reputation) {
    	if (reputation < DislikedRep) {
    		return Hated;
    	}else if (reputation < NeutralRep) {
    		return Disliked;
    	} else if (reputation < FriendlyRep) {
    		return Neutral;
    	} else if (reputation < HonouredRep) {
    		return Friendly;
    	} else if (reputation < ExaltedRep) {
    		return Honoured;
    	} else {
    		return Exalted;
    	}
    }
    
    protected static HashMap<OID, ArrayList<OID>> objectsInRange = new HashMap<OID, ArrayList<OID>>();

    public static final int HatedRep = -3000;
    public static final int DislikedRep = -1500;
    public static final int NeutralRep = -500;
    public static final int FriendlyRep = 500;
    public static final int HonouredRep = 1500;
    public static final int ExaltedRep = 3000;
    public static final int Hated = -2;
    public static final int Disliked = -1;
    public static final int Neutral = 0;
    public static final int Friendly = 1;
    public static final int Honoured = 2;
    public static final int Exalted = 3;
    
    public static final int Attackable = -1;
    public static final int Healable = 1;
    public static final int Neither = 0;
    
    public static int AGGRO_RADIUS = 15;
    
    protected PerceptionFilter perceptionFilter;
    protected long perceptionSubId;
    
}