package atavism.agis.plugins;

import atavism.agis.database.CombatDatabase;
import atavism.agis.database.ContentDatabase;
import atavism.agis.objects.AgisGroup;
import atavism.agis.objects.AgisGroupMember;
import atavism.agis.objects.CombatInfo;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.ResponseMessage;
import atavism.server.engine.*;
import atavism.server.messages.LoginMessage;
import atavism.server.messages.LogoutMessage;
import atavism.server.messages.PropertyMessage;
import atavism.server.objects.InstanceRestorePoint;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.VoiceClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedComMessage;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.locks.Lock;

public class GroupPlugin extends EnginePlugin {

    /*
     * Properties
     */
    
    protected static final Logger _log = new Logger("GroupPlugin");
    protected static List<String> _registeredStats = new ArrayList<String>();
    protected static Map<OID, AgisGroup> _currentGroups = new Hashtable<OID, AgisGroup>();
    protected static int _maxGroupSize = 4; // Default max group size to 4

    /*
     * Constructors
     */ 
    public GroupPlugin() {
        super("Group");
        setPluginType("Group");
    }

    public String GetName() {
        return "GroupPlugin";
    }

    /*
     * Events
     */ 
    
    public void onActivate() {
        super.onActivate();

        // register message hooks
        RegisterHooks();

        // setup message filters
        MessageTypeFilter filter = new MessageTypeFilter();
        filter.addType(PropertyMessage.MSG_TYPE_PROPERTY);
        filter.addType(GroupClient.MSG_TYPE_GROUP_INVITE);
        filter.addType(GroupClient.MSG_TYPE_GROUP_LEAVE);
        filter.addType(GroupClient.MSG_TYPE_GROUP_REMOVE_MEMBER);
        filter.addType(GroupClient.MSG_TYPE_GROUP_CHAT);
        filter.addType(GroupClient.MSG_TYPE_GROUP_INVITE_RESPONSE);
        filter.addType(GroupClient.MSG_TYPE_GROUP_SET_ALLOWED_SPEAKER);
        filter.addType(GroupClient.MSG_TYPE_GROUP_MUTE_VOICE_CHAT);
        filter.addType(GroupClient.MSG_TYPE_GROUP_VOICE_CHAT_STATUS);
        filter.addType(VoiceClient.MSG_TYPE_VOICE_MEMBER_ADDED);
        filter.addType(GroupClient.MSG_TYPE_GROUP_INVITE_BY_NAME);
        filter.addType(GroupClient.MSG_TYPE_CREATE_GROUP);
        filter.addType(GroupClient.MSG_TYPE_GROUP_PROMOTE_LEADER);
        Engine.getAgent().createSubscription(filter, this);
        
        //setup responder message filters
        MessageTypeFilter responderFilter = new MessageTypeFilter();
        //responderFilter.addType(LoginMessage.MSG_TYPE_LOGIN);
        responderFilter.addType(LogoutMessage.MSG_TYPE_LOGOUT);
        responderFilter.addType(GroupClient.MSG_TYPE_REQUEST_GROUP_INFO); 
        Engine.getAgent().createSubscription(responderFilter, this, MessageAgent.RESPONDER);

        if (Log.loggingDebug)
            _log.debug("GroupPlugin activated.");
        
        // Load settings
        ContentDatabase ctDB = new ContentDatabase(false);
        String groupSize = ctDB.loadGameSetting("GROUP_MAX_SIZE");
        if (groupSize != null)
        	_maxGroupSize = Integer.parseInt(groupSize);
        
        CombatDatabase cDB = new CombatDatabase(false);
        LinkedList<String> sharedStats = cDB.LoadGroupSharedStats();
        for (String statname : sharedStats) {
        	RegisterStat(statname);
        }
        
        // Hard coded level stat
        RegisterStat(CombatPlugin.LEVEL_STAT);
    }

    /*
     * Methods
     */ 
    
    public void RegisterHooks() {
        getHookManager().addHook(PropertyMessage.MSG_TYPE_PROPERTY,
                                 new PropertyHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_INVITE,
                                 new GroupInviteHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_INVITE_RESPONSE,
                                 new GroupInviteResponseHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_REMOVE_MEMBER,
                                 new GroupRemoveMemberHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_CHAT,
                                 new GroupChatHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_REQUEST_GROUP_INFO,
                                 new RequestGroupInfoHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_LEAVE,
                				new GroupLeaveHook());
        //getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
        //        new LoginHook());
        getHookManager().addHook(LogoutMessage.MSG_TYPE_LOGOUT,
                new LogOutHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_SET_ALLOWED_SPEAKER,
                new SetAllowedSpeakerHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_MUTE_VOICE_CHAT,
                new MuteGroupHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_VOICE_CHAT_STATUS,
                new VoiceStatusHook());
        getHookManager().addHook(VoiceClient.MSG_TYPE_VOICE_MEMBER_ADDED,
                new VoiceMemberAddedHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_INVITE_BY_NAME,
                new GroupInviteByNameHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_CREATE_GROUP,
        		new CreateGroupHook());
        getHookManager().addHook(GroupClient.MSG_TYPE_GROUP_PROMOTE_LEADER,
        		new PromoteToLeaderHook());
    }

    public static void RegisterStat(String stat) {
        _registeredStats.add(stat);
    }

    protected AgisGroup GetGroup(OID groupOid) {
        return _currentGroups.get(groupOid);
    }

    public static List<String> GetRegisteredStats() {
        return _registeredStats;
    }

    /**
     * Gets information about the group and its members and sends it to each
     * group member
     */
    protected void SendGroupUpdate(AgisGroup group) {
        MessageAgent agent = Engine.getAgent();
        TargetedExtensionMessage groupUpdateMsg = new TargetedExtensionMessage();
        groupUpdateMsg.setExtensionType(GroupClient.EXTMSG_GROUP_UPDATE);
        groupUpdateMsg.setProperty("maxGroupSize", String.valueOf(_maxGroupSize));
        // groupOid is the unique key for the group and the voice group
        groupUpdateMsg.setProperty("groupOid", group.getOid());
        // Set each group members info
        int counter = 1; // Counter is used to supply the client with an ordered key for accessing group members
        Hashtable<OID, AgisGroupMember> groupMembers = group.GetGroupMembers();
        // We must make sure keys are ordered properly by order the player joined the group
        Set<OID> groupMemberKeys = new TreeSet<OID>(group.GetGroupMembers().keySet());

        for (OID groupMemberKey : groupMemberKeys) {
            HashMap<String, Serializable> groupMemberInfo = new HashMap<String, Serializable>();
            AgisGroupMember groupMember = groupMembers.get(groupMemberKey);
            groupMemberInfo.put("memberOid", groupMember.GetGroupMemberOid());
            groupMemberInfo.put("name", groupMember.GetGroupMemberName());
            groupMemberInfo.put("voiceEnabled", groupMember.GetVoiceEnabled());
            groupMemberInfo.put("allowedSpeaker", groupMember.GetAllowedSpeaker());
            groupMemberInfo.put("groupMuted", group.GetGroupMuted());
            groupMemberInfo.put("statCount", _registeredStats.size());
            for (int i = 0; i < _registeredStats.size(); i++) { // Add any registered stats to info
            	String stat = _registeredStats.get(i);
            	groupMemberInfo.put("stat" + i, stat);
            	groupMemberInfo.put("stat" + i + "Value", groupMember.GetGroupMemberStat(stat));
                //groupMemberInfo.put(stat, groupMember.GetGroupMemberStat(stat));
            }
            // Store counter as our key so we can sort the list on the client
            groupUpdateMsg.setProperty(String.valueOf(counter), groupMemberInfo);
            //if group does not have a leader set, then the first group member in order is now the leader
            if((group.GetGroupLeaderOid() == null) && (counter == 1))
                group.SetGroupLeaderOid(groupMember.GetGroupMemberOid());
            counter += 1;
        }
        
        // do this after looping through group members incase the group leader was changed
        groupUpdateMsg.setProperty("groupLeaderOid", group.GetGroupLeaderOid());

        // Send message to each group member
        for (AgisGroupMember groupMember : group.GetGroupMembers().values()) {
            groupUpdateMsg.setTarget(groupMember.GetGroupMemberOid());
            agent.sendBroadcast(groupUpdateMsg);
        }
    }

    protected void RemoveGroupMember(CombatInfo info, boolean kicked, OID kicker, boolean removeFromInstance) {
    	Log.debug("GROUP: removing group member: " + info.getOwnerOid() + ". Remove from instance? " + removeFromInstance);
        AgisGroup group = GetGroup(info.getGroupOid());
        //Check to ensure group is valid
        if (group == null){
            if (Log.loggingDebug) {
                _log.error("GroupPlugin.RemoveGroupMember : group is null");
            }
            info.setGroupOid(null);
            info.setGroupMemberOid(null);
            return;
        }
        
        // If kicked, verify the kicker is a group leader
        if (kicked) {
        	if (!group.GetGroupLeaderOid().equals(kicker))
        		return;
        }
        
        // Dis-associate group with member and member with group
        group.RemoveGroupMember(info);

        //If the group leader left, trigger system to set new group leader
        if(info.getOwnerOid().equals(group.GetGroupLeaderOid())){
            group.SetGroupLeaderOid(null); //Clear group leader Oid so it is reset in SendGroupUpdate method
        }
        
        // Check if user was in a group instance - if so, remove them
        if (removeFromInstance) {
        	removeMemberFromInstance(group, info.getOwnerOid());
        }

        // Send update to group
        if (group.GetNumGroupMembers() > 1) {
            SendGroupUpdate(group);            
        } else {
            // Update remaining member that they no longer are in group either
            // since one person can't be a group
            CombatInfo groupLeader = CombatPlugin.getCombatInfo(group.GetGroupLeaderOid());
            if(groupLeader != null){
            	if (removeFromInstance) {
                	removeMemberFromInstance(group, groupLeader.getOwnerOid());
                }
                group.RemoveGroupMember(groupLeader); //Removing the last group member triggers removal of voice group
                TargetedExtensionMessage groupUpdateMsg = new TargetedExtensionMessage(groupLeader.getOwnerOid());
                groupUpdateMsg.setExtensionType(GroupClient.EXTMSG_GROUP_UPDATE);
                Engine.getAgent().sendBroadcast(groupUpdateMsg);
            } else {
                _log.error("GroupPlugin.RemoveGroupMember - Group leader is null");
            }
            _currentGroups.remove(group);
            group = null;
        }

        // Send group update message to player being removed in order to clear their group info
        TargetedExtensionMessage groupUpdateMsg = new TargetedExtensionMessage(info.getOwnerOid());
        groupUpdateMsg.setExtensionType(GroupClient.EXTMSG_GROUP_UPDATE);
        Engine.getAgent().sendBroadcast(groupUpdateMsg);
    }
    
    protected void removeMemberFromInstance(AgisGroup group, OID memberOid) {
    	OID instanceOid = WorldManagerClient.getObjectInfo(memberOid).instanceOid;
    	OID groupOid = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_GROUP_OID).groupOid;
    	if (groupOid != null && groupOid.equals(group.GetGroupOid())) {
    		LinkedList restoreStack = (LinkedList) EnginePlugin.getObjectProperty(
    				memberOid, Namespace.OBJECT_MANAGER, ObjectManagerClient.TEMPL_INSTANCE_RESTORE_STACK);
    		InstanceRestorePoint restorePoint = (InstanceRestorePoint) restoreStack
                    .get(restoreStack.size() - 1);
    		BasicWorldNode node = new BasicWorldNode();
    		Log.debug("GROUP: teleporting member from instance: " + instanceOid + " to: " + restorePoint.getInstanceID());
    		instanceOid = InstanceClient.getInstanceOid(restorePoint.getInstanceID());
    		node.setInstanceOid(instanceOid);
    		node.setLoc(restorePoint.getLoc());
    		InstanceClient.objectInstanceEntry(memberOid, node, InstanceClient.InstanceEntryReqMessage.FLAG_NONE);
    	}
    }
    
    protected void PromoteMemberToLeader(CombatInfo info, OID promoter) {
        AgisGroup group = GetGroup(info.getGroupOid());
        Log.debug("GROUP: got promote message for group: " + group);
        //Check to ensure group is valid
        if (group == null){
            if (Log.loggingDebug) {
                _log.error("GroupPlugin.PromoteMemberToLeader : group is null");
            }
            info.setGroupOid(null);
            info.setGroupMemberOid(null);
            return;
        }
        
        // Verify the leader is doing the promoting
        if (!group.GetGroupLeaderOid().equals(promoter)) {
        	Log.debug("GROUP: promoter is not the group leader: ");
        	return;
        }

        group.SetGroupLeaderOid(info.getOwnerOid()); //Clear group leader Oid so it is reset in SendGroupUpdate method

        // Send update to group
        SendGroupUpdate(group);            
    }

    /**
     * Sets the maximum number of players that can be in a single group - Default is 8
     */
    public static void SetMaxGroupSize(int size) {
        _maxGroupSize = size;
    }

    /**
     * Sends update to group members about the group and its members
     */
    protected boolean UpdateGroupMemberProps(PropertyMessage propMsg) {
        CombatInfo subject = CombatPlugin.getCombatInfo(propMsg.getSubject());
        //Log.debug("GROUP: got propMessage for subject: " + propMsg.getSubject());
        if (subject == null)
            return false;
        // If the subject is grouped, continue
        if (subject.isGrouped()) {
        	//Log.debug("GROUP: got propMessage for grouped player");
            Set<String> props = propMsg.keySet();
            Map<String, Serializable> statsToUpdate = new HashMap<String, Serializable>();
            // Check our registered properties to see if any are in the
            // update list
            for (String stat : _registeredStats) {
            	//Log.debug("GROUP: checking stat: " + stat);
                if (props.contains(stat)) {
                    statsToUpdate.put(stat, propMsg.getProperty(stat));
                }
            }
            //Log.debug("GROUP: got stat count: " + statsToUpdate.size());
            // If any properties that the group cares about was updated,
            // send out a message to all the group members
            if (statsToUpdate.size() > 0) {
                AgisGroup group = GetGroup(subject.getGroupOid());
                if(group == null){
                    _log.error("GroupPlugin.UpdateGroupMemberProps - group is null");
                    subject.setGroupMemberOid(null);
                    subject.setGroupOid(null);
                    return false;
                }

                SendGroupPropertyUpdate(subject.getOwnerOid(), group, statsToUpdate);
            }
        }
        return true;
    }

    /**
     * SendGroupPropertyUpdate - Sends an ao.GROUP_PROPERTY_UPDATE message to each client in the group client
     * @param playerOid - Player whos property changed
     * @param group - Group in which the subject belongs to
     * @param statsToUpdate - Map<String, Serializable> of properties that were updated
     */
    protected void SendGroupPropertyUpdate(OID playerOid, AgisGroup group, Map<String, Serializable> statsToUpdate){
        Collection<AgisGroupMember> groupMembers = group.GetGroupMembers().values();
        for (AgisGroupMember groupEntry : groupMembers) {
            TargetedExtensionMessage updateMessage =
                new TargetedExtensionMessage(groupEntry.GetGroupMemberOid());

            updateMessage.setExtensionType(GroupClient.EXTMSG_GROUP_PROPERTY_UPDATE);
            updateMessage.setProperty("memberOid", playerOid); // member being updated
            updateMessage.setProperty("statCount", statsToUpdate.size()); 
            int statNum = 0;
            for (String stat : statsToUpdate.keySet()) {
            	updateMessage.setProperty("stat" + statNum, stat);
                updateMessage.setProperty("stat" + statNum + "Value", statsToUpdate.get(stat));
                statNum++;
            }
            Engine.getAgent().sendBroadcast(updateMessage);
        }
    }

    /**
     * Handles logic for an invite request response - either accepted or declined
     * Creates a new group if the inviter is not currently grouped
     */
    protected boolean HandleInviteResponse(ExtensionMessage inviteMsg) {
        _log.debug("GroupPlugin.HandleInviteResponse");

        OID inviterOid = (OID) inviteMsg.getProperty("groupLeaderOid");
        OID inviteeOid = inviteMsg.getSubject();
        _log.debug("GroupPlugin.HandleInviteResponse: inviterOid=" + inviterOid + ", inviteeOid=" + inviteeOid);

        CombatInfo invitee = CombatPlugin.getCombatInfo(inviteeOid);
        CombatInfo inviter = CombatPlugin.getCombatInfo(inviterOid);
                
        if (inviter == null || invitee == null) {
            _log.debug("GroupPlugin.HandleInviteResponse: null inviter/invitee, inviter=" + inviter + ", invitee=" + invitee);
            return false;
        }
        
        if (!invitee.isPendingGroupInvite()) {
        	_log.debug("GROUP: user does not have a pending invite");
        	return true;
        }
        // Clear pending group invite flag
        invitee.setPendingGroupInvite(false);
        inviter.setPendingGroupInvite(false);
        
        String response = inviteMsg.getProperty("response").toString();
        if (response.equals("accept")) {
            AgisGroup group = null;
            //Boolean voiceEnabled = (Boolean)inviteMsg.getProperty("groupVoiceEnabled");
            if (inviter.isGrouped()) { // Add invitee to group
                group = GetGroup(inviter.getGroupOid());
                if(group == null){
                    _log.error("GroupPlugin.HandleInviteResponse - group is null");
                    inviter.setGroupMemberOid(null);
                    inviter.setGroupOid(null);
                    return false;
                }
                AgisGroupMember groupMember = group.AddGroupMember(invitee);  
                //groupMember.SetVoiceEnabled(voiceEnabled);
            } else { // Create a new group
                group = new AgisGroup();
                AgisGroupMember groupLeader = group.AddGroupMember(inviter);
                groupLeader.SetVoiceEnabled(true); //TODO: Figure out how to get leader's setting
                group.SetGroupLeaderOid(inviter.getOwnerOid());
                AgisGroupMember groupMember = group.AddGroupMember(invitee);  
                //groupMember.SetVoiceEnabled(voiceEnabled);
                _currentGroups.put(group.GetGroupOid(), group); // Add to our list to track
                
                // If the group leader is already in a dungeon, change the instances group oid
                OID instanceOid = WorldManagerClient.getObjectInfo(inviterOid).instanceOid;
                OID instanceGroupOid = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_GROUP_OID).groupOid;
                if (instanceGroupOid != null && inviter.getOwnerOid().equals(instanceGroupOid)) {
                	//InstanceClient.setInstanceGroup(instanceOid, group.GetGroupOid());
                }
            }      
            // Send clients info about the group and group members
            SendGroupUpdate(group);
        } else {
            _log.debug("GroupPlugin.HandleInviteResponse: declined");
            String inviteeName = WorldManagerClient.getObjectInfo(invitee.getOwnerOid()).name;
            SendTargetedGroupMessage(inviter.getOwnerOid(), inviteeName + " has declined your group invite.");
        }
        
        _log.debug("GroupPlugin.HandleInviteResponse: done");
        return true;
    }

    /**
     * Logic to handle group specific chat
     */
    protected void HandleGroupChat(ExtensionMessage groupChatMsg) {
        String message = groupChatMsg.getProperty("message").toString();
        OID senderOid = groupChatMsg.getSubject();

        CombatInfo sender = CombatPlugin.getCombatInfo(senderOid);
        if (sender.isGrouped()) {
            String senderName =  WorldManagerClient.getObjectInfo(sender.getOwnerOid()).name;
            AgisGroup group = GetGroup(sender.getGroupOid());
            if(group == null){
                _log.error("GroupPlugin.HandleGroupChat - group is null");
                sender.setGroupMemberOid(null);
                sender.setGroupOid(null);
                return;
            }
            Collection<AgisGroupMember> groupMembers = group.GetGroupMembers().values();            
            //Send chat message to each group member
            for (AgisGroupMember groupMember : groupMembers) {
                SendTargetedGroupMessage(groupMember.GetGroupMemberOid(), "[" + senderName + "]: " + message);
            }
        } else {
            SendTargetedGroupMessage(sender.getOwnerOid(), "You are not grouped!");
        }
    }

    /**
     * Handles invite request by sending invite request message to the invitee
     */
    protected boolean HandleGroupInvite(OID inviterOid, OID inviteeOid) {
        CombatInfo inviter = CombatPlugin.getCombatInfo(inviterOid);
        CombatInfo invitee = CombatPlugin.getCombatInfo(inviteeOid);
        if (inviter == null || invitee == null) {
            return false;
        }
        if (Log.loggingDebug) {
            _log.debug("GroupPlugin.GroupInviteHook: Received group invite message inviter:"
                       + inviter.getOwnerOid()
                       + " invitee:"
                       + invitee.getOwnerOid());
        }    

        // A player should not be able to invite themselves to a group
        if(inviter.getOwnerOid().equals(invitee.getOwnerOid())){
            return true;
        }
        
        if (inviter.isPendingGroupInvite()) {
        	return false;
        }
        
        if(inviter.isGrouped()){
            AgisGroup group = GetGroup(inviter.getGroupOid());
            if(group == null){
                _log.error("GroupPlugin.HandleGroupInvite - Inviter's group is null");
                inviter.setGroupMemberOid(null);
                inviter.setGroupOid(null);
                return false;
            }
            if (group.GetGroupMembers().size() >= _maxGroupSize){
                SendTargetedGroupMessage(inviter.getOwnerOid(), "Your group is full.");
                return true;
            }
            if (!group.GetGroupLeaderOid().equals(inviterOid)){
            	// Only group leader can invite more people
                SendTargetedGroupMessage(inviter.getOwnerOid(), "Only the group leader can invite new members");
                return true;
            }
        }
        
        String inviteeName = WorldManagerClient.getObjectInfo(invitee.getOwnerOid()).name;
        //Send message to inviter
        SendTargetedGroupMessage(inviter.getOwnerOid(), "You have invited " + inviteeName + " to your group.");

        if (invitee.isGrouped()) {            
            SendTargetedGroupMessage(inviter.getOwnerOid(), inviteeName + " is already grouped.");
        } else if (invitee.isPendingGroupInvite()) {
            SendTargetedGroupMessage(inviter.getOwnerOid(), inviteeName + " is already considering a group invite.");
        } else if (!invitee.isGrouped()) {            
            // Set pending group invite flag
            invitee.setPendingGroupInvite(true);
            inviter.setPendingGroupInvite(true);

            TargetedExtensionMessage inviteRequestMsg = new TargetedExtensionMessage(invitee.getOwnerOid());
            inviteRequestMsg.setExtensionType(GroupClient.EXTMSG_GROUP_INVITE_REQUEST);
            inviteRequestMsg.setProperty("groupLeaderOid", inviter.getOwnerOid());
            String inviterName = WorldManagerClient.getObjectInfo(inviter.getOwnerOid()).name;
            inviteRequestMsg.setProperty("groupLeaderName", inviterName);

            if (Log.loggingDebug) {
                _log.debug("GroupPlugin.GroupInviteHook: Sending group invite request inviter:"
                           + inviter.getOwnerOid()
                           + " invitee:"
                           + invitee.getOwnerOid());
            }

            Engine.getAgent().sendBroadcast(inviteRequestMsg);
        }
        return true;
    }

    /**
     * HandleGroupInfoRequest handles a request for information about a group.
     * Returns the groupOid, groupleaderOid and each member's Oid in a response message.
     */
    protected GroupClient.GroupInfo HandleGroupInfoRequest(CombatInfo subject){
        GroupClient.GroupInfo groupInfo = new GroupClient.GroupInfo();
 
        HashSet<OID> memberOids = new HashSet<OID>();
        if(subject.isGrouped()){
            AgisGroup group = GetGroup(subject.getGroupOid());
            if(group == null){
                //_log.error("GroupPlugin.HandleGroupInfoRequest - group is null");
                subject.setGroupMemberOid(null);
                subject.setGroupOid(null);
                return groupInfo;
            }
            groupInfo.groupOid = group.GetGroupOid();
            groupInfo.groupLeaderOid = group.GetGroupLeaderOid();
            
            Collection<AgisGroupMember> groupMembers = group.GetGroupMembers().values();
            for(AgisGroupMember groupMember : groupMembers){
                memberOids.add(groupMember.GetGroupMemberOid());
            }
            groupInfo.memberOidSet = memberOids;
        }
        return groupInfo;
    }

    /**
     * SendTargetedGroupMessage - Handles sending messages to the group com channel
     */
    protected void SendTargetedGroupMessage(OID target, String message){
        TargetedComMessage comMessage = new TargetedComMessage();
        comMessage.setString(message);
        comMessage.setChannel(4); //Group channel
        comMessage.setTarget(target);
        Engine.getAgent().sendBroadcast(comMessage);
    }

    protected static AgisGroupMember GetGroupMember(OID subjectOid){
        Collection<AgisGroup> groups = _currentGroups.values();
        for(AgisGroup group : groups){
            AgisGroupMember subject = group.GetGroupMember(subjectOid);
            if(subject != null)
                return subject;
        }
        return null;
    }

    /**
     * HandleSetAllowedSpeaker - Used to mark the target as an allowed speaker or not of the group's voice chat.
     *                              If the target is currently an allowed speaker they will in effect become muted
     * @param targetOid - Player to mute or un-mute
     * @param setterOid - Requesting Player
     * @param groupOid - Identifier for the group the target and setter belong to
     */
    protected boolean HandleSetAllowedSpeaker(OID targetOid, OID setterOid, OID groupOid) {
        AgisGroup group = GetGroup(groupOid);
        AgisGroupMember target = group.GetGroupMember(targetOid);

        if(group == null) {
            Log.error("GroupPlugin.HandleSetAllowedSpeaker - Group is null.");
            return false;
        }

        if(target == null){
            Log.error("GroupPlugin.HandleSetAllowedSpeaker - Target is null.");
            return false;
        }

        if(target.GetVoiceEnabled()){
            Map<String, Serializable> statToUpdate = new HashMap<String, Serializable>();
            // If group is muted, then cannot change status unless the setter is the gorup leader
            if(!group.GetGroupMuted() || (setterOid.equals(group.GetGroupLeaderOid()))) {
                target.SetAllowedSpeaker(!target.GetAllowedSpeaker());  
                // Update voice server                
                int result = VoiceClient.setAllowedSpeaker(groupOid, targetOid, target.GetAllowedSpeaker());
                if(result != VoiceClient.SUCCESS)
                    Log.error("GroupPlugin.HandleSetAllowedSpeaker : Create Voice Group Response - " + VoiceClient.errorString(result));
            }
            // Send voice status to all group members
            statToUpdate.put("allowedSpeaker", target.GetAllowedSpeaker());
            SendGroupPropertyUpdate(targetOid, group, statToUpdate);

        }
        return true;
    }
    
    /**
     * HandleMuteGroup - Allows group leader to mute or un-mute the group's voice chat
     * @param setterOid
     * @param groupOid
     */
    protected boolean HandleMuteGroup(OID setterOid, OID groupOid){
        AgisGroup group = GetGroup(groupOid);
        
        if(group == null){
            Log.error("GroupPlugin.HandleMuteGroup - Group is null.");
            return false;
        }

        // Only the group leader should be able to mute the group
        if(setterOid.equals(group.GetGroupLeaderOid())){
            group.SetGroupMuted(!group.GetGroupMuted());

            Collection<AgisGroupMember> groupMembers = group.GetGroupMembers().values();

            // Mute each player in the group except for group leader
            for(AgisGroupMember groupMember : groupMembers){
                if(groupMember.GetVoiceEnabled() && !groupMember.GetGroupMemberOid().equals(group.GetGroupLeaderOid())) {
                    groupMember.SetAllowedSpeaker(!group.GetGroupMuted());
                    // Call voice server to mute player
                    VoiceClient.setAllowedSpeaker(groupOid, groupMember.GetGroupMemberOid(), !group.GetGroupMuted());
                    // Update group members about player voice status
                    Map<String, Serializable> statToUpdate = new HashMap<String, Serializable>();
                    statToUpdate.put("allowedSpeaker", !group.GetGroupMuted());
                    statToUpdate.put("groupMuted", group.GetGroupMuted());
                    SendGroupPropertyUpdate(groupMember.GetGroupMemberOid(), group, statToUpdate);
                }
            }
            GroupClient.GroupEventType eventType = GroupClient.GroupEventType.MUTED;
            if (!group.GetGroupMuted())
                eventType = GroupClient.GroupEventType.UNMUTED;
            GroupClient.SendGroupEventMessage(eventType, group, setterOid);
        }

        return true;
    }

    /**
     * HandledVoiceStatus - Logic to handle ao.VOICE_CHAT_STATUS message.
     *                          Updates group member's voiceEnabled property and
     *                          broadcasts update to the other group members
     * @param playerOid - Player being updated
     * @param groupOid - Group being referenced
     * @param voiceEnabled - Value to determine if the player's voice is enabled on their client (Voice enabled and join Party enabled)
     */
    protected boolean HandledVoiceStatus(OID playerOid, OID groupOid, Boolean voiceEnabled){
        AgisGroup group = GetGroup(groupOid);        

        if(group == null){
            Log.error("GroupPlugin.HandledVoiceStatus - Group is null.");
            return false;
        }

        AgisGroupMember player = group.GetGroupMember(playerOid);

        if(player == null){
            Log.error("GroupPlugin.HandledVoiceStatus - Player is null.");
            return false; 
        }

        player.SetVoiceEnabled(voiceEnabled);
        Map<String, Serializable> statToUpdate = new HashMap<String, Serializable>();
        statToUpdate.put("voiceEnabled", voiceEnabled);
        SendGroupPropertyUpdate(playerOid, group, statToUpdate);

        return true;
    }

    /**
     * HandleVoiceMemberAdded - Handles logic for processing the VoiceClient.MSG_TYPE_VOICE_MEMBER_ADDED
     *  message type. Update any group or group member information related to a player joining the voice group
     *  that is associated with a corresponding AgisGroup object.
     * @param memberOid
     * @param groupOid
     */
    protected boolean HandleVoiceMemberAdded(OID memberOid, OID groupOid){
        _log.debug("GroupPlugin.HandleVoiceMemberAdded - Got member added message");
        // We only want to process groupOids that matches an OID in our current groups list
        if(_currentGroups.containsKey(groupOid)){
            _log.debug("GroupPlugin.HandleVoiceMemberAdded - Got member match");
            AgisGroup group = _currentGroups.get(groupOid);
            AgisGroupMember groupMember = group.GetGroupMember(memberOid);
            if(groupMember != null){
                Map<String, Serializable> statsToUpdate = new HashMap<String, Serializable>();
                //If the group is currently in a muted state, then we need to mute the new member
                if(group.GetGroupMuted()){
                    groupMember.SetAllowedSpeaker(Boolean.FALSE);                
                    statsToUpdate.put("allowedSpeaker", Boolean.FALSE);                
                }

                //If the group member is flagged to indicate their voice is disabled, then enable it
                if(!groupMember.GetVoiceEnabled()){
                    groupMember.SetVoiceEnabled(Boolean.TRUE);
                    statsToUpdate.put("voiceEnabled", Boolean.FALSE);
                }

                if(statsToUpdate.size() > 0)
                    SendGroupPropertyUpdate(memberOid, group, statsToUpdate);
            }
            else
                _log.error("GroupPlugin.HandleVoiceMemberAdded - Player with OID " + memberOid.toString() + 
                           " is not a member of the group with OID " + groupOid.toString());
        }
        return true;
    }
    
    /**
     * Creates a new group if the inviter is not currently grouped
     */
    protected boolean HandleCreateGroup(GroupClient.createGroupMessage createMsg) {
    	ArrayList<OID> groupMemberOids = (ArrayList) createMsg.getProperty("groupMembers");
    	ArrayList<CombatInfo> groupMembers = new ArrayList<CombatInfo>();
    	for (OID memberOid: groupMemberOids) {
    		groupMembers.add(CombatPlugin.getCombatInfo(memberOid));
    	}
        if (groupMembers.size() < 2)
            return false;

        AgisGroup group = null;// Create a new group
        group = new AgisGroup();
        AgisGroupMember groupLeader = group.AddGroupMember(groupMembers.get(0));
        groupLeader.SetVoiceEnabled(false); //TODO: Figure out how to get leader's setting
        group.SetGroupLeaderOid(groupMemberOids.get(0));
        for (int i = 1; i < groupMembers.size(); i++) {
        	AgisGroupMember groupMember = group.AddGroupMember(groupMembers.get(i));
            groupMember.SetVoiceEnabled(false);
            groupMembers.get(i).setPendingGroupInvite(false);
        }
        _currentGroups.put(group.GetGroupOid(), group); // Add to our list to track    
        // Send clients info about the group and group members
        SendGroupUpdate(group);
        
        return true;
    }
    
    /*
     * Hooks
     */     

    /**
     * PropertyHook catches any property updates. We only want to process
     * entities that are flagged as grouped
     */
    class PropertyHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            PropertyMessage propMsg = (PropertyMessage) msg;
            return UpdateGroupMemberProps(propMsg);
        }
    }

    /**
     * GroupInviteResponseHook Adds a player to a group, or creates a new group
     * and sends out group info to the clients
     */
    class GroupInviteResponseHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage inviteMsg = (ExtensionMessage) msg;
            return HandleInviteResponse(inviteMsg);
        }
    }

    /**
     * GroupRemoveMemberHook is used to remove the group member in question.
     * 
     */
    class GroupRemoveMemberHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage removeMemberMsg = (ExtensionMessage) msg;
            CombatInfo subject = CombatPlugin.getCombatInfo((OID) removeMemberMsg.getProperty("target"));
            if (subject == null)
                return false;

            RemoveGroupMember(subject, true, removeMemberMsg.getSubject(), true);

            return true;
        }
    }

    /**
     * Handles group chat messages from the client
     */
    class GroupChatHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage groupChatMsg = (ExtensionMessage) msg;
            HandleGroupChat(groupChatMsg);
            return true;
        }
    }

    /**
     * LoginHook is used to store the names of users currently logged in. It needs to be moved.
     * @author Andrew
     *
     */
    class LoginHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	//TODO: Move this code to somewhere more logical.
            LoginMessage message = (LoginMessage) msg;
            OID playerOid = message.getSubject();
            //String playerName = WorldManagerClient.getObjectInfo(playerOid).name;
            //playerNamesOids.put(playerName, playerOid);

            Engine.getAgent().sendResponse(new ResponseMessage(message));
            return true;
        }
    }

    /**
     * LogOutHook is used to remove group members from a group who log out of the game.
     */
    class LogOutHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LogoutMessage logoutMsg = (LogoutMessage) msg;
            OID playerOid = logoutMsg.getSubject();
            Log.debug("LOGOUT: group logout started for: " + playerOid);
            CombatInfo subject = CombatPlugin.getCombatInfo(logoutMsg.getSubject());
            
            //String playerName = WorldManagerClient.getObjectInfo(playerOid).name;
            //playerNamesOids.remove(playerName);

            // If the player logging out is grouped, then remove them from their group
            if (subject != null && subject.isGrouped()) {
                RemoveGroupMember(subject, false, null, false);
            }
            Engine.getAgent().sendResponse(new ResponseMessage(logoutMsg));
            Log.debug("LOGOUT: group logout finished for: " + playerOid);
            return true;
        }
    }

    /*
     * GroupInviteHook handles invitee response
     */
    class GroupInviteHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage inviteMsg = (ExtensionMessage) msg;
            OID inviterOid = inviteMsg.getSubject();
            OID inviteeOid = (OID) inviteMsg.getProperty("target");

            return HandleGroupInvite(inviterOid, inviteeOid);
        }
    }
    
    /*
     * GroupInviteByNameHook handles invitee response
     */
    class GroupInviteByNameHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            GroupClient.groupInviteByNameMessage inviteMsg = (GroupClient.groupInviteByNameMessage) msg;
            OID inviterOid = (OID) inviteMsg.getProperty("inviterOid");
            String inviteeName = (String) inviteMsg.getProperty("inviteeName");
            // Get the targets oid
			try {
				OID inviteeOid = GroupClient.getPlayerByName(inviteeName);
				if (inviteeOid != null) {
	            	//OID inviteeOid = playerNamesOids.get(inviteeName);
	            	HandleGroupInvite(inviterOid, inviteeOid);
	            	Log.debug("GROUP: invited player oid: " + inviteeOid);
	            } else {
	            	ExtendedCombatMessages.sendErrorMessage(inviterOid, "Player " + inviteeName + " could not be found.");
	            }
			} catch (IOException e) {
			}
            
            return true;
        }
    }
    
    class GroupLeaveHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage inviteMsg = (ExtensionMessage) msg;
            OID leaverOid = inviteMsg.getSubject();
            CombatInfo subject = CombatPlugin.getCombatInfo(leaverOid);
            if (subject == null)
                return false;

            RemoveGroupMember(subject, false, null, true);
            return true;
        }
    }

    /**
     * RequestGroupInfoHook handles group info requests. Returns information about the group and who is in it
     * Message is intend for server to server comm.
     */
    class RequestGroupInfoHook implements Hook{
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage requestGroupInfoMsg = (ExtensionMessage) msg;
            CombatInfo subject = CombatPlugin.getCombatInfo(requestGroupInfoMsg.getSubject());
            if(subject == null)
                return false;

            OID sOid = subject.getOwnerOid();
            Lock lock = getObjectLockManager().getLock(sOid);
            lock.lock();

            try {
                GroupClient.GroupInfo groupInfo = HandleGroupInfoRequest(subject);
                Engine.getAgent().sendObjectResponse(msg, groupInfo);
            } finally {
                lock.unlock();
            }
            return true;
        }
    }
    
    /**
     * SetAllowedSpeakerInfoHook - used to set a whether the group member is allowed to talk in voice chat
     * 
     */
    class SetAllowedSpeakerHook implements Hook{
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage setAllowedSpeakerMsg = (ExtensionMessage)msg;

            OID targetOid = (OID)setAllowedSpeakerMsg.getProperty("target");
            OID setterOid = (OID)setAllowedSpeakerMsg.getProperty("setter");
            OID groupOid = (OID)setAllowedSpeakerMsg.getProperty("groupOid");

            return HandleSetAllowedSpeaker(targetOid, setterOid, groupOid);
        }
    }

    /**
     * MuteGrouphook - Used to mute or un-mute the group voice chat
     *                  If muting, only the group leader may talk
     */
    class MuteGroupHook implements Hook{
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage setAllowedSpeakerMsg = (ExtensionMessage)msg;

            OID setterOid = (OID)setAllowedSpeakerMsg.getProperty("setter");
            OID groupOid = (OID)setAllowedSpeakerMsg.getProperty("groupOid");
            return HandleMuteGroup(setterOid, groupOid);
        }
    }

    /**
     * VoiceStatusHook - Handles updates that determine if the player in a group has their voice
     *  configuration set to be enabled and to join group chat.
     *  Updates AgisGroupMember._voiceEnabled and broadcasts that to the rest of the group.
     */
    class VoiceStatusHook implements Hook{
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage voiceStatusMsg = (ExtensionMessage)msg;

            OID playerOid = (OID)voiceStatusMsg.getProperty("playerOid");
            OID groupOid = (OID)voiceStatusMsg.getProperty("groupOid");
            Boolean voiceEnabled = (Boolean)voiceStatusMsg.getProperty("voiceEnabled");

            return HandledVoiceStatus(playerOid, groupOid, voiceEnabled);
        }
    }

    /**
     * VoiceMemberAddedHook - Handles messages from the VoiceServer that 
     *  a new player was added to a voice group.
     */
    class VoiceMemberAddedHook implements Hook{
        public boolean processMessage (Message msg, int flags){
            ExtensionMessage voiceMemberAddedMsg = (ExtensionMessage)msg;
            OID memberOid = (OID)voiceMemberAddedMsg.getProperty("memberOid");
            OID groupOid = (OID)voiceMemberAddedMsg.getProperty("groupOid");
            return HandleVoiceMemberAdded(memberOid, groupOid);
        }
    }
    
    /**
     * CreateGroupHook creates a new group and sends out group info to the clients
     */
    class CreateGroupHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            GroupClient.createGroupMessage createMsg = (GroupClient.createGroupMessage) msg;
            return HandleCreateGroup(createMsg);
        }
    }
    
    /**
     * VoiceMemberAddedHook - Handles messages from the VoiceServer that 
     *  a new player was added to a voice group.
     */
    class PromoteToLeaderHook implements Hook{
        public boolean processMessage (Message msg, int flags){
        	ExtensionMessage promoteMsg = (ExtensionMessage)msg;
        	Log.debug("GROUP: got promote message");
        	CombatInfo subject = CombatPlugin.getCombatInfo((OID) promoteMsg.getProperty("target"));
            if (subject == null)
                return false;

            PromoteMemberToLeader(subject, promoteMsg.getSubject());
            return true;
        }
    }
}
