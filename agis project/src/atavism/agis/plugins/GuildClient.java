package atavism.agis.plugins;

import atavism.msgsys.MessageType;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.messages.PropertyMessage;
import atavism.server.util.Log;

import java.io.Serializable;

/**
 * This class is responsible for sending out messages associated with the Guild System. 
 * The majority (if not all) of the messages should be caught by the GuildPlugin class.
 * @author Andrew Harrison
 *
 */
public class GuildClient {
	protected GuildClient() {
	}
	
	/**
	 * Sends the createGuildMessage which will create a new guild if one with
	 * the given name doesn't exist.
	 * @param oid: the identifier of the player who wishes to create the guild
	 * @param guildName: the name of the new guild to be created
	 */
	public static void createGuild(OID oid, String guildName) {
		createGuildMessage msg = new createGuildMessage(oid, guildName);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("GUILD CLIENT: createGuildMessage hit 2");
	}

	public static class createGuildMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public createGuildMessage() {
            super();
        }
        
        public createGuildMessage(OID oid, String guildName) {
        	super(oid);
        	setMsgType(MSG_TYPE_CREATE_GUILD);
        	setProperty("guildName", guildName);
        	Log.debug("GUILD CLIENT: createGuildMessage hit 1");
        }
	}
	
	/**
	 * Sends the guildCommandMessage which will deal with the given command appropriately.
	 * @param oid: the identifier of the player who sent the command
	 * @param commandType: the type of command
	 * @param data: some data to assist with execution of the command
	 * @param dataTwo: another piece of data to assist with execution of the command0
	 */
	public static void guildCommand(OID oid, String commandType, Serializable data, Serializable dataTwo) {
		guildCommandMessage msg = new guildCommandMessage(oid, commandType, data, dataTwo);
		Engine.getAgent().sendBroadcast(msg);
		Log.debug("GUILD CLIENT: guildCommandMessage hit 2");
	}

	public static class guildCommandMessage extends PropertyMessage {
		private static final long serialVersionUID = 1L;
        public guildCommandMessage() {
            super();
        }
        
        public guildCommandMessage(OID oid, String commandType, Serializable data, Serializable dataTwo) {
        	super(oid);
        	setMsgType(MSG_TYPE_GUILD_COMMAND);
        	setProperty("commandType", commandType);
        	setProperty("commandData", data);
        	setProperty("commandDataTwo", dataTwo);
        	Log.debug("GUILD CLIENT: guildCommandMessage hit 1");
        }
	}
	
	
	public static final MessageType MSG_TYPE_CREATE_GUILD = MessageType
    .intern("guild.createGuild");
	
	public static final MessageType MSG_TYPE_GUILD_COMMAND = MessageType
    .intern("guild.guildCommand");
}