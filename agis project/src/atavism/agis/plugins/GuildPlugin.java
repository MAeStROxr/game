package atavism.agis.plugins;

import atavism.agis.objects.Guild;
import atavism.msgsys.Message;
import atavism.msgsys.MessageTypeFilter;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Hook;
import atavism.server.engine.OID;
import atavism.server.objects.Entity;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public class GuildPlugin extends EnginePlugin {

	public String getName() {
		return GUILD_PLUGIN_NAME;
	}

	public static String GUILD_PLUGIN_NAME = "GuildPlugin";
	
	public void onActivate() {
		Log.debug("GUILD PLUGIN: activated");
		registerHooks();
		MessageTypeFilter filter = new MessageTypeFilter();
		filter.addType(GuildClient.MSG_TYPE_CREATE_GUILD);
		filter.addType(GuildClient.MSG_TYPE_GUILD_COMMAND);
		//filter.addType(GuildClient.MSG_TYPE_CHAT_GUILD);
		Engine.getAgent().createSubscription(filter, this);
		//registerSaveHook(GuildClient.NAMESPACE, new GuildSaveHook());

	}

	protected void registerHooks() {
		getHookManager().addHook(GuildClient.MSG_TYPE_CREATE_GUILD,
				new GuildCreateHook());
		getHookManager().addHook(GuildClient.MSG_TYPE_GUILD_COMMAND,
				new GuildCommandHook());
	}
	
	class GuildCreateHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			GuildClient.createGuildMessage gmMsg = (GuildClient.createGuildMessage) msg;
			OID oid = gmMsg.getSubject();
			
			// First make sure the player isn't already in a guild
			String guildName = "";
			try {
				guildName = (String) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "guildName");
			} catch (NullPointerException e1) {
			}
			
			if (!guildName.equals("")) {
				Log.warn("GUILD PLUGIN: player attempted to create a guild, but is already currently in a guild");
				WorldManagerClient.sendObjChatMsg(oid, 1, "You cannot create a guild while you are in a guild");
				return true;
			}
			
			guildName = (String) gmMsg.getProperty("guildName");
			boolean exists = true;
			try {
				Entity testGuild = ObjectManagerClient.loadObjectData("guild_" + guildName);
			} catch (NullPointerException e) {
				exists = false;
			}
			if (exists == true) {
				WorldManagerClient.sendObjChatMsg(oid, 1, "The guild "
						+ guildName + " already exists. Please choose another name.");
				return true;
			}
			
			//int guildID = 5; //TODO: have some way of getting a unique ID if deemed needed
			ArrayList<OID> initiates = new ArrayList<OID>(); //TODO: Get a list of players who signed the charter
			int race = (Integer) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "race");
			String faction = "";
			//if (race.equals("human"))
			//	faction = "Order";
			Guild newGuild = new Guild(guildName, faction, rankNames, 
					defaultPermissions, oid, initiates);
			
			Entity guildEntity = new Entity(guildName);
			guildEntity.setPersistenceFlag(true);
			guildEntity.setName("guild_" + guildName);
			guildEntity.setProperty("guildObject", newGuild);
			ObjectManagerClient.saveObjectData("guild_" + guildName,
					guildEntity, WorldManagerClient.NAMESPACE);
			
			return true;
		}
	}
	
	class GuildCommandHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			GuildClient.guildCommandMessage gmMsg = (GuildClient.guildCommandMessage) msg;
			/**
			 * get some info about player
			 */
			OID oid = gmMsg.getSubject();
			String guildName = "";
			try {
				guildName = (String) EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, "guildName");
			} catch (NullPointerException e1) {
				Log.warn("GUILD PLUGIN: player attempted guild command, but is not currently in a guild");
			}

			if (guildName.equals("")) {
				WorldManagerClient.sendObjChatMsg(oid, 1, "You are not in a guild.");
				return true;
			}

			String commandType = (String) gmMsg.getProperty("commandType");
			Serializable commandData = gmMsg.getProperty("commandData");
			Serializable commandDataTwo = gmMsg.getProperty("commandDataTwo");

			Entity guildEntity = ObjectManagerClient.loadObjectData("guild_" + guildName);
			Guild playersGuild = (Guild) guildEntity.getProperty("guildObject");
			playersGuild.handleCommand(oid, commandType, commandData, commandDataTwo);
			
			return true;
		}
	}
	
	class GuildInviteResponseHook implements Hook {
		public boolean processMessage(Message msg, int flags) {
			GuildClient.guildCommandMessage gmMsg = (GuildClient.guildCommandMessage) msg;
			OID oid = gmMsg.getSubject();
			
			OID inviteeOid = (OID) gmMsg.getProperty("invitee");
			int response = (Integer) gmMsg.getProperty("response");
			if (response == 1) {
				// The player accepted the invite
				String guildName = (String) EnginePlugin.getObjectProperty(inviteeOid, WorldManagerClient.NAMESPACE, "guildName");
				Entity guildEntity = ObjectManagerClient.loadObjectData("guild_" + guildName);
				Guild playersGuild = (Guild) guildEntity.getProperty("guildObject");
				playersGuild.handleCommand(inviteeOid, "acceptInvite", oid, null);
			} else {
				// The player rejected the invite
				String playerName = WorldManagerClient.getObjectInfo(oid).name;
				WorldManagerClient.sendObjChatMsg(inviteeOid, 1, playerName + " has declined your guild invitation.");
			}
			
			return true;
		}
	}
	
	
	public static int maxRanks = 10;
	public static ArrayList<String> rankNames = new ArrayList<String>();
	public static ArrayList<ArrayList<String>> defaultPermissions = new ArrayList<ArrayList<String>>();
}