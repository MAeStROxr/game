package atavism.agis.plugins;

import atavism.agis.database.AccountDatabase;
import atavism.agis.objects.SocialInfo;
import atavism.msgsys.Message;
import atavism.msgsys.ResponseMessage;
import atavism.server.engine.*;
import atavism.server.messages.LoginMessage;
import atavism.server.objects.Entity;
import atavism.server.objects.EntityManager;
import atavism.server.objects.Template;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Map;

/**
 * handles requests for social states such as friends, guilds etc.
 * 
 * @author AJ
 * 
 */
public class SocialPlugin extends EnginePlugin {
    public SocialPlugin() {
        super("Social");
        setPluginType("Social");
    }

    public void onActivate() {
        registerHooks();
        
        /*MessageTypeFilter filter = new MessageTypeFilter();
        filter.addType(SocialClient.MSG_TYPE_CHANNEL_CHANGE);
        filter.addType(QuestClient.MSG_TYPE_REQ_RESET_QUESTS);
        filter.addType(QuestClient.MSG_TYPE_ABANDON_QUEST);
        filter.addType(AgisMobClient.MSG_TYPE_CATEGORY_UPDATED);*/
        /* Long sub = */ //Engine.getAgent().createSubscription(filter, this);

        //MessageTypeFilter filter = new MessageTypeFilter();
        /*filter.addType(QuestClient.MSG_TYPE_NEW_QUESTSTATE);
        filter.addType(QuestClient.MSG_TYPE_GET_QUEST_STATUS);
        filter.addType(QuestClient.MSG_TYPE_CONCLUDE_QUEST);
        filter.addType(QuestClient.MSG_TYPE_QUEST_ITEM_REQS);*/
        //filter.addType(LoginMessage.MSG_TYPE_LOGIN);
        /* Long sub = */ //Engine.getAgent().createSubscription(filter, this,MessageAgent.RESPONDER);

        if (Log.loggingDebug)
            log.debug("SocialPlugin activated");
        
        registerLoadHook(SocialClient.NAMESPACE, new SocialStateLoadHook());
        registerSaveHook(SocialClient.NAMESPACE, new SocialStateSaveHook());
        registerUnloadHook(SocialClient.NAMESPACE, new SocialStateUnloadHook());
        registerPluginNamespace(SocialClient.NAMESPACE, new SocialSubObjectHook());
        
        aDB = new AccountDatabase();
    }

    // how to process incoming messages
    protected void registerHooks() {
        //getHookManager().addHook(SocialClient.MSG_TYPE_CHANNEL_CHANGE, new ChannelChangeHook());
    	getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
 	           new LoginHook());
    }
    
    public static SocialInfo getSocialInfo(OID oid) {
		return (SocialInfo)EntityManager.getEntityByNamespace(oid, SocialClient.NAMESPACE);
	}
	
	public static void registerSocialInfo(SocialInfo sInfo) {
		EntityManager.registerEntityByNamespace(sInfo, SocialClient.NAMESPACE);
	}
	
	class SocialStateLoadHook implements LoadHook {
    	public void onLoad(Entity e) {
    		SocialInfo sInfo = (SocialInfo) e;
    		// Re-activate all quest states
    		Log.debug("CHANNEL: got channelson Load:" + sInfo.getChannels());
    		for (String channel : sInfo.getChannels()) {
    			//TODO: Send join channel command.
    		}
    	}
    }
	
	class SocialStateSaveHook implements SaveHook {
		public void onSave(Entity e, Namespace namespace) {
		}
    }
	
	class SocialStateUnloadHook implements UnloadHook {
    	public void onUnload(Entity e) {
    		SocialInfo sInfo = (SocialInfo) e;
    		// Re-activate all quest states
    		for (String channel : sInfo.getChannels()) {
    			//TODO: Send leave channel command
    		}
    	}
    }
    
    public class SocialSubObjectHook extends GenerateSubObjectHook
    {
    	public SocialSubObjectHook() {
    	    super(SocialPlugin.this);
    	}
    	public SubObjData generateSubObject(Template template, Namespace name, OID masterOid)
    	{
    		if (Log.loggingDebug)
    		     Log.debug("SocialPlugin::GenerateSubObjectHook::generateSubObject()");
   	        if (masterOid == null) {
   	            Log.error("GenerateSubObjectHook: no master oid");
   	            return null;
   	        }
    		if (Log.loggingDebug)
   	             Log.debug("GenerateSubObjectHook: masterOid=" + masterOid + ", template=" + template);
   	        
   	        Map<String,Serializable> props = template.getSubMap(SocialClient.NAMESPACE);
   	            
   	        // generate the subobject
			SocialInfo sInfo = new SocialInfo(masterOid);
			sInfo.setName(template.getName());

			Boolean persistent = (Boolean)template.get(Namespace.OBJECT_MANAGER,
													   ObjectManagerClient.TEMPL_PERSISTENT);
			if (persistent == null)
				persistent = false;
			sInfo.setPersistenceFlag(persistent);
   		    
   	        if (props != null)
   	        {
   	        	// copy properties from template to object
   	        	for (Map.Entry<String,Serializable> entry : props.entrySet()) {
   	        		String key = entry.getKey();
   	        		Serializable value = entry.getValue();
   		    		if (!key.startsWith(":")) {
   		    			sInfo.setProperty(key, value);
   		    		}
   	        	}
   	        }
    		if (Log.loggingDebug)
    		     Log.debug("GenerateSubObjectHook: created entity " + sInfo);
    		
    		// register the entity
			registerSocialInfo(sInfo);
			
			if (persistent)
				Engine.getPersistenceManager().persistEntity(sInfo);
   	            
   	        // send a response message
   	        return new SubObjData();
    	}
    }
    
    // Log the login information and send a response
    class LoginHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            LoginMessage message = (LoginMessage) msg;
            OID playerOid = message.getSubject();
            OID instanceOid = message.getInstanceOid();
            Log.debug("LoginHook: playerOid=" + playerOid + " instanceOid=" + instanceOid);
            LinkedList<OID> friendsOf = aDB.getFriendsOf(playerOid);
            WorldManagerClient.sendObjChatMsg(playerOid, 2, "Welcome to Smoo Online!");
            
            Engine.getAgent().sendResponse(new ResponseMessage(message));
            return true;
        }
    }
    
    public class ChannelChangeHook implements Hook {
        public boolean processMessage(Message m, int flags) {
            /*SocialClient.ChannelChangeMessage msg = (SocialClient.ChannelChangeMessage) m;
            OID playerOid = msg.getSubject();
            String channelName = msg.getChannelName();
            boolean joining = msg.getJoining();
            if (Log.loggingDebug)
                log.debug("ChannelChangeHook: playerOid=" + playerOid + ", channel=" + channelName 
                		+ ", joining=" + joining);
            
            Lock lock = getObjectLockManager().getLock(playerOid);
            lock.lock();
            try {
                SocialInfo sInfo = getSocialInfo(playerOid);
                if (joining)
                	sInfo.addChannel(channelName);
                else
                	sInfo.removeChannel(channelName);
            }
            finally {
                lock.unlock();
            }*/
            return true;
        }
    }
    
    protected AccountDatabase aDB;

    private static final Logger log = new Logger("SocialPlugin");
}
