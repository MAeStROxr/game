package atavism.agis.plugins;


import atavism.msgsys.MessageType;
import atavism.server.engine.OID;
import mygame.Proto;
import mygame.proto.BattleCmd;
import mygame.proto.BattleMsg;
import mygame.proto.Tactical;
import mygame.proto.TacticalPlayerMsg;

import java.nio.ByteBuffer;

public class TacticalClient {
	public static void sendTargetedBattleMsg(BattleMsg battleMsg,OID target) {
		ByteBuffer bb =battleMsg.getByteBuffer().duplicate();
		int pos = bb.position(), remaining = bb.remaining();
		//bb.position(pos);
		byte[] array = new byte[remaining];
		bb.get(array);
		bb = ByteBuffer.wrap(array);

		if (battleMsg.cmd()== BattleCmd.BATTLE_INFO) System.out.print("\nbytebuffer length:"+bb.array().length+" pos:"+pos);
		Proto.sendExtensionFlatbuffer(bb,"BattleMsg",Tactical.name(Tactical.BATTLE_COMMAND),target,target);


	}

	public static void sendLongTargetedBattleMsg(BattleMsg battleMsg,OID target) {
		ByteBuffer bb =battleMsg.getByteBuffer().duplicate();
		int pos = bb.position(), remaining = bb.remaining();
		byte[] array = new byte[remaining];
		bb.get(array);
		bb = ByteBuffer.wrap(array);
		System.out.print("\nLongTargetMessage bytebuffer length:"+bb.array().length+" pos:"+pos);
		Proto.sendLongExtensionFlatbuffer(bb,"BattleMsg",Tactical.name(Tactical.BATTLE_COMMAND),target,target);


	}

	public static void sendTargetedTacticalPlayerMsg(TacticalPlayerMsg playerMsg,OID target) {

		Proto.sendExtensionFlatbuffer(playerMsg.getByteBuffer(),"TacticalPlayerMsg",Tactical.name(Tactical.PLAYER_TACTICAL),target,OID.fromLong(playerMsg.subjectOID()));



	}
	public static final MessageType MSG_TYPE_PLAYER_TACTICAL = MessageType
			.intern("tactical." + Tactical.name(Tactical.PLAYER_TACTICAL));
	//Tactical.name(Tactical.PLAYER_UPDATE));

	public static final MessageType MSG_TYPE_BATTLE_COMMAND = MessageType
			.intern("tactical." + Tactical.name(Tactical.BATTLE_COMMAND));
	
	

}
