package atavism.agis.plugins;

import atavism.msgsys.Message;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.ResponseMessage;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Hook;
import atavism.server.engine.OID;
import atavism.server.messages.LoginMessage;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.util.LockFactory;
import atavism.server.util.Logger;
import mygame.Proto;
import mygame.proto.BattleCmd;
import mygame.proto.BattleMsg;
import mygame.proto.TacticalPlayerCmd;
import mygame.proto.TacticalPlayerMsg;
import mygame.tactical.BattleManager;
import mygame.tactical.TacticalPlayer;
import mygame.tactical.TacticalPlayerManager;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;

public class TacticalPlugin extends EnginePlugin {

	protected static final Logger log = new Logger("TacticalPlugin");
	protected static Lock lock = LockFactory.makeLock("TacticalPlugin");

	BattleManager battleManager = new BattleManager();
	TacticalPlayerManager playerManager = new TacticalPlayerManager();
	

	public TacticalPlugin() {
		super("Tactical");
		setPluginType("Tactical");
		log.debug("TacticalPlugin Constructor\n");
		battleManager.setPlayerManager(playerManager);
	}

	public static String TACTICAL_PLUGIN_NAME = "Tactical";

	public String getName() {
		return TACTICAL_PLUGIN_NAME;
	}

	public void onActivate() {
		log.debug("TacticalPlugin.onActivate()");
		getHookManager().addHook(TacticalClient.MSG_TYPE_PLAYER_TACTICAL, new PlayerHook());
		getHookManager().addHook(TacticalClient.MSG_TYPE_BATTLE_COMMAND, new BattleHook());
		getHookManager().addHook(LoginMessage.MSG_TYPE_LOGIN,
				new LoginHook());
		MessageTypeFilter filter = new MessageTypeFilter();
		filter.addType(TacticalClient.MSG_TYPE_PLAYER_TACTICAL);
		filter.addType(TacticalClient.MSG_TYPE_BATTLE_COMMAND);
		filter.addType(LoginMessage.MSG_TYPE_LOGIN);
		Engine.getAgent().createSubscription(filter, this);
	}


	class LoginHook implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags){
			LoginMessage message = (LoginMessage) msg;
			OID playerOid = message.getSubject();
			String name = message.getPlayerName();
			TacticalPlayer p = new TacticalPlayer(playerOid.toLong(),name);
			playerManager.addPlayer(playerOid.toLong(),p);


			Engine.getAgent().sendResponse(new ResponseMessage(message));
			return true;
		}
	}

	class PlayerHook implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage gridMsg = (ExtensionMessage) msg;
			ByteBuffer bb = Proto.getProtoMessage(gridMsg,"TacticalPlayerMsg");
			TacticalPlayerMsg playerMsg = TacticalPlayerMsg.getRootAsTacticalPlayerMsg(bb);
			log.debug("TACTICAL:  player hook called: " + TacticalPlayerCmd.name(playerMsg.cmd())
					+ " playerOID:"+playerMsg.subjectOID());
			playerManager.processTacticalPlayer(playerMsg);
			return true;
		}
	}

	class BattleHook implements Hook {
		@Override
		public boolean processMessage(Message msg, int flags) {
			ExtensionMessage gridMsg = (ExtensionMessage) msg;
			ByteBuffer bb = Proto.getProtoMessage(gridMsg,"BattleMsg");
			BattleMsg battleMsg = BattleMsg.getRootAsBattleMsg(bb);
			log.debug("TACTICAL:  battle hook called: " + BattleCmd.name(battleMsg.cmd()) +" battleOID:"+battleMsg.OID()
					+ " playerOID:"+battleMsg.subjectOID());
			battleManager.processBattle(battleMsg);



			return true;
		}

		
	}


	
	/*
	
	public void sendBattleAction(OID sender, Map<String, Serializable> props, byte action) {
		boolean sendToSender = true;
		switch (action) {
		case BattleCmd.BATTLE_OVER:
			break;
		case BattleCmd.PLAYER_ENDED_TURN:
			break;
		case BattleCmd.PLAYER_JOINED_BATTLE:
		
			break;
		case BattleCmd.PLAYER_LEFT_BATTLE:
			
			break;
		case BattleCmd.PLAYER_STARTED_BATTLE:
			
			break;
		//case PLAYER_UPDATE:

			//break;
		case BattleCmd.UNIT_ABILITY:
			break;
		case BattleCmd.UNIT_ATTACK:
			break;
		case BattleCmd.UNIT_MOVE:
			break;
		default:
			break;

		}
		props.put("ext_msg_subtype", Tactical.name(action));
		//Player player = allPlayers.get(sender);

		
		//sendAllPlayers(props, sender, sendToSender);

	}
*/
}
