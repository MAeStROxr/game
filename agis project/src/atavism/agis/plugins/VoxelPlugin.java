package atavism.agis.plugins;

import atavism.agis.core.BuildingResourceAcquireHook;
import atavism.agis.database.ContentDatabase;
import atavism.agis.objects.BuildObjectTemplate;
import atavism.agis.objects.Claim;
import atavism.agis.plugins.CombatClient.interruptAbilityMessage;
import atavism.agis.plugins.VoxelClient.GetBuildingTemplateMessage;
import atavism.agis.util.ExtendedCombatMessages;
import atavism.msgsys.Message;
import atavism.msgsys.MessageAgent;
import atavism.msgsys.MessageTypeFilter;
import atavism.msgsys.SubjectMessage;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.Hook;
import atavism.server.engine.OID;
import atavism.server.math.AOVector;
import atavism.server.math.Point;
import atavism.server.math.Quaternion;
import atavism.server.objects.ObjectTypes;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.InventoryClient;
import atavism.server.plugins.ProxyPlugin;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.ExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.LockFactory;
import atavism.server.util.Log;
import atavism.server.util.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * the voxel plugin tracks changes in a voxel-based world
 */
public class VoxelPlugin extends atavism.server.engine.EnginePlugin {
	
	public VoxelPlugin() {
        super("Voxel");
        setPluginType("Voxel");
    }
	
	public static String VOXEL_PLUGIN_NAME = "Voxel";
	    
	protected static final Logger log = new Logger("VoxelPlugin");
	protected static Lock lock = LockFactory.makeLock("VoxelPlugin");
	
	public void onActivate() {
		log.debug("VoxelPlugin.onActivate()");

        // register message hooks
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_SPAWNED,
				 new SpawnedHook());
        getHookManager().addHook(WorldManagerClient.MSG_TYPE_DESPAWNED,
				 new DespawnedHook());
        getHookManager().addHook(InstanceClient.MSG_TYPE_INSTANCE_LOADED,
                new InstanceLoadedHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_CREATE_CLAIM,
                new CreateClaimHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_EDIT_CLAIM,
                new EditClaimHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_PURCHASE_CLAIM,
                new PurchaseClaimHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_DELETE_CLAIM,
                new DeleteClaimHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_CLAIM_PERMISSION,
                new ClaimPermissionHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_CLAIM_ACTION,
                new ClaimActionHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_PLACE_CLAIM_OBJECT,
                new PlaceClaimObjectHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_EDIT_CLAIM_OBJECT,
                new EditClaimObjectHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_UPGRADE_BUILDING_OBJECT,
                new UpgradeClaimObjectHook());
        
        getHookManager().addHook(VoxelClient.MSG_TYPE_GET_RESOURCES,
                new GetBuildingResourcesHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_NO_BUILD_CLAIM_TRIGGER,
                new NoBuildClaimTriggerHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_GET_BUILDING_TEMPLATE,
                new GetBuildObjectTemplateHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_GET_CLAIM_OBJECT_INFO,
                new GetClaimObjectInfoHook());
        getHookManager().addHook(CombatClient.MSG_TYPE_INTERRUPT_ABILITY,
                new InterruptHook());
        getHookManager().addHook(VoxelClient.MSG_TYPE_ATTACK_BUILDING_OBJECT,
                new AttackBuildingObjectHook());
        
        // setup message filters
        MessageTypeFilter filter = new MessageTypeFilter();
        filter.addType(WorldManagerClient.MSG_TYPE_SPAWNED);
        filter.addType(WorldManagerClient.MSG_TYPE_DESPAWNED);
        filter.addType(InstanceClient.MSG_TYPE_INSTANCE_LOADED);
        filter.addType(VoxelClient.MSG_TYPE_CREATE_CLAIM);
        filter.addType(VoxelClient.MSG_TYPE_EDIT_CLAIM);
        filter.addType(VoxelClient.MSG_TYPE_PURCHASE_CLAIM);
        filter.addType(VoxelClient.MSG_TYPE_SELL_CLAIM);
        filter.addType(VoxelClient.MSG_TYPE_DELETE_CLAIM);
        filter.addType(VoxelClient.MSG_TYPE_CLAIM_PERMISSION);
        filter.addType(VoxelClient.MSG_TYPE_CLAIM_ACTION);
        filter.addType(VoxelClient.MSG_TYPE_PLACE_CLAIM_OBJECT);
        filter.addType(VoxelClient.MSG_TYPE_EDIT_CLAIM_OBJECT);
        filter.addType(VoxelClient.MSG_TYPE_UPGRADE_BUILDING_OBJECT);
        filter.addType(VoxelClient.MSG_TYPE_GET_RESOURCES);
        filter.addType(VoxelClient.MSG_TYPE_NO_BUILD_CLAIM_TRIGGER);
        filter.addType(VoxelClient.MSG_TYPE_GET_CLAIM_OBJECT_INFO);
        filter.addType(VoxelClient.MSG_TYPE_ATTACK_BUILDING_OBJECT);
        filter.addType(CombatClient.MSG_TYPE_INTERRUPT_ABILITY);
        // Account login message - no idea if it should go here
        filter.addType(ProxyPlugin.MSG_TYPE_ACCOUNT_LOGIN);
        Engine.getAgent().createSubscription(filter, this);
        
        // Create responder subscription
  	    MessageTypeFilter filter2 = new MessageTypeFilter();
  	    filter2.addType(VoxelClient.MSG_TYPE_GET_BUILDING_TEMPLATE);
  	    Engine.getAgent().createSubscription(filter2, this,
  	       MessageAgent.RESPONDER);
        
        // Connect to the Content Database and store the connection
        cDB = new ContentDatabase(true);
        String removeOnFail = cDB.loadGameSetting("REMOVE_ITEM_ON_BUILD_FAIL");
        if (removeOnFail != null)
        	REMOVE_ITEM_ON_BUILD_FAIL = Boolean.parseBoolean(removeOnFail);
        String canBuildFail = cDB.loadGameSetting("BUILD_CAN_FAIL");
        if (canBuildFail != null)
        	BUILD_CAN_FAIL = Boolean.parseBoolean(canBuildFail);
        
        // Load in the build object templates
    	buildObjectTemplates = cDB.loadBuildObjectTemplates();
        
        log.debug("VoxelPlugin.onActivate() completed");
        
        // Start combat stat tick
    	Engine.getExecutor().scheduleAtFixedRate(claimTick, 10, 1, TimeUnit.SECONDS);
    }
	
	class InstanceLoadedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		SubjectMessage message = (SubjectMessage) msg;
            OID instanceOid = message.getSubject();
            Log.debug("VOXEL: got instance loaded message with oid: " + instanceOid);
            int instanceID = (Integer)InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
            
        	claims.putAll(cDB.loadClaims(instanceID));
        	for (Claim claim : claims.values()) {
        		if (claim.getInstanceID() == instanceID)
        			claim.spawn(instanceOid);
        	}
            
    		return true;
    	}
	}
    
    class SpawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.SpawnedMessage spawnedMsg = (WorldManagerClient.SpawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            if (WorldManagerClient.getObjectInfo(objOid).objType == ObjectTypes.player) {
            	// Set the players world property
            	Log.debug("SPAWNED: getting claims for player: " + objOid);
            	//OID instanceOid = spawnedMsg.getInstanceOid();
            	OID accountID = (OID) EnginePlugin.getObjectProperty(objOid, WorldManagerClient.NAMESPACE, "accountId");
        	    // Check for claims within their range at login
        	    Point p = WorldManagerClient.getObjectInfo(objOid).loc;
        	    ArrayList<Claim> claimsInRange = new ArrayList<Claim>();
        	    Claim closestClaim = null;
        	    float closestRange = Float.MAX_VALUE;
        	    for (Claim claim : claims.values()) {
        	    	float distance = Point.distanceToXZ(p, new Point(claim.getLoc()));
        	    	if (distance < Claim.CLAIM_DRAW_RADIUS) {
        	    		claimsInRange.add(claim);
        	    		if (distance < closestRange) {
        	    			closestRange = distance;
            	    		closestClaim = claim;
        	    		}
        	    	}
        	    	// Send down claim if player owns it
        	    	if (claim.getOwner() != null && claim.getOwner().equals(accountID)) {
        	    		claim.sendClaimData(objOid);
        	    	}
        	    }
        	    
        	    // Load the closest claim first
        	    if (closestClaim != null) {
        	    	closestClaim.addPlayer(objOid);
        	    }
        	    for (Claim claimInRange : claimsInRange) {
        	    	if (!claimInRange.equals(closestClaim)) {
        	    		claimInRange.addPlayer(objOid);
        	    	}
        	    }
            }
            
            return true;
    	}
    }
    
    class DespawnedHook implements Hook  {
    	public boolean processMessage(Message msg, int flags) {
    		WorldManagerClient.DespawnedMessage spawnedMsg = (WorldManagerClient.DespawnedMessage) msg;
            OID objOid = spawnedMsg.getSubject();
            if (WorldManagerClient.getObjectInfo(objOid).objType == ObjectTypes.player) {
            	//OID instanceOid = spawnedMsg.getInstanceOid();
        	    //String world = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_NAME).templateName;
        	    // Check if there are any voxelands in their world
        	    for (Claim claim : claims.values()) {
        	    	claim.removePlayer(objOid, true);
        	    }
            }
            
            return true;
    	}
    }
    
    /**
	 * Called when a player tries to create a claim. Checks if the claim can be placed (i.e. not too close to another claim).
	 * If the player can make the claim it will save the data to the database and spawn the claim.
	 * @author Andrew Harrison
	 *
	 */
	class CreateClaimHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got create building");
            String name = (String)gridMsg.getProperty("name");
            AOVector locVector = (AOVector)gridMsg.getProperty("loc");
            int size = (Integer)gridMsg.getProperty("size");
            boolean forSale = (Boolean)gridMsg.getProperty("forSale");
            int cost = (Integer)gridMsg.getProperty("cost");
            int currency = (Integer)gridMsg.getProperty("currency");
            boolean owned = (Boolean)gridMsg.getProperty("owned");
            OID itemOID = null;
            Serializable itemProp = gridMsg.getProperty("item");
            if (itemProp != null) {
            	itemOID = (OID) itemProp;
            }
            int itemID = -1;
            Integer claimTemplateItem = (Integer)gridMsg.getProperty("claimTemplateItem");
            if (claimTemplateItem != null) {
            	itemID = claimTemplateItem;
            }
            OID instanceOid = WorldManagerClient.getObjectInfo(playerOid).instanceOid;
            // Check the player can place/update the building
            int adminLevel = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "adminLevel");
        	if (adminLevel != AgisLoginPlugin.ACCOUNT_ADMIN) {
        		// If the user isn't an admin - have they provided an item?
        		if (itemOID == null) {
        			return true;
        		}
        		if (playersInNoBuild.contains(playerOid)) {
        			ExtendedCombatMessages.sendErrorMessage(playerOid, "You cannot create a claim here");
        			return true;
        		}
        		for (Claim claim : claims.values()) {
                	if (claim.getInstanceOID().equals(instanceOid)) {
                		// In the same instance, is it within the claim size limit?
                		if (Point.distanceToXZ(new Point(locVector), new Point(claim.getLoc())) < (claim.getSize() + 120)) {
                			ExtendedCombatMessages.sendErrorMessage(playerOid, "You cannot place a claim within 120 metres of another");
                			return true;
                		}
                	}
                }
        	}
            
            // Update grid both locally and in the database
            Claim newClaim = new Claim();
            newClaim.setName(name);
            newClaim.setLoc(locVector);
            newClaim.setSize(size);
            newClaim.setForSale(forSale);
            newClaim.setCost(cost);
            newClaim.setCurrency(currency);
            newClaim.setClaimItemTemplate(itemID);
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            if (owned)
            	newClaim.setOwner(accountID);
            else
            	newClaim.setOwner(OID.fromLong(0l));
            newClaim.setInstanceOID(instanceOid);
            
            int instanceID = (Integer)InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_ID).templateID;
            int claimID = cDB.writeClaim(newClaim, instanceID);
            newClaim.setContentDatabase(cDB);
            claims.put(claimID, newClaim);
            Log.debug("CLAIM: updated database");
            // Spawn Claim
            newClaim.spawn();
            newClaim.sendClaimData(playerOid);
            
            // Send down claim result
            Map<String, Serializable> props = new HashMap<String, Serializable>();
    		props.put("ext_msg_subtype", "claim_made");
    		props.put("claimID", claimID);
    		props.put("claimName", name);
    		props.put("claimLoc", locVector);
    		props.put("claimArea", size);
    		props.put("forSale", forSale);
    		if (forSale) {
    			props.put("cost", cost);
    			props.put("currency", currency);
    		}
    		
    		TargetedExtensionMessage temsg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    		Engine.getAgent().sendBroadcast(temsg);
    		
    		// Send item remove message
    		if (itemOID != null)
    			AgisInventoryClient.removeSpecificItem(playerOid, itemOID, true, 1);
    		
    		// Generate claim stone and give it to the player
    		HashMap<String, Serializable> itemProps = new HashMap<String, Serializable>();
    		itemProps.put("teleportLoc", new Point(locVector));
    		String instanceName = InstanceClient.getInstanceInfo(instanceOid, InstanceClient.FLAG_TEMPLATE_NAME).templateName;
    		itemProps.put("teleportInstance", instanceName);
    		AgisInventoryClient.generateItem(playerOid, CLAIM_STONE_ITEM_ID, null, 1, itemProps);
            
            return true;
        }
	}
	
	/**
	 * Called when a player edits their claim, such as renaming it or setting it for sale.
	 * @author Andrew
	 *
	 */
	class EditClaimHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got edit claim");
            int claimID = (Integer)gridMsg.getProperty("claimID");
            String name = (String)gridMsg.getProperty("name");
            boolean forSale = (Boolean)gridMsg.getProperty("forSale");
            int cost = (Integer)gridMsg.getProperty("cost");
            int currency = (Integer)gridMsg.getProperty("currency");

            // Check that the claim is for sale
            if (!claims.containsKey(claimID)) {
            	return true;
            }
            Claim claim = claims.get(claimID);
            
            // Verify the editor is the owner
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            if (!claim.getOwner().equals(accountID)) {
            	return true;
            }
            claim.setName(name);
            claim.setForSale(forSale);
            claim.setCost(cost);
            claim.setCurrency(currency);
            if  (forSale) {
            	claim.setSellerName(WorldManagerClient.getObjectInfo(playerOid).name);
            }
            cDB.updateClaim(claim);
            claim.claimUpdated(playerOid);
            Log.debug("CLAIM: updated database");
            
            return true;
        }
	}
	
	/**
	 * Called when a player tries to purchase a claim. Checks if the claim is for sale and that they are capable to cover the cost.
	 * If the player can buy the claim it will update the data in the database and claim.
	 * @author Andrew
	 *
	 */
	class PurchaseClaimHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got purchase claim");
            int claimID = (Integer)gridMsg.getProperty("claimID");

            // Check that the claim is for sale
            if (!claims.containsKey(claimID)) {
            	return true;
            }
            Claim claim = claims.get(claimID);
            if (!claim.getForSale()) {
            	return true;
            }
            
            // Check if the user has enough of the currency to purchase it
            boolean canAfford = AgisInventoryClient.checkCurrency(playerOid, claim.getCurrency(), claim.getCost());
            if (!canAfford) {
            	ExtendedCombatMessages.sendErrorMessage(playerOid, "Insufficient funds to purchase this claim");
            	return true;
            }
            
            // Send a mail item to take the money from the player and give it to the seller
            if (claim.getSellerName() != null && !claim.getSellerName().equals("")) {
            	Log.debug("MAIL: going to send claim purchased");
            	String message = WorldManagerClient.getObjectInfo(playerOid).name + " has purchased your claim: " 
            		+ claim.getName() + ". Your payment is attached.";
            	AgisInventoryClient.sendMail(playerOid, claim.getSellerName(), "Claim sold", message, claim.getCurrency(), claim.getCost(), false);
            } else {
            	AgisInventoryClient.alterCurrency(playerOid, claim.getCurrency(), -1 * claim.getCost());
            }
            
            // Change ownership of the claim
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            claim.changeClaimOwner(playerOid, accountID);
            cDB.updateClaim(claim);

            Log.debug("CLAIM: updated database");
            
            // Send down claim result
            /*Map<String, Serializable> props = new HashMap<String, Serializable>();
    		props.put("ext_msg_subtype", "claim_made");
    		
    		TargetedExtensionMessage temsg = new TargetedExtensionMessage(
    				WorldManagerClient.MSG_TYPE_EXTENSION, playerOid, 
    				playerOid, false, props);
    			Engine.getAgent().sendBroadcast(temsg);*/
            
            return true;
        }
	}
	
	/**
	 * Called when a player tries to delete a claim. Checks if the claim can be placed (i.e. not too close to another claim).
	 * If the player can make the claim it will save the data to the database and spawn the claim.
	 * @author Andrew
	 *
	 */
	class DeleteClaimHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got delete claim");
            int claimID = (Integer)gridMsg.getProperty("claimID");
            if (!claims.containsKey(claimID)) {
            	// No claim exists with that ID
            	return true;
            }
            // Check the player can place/update the building
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            int adminLevel = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "adminLevel");
        	if (adminLevel != AgisLoginPlugin.ACCOUNT_ADMIN && !claims.get(claimID).getOwner().equals(accountID)) {
        		// The player does not own this claim, and they do not have admin rights
        		Log.debug("CLAIM: user cannot delete this claim");
        		return true;
        	}
        	
        	int itemID = claims.get(claimID).getClaimItemTemplate();
        	if (itemID != -1)
        		AgisInventoryClient.generateItem(playerOid, itemID, "", 1, null);
            
        	claims.get(claimID).claimDeleted();
        	claims.remove(claimID);
            cDB.deleteClaim(claimID);
            
            return true;
        }
	}
	
	/**
	 * Hook for the ClaimPermission Extension Message. Called when a player wants to 
	 * add or remove a permission for their claim.
	 * @author Andrew Harrison
	 *
	 */
	class ClaimPermissionHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage claimMsg = (ExtensionMessage)msg;
            OID playerOid = claimMsg.getSubject();
            Log.debug("CLAIM: got get claim object data");
            int claimID = (Integer)claimMsg.getProperty("claimID");
            String playerName = (String)claimMsg.getProperty("playerName");
            OID targetOid = Engine.getDatabase().getOidByName(playerName, WorldManagerClient.NAMESPACE);
            if (targetOid == null) {
            	ExtendedCombatMessages.sendErrorMessage(playerOid, "Player named " + playerName + " could not be found.");
            	return true;
            }
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            
            String action = (String)claimMsg.getProperty("action");
            if (action.equals("Add")) {
            	int permissionLevel = (Integer)claimMsg.getProperty("permissionLevel");
            	claims.get(claimID).addPermission(playerOid, accountID, targetOid, playerName, permissionLevel);
            } else if (action.equals("Remove")) {
            	claims.get(claimID).removePermission(playerOid, accountID, targetOid);
            }
            
            return true;
        }
	}
	
	/**
	 * Called when a player wants to perform a claim action such as building, digging or placing a prefab.
	 * It first checks if the player has the right to perform an action in the claim then adds the action to
	 * the claim.
	 * @author Andrew
	 *
	 */
	class ClaimActionHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got create building");
            int claimID = (Integer)gridMsg.getProperty("claim");
            // Is the player performing the action the owner?
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            if (claims.get(claimID).getPlayerPermission(playerOid, accountID) == 0) {
            	return true;
            }
            String action = (String)gridMsg.getProperty("action");
            String type = (String)gridMsg.getProperty("type");
            AOVector size = (AOVector)gridMsg.getProperty("size");
            AOVector loc = (AOVector)gridMsg.getProperty("loc");
            AOVector normal = (AOVector)gridMsg.getProperty("normal");
            int material = (Integer)gridMsg.getProperty("mat");
            /*if (action.equals("build")) {
            	// Get resource being used
            	int resourceID = (Integer)gridMsg.getProperty("itemTemplateID");
            	String resource = "" + resourceID;
            	int count = (Integer)gridMsg.getProperty("count");
            	//AgisInventoryClient.removeGenericItem(playerOid, itemTemplateID, false, count);
            	HashMap<String, Integer> buildingResources = (HashMap)EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "buildingResources");
                if (buildingResources == null || !buildingResources.containsKey(resource) || buildingResources.get(resource) < count) {
                	// User does not have the required resources for this action
                	ExtendedCombatMessages.sendErrorMessage(playerOid, "You do not have enough resources to perform that action");
                	return true;
                } else {
                	int newCount = buildingResources.get(resource) - count;
                	buildingResources.put(resource, newCount);
                	EnginePlugin.setObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "buildingResources", buildingResources);
                	BuildingResourceAcquireHook.sendBuildingResources(playerOid, buildingResources);
                }
            }*/
            Log.debug("CLAIM: got action for claim: " + claimID + " with action: " + action);
            // First check if it is an undo
            if (action.equals("undo")) {
            	claims.get(claimID).undoAction();
            } else {
            	claims.get(claimID).performClaimAction(action, type, size, loc, normal, material);
            }
            // Increase the masonry skill
            CombatClient.abilityUsed(playerOid, 16);
            return true;
        }
	}
	
	/**
	 * Called when a player wants to place a claim object.
	 * @author Andrew
	 *
	 */
	class PlaceClaimObjectHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got place object");
            int claimID = (Integer)gridMsg.getProperty("claim");
            // Is the player performing the action the owner?
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            if (claims.get(claimID).getPlayerPermission(playerOid, accountID) == 0) {
            	Log.debug("CLAIM: claim does not belong to the player");
            	return true;
            }
            int buildObjectTemplateID = (Integer)gridMsg.getProperty("buildObjectTemplateID");
            if (!buildObjectTemplates.containsKey(buildObjectTemplateID)) {
            	//TODO: Send error message
            	return true;
            }
            AOVector loc = (AOVector)gridMsg.getProperty("loc");
            Quaternion orient = (Quaternion)gridMsg.getProperty("orient");
            int itemID = (Integer)gridMsg.getProperty("itemID");
            OID itemOid = (OID)gridMsg.getProperty("itemOID");
            Log.debug("CLAIM: got object for claim: " + claimID + " with object: " + itemID);
            boolean addTask = claims.get(claimID).buildClaimObject(playerOid, buildObjectTemplates.get(buildObjectTemplateID), 
            		loc, orient, itemID, itemOid);
            if (addTask) {
            	activeClaimTasks.put(playerOid, claimID);
            }
            return true;
        }
	}
	
	/**
	 * Called when a player wants to edit a claim object.
	 * @author Andrew Harrison
	 *
	 */
	class EditClaimObjectHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got edit object");
            int claimID = (Integer)gridMsg.getProperty("claimID");
            int objectID = (Integer)gridMsg.getProperty("objectID");
            String action = (String)gridMsg.getProperty("action");
            // Is the player performing the action the owner?
            OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            if (action.equals("state")) {
            	String state = (String)gridMsg.getProperty("state");
            	claims.get(claimID).updateClaimObjectState(objectID, state);
            	return true;
            }
            if (claims.get(claimID).getPlayerPermission(playerOid, accountID) < Claim.PERMISSION_ADD_DELETE) {
            	Log.debug("CLAIM: Insufficient permissions");
            	return true;
            }
            
            if (action.equals("convert")) {
            	int itemID = claims.get(claimID).removeClaimObject(objectID);
            	if (itemID > 0) {
            		// Generate the item for the player again
            		AgisInventoryClient.generateItem(playerOid, itemID, "", 1, null);
            	}
            } else if (action.equals("save")) {
            	AOVector loc = (AOVector)gridMsg.getProperty("loc");
                Quaternion orient = (Quaternion)gridMsg.getProperty("orient");
                claims.get(claimID).moveClaimObject(objectID, loc, orient);
            }
            
            return true;
        }
	}
	
	/**
	 * Called when a player wants to upgrade a claim building object
	 * @author Andrew Harrison
	 *
	 */
	class UpgradeClaimObjectHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            Log.debug("CLAIM: got upgrade object");
            OID playerOid = gridMsg.getSubject();
            int claimID = (Integer)gridMsg.getProperty("claimID");
            int objectID = (Integer)gridMsg.getProperty("objectID");
            int itemID = (Integer)gridMsg.getProperty("itemID");
            OID itemOid = (OID)gridMsg.getProperty("itemOID");
            int count = (Integer)gridMsg.getProperty("count");
            // Is the player performing the action the owner?
            //OID accountID = (OID) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "accountId");
            /*if (!claims.get(claimID).getOwner().equals(accountID)) {
            	Log.debug("CLAIM: claim does not belong to the player");
            	return true;
            }*/
            
            //TODO: Verify they have the item they are trying to use
            OID item = InventoryClient.findItem(playerOid, itemID);
            if (item == null) {
            	ExtendedCombatMessages.sendErrorMessage(playerOid, "Item not found");
            	return true;
            }
            boolean addTask = claims.get(claimID).addItemToUpgradeClaimObject(playerOid, objectID, itemID, itemOid, count);
            if (addTask) {
            	activeClaimTasks.put(playerOid, claimID);
            }
            
            return true;
        }
	}
	
	
	/**
	 * Called when a client has loaded up and it wants the list of building resources sent down
	 * @author Andrew Harrison
	 *
	 */
	class GetBuildingResourcesHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got get building resources");
            // Send down building resources
    	    HashMap<String, Integer> buildingResources = (HashMap)EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "buildingResources");
            if (buildingResources != null) {
            	Log.debug("RESOURCES: sending resources");
            	BuildingResourceAcquireHook.sendBuildingResources(playerOid, buildingResources);
            } else {
            	Log.debug("RESOURCES: player has no resources");
            }
            return true;
        }
	}
	
	
	class NoBuildClaimTriggerHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got noBuild message");
            // Update no build
            int noBuild = (Integer)gridMsg.getProperty("noBuild");
            if (noBuild == 1 && !playersInNoBuild.contains(playerOid)) {
            	playersInNoBuild.add(playerOid);
            	Log.debug("CLAIM: added player to no build list");
            } else if (noBuild == 0) {
            	playersInNoBuild.remove(playerOid);
            	Log.debug("CLAIM: removed player from no build list");
            }
    	    
            return true;
        }
	}
	
	/**
     * Hook for the GetBuildingTemplateMessage. Gets the Build Object Template that matches
     * the given templateID and sends it back to the remote call procedure that sent the 
     * message out.
     * @author Andrew Harrison
     *
     */
    class GetBuildObjectTemplateHook implements Hook {
    	public boolean processMessage(Message msg, int flags) {
    		GetBuildingTemplateMessage EBMsg = (GetBuildingTemplateMessage) msg;
    		Log.debug("SKILL: got GetPlayerSkillLevelMessage");
    	    int templateID = EBMsg.getTemplateID();
    	    if (buildObjectTemplates.containsKey(templateID)) {
    	    	Engine.getAgent().sendObjectResponse(msg, buildObjectTemplates.get(templateID));
    	    } else {
    	    	Engine.getAgent().sendObjectResponse(msg, null);
    	    }
    		
    		return true;
    	}
    }
    
    /**
	 * Called when a player wants to edit a claim object.
	 * @author Andrew Harrison 
	 *
	 */
	class GetClaimObjectInfoHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got get claim object data");
            int claimID = (Integer)gridMsg.getProperty("claimID");
            int objectID = (Integer)gridMsg.getProperty("objectID");
            
            claims.get(claimID).sendObjectInfo(playerOid, objectID);
            
            return true;
        }
	}
	
	/**
	 * Handles the InterruptAbility Message which is sent when the player moves. Will cancel any
	 * active tasks the player currently has.
	 * @author Andrew Harrison
	 *
	 */
	class InterruptHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
        	interruptAbilityMessage gridMsg = (interruptAbilityMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: interrupt");
            if (activeClaimTasks.containsKey(playerOid)) {
            	if (claims.get(activeClaimTasks.get(playerOid)).interruptBuildTask(playerOid)) {
            		activeClaimTasks.remove(playerOid);
            	}
            }
            
            return true;
        }
	}
	
	/**
	 * Called when a player wants to attack a claim object.
	 * @author Andrew Harrison
	 *
	 */
	class AttackBuildingObjectHook implements Hook {
        public boolean processMessage(Message msg, int flags) {
            ExtensionMessage gridMsg = (ExtensionMessage)msg;
            OID playerOid = gridMsg.getSubject();
            Log.debug("CLAIM: got attack object");
            int claimID = (Integer)gridMsg.getProperty("claimID");
            int objectID = (Integer)gridMsg.getProperty("objectID");
            
            claims.get(claimID).attackBuildObject(playerOid, objectID);
            
            return true;
        }
	}
	
	// Temporary tick system
    class ClaimMessageTick implements Runnable {
    	
    	public void run() {
    		for (Claim claim : claims.values()) {
    			claim.sendActionsToPlayers();
    		}
    	}
    }
    
    ClaimMessageTick claimTick = new ClaimMessageTick();
    List<OID> playersInNoBuild = new ArrayList<OID>();
	
	private static HashMap<Integer, Claim> claims = new HashMap<Integer, Claim>();
	private static HashMap<Integer, BuildObjectTemplate> buildObjectTemplates = new HashMap<Integer, BuildObjectTemplate>();
	private HashMap<OID, Integer> activeClaimTasks = new HashMap<OID, Integer>();
	
	public static final int CLAIM_STONE_ITEM_ID = 11;
	public static boolean REMOVE_ITEM_ON_BUILD_FAIL = false;
	public static boolean BUILD_CAN_FAIL = false;
	
	private ContentDatabase cDB;
}