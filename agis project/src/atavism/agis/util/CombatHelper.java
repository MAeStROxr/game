package atavism.agis.util;

import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.SkillData;
import atavism.agis.plugins.CombatPlugin;
import atavism.server.engine.BasicWorldNode;
import atavism.server.engine.OID;
import atavism.server.math.Quaternion;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.io.IOException;

/**
 * Contains a collection of static functions that are used to assist in combat calculations.
 * If you want to modify the combat calculations used in Atavism, this is where you make the
 * changes.
 * @author Andrew Harrison
 *
 */
public class CombatHelper {
	
	/**
	 * Calculate the chance to hit when using a physical ability such as a melee weapon attack.
	 * @param obj: The character performing the ability
	 * @param target: The target of the ability
	 * @param skillType: The skill type of the ability
	 * @return
	 */
	public static double CalcPhysicalHitChance(CombatInfo obj, CombatInfo target, int skillType) {
		//Chance to hit = ((success stat[Dex/Int] + skill level) / ((success stat[Dex/Int] + skill level) + (opponents perception + defensive skill level)+0.2))
		double accuracy = 1;
		if (CombatPlugin.PHYSICAL_ACCURACY_STAT != null) {
			accuracy = obj.statGetCurrentValue(CombatPlugin.PHYSICAL_ACCURACY_STAT);
		}
		double targetPerception = 20; //target.statGetCurrentValue("perception");
		double targetLevel = target.statGetCurrentValue("level");
		// Caster skill level
		int skillLevel = 0;
		Log.debug("COMBATHELPER: about to check for skill level");
		if (skillType != -1) {
	        if (!obj.getCurrentSkillInfo().getSkills().containsKey(skillType)) {
	        	Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
	        	int casterLevel = obj.statGetCurrentValue("level");
	        	skillLevel = casterLevel * 10;
	        } else
	    	    skillLevel = obj.getCurrentSkillInfo().getSkills().get(skillType).getSkillCurrent();
		}
		
		Log.debug("COMBATHELPER: about to calc hitChance");
		// New Formula: Chance to hit = (arctan ((success stat[Dex/Int] * skill level) - (opponents perception * defensive skill level)/1400) * .3) +.7
		// atan((100 * 4) - ((100 * 8)/1400) * 0.3) + 0.7
		// atan((400) - (0.57) * 0.3) + 0.7
		// atan(398.3) + 0.7
		// 1.57 + 0.7 = 2.27
		double hitChance = Math.atan((accuracy * skillLevel) - ((targetPerception * targetLevel)/1400) * 0.3) + 0.7;
		
		return hitChance; 
	}
	
	/**
	 * Calculate the chance to hit when using a magical ability such as a damaging spell.
	 * @param obj: The character performing the ability
	 * @param target: The target of the ability
	 * @param skillType: The skill type of the ability
	 * @return
	 */
	public static double CalcMagicalHitChance(CombatInfo obj, CombatInfo target, int skillType) {
		//Chance to hit = ((success stat[Dex/Int] + skill level) / ((success stat[Dex/Int] + skill level) + (opponents perception + defensive skill level)+0.2))
		double accuracy = 1;
		if (CombatPlugin.MAGICAL_ACCURACY_STAT != null) {
			accuracy = obj.statGetCurrentValue(CombatPlugin.MAGICAL_ACCURACY_STAT);
		}
		double targetPerception = 20; //target.statGetCurrentValue("perception");
		double targetLevel = target.statGetCurrentValue("level"); 
		// Caster skill level
		int skillLevel = 0;
		if (skillType != -1) {
	        if (!obj.getCurrentSkillInfo().getSkills().containsKey(skillType)) {
	        	Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
	        	int casterLevel = obj.statGetCurrentValue("level");
	        	skillLevel = casterLevel * 10;
	        } else
	    	    skillLevel = obj.getCurrentSkillInfo().getSkills().get(skillType).getSkillCurrent();
		}

		double hitChance = Math.atan((accuracy * skillLevel) - ((targetPerception * targetLevel)/1400) * 0.3) + 0.7;
		return hitChance;
	}
	
	/**
	 * Calculates the amount of damage done by the effect using the physical-based stats for the calculations
	 * @param obj
	 * @param caster
	 * @param dmg
	 * @param dmgType
	 * @param skillMod
	 * @param skillType
	 * @param hitRoll
	 * @return
	 */
	public static int CalcMeleeDamage(CombatInfo obj, CombatInfo caster, int dmg, String dmgType, 
			float skillMod, int skillType, int hitRoll, boolean useHitRoll) {
		Log.debug("COMBATHELPER: calcMeleeDamage with base dmage: " + dmg + ", about to get attackType");
		if (dmgType == null || dmgType.equals("")) {
			// Get the attackers damageType
			dmgType = (String) caster.getProperty("attackType");
		}
		Log.debug("COMBATHELPER: calcMeleeDamage, just got attackType: " + dmgType);
		double damage = dmg;
		// increase the damage taken based on casters stats
		int skillLevel = 0;
		if (skillType != -1) {
	        if (!caster.getCurrentSkillInfo().getSkills().containsKey(skillType))
	    	    Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
	        else
	    	    skillLevel = caster.getCurrentSkillInfo().getSkills().get(skillType).getSkillCurrent();
		}
		
		damage += skillMod * (float)skillLevel;
		if (CombatPlugin.PHYSICAL_POWER_STAT != null) {
			double casterStrength = caster.statGetCurrentValue(CombatPlugin.PHYSICAL_POWER_STAT);
			double strengthModifier = casterStrength / 25.0; //100.0;
			//Log.debug("COMBAT: strength modifier: " + strengthModifier);
			damage += damage * strengthModifier;
		}
		double casterDamageModifier = caster.statGetCurrentValue(CombatPlugin.DAMAGE_DEALT_MODIFIER);
		casterDamageModifier /= 100.0;
		damage += damage * casterDamageModifier;
		// Modify by the hit Roll
		if (useHitRoll) {
			damage *= ((hitRoll / 3 + 65) / 100.0);
		}

		// Decrease the damage taken by the modifier
		Log.debug("COMBAT: before resist melee dmg: " + damage + "; damage type: " + dmgType);
		Log.debug("DMGTYPE: getting resistance stat: " + CombatPlugin.DAMAGE_TYPES.get(dmgType) + " for damage type: " + dmgType);
		double targetArmor = obj.statGetCurrentValue(CombatPlugin.DAMAGE_TYPES.get(dmgType));
		damage = damage * ((100-targetArmor) / 100);
		//damage -= damage * targetArmor / 100;
		Log.debug("COMBAT: final melee dmg: " + damage);
		double targetDamageModifier = obj.statGetCurrentValue(CombatPlugin.DAMAGE_TAKEN_MODIFIER);
		targetDamageModifier /= 100.0;
		damage += damage * targetDamageModifier;
		Log.debug("COMBAT: final melee dmg2: " + damage);
		if (damage <= 0) {
			damage = 1.0;
		}
		return (int) damage;
	}
	
	/**
	 * Calculates the amount of damage done by the effect using the magical-based stats for the calculations
	 * @param obj
	 * @param caster
	 * @param dmg
	 * @param dmgType
	 * @param skillMod
	 * @param skillType
	 * @param hitRoll
	 * @return
	 */
	public static int CalcMagicalDamage(CombatInfo obj, CombatInfo caster, int dmg, String dmgType, 
			float skillMod, int skillType, int hitRoll, boolean useHitRoll) {
		Log.debug("COMBATHELPER: CalcNonPhysicalDamage, about to get attackType");
		if (dmgType.equals("")) {
			// Get the attackers damageType
			dmgType = (String) caster.getProperty("attackType");
		}
		Log.debug("COMBATHELPER: CalcNonPhysicalDamage, just got attackType: " + dmgType);
		double damage = dmg;
		// increase the damage taken based on casters stats
		int skillLevel = 0;
		if (skillType != -1) {
	        if (!caster.getCurrentSkillInfo().getSkills().containsKey(skillType))
	    	    Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
	        else
	    	    skillLevel = caster.getCurrentSkillInfo().getSkills().get(skillType).getSkillCurrent();
		}
		
		damage += skillMod * (float)skillLevel;
		double potentialModifier = 0;
		if (CombatPlugin.MAGICAL_POWER_STAT != null) { 
			double casterPotential = caster.statGetCurrentValue(CombatPlugin.MAGICAL_POWER_STAT);
			potentialModifier = casterPotential / 25.0; //100.0;
		}
		damage += damage * potentialModifier;
		// Modify by the hit Roll
		if (useHitRoll) {
			damage *= ((hitRoll / 3 + 65) / 100.0);
		}
		
		double casterDamageModifier = caster.statGetCurrentValue(CombatPlugin.DAMAGE_DEALT_MODIFIER);
		casterDamageModifier /= 100.0;
		damage += damage * casterDamageModifier;

		// Decrease the damage taken by the modifier
		Log.debug("COMBAT: before flat resist magical dmg: " + damage);
		String resistanceStat = CombatPlugin.DAMAGE_TYPES.get(dmgType);
		if (resistanceStat != null && !resistanceStat.contains("none")) {
			double targetArmor = obj.statGetCurrentValue(resistanceStat);
			damage -= targetArmor;
		}
		Log.debug("COMBAT: final magical dmg: " + damage);
		double targetDamageModifier = obj.statGetCurrentValue(CombatPlugin.DAMAGE_TAKEN_MODIFIER);
		targetDamageModifier /= 100.0;
		damage += damage * targetDamageModifier;

		if (damage <= 0) {
			damage = 1.0;
		}
		return (int) damage;
	}
	
	/**
	 * Calculates the amount of health restored by the effect calling it. Uses magical based stats.
	 * @param obj
	 * @param caster
	 * @param heal
	 * @param skillMod
	 * @param skillType
	 * @return
	 */
	public static int CalcHeal(CombatInfo obj, CombatInfo caster, int heal, float skillMod, int skillType) {
		Log.debug("COMBATHELPER: CalcHeal hit with heal: " + heal + " skillType: " + skillType + " and skillMod: " + skillMod);
		double healVal = heal;
		// increase the damage taken based on casters stats
		int skillLevel = 0;
		if (skillType != -1) {
	        if (!caster.getCurrentSkillInfo().getSkills().containsKey(skillType))
	    	    Log.warn("COMBAT HELPER: player does not have this skill: " + skillType);
	        else
	    	    skillLevel = caster.getCurrentSkillInfo().getSkills().get(skillType).getSkillCurrent();
		}
		
		healVal += skillMod * (float)skillLevel;
		if (CombatPlugin.MAGICAL_POWER_STAT != null) {
			double casterPotential = caster.statGetCurrentValue(CombatPlugin.MAGICAL_POWER_STAT);
			double potentialModifier = (casterPotential) / 25.0; // 100
			Log.debug("HEAL: healVal: " + healVal + " potentialMod: " + potentialModifier);
			healVal += healVal * potentialModifier;
		}

		if (healVal <= 0) {
			healVal = 1.0;
		}
		return (int) healVal;
	}
	
	public static int calculateProperty(int level, int percentage) {
		return (int)((float)(100 + level) * ((float)percentage / 100.0f));
	}
	
	public static int calculateFlatResist(int level, int percentage) {
		return (int)((float)(100 + level) * ((float)percentage / 100.0f));
	}
	
	public static int calculatePercentResist(int level, int percentage) {
		return (int)(10f * ((float)percentage / 100.0f));
	}
	
	/**
	 * Calculates the chance the player has of leveling up the given skill. The chance
	 * is out of 100.
	 * @param skillData
	 * @return
	 */
	public static float calcSkillUpChance(SkillData skillData) {
		//TODO: Modify the formula below
	    //int gainRate = (skillData.getSkillMaxLevel() * CombatPlugin.POINTS_PER_SKILL_LEVEL 
	    //		- skillData.getSkillCurrent()) * CombatPlugin.SKILL_GAIN_RATE;
	    //int increaseChance = (int)((double)gainRate / ((double)skillData.getSkillMaxLevel() * 10.0));
		
	    int increaseChance = 60 - (skillData.getSkillLevel() / 6);
	    return increaseChance;
	}
	
	/**
	 * Calculates the chance the player has of levelling up the max skill level of the given skill.
	 * The chance is out of 100.
	 * @param skillData
	 * @return
	 */
	public static float calcMaxSkillUpChance(SkillData skillData) {
		//TODO: Modify the formula below
	    int increaseChance = 30 - (skillData.getSkillLevel() / 6);
	    return increaseChance;
	}
	
	/*
	 * Positional Helpers
	 */
	public static float calculateValue(CombatInfo obj, CombatInfo target) {
		float angle = 0;
		BasicWorldNode attackerNode;
		try {
			attackerNode = WorldManagerClient.getWorldNode(obj.getOwnerOid());
			angle = getAngleToTarget(attackerNode.getLoc().getX(), attackerNode.getLoc().getZ(), 
					target.getOwnerOid()); // pass my info as attacker
			// and use targets as base
		} catch (IOException e1) {
			Log.error("draive io exception occured: " + e1.getMessage());
		}

		return angle;
	}
	
	public static float getAngleToTarget(float f, float g, OID oid)
	throws IOException {
		BasicWorldNode targetNode = WorldManagerClient.getWorldNode(oid);
		float headingDifference = (getHeadingToSpot(f, g, oid) & 0xFFF)
		- ((short) getMobsHeading(targetNode.getOrientation()) & 0xFFF);
		if (headingDifference < 0)
			headingDifference += 4096.0f;
		return (headingDifference * 360.0f / 4096.0f);
	}
	
	public static float getMobsHeading(Quaternion q1) {
		float w = q1.getW();
		float y = q1.getY();
		float x = q1.getX();
		float z = q1.getZ();
		double heading = 0;
		double test = x * y + z * w;
		
		if (test > 0.499) {
			heading = 2 * Math.atan2(x, w);
			heading = heading * (180 / Math.PI);
			return (int) heading;
		}
		if (test < -0.499) {
			heading = -2 * Math.atan2(x, w);
			heading = heading * (180 / Math.PI);
			return (int) heading;
		}

		heading = Math.atan2(2 * y * w - 2 * x * z, 1 - 2 * y * y - 2 * z * z);
		heading = heading * (180 / Math.PI);

		if (heading < 0) {
			// remove this if not using the getangle method and get a raw
			// 0-30 value
			// heading = Math.abs(heading);
			return (int) heading;
		} else {
			// remove this if not using the getangle method and get a raw
			// 0-30 value
			// heading = 360 - heading;
			return (int) heading;
		}
	}

	public static short getHeadingToSpot(float f, float g, OID oid)
			throws IOException {
		BasicWorldNode tnode = WorldManagerClient.getWorldNode(oid);
		float dx = (long) f - tnode.getLoc().getX();
		float dz = (long) g - tnode.getLoc().getZ();
		short heading = (short) (Math.atan2(-dx, dz) * HEADING_CONST);
		if (heading < 0)
			heading += 0x1000;
		return heading;
	}
	
	public static double HEADING_CONST = 651.89864690440329530934789477382;
}