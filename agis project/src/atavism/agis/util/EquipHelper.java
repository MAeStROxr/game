package atavism.agis.util;

import atavism.agis.objects.AgisEquipSlot;
import atavism.agis.objects.AgisItem;
import atavism.agis.objects.CombatInfo;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.CombatPlugin;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Log;

import java.util.HashMap;

/**
 * Contains static functions that assist with Equipping and Unequipping items.
 * @author Andrew
 *
 */
public class EquipHelper {
	
	/**
	 * Updates the stats and other properties of the character who has just acquired or lost
	 * an item. By default is empty and is to be filled in by developers if they wish.
	 * @param oid
	 * @param item: The item acquired or lost
	 * @param acquired: True means the player now has the item, false means they lost it.
	 * @param obj
	 */
	public static void ItemAcquiredStatHelper(OID oid, AgisItem item, boolean acquired, CombatInfo obj) {
		Log.debug("EquipHelper.ItemAcquiredStatHelper item =" + item);
		if (item == null || item.getProperty("weight") == null || CombatPlugin.WEIGHT_STAT == null) {
			// Requires a valid item to do any stat changing
			return;
		}
		
		obj.statRemoveModifier(CombatPlugin.WEIGHT_STAT, "acquired" + item.getOid());
		Log.debug("WEIGHT: removing item =" + item.getOid());
		
		Integer weight = (Integer) item.getProperty("weight");
		weight *= item.getStackSize();
		if (acquired) {
			obj.statAddModifier(CombatPlugin.WEIGHT_STAT, "acquired" + item.getOid(), weight); // Weight is added on
			Log.debug("WEIGHT: item =" + item.getOid() + " with count: " + item.getStackSize() + " is giving weight: " + weight);
		}
		
		obj.statSendUpdate(true);
	}

	/**
	 * Updates the stats and other properties of the character equipping or unequipping an item.
	 * @param oid: the identifier of the equipers
	 * @param item: the item being equipped
	 * @param equipping: whether the item is being equipped or unequipped
	 * @param obj: the equipers combat object
	 */
	public static void UpdateEquiperStats(OID oid, AgisItem item, boolean equipping, CombatInfo obj) {
		Log.debug("EquipHelper.UpdateEquiperStats item =" + item);
		if (item == null) {
			// Requires a valid item to do any stat changing
			return;
		}
		
		// First get the item type
		String itemType = (String) item.getProperty("itemType");
		String slot = (String) item.getProperty("slot");
		
		// Then if the item is broke
		boolean itemBroke = false;
		if (item.getProperty("durability") != null && (Integer)item.getProperty("durability") == 0) {
			itemBroke = true;
		}
		if (itemType.equals("Weapon")) {
			// It's a weapon;
			if (equipping && !itemBroke) {
				String subType = (String) item.getProperty("subType");
				String weaponType = "Armed";
				Log.debug("ITEMS: weapon type for item " + item.getName() + " is: " + weaponType + " in slot: " + slot);
				if (slot.contains("Off Hand")) {
					EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "weapon2Type", subType);
				} else {
					EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "weaponType", subType);
					
					String damageType = (String) item.getProperty("damageType");
			    	EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "attackType", damageType);
			    	int speed = (Integer) item.getProperty("delay");
			    	int speed_mod = speed - obj.statGetBaseValue(CombatPlugin.ATTACK_SPEED_STAT);
			    	Log.debug("SPEED: weapon speed: " + speed + " and player speed: " + obj.statGetBaseValue(CombatPlugin.ATTACK_SPEED_STAT) 
			    			+ " gives mod: " + speed_mod);
			    	obj.statAddModifier(CombatPlugin.ATTACK_SPEED_STAT, item.getOid(), speed_mod);
			    	// Set equip type
			    	EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "equipType", subType);
			    	// If the item has an auto attack, set it now
			    	Integer autoAttack = (Integer)item.getProperty("autoAttack");
			    	if (autoAttack != null && autoAttack > 0) {
			    		//obj.overrideAutoAttackAbility(autoAttack);
			    		EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY, autoAttack);
			    	}
				}
		    	//obj.statSetBaseValue(CombatPlugin.ATTACK_SPEED_STAT, speed); // Speed is a flat value
		    	int damage = (Integer) item.getProperty("damage");
		    	obj.statAddModifier("dmg-base", item.getOid(), damage); // Damage is added on
			} else {
				Log.debug("ITEMS: slot for item " + item.getName() + " is: " + slot);
				if (slot.contains("Off Hand")) {
					EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "weapon2Type", "");
				} else {
					EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "weaponType", "Unarmed");
					EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "attackType", "crush");
					EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, "equipType", "");
					obj.statRemoveModifier(CombatPlugin.ATTACK_SPEED_STAT, item.getOid());
					Log.debug("SPEED: removing attack speed modifier");
					//obj.statSetBaseValue(CombatPlugin.ATTACK_SPEED_STAT, obj.statGetMaxValue(CombatPlugin.ATTACK_SPEED_STAT));
					Integer autoAttack = (Integer)item.getProperty("autoAttack");
			    	if (autoAttack != null && autoAttack > 0) {
			    		//obj.resetAutoAttackAbility();
			    		EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_AUTOATTACK_ABILITY, 
			    				obj.getProperty(CombatInfo.COMBAT_PROP_AUTOATTACK_BASE));
			    	}
				}
				obj.statRemoveModifier("dmg-base", item.getOid());
				
				// Clear out ammo settings
				//EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_LOADED, -1);
		        //EnginePlugin.setObjectPropertiesNoResponse(oid, CombatClient.NAMESPACE, CombatInfo.COMBAT_AMMO_DAMAGE, 0);
			}
		} else if (itemType.equals("Armor")){
			/*HashMap<String, Integer> resistances = (HashMap) item.getProperty("resistanceStats");
			for (String statName: resistances.keySet()) {
				int value = resistances.get(statName);
				Log.debug("EQUIP: altering resistance: " + statName + " by: " + value);
				if (statName.contains("_percent"))
					value = value / 10;
				int newValue = Math.round(value);
				if (equipping)
				    obj.statModifyBaseValue(statName, newValue);
				else
				    obj.statModifyBaseValue(statName, -newValue);
			}*/
		} else {
			// It's an unstated piece of armor or something, lets do nothing atm
		}
			
		// Alter players stats - First remove all stat links to this item
		for (String statName : CombatPlugin.STAT_LIST) {
			obj.statRemoveModifier(statName, item.getOid());
		}
		HashMap<String, Integer> stats = (HashMap) item.getProperty("bonusStats");
		for (String statName: stats.keySet()) {
			int value = stats.get(statName);
			if (equipping && !itemBroke) {
				Log.debug("EQUIP: equipping item altering stat: " + statName + " by: " + value);
			    obj.statAddModifier(statName, item.getOid(), value);
			    //TODO: Update vitality stats based on the mods?
			}
		}
			
		// Alter player energy
		/*int energyCost = (Integer) item.getProperty("energyCost");
		Log.debug("EQUIP: altering energy by: " + energyCost);
		if (equipping)
		    obj.statModifyBaseValue("mana-base", -energyCost);
		else
		   	obj.statModifyBaseValue("mana-base", energyCost);*/
		obj.statSendUpdate(true);
	}
	
	/**
	 * Sets the displayID on the player for the specified slot.
	 * @param mobOid
	 * @param displayVal
	 * @param slot
	 */
	public static void updateDisplay(OID mobOid, String displayVal, AgisEquipSlot slot) {
		if (slot.equals(AgisEquipSlot.CHEST)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "chestDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.LEGS)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "legDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.HEAD)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "headDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.HANDS)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "handDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.FEET)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "feetDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.SHOULDER)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "shoulderDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.SHIRT)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "shirtDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.BACK)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "capeDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.BELT)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "beltDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.PRIMARYWEAPON)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "weaponDisplayID", displayVal);
		} else if (slot.equals(AgisEquipSlot.SECONDARYWEAPON)) {
			EnginePlugin.setObjectPropertiesNoResponse(mobOid, WorldManagerClient.NAMESPACE, "weapon2DisplayID", displayVal);
		}
	}
}