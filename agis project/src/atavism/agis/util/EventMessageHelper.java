package atavism.agis.util;

import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper class that contains static functions for sending event messages down to the client.
 * @author Andrew Harrison
 *
 */
public class EventMessageHelper {
	
	/**
	 * Sends a Combat Event to the client allowing it to add an entry to the combat log and be used
	 * for any other display purposes.
	 * @param casterOID
	 * @param targetOID
	 * @param eventType
	 * @param val
	 */
	public static void SendCombatEvent(OID casterOID, OID targetOID, String eventType, int abilityID, int effectID, int val, int val2) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "combat_event");
        props.put("event", eventType);
        props.put("caster", casterOID);
        props.put("target", targetOID);
        props.put("abilityID", abilityID);
        props.put("effectID", effectID);
        props.put("value1", val);
        props.put("value2", val2);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		casterOID, targetOID, false, props);
        Engine.getAgent().sendBroadcast(msg);
        if (!casterOID.equals(targetOID)) {
        	msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		targetOID, casterOID, false, props);
        	Engine.getAgent().sendBroadcast(msg);
        }
        Log.debug("EventMessageHelper.SendCombatEvent Event: " + eventType);
	    
	}
	
	public static final String COMBAT_PHYSICAL_DAMAGE = "CombatPhysicalDamage";
	public static final String COMBAT_MAGICAL_DAMAGE = "CombatMagicalDamage";
	public static final String COMBAT_PHYSICAL_CRITICAL = "CombatPhysicalCritical";
	public static final String COMBAT_MAGICAL_CRITICAL = "CombatMagicalCritical";
	public static final String COMBAT_HEAL = "CombatHeal";
	public static final String COMBAT_HEALTH_TRANSFER = "CombatHealthTransfer";
	public static final String COMBAT_REVIVED = "CombatRevived";
	public static final String COMBAT_MISSED = "CombatMissed";
	public static final String COMBAT_DODGED = "CombatDodged";
	public static final String COMBAT_BLOCKED = "CombatBlocked";
	public static final String COMBAT_PARRIED = "CombatParried";
	public static final String COMBAT_EVADED = "CombatEvaded";
	public static final String COMBAT_IMMUNE = "CombatImmune";
	public static final String COMBAT_BUFF_GAINED = "CombatBuffGained";
	public static final String COMBAT_DEBUFF_GAINED = "CombatDebuffGained";
	public static final String COMBAT_BUFF_LOST = "CombatBuffLost";
	public static final String COMBAT_DEBUFF_LOST = "CombatDebuffLost";
	public static final String COMBAT_COOLDOWN_EXTENDED = "CombatCooldownExtended";
	public static final String COMBAT_REPUTATION_CHANGED = "CombatReputationChanged";
	public static final String COMBAT_EXP_GAINED = "CombatExpGained";
	public static final String COMBAT_ABILITY_LEARNED = "CombatAbilityLearned";
	public static final String COMBAT_CASTING_STARTED = "CastingStarted";
	public static final String COMBAT_CASTING_CANCELLED = "CastingCancelled";
	
	/**
	 * Sends an Inventory Event to the client allowing it to display the information if wanted.
	 * @param playerOid
	 * @param eventType
	 * @param itemID
	 * @param count
	 * @param data
	 */
	public static void SendInventoryEvent(OID playerOid, String eventType, int itemID, int count, String data) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "inventory_event");
        props.put("event", eventType);
        props.put("itemID", itemID);
        props.put("count", count);
        props.put("data", data);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		playerOid, playerOid, false, props);
        Log.debug("EventMessageHelper.SendInventoryEvent Event: " + eventType + " with item: " + itemID);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static final String ITEM_HARVESTED = "ItemHarvested";
	public static final String ITEM_LOOTED = "ItemLooted";
	
	/**
	 * Not used yet. To replace sendAnnouncementMessage
	 * @param playerOid
	 * @param eventType
	 * @param val
	 * @param data
	 */
	public static void SendGeneralEvent(OID playerOid, String eventType, int val, String data) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "general_event");
        props.put("event", eventType);
        props.put("val", val);
        props.put("data", data);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		playerOid, playerOid, false, props);
        Log.debug("EventMessageHelper.SendGeneralEvent Event: " + eventType);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static final String REPUTATION_CHANGED = "ReputationChanged";
	public static final String REPAIR_SUCCESSFUL = "RepairSuccessful";
	public static final String DUEL_COUNTDOWN = "DuelCountdown";
	public static final String DUEL_START = "DuelStart";
	public static final String DUEL_VICTORY = "DuelVictory";
	public static final String DUEL_DEFEAT = "DuelDefeat";
	public static final String DUEL_OUT_OF_BOUNDS = "DuelOutOfBounds";
	public static final String DUEL_NOT_OUT_OF_BOUNDS = "DuelNotOutOfBounds";
	public static final String STAT_INCREASE = "StatIncrease";
	public static final String STAT_DECREASE = "StatDecrease";
	
	/**
	 * Not used yet. To replace sendErrorMessage
	 * @param playerOid
	 * @param eventType
	 * @param val
	 * @param data
	 */
	public static void SendErrorEvent(OID playerOid, String eventType, int val, String data) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "error_event");
        props.put("event", eventType);
        props.put("val", val);
        props.put("data", data);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		playerOid, playerOid, false, props);
        Log.debug("EventMessageHelper.SendErrorEvent Event: " + eventType);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static final String NOT_ENOUGH_CURRENCY = "NotEnoughCurrency";
	public static final String INVALID_TRADE_CURRENCY = "InvalidTradeCurrency";
	public static final String NO_ITEM_DURABILITY = "NoItemDurability";
	public static final String SKILL_MISSING = "SkillMissing";
	public static final String SKILL_LEVEL_TOO_LOW = "SkillLevelTooLow";
	public static final String EQUIP_MISSING = "EquipMissing";
	public static final String TOO_FAR_AWAY = "TooFarAway";
	public static final String RESOURCE_NODE_BUSY = "ResourceNodeBusy";
	public static final String RESOURCE_HARVEST_FAILED = "ResourceHarvestFailed";
	public static final String INSTANCE_REQUIRES_GROUP = "InstanceRequiresGroup";
	public static final String SKILL_ALREADY_KNOWN = "SkillAlreadyKnown";
	public static final String CANNOT_SELL_ITEM = "CannotSellItem";
	
	public static void SendRequirementFailedEvent(OID playerOid, RequirementCheckResult result) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "requirement_failed_event");
        props.put("event", result.result);
        props.put("val", result.numericData);
        props.put("data", result.stringData);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		playerOid, playerOid, false, props);
        Log.debug("EventMessageHelper.SendRequirementFailedEvent Event: " + result.result);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void SendQuestEvent(OID playerOid, String eventType, String data, int val1, int val2, int val3) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "quest_event");
        props.put("event", eventType);
        props.put("val1", val1);
        props.put("val2", val2);
        props.put("val3", val3);
        props.put("data", data);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		playerOid, playerOid, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static final String QUEST_PROGRESS = "QuestProgress";
}