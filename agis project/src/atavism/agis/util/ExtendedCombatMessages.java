package atavism.agis.util;

import atavism.agis.core.Agis;
import atavism.agis.core.AgisAbility.AbilityResult;
import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.Currency;
import atavism.agis.objects.SkillData;
import atavism.agis.objects.SkillInfo;
import atavism.agis.plugins.AgisInventoryPlugin;
import atavism.agis.plugins.ClassAbilityPlugin;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.plugins.WorldManagerClient.TargetedExtensionMessage;
import atavism.server.plugins.WorldManagerClient.TargetedPropertyMessage;
import atavism.server.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExtendedCombatMessages {

	/**
	 * Sends a message to the client to display the combat event that occured to this person.
	 * @param oid
	 * @param dmg
	 * @param dmgType
	 */
	public static void sendCombatText(OID oid, String dmg, int dmgType) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "combat_text");
        props.put("DmgAmount", dmg);
        props.put("DmgType", dmgType);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		oid, oid, false, props);
        Log.debug("ECM: Sending combat text with value " + dmg + " and type: " + dmgType);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void sendCombatChat(CombatInfo obj, String cmsg, String msgType) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "combat_chat");
        props.put("Msg", cmsg);
        props.put("MsgType", msgType);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		obj.getOwnerOid(), obj.getOwnerOid(), false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	/**
	 * Sends a message to the client to display the combat event on the target to the caster.
	 * @param caster
	 * @param target
	 * @param dmg
	 * @param dmgType
	 */
	public static void sendCombatText2(OID caster, OID target, String dmg, int dmgType) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "combat_text2");
        props.put("DmgAmount", dmg);
        props.put("DmgType", dmgType);
        props.put("target", target);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		caster, caster, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void sendAbilityFailMessage(CombatInfo obj, AbilityResult result) {
        int failmessageType = 0;
        if (result == AbilityResult.INVALID_TARGET)
        	failmessageType = 1;
        else if (result == AbilityResult.OUT_OF_RANGE)
        	failmessageType = 2;
        else if (result == AbilityResult.TOO_CLOSE)
        	failmessageType = 3;
        else if (result == AbilityResult.BUSY || result == AbilityResult.NOT_READY)
        	failmessageType = 4;
        else if (result == AbilityResult.INSUFFICIENT_ENERGY)
        	failmessageType = 5;
        else if (result == AbilityResult.MISSING_REAGENT)
        	failmessageType = 6;
        else if (result == AbilityResult.MISSING_TOOL)
        	failmessageType = 7;
        else if (result == AbilityResult.MISSING_AMMO)
        	failmessageType = 8;
        //else if (result == AbilityResult.WRONG_STANCE)
        //	failmessageType = 9;
        else if (result == AbilityResult.INSUFFICIENT_VIGOR)
        	failmessageType = 12;
        else if (result == AbilityResult.EFFECT_MISSING)
        	failmessageType = 13;
        else if (result == AbilityResult.NO_TARGET)
        	failmessageType = 14;
        else if (result == AbilityResult.MISSING_WEAPON)
        	failmessageType = 15;
        else if (result == AbilityResult.PASSIVE)
        	failmessageType = 16;
        else if (result == AbilityResult.INTERRUPTED)
        	failmessageType = 17;
        else if (result == AbilityResult.DEAD)
        	failmessageType = 18;
        
        Log.debug("ANDREW - sending  message: " + failmessageType);
        
        Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "ability_error");
        props.put("ErrorText", failmessageType);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		obj.getOwnerOid(), obj.getOwnerOid(), false, props);
	    Engine.getAgent().sendBroadcast(msg);
    }
	
	public static void sendErrorMessage(OID oid, String message) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "error_message");
        props.put("ErrorText", message);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		oid, oid, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void sendCooldownMessage(OID oid, String type, long length) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "cooldown");
        props.put("CdType", type);
        props.put("CdLength", length);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		oid, oid, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	/**
	 * Unused.
	 * @param obj
	 * @param type
	 * @param length
	 */
	public static void sendEffectMessage(CombatInfo obj, int type, long length) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "effect");
        props.put("EffectType", type);
        props.put("EffectLength", length);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		obj.getOwnerOid(), obj.getOwnerOid(), false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void sendAnouncementMessage(OID oid, String message, String type) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "announcement");
        props.put("AnnouncementText", message);
        props.put("AnnouncementType", type);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		oid, oid, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void sendAbilities(OID oid, ArrayList<Integer> abilities) {
		TargetedPropertyMessage propMsg = new TargetedPropertyMessage(oid, oid);
    	propMsg.setProperty("abilities", abilities);
        Engine.getAgent().sendBroadcast(propMsg);
	}
	
	public static void sendActions(OID oid, ArrayList<String> actions) {
		TargetedPropertyMessage propMsg = new TargetedPropertyMessage(oid, oid);
		Log.warn("ACTIONS: sending action message to OID: " + oid + " with actions: " + actions);
    	propMsg.setProperty("actions", actions);
        Engine.getAgent().sendBroadcast(propMsg);
	}
	
	public static void sendSkills(OID oid, SkillInfo skillInfo) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "skills");
        props.put("skillPoints", skillInfo.getSkillPoints());
        int totalPoints = skillInfo.getSkillPoints() + skillInfo.getPointsSpent();
        props.put("totalSkillPoints", totalPoints);
        props.put("skillPointCost", ClassAbilityPlugin.GetSkillPointCost(totalPoints));
        int numSkills = 0;
        //Log.debug("SKILL: got " + skillInfo.getSkills().size() + " skills to send down");
        for (SkillData skillData : skillInfo.getSkills().values()) {
        	props.put("skill" + numSkills + "ID", skillData.getSkillID());
        	props.put("skill" + numSkills + "Name", skillData.getSkillName());
        	props.put("skill" + numSkills + "Current", skillData.getSkillCurrent());
        	props.put("skill" + numSkills + "Level", skillData.getSkillLevel());
        	props.put("skill" + numSkills + "Max", skillData.getSkillMaxLevel());
        	props.put("skill" + numSkills + "State", skillData.getState());
        	numSkills++;
        }
        
        props.put("numSkills", numSkills);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		oid, oid, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	}
	
	public static void sendCurrencies(OID oid, HashMap<Integer, Integer> currencies) {
		Map<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", "currencies");
        int numCurrencies = 0;
        OID accountID = (OID)EnginePlugin.getObjectProperty(oid, WorldManagerClient.NAMESPACE, WorldManagerClient.ACCOUNT_PROPERTY);
        for (int currencyID : currencies.keySet()) {
        	Currency currency = Agis.CurrencyManager.get(currencyID);
        	//Log.debug("CURRENCY: got currency to send: " + currencyID);
        	props.put("currency" + numCurrencies + "ID", currency.getCurrencyID());
        	props.put("currency" + numCurrencies + "Name", currency.getCurrencyName());
        	props.put("currency" + numCurrencies + "Icon", currency.getCurrencyIcon());
        	if (currency.getExternal()) {
        		props.put("currency" + numCurrencies + "Current", AgisInventoryPlugin.aDB.getCharacterCoinAmount(accountID));
        		//props.put("currency" + numCurrencies + "Current", currencies.get(currencyID));
        	} else {
        		props.put("currency" + numCurrencies + "Current", currencies.get(currencyID));
        	}
        	numCurrencies++;
        }
        
        props.put("numCurrencies", numCurrencies);
        TargetedExtensionMessage msg = new TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
        		oid, oid, false, props);
	    Engine.getAgent().sendBroadcast(msg);
	    Log.debug("AJCURRENCY: sending down currencies message to: " + oid + " with props: " + props);
	}
}