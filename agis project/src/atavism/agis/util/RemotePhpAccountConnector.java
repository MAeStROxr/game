package atavism.agis.util;

import atavism.server.objects.RemoteAccountConnector;
import atavism.server.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Sends login details to a php file to verify their account. Called by the MasterServer if 
 * set in the auth_server.py script.
 * @author Andrew Harrison
 *
 */
public class RemotePhpAccountConnector extends RemoteAccountConnector {
    public RemotePhpAccountConnector() {
        super();
    }

	@Override
	public AccountLoginStatus verifyAccount(String accountName, String password, HashMap<String, Object> props) {
		// TODO Auto-generated method stub
		Log.debug("CONNECTOR: verifying account with php connection");
		try {
		    
		    String res = "";
		    URL urlObj = new URL(url);
		    URLConnection lu = urlObj.openConnection();

		    // Send data - if you don't need to send data 
		    // ignore this section and just move on to the next one
		    String data = "user=" + URLEncoder.encode(accountName, "UTF-8") 
		    		+ "&password=" + URLEncoder.encode(password, "UTF-8");
		    lu.setDoOutput(true);
		    OutputStreamWriter wr = new OutputStreamWriter(lu.getOutputStream());
		    wr.write(data);
		    wr.flush();

		    // Get the response
		    BufferedReader rd = new BufferedReader(new InputStreamReader(lu.getInputStream()));
		    String line = ""; //res = "";
		    while ((line = rd.readLine()) != null) {
		      res += line;
		    }
		    Log.debug("PHP: response: " + res);

		    wr.flush();
		    wr.close();
		    System.out.println(res);
		    if (res.equals("Success")) {
		    	return AccountLoginStatus.Success;
		    }
		} catch (Exception e) {
			Log.debug("CONNECTOR: failed verifying account with php connection: " + e);
		}
		return AccountLoginStatus.ServerError;
	}
	
	@Override
	public AccountLoginStatus createAccount(String accountName, String email,
			String password, HashMap<String, Object> props) {
		// TODO Auto-generated method stub
		return AccountLoginStatus.NoAccess;
	}
	
	private String url = "http://yourdomain.com/verifyAccount.php";
	
	public void setUrl(String url) {
		this.url = url;
	}
}