package mygame;

import atavism.agis.objects.PlayerFactionData;
import atavism.agis.plugins.FactionPlugin;
import atavism.server.engine.OID;
import atavism.server.util.Logger;

import java.util.HashMap;

public class Player {
	
	private OID oid;
	private static final Logger log = new Logger("player log");
	private String name;
	private int faction;
	private HashMap<Integer,PlayerFactionData> pfdMap;
	
	public Player(long playerOid, String playerName) {
		oid = OID.fromLong(playerOid);
		name = playerName;
	}

	public Player(OID playerOid, String playerName) {
		oid = playerOid;
		name = playerName;
	}
	
	public int getFaction(){ return faction;}
	
	public void setFaction(int fac){faction = fac;}
	
	@Override
	public String toString(){
		return "["+oid.toLong()+","+name+"]";
	}

	public int getStanding(int faction){
		log.debug("getting standing for faction:"+faction+" on player:"+oid.toLong()+ " that has faction:"+getFaction());
		if (faction == this.faction) {
			if (faction == 1) return FactionPlugin.Neutral;
			log.debug("same faction, returning exalted");
			return FactionPlugin.Exalted;
		}
		if (pfdMap.containsKey(faction)) {
			int standing =  FactionPlugin.calculateStanding(pfdMap.get(faction).getReputation());
			log.debug("pfdMap contains faction, got standing:"+standing);
			return standing;
		}
		else {
			log.debug(" pfdMap does NOT contain faction, returning Neutral");
			return FactionPlugin.Neutral;
		}
	}

	public void setPfdMap(HashMap<Integer,PlayerFactionData> pfdMap) {
		this.pfdMap = pfdMap;
	}


	@Override
	public boolean equals(Object o){
		Player p = (Player)o;
		return p.oid==oid;
	}
	public OID getOID() {
		return oid;
	}
	public String getName() {
		return name;
	}

}