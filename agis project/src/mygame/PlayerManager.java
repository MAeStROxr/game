package mygame;

import atavism.agis.objects.PlayerFactionData;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PlayerManager<T extends Player> {

	protected static Proto proto = new Proto();
	protected HashMap<OID, T> players = new HashMap<>();
	protected HashSet<T> loggedIn = new HashSet<T>();
	protected static final Logger log = new Logger("PlayerManager");
	
	public PlayerManager(){
	}

	public  T addPlayer(long playerOID, T player) {
		synchronized(players) {
			return players.putIfAbsent(OID.fromLong(playerOID), player);
		}
	}

	 public  T addPlayer(OID playerOID, T player) {
		 return players.putIfAbsent(playerOID, player);
	}
	public T getPlayer(long oid) {
		return players.get(OID.fromLong(oid));
	}


	public T getPlayer(OID oid) {
		return players.get(oid);
	}

	public boolean isLoggedIn(T player){
		return loggedIn.contains(player);
	}

	public void loginPlayer(T player){
		int subjectFaction = (Integer) EnginePlugin.getObjectProperty(player.getOID(), WorldManagerClient.NAMESPACE, "faction");
		player.setFaction(subjectFaction);
		log.debug("playermanager adding player: subjectoid:"+player.getOID().toLong()+" factionId:"+subjectFaction);
		HashMap<Integer, PlayerFactionData> playerPfdMap = (HashMap) EnginePlugin.getObjectProperty(player.getOID(), WorldManagerClient.NAMESPACE, "factionData");
		playerPfdMap.forEach((facID,facData)->log.debug("pfdmap entry- facID:"+facID+" facDataId:"+facData.getFaction()+" facData rep:"+facData.getReputation()));
		player.setPfdMap(playerPfdMap);
		loggedIn.add(player);
	}

	public void logoutPlayer(T player){
		loggedIn.remove(player);
	}

	public Set<OID> getsPlayerSet(){
		return players.keySet();
	}
	
	public boolean playerExists(long oid) { return playerExists(OID.fromLong(oid));}

	public boolean playerExists(OID oid) { return players.containsKey(oid);}


	public void clear() {
		players.clear();
	}
}
