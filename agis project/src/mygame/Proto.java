package mygame;

import atavism.msgsys.MessageType;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.control.ControlPlayer;
import mygame.control.ControlPoint;
import mygame.control.PlayerNode;
import mygame.proto.*;
import mygame.tactical.Battle;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.function.Consumer;

public class Proto {

    public static Logger controlLog;
    public static Logger tacticalLog;


    public void addUnitBasic(FlatBufferBuilder fbb, long unitOID, String name){
        int nameOffset = fbb.createString(name);
        UnitMsg.startUnitMsg(fbb);
        UnitMsg.addOID(fbb, unitOID);
        UnitMsg.addUnitName(fbb, nameOffset);
    }

    public int addUnitMsg(FlatBufferBuilder fbb, long unitOID, String name, int factionId, int prefabId, int dataId, int HP){
        addUnitBasic(fbb,unitOID,name);
        UnitMsg.addFactionID(fbb,factionId);
        UnitMsg.addPrefabId(fbb,prefabId);
        UnitMsg.addDataId(fbb,dataId);
        UnitMsg.addTileId(fbb,-1);
        UnitMsg.addHP(fbb,HP);
        return UnitMsg.endUnitMsg(fbb);
    }

    public int addUnitMsgBasic(FlatBufferBuilder fbb, long unitOID, String name) {
        addUnitBasic(fbb,unitOID,name);
        return UnitMsg.endUnitMsg(fbb);
    }

    public UnitMsg createUnitMsgBasic(long unitOID, String name) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int unitMsgOffset = addUnitMsgBasic(fbb, unitOID, name);
        fbb.finish(unitMsgOffset);
        return UnitMsg.getRootAsUnitMsg(fbb.dataBuffer());
    }

    public int addExistingUnit(FlatBufferBuilder fbb, UnitMsg unit) {
        int nameOffset = fbb.createString(unit.unitName());
        int unitOffset = UnitMsg.createUnitMsg(fbb, unit.OID(), nameOffset, unit.prefabId(), unit.dataId(),
                unit.tileId(), unit.factionID(), unit.level(), unit.HP(), unit.AP(), unit.turnPriority(),
                unit.moveRange(), unit.attackRange(), unit.hitChance(), unit.dodgeChance(), unit.damageMin(),
                unit.damageMax(), unit.critChance(), unit.critAvoidance(), unit.critMultiplier(), unit.stunChance(),
                unit.stunAvoidance(), unit.stunDuration(), unit.silentChance(), unit.silentAvoidance(),
                unit.silentDuration(), unit.HPPerTurn(), unit.APPerTurn(), unit.defaultHP(), unit.defaultAP());
        return unitOffset;
    }


    public int addExistingTile(FlatBufferBuilder fbb, TileMsg tile) {
        TileMsg.startTileMsg(fbb);
        TileMsg.addTileId(fbb, tile.tileId());
        TileMsg.addWalkable(fbb, tile.walkable());
        return TileMsg.endTileMsg(fbb);
    }


    public int addExistingTiles(FlatBufferBuilder fbb, List<TileMsg> tiles) {
        int[] tileOffsets = new int[tiles.size()];
        int i = 0;
        for (TileMsg tile : tiles) {
            tileOffsets[i] = addExistingTile(fbb, tile);
            i++;
        }
        return BattleMsg.createGridTilesVector(fbb, tileOffsets);
    }

    public int addArmyMsgBasic(boolean endMsg, FlatBufferBuilder fbb, long armyOID, int armyNameOffset, int unitsVec) {
        ArmyMsg.startArmyMsg(fbb);
        ArmyMsg.addArmyUnits(fbb, unitsVec);
        ArmyMsg.addOID(fbb, armyOID);
        ArmyMsg.addArmyName(fbb, armyNameOffset);
        if (!endMsg) return -1;
        return ArmyMsg.endArmyMsg(fbb);
    }

    public ArmyMsg createExistingArmyWithUnits(ArmyMsg army,List<UnitMsg> newUnits){
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        int offset = addExistingArmyWithUnits(fbb,army,newUnits);
        fbb.finish(offset);
        return ArmyMsg.getRootAsArmyMsg(fbb.dataBuffer());
    }


    public int addExistingArmyMsg(FlatBufferBuilder fbb, ArmyMsg army) {
        List<UnitMsg> units = new ArrayList<>();
        for (int i = 0; i < army.armyUnitsLength(); i++)
            units.add(army.armyUnits(i));
        return addExistingArmyWithUnits(fbb, army, units);
    }


    public int addExistingArmyWithUnits(FlatBufferBuilder fbb, ArmyMsg army, List<UnitMsg> units) {
        int unitsOffset = addExistingArmyUnits(fbb, units);
        int nameOffset = army.armyName() != null ? fbb.createString(army.armyName()) : fbb.createString("Unknown");
        Vec3 pos = army.position(), color = army.color();
        ArmyMsg.startArmyMsg(fbb);
        ArmyMsg.addArmyUnits(fbb, unitsOffset);
        ArmyMsg.addOID(fbb, army.OID());
        ArmyMsg.addArmyName(fbb, nameOffset);
        fbb.forceDefaults(true);
        ArmyMsg.addIsNetworkPlayer(fbb, army.isNetworkPlayer());
        ArmyMsg.addIsPlayerFaction(fbb, army.isPlayerFaction());
        ArmyMsg.addRequireDeployment(fbb, army.requireDeployment());
        ArmyMsg.addIsHostile(fbb,army.isHostile());
        ArmyMsg.addFactionID(fbb,army.factionID());
        ArmyMsg.addControllerOid(fbb,army.controllerOid());
        ArmyMsg.addIsMainArmy(fbb, army.isMainArmy());
        fbb.forceDefaults(false);
        if (color!=null) ArmyMsg.addColor(fbb,Vec3.createVec3(fbb,color.x(),color.y(),color.z()));
        if (pos!=null) ArmyMsg.addPosition(fbb, Vec3.createVec3(fbb,pos.x(),pos.y(),pos.z()));
        ArmyMsg.addDataID(fbb, army.dataID());
        int armyOffset = ArmyMsg.endArmyMsg(fbb);
        return armyOffset;
    }

    public int[] addExistingArmies(FlatBufferBuilder fbb, Collection<ArmyMsg> armiesList) {
        int[] armies = new int[armiesList.size()];
        int i = 0;
        for (ArmyMsg army : armiesList) {
            armies[i] = addExistingArmyMsg(fbb, army);
            i++;
        }
        return armies;
    }

    public int addPointExistingArmies(FlatBufferBuilder fbb, Collection<ArmyMsg> armiesList) {
        return ControlPointMsg.createGarrisonedArmiesVector(fbb, addExistingArmies(fbb, armiesList));
    }


    public ArmyMsg createArmy(long armyOID, String armyName, List<UnitMsg> units) {
        FlatBufferBuilder garrisonBuff = new FlatBufferBuilder();
        int unitsVecOffset = addExistingArmyUnits(garrisonBuff, units);
        int nameOffset = garrisonBuff.createString(armyName);
        int armyOffset = addArmyMsgBasic(true, garrisonBuff, armyOID, nameOffset, unitsVecOffset);
        garrisonBuff.finish(armyOffset);
        ArmyMsg army = ArmyMsg.getRootAsArmyMsg(garrisonBuff.dataBuffer());
        return army;
    }

    public int addExistingArmyUnits(FlatBufferBuilder fbb, List<UnitMsg> units) {
        int[] unitsVec = new int[units.size()];
        for (int i = 0; i < units.size(); i++)
            unitsVec[i] = addExistingUnit(fbb, units.get(i));
        return ArmyMsg.createArmyUnitsVector(fbb, unitsVec);
    }


    public int addPointMsg(boolean endMsg, FlatBufferBuilder fbb, long pointOID, String pointName, int garrisonOffset, int armiesOffset, byte action) {
        if (pointName == null) pointName = "Null Point Name";
        int nameOffset = fbb.createString(pointName);
        ControlPointMsg.startControlPointMsg(fbb);
        ControlPointMsg.addName(fbb,nameOffset);
        ControlPointMsg.addAction(fbb, action);
        ControlPointMsg.addOID(fbb, pointOID);
        ControlPointMsg.addGarrison(fbb, garrisonOffset);
        ControlPointMsg.addGarrisonedArmies(fbb, armiesOffset);
        if (!endMsg) return -1;
        return ControlPointMsg.endControlPointMsg(fbb);
    }

    public int addPointMsg(boolean endMsg, FlatBufferBuilder fbb, long pointOID, String pointName, ArmyMsg garrison, Collection<ArmyMsg> armies, byte action) {
        int garrisonOffset = garrison == null ? 0 : addExistingArmyMsg(fbb, garrison);
        int armiesOffset = armies == null ? 0 : addPointExistingArmies(fbb, armies);
        return addPointMsg(endMsg, fbb, pointOID, pointName, garrisonOffset, armiesOffset, action);
    }

    public ControlPointMsg createPointMsg(long pointOID, String pointName, byte cmd, ArmyMsg garrison, Collection<ArmyMsg> garrisonArmies, byte action) {
        return createPointMsg(pointOID, pointName, cmd, garrison, garrisonArmies, new ArrayList<>(), action);
    }

    public ControlPointMsg createPointMsg(long pointOID, String pointName, byte cmd, ArmyMsg garrison, Collection<ArmyMsg> garrisonArmies, Collection<ArmyMsg> armies, byte action) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int armiesOffset = -1;
        if (armies.size() > 0) armiesOffset = ControlPointMsg.createArmiesVector(fbb, addExistingArmies(fbb, armies));
        addPointMsg(false, fbb, pointOID, pointName, garrison, garrisonArmies, action);
        if (armies.size() > 0) ControlPointMsg.addArmies(fbb, armiesOffset);
        ControlPointMsg.addCmd(fbb, cmd);
        ControlPointMsg.addSubjectOID(fbb, pointOID);
        int pointMsgOffset = ControlPointMsg.endControlPointMsg(fbb);
        fbb.finish(pointMsgOffset);
        ControlPointMsg pointMsg = ControlPointMsg.getRootAsControlPointMsg(fbb.dataBuffer());
        return pointMsg;
    }

    void constructUnitInfoString(StringBuffer sb, UnitMsg u){
        sb.append("[Name:" + u.unitName() + ",OID:" + u.OID() + ",PrefabId:" + u.prefabId() +
                ",HP:"+u.HP()+",tileId:"+u.tileId()+",factionId:"+u.factionID()+",dataId:"+u.dataId()+"]");
    }

    void constructUnitCollectionString(StringBuffer sb, Collection<UnitMsg> uCol){
        for (UnitMsg u : uCol) constructUnitInfoString(sb,u);
    }

    public String logUnitCollection(Collection<UnitMsg> uCol){
        StringBuffer sb = new StringBuffer();
        constructUnitCollectionString(sb,uCol);
        return sb.toString();
    }

    public String battleInfoString(BattleMsg msg) {
        StringBuffer sb = new StringBuffer();
        sb.append("[BattleMsgOID:" + msg.OID() + ",BattleCmd:" + BattleCmd.name(msg.cmd()) + ",ArmiesLength:" + msg.armiesLength());
        sb.append("\n,Armies:");
        for (int i = 0; i < msg.armiesLength(); i++)
            constructArmyInfoString(sb, msg.armies(i));
        sb.append("]");
        return sb.toString();
    }


    public String pointInfoString(ControlPointMsg msg) {
        StringBuffer sb = new StringBuffer();
        sb.append("[ControlPointOID:" + msg.OID() + "Garrison:");
        if (msg.garrison()==null) sb.append("Null");
        else constructArmyInfoString(sb, msg.garrison());
        sb.append(",\nGarrisonedArmies:");
        for (int i = 0; i < msg.garrisonedArmiesLength(); i++)
            constructArmyInfoString(sb, msg.garrisonedArmies(i));
        sb.append(",\nArmies:");
        for (int i = 0; i < msg.armiesLength(); i++)
            constructArmyInfoString(sb, msg.armies(i));
        sb.append("]");
        return sb.toString();
    }


    public String pointInfoString(ControlPoint cp) {
        StringBuffer sb = new StringBuffer();
        sb.append("[ControlPointName:" + cp.getName() + ",ControlPointOID:" + cp.OID() + "Garrison:");
        constructArmyInfoString(sb, cp.getGarrison());
        sb.append(",\nGarrisonedArmies:");
        for (int i = 0; i < cp.getGarrisonedArmies().size(); i++)
            constructArmyInfoString(sb, cp.getGarrisonedArmies().get(i));
        sb.append("]");
        return sb.toString();
    }

    public String logArmyCollection(Collection<ArmyMsg> armies) {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (ArmyMsg army : armies)
            constructArmyInfoString(sb, army);
        sb.append("]");
        return sb.toString();
    }

    public String armyInfoString(ArmyMsg army) {
        StringBuffer sb = new StringBuffer();
        constructArmyInfoString(sb, army);
        return sb.toString();
    }



    private void constructArmyInfoString(StringBuffer sb, ArmyMsg army) {
//ArmyName:" + army.armyName()==null?"Null":army.armyName() + ",
        if (army==null) {
            sb.append("[NULL_ARMY]");
            return;
        }
        String color = army.color()!=null? army.color().x()+","+army.color().y()+","+army.color().z() : "null color",
        position = army.position() != null? army.position().x()+","+army.position().y()+","+army.position().z() : "null position";
        sb.append("\n[ArmyOID:" + army.OID() +",factionId:" +army.factionID()+", isNetworkPlayer:"+army.isNetworkPlayer()
                +",isPlayerFaction"+army.isPlayerFaction()+",requrieDeployment:"+army.requireDeployment()+" ,isHostile:"+army.isHostile()+
                " ,isMainArmy:"+army.isMainArmy()+ " ,controllerOID:"+army.controllerOid()+
                " ,Color:("+color+")"+" ,Position:("+position+")"+"\nUnits:");
        for (int i = 0; i < army.armyUnitsLength(); i++) {
            UnitMsg u = army.armyUnits(i);
            constructUnitInfoString(sb,u);
        }
        sb.append(",\nTiles:");
        for (int i = 0; i < army.deployableTilesLength(); i++) {
            TileMsg t = army.deployableTiles(i);
            sb.append("[tileId:" + t.tileId() +"]");
        }
        sb.append("]");
    }


    public List<UnitMsg> getArmyUnits(ArmyMsg army) {
        List<UnitMsg> units = new ArrayList<UnitMsg>();
        for (int i = 0; i < army.armyUnitsLength(); i++)
            units.add(army.armyUnits(i));
        return units;
    }

    public List<ArmyMsg> getPointGarrisonArmies(ControlPointMsg pointMsg) {
        List<ArmyMsg> armies = new ArrayList<ArmyMsg>();
        for (int i = 0; i < pointMsg.garrisonedArmiesLength(); i++)
            armies.add(pointMsg.garrisonedArmies(i));
        return armies;
    }

    public List<ArmyMsg> getPointArmies(ControlPointMsg pointMsg) {
        List<ArmyMsg> armies = new ArrayList<ArmyMsg>();
        for (int i = 0; i < pointMsg.armiesLength(); i++)
            armies.add(pointMsg.armies(i));
        return armies;
    }

    public ControlPointMsg updatePointGarrison(ControlPointMsg pointMsg, ArmyMsg garrison) {
        return createPointMsg(pointMsg.OID(), pointMsg.name(), pointMsg.cmd(), garrison, getPointGarrisonArmies(pointMsg), pointMsg.action());
    }


    public ControlPointMsg updatePointGarrisonArmies(ControlPointMsg pointMsg, Collection<ArmyMsg> garrisonArmies) {
        return createPointMsg(pointMsg.OID(), pointMsg.name(), pointMsg.cmd(), pointMsg.garrison(), garrisonArmies, pointMsg.action());
    }

    public int createPointMsg(boolean endMsg, FlatBufferBuilder fbb, ControlPoint point) {
        String controllingName = point.getControllingPlayer() == null ? "Neutral Control" : point.getControllingPlayer().getName();
        String enablingName = point.getEnablingPlayer() == null ? "Unactivated" : point.getEnablingPlayer().getName();
        int controllingNameOffset = fbb.createString(controllingName);
        int enablingNameOffset = fbb.createString(enablingName);
        long controllingOID = point.getControllingPlayer() == null ? -1 : point.getControllingPlayer().getOID().toLong();
        long enablingOID = point.getEnablingPlayer() == null ? -1 : point.getEnablingPlayer().getOID().toLong();
        addPointMsg(false, fbb, point.OID(), point.getName(), point.getGarrison(), point.getGarrisonedArmies(), DataAction.NONE);
        ControlPointMsg.addControllingOID(fbb, controllingOID);
        ControlPointMsg.addControllingName(fbb, controllingNameOffset);
        ControlPointMsg.addEnablingOID(fbb, enablingOID);
        ControlPointMsg.addEnablingName(fbb, enablingNameOffset);
        if (!endMsg) return -1;
        return ControlPointMsg.endControlPointMsg(fbb);
    }

    public static int getNodeMsg(FlatBufferBuilder fbb, PlayerNode requested) {
        int offset = fbb.offset();
        return addNodeRecursive(fbb, requested, offset);
    }

    public static int addPointBuffer(FlatBufferBuilder fbb, ControlPoint point) {
        ControlPointMsg.startControlPointMsg(fbb);
        ControlPointMsg.addOID(fbb, point.OID());
        return ControlPointMsg.endControlPointMsg(fbb);
    }

    public String printSubTree(ControlNode node){
        StringBuffer sb = new StringBuffer();
        addNodeToBuffer(node,sb);
        return sb.toString();
    }
    public void addNodeToBuffer(ControlNode node,StringBuffer sb){
        sb.append("[PlayerNode:"+node.OID()+","+node.controlName()+"]");
        if (node.controlledLength()>0) {
            sb.append("<");
            for (int i=0;i<node.controlledLength();i++)
                addNodeToBuffer(node.controlled(i),sb);
            sb.append(">");
        }
    }


    public void traverseNode(ControlNode node, Consumer<ControlNode> consumer) {
        for (int i = 0; i < node.controlledLength(); i++) {
            traverseNode(node.controlled(i), consumer);
        }
        consumer.accept(node);
    }

    public static int addNodeRecursive(FlatBufferBuilder fbb, PlayerNode playerNode, int parentOffset) {
        ControlPlayer player = playerNode.getPlayer();
        int offset = fbb.offset();
        int[] children = new int[playerNode.getChildCount()];
        int[] points = new int[0];
        String name = "Root";
        long oid = -1;
        int taxRate = 0;
        if (playerNode.getChildCount() > 0) {
            for (int i = 0; i < playerNode.getChildCount(); i++) {
                children[i] = addNodeRecursive(fbb, playerNode.getChildAt(i), offset);
            }
        }
        if (playerNode.getPlayer()!=null) {
            List<ControlPoint> playerPoints = new ArrayList(player.getControlledPoints());
            if (false && points.length > 0) {
                points = new int[playerPoints.size()];
                for (int i = 0; i < playerPoints.size(); i++) {
                    points[i] = addPointBuffer(fbb, playerPoints.get(i));
                }
            }
            name=player.getName();
            oid = player.getOID().toLong();
            taxRate = player.getTaxRate();
        }

        int controlledOffset = ControlNode.createControlledVector(fbb, children);
        int controlPointsOffset = ControlNode.createControlPointsVector(fbb, points);
        int controlNameOffset = fbb.createString(name);
        return ControlNode.createControlNode(fbb,oid , controlNameOffset, controlledOffset, controlPointsOffset,
                parentOffset, taxRate,  0);
    }

    public TacticalMsg createTacticalBattle(BattleMsg battleMsg, long subjectOid) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int battleOffset = TacticalMsg.createBattleMsgVector(fbb, battleMsg.getByteBuffer().array());
        TacticalMsg.startTacticalMsg(fbb);
        TacticalMsg.addSubjectOID(fbb, subjectOid);
        int msgOffset = TacticalMsg.endTacticalMsg(fbb);
        fbb.finish(msgOffset);
        return TacticalMsg.getRootAsTacticalMsg(fbb.dataBuffer());
    }

    public ControlPlayerMsg createControlPlayer(long subjectOid, PlayerNode playerNode) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();

        int controlNodeOffset = getNodeMsg(fbb, playerNode);
        ControlPlayerMsg.startControlPlayerMsg(fbb);
        ControlPlayerMsg.addSubjectOID(fbb, subjectOid);
        ControlPlayerMsg.addNodeMsg(fbb, controlNodeOffset);
        ControlPlayerMsg.addCmd(fbb, ControlPlayerCmd.PLAYER_CONTROL_INFO);
        int offset = ControlPlayerMsg.endControlPlayerMsg(fbb);
        fbb.finish(offset);
        return ControlPlayerMsg.getRootAsControlPlayerMsg(fbb.dataBuffer());
    }


    public List<TileMsg> getBattleMsgTiles(BattleMsg battleMsg) {
        ArrayList<TileMsg> tileList = new ArrayList<>();
        for (int i = 0; i < battleMsg.gridTilesLength(); i++)
            tileList.add(battleMsg.gridTiles(i));
        return tileList;
    }


    public BattleMsg getBattleMsg(Battle battle, byte cmd, long subjectOid) {
        return getBattleMsg(battle, cmd, subjectOid, new ArrayList<>(), new ArrayList<TileMsg>());
    }

    public BattleMsg getBattleMsg(Battle battle, byte cmd, long subjectOid, List<ArmyMsg> armies, List<TileMsg> gridTiles) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        addBattleMsg(fbb,battle,cmd,subjectOid,armies,gridTiles);
        int offset = BattleMsg.endBattleMsg(fbb);
        fbb.finish(offset);
        return BattleMsg.getRootAsBattleMsg(fbb.dataBuffer());
    }

    public Map<OID,List<ArmyMsg>> getPlayerArmies(BattleMsg msg){
        Map<OID,List<ArmyMsg>> map = new HashMap<>();
        for (int i=0;i<msg.armiesLength();i++){
            ArmyMsg a = msg.armies(i);
            OID oid = a.isMainArmy() ? OID.fromLong(a.OID()) : OID.fromLong(a.controllerOid());
            map.putIfAbsent(oid,new ArrayList<>());
            map.get(oid).add(a);
        }
        return map;
    }


    public void addBattleMsg(FlatBufferBuilder fbb,Battle battle, byte cmd, long subjectOid, List<ArmyMsg> armies, List<TileMsg> gridTiles) {
        boolean armiesPresent = armies!=null && armies.size() > 0, tilesPresent = gridTiles != null && gridTiles.size() > 0;
        int armiesVec = -1, gridTilesVec = -1;
        if (armiesPresent)
            armiesVec = BattleMsg.createArmiesVector(fbb, addExistingArmies(fbb, armies));
        if (tilesPresent)
            gridTilesVec = addExistingTiles(fbb, gridTiles);
        Point p = battle.getBattlePosition();
        BattleMsg.startBattleMsg(fbb);
        if (armiesPresent) BattleMsg.addArmies(fbb, armiesVec);
        if (tilesPresent) BattleMsg.addGridTiles(fbb, gridTilesVec);
        fbb.forceDefaults(true);
        BattleMsg.addBattlePhase(fbb,battle.getPhase().ordinal());
        fbb.forceDefaults(false);
        BattleMsg.addIsSiege(fbb,battle.isSiege());
        BattleMsg.addPosition(fbb, Vec3.createVec3(fbb, p.getX(), p.getY(), p.getZ()));
        BattleMsg.addOID(fbb, battle.getBattleOID().toLong());
        BattleMsg.addCmd(fbb, cmd);
        BattleMsg.addSubjectOID(fbb, subjectOid);
        BattleMsg.addCurrentFactionID(fbb, battle.getCurrentFactionId());
    }

    public static ByteBuffer getProtoMessage(WorldManagerClient.ExtensionMessage msg, String protoName) {
        WorldManagerClient.ExtensionMessage gridMsg = (WorldManagerClient.ExtensionMessage) msg;
        byte[] byteMsg = (byte[]) gridMsg.getProperty(protoName);
        int position = (int) gridMsg.getProperty("BufferPosition");
        ByteBuffer bb = ByteBuffer.wrap(byteMsg);
        bb.position(position);
        return bb;
    }

    public HashMap<OID, HashMap<OID, UnitMsg>> getPlayerUnits(Collection<ArmyMsg> armies){
        HashMap<OID, HashMap<OID, UnitMsg>> playerUnitsMap = new HashMap<OID, HashMap<OID, UnitMsg>>();
        for (ArmyMsg army : armies) {
            OID oid = army.isMainArmy() ? OID.fromLong(army.OID()) : OID.fromLong(army.controllerOid());
            playerUnitsMap.putIfAbsent(oid, new HashMap<>());
            getArmyUnits(army).forEach(u -> playerUnitsMap.get(oid).put(OID.fromLong(u.OID()), u));
        }
        return playerUnitsMap;
    }

    public HashMap<OID, HashMap<OID, Integer>> getPlayerUnitsQuantities(Collection<ArmyMsg> armies){
        HashMap<OID, HashMap<OID, Integer>> playerUnitsMap = new HashMap<OID, HashMap<OID, Integer>>();
        for (ArmyMsg army : armies) {
            OID oid = army.isMainArmy() ? OID.fromLong(army.OID()) : OID.fromLong(army.controllerOid());
            playerUnitsMap.putIfAbsent(oid, new HashMap<>());
            getArmyUnits(army).forEach(u -> playerUnitsMap.get(oid).put(OID.fromLong(u.OID()), 1));
        }
        return playerUnitsMap;
    }

    public Collection<ArmyMsg> clearDeadUnits(Collection<ArmyMsg> armies){
        List<ArmyMsg> updated = new ArrayList<>();
        armies.forEach(a->updated.add(clearDeadUnits(a)));
        return updated;
    }

    public ArmyMsg clearDeadUnits(ArmyMsg army) {
        List<UnitMsg> liveUnits = new ArrayList<>();
        for (int i = 0; i < army.armyUnitsLength(); i++)
            if (army.armyUnits(i).HP() > 0)
                liveUnits.add(army.armyUnits(i));
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int offset = addExistingArmyWithUnits(fbb, army, liveUnits);
        fbb.finish(offset);
        return ArmyMsg.getRootAsArmyMsg(fbb.dataBuffer());
    }

    public Collection<ArmyMsg> getDeadUnits(Collection<ArmyMsg> armies){
        List<ArmyMsg> updated = new ArrayList<>();
        armies.forEach(a->updated.add(getDeadUnits(a)));
        return updated;
    }

    public ArmyMsg getDeadUnits(ArmyMsg army) {
        List<UnitMsg> liveUnits = new ArrayList<>();
        for (int i = 0; i < army.armyUnitsLength(); i++)
            if (army.armyUnits(i).HP() <= 0)
                liveUnits.add(army.armyUnits(i));
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int offset = addExistingArmyWithUnits(fbb, army, liveUnits);
        fbb.finish(offset);
        return ArmyMsg.getRootAsArmyMsg(fbb.dataBuffer());
    }

    public BattleMsg createEndTurn(Battle battle) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        addBattleMsg(fbb,battle, BattleCmd.PLAYER_ENDED_TURN,-1, null, null);
        fbb.finish(BattleMsg.endBattleMsg(fbb));
        return BattleMsg.getRootAsBattleMsg(fbb.dataBuffer());
    }



    public static void sendExtensionFlatbuffer(ByteBuffer bb, String bufferName, String extension_subtype, OID targetOID, OID subjectOID) {
        HashMap<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", extension_subtype);
        props.put("BufferPosition", bb.position());
        props.put(bufferName, bb.array());
        WorldManagerClient.TargetedExtensionMessage playerMsg = new WorldManagerClient.TargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
                targetOID, subjectOID, true, props);
        Engine.getAgent().sendBroadcast(playerMsg);
    }


    public static void sendMessageFlatbuffer(ByteBuffer bb, MessageType msgType, String bufferName) {
        WorldManagerClient.ExtensionMessage props = new WorldManagerClient.ExtensionMessage();
        props.setMsgType(msgType);
        props.setProperty("BufferPosition", bb.position());
        props.setProperty(bufferName, bb.array());
        Engine.getAgent().sendBroadcast(props);
    }

    public  static void sendLongExtensionFlatbuffer(ByteBuffer bb,  String bufferName, String extension_subtype, OID targetOID, OID subjectOID) {
        HashMap<String, Serializable> props = new HashMap<String, Serializable>();
        props.put("ext_msg_subtype", extension_subtype);
        props.put("BufferPosition", bb.position());
        props.put(bufferName, bb.array());
        WorldManagerClient.LongTargetedExtensionMessage playerMsg = new WorldManagerClient.LongTargetedExtensionMessage(WorldManagerClient.MSG_TYPE_EXTENSION,
                targetOID, subjectOID, true, props);
        playerMsg.setExtensionType(extension_subtype);
        playerMsg.setMsgType(WorldManagerClient.MSG_TYPE_EXTENSION);
        Engine.getAgent().sendBroadcast(playerMsg);
    }

    public ControlPointDatabase createPointDatabase(Collection<ControlPoint> values) {
        List<ControlPoint> points = new ArrayList<>();
        points.addAll(values);
        FlatBufferBuilder fbb = new FlatBufferBuilder(1000);
        int[] pointsOffsets = new int[points.size()];
        for (int i =0; i<points.size();i++){
            ControlPoint cp = points.get(i);
            synchronized (cp) {
                pointsOffsets[i] = createPointMsg(true,fbb, cp);
            }
        }
        int pointVecOffset = ControlPointDatabase.createControlPointsVector(fbb,pointsOffsets);
        ControlPointDatabase.startControlPointDatabase(fbb);
        ControlPointDatabase.addControlPoints(fbb,pointVecOffset);
        int offset = ControlPointDatabase.endControlPointDatabase(fbb);
        fbb.finish(offset);
        return ControlPointDatabase.getRootAsControlPointDatabase(fbb.dataBuffer());
    }

}
