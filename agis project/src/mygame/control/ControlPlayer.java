package mygame.control;

import atavism.agis.objects.Faction;
import atavism.agis.objects.PlayerFactionData;
import atavism.agis.plugins.AgisMobClient;
import atavism.agis.plugins.FactionPlugin;
import atavism.server.engine.Engine;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Logger;
import mygame.Player;
import mygame.Proto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * Created by meme on 7/5/2016.
 */
public class ControlPlayer extends Player {
    private Set<ControlPoint> controlledPoints = new HashSet<ControlPoint>();
    private Set<ControlPoint> enabledPoints = new HashSet<ControlPoint>();
    protected static final Logger log = new Logger("ControlPlayer");
    protected static final Proto proto = new Proto();
    private PlayerNode playerNode;
    private int accumulatedGold = 0;
    private int taxRate = 10;

    public ControlPlayer(long playerOid, String playerName) {
        super(playerOid, playerName);
    }

    public ControlPlayer(OID playerOid, String playerName) {
        super(playerOid, playerName);
    }


    public Set<ControlPoint> getControlledPoints() {
        return controlledPoints;
    }

    public PlayerNode getPlayerNode() {
        return playerNode;
    }

    public void setPlayerNode(PlayerNode node) {
        playerNode = node;
    }


    public int getAccumulatedGold() {
        return accumulatedGold;
    }

    public void setAccumulatedGold(int accumulatedGold) {
        this.accumulatedGold = accumulatedGold;
    }

    public void setTax(int rate) {
        taxRate = rate;
    }

    public boolean isPlayerInTree(ControlPlayer other) {
        if (playerNode == null || other.playerNode == null)
            return false;
        PlayerNode sharedAncesdtor = (PlayerNode) playerNode.getSharedAncestor(other.playerNode);
        if (sharedAncesdtor == null || sharedAncesdtor.isRoot()) return false;
        else return true;
    }

    public int getTaxRate() {
        return taxRate;
    }

    public Set<ControlPoint> getEnabledPoints() {
        return enabledPoints;
    }

    public void controlPlayer(ControlPlayer controlled) {
        playerNode.add(controlled.playerNode);
        controlled.playerNode.setParent(playerNode);
    }

    public boolean canEnable(ControlPoint point) {
        return true;
    }

    public void enablePoint(ControlPoint controlPoint) {
        enabledPoints.add(controlPoint);
        controlPoint.setEnablingPlayer(this);
    }

    public void disablePoint(ControlPoint controlPoint) {
        enabledPoints.remove(controlPoint);
        if (!controlPoint.getEnablingPlayer().equals(this)) return;
        controlPoint.setEnablingPlayer(null);
    }

    public void discapturePoint(ControlPoint controlPoint) {
        controlledPoints.remove(controlPoint);

    }

    public void addControlledPoint(ControlPoint cp) {
        controlledPoints.add(cp);
    }

    public void addEnabledPoint(ControlPoint cp) {
        enabledPoints.add(cp);
    }

    public void capturePoint(ControlPoint controlPoint, BiConsumer<ControlPlayer, ControlPlayer> controlPlayerConsumer) {
        controlledPoints.add(controlPoint);
        controlPoint.resetGarrison();
        ControlPlayer controller = controlPoint.getControllingPlayer(), enabler = controlPoint.getEnablingPlayer();
        controlPoint.setControllingPlayer(this);
        boolean controllerPresent = controller != null, enablerPresent = enabler != null;
        String controllerName = controllerPresent ? " controller:" + controller.getName().toString() : "No Controller",
                enablerName = enablerPresent ? " enabler:" + enabler.getName().toString() : "No Enabler";
        log.debug("player capturePoint, pointInfo before:" + proto.pointInfoString(controlPoint) + " player faction:" + getFaction()
                + controllerName + enablerName + " capturing player:" + getOID().toLong() + " faction:" + getFaction());
        if (controllerPresent) {
            controller.discapturePoint(controlPoint);
            if (controller.getControlledPoints().size() == 0) {
                log.debug("old controller lost control of his last point, tree before:" + playerNode.getRoot().printSubTree());
                controlPlayer(controller);
                controlPlayerConsumer.accept(this, controller);
                log.debug("old controller lost control of his last point, tree after:" + playerNode.getRoot().printSubTree());
            }
        }
        if (enablerPresent) {

            if (controllerPresent) {
                boolean playerInTree = enabler.isPlayerInTree(controller), controllerIsEnabler = controller.getOID().equals(enabler.getOID());
                log.debug("controller present, is enabler in controller's tree:" + playerInTree + " controllerIsEnabler:" + controllerIsEnabler);
                if (playerInTree || controllerIsEnabler) {
                    log.debug("enabler is the old controller, or controller has enabler in tree");
                } else {
                    log.debug("enabler exists");
                    if (enabler.getEnabledPoints().size() <= 1) {
                        log.debug(" enabler must stay, current tree:" + playerNode.getRoot().printSubTree());
                        controlPlayer(enabler);
                        controlPlayerConsumer.accept(this, enabler);
                        log.debug(" enabler must stay, after tree change:" + playerNode.getRoot().printSubTree());
                        return;
                    } else if (controller.canEnable(controlPoint)) {
                        log.debug("enabler can go and controller can enable");
                        enabler.disablePoint(controlPoint);
                        controller.enablePoint(controlPoint);
                    }
                }
            } else {
                log.debug("enabler was present but controller wasn't, so enabler stays");
            }
        } else {
            log.debug("no enabler");
            if (controllerPresent && controller.canEnable(controlPoint)) {
                log.debug("controller can enable");
                controller.enablePoint(controlPoint);
            } else {
                log.debug("controller is missing or can't enable");
                if (canEnable(controlPoint)) {
                    log.debug("capturing player can enable");
                    enablePoint(controlPoint);
                }
            }
        }

    }

    public boolean inControl(ControlPlayer isControlled) {
        return isControlled.playerNode.isNodeAncestor(playerNode);
    }


    public void addOwnFaction() {
        OID playerOid = getOID();
        HashMap<String, Serializable> props = new HashMap<>();
        String name = "faction:" + getName(), subtitle = "";
        props.put("playerOid", playerOid.toLong());
        props.put("name", name);
        props.put("subtitle", subtitle);
        props.put("stanceCount", 1);
        props.put("defaultStance", FactionPlugin.Hated);
        props.put("isPublic", true);
        props.put("faction0ID", 1);
        props.put("faction0Stance", FactionPlugin.Hated);
        WorldManagerClient.ExtensionMessage spawn = new WorldManagerClient.ExtensionMessage(AgisMobClient.MSG_TYPE_CREATE_FACTION, playerOid, props);
        Engine.getAgent().sendBroadcast(spawn);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int newFactionId = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "faction");
        log.debug("added player faction, player:" + getOID().toLong() + " new faction:" + newFactionId);
        PlayerFactionData humansPfd = Faction.addFactionToPlayer(playerOid, new Faction(1, "Humans", "", 1), newFactionId);
        HashMap<Integer, PlayerFactionData> pfdMap = (HashMap) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "factionData");
        if (humansPfd != null) {
            humansPfd.setReputation(FactionPlugin.HatedRep);
            pfdMap.put(1, humansPfd);
        }
        EnginePlugin.setObjectProperty(getOID(), WorldManagerClient.NAMESPACE, "factionData", pfdMap);
        setFaction(newFactionId);
    }
}
