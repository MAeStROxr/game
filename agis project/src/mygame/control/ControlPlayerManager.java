package mygame.control;

import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.ControlClient;
import atavism.agis.plugins.FactionClient;
import atavism.agis.plugins.FactionPlugin;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import mygame.PlayerManager;
import mygame.proto.ControlNode;
import mygame.proto.ControlPlayerCmd;
import mygame.proto.ControlPlayerMsg;

import javax.swing.tree.DefaultTreeModel;

/**
 * Created by meme on 7/5/2016.
 */
public class ControlPlayerManager extends PlayerManager<ControlPlayer> {
    public PlayerNode root = new PlayerNode( "root players", null);
    private DefaultTreeModel controlPointsTree = new DefaultTreeModel(root);

    public ControlPlayerManager() {
    }




    @Override
    public ControlPlayer addPlayer(OID playerOID, ControlPlayer player) {
        ControlPlayer cp = super.addPlayer(playerOID, player);
        if (cp == null) {
            createPlayerNode(player);
        }
        return cp;
    }

    @Override
    public void clear(){
        root = new PlayerNode( "root players", null);
        controlPointsTree = new DefaultTreeModel(root);
        super.clear();
    }

    @Override
    public ControlPlayer addPlayer(long playerOID, ControlPlayer player) {
        return addPlayer(OID.fromLong(playerOID),player);
    }


    public PlayerNode getRoot() {
        return root;
    }


    public PlayerNode createPlayerNode(ControlPlayer player) {
        if (player.getPlayerNode() == null) {
            PlayerNode pNode = new PlayerNode( player.getName(), player);
            player.setPlayerNode(pNode);
            root.add(pNode);
            return pNode;
        }
        return player.getPlayerNode();
    }

    public static String printControlNodeTree(ControlNode controlNode) {
        StringBuffer sb = new StringBuffer();
        addControlNode(sb, controlNode);
        return sb.toString();
    }

    private static void addControlNode(StringBuffer sb, ControlNode controlNode) {
        sb.append("[" + controlNode.OID() + "," + controlNode.controlName() + "]");
        if (controlNode.controlledLength() > 0) {
            sb.append("[");
            for (int i = 0; i < controlNode.controlledLength(); i++)
                addControlNode(sb, controlNode.controlled(i));
            sb.append("]");
        }
    }

    public void processPlayerMsg(ControlPlayerMsg msg) {
        OID oid = OID.fromLong(msg.subjectOID());
        //log.debug("controlplayermanager: -cmd" + msg.cmd());
        switch (msg.cmd()) {
            case ControlPlayerCmd.PLAYER_CONTROL_INFO:
                if (players.containsKey(oid)) {
                    ControlClient.sendTargetedControlPlayerMsg(proto.createControlPlayer(msg.subjectOID(), root),OID.fromLong(msg.subjectOID()));
                    //log.debug("player already exists, sending:\n"+root.printSubTree());
                } else {
                    ControlPlayer p = addPlayer(oid, new ControlPlayer(msg.subjectOID(), msg.subjectName()));
                    ControlPlayerMsg returnMsg = proto.createControlPlayer(msg.subjectOID(), root);
                    ControlClient.sendControlPlayerMsg(returnMsg,players.keySet(),(playerOid,controlPlayerMsg)-> {
                        //log.debug("sending player tree to:"+playerOid);
                        controlPlayerMsg.mutateSubjectOID(playerOid.toLong());
                    });
                }
                loginPlayer(getPlayer(msg.subjectOID()));
                break;
            case ControlPlayerCmd.PLAYER_TAX:
                ControlNode nodeMsg = msg.nodeMsg();
                ControlPlayer player = players.get(msg.subjectOID());
                player.setTax(nodeMsg.taxRate());
                ControlClient.sendTargetedControlPlayerMsg(msg,OID.fromLong(msg.subjectOID()));
                break;
            case ControlPlayerCmd.PLAYER_DECLARE_WAR:
                OID subject = OID.fromLong(msg.subjectOID()), target =OID.fromLong(msg.targetOID());
                int targetFaction = (Integer) EnginePlugin.getObjectProperty(target, WorldManagerClient.NAMESPACE, "faction");
                FactionClient.alterReputation(subject,targetFaction,FactionPlugin.HatedRep);
                break;
        }
    }

    int maxPlayerControlledPoints = 10;

    public boolean canCapture(long playerOid,ControlPoint point) {
        ControlPlayer capturingPlayer = getPlayer(playerOid);
        ControlPlayer controlling = point.getControllingPlayer(),enabling = point.getEnablingPlayer();
        log.debug("ControlPlayerManager: checking can capture point");
        if (controlling != null) {
            PlayerNode controllerAncestor = (PlayerNode)controlling.getPlayerNode().getSharedAncestor(capturingPlayer.getPlayerNode());
            if (controllerAncestor!=null && !controllerAncestor.isRoot()) {
                log.debug("can't capture, ancestor isn't null and isn't root, shared ancestor name:"+controllerAncestor.getPlayer().getName()+" oid:"+controllerAncestor.getPlayer().getOID().toLong()+"\n"+
                        controllerAncestor.printSubTree());
                return false;
            }
            log.debug("ControlPlayerManager capturing player:"+capturingPlayer.toString()+" and controlling:"+controlling.toString());
            boolean isRoot = controllerAncestor.isRoot(), equalsRoot = controllerAncestor == root;
            log.debug("isRoot:"+isRoot+" equalsRoot:"+equalsRoot);
        }
        if (capturingPlayer.getControlledPoints().size() > maxPlayerControlledPoints) {
            log.debug("capturing player has more than " + maxPlayerControlledPoints);
            return false;
        }
        return true;
    }

    public boolean setTax(long playerOid, int taxRate) {
        ControlPlayer p =getPlayer(playerOid);
        p.setTax(taxRate);
        return true;
    }


    public void taxTree(){
        PlayerNode leaf = (PlayerNode)root.getFirstLeaf();
        while (leaf!=null){
            if (leaf==root) break;
            leaf.payTaxes();
            leaf=(PlayerNode)leaf.getNextLeaf();
        }
        for (ControlPlayer p : players.values()) {
            log.debug("player:"+p.getOID()+" "+p.getName()+" got taxes:"+p.getAccumulatedGold());
            if (isLoggedIn(p)) {
                AgisInventoryClient.alterCurrency(p.getOID(), 1, p.getAccumulatedGold());
                p.setAccumulatedGold(0);
            }
        }
    }

    public void marshalTree(ControlPlayerMsg playerMsg) {
        for (int i=0;i<playerMsg.nodeMsg().controlledLength();i++)
            recursiveMarshalNode(playerMsg.nodeMsg().controlled(i));
    }

    public void recursiveMarshalNode(ControlNode node){
        addPlayer(node.OID(), new ControlPlayer(node.OID(),node.controlName()));
        ControlPlayer player = getPlayer(node.OID());
        PlayerNode treeNode = player.getPlayerNode();
        root.add(treeNode);
        treeNode.removeAllChildren();
        for (int i=0;i<node.controlledLength();i++){
            ControlNode childNode = node.controlled(i);
            recursiveMarshalNode(childNode);
            ControlPlayer child = getPlayer(childNode.OID());
            player.controlPlayer(child);
        }
    }
}
