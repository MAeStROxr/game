package mygame.control;

import atavism.server.engine.OID;
import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.Proto;
import mygame.proto.ArmyMsg;
import mygame.tactical.BattleManager;
import mygame.tactical.UnitStats;

import java.util.*;
import java.util.stream.Collectors;
public class ControlPoint {

	private long oid;
	private String name;
	private boolean activated = false;
	private int revenue=10;
	private ControlPlayer controllingPlayer;
	private ControlPlayer enablingPlayer;
	private List<ArmyMsg> siegingArmies = new ArrayList<>();
	private ArmyMsg garrison;
	private Map<Long, ArmyMsg> garrisonedGenerals = new HashMap<Long, ArmyMsg>();
	private static Proto proto = new Proto();
	private boolean underSiege = false;
	private long capturingSieger;
	private static int unitOIDs = 6600;
	FlatBufferBuilder fbb = new FlatBufferBuilder();
	private static final Logger log= new Logger("ControlPoint");

	public ControlPoint( long controlPointOid, String name) {
		this.oid = controlPointOid;
		this.name = name;
		Random rand = new Random();
		if (rand.nextInt(20)>0)
			fillRandomGarrison();
		else resetGarrison();
	}

	public ControlPoint( long controlPointOid, String name, ArmyMsg unitGarrison, Map<Long, ArmyMsg> garrisonedGenerals) {
		this.oid = controlPointOid;
		this.name = name;
		this.garrison = unitGarrison;
		this.garrisonedGenerals = garrisonedGenerals;
	}

	public void resetGarrison(){
		fbb = new FlatBufferBuilder();
		createGarrison(new int[]{});
	}


	public void createGarrison(int[] units){
		int nameOffset = fbb.createString(name);
		int unitsVec = ArmyMsg.createArmyUnitsVector(fbb, units);
		ArmyMsg.startArmyMsg(fbb);
		ArmyMsg.addOID(fbb, oid);
		ArmyMsg.addArmyName(fbb, nameOffset);
		ArmyMsg.addArmyUnits(fbb, unitsVec);
		fbb.forceDefaults(true);
		ArmyMsg.addIsMainArmy(fbb,true);
		ArmyMsg.addFactionID(fbb,0);
		ArmyMsg.addIsNetworkPlayer(fbb,false);
		ArmyMsg.addIsPlayerFaction(fbb,false);
		ArmyMsg.addRequireDeployment(fbb,true);
		if (controllingPlayer!=null) ArmyMsg.addControllerOid(fbb,controllingPlayer.getOID().toLong());
		else ArmyMsg.addControllerOid(fbb,-1);
		fbb.forceDefaults(false);
		fbb.finish(ArmyMsg.endArmyMsg(fbb));
		garrison = ArmyMsg.getRootAsArmyMsg(fbb.dataBuffer());
	}

	public void fillRandomGarrison(){
		fbb = new FlatBufferBuilder();
		Map<Integer,UnitStats> unitStats = BattleManager.getUnitStats();
		Random rand = new Random();
		int unitAmount = rand.nextInt(3)+1, turretAmount = rand.nextInt(3)+1, count=0;
		int[] offsetArray=new int[unitAmount+turretAmount+1];
		for (int i =0; i<unitAmount;i++){
			int unitPrefab = rand.nextInt(6)+5;
			offsetArray[count] = proto.addUnitMsg(fbb,unitOIDs++,"unit",-1,unitPrefab,count,unitStats.get(unitPrefab).HP);
			count++;
		}
		for (int i=0;i<turretAmount;i++){
			int turretPrefab = rand.nextInt(2)+14;
			offsetArray[count] = proto.addUnitMsg(fbb,unitOIDs++,"unit",-1,turretPrefab,count,unitStats.get(turretPrefab).HP);
			count++;
		}
		offsetArray[count] = proto.addUnitMsg(fbb,unitOIDs++,"unit",-1,13,count,unitStats.get(13).HP);
		createGarrison(offsetArray);
	}

	public long OID() {
		return oid;
	}
	public String getName(){
		return name;
	}

	public void payRevenue() {
		if (enablingPlayer != null) {
			int newAccumulated = enablingPlayer.getAccumulatedGold()+revenue;
			log.debug("updating accumulated of enabling player, newAccumulated:"+newAccumulated+
							" playerOid:"+enablingPlayer.getOID().toLong());
			enablingPlayer.setAccumulatedGold(newAccumulated);
		}
		if (controllingPlayer != null) {
			int newAccumulated = controllingPlayer.getAccumulatedGold()+revenue/2;
			log.debug("updating accumulated of controlling player, newAccumulated:"+newAccumulated+
					" playerOid:"+controllingPlayer.getOID().toLong());
			controllingPlayer.setAccumulatedGold(newAccumulated);
		}
	}

	
	public int getRevenue(){ return revenue;}

	public void setGarrison(ArmyMsg garrisonUnits) {
		garrison = garrisonUnits;

	}

	public ArmyMsg getGarrison() {
		return garrison;
	}



	public ControlPlayer getControllingPlayer() {
		return controllingPlayer;
	}
	public void setControllingPlayer(ControlPlayer player){
		controllingPlayer = player;
		getGarrison().mutateControllerOid(player.getOID().toLong());
	}
	public ControlPlayer getEnablingPlayer() {
		return enablingPlayer;
	}
	public void setEnablingPlayer(ControlPlayer  player){ enablingPlayer = player; }
	public void addArmyGarrison(ArmyMsg army) {
		log.debug("adding army garrison: armyOid-"+army.OID()+" armyObj"+army);
		garrisonedGenerals.putIfAbsent(army.OID(), army);

	}

	public void setArmyGarrison(ArmyMsg army) {
		log.debug("setting army garrison: armyOid-"+army.OID()+" armyObj"+army);
		garrisonedGenerals.put(army.OID(), army);

	}

	public void removeArmyGarrison(long armyOID) {
		garrisonedGenerals.remove(armyOID);

	}


	public void addListener(OID oid){
	}


	public List<ArmyMsg> getGarrisonedArmies(){
		ArrayList<ArmyMsg> list = new ArrayList<>();
		garrisonedGenerals.forEach((pOid,a)->{if
				(a==null)
			log.debug("garrison army is null:"+pOid);
		});
		list.addAll(garrisonedGenerals.values());
		return list;
	}
	public List<ArmyMsg> getGarrisonTotal() {
		List<ArmyMsg> armies = new ArrayList<ArmyMsg>();
		if (garrison.armyUnitsLength() > 0)
			armies.add(garrison);
		armies.addAll(garrisonedGenerals.values().stream().sorted((u1, u2) -> u1.dataID() - u2.dataID())
				.collect(Collectors.toList()));
		return armies;
	}
	public boolean isUnderSiege(){
		return underSiege;
	}

	public boolean requireSiege(){
		return (garrison.armyUnitsLength()>0 || garrisonedGenerals.values().stream().anyMatch(general->general.armyUnitsLength()>0));
	}

	public long getCapturingSieger(){ return capturingSieger;}

	public void siege(long capturer,List<ArmyMsg> siegingArmies){
		this.siegingArmies.addAll(siegingArmies);
		this.capturingSieger = capturer;
		underSiege = true;
	}

	public void siegeCompleted(){
		siegingArmies.clear();
		underSiege=false;
	}


	public void updateGarrisonedArmy(ArmyMsg updatedGarrisonArmy) {
		garrisonedGenerals.put(updatedGarrisonArmy.OID(),updatedGarrisonArmy);
	}

	public void updateGarrison(ArmyMsg newUnitsGarrison) {
		garrison = proto.createExistingArmyWithUnits(garrison,proto.getArmyUnits(newUnitsGarrison));
	}
}