package mygame.control;

import atavism.agis.plugins.*;
import atavism.agis.util.EventMessageHelper;
import atavism.server.engine.Engine;
import atavism.server.engine.OID;
import atavism.server.objects.ObjectTracker;
import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.Proto;
import mygame.proto.*;
import mygame.tactical.BattleManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.prefs.Preferences;

public class ControlPointManager {

    protected static final Logger log = new Logger("ControlPointManager");
    HashMap<Long, ControlPoint> points = new HashMap<>();
    TimedTasks tasksRunnable;
    Thread tasksThread;
    private static final Proto proto = new Proto();
    ControlPlayerManager playerManager;
    public String pointsDBPath = "ControlPointDatabase";
    public String treeDBPath = "ControlTreeDatabase";
    private Preferences prefs;
	
    public ControlPointManager() {
        prefs = Preferences.userRoot().node(this.getClass().getName());
        tasksRunnable = new TimedTasks();

    }
	
    public void startTasks(){
        if (tasksThread != null) tasksRunnable.setKeepRunning(false);
        tasksRunnable = new TimedTasks();
        tasksThread= new Thread(tasksRunnable);
        tasksThread.start();
    }

    public void loadControl(){
        Path pointsPath = Paths.get(pointsDBPath), treePath = Paths.get(treeDBPath);
        File pointsFile = pointsPath.toFile(), treeFile = treePath.toFile();
        if (!pointsFile.isFile() || !treeFile.isFile()){
            log.debug("One or more database file missing");
            return;
        }
        try {
            byte[] pointsArray = Files.readAllBytes(pointsPath), treeArray = Files.readAllBytes(treePath);
            int pointsBufferPosition = prefs.getInt("pointsBufferPosition", 0), treeBufferPosition = prefs.getInt("treeBufferPosition", 0);
            ByteBuffer pointsBuffer = ByteBuffer.wrap(pointsArray), treeBuffer = ByteBuffer.wrap(treeArray);
            pointsBuffer.position(pointsBufferPosition);
            treeBuffer.position(treeBufferPosition);
            ControlPointDatabase pointsDB = ControlPointDatabase.getRootAsControlPointDatabase(pointsBuffer);
            ControlPlayerMsg treeDB = ControlPlayerMsg.getRootAsControlPlayerMsg(treeBuffer);
            for (int i=0;i<pointsDB.controlPointsLength();i++) {
                ControlPointMsg p = pointsDB.controlPoints(i);
                if (p.controllingOID()>0) log.debug("\nloaded Control Point:"+proto.pointInfoString(pointsDB.controlPoints(i)));
            }
            log.debug("\nloaded Tree:"+proto.printSubTree(treeDB.nodeMsg()));
            playerManager.marshalTree(treeDB);
            marshalPoints(pointsDB);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void marshalPoints(ControlPointDatabase pointDatabase) {
        for (int i=0;i<pointDatabase.controlPointsLength();i++){
            ControlPointMsg pointMsg = pointDatabase.controlPoints(i);
            Map<Long, ArmyMsg> garrisonedGenerals = new HashMap<>();
            for (ArmyMsg a : proto.getPointGarrisonArmies(pointMsg))
                garrisonedGenerals.put(a.OID(),a);
            ControlPoint newPoint = new ControlPoint(pointMsg.OID(),pointMsg.name(),pointMsg.garrison(),garrisonedGenerals);
            ControlPlayer controller = playerManager.getPlayer(pointMsg.controllingOID()), enabler = playerManager.getPlayer(pointMsg.enablingOID());
            if (controller != null) {
                newPoint.setControllingPlayer(controller);
                controller.addControlledPoint(newPoint);
            }
            if (enabler != null) {
                newPoint.setEnablingPlayer(enabler);
                enabler.addEnabledPoint(newPoint);
            }
            points.put(newPoint.OID(),newPoint);
        }
    }


    public void persistControl(){
        ControlPointDatabase pointsDB = proto.createPointDatabase(points.values());
        ControlPlayerMsg treeDB = proto.createControlPlayer(-666,playerManager.getRoot());
        for (int i=0;i<pointsDB.controlPointsLength();i++) {
            ControlPointMsg p = pointsDB.controlPoints(i);
            if (p.controllingOID()>0) log.debug("persisting Control Point:" + proto.pointInfoString(p));
        }
        log.debug("\npersisintg Tree:"+proto.printSubTree(treeDB.nodeMsg()));
        ByteBuffer pointsBuffer = pointsDB.getByteBuffer(), treeBuffer = treeDB.getByteBuffer();
        int pointsBufferPosition = pointsBuffer.position(), treeBufferPosition = treeBuffer.position();
        try {
            persistBytesToFile(pointsBuffer.array(),pointsDBPath);
            persistBytesToFile(treeBuffer.array(),treeDBPath);
            prefs.putInt("pointsBufferPosition",pointsBufferPosition);
            prefs.putInt("treeBufferPosition",treeBufferPosition);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void persistBytesToFile(byte[] array, String filename) throws IOException {
        FileOutputStream fos = new FileOutputStream(filename);
        fos.write(array);
        fos.close();
    }

    public void setPlayerManager(ControlPlayerManager playerManager) {
        this.playerManager = playerManager;
    }

    public void processPointMsg(ControlPointMsg pointMsg) {
        ControlPointMsg returnMsg = null;
        ControlPoint cp = getPoint(pointMsg.OID());
        boolean targeted = false;
        BiConsumer<OID, ControlPointMsg> changer = null;
        //log.debug("entering processPointMsg:\n"+proto.pointInfoString(pointMsg));
        switch (pointMsg.cmd()) {
            case PointCmd.POINT_INFO:
                returnMsg = pointGetInfo(pointMsg, cp);
                targeted = true;
                break;
            case PointCmd.POINT_UNIT_GARRISON:
                returnMsg = updateUnitGarrison(pointMsg, cp);

                break;
            case PointCmd.POINT_ARMY_GARRISON:
                returnMsg = updateArmiesGarrison(pointMsg, cp);
                break;
            case PointCmd.POINT_ENABLE:
                returnMsg = enablePoint(pointMsg, cp);
                break;
            case PointCmd.POINT_CAPTURE:
                returnMsg = capturePoint(pointMsg, cp);
                if (returnMsg.captured() == true) {
                    changer = (oid, msg) -> {
                        boolean canCapture = playerManager.canCapture(oid.toLong(), cp);
                        msg.mutateCapturable(canCapture);
                        msg.mutateSubjectOID(oid.toLong());
                        log.debug("sending targeted capture msg: playeroid-" + oid.toLong() + " capturable-" + canCapture + " msgCaptureable-" + msg.capturable());
                    };
                    targeted = false;
                } else targeted = true;
                break;
            case PointCmd.POINT_SIEGE:

                synchronized (cp) {
                    boolean canCapture = playerManager.canCapture(pointMsg.subjectOID(), cp);
                    boolean canSiege = cp.requireSiege() && !cp.isUnderSiege();
                    log.debug("Siege point recieved, canCapture:"+canCapture+" canSiege:"+canSiege);
                    if (canCapture && canSiege) {
                        BattleMsg msg = siegePoint(pointMsg, cp);
                        log.debug("sending siegeMsg pointOid:" + msg.pointOID() + " subject:" + msg.subjectOID() +
                                " battleMsg:" + proto.battleInfoString(msg));
                        proto.sendMessageFlatbuffer(msg.getByteBuffer(), TacticalClient.MSG_TYPE_BATTLE_COMMAND, "BattleMsg");
                    }
                }
                return;
            case PointCmd.POINT_SIEGE_RESULT:
                returnMsg = siegeResult(pointMsg, cp);
                changer = (oid, msg) -> {
                    boolean canCapture = playerManager.canCapture(oid.toLong(), cp);
                    msg.mutateCapturable(canCapture);
                    msg.mutateSubjectOID(oid.toLong());
                    log.debug("sending targeted siege result msg: playeroid-" + oid.toLong() + " capturable-" + canCapture + " msgCaptureable-" + msg.capturable());
                };
                targeted = false;
                break;
        }
        if (returnMsg == null) return;
        //log.debug("sending pointMsg pointOid:" + returnMsg.OID() + " msg:\n" + proto.pointInfoString(returnMsg));
        if (targeted)
            ControlClient.sendTargetedControlPointMsg(returnMsg, OID.fromLong(returnMsg.subjectOID()));
        else {
            //log.debug("point listeners size:" + cp.getListeners().size() + " listeners:" + cp.logListeners() + PointCmd.name(returnMsg.cmd()));
            if (changer == null)
                ControlClient.sendControlPointMsg(returnMsg, playerManager.getsPlayerSet());
            else ControlClient.sendControlPointMsg(returnMsg, playerManager.getsPlayerSet(), changer);
        }
    }


    public ControlPointMsg updateUnitGarrison(ControlPointMsg controlMsg, ControlPoint point) {
        List<UnitMsg> msgGarrison = proto.getArmyUnits(controlMsg.garrison());
        List<UnitMsg> pointGarrison = proto.getArmyUnits(point.getGarrison());
        ControlPointMsg returnMsg = controlMsg;
        log.debug("Update unit garrison-before:" + PointCmd.name(controlMsg.cmd()) + "DataACtion:" + DataAction.name(controlMsg.action()) +
                " Garrison:" + proto.armyInfoString(point.getGarrison()) + " MsgArmy:" + proto.armyInfoString(controlMsg.garrison()));
        switch (controlMsg.action()) {
            case DataAction.REMOVE:
                pointGarrison.removeIf(u -> {
                    for (UnitMsg unit : msgGarrison)
                        if (unit.OID() == u.OID())
                            return true;
                    return false;
                });
                break;
            case DataAction.ADD:
                pointGarrison.addAll(msgGarrison);
                break;
            case DataAction.REPLACE:
                pointGarrison = msgGarrison;
        }
        ArmyMsg garrison =proto.createExistingArmyWithUnits(point.getGarrison(),pointGarrison);
        point.setGarrison(garrison);
        returnMsg = proto.updatePointGarrison(controlMsg, garrison);
        log.debug("Update unit garrison-after:" + PointCmd.name(controlMsg.cmd()) +
                " Garrison:" + proto.armyInfoString(point.getGarrison()) + " MsgArmy:" + proto.armyInfoString(returnMsg.garrison()));
        return returnMsg;
    }

    public ControlPointMsg updateArmiesGarrison(ControlPointMsg pointMsg, ControlPoint point) {
        String msgArmiesString = "";
        for (int i = 0; i < pointMsg.garrisonedArmiesLength(); i++)
            msgArmiesString += proto.armyInfoString(pointMsg.garrisonedArmies(i));
        log.debug("Update armies msg garrison-before:" + PointCmd.name(pointMsg.cmd()) + "DataACtion:" + DataAction.name(pointMsg.action()) +
                proto.pointInfoString(point) + " MsgArmies:" + msgArmiesString);
        switch (pointMsg.action()) {
            case DataAction.REMOVE:
                Collection<ArmyMsg> armies = point.getGarrisonedArmies(),removeArmiesMsg = proto.getPointGarrisonArmies(pointMsg),removeLocal= new ArrayList<>();
                removeArmiesMsg.forEach(msgArmy->{
                    armies.stream().filter(a->a.OID()==msgArmy.OID()).findFirst().ifPresent(a->removeLocal.add(a));
                });
                removeLocal.forEach(a->point.removeArmyGarrison(a.OID()));
                addUnitsToInventory(removeLocal);
                break;
            case DataAction.ADD:
                for (int i = 0; i < pointMsg.garrisonedArmiesLength(); i++)
                    point.setArmyGarrison(pointMsg.garrisonedArmies(i));
                    removeUnitsFromInventory(proto.getPointGarrisonArmies(pointMsg));
                break;
        }
        ControlPointMsg returnMsg = proto.updatePointGarrisonArmies(pointMsg, point.getGarrisonedArmies());
        msgArmiesString = "";
        for (int i = 0; i < returnMsg.garrisonedArmiesLength(); i++)
            msgArmiesString += proto.armyInfoString(returnMsg.garrisonedArmies(i));
        log.debug("Update armies garrison-after:" + PointCmd.name(pointMsg.cmd()) +
                proto.pointInfoString(point) + " MsgArmies:" + msgArmiesString);
        return returnMsg;
    }

    public ControlPointMsg pointGetInfo(ControlPoint point){
        Boolean capturable = false;
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        proto.createPointMsg(false, fbb, point);
        fbb.forceDefaults(true);
        ControlPointMsg.addCmd(fbb, PointCmd.POINT_INFO);
        ControlPointMsg.addCapturable(fbb, capturable);
        ControlPointMsg.addSubjectOID(fbb, -1);
        ControlPointMsg.addAction(fbb,(byte)1);
        fbb.forceDefaults(false);
        int msgOffset = ControlPointMsg.endControlPointMsg(fbb);
        fbb.finish(msgOffset);
        ControlPointMsg returnMsg = ControlPointMsg.getRootAsControlPointMsg(fbb.dataBuffer());
        return returnMsg;
    }


    public ControlPointMsg pointGetInfo(ControlPointMsg pointMsg, ControlPoint point) {
        if (point == null) point = addPoint(pointMsg.OID(), pointMsg.name());
        point.addListener(OID.fromLong(pointMsg.subjectOID()));
        Boolean capturable = playerManager.canCapture(pointMsg.subjectOID(), point);
        ControlPointMsg returnMsg = pointGetInfo(point);
        returnMsg.mutateCapturable(capturable);
        returnMsg.mutateSubjectOID(pointMsg.subjectOID());
        returnMsg.mutateAction(pointMsg.action());
        return returnMsg;
    }


    public ControlPointMsg capturePoint(ControlPointMsg pointMsg, ControlPoint point) {

        ControlPlayer capturing = playerManager.getPlayer(pointMsg.subjectOID());
        Boolean captured = null, siegeRequired = false;
        synchronized (point) {
            boolean canCapture = playerManager.canCapture(pointMsg.subjectOID(), point);
            log.debug("Before Capture called: capturable-" + canCapture + " -captured" + captured + "\n" + playerManager.getRoot().printSubTree());
            captured = capturePoint(point, capturing);
            canCapture = playerManager.canCapture(pointMsg.subjectOID(), point);
            log.debug("Capture called: capturable-" + canCapture + " -captured" + captured + "\n" + playerManager.getRoot().printSubTree());
            FlatBufferBuilder fbb = new FlatBufferBuilder();
            int subjectName = fbb.createString(capturing.getName());
            proto.createPointMsg(false, fbb, point);
            ControlPointMsg.addCmd(fbb, PointCmd.POINT_CAPTURE);
            fbb.forceDefaults(true);
            ControlPointMsg.addCapturable(fbb, canCapture);
            ControlPointMsg.addSubjectOID(fbb, pointMsg.subjectOID());
            fbb.forceDefaults(false);
            ControlPointMsg.addCaptured(fbb, captured);
            ControlPointMsg.addSubjectName(fbb, subjectName);
            fbb.finish(ControlPointMsg.endControlPointMsg(fbb));
            pointMsg = ControlPointMsg.getRootAsControlPointMsg(fbb.dataBuffer());
        }
        if (captured) {
            point.getGarrison().mutateOID(capturing.getOID().toLong());
            ControlPlayerMsg returnMsg = proto.createControlPlayer(pointMsg.subjectOID(), playerManager.getRoot());
            log.debug("controlpointmanager: sending nodeMsg" + returnMsg.cmd());
            ControlClient.sendControlPlayerMsg(returnMsg,playerManager.getsPlayerSet(),(player,msg)->{
                msg.mutateSubjectOID(player.toLong());
                msg.mutateSubjectOID(player.toLong());
            });
        }
        return pointMsg;
    }

    public BattleMsg siegePoint(ControlPointMsg pointMsg, ControlPoint point) {
        List<ArmyMsg> garrisonArmies = point.getGarrisonedArmies(), siegingArmies = proto.getPointArmies(pointMsg);
        log.debug("siegePoint: point garrisoned armies:" + proto.logArmyCollection(garrisonArmies) + " sieging armies:" + proto.logArmyCollection(siegingArmies) +
                "point unit garrison:" + proto.armyInfoString(point.getGarrison()));
        if (point.getGarrison().armyUnitsLength() > 0) {
            ArmyMsg unitsGarrison = point.getGarrison();
            log.debug("units army exists, before:"+proto.armyInfoString(unitsGarrison));
            unitsGarrison.mutateIsMainArmy(true);
            unitsGarrison.mutateOID(point.OID());
            ControlPlayer controlling = point.getControllingPlayer();
            if (controlling!=null) unitsGarrison.mutateControllerOid(controlling.getOID().toLong());
            else unitsGarrison.mutateControllerOid(-1);
            garrisonArmies.add(unitsGarrison);
            log.debug("units army exists, after:"+proto.armyInfoString(unitsGarrison));
        }
        point.siege(pointMsg.subjectOID(), siegingArmies);
        garrisonArmies.addAll(siegingArmies);
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int armiesOffset = BattleMsg.createArmiesVector(fbb, proto.addExistingArmies(fbb, garrisonArmies));
        BattleMsg.startBattleMsg(fbb);
        BattleMsg.addOID(fbb, -1);
        BattleMsg.addPointOID(fbb, point.OID());
        BattleMsg.addPosition(fbb, Vec3.createVec3(fbb, 0, 5, 0));
        BattleMsg.addCmd(fbb, BattleCmd.PLAYER_STARTED_BATTLE);
        BattleMsg.addIsSiege(fbb, true);
        BattleMsg.addSubjectOID(fbb, pointMsg.subjectOID());
        BattleMsg.addArmies(fbb, armiesOffset);
        int offset = BattleMsg.endBattleMsg(fbb);
        fbb.finish(offset);
        BattleMsg msg = BattleMsg.getRootAsBattleMsg(fbb.dataBuffer());
        return msg;
    }

    public static void addUnitsToInventory(Collection<ArmyMsg> armies){
        HashMap<OID, HashMap<OID, UnitMsg>> addUnits = proto.getPlayerUnits(armies);
        addUnits.forEach((playerOid,unitMap)->{
            unitMap.forEach((unitOid,u)->AgisInventoryClient.generateItem(playerOid,u.prefabId(),u.unitName(),1,new HashMap<>()));
        });
    }

    public static void removeUnitsFromInventory(Collection<ArmyMsg> armies) {
        HashMap<OID, HashMap<OID, Integer>> removeUnits = proto.getPlayerUnitsQuantities(armies);
        removeUnits.forEach((playerOid, unitsMap) -> AgisInventoryClient.removeSpecificItem(playerOid, unitsMap, false));
    }

    public ControlPointMsg siegeResult(ControlPointMsg pointMsg, ControlPoint point) {
        Collection<ArmyMsg> winningArmies = proto.getPointGarrisonArmies(pointMsg), losingArmies = proto.getPointArmies(pointMsg);
        Collection<ArmyMsg> oldGarrison = point.getGarrisonedArmies();
        ArmyMsg newUnitsGarrison = pointMsg.garrison();
        ControlPlayer sieger = playerManager.getPlayer(point.getCapturingSieger());
        pointMsg.mutateSubjectOID(sieger.getOID().toLong());
        log.debug("recieved siege result-before, pointMsg:" +
                proto.pointInfoString(pointMsg) + " controlPoint:" + proto.pointInfoString(point));
        if (newUnitsGarrison!=null) {
            newUnitsGarrison = proto.clearDeadUnits(newUnitsGarrison);
            log.debug("units garrison survived:"+proto.armyInfoString(newUnitsGarrison));
        } else log.debug("there was no units garrison present at siege");
        boolean pointDefended = (newUnitsGarrison != null && newUnitsGarrison.armyUnitsLength()>0) || winningArmies.stream().anyMatch(newArmy -> oldGarrison.stream().anyMatch(oldArmy -> newArmy.OID() == oldArmy.OID()));
        log.debug("point defended:"+pointDefended);
        synchronized (point) {
            point.setGarrison(pointMsg.garrison());
            if (pointDefended) {
                if (newUnitsGarrison != null) point.updateGarrison(newUnitsGarrison);
                for (ArmyMsg updatedGarrisonArmy : winningArmies) {
                    point.updateGarrison(proto.clearDeadUnits(updatedGarrisonArmy));
                }
                BattleManager.clearDeadUnitsFromInventory(losingArmies);
            } else {
                for (ArmyMsg oldArmy : oldGarrison) {
                    point.removeArmyGarrison(oldArmy.OID());
                }
                BattleManager.clearDeadUnitsFromInventory(losingArmies);
                BattleManager.clearDeadUnitsFromInventory(winningArmies);
                winningArmies = proto.clearDeadUnits(winningArmies);
                pointMsg.mutateOID(point.getCapturingSieger());
                ControlPointMsg capturedMsg = capturePoint(pointMsg, point);
                if (capturedMsg.captured() == true) {
                    log.debug("siege result, point captured-captureMsg:" +
                            proto.pointInfoString(capturedMsg) + " controlPoint:" + proto.pointInfoString(point));
                } else log.debug(" -------fatal error: siege won but player did not capture-------");
            }
        }
        point.siegeCompleted();
        ControlPointMsg returnMsg = pointGetInfo(point);
        return returnMsg;
    }


    public ControlPointMsg enablePoint(ControlPointMsg pointMsg, ControlPoint point) {
        ControlPlayer enabling = playerManager.getPlayer(pointMsg.subjectOID());
        synchronized (point) {
            Boolean enabled = enabling.canEnable(point);
            if (enabled) enabling.enablePoint(point);
            FlatBufferBuilder fbb = new FlatBufferBuilder();
            proto.createPointMsg(false, fbb, point);
            ControlPointMsg.addCmd(fbb, pointMsg.cmd());
            ControlPointMsg.addCaptured(fbb, enabled);
            ControlPointMsg.addSubjectOID(fbb, pointMsg.subjectOID());
            ControlPointMsg.addAction(fbb, pointMsg.action());
            fbb.finish(ControlPointMsg.endControlPointMsg(fbb));
            pointMsg = ControlPointMsg.getRootAsControlPointMsg(fbb.dataBuffer());
            return pointMsg;
        }
    }

    public void clear() {
        points.clear();
        playerManager.clear();
    }

    public  HashMap<Long,ControlPoint> getPointMap() {
        return points;
    }

    public class TimedTasks implements Runnable {
        private boolean keepRunning = true;
        public void setKeepRunning(boolean keepRunning){ this.keepRunning = keepRunning;}
        public void run() {
            while (keepRunning ) {
                try {
                    Thread.sleep(60000 * 2);// 2 min
                } catch (InterruptedException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                persistControl();
                try {
                    for (ControlPoint c : points.values()) {
                        if (c.getEnablingPlayer() == null) continue;
                        else log.debug("paying revenue, before: pointOid:" + c.OID());
                        c.payRevenue();
                    }
                    log.debug("ControlPointManager:collecting resources from points");
                    playerManager.taxTree();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ControlPoint getPoint(long oid) {
        return points.get(oid);
    }

    public ControlPoint addPoint(long oid, String name) {
        points.putIfAbsent(oid, new ControlPoint(oid, name));
        return points.get(oid);
    }

    public void removePoint(long oid) {
        points.remove(oid);
    }


    public boolean capturePoint(ControlPoint point, ControlPlayer capturing) {
        boolean canCapture = playerManager.canCapture(capturing.getOID().toLong(), point);
        log.debug("Capturing point: canCapture-" + canCapture);
        if (!canCapture)
            return false;
        if (capturing.getFaction()<=1) capturing.addOwnFaction();
        capturing.capturePoint(point, (conformToPlayer, conformingPlayer) -> conformReputation(conformingPlayer,conformToPlayer));
        return true;
    }



    private void conformReputation( ControlPlayer conforming,ControlPlayer conformTo) {
        if (conformTo==null || conforming==null || conforming==conformTo){
            log.debug("conforming reputation one of the players was null");
            return;
        }
        if (conformTo.getFaction()<=1 || conforming.getFaction()<=1){
            log.debug("conformReputation one of the player factions isn't right, conformTo:"+
                    conformTo.getOID().toLong()+" conformTo faction:"+conformTo.getFaction()+
            " conforming:"+conforming.getOID().toLong()+" conformingFaction:"+conforming.getFaction());
            return;
        }
        PlayerNode conformToController = conformTo.getPlayerNode().getAncestorChildOfRoot(), breakNode = conforming.getPlayerNode();
        ArrayList<PlayerNode> conformingTree = Collections.list(conforming.getPlayerNode().breadthFirstEnumeration());
        Thread t = new Thread(()->conformPlayerReputationRecursive(conformingTree, conformToController,breakNode));
        t.run();
    }

    private void conformPlayerReputationRecursive(List<PlayerNode> conformingNodes, PlayerNode conformToNode,PlayerNode breakNode) {
        if (conformToNode.getPlayer().getOID().equals(breakNode.getPlayer().getOID())) return;
        for (PlayerNode node : conformingNodes) {
            ControlPlayer conformTo = conformToNode.getPlayer(), conforming = node.getPlayer();

            if (conformTo.getFaction() <= 1 || conforming.getFaction() <= 1) {
                log.debug("conformReputation one of the player factions isn't right, conformTo:" +
                        conformTo.getOID().toLong() + " conformTo faction:" + conformTo.getFaction() +
                        " conforming:" + conforming.getOID().toLong() + " conformingFaction:" + conforming.getFaction());
                return;
            }
            log.debug("conforming reputation recursive, conformToPlayer:" + conformTo.getOID().toLong() + " conformToFaction:" +
                    conformTo.getFaction() + " conforming:" + conforming.getOID().toLong() + " conformingFaction:" + conforming.getFaction());
            conformTwoPlayers(conforming, conformTo);
        }
        Enumeration children = conformToNode.children();
        while (children.hasMoreElements()){
            PlayerNode child = (PlayerNode)children.nextElement();
            log.debug("entering child node of:"+conformToNode.getPlayer().getOID().toLong()+" nodeOid:"+child.getPlayer().getOID().toLong());
            conformPlayerReputationRecursive(conformingNodes,child,breakNode);
        }
    }

    public void conformTwoPlayers(ControlPlayer conforming, ControlPlayer conformTo){
        try {
            FactionClient.alterReputation(conformTo.getOID(),conforming.getFaction(),FactionPlugin.ExaltedRep,true);
            Thread.sleep(125);
            FactionClient.alterReputation(conforming.getOID(),conformTo.getFaction(),FactionPlugin.ExaltedRep,true);
            Thread.sleep(125);
            ObjectTracker.NotifyReactionRadiusMessage notifyMsg = new ObjectTracker.NotifyReactionRadiusMessage(conforming.getOID(),conformTo.getOID(),true,true);
            Engine.getAgent().sendBroadcast(notifyMsg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EventMessageHelper.SendGeneralEvent(conforming.getOID(), EventMessageHelper.REPUTATION_CHANGED, conformTo.getFaction(), "" + FactionPlugin.ExaltedRep);
        EventMessageHelper.SendGeneralEvent(conformTo.getOID(), EventMessageHelper.REPUTATION_CHANGED, conforming.getFaction(), "" + FactionPlugin.ExaltedRep);
    }
}
