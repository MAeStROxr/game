package mygame.control;

import atavism.server.util.Logger;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class PlayerNode extends DefaultMutableTreeNode {


    private static final long serialVersionUID = -5863423700778585853L;
    private static final Logger log = new Logger("PlayerNode");

    public ControlPlayer getPlayer() {
        return (ControlPlayer) getUserObject();
    }

    public PlayerNode(String name, ControlPlayer playerInfo) {
        super(playerInfo);
    }

    public PlayerNode getAncestorChildOfRoot() {
        TreeNode ancestor = this;
        TreeNode previous;
        do {
            previous = ancestor;
            ancestor = ancestor.getParent();
        } while (ancestor.getParent() != null);

        return (PlayerNode) previous;
    }

    public PlayerNode getRoot() {
        TreeNode root = super.getRoot();
        return (PlayerNode) root;
    }

    public PlayerNode getParent() {
        TreeNode parent = super.getParent();
        return (PlayerNode) parent;
    }

    public PlayerNode getChildAt(int index) {
        return (PlayerNode) super.getChildAt(index);
    }

    public void payTaxes() {
        PlayerNode parentNode = getParent();

        if (parentNode == null || parentNode.isRoot() || isRoot())
            return;
        ControlPlayer player = getPlayer(), parent = parentNode.getPlayer();
        if (player == null || parent == null) {
            if (player == null) log.debug("payTaxes- player null");
            else if (parent == null) log.debug("payTaxes: playerOid:" + player.getOID().toLong() + " parent null");
            return;
        }
        int tax = player.getAccumulatedGold() * parent.getTaxRate() / 100,
                newPlayerAccumulated = player.getAccumulatedGold() - tax, newParentAccumulated = parent.getAccumulatedGold() + tax;
        log.debug("player " + player.getOID().toLong() + " paying tax:" + tax + " oldPlayerGold:" + player.getAccumulatedGold() +
                " newPlayerGold:" + newPlayerAccumulated + " oldParentGold:" + parent.getAccumulatedGold() +
                " newParentGold:" + newParentAccumulated);
        player.setAccumulatedGold(newPlayerAccumulated);
        parent.setAccumulatedGold(newParentAccumulated);
        parentNode.payTaxes();
    }

    public String printSubTree() {
        StringBuffer sb = new StringBuffer();
        addNodeToBuffer(sb);
        return sb.toString();
    }

    public void addNodeToBuffer(StringBuffer sb) {
        sb.append(this.toString());
        if (getChildCount() > 0) {
            sb.append("<");
            for (int i = 0; i < getChildCount(); i++)
                getChildAt(i).addNodeToBuffer(sb);
            sb.append(">");
        }
    }

}

