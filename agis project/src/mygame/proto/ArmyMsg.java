// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class ArmyMsg extends Table {
  public static ArmyMsg getRootAsArmyMsg(ByteBuffer _bb) { return getRootAsArmyMsg(_bb, new ArmyMsg()); }
  public static ArmyMsg getRootAsArmyMsg(ByteBuffer _bb, ArmyMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public ArmyMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID() { int o = __offset(4); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.putLong(o + bb_pos, OID); return true; } else { return false; } }
  public UnitMsg armyUnits(int j) { return armyUnits(new UnitMsg(), j); }
  public UnitMsg armyUnits(UnitMsg obj, int j) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int armyUnitsLength() { int o = __offset(6); return o != 0 ? __vector_len(o) : 0; }
  public String armyName() { int o = __offset(8); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer armyNameAsByteBuffer() { return __vector_as_bytebuffer(8, 1); }
  public byte action() { int o = __offset(10); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateAction(byte action) { int o = __offset(10); if (o != 0) { bb.put(o + bb_pos, action); return true; } else { return false; } }
  public boolean isNetworkPlayer() { int o = __offset(12); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsNetworkPlayer(boolean isNetworkPlayer) { int o = __offset(12); if (o != 0) { bb.put(o + bb_pos, (byte)(isNetworkPlayer ? 1 : 0)); return true; } else { return false; } }
  public boolean isPlayerFaction() { int o = __offset(14); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsPlayerFaction(boolean isPlayerFaction) { int o = __offset(14); if (o != 0) { bb.put(o + bb_pos, (byte)(isPlayerFaction ? 1 : 0)); return true; } else { return false; } }
  public int factionID() { int o = __offset(16); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateFactionID(int factionID) { int o = __offset(16); if (o != 0) { bb.putInt(o + bb_pos, factionID); return true; } else { return false; } }
  public int dataID() { int o = __offset(18); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateDataID(int dataID) { int o = __offset(18); if (o != 0) { bb.putInt(o + bb_pos, dataID); return true; } else { return false; } }
  public boolean isMainArmy() { int o = __offset(20); return o != 0 ? 0!=bb.get(o + bb_pos) : true; }
  public boolean mutateIsMainArmy(boolean isMainArmy) { int o = __offset(20); if (o != 0) { bb.put(o + bb_pos, (byte)(isMainArmy ? 1 : 0)); return true; } else { return false; } }
  public boolean requireDeployment() { int o = __offset(22); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateRequireDeployment(boolean requireDeployment) { int o = __offset(22); if (o != 0) { bb.put(o + bb_pos, (byte)(requireDeployment ? 1 : 0)); return true; } else { return false; } }
  public TileMsg deployableTiles(int j) { return deployableTiles(new TileMsg(), j); }
  public TileMsg deployableTiles(TileMsg obj, int j) { int o = __offset(24); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int deployableTilesLength() { int o = __offset(24); return o != 0 ? __vector_len(o) : 0; }
  public Vec3 position() { return position(new Vec3()); }
  public Vec3 position(Vec3 obj) { int o = __offset(26); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }
  public Vec3 color() { return color(new Vec3()); }
  public Vec3 color(Vec3 obj) { int o = __offset(28); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }
  public long controllerOid() { int o = __offset(30); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateControllerOid(long controllerOid) { int o = __offset(30); if (o != 0) { bb.putLong(o + bb_pos, controllerOid); return true; } else { return false; } }
  public boolean isHostile() { int o = __offset(32); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsHostile(boolean isHostile) { int o = __offset(32); if (o != 0) { bb.put(o + bb_pos, (byte)(isHostile ? 1 : 0)); return true; } else { return false; } }

  public static void startArmyMsg(FlatBufferBuilder builder) { builder.startObject(15); }
  public static void addOID(FlatBufferBuilder builder, long OID) { builder.addLong(0, OID, 0); }
  public static void addArmyUnits(FlatBufferBuilder builder, int armyUnitsOffset) { builder.addOffset(1, armyUnitsOffset, 0); }
  public static int createArmyUnitsVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startArmyUnitsVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addArmyName(FlatBufferBuilder builder, int armyNameOffset) { builder.addOffset(2, armyNameOffset, 0); }
  public static void addAction(FlatBufferBuilder builder, byte action) { builder.addByte(3, action, 0); }
  public static void addIsNetworkPlayer(FlatBufferBuilder builder, boolean isNetworkPlayer) { builder.addBoolean(4, isNetworkPlayer, false); }
  public static void addIsPlayerFaction(FlatBufferBuilder builder, boolean isPlayerFaction) { builder.addBoolean(5, isPlayerFaction, false); }
  public static void addFactionID(FlatBufferBuilder builder, int factionID) { builder.addInt(6, factionID, 0); }
  public static void addDataID(FlatBufferBuilder builder, int dataID) { builder.addInt(7, dataID, 0); }
  public static void addIsMainArmy(FlatBufferBuilder builder, boolean isMainArmy) { builder.addBoolean(8, isMainArmy, true); }
  public static void addRequireDeployment(FlatBufferBuilder builder, boolean requireDeployment) { builder.addBoolean(9, requireDeployment, false); }
  public static void addDeployableTiles(FlatBufferBuilder builder, int deployableTilesOffset) { builder.addOffset(10, deployableTilesOffset, 0); }
  public static int createDeployableTilesVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startDeployableTilesVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addPosition(FlatBufferBuilder builder, int positionOffset) { builder.addStruct(11, positionOffset, 0); }
  public static void addColor(FlatBufferBuilder builder, int colorOffset) { builder.addStruct(12, colorOffset, 0); }
  public static void addControllerOid(FlatBufferBuilder builder, long controllerOid) { builder.addLong(13, controllerOid, 0); }
  public static void addIsHostile(FlatBufferBuilder builder, boolean isHostile) { builder.addBoolean(14, isHostile, false); }
  public static int endArmyMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

