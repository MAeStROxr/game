// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class AttackInstanceMsg extends Table {
  public static AttackInstanceMsg getRootAsAttackInstanceMsg(ByteBuffer _bb) { return getRootAsAttackInstanceMsg(_bb, new AttackInstanceMsg()); }
  public static AttackInstanceMsg getRootAsAttackInstanceMsg(ByteBuffer _bb, AttackInstanceMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public AttackInstanceMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public boolean processed() { int o = __offset(4); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateProcessed(boolean processed) { int o = __offset(4); if (o != 0) { bb.put(o + bb_pos, (byte)(processed ? 1 : 0)); return true; } else { return false; } }
  public boolean isAbility() { int o = __offset(6); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsAbility(boolean isAbility) { int o = __offset(6); if (o != 0) { bb.put(o + bb_pos, (byte)(isAbility ? 1 : 0)); return true; } else { return false; } }
  public boolean calculated() { int o = __offset(8); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateCalculated(boolean calculated) { int o = __offset(8); if (o != 0) { bb.put(o + bb_pos, (byte)(calculated ? 1 : 0)); return true; } else { return false; } }
  public UnitMsg srcUnit() { return srcUnit(new UnitMsg()); }
  public UnitMsg srcUnit(UnitMsg obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public UnitMsg tgtUnit() { return tgtUnit(new UnitMsg()); }
  public UnitMsg tgtUnit(UnitMsg obj) { int o = __offset(12); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int unitAbilityID() { int o = __offset(14); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateUnitAbilityID(int unitAbilityID) { int o = __offset(14); if (o != 0) { bb.putInt(o + bb_pos, unitAbilityID); return true; } else { return false; } }
  public boolean isMelee() { int o = __offset(16); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsMelee(boolean isMelee) { int o = __offset(16); if (o != 0) { bb.put(o + bb_pos, (byte)(isMelee ? 1 : 0)); return true; } else { return false; } }
  public boolean isCounter() { int o = __offset(18); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsCounter(boolean isCounter) { int o = __offset(18); if (o != 0) { bb.put(o + bb_pos, (byte)(isCounter ? 1 : 0)); return true; } else { return false; } }
  public boolean missed() { int o = __offset(20); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateMissed(boolean missed) { int o = __offset(20); if (o != 0) { bb.put(o + bb_pos, (byte)(missed ? 1 : 0)); return true; } else { return false; } }
  public boolean critical() { int o = __offset(22); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateCritical(boolean critical) { int o = __offset(22); if (o != 0) { bb.put(o + bb_pos, (byte)(critical ? 1 : 0)); return true; } else { return false; } }
  public boolean Stunned() { int o = __offset(24); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateStunned(boolean Stunned) { int o = __offset(24); if (o != 0) { bb.put(o + bb_pos, (byte)(Stunned ? 1 : 0)); return true; } else { return false; } }
  public boolean Silenced() { int o = __offset(26); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateSilenced(boolean Silenced) { int o = __offset(26); if (o != 0) { bb.put(o + bb_pos, (byte)(Silenced ? 1 : 0)); return true; } else { return false; } }
  public boolean flanked() { int o = __offset(28); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateFlanked(boolean flanked) { int o = __offset(28); if (o != 0) { bb.put(o + bb_pos, (byte)(flanked ? 1 : 0)); return true; } else { return false; } }
  public boolean Destroyed() { int o = __offset(30); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateDestroyed(boolean Destroyed) { int o = __offset(30); if (o != 0) { bb.put(o + bb_pos, (byte)(Destroyed ? 1 : 0)); return true; } else { return false; } }
  public float damage() { int o = __offset(32); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDamage(float damage) { int o = __offset(32); if (o != 0) { bb.putFloat(o + bb_pos, damage); return true; } else { return false; } }
  public int stun() { int o = __offset(34); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateStun(int stun) { int o = __offset(34); if (o != 0) { bb.putInt(o + bb_pos, stun); return true; } else { return false; } }
  public int silent() { int o = __offset(36); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateSilent(int silent) { int o = __offset(36); if (o != 0) { bb.putInt(o + bb_pos, silent); return true; } else { return false; } }
  public float damageTableModifier() { int o = __offset(38); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDamageTableModifier(float damageTableModifier) { int o = __offset(38); if (o != 0) { bb.putFloat(o + bb_pos, damageTableModifier); return true; } else { return false; } }
  public float flankingBonus() { int o = __offset(40); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateFlankingBonus(float flankingBonus) { int o = __offset(40); if (o != 0) { bb.putFloat(o + bb_pos, flankingBonus); return true; } else { return false; } }
  public UnitMsg unitList(int j) { return unitList(new UnitMsg(), j); }
  public UnitMsg unitList(UnitMsg obj, int j) { int o = __offset(42); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int unitListLength() { int o = __offset(42); return o != 0 ? __vector_len(o) : 0; }

  public static int createAttackInstanceMsg(FlatBufferBuilder builder,
      boolean processed,
      boolean isAbility,
      boolean calculated,
      int srcUnitOffset,
      int tgtUnitOffset,
      int unitAbilityID,
      boolean isMelee,
      boolean isCounter,
      boolean missed,
      boolean critical,
      boolean Stunned,
      boolean Silenced,
      boolean flanked,
      boolean Destroyed,
      float damage,
      int stun,
      int silent,
      float damageTableModifier,
      float flankingBonus,
      int unitListOffset) {
    builder.startObject(20);
    AttackInstanceMsg.addUnitList(builder, unitListOffset);
    AttackInstanceMsg.addFlankingBonus(builder, flankingBonus);
    AttackInstanceMsg.addDamageTableModifier(builder, damageTableModifier);
    AttackInstanceMsg.addSilent(builder, silent);
    AttackInstanceMsg.addStun(builder, stun);
    AttackInstanceMsg.addDamage(builder, damage);
    AttackInstanceMsg.addUnitAbilityID(builder, unitAbilityID);
    AttackInstanceMsg.addTgtUnit(builder, tgtUnitOffset);
    AttackInstanceMsg.addSrcUnit(builder, srcUnitOffset);
    AttackInstanceMsg.addDestroyed(builder, Destroyed);
    AttackInstanceMsg.addFlanked(builder, flanked);
    AttackInstanceMsg.addSilenced(builder, Silenced);
    AttackInstanceMsg.addStunned(builder, Stunned);
    AttackInstanceMsg.addCritical(builder, critical);
    AttackInstanceMsg.addMissed(builder, missed);
    AttackInstanceMsg.addIsCounter(builder, isCounter);
    AttackInstanceMsg.addIsMelee(builder, isMelee);
    AttackInstanceMsg.addCalculated(builder, calculated);
    AttackInstanceMsg.addIsAbility(builder, isAbility);
    AttackInstanceMsg.addProcessed(builder, processed);
    return AttackInstanceMsg.endAttackInstanceMsg(builder);
  }

  public static void startAttackInstanceMsg(FlatBufferBuilder builder) { builder.startObject(20); }
  public static void addProcessed(FlatBufferBuilder builder, boolean processed) { builder.addBoolean(0, processed, false); }
  public static void addIsAbility(FlatBufferBuilder builder, boolean isAbility) { builder.addBoolean(1, isAbility, false); }
  public static void addCalculated(FlatBufferBuilder builder, boolean calculated) { builder.addBoolean(2, calculated, false); }
  public static void addSrcUnit(FlatBufferBuilder builder, int srcUnitOffset) { builder.addOffset(3, srcUnitOffset, 0); }
  public static void addTgtUnit(FlatBufferBuilder builder, int tgtUnitOffset) { builder.addOffset(4, tgtUnitOffset, 0); }
  public static void addUnitAbilityID(FlatBufferBuilder builder, int unitAbilityID) { builder.addInt(5, unitAbilityID, 0); }
  public static void addIsMelee(FlatBufferBuilder builder, boolean isMelee) { builder.addBoolean(6, isMelee, false); }
  public static void addIsCounter(FlatBufferBuilder builder, boolean isCounter) { builder.addBoolean(7, isCounter, false); }
  public static void addMissed(FlatBufferBuilder builder, boolean missed) { builder.addBoolean(8, missed, false); }
  public static void addCritical(FlatBufferBuilder builder, boolean critical) { builder.addBoolean(9, critical, false); }
  public static void addStunned(FlatBufferBuilder builder, boolean Stunned) { builder.addBoolean(10, Stunned, false); }
  public static void addSilenced(FlatBufferBuilder builder, boolean Silenced) { builder.addBoolean(11, Silenced, false); }
  public static void addFlanked(FlatBufferBuilder builder, boolean flanked) { builder.addBoolean(12, flanked, false); }
  public static void addDestroyed(FlatBufferBuilder builder, boolean Destroyed) { builder.addBoolean(13, Destroyed, false); }
  public static void addDamage(FlatBufferBuilder builder, float damage) { builder.addFloat(14, damage, 0); }
  public static void addStun(FlatBufferBuilder builder, int stun) { builder.addInt(15, stun, 0); }
  public static void addSilent(FlatBufferBuilder builder, int silent) { builder.addInt(16, silent, 0); }
  public static void addDamageTableModifier(FlatBufferBuilder builder, float damageTableModifier) { builder.addFloat(17, damageTableModifier, 0); }
  public static void addFlankingBonus(FlatBufferBuilder builder, float flankingBonus) { builder.addFloat(18, flankingBonus, 0); }
  public static void addUnitList(FlatBufferBuilder builder, int unitListOffset) { builder.addOffset(19, unitListOffset, 0); }
  public static int createUnitListVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startUnitListVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static int endAttackInstanceMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

