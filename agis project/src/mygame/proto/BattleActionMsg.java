// automatically generated, do not modify

package mygame.proto;

public final class BattleActionMsg {
  private BattleActionMsg() { }
  public static final byte NONE = 0;
  public static final byte UnitMoveMsg = 1;
  public static final byte UnitAbilityMsg = 2;
  public static final byte UnitAttackMsg = 3;

  private static final String[] names = { "NONE", "UnitMoveMsg", "UnitAbilityMsg", "UnitAttackMsg", };

  public static String name(int e) { return names[e]; }
};

