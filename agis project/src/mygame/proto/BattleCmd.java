// automatically generated, do not modify

package mygame.proto;

public final class BattleCmd {
  private BattleCmd() { }
  public static final byte EXECUTE_FAILURE = 0;
  public static final byte PLAYER_STARTED_BATTLE = 1;
  public static final byte PLAYER_JOINED_BATTLE = 2;
  public static final byte PLAYER_DEPLOYED_ARMY = 3;
  public static final byte PLAYER_LEFT_BATTLE = 4;
  public static final byte PLAYER_ENDED_TURN = 5;
  public static final byte PLAYER_RETREAT_BATTLE = 6;
  public static final byte BATTLE_OVER = 7;
  public static final byte BATTLE_COMMENCED = 8;
  public static final byte UNIT_MOVE = 9;
  public static final byte UNIT_ATTACK = 10;
  public static final byte UNIT_ABILITY = 11;
  public static final byte BATTLE_INFO = 12;

  private static final String[] names = { "EXECUTE_FAILURE", "PLAYER_STARTED_BATTLE", "PLAYER_JOINED_BATTLE", "PLAYER_DEPLOYED_ARMY", "PLAYER_LEFT_BATTLE", "PLAYER_ENDED_TURN", "PLAYER_RETREAT_BATTLE", "BATTLE_OVER", "BATTLE_COMMENCED", "UNIT_MOVE", "UNIT_ATTACK", "UNIT_ABILITY", "BATTLE_INFO", };

  public static String name(int e) { return names[e]; }
};

