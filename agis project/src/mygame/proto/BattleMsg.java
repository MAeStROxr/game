// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class BattleMsg extends Table {
  public static BattleMsg getRootAsBattleMsg(ByteBuffer _bb) { return getRootAsBattleMsg(_bb, new BattleMsg()); }
  public static BattleMsg getRootAsBattleMsg(ByteBuffer _bb, BattleMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public BattleMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID() { int o = __offset(4); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.putLong(o + bb_pos, OID); return true; } else { return false; } }
  public byte cmd() { int o = __offset(6); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateCmd(byte cmd) { int o = __offset(6); if (o != 0) { bb.put(o + bb_pos, cmd); return true; } else { return false; } }
  public long subjectOID() { int o = __offset(8); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateSubjectOID(long subjectOID) { int o = __offset(8); if (o != 0) { bb.putLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public ArmyMsg armies(int j) { return armies(new ArmyMsg(), j); }
  public ArmyMsg armies(ArmyMsg obj, int j) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int armiesLength() { int o = __offset(10); return o != 0 ? __vector_len(o) : 0; }
  public int currentFactionID() { int o = __offset(12); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateCurrentFactionID(int currentFactionID) { int o = __offset(12); if (o != 0) { bb.putInt(o + bb_pos, currentFactionID); return true; } else { return false; } }
  public byte battleActionType() { int o = __offset(14); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateBattleActionType(byte battleAction_type) { int o = __offset(14); if (o != 0) { bb.put(o + bb_pos, battleAction_type); return true; } else { return false; } }
  public Table battleAction(Table obj) { int o = __offset(16); return o != 0 ? __union(obj, o) : null; }
  public Vec3 position() { return position(new Vec3()); }
  public Vec3 position(Vec3 obj) { int o = __offset(18); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }
  public TileMsg gridTiles(int j) { return gridTiles(new TileMsg(), j); }
  public TileMsg gridTiles(TileMsg obj, int j) { int o = __offset(20); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int gridTilesLength() { int o = __offset(20); return o != 0 ? __vector_len(o) : 0; }
  public boolean isSiege() { int o = __offset(22); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateIsSiege(boolean isSiege) { int o = __offset(22); if (o != 0) { bb.put(o + bb_pos, (byte)(isSiege ? 1 : 0)); return true; } else { return false; } }
  public long pointOID() { int o = __offset(24); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutatePointOID(long pointOID) { int o = __offset(24); if (o != 0) { bb.putLong(o + bb_pos, pointOID); return true; } else { return false; } }
  public String text() { int o = __offset(26); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer textAsByteBuffer() { return __vector_as_bytebuffer(26, 1); }
  public int battlePhase() { int o = __offset(28); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateBattlePhase(int battlePhase) { int o = __offset(28); if (o != 0) { bb.putInt(o + bb_pos, battlePhase); return true; } else { return false; } }

  public static void startBattleMsg(FlatBufferBuilder builder) { builder.startObject(13); }
  public static void addOID(FlatBufferBuilder builder, long OID) { builder.addLong(0, OID, 0); }
  public static void addCmd(FlatBufferBuilder builder, byte cmd) { builder.addByte(1, cmd, 0); }
  public static void addSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.addLong(2, subjectOID, 0); }
  public static void addArmies(FlatBufferBuilder builder, int armiesOffset) { builder.addOffset(3, armiesOffset, 0); }
  public static int createArmiesVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startArmiesVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addCurrentFactionID(FlatBufferBuilder builder, int currentFactionID) { builder.addInt(4, currentFactionID, 0); }
  public static void addBattleActionType(FlatBufferBuilder builder, byte battleActionType) { builder.addByte(5, battleActionType, 0); }
  public static void addBattleAction(FlatBufferBuilder builder, int battleActionOffset) { builder.addOffset(6, battleActionOffset, 0); }
  public static void addPosition(FlatBufferBuilder builder, int positionOffset) { builder.addStruct(7, positionOffset, 0); }
  public static void addGridTiles(FlatBufferBuilder builder, int gridTilesOffset) { builder.addOffset(8, gridTilesOffset, 0); }
  public static int createGridTilesVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startGridTilesVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addIsSiege(FlatBufferBuilder builder, boolean isSiege) { builder.addBoolean(9, isSiege, false); }
  public static void addPointOID(FlatBufferBuilder builder, long pointOID) { builder.addLong(10, pointOID, 0); }
  public static void addText(FlatBufferBuilder builder, int textOffset) { builder.addOffset(11, textOffset, 0); }
  public static void addBattlePhase(FlatBufferBuilder builder, int battlePhase) { builder.addInt(12, battlePhase, 0); }
  public static int endBattleMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

