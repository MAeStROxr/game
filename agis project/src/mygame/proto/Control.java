// automatically generated, do not modify

package mygame.proto;

public final class Control {
  private Control() { }
  public static final byte PLAYER_CONTROL = 0;
  public static final byte CONTROL_POINT = 1;

  private static final String[] names = { "PLAYER_CONTROL", "CONTROL_POINT", };

  public static String name(int e) { return names[e]; }
};

