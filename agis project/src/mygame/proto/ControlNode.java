// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class ControlNode extends Table {
  public static ControlNode getRootAsControlNode(ByteBuffer _bb) { return getRootAsControlNode(_bb, new ControlNode()); }
  public static ControlNode getRootAsControlNode(ByteBuffer _bb, ControlNode obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public ControlNode __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID() { int o = __offset(4); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.putLong(o + bb_pos, OID); return true; } else { return false; } }
  public String controlName() { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer controlNameAsByteBuffer() { return __vector_as_bytebuffer(6, 1); }
  public ControlNode controlled(int j) { return controlled(new ControlNode(), j); }
  public ControlNode controlled(ControlNode obj, int j) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int controlledLength() { int o = __offset(8); return o != 0 ? __vector_len(o) : 0; }
  public ControlPointMsg controlPoints(int j) { return controlPoints(new ControlPointMsg(), j); }
  public ControlPointMsg controlPoints(ControlPointMsg obj, int j) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int controlPointsLength() { int o = __offset(10); return o != 0 ? __vector_len(o) : 0; }
  public ControlNode parent() { return parent(new ControlNode()); }
  public ControlNode parent(ControlNode obj) { int o = __offset(12); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int taxRate() { int o = __offset(14); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateTaxRate(int taxRate) { int o = __offset(14); if (o != 0) { bb.putInt(o + bb_pos, taxRate); return true; } else { return false; } }
  public int treeId() { int o = __offset(16); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateTreeId(int treeId) { int o = __offset(16); if (o != 0) { bb.putInt(o + bb_pos, treeId); return true; } else { return false; } }

  public static int createControlNode(FlatBufferBuilder builder,
      long OID,
      int controlNameOffset,
      int controlledOffset,
      int controlPointsOffset,
      int parentOffset,
      int taxRate,
      int treeId) {
    builder.startObject(7);
    ControlNode.addOID(builder, OID);
    ControlNode.addTreeId(builder, treeId);
    ControlNode.addTaxRate(builder, taxRate);
    ControlNode.addParent(builder, parentOffset);
    ControlNode.addControlPoints(builder, controlPointsOffset);
    ControlNode.addControlled(builder, controlledOffset);
    ControlNode.addControlName(builder, controlNameOffset);
    return ControlNode.endControlNode(builder);
  }

  public static void startControlNode(FlatBufferBuilder builder) { builder.startObject(7); }
  public static void addOID(FlatBufferBuilder builder, long OID) { builder.addLong(0, OID, 0); }
  public static void addControlName(FlatBufferBuilder builder, int controlNameOffset) { builder.addOffset(1, controlNameOffset, 0); }
  public static void addControlled(FlatBufferBuilder builder, int controlledOffset) { builder.addOffset(2, controlledOffset, 0); }
  public static int createControlledVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startControlledVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addControlPoints(FlatBufferBuilder builder, int controlPointsOffset) { builder.addOffset(3, controlPointsOffset, 0); }
  public static int createControlPointsVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startControlPointsVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addParent(FlatBufferBuilder builder, int parentOffset) { builder.addOffset(4, parentOffset, 0); }
  public static void addTaxRate(FlatBufferBuilder builder, int taxRate) { builder.addInt(5, taxRate, 0); }
  public static void addTreeId(FlatBufferBuilder builder, int treeId) { builder.addInt(6, treeId, 0); }
  public static int endControlNode(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

