// automatically generated, do not modify

package mygame.proto;

public final class ControlPlayerCmd {
  private ControlPlayerCmd() { }
  public static final byte PLAYER_CONTROL_INFO = 0;
  public static final byte PLAYER_TAX = 1;
  public static final byte PLAYER_DECLARE_WAR = 2;
  public static final byte PLAYER_FACTION = 3;

  private static final String[] names = { "PLAYER_CONTROL_INFO", "PLAYER_TAX", "PLAYER_DECLARE_WAR", "PLAYER_FACTION", };

  public static String name(int e) { return names[e]; }
};

