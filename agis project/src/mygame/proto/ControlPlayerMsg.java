// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class ControlPlayerMsg extends Table {
  public static ControlPlayerMsg getRootAsControlPlayerMsg(ByteBuffer _bb) { return getRootAsControlPlayerMsg(_bb, new ControlPlayerMsg()); }
  public static ControlPlayerMsg getRootAsControlPlayerMsg(ByteBuffer _bb, ControlPlayerMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public ControlPlayerMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public byte cmd() { int o = __offset(4); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateCmd(byte cmd) { int o = __offset(4); if (o != 0) { bb.put(o + bb_pos, cmd); return true; } else { return false; } }
  public long subjectOID() { int o = __offset(6); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateSubjectOID(long subjectOID) { int o = __offset(6); if (o != 0) { bb.putLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public int taxRate() { int o = __offset(8); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateTaxRate(int taxRate) { int o = __offset(8); if (o != 0) { bb.putInt(o + bb_pos, taxRate); return true; } else { return false; } }
  public ControlNode nodeMsg() { return nodeMsg(new ControlNode()); }
  public ControlNode nodeMsg(ControlNode obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public String subjectName() { int o = __offset(12); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer subjectNameAsByteBuffer() { return __vector_as_bytebuffer(12, 1); }
  public long targetOID() { int o = __offset(14); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateTargetOID(long targetOID) { int o = __offset(14); if (o != 0) { bb.putLong(o + bb_pos, targetOID); return true; } else { return false; } }
  public int factionId() { int o = __offset(16); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateFactionId(int factionId) { int o = __offset(16); if (o != 0) { bb.putInt(o + bb_pos, factionId); return true; } else { return false; } }

  public static int createControlPlayerMsg(FlatBufferBuilder builder,
      byte cmd,
      long subjectOID,
      int taxRate,
      int nodeMsgOffset,
      int subjectNameOffset,
      long targetOID,
      int factionId) {
    builder.startObject(7);
    ControlPlayerMsg.addTargetOID(builder, targetOID);
    ControlPlayerMsg.addSubjectOID(builder, subjectOID);
    ControlPlayerMsg.addFactionId(builder, factionId);
    ControlPlayerMsg.addSubjectName(builder, subjectNameOffset);
    ControlPlayerMsg.addNodeMsg(builder, nodeMsgOffset);
    ControlPlayerMsg.addTaxRate(builder, taxRate);
    ControlPlayerMsg.addCmd(builder, cmd);
    return ControlPlayerMsg.endControlPlayerMsg(builder);
  }

  public static void startControlPlayerMsg(FlatBufferBuilder builder) { builder.startObject(7); }
  public static void addCmd(FlatBufferBuilder builder, byte cmd) { builder.addByte(0, cmd, 0); }
  public static void addSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.addLong(1, subjectOID, 0); }
  public static void addTaxRate(FlatBufferBuilder builder, int taxRate) { builder.addInt(2, taxRate, 0); }
  public static void addNodeMsg(FlatBufferBuilder builder, int nodeMsgOffset) { builder.addOffset(3, nodeMsgOffset, 0); }
  public static void addSubjectName(FlatBufferBuilder builder, int subjectNameOffset) { builder.addOffset(4, subjectNameOffset, 0); }
  public static void addTargetOID(FlatBufferBuilder builder, long targetOID) { builder.addLong(5, targetOID, 0); }
  public static void addFactionId(FlatBufferBuilder builder, int factionId) { builder.addInt(6, factionId, 0); }
  public static int endControlPlayerMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

