// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class ControlPointDatabase extends Table {
  public static ControlPointDatabase getRootAsControlPointDatabase(ByteBuffer _bb) { return getRootAsControlPointDatabase(_bb, new ControlPointDatabase()); }
  public static ControlPointDatabase getRootAsControlPointDatabase(ByteBuffer _bb, ControlPointDatabase obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public ControlPointDatabase __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public ControlPointMsg controlPoints(int j) { return controlPoints(new ControlPointMsg(), j); }
  public ControlPointMsg controlPoints(ControlPointMsg obj, int j) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int controlPointsLength() { int o = __offset(4); return o != 0 ? __vector_len(o) : 0; }

  public static int createControlPointDatabase(FlatBufferBuilder builder,
      int controlPointsOffset) {
    builder.startObject(1);
    ControlPointDatabase.addControlPoints(builder, controlPointsOffset);
    return ControlPointDatabase.endControlPointDatabase(builder);
  }

  public static void startControlPointDatabase(FlatBufferBuilder builder) { builder.startObject(1); }
  public static void addControlPoints(FlatBufferBuilder builder, int controlPointsOffset) { builder.addOffset(0, controlPointsOffset, 0); }
  public static int createControlPointsVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startControlPointsVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static int endControlPointDatabase(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

