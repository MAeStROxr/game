// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class ControlPointMsg extends Table {
  public static ControlPointMsg getRootAsControlPointMsg(ByteBuffer _bb) { return getRootAsControlPointMsg(_bb, new ControlPointMsg()); }
  public static ControlPointMsg getRootAsControlPointMsg(ByteBuffer _bb, ControlPointMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public ControlPointMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID() { int o = __offset(4); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.putLong(o + bb_pos, OID); return true; } else { return false; } }
  public byte cmd() { int o = __offset(6); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateCmd(byte cmd) { int o = __offset(6); if (o != 0) { bb.put(o + bb_pos, cmd); return true; } else { return false; } }
  public long subjectOID() { int o = __offset(8); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateSubjectOID(long subjectOID) { int o = __offset(8); if (o != 0) { bb.putLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public ArmyMsg garrison() { return garrison(new ArmyMsg()); }
  public ArmyMsg garrison(ArmyMsg obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public ArmyMsg garrisonedArmies(int j) { return garrisonedArmies(new ArmyMsg(), j); }
  public ArmyMsg garrisonedArmies(ArmyMsg obj, int j) { int o = __offset(12); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int garrisonedArmiesLength() { int o = __offset(12); return o != 0 ? __vector_len(o) : 0; }
  public ArmyMsg armies(int j) { return armies(new ArmyMsg(), j); }
  public ArmyMsg armies(ArmyMsg obj, int j) { int o = __offset(14); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int armiesLength() { int o = __offset(14); return o != 0 ? __vector_len(o) : 0; }
  public byte action() { int o = __offset(16); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateAction(byte action) { int o = __offset(16); if (o != 0) { bb.put(o + bb_pos, action); return true; } else { return false; } }
  public String subjectName() { int o = __offset(18); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer subjectNameAsByteBuffer() { return __vector_as_bytebuffer(18, 1); }
  public long controllingOID() { int o = __offset(20); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateControllingOID(long controllingOID) { int o = __offset(20); if (o != 0) { bb.putLong(o + bb_pos, controllingOID); return true; } else { return false; } }
  public String controllingName() { int o = __offset(22); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer controllingNameAsByteBuffer() { return __vector_as_bytebuffer(22, 1); }
  public long enablingOID() { int o = __offset(24); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateEnablingOID(long enablingOID) { int o = __offset(24); if (o != 0) { bb.putLong(o + bb_pos, enablingOID); return true; } else { return false; } }
  public String enablingName() { int o = __offset(26); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer enablingNameAsByteBuffer() { return __vector_as_bytebuffer(26, 1); }
  public boolean capturable() { int o = __offset(28); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateCapturable(boolean capturable) { int o = __offset(28); if (o != 0) { bb.put(o + bb_pos, (byte)(capturable ? 1 : 0)); return true; } else { return false; } }
  public boolean captured() { int o = __offset(30); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateCaptured(boolean captured) { int o = __offset(30); if (o != 0) { bb.put(o + bb_pos, (byte)(captured ? 1 : 0)); return true; } else { return false; } }
  public String name() { int o = __offset(32); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer nameAsByteBuffer() { return __vector_as_bytebuffer(32, 1); }
  public Vec3 position() { return position(new Vec3()); }
  public Vec3 position(Vec3 obj) { int o = __offset(34); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }

  public static void startControlPointMsg(FlatBufferBuilder builder) { builder.startObject(16); }
  public static void addOID(FlatBufferBuilder builder, long OID) { builder.addLong(0, OID, 0); }
  public static void addCmd(FlatBufferBuilder builder, byte cmd) { builder.addByte(1, cmd, 0); }
  public static void addSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.addLong(2, subjectOID, 0); }
  public static void addGarrison(FlatBufferBuilder builder, int garrisonOffset) { builder.addOffset(3, garrisonOffset, 0); }
  public static void addGarrisonedArmies(FlatBufferBuilder builder, int garrisonedArmiesOffset) { builder.addOffset(4, garrisonedArmiesOffset, 0); }
  public static int createGarrisonedArmiesVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startGarrisonedArmiesVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addArmies(FlatBufferBuilder builder, int armiesOffset) { builder.addOffset(5, armiesOffset, 0); }
  public static int createArmiesVector(FlatBufferBuilder builder, int[] data) { builder.startVector(4, data.length, 4); for (int i = data.length - 1; i >= 0; i--) builder.addOffset(data[i]); return builder.endVector(); }
  public static void startArmiesVector(FlatBufferBuilder builder, int numElems) { builder.startVector(4, numElems, 4); }
  public static void addAction(FlatBufferBuilder builder, byte action) { builder.addByte(6, action, 0); }
  public static void addSubjectName(FlatBufferBuilder builder, int subjectNameOffset) { builder.addOffset(7, subjectNameOffset, 0); }
  public static void addControllingOID(FlatBufferBuilder builder, long controllingOID) { builder.addLong(8, controllingOID, 0); }
  public static void addControllingName(FlatBufferBuilder builder, int controllingNameOffset) { builder.addOffset(9, controllingNameOffset, 0); }
  public static void addEnablingOID(FlatBufferBuilder builder, long enablingOID) { builder.addLong(10, enablingOID, 0); }
  public static void addEnablingName(FlatBufferBuilder builder, int enablingNameOffset) { builder.addOffset(11, enablingNameOffset, 0); }
  public static void addCapturable(FlatBufferBuilder builder, boolean capturable) { builder.addBoolean(12, capturable, false); }
  public static void addCaptured(FlatBufferBuilder builder, boolean captured) { builder.addBoolean(13, captured, false); }
  public static void addName(FlatBufferBuilder builder, int nameOffset) { builder.addOffset(14, nameOffset, 0); }
  public static void addPosition(FlatBufferBuilder builder, int positionOffset) { builder.addStruct(15, positionOffset, 0); }
  public static int endControlPointMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

