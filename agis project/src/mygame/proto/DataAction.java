// automatically generated, do not modify

package mygame.proto;

public final class DataAction {
  private DataAction() { }
  public static final byte NONE = 0;
  public static final byte REPLACE = 1;
  public static final byte SET = 2;
  public static final byte GET = 3;
  public static final byte ADD = 4;
  public static final byte REMOVE = 5;

  private static final String[] names = { "NONE", "REPLACE", "SET", "GET", "ADD", "REMOVE", };

  public static String name(int e) { return names[e]; }
};

