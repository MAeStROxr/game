// automatically generated, do not modify

package mygame.proto;

public final class PointCmd {
  private PointCmd() { }
  public static final byte POINT_INFO = 0;
  public static final byte POINT_CAPTURE = 1;
  public static final byte POINT_ENABLE = 2;
  public static final byte POINT_ARMY_GARRISON = 3;
  public static final byte POINT_UNIT_GARRISON = 4;
  public static final byte POINT_SIEGE = 5;
  public static final byte POINT_SIEGE_RESULT = 6;

  private static final String[] names = { "POINT_INFO", "POINT_CAPTURE", "POINT_ENABLE", "POINT_ARMY_GARRISON", "POINT_UNIT_GARRISON", "POINT_SIEGE", "POINT_SIEGE_RESULT", };

  public static String name(int e) { return names[e]; }
};

