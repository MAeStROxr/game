// automatically generated, do not modify

package mygame.proto;

public final class Tactical {
  private Tactical() { }
  public static final byte PLAYER_TACTICAL = 0;
  public static final byte BATTLE_COMMAND = 4;

  private static final String[] names = { "PLAYER_TACTICAL", "", "", "", "BATTLE_COMMAND", };

  public static String name(int e) { return names[e]; }
};

