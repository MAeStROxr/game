// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class TacticalMsg extends Table {
  public static TacticalMsg getRootAsTacticalMsg(ByteBuffer _bb) { return getRootAsTacticalMsg(_bb, new TacticalMsg()); }
  public static TacticalMsg getRootAsTacticalMsg(ByteBuffer _bb, TacticalMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public TacticalMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public byte cmd() { int o = __offset(4); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateCmd(byte cmd) { int o = __offset(4); if (o != 0) { bb.put(o + bb_pos, cmd); return true; } else { return false; } }
  public long subjectOID() { int o = __offset(6); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateSubjectOID(long subjectOID) { int o = __offset(6); if (o != 0) { bb.putLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public int battleMsg(int j) { int o = __offset(8); return o != 0 ? bb.get(__vector(o) + j * 1) & 0xFF : 0; }
  public int battleMsgLength() { int o = __offset(8); return o != 0 ? __vector_len(o) : 0; }
  public ByteBuffer battleMsgAsByteBuffer() { return __vector_as_bytebuffer(8, 1); }
  public boolean mutateBattleMsg(int j, int battleMsg) { int o = __offset(8); if (o != 0) { bb.put(__vector(o) + j * 1, (byte)battleMsg); return true; } else { return false; } }
  public byte action() { int o = __offset(10); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateAction(byte action) { int o = __offset(10); if (o != 0) { bb.put(o + bb_pos, action); return true; } else { return false; } }
  public String subjectName() { int o = __offset(12); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer subjectNameAsByteBuffer() { return __vector_as_bytebuffer(12, 1); }
  public ArmyMsg army() { return army(new ArmyMsg()); }
  public ArmyMsg army(ArmyMsg obj) { int o = __offset(14); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static int createTacticalMsg(FlatBufferBuilder builder,
      byte cmd,
      long subjectOID,
      int battleMsgOffset,
      byte action,
      int subjectNameOffset,
      int armyOffset) {
    builder.startObject(6);
    TacticalMsg.addSubjectOID(builder, subjectOID);
    TacticalMsg.addArmy(builder, armyOffset);
    TacticalMsg.addSubjectName(builder, subjectNameOffset);
    TacticalMsg.addBattleMsg(builder, battleMsgOffset);
    TacticalMsg.addAction(builder, action);
    TacticalMsg.addCmd(builder, cmd);
    return TacticalMsg.endTacticalMsg(builder);
  }

  public static void startTacticalMsg(FlatBufferBuilder builder) { builder.startObject(6); }
  public static void addCmd(FlatBufferBuilder builder, byte cmd) { builder.addByte(0, cmd, 0); }
  public static void addSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.addLong(1, subjectOID, 0); }
  public static void addBattleMsg(FlatBufferBuilder builder, int battleMsgOffset) { builder.addOffset(2, battleMsgOffset, 0); }
  public static int createBattleMsgVector(FlatBufferBuilder builder, byte[] data) { builder.startVector(1, data.length, 1); for (int i = data.length - 1; i >= 0; i--) builder.addByte(data[i]); return builder.endVector(); }
  public static void startBattleMsgVector(FlatBufferBuilder builder, int numElems) { builder.startVector(1, numElems, 1); }
  public static void addAction(FlatBufferBuilder builder, byte action) { builder.addByte(3, action, 0); }
  public static void addSubjectName(FlatBufferBuilder builder, int subjectNameOffset) { builder.addOffset(4, subjectNameOffset, 0); }
  public static void addArmy(FlatBufferBuilder builder, int armyOffset) { builder.addOffset(5, armyOffset, 0); }
  public static int endTacticalMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

