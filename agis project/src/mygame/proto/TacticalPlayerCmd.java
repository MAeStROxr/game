// automatically generated, do not modify

package mygame.proto;

public final class TacticalPlayerCmd {
  private TacticalPlayerCmd() { }
  public static final byte PLAYER_TACTICAL_INFO = 0;
  public static final byte PLAYER_UNITS = 1;

  private static final String[] names = { "PLAYER_TACTICAL_INFO", "PLAYER_UNITS", };

  public static String name(int e) { return names[e]; }
};

