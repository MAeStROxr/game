// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class TacticalPlayerMsg extends Table {
  public static TacticalPlayerMsg getRootAsTacticalPlayerMsg(ByteBuffer _bb) { return getRootAsTacticalPlayerMsg(_bb, new TacticalPlayerMsg()); }
  public static TacticalPlayerMsg getRootAsTacticalPlayerMsg(ByteBuffer _bb, TacticalPlayerMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public TacticalPlayerMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public byte cmd() { int o = __offset(4); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateCmd(byte cmd) { int o = __offset(4); if (o != 0) { bb.put(o + bb_pos, cmd); return true; } else { return false; } }
  public long subjectOID() { int o = __offset(6); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateSubjectOID(long subjectOID) { int o = __offset(6); if (o != 0) { bb.putLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public ControlNode playerControl() { return playerControl(new ControlNode()); }
  public ControlNode playerControl(ControlNode obj) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public byte action() { int o = __offset(10); return o != 0 ? bb.get(o + bb_pos) : 0; }
  public boolean mutateAction(byte action) { int o = __offset(10); if (o != 0) { bb.put(o + bb_pos, action); return true; } else { return false; } }
  public String subjectName() { int o = __offset(12); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer subjectNameAsByteBuffer() { return __vector_as_bytebuffer(12, 1); }
  public ArmyMsg army() { return army(new ArmyMsg()); }
  public ArmyMsg army(ArmyMsg obj) { int o = __offset(14); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static int createTacticalPlayerMsg(FlatBufferBuilder builder,
      byte cmd,
      long subjectOID,
      int playerControlOffset,
      byte action,
      int subjectNameOffset,
      int armyOffset) {
    builder.startObject(6);
    TacticalPlayerMsg.addSubjectOID(builder, subjectOID);
    TacticalPlayerMsg.addArmy(builder, armyOffset);
    TacticalPlayerMsg.addSubjectName(builder, subjectNameOffset);
    TacticalPlayerMsg.addPlayerControl(builder, playerControlOffset);
    TacticalPlayerMsg.addAction(builder, action);
    TacticalPlayerMsg.addCmd(builder, cmd);
    return TacticalPlayerMsg.endTacticalPlayerMsg(builder);
  }

  public static void startTacticalPlayerMsg(FlatBufferBuilder builder) { builder.startObject(6); }
  public static void addCmd(FlatBufferBuilder builder, byte cmd) { builder.addByte(0, cmd, 0); }
  public static void addSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.addLong(1, subjectOID, 0); }
  public static void addPlayerControl(FlatBufferBuilder builder, int playerControlOffset) { builder.addOffset(2, playerControlOffset, 0); }
  public static void addAction(FlatBufferBuilder builder, byte action) { builder.addByte(3, action, 0); }
  public static void addSubjectName(FlatBufferBuilder builder, int subjectNameOffset) { builder.addOffset(4, subjectNameOffset, 0); }
  public static void addArmy(FlatBufferBuilder builder, int armyOffset) { builder.addOffset(5, armyOffset, 0); }
  public static int endTacticalPlayerMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

