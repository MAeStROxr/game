// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class TileMsg extends Table {
  public static TileMsg getRootAsTileMsg(ByteBuffer _bb) { return getRootAsTileMsg(_bb, new TileMsg()); }
  public static TileMsg getRootAsTileMsg(ByteBuffer _bb, TileMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public TileMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int tileId() { int o = __offset(4); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateTileId(int tileId) { int o = __offset(4); if (o != 0) { bb.putInt(o + bb_pos, tileId); return true; } else { return false; } }
  public boolean walkable() { int o = __offset(6); return o != 0 ? 0!=bb.get(o + bb_pos) : false; }
  public boolean mutateWalkable(boolean walkable) { int o = __offset(6); if (o != 0) { bb.put(o + bb_pos, (byte)(walkable ? 1 : 0)); return true; } else { return false; } }

  public static int createTileMsg(FlatBufferBuilder builder,
      int tileId,
      boolean walkable) {
    builder.startObject(2);
    TileMsg.addTileId(builder, tileId);
    TileMsg.addWalkable(builder, walkable);
    return TileMsg.endTileMsg(builder);
  }

  public static void startTileMsg(FlatBufferBuilder builder) { builder.startObject(2); }
  public static void addTileId(FlatBufferBuilder builder, int tileId) { builder.addInt(0, tileId, 0); }
  public static void addWalkable(FlatBufferBuilder builder, boolean walkable) { builder.addBoolean(1, walkable, false); }
  public static int endTileMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

