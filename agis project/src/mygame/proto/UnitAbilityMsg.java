// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class UnitAbilityMsg extends Table {
  public static UnitAbilityMsg getRootAsUnitAbilityMsg(ByteBuffer _bb) { return getRootAsUnitAbilityMsg(_bb, new UnitAbilityMsg()); }
  public static UnitAbilityMsg getRootAsUnitAbilityMsg(ByteBuffer _bb, UnitAbilityMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public UnitAbilityMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public UnitMsg activator() { return activator(new UnitMsg()); }
  public UnitMsg activator(UnitMsg obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int abilityID() { int o = __offset(6); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateAbilityID(int abilityID) { int o = __offset(6); if (o != 0) { bb.putInt(o + bb_pos, abilityID); return true; } else { return false; } }
  public TileMsg targetedTile() { return targetedTile(new TileMsg()); }
  public TileMsg targetedTile(TileMsg obj) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public AttackInstanceMsg att() { return att(new AttackInstanceMsg()); }
  public AttackInstanceMsg att(AttackInstanceMsg obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static int createUnitAbilityMsg(FlatBufferBuilder builder,
      int activatorOffset,
      int abilityID,
      int targetedTileOffset,
      int attOffset) {
    builder.startObject(4);
    UnitAbilityMsg.addAtt(builder, attOffset);
    UnitAbilityMsg.addTargetedTile(builder, targetedTileOffset);
    UnitAbilityMsg.addAbilityID(builder, abilityID);
    UnitAbilityMsg.addActivator(builder, activatorOffset);
    return UnitAbilityMsg.endUnitAbilityMsg(builder);
  }

  public static void startUnitAbilityMsg(FlatBufferBuilder builder) { builder.startObject(4); }
  public static void addActivator(FlatBufferBuilder builder, int activatorOffset) { builder.addOffset(0, activatorOffset, 0); }
  public static void addAbilityID(FlatBufferBuilder builder, int abilityID) { builder.addInt(1, abilityID, 0); }
  public static void addTargetedTile(FlatBufferBuilder builder, int targetedTileOffset) { builder.addOffset(2, targetedTileOffset, 0); }
  public static void addAtt(FlatBufferBuilder builder, int attOffset) { builder.addOffset(3, attOffset, 0); }
  public static int endUnitAbilityMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

