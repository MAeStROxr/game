// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class UnitAttackMsg extends Table {
  public static UnitAttackMsg getRootAsUnitAttackMsg(ByteBuffer _bb) { return getRootAsUnitAttackMsg(_bb, new UnitAttackMsg()); }
  public static UnitAttackMsg getRootAsUnitAttackMsg(ByteBuffer _bb, UnitAttackMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public UnitAttackMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public UnitMsg attacking() { return attacking(new UnitMsg()); }
  public UnitMsg attacking(UnitMsg obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public UnitMsg attacked() { return attacked(new UnitMsg()); }
  public UnitMsg attacked(UnitMsg obj) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public AttackInstanceMsg att() { return att(new AttackInstanceMsg()); }
  public AttackInstanceMsg att(AttackInstanceMsg obj) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int shotsFired() { int o = __offset(10); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateShotsFired(int shotsFired) { int o = __offset(10); if (o != 0) { bb.putInt(o + bb_pos, shotsFired); return true; } else { return false; } }

  public static int createUnitAttackMsg(FlatBufferBuilder builder,
      int attackingOffset,
      int attackedOffset,
      int attOffset,
      int shotsFired) {
    builder.startObject(4);
    UnitAttackMsg.addShotsFired(builder, shotsFired);
    UnitAttackMsg.addAtt(builder, attOffset);
    UnitAttackMsg.addAttacked(builder, attackedOffset);
    UnitAttackMsg.addAttacking(builder, attackingOffset);
    return UnitAttackMsg.endUnitAttackMsg(builder);
  }

  public static void startUnitAttackMsg(FlatBufferBuilder builder) { builder.startObject(4); }
  public static void addAttacking(FlatBufferBuilder builder, int attackingOffset) { builder.addOffset(0, attackingOffset, 0); }
  public static void addAttacked(FlatBufferBuilder builder, int attackedOffset) { builder.addOffset(1, attackedOffset, 0); }
  public static void addAtt(FlatBufferBuilder builder, int attOffset) { builder.addOffset(2, attOffset, 0); }
  public static void addShotsFired(FlatBufferBuilder builder, int shotsFired) { builder.addInt(3, shotsFired, 0); }
  public static int endUnitAttackMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

