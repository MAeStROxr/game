// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class UnitMoveMsg extends Table {
  public static UnitMoveMsg getRootAsUnitMoveMsg(ByteBuffer _bb) { return getRootAsUnitMoveMsg(_bb, new UnitMoveMsg()); }
  public static UnitMoveMsg getRootAsUnitMoveMsg(ByteBuffer _bb, UnitMoveMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public UnitMoveMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public UnitMsg unit() { return unit(new UnitMsg()); }
  public UnitMsg unit(UnitMsg obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public TileMsg targetedTile() { return targetedTile(new TileMsg()); }
  public TileMsg targetedTile(TileMsg obj) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static int createUnitMoveMsg(FlatBufferBuilder builder,
      int unitOffset,
      int targetedTileOffset) {
    builder.startObject(2);
    UnitMoveMsg.addTargetedTile(builder, targetedTileOffset);
    UnitMoveMsg.addUnit(builder, unitOffset);
    return UnitMoveMsg.endUnitMoveMsg(builder);
  }

  public static void startUnitMoveMsg(FlatBufferBuilder builder) { builder.startObject(2); }
  public static void addUnit(FlatBufferBuilder builder, int unitOffset) { builder.addOffset(0, unitOffset, 0); }
  public static void addTargetedTile(FlatBufferBuilder builder, int targetedTileOffset) { builder.addOffset(1, targetedTileOffset, 0); }
  public static int endUnitMoveMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

