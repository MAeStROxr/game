// automatically generated, do not modify

package mygame.proto;

import com.google.flatbuffers.FlatBufferBuilder;
import com.google.flatbuffers.Table;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public final class UnitMsg extends Table {
  public static UnitMsg getRootAsUnitMsg(ByteBuffer _bb) { return getRootAsUnitMsg(_bb, new UnitMsg()); }
  public static UnitMsg getRootAsUnitMsg(ByteBuffer _bb, UnitMsg obj) { _bb.order(ByteOrder.LITTLE_ENDIAN); return (obj.__init(_bb.getInt(_bb.position()) + _bb.position(), _bb)); }
  public UnitMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID() { int o = __offset(4); return o != 0 ? bb.getLong(o + bb_pos) : 0; }
  public boolean mutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.putLong(o + bb_pos, OID); return true; } else { return false; } }
  public String unitName() { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; }
  public ByteBuffer unitNameAsByteBuffer() { return __vector_as_bytebuffer(6, 1); }
  public int prefabId() { int o = __offset(8); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutatePrefabId(int prefabId) { int o = __offset(8); if (o != 0) { bb.putInt(o + bb_pos, prefabId); return true; } else { return false; } }
  public int dataId() { int o = __offset(10); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateDataId(int dataId) { int o = __offset(10); if (o != 0) { bb.putInt(o + bb_pos, dataId); return true; } else { return false; } }
  public int tileId() { int o = __offset(12); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateTileId(int tileId) { int o = __offset(12); if (o != 0) { bb.putInt(o + bb_pos, tileId); return true; } else { return false; } }
  public int factionID() { int o = __offset(14); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateFactionID(int factionID) { int o = __offset(14); if (o != 0) { bb.putInt(o + bb_pos, factionID); return true; } else { return false; } }
  public int level() { int o = __offset(16); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateLevel(int level) { int o = __offset(16); if (o != 0) { bb.putInt(o + bb_pos, level); return true; } else { return false; } }
  public float HP() { int o = __offset(18); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateHP(float HP) { int o = __offset(18); if (o != 0) { bb.putFloat(o + bb_pos, HP); return true; } else { return false; } }
  public float AP() { int o = __offset(20); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateAP(float AP) { int o = __offset(20); if (o != 0) { bb.putFloat(o + bb_pos, AP); return true; } else { return false; } }
  public float turnPriority() { int o = __offset(22); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateTurnPriority(float turnPriority) { int o = __offset(22); if (o != 0) { bb.putFloat(o + bb_pos, turnPriority); return true; } else { return false; } }
  public int moveRange() { int o = __offset(24); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateMoveRange(int moveRange) { int o = __offset(24); if (o != 0) { bb.putInt(o + bb_pos, moveRange); return true; } else { return false; } }
  public int attackRange() { int o = __offset(26); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateAttackRange(int attackRange) { int o = __offset(26); if (o != 0) { bb.putInt(o + bb_pos, attackRange); return true; } else { return false; } }
  public float hitChance() { int o = __offset(28); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateHitChance(float hitChance) { int o = __offset(28); if (o != 0) { bb.putFloat(o + bb_pos, hitChance); return true; } else { return false; } }
  public float dodgeChance() { int o = __offset(30); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDodgeChance(float dodgeChance) { int o = __offset(30); if (o != 0) { bb.putFloat(o + bb_pos, dodgeChance); return true; } else { return false; } }
  public float damageMin() { int o = __offset(32); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDamageMin(float damageMin) { int o = __offset(32); if (o != 0) { bb.putFloat(o + bb_pos, damageMin); return true; } else { return false; } }
  public float damageMax() { int o = __offset(34); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDamageMax(float damageMax) { int o = __offset(34); if (o != 0) { bb.putFloat(o + bb_pos, damageMax); return true; } else { return false; } }
  public float critChance() { int o = __offset(36); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateCritChance(float critChance) { int o = __offset(36); if (o != 0) { bb.putFloat(o + bb_pos, critChance); return true; } else { return false; } }
  public float critAvoidance() { int o = __offset(38); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateCritAvoidance(float critAvoidance) { int o = __offset(38); if (o != 0) { bb.putFloat(o + bb_pos, critAvoidance); return true; } else { return false; } }
  public float critMultiplier() { int o = __offset(40); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateCritMultiplier(float critMultiplier) { int o = __offset(40); if (o != 0) { bb.putFloat(o + bb_pos, critMultiplier); return true; } else { return false; } }
  public float stunChance() { int o = __offset(42); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateStunChance(float stunChance) { int o = __offset(42); if (o != 0) { bb.putFloat(o + bb_pos, stunChance); return true; } else { return false; } }
  public float stunAvoidance() { int o = __offset(44); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateStunAvoidance(float stunAvoidance) { int o = __offset(44); if (o != 0) { bb.putFloat(o + bb_pos, stunAvoidance); return true; } else { return false; } }
  public int stunDuration() { int o = __offset(46); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateStunDuration(int stunDuration) { int o = __offset(46); if (o != 0) { bb.putInt(o + bb_pos, stunDuration); return true; } else { return false; } }
  public float silentChance() { int o = __offset(48); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateSilentChance(float silentChance) { int o = __offset(48); if (o != 0) { bb.putFloat(o + bb_pos, silentChance); return true; } else { return false; } }
  public float silentAvoidance() { int o = __offset(50); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateSilentAvoidance(float silentAvoidance) { int o = __offset(50); if (o != 0) { bb.putFloat(o + bb_pos, silentAvoidance); return true; } else { return false; } }
  public int silentDuration() { int o = __offset(52); return o != 0 ? bb.getInt(o + bb_pos) : 0; }
  public boolean mutateSilentDuration(int silentDuration) { int o = __offset(52); if (o != 0) { bb.putInt(o + bb_pos, silentDuration); return true; } else { return false; } }
  public float HPPerTurn() { int o = __offset(54); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateHPPerTurn(float HPPerTurn) { int o = __offset(54); if (o != 0) { bb.putFloat(o + bb_pos, HPPerTurn); return true; } else { return false; } }
  public float APPerTurn() { int o = __offset(56); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateAPPerTurn(float APPerTurn) { int o = __offset(56); if (o != 0) { bb.putFloat(o + bb_pos, APPerTurn); return true; } else { return false; } }
  public float defaultHP() { int o = __offset(58); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDefaultHP(float defaultHP) { int o = __offset(58); if (o != 0) { bb.putFloat(o + bb_pos, defaultHP); return true; } else { return false; } }
  public float defaultAP() { int o = __offset(60); return o != 0 ? bb.getFloat(o + bb_pos) : 0; }
  public boolean mutateDefaultAP(float defaultAP) { int o = __offset(60); if (o != 0) { bb.putFloat(o + bb_pos, defaultAP); return true; } else { return false; } }

  public static int createUnitMsg(FlatBufferBuilder builder,
      long OID,
      int unitNameOffset,
      int prefabId,
      int dataId,
      int tileId,
      int factionID,
      int level,
      float HP,
      float AP,
      float turnPriority,
      int moveRange,
      int attackRange,
      float hitChance,
      float dodgeChance,
      float damageMin,
      float damageMax,
      float critChance,
      float critAvoidance,
      float critMultiplier,
      float stunChance,
      float stunAvoidance,
      int stunDuration,
      float silentChance,
      float silentAvoidance,
      int silentDuration,
      float HPPerTurn,
      float APPerTurn,
      float defaultHP,
      float defaultAP) {
    builder.startObject(29);
    UnitMsg.addOID(builder, OID);
    UnitMsg.addDefaultAP(builder, defaultAP);
    UnitMsg.addDefaultHP(builder, defaultHP);
    UnitMsg.addAPPerTurn(builder, APPerTurn);
    UnitMsg.addHPPerTurn(builder, HPPerTurn);
    UnitMsg.addSilentDuration(builder, silentDuration);
    UnitMsg.addSilentAvoidance(builder, silentAvoidance);
    UnitMsg.addSilentChance(builder, silentChance);
    UnitMsg.addStunDuration(builder, stunDuration);
    UnitMsg.addStunAvoidance(builder, stunAvoidance);
    UnitMsg.addStunChance(builder, stunChance);
    UnitMsg.addCritMultiplier(builder, critMultiplier);
    UnitMsg.addCritAvoidance(builder, critAvoidance);
    UnitMsg.addCritChance(builder, critChance);
    UnitMsg.addDamageMax(builder, damageMax);
    UnitMsg.addDamageMin(builder, damageMin);
    UnitMsg.addDodgeChance(builder, dodgeChance);
    UnitMsg.addHitChance(builder, hitChance);
    UnitMsg.addAttackRange(builder, attackRange);
    UnitMsg.addMoveRange(builder, moveRange);
    UnitMsg.addTurnPriority(builder, turnPriority);
    UnitMsg.addAP(builder, AP);
    UnitMsg.addHP(builder, HP);
    UnitMsg.addLevel(builder, level);
    UnitMsg.addFactionID(builder, factionID);
    UnitMsg.addTileId(builder, tileId);
    UnitMsg.addDataId(builder, dataId);
    UnitMsg.addPrefabId(builder, prefabId);
    UnitMsg.addUnitName(builder, unitNameOffset);
    return UnitMsg.endUnitMsg(builder);
  }

  public static void startUnitMsg(FlatBufferBuilder builder) { builder.startObject(29); }
  public static void addOID(FlatBufferBuilder builder, long OID) { builder.addLong(0, OID, 0); }
  public static void addUnitName(FlatBufferBuilder builder, int unitNameOffset) { builder.addOffset(1, unitNameOffset, 0); }
  public static void addPrefabId(FlatBufferBuilder builder, int prefabId) { builder.addInt(2, prefabId, 0); }
  public static void addDataId(FlatBufferBuilder builder, int dataId) { builder.addInt(3, dataId, 0); }
  public static void addTileId(FlatBufferBuilder builder, int tileId) { builder.addInt(4, tileId, 0); }
  public static void addFactionID(FlatBufferBuilder builder, int factionID) { builder.addInt(5, factionID, 0); }
  public static void addLevel(FlatBufferBuilder builder, int level) { builder.addInt(6, level, 0); }
  public static void addHP(FlatBufferBuilder builder, float HP) { builder.addFloat(7, HP, 0); }
  public static void addAP(FlatBufferBuilder builder, float AP) { builder.addFloat(8, AP, 0); }
  public static void addTurnPriority(FlatBufferBuilder builder, float turnPriority) { builder.addFloat(9, turnPriority, 0); }
  public static void addMoveRange(FlatBufferBuilder builder, int moveRange) { builder.addInt(10, moveRange, 0); }
  public static void addAttackRange(FlatBufferBuilder builder, int attackRange) { builder.addInt(11, attackRange, 0); }
  public static void addHitChance(FlatBufferBuilder builder, float hitChance) { builder.addFloat(12, hitChance, 0); }
  public static void addDodgeChance(FlatBufferBuilder builder, float dodgeChance) { builder.addFloat(13, dodgeChance, 0); }
  public static void addDamageMin(FlatBufferBuilder builder, float damageMin) { builder.addFloat(14, damageMin, 0); }
  public static void addDamageMax(FlatBufferBuilder builder, float damageMax) { builder.addFloat(15, damageMax, 0); }
  public static void addCritChance(FlatBufferBuilder builder, float critChance) { builder.addFloat(16, critChance, 0); }
  public static void addCritAvoidance(FlatBufferBuilder builder, float critAvoidance) { builder.addFloat(17, critAvoidance, 0); }
  public static void addCritMultiplier(FlatBufferBuilder builder, float critMultiplier) { builder.addFloat(18, critMultiplier, 0); }
  public static void addStunChance(FlatBufferBuilder builder, float stunChance) { builder.addFloat(19, stunChance, 0); }
  public static void addStunAvoidance(FlatBufferBuilder builder, float stunAvoidance) { builder.addFloat(20, stunAvoidance, 0); }
  public static void addStunDuration(FlatBufferBuilder builder, int stunDuration) { builder.addInt(21, stunDuration, 0); }
  public static void addSilentChance(FlatBufferBuilder builder, float silentChance) { builder.addFloat(22, silentChance, 0); }
  public static void addSilentAvoidance(FlatBufferBuilder builder, float silentAvoidance) { builder.addFloat(23, silentAvoidance, 0); }
  public static void addSilentDuration(FlatBufferBuilder builder, int silentDuration) { builder.addInt(24, silentDuration, 0); }
  public static void addHPPerTurn(FlatBufferBuilder builder, float HPPerTurn) { builder.addFloat(25, HPPerTurn, 0); }
  public static void addAPPerTurn(FlatBufferBuilder builder, float APPerTurn) { builder.addFloat(26, APPerTurn, 0); }
  public static void addDefaultHP(FlatBufferBuilder builder, float defaultHP) { builder.addFloat(27, defaultHP, 0); }
  public static void addDefaultAP(FlatBufferBuilder builder, float defaultAP) { builder.addFloat(28, defaultAP, 0); }
  public static int endUnitMsg(FlatBufferBuilder builder) {
    int o = builder.endObject();
    return o;
  }
};

