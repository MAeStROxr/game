package mygame.tactical;

import atavism.agis.objects.CombatInfo;
import atavism.agis.objects.PlayerFactionData;
import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.CombatClient;
import atavism.agis.plugins.ControlClient;
import atavism.agis.plugins.FactionPlugin;
import atavism.msgsys.Message;
import atavism.msgsys.MessageCallback;
import atavism.msgsys.SubjectFilter;
import atavism.server.engine.*;
import atavism.server.math.Point;
import atavism.server.objects.*;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.MobManagerPlugin;
import atavism.server.plugins.ObjectManagerClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Logger;
import mygame.Proto;
import mygame.proto.*;

import java.io.Serializable;
import java.util.*;
import java.util.function.BiConsumer;

public class Battle implements Serializable, MessageCallback, Runnable {


    private final static Logger battleLog = new Logger("Battle");
    HashSet<OID> playersInRange = new HashSet<>();
    private Proto proto = new Proto();
    private OID battleOID;
    private int currentFactionId = -1;
    private Map<OID, TacticalPlayer> players = new HashMap<>();
    private Map<Integer, TacticalPlayer> armyIds = new HashMap<>();
    private Map<Long, UnitMsg> liveUnits = new HashMap<Long, UnitMsg>();
    private Map<Long, UnitMsg> deadUnits = new HashMap<Long, UnitMsg>();
    private _GamePhase battlePhase = _GamePhase.Initialization;
    private BattleMsg battle;
    private boolean battleOver = false;
    private boolean isSiege = false;
    private boolean active = false;
    private long pointOid = -1;
    private boolean dirty = true;
    private static final int secondsPerTurn = 45, minutesBattleDuration = 15;
    private OID instanceOid;
    private String gameObject = "EmptyBattle";
    private Point battlePosition;
    private TacticalPlayer battleStarter;
    private HashMap<String, Serializable> props;
    private BiConsumer<OID, BattleMsg> sendBattleMsg, sendLongBattleMsg;
    private BattleEntity battleEntity;
    private Timer timer = new Timer();
    private TimerTask endTurnTask = new EndTurnTask(), endBattleTask = new EndBattleTask();
    private class EndTurnTask extends TimerTask {
        public void run(){
            endTurn();
        }
    }
    private class EndBattleTask extends TimerTask {
        public void run(){
            endBattle();
        }
    }
    public Battle(OID instance, TacticalPlayer starter, BiConsumer<OID, BattleMsg> battleSender, BiConsumer<OID, BattleMsg> longBattleSender, Vec3 pos) {
        sendLongBattleMsg = longBattleSender;
        instanceOid = instance;
        sendBattleMsg = battleSender;
        battleStarter = starter;
        starter.setBattle(this);
        setBattlePosition(new Point(pos.x(), pos.y(), pos.z()));
    }

    public void setBattle(BattleMsg battleMsg) {
        this.battle = battleMsg;
        setBattlePosition(new Point(battleMsg.position().x(), battleMsg.position().y(), battleMsg.position().z()));
    }

    public TacticalPlayer getPlayer(OID playerOID) {
        return players.get(playerOID);
    }

    public TacticalPlayer getPlayer(long playerOID) {
        return players.get(OID.fromLong(playerOID));
    }

    public Collection<TacticalPlayer> getAllPlayers() {
        return players.values();
    }

    public boolean addPlayerToBattle(TacticalPlayer player) {
        if (players.containsKey(player.getOID())) return false;
        player.setBattle(this);
        OID playerOid = player.getOID();
        boolean pointGarrison = isSiege() && player.getOID().equals(OID.fromLong(getPointOid()));
        if (pointGarrison) {
            battleLog.debug("adding point garrison to siege battle, armies of garrison player:\n" + proto.logArmyCollection(player.getArmies()));
            ArmyMsg garrisonArmy = player.getArmies().iterator().next();
            long pointController = player.getControllerOid();
            if (pointController<=0) {
                players.values().forEach(p->{
                    p.addAttackable(player.getOID());
                    player.addAttackable(p.getOID());
                });
                battleLog.debug("no point controller");
                return players.putIfAbsent(player.getOID(), player) == null;
            }  else  playerOid = OID.fromLong(pointController);

        } //else if (playersInRange.contains(player.getOID())) playersInRange.remove(player.getOID());
        int playerFaction = (Integer) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "faction");
        player.setFaction(playerFaction);
        HashMap<Integer, PlayerFactionData> playerPfdMap = (HashMap) EnginePlugin.getObjectProperty(playerOid, WorldManagerClient.NAMESPACE, "factionData");
        player.setPfdMap(playerPfdMap);
        battleLog.debug("player added to battle:" + player.getOID().toLong()+" faction:"+playerFaction);
        for (TacticalPlayer p : players.values()) {
            if (player.getStanding(p.getFaction()) <= FactionPlugin.Neutral ||
                    p.getStanding(player.getFaction()) <= FactionPlugin.Neutral) {
                player.addAttackable(p.getOID());
                p.addAttackable(player.getOID());
                battleLog.debug("adding attackable player from faction data:" + p.getOID().toLong()+" othersFaction:"+p.getFaction());
            } else {
                battleLog.debug("non-attackable player by factionData:" + p.getOID().toLong()+" othersFaction:"+p.getFaction());
            }
            battleLog.debug("other player:"+p.getOID().toLong()+" faction:"+p.getFaction());
        }
        dirty = true;
        if (!pointGarrison) {
            EnginePlugin.setObjectPropertyNoResponse(player.getOID(), WorldManagerClient.NAMESPACE, "inBattle", true);
            EnginePlugin.setObjectPropertyNoResponse(player.getOID(), CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_ATTACKABLE, false);
        }
        return players.putIfAbsent(player.getOID(), player) == null;

    }

    public void join(TacticalPlayer player) {
        addPlayerToBattle(player);
        for (ArmyMsg a : player.getArmies()) {
            armyIds.put(a.factionID(), player);
        }
        player.joinBattle(this);
        liveUnits.putAll(player.getLiveUnits());
        dirty = true;
    }

    public boolean didAllJoin() {
        return players.values().stream().allMatch(p -> p.isJoined());
    }

    public int getCurrentFactionId() {
        return currentFactionId;
    }

    public boolean isDeploymentComplete() {
        for (TacticalPlayer p : players.values())
            if (!p.isDeployed()) {
                battleLog.debug("player " + p.getOID().toLong() + " not deployed");
                return false;
            }
        return true;
    }

    public void dirty() {
        dirty = true;
    }

    public BattleMsg getBattleImage() {
        if (dirty) {
            battle = proto.getBattleMsg(this, BattleCmd.BATTLE_INFO, 2, getAllArmies(), proto.getBattleMsgTiles(battle));
            Map<OID, List<ArmyMsg>> playerArmies = proto.getPlayerArmies(battle);
            playerArmies.forEach((playerOid, armyList) -> {
                TacticalPlayer p = players.get(playerOid);
                armyList.forEach(a -> {
                    p.updateArmy(a);
                    proto.getArmyUnits(a).forEach(u -> {
                        liveUnits.replace(u.OID(), u);
                        deadUnits.replace(u.OID(), u);
                    });
                });
            });
            dirty = false;
        }
        return battle;
    }


    public _GamePhase getPhase() {
        return battlePhase;
    }

    public void start() {
        battlePhase = _GamePhase.UnitDeployment;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE,minutesBattleDuration);
        timer.schedule(endBattleTask, cal.getTime());
    }

    public void commence() {
        battlePhase = _GamePhase.Play;
        //endTurn();
    }


    public void endTurn() {
        endTurnTask.cancel();
        timer.purge();
        endTurnTask = new EndTurnTask();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND,secondsPerTurn);
        timer.schedule(endTurnTask,cal.getTime());
        currentFactionId++;
        currentFactionId = currentFactionId % players.size();
        sendAll(proto.createEndTurn(this));
    }

    public void removePlayer(OID oid) {
        TacticalPlayer player = players.get(oid);
        if (player == null) return;
        players.remove(oid);
        player.setBattle(null);

    }


    public boolean isVictoryConditionMet() {
        return players.values().stream().anyMatch(player ->player.getLiveUnits().size()>0 && !players.values().stream().anyMatch(otherPlayer ->
                (otherPlayer.isAttackable(player.getOID()) && otherPlayer.getLiveUnits().size() > 0)));
    }

    public void endBattle() {
        if (battleOver) return;
        timer.cancel();
        timer.purge();
        battleOver = true;
        WorldManagerClient.despawn(battleOID);
        BattleMsg endBattleMsg = proto.getBattleMsg(this, BattleCmd.BATTLE_OVER, 1);
        sendAll(endBattleMsg);
        int totalXP = getXP();
        int totalCash = getCash();
        Collection<TacticalPlayer> winningPlayers = getWinningPlayers(), allPlayers = getAllPlayers(), losingPlayers = new HashSet<>();
        losingPlayers.addAll(allPlayers);
        losingPlayers.removeAll(winningPlayers);
        battleLog.debug("ending battle with " + winningPlayers.size() + " winning players and " + allPlayers.size() +
                " players overall:" + battleOID + " siege:" + battle.isSiege() + " totalXP:" + totalXP + " totalCash:" +
                totalCash);
        if (isSiege()) {
            List<ArmyMsg> newArmiesGarrison = new ArrayList<>(), defeatedArmies = new ArrayList<>();
            TacticalPlayer garrisonPlayer = players.get(OID.fromLong(getPointOid()));
            ArmyMsg newUnitsGarrison = null;
            if (garrisonPlayer != null) {
                allPlayers.remove(garrisonPlayer);
                winningPlayers.remove(garrisonPlayer);
                losingPlayers.remove(garrisonPlayer);
                newUnitsGarrison = garrisonPlayer.getArmiesMap().get(getPointOid());
            }
            for (TacticalPlayer p : winningPlayers) newArmiesGarrison.addAll(p.getArmies());
            for (TacticalPlayer p : losingPlayers) {
                defeatedArmies.addAll(p.getArmies());
            }
            ControlPointMsg pointMsg = proto.createPointMsg(getPointOid(), "", PointCmd.POINT_SIEGE_RESULT, newUnitsGarrison, newArmiesGarrison, defeatedArmies, DataAction.REPLACE);
            battleLog.debug("sige won battle, winning players and new armies garrison:" + proto.logArmyCollection(newArmiesGarrison)
                    + "\ndefeated players and to be exiting control point:" + proto.logArmyCollection(defeatedArmies));
            for (TacticalPlayer p : allPlayers) {
                battleLog.debug("sending player " + p.getOID().toLong() + " to instance:" + p.getInstanceOid());
                BasicWorldNode node = new BasicWorldNode();
                node.setInstanceOid(p.getInstanceOid());
                Vec3 v = p.getArmiesMap().values().stream().filter(a -> a.OID() == p.getOID().toLong()).findFirst().get().position();
                node.setLoc(new Point(v.x(), v.y() + 20, v.z()));
                InstanceClient.objectInstanceEntry(p.getOID(), node, InstanceClient.InstanceEntryReqMessage.FLAG_PUSH);
            }
            battleLog.debug("siege complete sending to " + allPlayers.size() + " point result:" + proto.pointInfoString(pointMsg));
            proto.sendMessageFlatbuffer(pointMsg.getByteBuffer(), ControlClient.MSG_TYPE_CONTROL_POINT, "ControlPointMsg");
        } else {
            clearAllDeadUnits();
        }
        for (TacticalPlayer p : losingPlayers) {
            battleLog.debug("setting dead state for player:" + p.getOID().toLong());
            EnginePlugin.setObjectPropertyNoResponse(p.getOID(), CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_DEADSTATE, true);
        }
        for (TacticalPlayer p : winningPlayers) {
            CombatClient.alterExp(p.getOID(), totalXP / winningPlayers.size());
            AgisInventoryClient.alterCurrency(p.getOID(), 1, totalCash / winningPlayers.size());
        }
        for (TacticalPlayer player : allPlayers) {
            player.setBattle(null);
            EnginePlugin.setObjectPropertyNoResponse(player.getOID(), WorldManagerClient.NAMESPACE, "inBattle", false);
            EnginePlugin.setObjectPropertyNoResponse(player.getOID(), CombatClient.NAMESPACE, CombatInfo.COMBAT_PROP_ATTACKABLE, true);
        }
    }

    public boolean isBattleOver() {
        return battleOver;
    }


    public OID getBattleOID() {
        return battleOID;
    }

    public void unitMove(UnitMoveMsg moveMsg) {
        battleLog.debug("moving unit:" + moveMsg.unit().OID() + " to tileId:" + moveMsg.targetedTile().tileId());
        UnitMsg u = liveUnits.get(moveMsg.unit().OID());
        //armyIds.get(moveMsg.unit().factionID()).getLiveUnits().get(moveMsg.unit().OID());
        if (u!=null)
            u.mutateTileId(moveMsg.targetedTile().tileId());
        battleLog.debug("after move unit tileId:" + u.tileId());
    }

    public void unitAttack(UnitAttackMsg attackMsg) {
        UnitMsg attacked = attackMsg.attacked();
        TacticalPlayer p = armyIds.get(attacked.factionID());
        Map<Long, UnitMsg> playerLiveUnits = p.getLiveUnits();
        battleLog.debug("attacking unit:" + attackMsg.attacking().OID() + " attacked unit:" + attackMsg.attacked().OID() +
                " attacked factionID:" + attacked.factionID() + " attacked player oid:" + p.getOID().toLong() + " isCounter:" + attackMsg.att().isCounter() +
                " damage:" + attackMsg.att().damage() + " shotsFired:" + attackMsg.shotsFired() + " player's live units:" + proto.logUnitCollection(playerLiveUnits.values()));
        attacked = playerLiveUnits.get(attackMsg.attacked().OID());
        battleLog.debug(" attackedFactionId:" + attacked.factionID() + " attacked HP before:" + attacked.HP());
        attacked.mutateHP(attacked.HP() - attackMsg.att().damage() * attackMsg.shotsFired());
        if (attacked.HP() <= 0f) {
            battleLog.debug("attacked unit dead, player's live units before:" + proto.logUnitCollection(playerLiveUnits.values()));
            p.killUnit(attacked.OID());
            deadUnits.put(attacked.OID(), liveUnits.get(attacked.OID()));
            liveUnits.remove(attacked.OID());
            p.getLiveUnits().forEach((uOid, uMsg) -> battleLog.debug("live unit: oid-" + uOid + " HP-" + uMsg.HP() + " factionId-" + uMsg.factionID()));
            battleLog.debug("player's live units after:" + proto.logUnitCollection(playerLiveUnits.values()));

        }
        battleLog.debug(" attacked HP after:" + attacked.HP());
    }

    public void unitAbility(UnitAbilityMsg abilityMsg) {

    }

    public int creditedDeadUnits() {
        HashSet<TacticalPlayer> losingPlayers = new HashSet<>();
        losingPlayers.addAll(players.values());
        losingPlayers.removeAll(getWinningPlayers());
        Integer creditedUnits = 0;
        for (TacticalPlayer p : losingPlayers) {
            battleLog.debug("losing player " + p.getOID() + " with " + p.getDeadUnits().size() + " dead units");
            creditedUnits += p.getDeadUnits().size();
        }
        return creditedUnits;
    }

    public int getXP() {
        return creditedDeadUnits() * 50;
    }

    public int getCash() {
        return getXP() / 20;
    }

    public Set<TacticalPlayer> getWinningPlayers() {
        Set<TacticalPlayer> winners = new HashSet<>();
        battleLog.debug("getting winning players");
        players.values().forEach(p -> {
            battleLog.debug("player " + p.getOID() + " with " + p.getLiveUnits().size() + " live units");
            if (p.getLiveUnits().size() > 0) {
                winners.add(p);
            }
        });
        return winners;
    }

    public boolean isSiege() {
        return isSiege;
    }

    public void setSiege(long pointOID) {
        this.pointOid = pointOID;
        isSiege = true;
        battlePosition = new Point(0, 0, 0);
    }

    public long getPointOid() {
        return pointOid;
    }

    public void setInstanceOid(OID instanceOid1) {
        instanceOid = instanceOid1;
    }

    //First one is the garrison
    public List<ArmyMsg> getAllArmies() {
        List<ArmyMsg> armies = new ArrayList<>();
        players.values().forEach(p -> armies.addAll(p.getArmiesMap().values()));
        return armies;
    }


    public void clearAllDeadUnits() {
        BattleManager.clearDeadUnitsFromInventory(getAllArmies());
        getAllPlayers().forEach(p -> p.clearDeadUnits());
    }

    public Point getBattlePosition() {
        return battlePosition;
    }

    public void setBattlePosition(Point p) {
        battlePosition = p;
    }

    public void deployArmy(ArmyMsg deploying) {
        TacticalPlayer p = players.get(OID.fromLong(deploying.OID()));
        ArmyMsg deployed = p.getArmiesMap().get(deploying.OID());
        UnitMsg u1 = new UnitMsg(), u2 = new UnitMsg();
        battleLog.debug("deploy army called, player:"+p.getOID().toLong()+" armyFaction:"+deployed.factionID()+" armyOID:"+deployed.OID()+"currently deployed army before deployment:" + proto.armyInfoString(deployed));
        List<UnitMsg> deployingUnits = proto.getArmyUnits(deploying), deployedUnits = proto.getArmyUnits(deployed);
        deployingUnits.forEach(u -> deployedUnits.stream().filter(deployedUnit -> deployedUnit.OID() == u.OID()).findFirst().get().mutateTileId(u.tileId()));
        //battleLog.debug("deployed army:" + proto.armyInfoString(deployed));
        p.setDeployed(true);
    }

    public void spawn() {
        Template markerTemplate = new Template();
        markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_NAME, "_ign_" + "battle");
        markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_OBJECT_TYPE, ObjectTypes.mob);
        markerTemplate.put(WorldManagerClient.NAMESPACE, WorldManagerClient.TEMPL_PERCEPTION_RADIUS, 75);
        markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_INSTANCE, instanceOid);
        markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_LOC, battlePosition);
        //markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_ORIENT, orientation);
        DisplayContext dc = new DisplayContext(gameObject, true);
        dc.addSubmesh(new DisplayContext.Submesh("", ""));
        markerTemplate.put(Namespace.WORLD_MANAGER, WorldManagerClient.TEMPL_DISPLAY_CONTEXT, dc);
        markerTemplate.put(Namespace.WORLD_MANAGER, "model", gameObject);
        // Put in any additional props
        if (props != null) {
            for (String propName : props.keySet()) {
                markerTemplate.put(Namespace.WORLD_MANAGER, propName, props.get(propName));
            }
        }
        // Create the object
        OID objectOID = ObjectManagerClient.generateObject(ObjectManagerClient.BASE_TEMPLATE_ID,
                ObjectManagerClient.BASE_TEMPLATE, markerTemplate);
        battleOID = objectOID;
        if (objectOID != null) {
            // Need to create an interpolated world node to add a tracker/reaction radius to the claim world object
            BasicWorldNode bwNode = WorldManagerClient.getWorldNode(objectOID);
            InterpolatedWorldNode iwNode = new InterpolatedWorldNode(bwNode);
            battleEntity = new Battle.BattleEntity(objectOID, iwNode);
            EntityManager.registerEntityByNamespace(battleEntity, Namespace.MOB);
            MobManagerPlugin.getTracker(instanceOid).addLocalObject(objectOID, 100);

            WorldManagerClient.spawn(objectOID);
            battleLog.debug("BATTLE: spawned battle at : " + battlePosition);
            activate();
        }
    }

    @Override
    public void run() {
    }

    public void activate() {
        SubjectFilter filter = new SubjectFilter(battleOID);
        filter.addType(ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS);
        Long eventSub = Engine.getAgent().createSubscription(filter, this);
        // Set the reaction radius tracker to alert the object if a player has entered its draw radius
        MobManagerPlugin.getTracker(instanceOid).addReactionRadius(battleOID, 100);
        active = true;
        battleLog.debug("BATTLE: battle with oid: " + battleOID.toLong() + " activated");
    }

    /**
     * Deals with the messages the instance has picked up.
     */
    public void handleMessage(Message msg, int flags) {
        if (active == false) {
            return;
        }
        if (msg.getMsgType() == ObjectTracker.MSG_TYPE_NOTIFY_REACTION_RADIUS) {
            ObjectTracker.NotifyReactionRadiusMessage nMsg = (ObjectTracker.NotifyReactionRadiusMessage) msg;
            battleLog.debug("BATTLE: myOid=" + battleOID + " objOid=" + nMsg.getSubject()
                    + " inRadius=" + nMsg.getInRadius() + " wasInRadius=" + nMsg.getWasInRadius());
            if (nMsg.getInRadius()) {
                addPlayerListener(nMsg.getSubject());
            } else {
                // Remove subject from targets in range
                removePlayerListener(nMsg.getSubject());
            }
        }
    }

    public HashSet<OID> getPlayersInRange() {
        return playersInRange;
    }

    public void addPlayerListener(OID player) {
        battleLog.debug("adding listener:" + player.toLong());
        if (!players.containsKey(player) && !playersInRange.contains(player)) {
            dirty = true;
            playersInRange.add(player);
            if (battlePhase != _GamePhase.Initialization && battlePhase != _GamePhase.Over) {
            }
        }
    }

    public void removePlayerListener(OID player) {
        if (playersInRange.contains(player)) playersInRange.remove(player);
    }

    public void sendAllListeners(BattleMsg battleMsg) {
        for (OID oid : getPlayersInRange()) {
            sendBattleMsg.accept(oid, battleMsg);
        }
    }
    public void sendBattlePlayers(BattleMsg battleMsg, boolean longMsg) {
        for (TacticalPlayer p : players.values()) {
            battleLog.debug("sendBattlePlayers, playerOid:"+p.getOID().toLong());
            if (isSiege() && p.getOID().equals(OID.fromLong(getPointOid()))) continue;
            if (longMsg)sendLongBattleMsg.accept(p.getOID(), battleMsg);
            else sendBattleMsg.accept(p.getOID(), battleMsg);
        }



    }
    public void sendBattlePlayers(BattleMsg battleMsg) {

        sendBattlePlayers(battleMsg, false);
    }

    public void sendAll(BattleMsg battleMsg) {
        //sendAllListeners(battleMsg);
        sendBattlePlayers(battleMsg);
    }

    public class BattleEntity extends Entity implements EntityWithWorldNode {

        InterpolatedWorldNode node;

        BattleEntity(OID oid, InterpolatedWorldNode node) {
            setWorldNode(node);
            setOid(oid);
        }

        @Override
        public void setDirLocOrient(BasicWorldNode basicWorldNode) {
            if (node != null)
                node.setDirLocOrient(basicWorldNode);
        }

        @Override
        public InterpolatedWorldNode getWorldNode() {
            return node;
        }

        @Override
        public void setWorldNode(InterpolatedWorldNode node) {
            this.node = node;
        }

        @Override
        public Entity getEntity() {
            return null;
        }
    }


}