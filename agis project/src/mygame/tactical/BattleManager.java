package mygame.tactical;

import atavism.agis.plugins.AgisInventoryClient;
import atavism.agis.plugins.TacticalClient;
import atavism.server.engine.BasicWorldNode;
import atavism.server.engine.OID;
import atavism.server.math.Point;
import atavism.server.objects.Template;
import atavism.server.plugins.InstanceClient;
import atavism.server.plugins.WorldManagerClient;
import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.Proto;
import mygame.proto.*;

import java.util.*;

public class BattleManager {
    static Proto proto = new Proto();
    LinkedHashMap<OID, Battle> battles = new LinkedHashMap<>();
    TacticalPlayerManager playerManager;
    protected static final Logger log = new Logger("BattleManager");
    int siegeTemplateId = 30;
    int worldTemplateId = 29;
    long battleOIDs = 100;
    public static final Map<Integer,UnitStats> unitPrefabStats = new HashMap<>();
    static
    {
        unitPrefabStats.put(5,new UnitStats(15));
        unitPrefabStats.put(6, new UnitStats(5));
        unitPrefabStats.put(7, new UnitStats(12));
        unitPrefabStats.put(8, new UnitStats(15));
        unitPrefabStats.put(9, new UnitStats(18));
        unitPrefabStats.put(10, new UnitStats(22));
        unitPrefabStats.put(13, new UnitStats(15));
        unitPrefabStats.put(14, new UnitStats(15));
        unitPrefabStats.put(15, new UnitStats(15));
    }
    public static Map<Integer,UnitStats> getUnitStats() { return unitPrefabStats; }


    public BattleManager() {
        unitPrefabStats.put(5,new UnitStats(15));
        unitPrefabStats.put(6, new UnitStats(5));
        unitPrefabStats.put(7, new UnitStats(12));
        unitPrefabStats.put(8, new UnitStats(15));
        unitPrefabStats.put(9, new UnitStats(18));
        unitPrefabStats.put(10, new UnitStats(22));
        unitPrefabStats.put(13, new UnitStats(15));
        unitPrefabStats.put(14, new UnitStats(15));
        unitPrefabStats.put(15, new UnitStats(15));

    }

    public void setPlayerManager(TacticalPlayerManager playerManager) {
        this.playerManager = playerManager;
    }

    public void processBattle(BattleMsg battleMsg) {
        log.debug("\n"+proto.battleInfoString(battleMsg));
        Battle battle = battles.get(OID.fromLong(battleMsg.OID()));
        // TODO Auto-generated method stub
        switch (battleMsg.cmd()) {
            case BattleCmd.PLAYER_STARTED_BATTLE:
                battleStarted(battleMsg);
                break;
            case BattleCmd.PLAYER_DEPLOYED_ARMY:
                battleDeployed(battleMsg, battle);
                break;
            case BattleCmd.PLAYER_JOINED_BATTLE:
                battleJoined(battleMsg, battle);
                break;
            case BattleCmd.BATTLE_COMMENCED:
                //battleCommenced(battleMsg);
                break;
            case BattleCmd.PLAYER_ENDED_TURN:
                battleTurnEnded(battleMsg, battle);
                break;
            case BattleCmd.BATTLE_OVER:
                //battleOver(battleMsg, battle);
                break;
            case BattleCmd.PLAYER_LEFT_BATTLE:
                battleLeft(battleMsg, battle);
                break;
            case BattleCmd.UNIT_ABILITY:
            case BattleCmd.UNIT_MOVE:
            case BattleCmd.UNIT_ATTACK:
                battleAction(battleMsg, battle);
            default:
                break;
        }

    }

    private void sendBattleMsg(BattleMsg battleMsg, OID playerOid) {
        if (!playerManager.playerExists(playerOid)) {
            log.debug("Player doesn't exit in player base:"
                    + BattleCmd.name(battleMsg.cmd()) + " battleOid:" + battleMsg.OID() + " playerOid:" + playerOid.toLong()
                    + " subjectOid:" + battleMsg.subjectOID());
            return;
        }
        battleMsg.mutateSubjectOID(playerOid.toLong());
        log.debug("Sending Battle Message:"
                + BattleCmd.name(battleMsg.cmd()) + " battleOid:" + battleMsg.OID() + " playerOid:" + playerOid.toLong()
                + " subjectOid:" + battleMsg.subjectOID());

        TacticalClient.sendTargetedBattleMsg(battleMsg, playerOid);

    }

    private void sendLongBattleMsg(BattleMsg battleMsg, OID playerOid) {
        if (!playerManager.playerExists(playerOid)) {
            log.debug("Player doesn't exit in player base:"
                    + BattleCmd.name(battleMsg.cmd()) + " battleOid:" + battleMsg.OID() + " playerOid:" + playerOid.toLong()
                    + " subjectOid:" + battleMsg.subjectOID());
            return;
        }
        battleMsg.mutateSubjectOID(playerOid.toLong());
        log.debug("Sending Long Battle Message:"
                + BattleCmd.name(battleMsg.cmd()) + " battleOid:" + battleMsg.OID() + " playerOid:" + playerOid.toLong()
                + " subjectOid:" + battleMsg.subjectOID());

        TacticalClient.sendLongTargetedBattleMsg(battleMsg, playerOid);

    }


    public static void clearDeadUnitsFromInventory(Collection<ArmyMsg> armies) {
        Collection deadArmies = proto.getDeadUnits(armies);
        log.debug("clearing dead units:" + proto.logArmyCollection(armies));
        HashMap<OID, HashMap<OID, Integer>> removeUnits = proto.getPlayerUnitsQuantities(deadArmies);
        removeUnits.forEach((playerOid, unitsMap) -> {
            log.debug("clearing units of player:" + playerOid.toLong() + " units amount to be cleared:" + unitsMap.size());
            if (unitsMap.size() > 0)
                AgisInventoryClient.removeSpecificItem(playerOid, unitsMap, false);
        });
    }

    private void battleStarted(BattleMsg battleMsg) {
        Map<OID, List<ArmyMsg>> playerArmies = proto.getPlayerArmies(battleMsg);
        int factionCount = 0;
        for (List<ArmyMsg> armyList : playerArmies.values()){

            for (ArmyMsg army : armyList) {
                int currentFaction = factionCount++;
                army.mutateFactionID(currentFaction);
                /*
                float sum = currentFaction%3 + currentFaction%2 + currentFaction;
                if (army.color()!=null) {
                    army.color().mutateX((currentFaction % 3) / sum);
                    army.color().mutateY((currentFaction % 2) / sum);
                    army.color().mutateZ((currentFaction) / sum);
                }*/
                for (int i=0;i<army.armyUnitsLength();i++){

                    UnitMsg unit = army.armyUnits(i);
                    unit.mutateFactionID(currentFaction);
                    unit.mutateDataId(i);
                }
            }
        }
        HashMap<TacticalPlayer, List<ArmyMsg>> battlePlayers = new HashMap<>();
        playerArmies.forEach((oid, list) -> {
            if (playerManager.playerExists(oid)) battlePlayers.put(playerManager.getPlayer(oid), list);
        });
        battlePlayers.forEach((player, armyList) -> {
            OID playerOid = player.getOID();
            player.clearArmies();
            if (player == null) {
                logAndSendError(battleMsg, "start battle called for missing player:" + playerOid);
                return;
            }
            if (player.getBattle() != null) {
                logAndSendError(battleMsg, "start battle called for already engaged player:" + playerOid);
                return;
            }
            armyList.forEach(army -> {
                army.mutateIsNetworkPlayer(true);
                army.mutateRequireDeployment(true);
                player.addArmy(army);
                WorldManagerClient.ObjectInfo objInfo = WorldManagerClient.getObjectInfo(playerOid);
                player.setInstanceOid(objInfo.instanceOid);
            });
        });
        ArmyMsg garrisonArmy = null;

        TacticalPlayer battleInitiater = playerManager.getPlayer(battleMsg.subjectOID()), garrisonPlayer = null;
        OID instanceOid = battleInitiater.getInstanceOid();
        if (battleMsg.isSiege()) {
            log.debug("entering siege:");
            garrisonPlayer = new TacticalPlayer(battleMsg.pointOID(), "Garrison Army");
            if (playerArmies.containsKey(garrisonPlayer.getOID())) {
                garrisonArmy = playerArmies.get(garrisonPlayer.getOID()).get(0);
                garrisonArmy.mutateIsNetworkPlayer(false);
                garrisonArmy.mutateIsPlayerFaction(false);
                garrisonPlayer.addArmy(garrisonArmy);
                garrisonPlayer.setControllerOid(garrisonArmy.controllerOid());
                playerArmies.remove(garrisonPlayer.getOID());
            }
            instanceOid = InstanceClient.createInstance(siegeTemplateId, new Template("Siege on:" + battleMsg.pointOID(), siegeTemplateId, "Template"));

            for (OID playerOid : playerArmies.keySet()) {
                log.debug("sending player:" + playerOid + " to instance:" + instanceOid);
                BasicWorldNode node = new BasicWorldNode();
                node.setInstanceOid(instanceOid);
                node.setLoc(new Point(0, 0, 0));
                InstanceClient.objectInstanceEntry(playerOid, node, InstanceClient.InstanceEntryReqMessage.FLAG_PUSH);
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Battle battle = new Battle(instanceOid, battleInitiater, (player, bMsg) -> {
                sendBattleMsg(bMsg, player);
        },
                (player, bMsg) -> {
                        sendLongBattleMsg(bMsg, player);
                }, battleMsg.position());
        if (battleMsg.isSiege())
            battle.setSiege(battleMsg.pointOID());
        battle.setBattle(battleMsg);
        battle.spawn();
        battleMsg.mutateOID(battle.getBattleOID().toLong());
        battles.put(battle.getBattleOID(), battle);
        battlePlayers.forEach((player, list) -> {
            if (player.areAllArmiesPresent())
                battle.join(player);
            else battle.addPlayerToBattle(player);
        });
        final boolean garrisonPresent = battleMsg.isSiege() && garrisonPlayer.getArmies().size()>0;
        if (garrisonPresent){
            battle.join(garrisonPlayer);
            battlePlayers.put(garrisonPlayer, new ArrayList<ArmyMsg>(garrisonPlayer.getArmies()));
        }
        ArmyMsg finalGarrisonArmy = garrisonArmy;
        battlePlayers.forEach((player, currentArmies) -> {
            OID playerOid = player.getOID();
            currentArmies.forEach(a -> {
                a.mutateIsNetworkPlayer(false);
                a.mutateRequireDeployment(true);
                a.mutateIsHostile(false);
            });
            HashMap<TacticalPlayer,List<ArmyMsg>> otherPlayers = new HashMap<TacticalPlayer, List<ArmyMsg>>();
            otherPlayers.putAll(battlePlayers);
            otherPlayers.remove(player);

            otherPlayers.forEach((player2, list2) -> {
                boolean attackable = player.isAttackable(player2.getOID());
                list2.forEach(a -> {
                    a.mutateIsHostile(attackable);
                    a.mutateIsNetworkPlayer(true);
                    a.mutateRequireDeployment(false);
                });
            });
            if (garrisonPresent) {
                if (playerOid.equals(battleMsg.pointOID())) {
                    return;
                } else if (playerOid.equals(battleInitiater.getOID())) {
                    finalGarrisonArmy.mutateIsNetworkPlayer(false);
                    finalGarrisonArmy.mutateRequireDeployment(true);
                }
            }
            log.debug("sending start battle to:" + playerOid.toLong() + " msg:" + proto.battleInfoString(battleMsg));
            sendLongBattleMsg(battleMsg, playerOid);
        });
        if (battle.didAllJoin()) {
            if (battle.getPhase() == _GamePhase.Initialization) {
                battle.start();
                //BattleMsg img = battle.getBattleImage();
                //log.debug("all players joined at start battle, image:" + proto.battleInfoString(img));
                //battle.sendAllListeners(img);
            }
        }
    }

    public void logAndSendError(BattleMsg msg, String error) {
        error = "Battle error on command:" + BattleCmd.name(msg.cmd()) + " battleOid:" + msg.OID() + " error:" + error;
        log.debug(error);
        BattleMsg returnMsg = addError(msg, error);
        sendBattleMsg(returnMsg, OID.fromLong(msg.subjectOID()));
    }
    public BattleMsg addError(BattleMsg msg, String error) {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int errorOffset = fbb.createString(error);
        BattleMsg.startBattleMsg(fbb);
        BattleMsg.addSubjectOID(fbb, msg.subjectOID());
        BattleMsg.addText(fbb, errorOffset);
        BattleMsg.addOID(fbb, msg.OID());
        BattleMsg.addCmd(fbb, BattleCmd.EXECUTE_FAILURE);
        int offset = BattleMsg.endBattleMsg(fbb);
        fbb.finish(offset);
        return BattleMsg.getRootAsBattleMsg(fbb.dataBuffer());
    }

    public void battleJoined(BattleMsg battleMsg, Battle battle) {
        synchronized (battle) {
            log.debug("battle joined with message:" + proto.battleInfoString(battleMsg));
            for (int i = 0; i < battleMsg.armiesLength(); i++) {
                ArmyMsg army = battleMsg.armies(i);
                log.debug("battle joined by army oid:" + army.OID() + " with unit amount:" + army.armyUnitsLength());
                TacticalPlayer battleJoiner = playerManager.getPlayer(army.OID());
                if (battleJoiner.getBattle() != null && !battleJoiner.getBattle().getBattleOID().equals(OID.fromLong(battleMsg.OID()))) {
                    logAndSendError(battleMsg, "Player " + battleJoiner.getName() +
                            " can't start a battle: already in battle");
                }
                battleJoiner.updateArmy(army);
                battle.join(battleJoiner);
            }
            battle.sendBattlePlayers(battleMsg,true);
            if (battle.didAllJoin()) {
                if (battle.getPhase() == _GamePhase.Initialization) {
                    battle.start();
                    //BattleMsg img = battle.getBattleImage();
                   // log.debug("battle joined, sending image:" + proto.battleInfoString(img));
                   // battle.sendAllListeners(img);
                }
            }
        }
    }

    public void battleDeployed(BattleMsg battleMsg, Battle battle) {
        for (int i = 0; i < battleMsg.armiesLength(); i++) {
            ArmyMsg army = battleMsg.armies(i);
            log.debug("army deployed:" + army.OID() + " army units size:" + army.armyUnitsLength());
            TacticalPlayer player = battle.getPlayer(army.OID());
            battle.deployArmy(army);
        }
        battle.sendBattlePlayers(battleMsg,true);
        if (battle.isDeploymentComplete()) {
            if (battle.getPhase() == _GamePhase.UnitDeployment) {
                battle.commence();
                BattleMsg commenceBattle = proto.getBattleMsg(battle, BattleCmd.BATTLE_COMMENCED, battleMsg.subjectOID());
                battle.sendAll(commenceBattle);
            }
            //if (battle.getPhase()== _GamePhase.Play)
        }
    }


    public void battleTurnEnded(BattleMsg battleMsg, Battle battle) {
        battle.endTurn();
    }

    public void battleLeft(BattleMsg battleMsg, Battle battle) {
        TacticalPlayer battleLeaver = playerManager.getPlayer(battleMsg.subjectOID());
        if (battle == null) {
            logAndSendError(battleMsg, "Player " + battleLeaver.getName() + " can't leave: not in battle");
            return;
        }
        synchronized (battle) {
            switch (battleMsg.cmd()) {
                case BattleCmd.BATTLE_OVER:
                    if (battle.isBattleOver()) {
                        logAndSendError(battleMsg, "battle already over");
                        return;
                    }
                    battle.endBattle();
                    break;
                case BattleCmd.PLAYER_RETREAT_BATTLE:
                case BattleCmd.PLAYER_LEFT_BATTLE:
                default:
                    break;
            }
            battle.removePlayer(battleLeaver.getOID());
            battle.sendAll(battleMsg);
        }
    }


    public void battleAction(BattleMsg battleMsg, Battle battle) {
        synchronized (battle) {
            switch (battleMsg.battleActionType()) {
                case BattleActionMsg.UnitMoveMsg:
                    UnitMoveMsg moveMsg = (UnitMoveMsg) battleMsg.battleAction(new UnitMoveMsg());
                    battle.unitMove(moveMsg);
                    break;
                case BattleActionMsg.UnitAttackMsg:
                    UnitAttackMsg attackMsg = (UnitAttackMsg) battleMsg.battleAction(new UnitAttackMsg());
                    battle.unitAttack(attackMsg);
                    battle.sendAll(battleMsg);
                    if (battle.isVictoryConditionMet()) {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        log.debug("victory condition met");
                        battle.endBattle();
                    }
                    return;
                case BattleActionMsg.UnitAbilityMsg:
                    UnitAbilityMsg abilityMsg = (UnitAbilityMsg) battleMsg.battleAction(new UnitAbilityMsg());
                    battle.unitAbility(abilityMsg);
                    break;
            }
            battle.sendAll(battleMsg);
        }
    }

    public void clearDeadUnits(Battle battle) {
        Collection<TacticalPlayer> players = battle.getAllPlayers();
        for (TacticalPlayer p : players) {
            p.getArmies().forEach(a -> {
                p.updateArmy(proto.clearDeadUnits(a));
            });
        }
    }


    public void battleOver(BattleMsg battleMsg, Battle battle) {
        synchronized (battle) {
            if (!battle.isBattleOver()) {
                battle.endBattle();
                //send control point manager siege result
            }
        }
    }

}
