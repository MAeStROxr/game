package mygame.tactical;

import atavism.server.engine.OID;
import mygame.Player;
import mygame.Proto;
import mygame.proto.ArmyMsg;
import mygame.proto.UnitMsg;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by meme on 5/13/2016.
 */
public class TacticalPlayer extends Player {

    private HashMap<Long,ArmyMsg> armies = new HashMap<>();
    private Map<Long, UnitMsg> allUnits = new HashMap<Long, UnitMsg>();
    private Map<Long, UnitMsg> liveUnits = new HashMap<Long, UnitMsg>();
    private Map<Long, UnitMsg> deadUnits = new HashMap<Long, UnitMsg>();
    private Proto proto= new Proto();
    private HashSet<OID> attackable = new HashSet<>();
    private Battle currentBattle;
    private boolean joined = false;
    private boolean deployed = false;
    private OID instanceOid;
    private long controllerOid=-1;
	
    public TacticalPlayer(long playerOid, String playerName) {
        super(playerOid, playerName);

    }

    public TacticalPlayer(OID playerOid, String playerName) {
        super(playerOid, playerName);

    }

    public void setControllerOid(long controllerOid){
        this.controllerOid = controllerOid;
    }
	public long getControllerOid(){
		return controllerOid;
	}



    public Battle getBattle() {
        return currentBattle;
    }

    public void setBattle(Battle battle) {
        currentBattle = battle;
        joined = false;
        deployed = false;
    }

    public boolean isJoined() {
        return joined;
    }

    public void joinBattle(Battle battle) {
        setBattle(battle);
        joined = true;
        updateUnitsFromArmies();
    }

    public void updateUnitsFromArmies(){
        clearUnits();
        for (ArmyMsg playerArmy : getArmies()) {
            for (int i = 0; i < playerArmy.armyUnitsLength(); i++) {
                UnitMsg u = playerArmy.armyUnits(i);
                if (u.HP() > 0) liveUnits.putIfAbsent(u.OID(), u);
                else deadUnits.put(u.OID(), u);
                allUnits.putIfAbsent(u.OID(), u);
            }
        }
    }


    public void addUnits(List<UnitMsg> units) {
        for (UnitMsg u : units) {
            if (u.HP() > 0) liveUnits.putIfAbsent(u.OID(), u);
            else deadUnits.putIfAbsent(u.OID(), u);
            allUnits.putIfAbsent(u.OID(), u);
        }
    }

    public Collection<UnitMsg> getUnits() { return allUnits.values(); }

    public Map<Long,UnitMsg> getLiveUnits(){ return liveUnits;}

    public Collection<UnitMsg> getDeadUnits(){ return deadUnits.values();}

    public void clearUnits() {
        allUnits.clear();
        liveUnits.clear();
        deadUnits.clear();
    }

    public void killUnit(long unitOID){
        UnitMsg u = liveUnits.get(unitOID);
        liveUnits.remove(unitOID);
        deadUnits.put(unitOID,u);
    }

    public void removeUnits(List<Long> unitsOID) {
        for (long unitOID : unitsOID) {
            allUnits.remove(unitOID);
            liveUnits.remove(unitOID);
            deadUnits.remove(unitOID);
        }

    }
    public Collection<ArmyMsg> getArmies() {
        return armies.values();
    }

    public Map<Long,ArmyMsg> getArmiesMap() {
        return armies;
    }

    public void clearArmies(){
        armies.clear();
        clearUnits();
    }

    public void removeArmy(ArmyMsg army) {
        removeUnits(proto.getArmyUnits(army).stream().mapToLong(u->u.OID()).boxed().collect(Collectors.toList()));
        armies.remove(army);
    }

    public void addArmy(ArmyMsg armyData) {
        armies.putIfAbsent(armyData.OID(),armyData);
        addUnits(proto.getArmyUnits(armyData));
    }

    public void updateArmies(Map<Long,ArmyMsg> updatedArmies){
        updatedArmies.replaceAll((oid,a)->updatedArmies.get(a.OID()));
        updateUnitsFromArmies();
    }



    public OID getInstanceOid(){ return instanceOid;}
    public void setInstanceOid(OID instanceOid1){ instanceOid=  instanceOid1;}


    public boolean isDeployed() {
        return deployed;
    }


    public void setDeployed(boolean deployed) {
        this.deployed = deployed;
    }

    public boolean areAllArmiesPresent() {
        return getArmies().stream().allMatch(a->a.armyUnitsLength()>0);
    }

    public void updateArmy(ArmyMsg army) {
        armies.replace(army.OID(),proto.createExistingArmyWithUnits(armies.get(army.OID()),proto.getArmyUnits(army)));
        List<UnitMsg> newUnits = proto.getArmyUnits(army);
        newUnits.forEach(u->{
            allUnits.put(u.OID(),u);
            liveUnits.put(u.OID(),u);
            deadUnits.put(u.OID(),u);
        });
    }

    public void addAttackable(OID oid){
        attackable.add(oid);
    }


    public boolean isAttackable(OID oid) {
        return attackable.contains(oid);

    }

    public void clearDeadUnits() {
        armies.replaceAll((oid,army)->proto.clearDeadUnits(army));
    }
}
