package mygame.tactical;

import atavism.agis.plugins.TacticalClient;
import atavism.server.engine.EnginePlugin;
import atavism.server.engine.OID;
import atavism.server.plugins.WorldManagerClient;
import mygame.PlayerManager;
import mygame.proto.DataAction;
import mygame.proto.TacticalPlayerCmd;
import mygame.proto.TacticalPlayerMsg;
import mygame.proto.UnitMsg;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by meme on 7/5/2016.
 */
public class TacticalPlayerManager extends PlayerManager<TacticalPlayer> {
    public TacticalPlayerManager(){
        super();

    }
   public void  processTacticalPlayer(TacticalPlayerMsg playerMsg){
        OID playerOID = OID.fromLong(playerMsg.subjectOID());
       switch (playerMsg.cmd()) {
           case TacticalPlayerCmd.PLAYER_TACTICAL_INFO:
               EnginePlugin.setObjectProperty(playerOID, WorldManagerClient.NAMESPACE,"inBattle",false);
               if (players.containsKey(playerOID)){
               } else {
                   addPlayer(playerOID, new TacticalPlayer(playerOID,
                           playerMsg.subjectName()));
               }
                break;
           case TacticalPlayerCmd.PLAYER_UNITS:
               TacticalPlayer player = getPlayer(playerOID);
               List<UnitMsg> playerUnits = proto.getArmyUnits(playerMsg.army());
           switch (playerMsg.action()) {
               case DataAction.ADD:
                   player.addUnits(playerUnits);
                   break;
               case DataAction.REMOVE:
                   player.removeUnits(playerUnits.stream().map(unit -> unit.OID()).collect(Collectors.toList()));
                   break;
               case DataAction.REPLACE:
                   player.clearUnits();
                   player.addUnits(playerUnits);
                   break;
               default:
                   break;
           }
               break;
       }
       TacticalClient.sendTargetedTacticalPlayerMsg(playerMsg, OID.fromLong(playerMsg.subjectOID()));
   }
}
