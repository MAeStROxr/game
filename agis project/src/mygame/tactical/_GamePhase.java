package mygame.tactical;

/**
 * Created by MAeStRO on 7/29/2016.
 */
public enum _GamePhase {
    Initialization,         //the game is being initialized
    UnitDeployment,     //unit deployment is taking place
    Play,                   //the game is playing
    Over


}
