package unitest;

import atavism.server.util.Logger;
import mygame.control.ControlPlayer;
import mygame.control.ControlPlayerManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ControlPlayerManagerTest {

	Logger log = new Logger("player manager");
	ControlPlayerManager pManager;
	long p1oid=11,p2oid=22,p3oid=33,p4oid=44;
	String p1name="p1name",p2name="p2name",p3name="p3name",p4name="p4name";
	ControlPlayer p1=new ControlPlayer(p1oid,p1name),p2=new ControlPlayer(p2oid,p2name),
			p3=new ControlPlayer(p3oid,p3name),p4=new ControlPlayer(p4oid,p4name);
	@Before
	public void setup(){
		pManager = new ControlPlayerManager();
		pManager.addPlayer(p1oid, p1);
		pManager.addPlayer(p2oid, p2);
		pManager.addPlayer(p3oid, p3);
		pManager.addPlayer(p4oid, p4);


	}
	

	@Test
	public void testPlayerNodeDefault(){
		ControlPlayer p = new ControlPlayer(0,"");
		assertEquals(null,p.getPlayerNode());
	}
	
	@Test
	public void testPlayerNodeCreate(){
		pManager.addPlayer(666,new ControlPlayer(666,"temp"));
		assertEquals(pManager.getRoot(),pManager.getPlayer(666).getPlayerNode().getParent());

	}
	
	
	@Test
	public void testControlPlayerSameTree(){
		p1.controlPlayer(p2);
		assertEquals(true,p1.isPlayerInTree(p2));
		assertEquals(true,p2.isPlayerInTree(p1));
	}
	
	
	@Test 
	public void inControl(){
		assertEquals(false,p1.inControl(p2));
		assertEquals(false,p2.inControl(p1));
		p1.controlPlayer(p2);
		assertEquals(true,p1.inControl(p2));
		assertEquals(false,p2.inControl(p1));
	}
	
	@Test
	public void testChangePlayerTree(){
		p1.controlPlayer(p2);
		p3.controlPlayer(p4);
		assertEquals(false,p1.inControl(p4));
		assertEquals(false,p3.inControl(p2));
		p1.controlPlayer(p4);
		p3.controlPlayer(p2);
		assertEquals(true,p1.inControl(p4));
		assertEquals(true,p3.inControl(p2));
		assertEquals(false,p1.inControl(p2));
		assertEquals(false,p3.inControl(p4));
	}
	
}
