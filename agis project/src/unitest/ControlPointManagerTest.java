package unitest;

import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.Proto;
import mygame.control.ControlPlayer;
import mygame.control.ControlPlayerManager;
import mygame.control.ControlPoint;
import mygame.control.ControlPointManager;
import mygame.proto.ArmyMsg;
import mygame.proto.ControlPointMsg;
import mygame.proto.DataAction;
import mygame.proto.PointCmd;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ControlPointManagerTest {
	public long unit1oid=11,unit2oid=22,unit3oid=33,unit4oid=44;
	public String unit1name="unit1",unit2name="unit2",unit3name="unit3",
			unit4name="unit4";
	public String army1name="army1",army2name="army2",army3name="army3",point1name="[point1oid]",point2name="[point2oid]";
	public long player1=1,player2=2,player3=3;
	public long point1oid =101, point2oid =202;
	int firstArmyOffset=-1,secondArmyOffset=-2,armiesOffset=-1;
	FlatBufferBuilder fbb,firstArmyFbb,secondArmyFbb,armiesFbb;
	Logger log = new Logger("point manager");
	ArmyMsg army1,army2;
	List<ArmyMsg> armies= new ArrayList<>();
	ControlPlayerManager playerManager = new ControlPlayerManager();
	ControlPlayer p1,p2,p3;
	ControlPoint point1,point2;
	BiConsumer<ControlPlayer,ControlPlayer> controlConsumer = (p1,p2)->{System.out.print("\ncontrol consumer called"+playerManager.getRoot().printSubTree());};
	Proto proto = new Proto();
	ControlPointManager manager = new ControlPointManager();


	@Before
	public void setup(){

		fbb = new FlatBufferBuilder();
		firstArmyFbb=new FlatBufferBuilder();
		firstArmyOffset=prepareArmy(firstArmyFbb);
		secondArmyFbb=new FlatBufferBuilder();
		secondArmyOffset=prepareSecondArmy(secondArmyFbb);
		armiesFbb = new FlatBufferBuilder();
		int[] armies = new int[]{prepareArmy(armiesFbb),prepareSecondArmy(armiesFbb)};
		armiesOffset=ControlPointMsg.createGarrisonedArmiesVector(armiesFbb, armies);

		playerManager = new ControlPlayerManager();
		manager = new ControlPointManager();
		manager.setPlayerManager(playerManager);
		manager.addPoint(point2oid,point2name);
		point2 = manager.getPoint(point2oid);
		manager.treeDBPath = "tempTreeDB";
		manager.pointsDBPath = "tempPointsDB";



		FlatBufferBuilder tempFbb = new FlatBufferBuilder();
		tempFbb.finish(prepareArmy(tempFbb));
		army1 = ArmyMsg.getRootAsArmyMsg(tempFbb.dataBuffer());
		tempFbb= new FlatBufferBuilder();
		tempFbb.finish(prepareSecondArmy(tempFbb));
		army2 = ArmyMsg.getRootAsArmyMsg(tempFbb.dataBuffer());

		p1 = new ControlPlayer(player1,army1name);
		p2 = new ControlPlayer(player2,army2name);
		p3 = new ControlPlayer(player3,army3name);
		playerManager.addPlayer(player1,p1);
		playerManager.addPlayer(player2,p2);
		playerManager.addPlayer(player3,p3);
	}
	
	private int prepareArmy(FlatBufferBuilder fbb){
		int unitMsgOffset1 = proto.addUnitMsgBasic(fbb,unit1oid,unit1name);
		int unitMsgOffset2 = proto.addUnitMsgBasic(fbb,unit2oid,unit2name);
		int armyNameOffset = fbb.createString(army1name);
		int unitsVec = ArmyMsg.createArmyUnitsVector(fbb, new int[]{unitMsgOffset1,unitMsgOffset2});
		int armyOffset = proto.addArmyMsgBasic(true,fbb, player1, armyNameOffset, unitsVec);
		return armyOffset;
	}
	
	private int prepareSecondArmy(FlatBufferBuilder fbb){
		int unitMsgOffset1 = proto.addUnitMsgBasic(fbb,unit1oid,unit1name);
		int unitMsgOffset2 = proto.addUnitMsgBasic(fbb,unit2oid,unit2name);
		int unitMsgOffset3 = proto.addUnitMsgBasic(fbb,unit3oid,unit3name);
		int unitMsgOffset4 = proto.addUnitMsgBasic(fbb,unit4oid,unit4name);
		
		int armyNameOffset = fbb.createString(army2name);
		int unitsVec = ArmyMsg.createArmyUnitsVector(fbb, new int[]{unitMsgOffset1,unitMsgOffset2,
				unitMsgOffset3,unitMsgOffset4});
		int armyOffset = proto.addArmyMsgBasic(true,fbb, player2, armyNameOffset, unitsVec);
		return armyOffset;
	}
	@Ignore
	@Test
	public void testAddPoint(){
		assertEquals(manager.getPoint(point1oid),null);
		manager.addPoint(point1oid,point1name);
		assertEquals(manager.getPoint(point1oid).OID(), point1oid);
	}
	@Ignore
	@Test
	public void testRemovePoint(){
		manager.addPoint(point1oid,point1name);
		assertEquals( point1oid,manager.getPoint(point1oid).OID());
		manager.removePoint(point1oid);
		assertEquals(null,manager.getPoint(point1oid));
	}
	@Ignore
	@Test
	public void testUpdateUnitGarrisonAdd(){
		ControlPointMsg pointMsg = proto.createPointMsg(point2oid, "point2oid", PointCmd.POINT_UNIT_GARRISON,army1,null, DataAction.ADD);
		ControlPoint point =  manager.getPoint(point2oid);
		assertEquals(point.getGarrison().armyUnitsLength(),0);
		manager.updateUnitGarrison(pointMsg,point);
		assertEquals(army1.armyUnitsLength(),point.getGarrison().armyUnitsLength());
		for (int i=0;i<point.getGarrison().armyUnitsLength();i++)
			assertEquals(point.getGarrison().armyUnits(i).OID(),army1.armyUnits(i).OID());
	}
	@Ignore
	@Test
	public void testUpdateUnitGarrisonRemove(){
		ControlPointMsg pointMsg = proto.createPointMsg( point2oid, "point2oid",PointCmd.POINT_UNIT_GARRISON, army2, null, DataAction.ADD);
		ControlPoint point =  manager.getPoint(point2oid);
		manager.updateUnitGarrison(pointMsg,point);
		int fourUnitsInSecondArmy = 4;
		assertEquals(fourUnitsInSecondArmy,point.getGarrison().armyUnitsLength());
		pointMsg = proto.createPointMsg(point2oid, "point2oid",PointCmd.POINT_UNIT_GARRISON, army1, armies, DataAction.REMOVE);
		manager.updateUnitGarrison(pointMsg,point);
		int twoExpectedLeftInGarrison=2;
		assertEquals(twoExpectedLeftInGarrison,point.getGarrison().armyUnitsLength());
		assertEquals(army2.armyUnits(2).OID(),point.getGarrison().armyUnits(0).OID());
		assertEquals(army2.armyUnits(3).OID(),point.getGarrison().armyUnits(1).OID());
	}
	@Ignore
	@Test
	public void testUpdateUnitGarrisonReplace(){
		ControlPoint point =  manager.getPoint(point2oid);
		
		ControlPointMsg pointMsg = proto.createPointMsg( point2oid, "point2oid",PointCmd.POINT_UNIT_GARRISON, army1, null, DataAction.ADD);
		manager.updateUnitGarrison(pointMsg,point);
		int twoExpectedInGarrison=2;
		assertEquals(twoExpectedInGarrison,point.getGarrison().armyUnitsLength());
		
		pointMsg = proto.createPointMsg(point2oid, "point2oid",PointCmd.POINT_UNIT_GARRISON, army2, null, DataAction.REPLACE);
		manager.updateUnitGarrison(pointMsg,point);
		int fourUnitsReplacedTwo = 4;
		assertEquals(fourUnitsReplacedTwo,point.getGarrison().armyUnitsLength());
		assertEquals(army2.armyUnits(2).OID(),point.getGarrison().armyUnits(2).OID());
		assertEquals(army2.armyUnits(3).OID(),point.getGarrison().armyUnits(3).OID());
	}

	@Ignore
	@Test
	public void testUpdateArmiesGarrisonAdd(){
		armies.add(army1);
		armies.add(army2);
		ControlPointMsg pointMsg = proto.createPointMsg( point2oid, "point2oid",PointCmd.POINT_UNIT_GARRISON, null, armies, DataAction.ADD);
		ControlPoint point =  manager.getPoint(point2oid);
		assertEquals(point.getGarrisonedArmies().size(),0);
		manager.updateArmiesGarrison(pointMsg,point);
		int expectedTwoArmiesInGarrison=2;
		assertEquals(point.getGarrisonedArmies().size(),expectedTwoArmiesInGarrison);
		
		ArmyMsg firstArmy=point.getGarrisonedArmies().get(0);
		ArmyMsg secondArmy=point.getGarrisonedArmies().get(1);
		
		assertEquals(firstArmy.OID(),player1);
		assertEquals(secondArmy.OID(),player2);
		for (int i=0;i<army2.armyUnitsLength();i++)
			assertEquals(army2.armyUnits(i).OID(),secondArmy.armyUnits(i).OID());
		
	}

	@Ignore
	@Test
	public void testPointCapture(){
		assertEquals(0,p1.getControlledPoints().size());
		p1.capturePoint(point2, controlConsumer);
		assertEquals(1,p1.getControlledPoints().size());
		assertEquals(player1,point2.getControllingPlayer().getOID().toLong());
	}
	@Ignore
	@Test
	public void testPointEnable(){
		assertEquals(0,p1.getEnabledPoints().size());
		p1.enablePoint(point2);
		assertEquals(1,p1.getEnabledPoints().size());
		assertEquals(player1,point2.getEnablingPlayer().getOID().toLong());
	}
	@Ignore
	@Test
	public void testPointControllerPushedToEnable(){
		p1.capturePoint(point2,controlConsumer);
		p2.capturePoint(point2,controlConsumer);
		assertEquals(1,p1.getEnabledPoints().size());
		assertEquals(1,p2.getControlledPoints().size());
	}
	@Ignore
	@Test
	public void testEnablerStaysIfLastPoint(){
		System.out.print("\n"+playerManager.getRoot().printSubTree());
		p1.capturePoint(point2,controlConsumer);
		p2.capturePoint(point2,controlConsumer);
		assertEquals(p2.getPlayerNode(),p1.getPlayerNode().getParent());
		assertEquals(1,p1.getEnabledPoints().size());
		assertEquals(0,p2.getEnabledPoints().size());
		p3.capturePoint(point2,controlConsumer);
		assertEquals(p3.getPlayerNode(),p1.getPlayerNode().getParent());
		assertNotEquals(p2.getPlayerNode(),p1.getPlayerNode().getParent());
		assertEquals(1,p3.getControlledPoints().size());
		assertEquals(1,p1.getEnabledPoints().size());
		assertEquals(0,p2.getControlledPoints().size());
		assertEquals(0,p2.getEnabledPoints().size());
		System.out.print("\n"+playerManager.getRoot().printSubTree());
	}


	@Test
	public void testPersistControl(){

		p1.controlPlayer(p2);
		manager.persistControl();

	}

	@Test
	public void testMarshalControl(){

		assertEquals(1,manager.getPointMap().size());
		assertEquals(3,playerManager.getsPlayerSet().size());
		System.out.print("\n" + playerManager.getRoot().printSubTree());
		manager.clear();
		assertEquals(0,manager.getPointMap().size());
		assertEquals(0,playerManager.getsPlayerSet().size());
		System.out.print("\n" + playerManager.getRoot().printSubTree());
		manager.loadControl();
		assertEquals(1,manager.getPointMap().size());
		assertEquals(3,playerManager.getsPlayerSet().size());
		System.out.print("\n" + playerManager.getRoot().printSubTree());
		p1 = playerManager.getPlayer(p1.getOID());
		p2 = playerManager.getPlayer(p2.getOID());
		assertEquals(true,p1.isPlayerInTree(p2));
	}

}
