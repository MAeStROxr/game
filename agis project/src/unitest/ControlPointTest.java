package unitest;

import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.Proto;
import mygame.control.ControlPlayer;
import mygame.control.ControlPoint;
import mygame.proto.ArmyMsg;
import mygame.proto.UnitMsg;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ControlPointTest {
	Logger log = new Logger("control point");
	Proto proto = new Proto();
	long p1oid=1,p2oid=2;
	String p1name="player1",p2name="player2";
	List<UnitMsg> units = new ArrayList<>();
	ArmyMsg army1;
	ControlPoint cp;
	ControlPlayer p1,p2;
	@Before
	public void setup(){
		cp = new ControlPoint(101,"point");
		FlatBufferBuilder fbb = new FlatBufferBuilder();
		units.add(proto.createUnitMsgBasic(150,"unit150"));
		units.add(proto.createUnitMsgBasic(151,"unit151"));
		units.add(proto.createUnitMsgBasic(152,"unit152"));
		army1 = proto.createArmy(p1oid,p1name,units);
		p1=new ControlPlayer(p1oid,p1name);
		p2=new ControlPlayer(p2oid,p2name);
	}
	@Test
	public void testDefaultUnitGarrison() {
		assertEquals(0,cp.getGarrison().armyUnitsLength());
	}
	
	@Test
	public void testDefaultArmiesGarrison() {
		assertEquals(0,cp.getGarrisonTotal().size());
	}
	
	@Test
	public void testSetUnitGarrison(){
		cp.setGarrison(army1);
		assertEquals(p1oid,cp.getGarrison().OID());
		assertEquals(p1name,cp.getGarrison().armyName());
	}

	@Test
	public void testAddGarrisonArmy(){
		cp.addArmyGarrison(army1);
		assertEquals(1,cp.getGarrisonTotal().size());
		assertEquals(p1oid,cp.getGarrisonTotal().get(0).OID());
		assertEquals(p1name,cp.getGarrisonTotal().get(0).armyName());

	}

}
