package unitest;

import atavism.server.util.Logger;
import mygame.Player;
import mygame.PlayerManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PlayerManagerTest {

	Logger log = new Logger("player manager");
	PlayerManager pManager;
	long p1oid=11,p2oid=22,p3oid=33,p4oid=44;
	String p1name="p1name",p2name="p2name",p3name="p3name",p4name="p4name";
	Player p1,p2,p3,p4;
	@Before
	public void setup(){
		pManager = new PlayerManager();
		pManager.addPlayer(p1oid, p1);
		pManager.addPlayer(p2oid, p2);
		pManager.addPlayer(p3oid, p3);
		pManager.addPlayer(p4oid, p4);
	}
	
	@Test
	public void testAddPlayer(){
		assertEquals(null,pManager.getPlayer(66));
		pManager.addPlayer(66, new Player(66,"p66name"));
		assertNotEquals(null,pManager.getPlayer(66));
		assertEquals(66,pManager.getPlayer(66).getOID().toLong());
		assertEquals("p66name",pManager.getPlayer(66).getName());

	}
	

	
}
