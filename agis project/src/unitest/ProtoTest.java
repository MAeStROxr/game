package unitest;


import atavism.server.util.Logger;
import com.google.flatbuffers.FlatBufferBuilder;
import mygame.Proto;
import mygame.control.ControlPlayer;
import mygame.control.ControlPlayerManager;
import mygame.proto.*;
import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ProtoTest {
    Logger log = new Logger("Proto logger");
    public long unit1oid = 11, unit2oid = 22, unit3oid = 33;
    public String unit1name = "unit1", unit2name = "unit2", unit3name = "unit3",
            unit4name = "unit4";
    public String army1name = "army1", army2name = "army2";
    public long p1 = 1, p2 = 2, p3 = 3;
    public String p1name = "player1", p2name = "player2", p3name = "player3";
    public long point1 = 101, point2 = 202;
    public ControlPlayer player1 = new ControlPlayer(p1, p1name), player2 = new ControlPlayer(p2, p2name), player3 = new ControlPlayer(p3, p3name);
    public ControlPlayerManager pManager = new ControlPlayerManager();
    int preparedArmyOffset = -1;
    Proto proto = new Proto();
    FlatBufferBuilder fbb, armyFbb, pointFbb;
    ArmyMsg army1;

    @Before
    public void setup() {
        fbb = new FlatBufferBuilder();
        armyFbb = new FlatBufferBuilder();
        pointFbb = new FlatBufferBuilder();

        preparedArmyOffset = prepareArmy(armyFbb);
        preparedArmyOffset = prepareArmy(pointFbb);
        FlatBufferBuilder army1fbb = new FlatBufferBuilder();
        army1fbb.finish(prepareArmy(army1fbb));
        army1 = ArmyMsg.getRootAsArmyMsg(army1fbb.dataBuffer());
        pManager.addPlayer(p1, player1);
        pManager.addPlayer(p2, player2);
        pManager.addPlayer(p3, player3);
        //player1.controlPlayer(player2);
        //player2.controlPlayer(player3);
    }

    private int prepareArmy(FlatBufferBuilder fbb) {
        int unitMsgOffset1 = proto.addUnitMsgBasic(fbb, unit1oid, unit1name);
        int unitMsgOffset2 = proto.addUnitMsgBasic(fbb, unit2oid, unit2name);
        int armyNameOffset = fbb.createString(army1name);
        int unitsVec = ArmyMsg.createArmyUnitsVector(fbb, new int[]{unitMsgOffset1, unitMsgOffset2});
        proto.addArmyMsgBasic(false, fbb, p1, armyNameOffset, unitsVec);
        fbb.forceDefaults(true);
        ArmyMsg.addIsNetworkPlayer(fbb,false);
        fbb.forceDefaults(false);
        int armyOffset = ArmyMsg.endArmyMsg(fbb);
        return armyOffset;
    }

    @Test
    public void testUnitMsgCreateBasic() {
        int unitMsgOffset = proto.addUnitMsgBasic(fbb, unit1oid, unit1name);
        fbb.finish(unitMsgOffset);
        UnitMsg unitMsg = UnitMsg.getRootAsUnitMsg(fbb.dataBuffer());
        assertEquals(unitMsg.OID(), unit1oid);
        assertEquals(unitMsg.unitName(), unit1name);
        System.out.print(unitMsg.OID() + unitMsg.unitName());
    }

    @Test
    public void testUnitMsgCreateSizedArray() {
        int unitMsgOffset = proto.addUnitMsgBasic(fbb, unit1oid, unit1name);
        fbb.finish(unitMsgOffset);
        ByteBuffer bb=fbb.dataBuffer();
        int pos = bb.position();

        UnitMsg unitMsg = UnitMsg.getRootAsUnitMsg(bb);
        System.out.print("pos:"+pos+" remaining:"+bb.remaining()+" capacity:"+bb.capacity()+" limit:"+bb.limit());
        bb.position(pos);
        byte[] arr = new byte[unitMsg.getByteBuffer().remaining()];
        unitMsg.getByteBuffer().get(arr);
                //fbb.sizedByteArray();
        assertNotEquals(arr.length,bb.array().length);
        ByteBuffer bb2 = ByteBuffer.wrap(arr);
        //bb2.position(pos);
        UnitMsg unitBuffer = UnitMsg.getRootAsUnitMsg(bb2);
        assertEquals(unitMsg.OID(), unitBuffer.OID());
        assertEquals(unitMsg.unitName(), unitBuffer.unitName());
        System.out.print("original length:"+bb.array().length+" truncated length:"+arr.length);
    }

    @Test
    public void testAddExistingUnitMsg() {
        assertEquals(unit1oid, army1.armyUnits(0).OID());
        int offset = proto.addExistingUnit(fbb, army1.armyUnits(0));
        fbb.finish(offset);
        UnitMsg existing = UnitMsg.getRootAsUnitMsg(fbb.dataBuffer());
        assertEquals(army1.armyUnits(0).OID(), existing.OID());

    }

    @Test
    public void testArmyMsgCreateBasic() {
        int unitMsgOffset1 = proto.addUnitMsgBasic(fbb, unit1oid, unit1name);
        int unitMsgOffset2 = proto.addUnitMsgBasic(fbb, unit2oid, unit2name);
        int armyNameOffset = fbb.createString(army1name);
        int unitsVec = ArmyMsg.createArmyUnitsVector(fbb, new int[]{unitMsgOffset1, unitMsgOffset2});
        // testing endMsg=true
        int armyOffset = proto.addArmyMsgBasic(true, fbb, p1, armyNameOffset, unitsVec);
        fbb.finish(armyOffset);
        ArmyMsg armyMsg = ArmyMsg.getRootAsArmyMsg(fbb.dataBuffer());
        assertEquals(armyMsg.OID(), p1);
        assertEquals(armyMsg.armyName(), army1name);
        UnitMsg unitMsg = armyMsg.armyUnits(0);
        assertEquals(unitMsg.OID(), unit1oid);
        assertEquals(unitMsg.unitName(), unit1name);
        unitMsg = armyMsg.armyUnits(1);
        assertEquals(unitMsg.OID(), unit2oid);
        assertEquals(unitMsg.unitName(), unit2name);

        System.out.print("army1:" + armyMsg.OID() + armyMsg.armyName());
    }


    private ControlPointMsg createControlPointMsg() {
        int nameOffset = pointFbb.createString(p1name);
        ControlPointMsg.startControlPointMsg(pointFbb);
        ControlPointMsg.addAction(pointFbb, DataAction.ADD);
        ControlPointMsg.addSubjectOID(pointFbb, p1);
        ControlPointMsg.addSubjectName(pointFbb, nameOffset);
        ControlPointMsg.addCmd(pointFbb, PointCmd.POINT_CAPTURE);
        ControlPointMsg.addOID(pointFbb, point1);
        ControlPointMsg.addGarrison(pointFbb, preparedArmyOffset);
        int pointMsgOffset = ControlPointMsg.endControlPointMsg(pointFbb);
        pointFbb.finish(pointMsgOffset);
        return ControlPointMsg.getRootAsControlPointMsg(pointFbb.dataBuffer());

    }

    @Test
    public void testControlPointMsgCreate() {
        ControlPointMsg pointMsg = createControlPointMsg();
        assertEquals(point1, pointMsg.OID());
        assertEquals(DataAction.ADD, pointMsg.action());
        ArmyMsg garrison = pointMsg.garrison();
        assertEquals(army1.OID(), garrison.OID());
        for (int i = 0; i < garrison.armyUnitsLength(); i++)
            assertEquals(army1.armyUnits(i).OID(), garrison.armyUnits(i).OID());

    }

    @Test
    public void testControlPointMsgRead() {

        ControlPointMsg pointMsg = createControlPointMsg();

        byte[] byteMsg = pointMsg.getByteBuffer().array();
        ByteBuffer bb = ByteBuffer.wrap(byteMsg);
        bb.position(pointMsg.getByteBuffer().position());
        ControlPointMsg readMsg = ControlPointMsg.getRootAsControlPointMsg(bb);
        System.out.print("\nread2:" + byteMsg + " " + bb.hasArray());

        assertEquals(point1, readMsg.OID());
        assertEquals(PointCmd.POINT_CAPTURE, pointMsg.cmd());
        assertEquals(p1, pointMsg.subjectOID());
        assertEquals(p1name, pointMsg.subjectName());

    }


    @Test
    public void testControlNodeTreeCreation() {
        FlatBufferBuilder fbb = new FlatBufferBuilder();
        int offset = Proto.getNodeMsg(fbb, player1.getPlayerNode());
        fbb.finish(offset);
        ControlNode node = ControlNode.getRootAsControlNode(fbb.dataBuffer());
        assertEquals(p1, node.OID());
        assertEquals(p1name, node.controlName());

        assertEquals(p2, node.controlled(0).OID());
        assertEquals(p2name, node.controlled(0).controlName());
        assertEquals(p3, node.controlled(0).controlled(0).OID());
        assertEquals(p3name, node.controlled(0).controlled(0).controlName());
        StringBuffer sb = new StringBuffer();
        System.out.print("\n" + pManager.getRoot().printSubTree());
        System.out.print("\n" + ControlPlayerManager.printControlNodeTree(node));
        System.out.print("\n" + java.util.Arrays.toString(node.getByteBuffer().array()));
        System.out.print("\n" + node.getByteBuffer().position() + "\n");


        byte[] encodedBytes = Base64.encodeBase64(node.getByteBuffer().array());
        System.out.println("encodedBytes " + new String(encodedBytes));
        byte[] decodedBytes = Base64.decodeBase64(encodedBytes);
        System.out.println("decodedBytes " + new String(decodedBytes));


    }

    @Test
    public void testBattleMsgMutateOID() {
        BattleMsg.startBattleMsg(fbb);
        BattleMsg.addOID(fbb, 100);
        fbb.finish(BattleMsg.endBattleMsg(fbb));
        BattleMsg msg = BattleMsg.getRootAsBattleMsg(fbb.dataBuffer());
        assertEquals(100,msg.OID());
        msg.mutateOID(205);
        assertEquals(205,msg.OID());
    }

    @Test
    public void testBattleMsgMutateIsNetworkPlayer() {

        int armiesOffset = BattleMsg.createArmiesVector(armyFbb,new int[] {preparedArmyOffset});
        BattleMsg.startBattleMsg(armyFbb);
        BattleMsg.addOID(armyFbb, 100);
        BattleMsg.addArmies(armyFbb,armiesOffset);
        armyFbb.finish(BattleMsg.endBattleMsg(armyFbb));
        BattleMsg msg = BattleMsg.getRootAsBattleMsg(armyFbb.dataBuffer());
        ArmyMsg army = msg.armies(0);
        assertEquals(p1,army.OID());
        assertEquals(false,army.isNetworkPlayer());
        army.mutateIsNetworkPlayer(true);
        assertEquals(true,army.isNetworkPlayer());
    }
}
