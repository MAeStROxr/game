package unitest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
	ControlPointTest.class,
	ControlPointManagerTest.class,
	ProtoTest.class,
	PlayerManagerTest.class,
	ControlPlayerManagerTest.class,
})
public class TestSuite {   
}  