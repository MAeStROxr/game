﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtavismCraftingRecipe : MonoBehaviour {

	public int recipeID;
	public string recipeName;
	public Sprite icon;
	public int skillID = -1;
	public int skillLevelReq = -1;
	public string stationReq = "";
	public int creationTime = 0;
	
	public List<int> createsItems;
	public List<int> createsItemsCounts;
	public List<int> itemsReq;
	public List<int> itemsReqCounts;
	
}
