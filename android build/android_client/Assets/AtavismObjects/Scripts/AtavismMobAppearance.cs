using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AttachmentSocket {
	Root,
	LeftFoot,
	RightFoot,
	Pelvis,
	LeftHip,
	RightHip,
	MainHand,
	MainHand2,
	OffHand,
	OffHand2,
	MainHandRest,
	MainHandRest2,
	OffHandRest,
	OffHandRest2,
	Shield,
	Shield2,
	ShieldRest,
	ShieldRest2,
	Chest,
	Back,
	LeftShoulder,
	RightShoulder,
	Head,
	Neck,
	Mouth,
	LeftEye,
	RightEye,
	Overhead,
	MainWeapon,
	SecondaryWeapon,
	None
}

public class ActiveEquipmentDisplay {
	public EquipmentDisplay equipDisplay;
	public GameObject attachedObject;
	public AttachmentSocket socket;
	public Material baseMaterial;
	
	public ActiveEquipmentDisplay() {
	}
	
	public ActiveEquipmentDisplay(EquipmentDisplay equipDisplay, GameObject attachedObject, AttachmentSocket socket) {
		this.equipDisplay = equipDisplay;
		this.attachedObject = attachedObject;
		this.socket = socket;
	}
	
	public ActiveEquipmentDisplay(EquipmentDisplay equipDisplay, GameObject attachedObject, Material baseMaterial) {
		this.equipDisplay = equipDisplay;
		this.attachedObject = attachedObject;
		this.baseMaterial = baseMaterial;
	}
}

public class AtavismMobAppearance : MonoBehaviour {

	// Icon
	public Sprite portraitIcon;
	
	// Sockets for attaching weapons (and particles)
	public Transform mainHand;
	public Transform mainHand2;
	public Transform offHand;
	public Transform offHand2;
	public Transform mainHandRest;
	public Transform mainHandRest2;
	public Transform offHandRest;
	public Transform offHandRest2;
	public Transform shield;
	public Transform shield2;
	public Transform shieldRest;
	public Transform shieldRest2;
	public Transform head;
	public Transform leftShoulderSocket;
	public Transform rightShoulderSocket;
	
	// Sockets for particles
	public Transform rootSocket;
	public Transform leftFootSocket;
	public Transform rightFootSocket;
	public Transform pelvisSocket;
	public Transform leftHipSocket;
	public Transform rightHipSocket;
	public Transform chestSocket;
	public Transform backSocket;
	public Transform neckSocket;
	public Transform mouthSocket;
	public Transform leftEyeSocket;
	public Transform rightEyeSocket;
	public Transform overheadSocket;
	
	List<string> displayProperties = new List<string>() {
		"weaponDisplayID",
		"weapon2DisplayID",
		"legDisplayID",
		"chestDisplayID",
		"headDisplayID",
		"feetDisplayID",
		"handDisplayID",
		"capeDisplayID",
		"shoulderDisplayID",
		"beltDisplayID"
	};
	
	Dictionary<string, List<ActiveEquipmentDisplay>> activeEquipDisplays = new Dictionary<string, List<ActiveEquipmentDisplay>>();
	bool inCombat = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	public Transform GetSocketTransform(AttachmentSocket slot) {
		switch (slot) {
		case AttachmentSocket.MainHand:
			if (mainHand != null)
				return mainHand;
			else
				return transform;
			break;
		case AttachmentSocket.MainHand2:
			if (mainHand2 != null)
				return mainHand2;
			else
				return transform;
			break;
		case AttachmentSocket.OffHand:
			if (offHand != null)
				return offHand;
			else
				return transform;
			break;
		case AttachmentSocket.OffHand2:
			if (offHand2 != null)
				return offHand2;
			else
				return transform;
			break;
		case AttachmentSocket.MainHandRest:
			if (mainHandRest != null)
				return mainHandRest;
			else
				return transform;
			break;
		case AttachmentSocket.MainHandRest2:
			if (mainHandRest2 != null)
				return mainHandRest2;
			else
				return transform;
			break;
		case AttachmentSocket.OffHandRest:
			if (offHandRest != null)
				return offHandRest;
			else
				return transform;
			break;
		case AttachmentSocket.OffHandRest2:
			if (offHandRest2 != null)
				return offHandRest2;
			else
				return transform;
			break;
		case AttachmentSocket.Shield:
			if (shield != null)
				return shield;
			else
				return transform;
			break;
		case AttachmentSocket.Shield2:
			if (shield2 != null)
				return shield2;
			else
				return transform;
			break;
		case AttachmentSocket.ShieldRest:
			if (shieldRest != null)
				return shieldRest;
			else
				return transform;
			break;
		case AttachmentSocket.ShieldRest2:
			if (shieldRest2 != null)
				return shieldRest2;
			else
				return transform;
			break;
		case AttachmentSocket.Head:
			if (head != null)
				return head;
			else
				return transform;
			break;
		case AttachmentSocket.LeftShoulder:
			if (leftShoulderSocket != null)
				return leftShoulderSocket;
			else
				return transform;
			break;
		case AttachmentSocket.RightShoulder:
			if (rightShoulderSocket != null)
				return rightShoulderSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Root:
			return transform;
			break;
		case AttachmentSocket.LeftFoot:
			if (leftFootSocket != null)
				return leftFootSocket;
			else
				return transform;
			break;
		case AttachmentSocket.RightFoot:
			if (rightFootSocket != null)
				return rightFootSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Pelvis:
			if (pelvisSocket != null)
				return pelvisSocket;
			else
				return transform;
			break;
		case AttachmentSocket.LeftHip:
			if (leftHipSocket != null)
				return leftHipSocket;
			else
				return transform;
			break;
		case AttachmentSocket.RightHip:
			if (rightHipSocket != null)
				return rightHipSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Chest:
			if (chestSocket != null)
				return chestSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Back:
			if (backSocket != null)
				return backSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Neck:
			if (neckSocket != null)
				return neckSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Mouth:
			if (mouthSocket != null)
				return mouthSocket;
			else
				return transform;
			break;
		case AttachmentSocket.LeftEye:
			if (leftEyeSocket != null)
				return leftEyeSocket;
			else
				return transform;
			break;
		case AttachmentSocket.RightEye:
			if (rightEyeSocket != null)
				return rightEyeSocket;
			else
				return transform;
			break;
		case AttachmentSocket.Overhead:
			if (overheadSocket != null)
				return overheadSocket;
			else
				return transform;
			break;
		case AttachmentSocket.MainWeapon:
			if (mainHand != null && mainHand.childCount > 0)
				return mainHand.GetChild(0).FindChild("socket");
			else
				return null;
			break;
		case AttachmentSocket.SecondaryWeapon:
			if (offHand != null)
				return offHand.GetChild(0).FindChild("socket");
			else
				return null;
			break;
		}
		return null;
	}
	
	void OnDestroy() {
		if (GetComponent<AtavismNode>()) {
			foreach (string displayProperty in displayProperties) {
				GetComponent<AtavismNode> ().RemoveObjectPropertyChangeHandler(displayProperty, EquipPropertyHandler);
			}
			GetComponent<AtavismNode> ().RemoveObjectPropertyChangeHandler("combatstate", HandleCombatState); 
			GetComponent<AtavismNode> ().RemoveObjectPropertyChangeHandler("model", ModelHandler);
		}
	}
	
	protected void ObjectNodeReady () {
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("model", ModelHandler);
		
		foreach (string displayProperty in displayProperties) {
			GetComponent<AtavismNode> ().RegisterObjectPropertyChangeHandler(displayProperty, EquipPropertyHandler);
			if (GetComponent<AtavismNode>().PropertyExists(displayProperty)) {
				string displayID = (string)GetComponent<AtavismNode> ().GetProperty (displayProperty);
				UpdateEquipDisplay(displayProperty, displayID);
			}
		}
		
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("combatstate", HandleCombatState);
		if (GetComponent<AtavismNode>().PropertyExists("combatstate")) {
			inCombat = (bool)GetComponent<AtavismNode>().GetProperty("combatstate");
			SetWeaponsAttachmentSlot();
		}
		AtavismLogger.LogInfoMessage("Registered display properties for: " + name);
	}
	
	public void ModelHandler(object sender, PropertyChangeEventArgs args) {
		UnityEngine.Debug.Log("Got model");
		UpdateModel((string)GetComponent<AtavismNode> ().GetProperty (args.PropertyName));
	}
	
	public void UpdateModel(string prefabName) {
		if (prefabName.Contains(".prefab"))
		{
			int resourcePathPos = prefabName.IndexOf("Resources/");
			prefabName = prefabName.Substring(resourcePathPos + 10);
			prefabName = prefabName.Remove(prefabName.Length - 7);
		}
		
		GameObject prefab = (GameObject)Resources.Load(prefabName);
		GameObject newCharacter = (GameObject)UnityEngine.Object.Instantiate(prefab, transform.position, transform.rotation);
		newCharacter.name = name;
		GetComponent<AtavismNode> ().ReplaceGameObject (newCharacter);
		
		// Check if the player should be hidden as they are in spirit world
		string state = (string)GetComponent<AtavismNode>().GetProperty("state");
		if (state == "spirit")
			newCharacter.SetActive(false);
	}
	
	public void HandleCombatState (object sender, PropertyChangeEventArgs args)
	{
		inCombat = (bool)GetComponent<AtavismNode> ().GetProperty (args.PropertyName);
		// Reset weapon attached weapons based on combat state
		SetWeaponsAttachmentSlot();
	}
	
	public void SetWeaponsAttachmentSlot() {
		SetWeaponsAttachmentSlot(false);
	}
	
	public void SetWeaponsAttachmentSlot(bool force) {
		foreach(List<ActiveEquipmentDisplay> activeDisplays in activeEquipDisplays.Values) {
			foreach (ActiveEquipmentDisplay activeDisplay in activeDisplays) {
				if (activeDisplay.equipDisplay.equipDisplayType != EquipDisplayType.AttachedObject) 
					continue;
				//TODO: Handle items that can be both primary and off hand
				if (inCombat || activeDisplay.equipDisplay.restSocket == AttachmentSocket.None) {
					if (activeDisplay.socket == activeDisplay.equipDisplay.socket && !force)
						continue;
					// Set the weapons socket to the main socket for the display
					activeDisplay.socket = activeDisplay.equipDisplay.socket;
					GameObject weapon = activeDisplay.attachedObject;
					weapon.transform.parent = null;
					weapon.transform.position = GetSocketTransform(activeDisplay.socket).position;
					weapon.transform.rotation = GetSocketTransform(activeDisplay.socket).rotation;
					weapon.transform.parent = GetSocketTransform(activeDisplay.socket);
				} else if (force || activeDisplay.socket != activeDisplay.equipDisplay.restSocket) {
					// Set the weapons socket to the rest socket for the display
					activeDisplay.socket = activeDisplay.equipDisplay.restSocket;
					GameObject weapon = activeDisplay.attachedObject;
					weapon.transform.parent = null;
					weapon.transform.position = GetSocketTransform(activeDisplay.socket).position;
					weapon.transform.rotation = GetSocketTransform(activeDisplay.socket).rotation;
					weapon.transform.parent = GetSocketTransform(activeDisplay.socket);
				}
			}
		}
	}
	
	public void EquipPropertyHandler(object sender, PropertyChangeEventArgs args)
	{
		string displayID = (string)GetComponent<AtavismNode> ().GetProperty (args.PropertyName);
		UpdateEquipDisplay(args.PropertyName, displayID);
	}
	
	public void UpdateEquipDisplay(string propName, string displayID) { 
		if (activeEquipDisplays.ContainsKey(propName)) {
			foreach (ActiveEquipmentDisplay activeDisplay in activeEquipDisplays[propName]) {
				if (activeDisplay.equipDisplay.equipDisplayType == EquipDisplayType.AttachedObject) {
					RemoveAttachedObject(activeDisplay);
				} else if (activeDisplay.equipDisplay.equipDisplayType == EquipDisplayType.ActivatedModel) {
					DeactivateModel(activeDisplay);
				} else if (activeDisplay.equipDisplay.equipDisplayType == EquipDisplayType.BaseTextureSwap) {
					ResetBaseTexture(activeDisplay);
				}
			}
			activeEquipDisplays.Remove(propName);
		}
		if (displayID != null && displayID != "") {
			List<EquipmentDisplay> displays = ClientAPI.ScriptObject.GetComponent<Inventory>().LoadEquipmentDisplay(displayID);
			if (displays == null || displays.Count == 0)
				return;
			List<ActiveEquipmentDisplay> activeDisplays = new List<ActiveEquipmentDisplay>();
			foreach (EquipmentDisplay display in displays) {
				if (display.equipDisplayType == EquipDisplayType.AttachedObject) {
					activeDisplays.Add(AttachObject(display));
				} else if (display.equipDisplayType == EquipDisplayType.ActivatedModel) {
					activeDisplays.Add(ActivateModel(display));
				} else if (display.equipDisplayType == EquipDisplayType.BaseTextureSwap) {
					activeDisplays.Add(SwapBaseModelTexture(display));
				}
			}
			activeEquipDisplays.Add(propName, activeDisplays);
			SetWeaponsAttachmentSlot();
		}
	}
	
	protected ActiveEquipmentDisplay AttachObject(EquipmentDisplay equipDisplay) {
		GameObject weapon = (GameObject) Instantiate(equipDisplay.model, GetSocketTransform(equipDisplay.socket).position, 
		                                             GetSocketTransform(equipDisplay.socket).rotation);
		if (equipDisplay.material != null) {
			weapon.GetComponent<Renderer>().material = equipDisplay.material;
		}
		weapon.transform.parent = GetSocketTransform(equipDisplay.socket);
		return new ActiveEquipmentDisplay(equipDisplay, weapon, equipDisplay.socket);
	}
	
	protected ActiveEquipmentDisplay ActivateModel(EquipmentDisplay equipDisplay) {
		Transform newModel = transform.FindChild(equipDisplay.model.name);
		ActiveEquipmentDisplay activeDisplay = new ActiveEquipmentDisplay(equipDisplay, null, null);
		if (newModel != null) {
			newModel.gameObject.SetActive(true);
			if (equipDisplay.material != null) {
				activeDisplay.baseMaterial = newModel.GetComponent<Renderer>().material;
				newModel.GetComponent<Renderer>().material = equipDisplay.material;
			}
		}
		return activeDisplay;
	}
	
	protected ActiveEquipmentDisplay SwapBaseModelTexture(EquipmentDisplay equipDisplay) {
		Transform model = transform.FindChild(equipDisplay.model.name);
		// Store the base material first
		ActiveEquipmentDisplay activeDisplay = new ActiveEquipmentDisplay(equipDisplay, null, model.GetComponent<Renderer>().material);
		model.GetComponent<Renderer>().material = equipDisplay.material;
		return activeDisplay;
	}
	
	protected void RemoveAttachedObject(ActiveEquipmentDisplay activeDisplay) {
		Destroy(activeDisplay.attachedObject);
	}
	
	protected void DeactivateModel(ActiveEquipmentDisplay activeDisplay) {
		Transform model = transform.FindChild(activeDisplay.equipDisplay.model.name);
		if (model != null) {
			if (activeDisplay.baseMaterial != null) {
				model.GetComponent<Renderer>().material = activeDisplay.baseMaterial;
			}
			model.gameObject.SetActive(false);
		}
	}
	
	protected void ResetBaseTexture(ActiveEquipmentDisplay activeDisplay) {
		Transform model = transform.FindChild(activeDisplay.equipDisplay.model.name);
		model.GetComponent<Renderer>().material = activeDisplay.baseMaterial;
	}
}
