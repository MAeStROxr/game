using UnityEngine;
using System.Collections;

public enum EquipDisplayType {
	AttachedObject,
	ActivatedModel,
	BaseTextureSwap
}

public class EquipmentDisplay : MonoBehaviour {
	
	int id;
	public EquipDisplayType equipDisplayType;
	public GameObject model;
	public Material material;
	public AttachmentSocket socket = AttachmentSocket.None;
	public AttachmentSocket restSocket = AttachmentSocket.None;
	
}
