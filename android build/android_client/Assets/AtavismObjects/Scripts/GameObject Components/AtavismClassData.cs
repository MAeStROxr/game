﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AtavismClassData : MonoBehaviour {

	public string className;
	public Sprite maleClassIcon;
	public Sprite femaleClassIcon;
	public string description;
}
