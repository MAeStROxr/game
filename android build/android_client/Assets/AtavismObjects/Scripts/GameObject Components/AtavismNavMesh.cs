using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Detour;
using Recast;
using Recast.Data;
using RecastNavCSharp.Recast.Data;

public class NavMeshSettings {
	public int Tags;
	
	public float CellSize;
	public float CellHeight;
	public float AgentHeight;
	public float AgentRadius;
	public float AgentMaxClimb;
	public float EdgeMaxError;
	
	public int AgentMaxSlope;
	public int RegionMinSize;
	public int RegionMergeSize;
	public int EdgeMaxLen;
	public int VertsPerPoly;
	public int DetailSampleDist;
	public int DetailSampleMaxError;
	
	public int TileSize;
	private int OldTileSize;
	public int TileWidth;
	public int TileHeight;
	public int MaxTiles;
	public int MaxPolysPerTile;
}

public class RecastNavTile {
	public int x;
	public int y;
	public Detour.AtavismNavTile builder;
	public List<Vector3> NavMeshVerts = new List<Vector3>();
	public List<int> NavMeshTriangles = new List<int>();
	public List<Color> NavMeshColors = new List<Color>();
	public List<Vector2> NavMeshUVs = new List<Vector2>();
	
	public GameObject MeshObject;
	public MeshFilter MeshFilter;
	public MeshRenderer MeshRenderer;
	public Mesh RecastMesh;
	public Material Mat { get; set; }
}

public class AtavismNavMesh : MonoBehaviour {
	
	public NavMeshSettings settings;
	public Detour.NavMesh NavMesh { get; set; }
	public Config Config { get; set; }
	public Geometry Geometry { get; set; }
	public int tags;
	public int tileSize;
	
	public GameObject tileIndicator;
	public Vector3 origin;
	public Vector3 size;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public static NavMeshSettings GenerateSettings() {
		NavMeshSettings settings = new NavMeshSettings();
		settings.CellSize = 0.50000f;
		settings.CellHeight = 0.1f;
		settings.AgentHeight = 1.8f;
		settings.AgentRadius = .3f;
		settings.AgentMaxClimb = 0.9f;
		settings.AgentMaxSlope = 45;
		settings.RegionMinSize = 6;
		settings.RegionMergeSize = 20;
		settings.EdgeMaxLen = 24;
		settings.EdgeMaxError = 1.3f;
		settings.VertsPerPoly = 6;
		settings.DetailSampleDist = 9;
		settings.DetailSampleMaxError = 2;
		settings.TileSize = 500;
		
		return settings; 
		//settings.Tags = tags;
	}
	
	public void GenerateTiles(NavMeshSettings settings, Vector3 origin, Vector3 size) {
		this.settings = settings;
		this.tags = settings.Tags;
		this.tileSize = settings.TileSize;
		this.origin = origin;
		this.size = size;
		for (int x = 0; x < settings.TileWidth; x++) {
			for (int z = 0; z < settings.TileHeight; z++) {
				float posX = origin.x + (x * (settings.TileSize-2) * settings.CellSize) + (float)Math.Floor((float)x * settings.CellSize * 2f) - 1;
				float posZ = origin.z + (z * (settings.TileSize-2) * settings.CellSize) + (float)Math.Floor((float)z * settings.CellSize * 2f) - 1;
				Vector3 position = new Vector3(posX, origin.y, posZ);
				//GameObject meshObject = new GameObject("Mesh_" + x + "_" + z);
				GameObject meshObject;
				
				if (tileIndicator != null) {
					meshObject = (GameObject) Instantiate(tileIndicator);
				} else {
					meshObject = new GameObject();
				} 
				meshObject.name = "Tile_" + x + "_" + z;
				meshObject.transform.localScale = new Vector3(settings.TileSize * settings.CellSize, size.y + 100, settings.TileSize * settings.CellSize);
				meshObject.transform.position = new Vector3(position.x + settings.TileSize * settings.CellSize / 2, position.y + (size.y / 2) + 50, 
				                                            position.z + settings.TileSize * settings.CellSize / 2);
				//meshObject.transform.position = position;
				meshObject.transform.parent = this.transform;
				//meshObject.AddComponent<MeshFilter>();
				//meshObject.AddComponent<MeshRenderer>();
				meshObject.AddComponent<AtavismNavMeshTile>();
				Vector3 centre = new Vector3(posX + settings.TileSize / 4, position.y, posZ + settings.TileSize / 4);
				Bounds b = new Bounds();
				b.SetMinMax(position, position + new Vector3(settings.TileSize / 2, size.y, settings.TileSize / 2));
				meshObject.GetComponent<AtavismNavMeshTile>().SetTileData(x, z, b, this);
				
				/*GameObject tileIndicatorMesh = (GameObject) Instantiate(tileIndicator);
				tileIndicatorMesh.transform.localScale = new Vector3(settings.TileSize * settings.CellSize, size.y + 100, settings.TileSize * settings.CellSize);
				tileIndicatorMesh.transform.parent = meshObject.transform;
				tileIndicatorMesh.transform.localPosition = new Vector3(settings.TileSize * settings.CellSize / 2, size.y / 2 + 50, settings.TileSize * settings.CellSize / 2);*/
			}
		}
		
		InitNavMesh(false);
	}
	
	public void InitNavMesh(bool loadSettings) {
		if (loadSettings) {
			settings = GenerateSettings();
			settings.Tags = tags;
			settings.TileSize = tileSize;
		}
		Config config;
		Geometry = BuildConfig(settings, origin, origin + size, out config);
		Config = config;
		
		NavMesh = new Detour.NavMesh();
		NavMeshParams param = new NavMeshParams()
		{
			Orig = Geometry.MinBounds.ToArray(),
			MaxPolys = settings.MaxPolysPerTile,
			MaxTiles = settings.MaxTiles,
			TileWidth = config.TileSize*config.CellSize,
			TileHeight = config.TileSize*config.CellSize
			
		};
		NavMesh.Init(param);
		RecastVertex bmin = Geometry.MinBounds;
		RecastVertex bmax = Geometry.MaxBounds;
	}
	
	public Geometry BuildConfig(NavMeshSettings settings, Vector3 minBounds, Vector3 maxBounds, out Config config)
	{
		config = new Config()
		{
			CellSize = settings.CellSize,
			CellHeight = settings.CellHeight,
			WalkableSlopeAngle = settings.AgentMaxSlope,
			WalkableHeight = (int) Math.Ceiling(settings.AgentHeight/settings.CellHeight),
			WalkableClimb = (int) Math.Floor(settings.AgentMaxClimb/settings.CellHeight),
			WalkableRadius = (int) Math.Ceiling(settings.AgentRadius/settings.CellSize), //0
			MaxEdgeLength = (int) (settings.EdgeMaxLen/settings.CellSize),
			MaxSimplificationError = settings.EdgeMaxError,
			MinRegionArea = (int) (settings.RegionMinSize*settings.RegionMinSize),
			MergeRegionArea = (int) (settings.RegionMergeSize*settings.RegionMergeSize),
			MaxVertexesPerPoly = (int) settings.VertsPerPoly,
			DetailSampleDistance = settings.DetailSampleDist < 0.9 ? 0 : settings.CellSize*settings.DetailSampleDist,
			DetailSampleMaxError = settings.CellHeight*settings.DetailSampleMaxError,
			BorderSize = (int) Math.Ceiling(settings.AgentRadius/settings.CellSize) + 3,
			TileSize = settings.TileSize,
		};
		
		config.Width = config.TileSize + config.BorderSize*2;
		config.Height = config.TileSize + config.BorderSize*2;
		
		Geometry geom = new Geometry();
		// Set the bounds of the geom
		geom.MinBounds = new RecastVertex(minBounds.x, minBounds.y, minBounds.z);
		geom.MaxBounds = new RecastVertex(maxBounds.z, maxBounds.y, maxBounds.z);
		config.CalculateGridSize(geom);
		// Still call this, but don't use Geom, but rather just pass the bounds
		BuildTileSizeData(settings, config, geom);
		return geom;
	}
	
	public void BuildTileSizeData(NavMeshSettings settings, Config _config, Geometry _geom)
	{
		RecastVertex bmin = _geom.MinBounds;
		RecastVertex bmax = _geom.MaxBounds;
		
		int gw = 0, gh = 0;
		
		CalcGridSize(bmin, bmax, _config.CellSize, out gw, out gh);
		
		int ts = settings.TileSize;
		int tw = (gw + ts - 1)/ts;
		int th = (gh + ts - 1)/ts;
		
		settings.TileWidth = tw;
		settings.TileHeight = th;
		
		int tileBits = Math.Min(ilog2(nextPow2(th*tw)), 14);
		if (tileBits > 14)
			tileBits = 14;
		
		int polyBits = 22 - tileBits;
		settings.MaxTiles = 1 << tileBits;
		settings.MaxPolysPerTile = 1 << polyBits;
	}
	
	private void CalcGridSize(RecastVertex bmin, RecastVertex bmax, float cellSize, out int w, out int h)
	{
		if (bmin != null && bmax != null)
		{
			w = (int) ((bmax.X - bmin.X)/cellSize + 0.5f);
			h = (int) ((bmax.Z - bmin.Z)/cellSize + 0.5f);
		}
		else
		{
			w = 0;
			h = 0;
		}
	}
	
	private int nextPow2(int v)
	{
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
		return v;
	}
	
	private int ilog2(int v)
	{
		int r;
		int shift;
		
		r = ((v > 0xffff) ? 1 : 0) << 4;
		v >>= r;
		shift = ((v > 0xff) ? 1 : 0) << 3;
		v >>= shift;
		r |= shift;
		shift = ((v > 0xf) ? 1 : 0) << 2;
		v >>= shift;
		r |= shift;
		shift = ((v > 0x3) ? 1 : 0) << 1;
		v >>= shift;
		r |= shift;
		r |= (v >> 1);
		return r;
	}
}
