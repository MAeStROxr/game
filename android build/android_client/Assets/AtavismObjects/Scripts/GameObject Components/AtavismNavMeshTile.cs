﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Detour;
using Recast;
using Recast.Data;
using RecastNavCSharp.Recast.Data;

public class AtavismNavMeshTile : MonoBehaviour {
	
	public List<Vector3> NavMeshVerts = new List<Vector3>();
	public List<int> NavMeshTriangles = new List<int>();
	public List<Color> NavMeshColors = new List<Color>();
	public List<Vector2> NavMeshUVs = new List<Vector2>();
	public Material Mat { get; set; }
	public bool MeshActive = false;
	
	public int tileX;
	public int tileY;
	public Bounds bounds;
	public AtavismNavMesh navMesh;
	RecastNavTile navTile;
	MeshTile meshTile;
	AtavismNavTile builder;
	
	public Config Config { get; set; }
	public Geometry Geometry { get; set; }
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void SetTileData(int x, int y, Bounds b, AtavismNavMesh navMesh) {
		this.tileX = x;
		this.tileY = y;
		this.bounds = b;
		this.navMesh = navMesh;
	}
	
	public void GenerateNavMesh() {
		// Delete existing mesh
		if (transform.FindChild("NavMeshTile") != null)
			DestroyImmediate(transform.FindChild("NavMeshTile").gameObject);
		if (navMesh.settings == null) {
			navMesh.InitNavMesh(true);
		}
		navMesh.settings.Tags = navMesh.tags;
		Config = navMesh.Config;
		
		Geometry = new Geometry();
		BuildGeometry(navMesh.settings, Config, Geometry);
		BuildTile(tileX, tileY);
		BuildGeometry();
	}
	
	/// <summary>
	/// This takes the current geometry and builds the data to go into Recast
	/// It needs to be updated to take into account scale, position, and rotation
	/// It needs to be updated to look for specific tags
	/// </summary>
	/// <param name="geom"></param>
	private void BuildGeometry(NavMeshSettings settings, Config config, Geometry geom)
	{
#if UNITY_EDITOR
		for (int i = 0; i < UnityEditorInternal.InternalEditorUtility.tags.Length; i++)
		{
			if ((settings.Tags & (1 << i)) != 0)
			{
				foreach (var gameObject in GameObject.FindGameObjectsWithTag(UnityEditorInternal.InternalEditorUtility.tags[i]))
				{
					foreach (var terrainObj in gameObject.GetComponentsInChildren<Terrain>())
					{
						// only get the heights for the area this tile covers
						var terrain = terrainObj.terrainData;
						float locX = (bounds.min.x - terrainObj.GetPosition().x) / terrain.size.x;
						float locZ = (bounds.min.z - terrainObj.GetPosition().z) / terrain.size.z;
						locX *= terrain.heightmapWidth;
						locZ *= terrain.heightmapHeight;
						//var width = terrain.heightmapWidth;
						//var height = terrain.heightmapHeight;
						int width = (int)bounds.size.x;
						int height = (int)bounds.size.z;
						width = (width * terrain.heightmapWidth / (int)terrain.size.x) + (tileX/2) + 5;
						height = (height * terrain.heightmapHeight / (int)terrain.size.z) + (tileY/2) + 5;
						if (width + (int)locX > terrain.heightmapWidth) {
							width = terrain.heightmapWidth - (int)locX;
						}
						if (height + (int)locZ > terrain.heightmapHeight) {
							height = terrain.heightmapHeight - (int)locZ;
						}
						var meshScale = terrain.size;
						var tRes = 1;
						meshScale = new Vector3(meshScale.x / (terrain.heightmapWidth - 1) * tRes, meshScale.y, meshScale.z / (terrain.heightmapHeight - 1) * tRes);
						
						var tData = terrain.GetHeights((int)locX, (int)locZ, width, height);
						//var tData = terrain.GetHeights(0, 0, width, height);
						
						width = (width - 1) / tRes + 1;
						height = (height - 1) / tRes + 1;
						var tVertices = new Vector3[width * height];
						var tPolys = new int[(width - 1) * (height - 1) * 6];
						
						// Build vertices and UVs
						for (int y = 0; y < height; y++)
						{
							for (int x = 0; x < width; x++)
							{
								tVertices[y * width + x] = Vector3.Scale(meshScale, new Vector3(x, tData[y * tRes, x * tRes], y)) + bounds.min;
							}
						}
						
						var index = 0;
						// Build triangle indices: 3 indices into vertex array for each triangle
						for (int y = 0; y < height - 1; y++)
						{
							for (int x = 0; x < width - 1; x++)
							{
								// For each grid cell output two triangles
								tPolys[index++] = (y * width) + x;
								tPolys[index++] = ((y + 1) * width) + x;
								tPolys[index++] = (y * width) + x + 1;
								
								tPolys[index++] = ((y + 1) * width) + x;
								tPolys[index++] = ((y + 1) * width) + x + 1;
								tPolys[index++] = (y * width) + x + 1;
							}
						}
						int subTotalVerts = geom.NumVertexes;
						foreach (var tVertex in tVertices)
						{
							geom.Vertexes.Add(new RecastVertex(tVertex.x, tVertex.y, tVertex.z));
							geom.NumVertexes++;
						}
						for (int j = 0; j < tPolys.Length; j += 3)
						{
							geom.Triangles.Add(tPolys[j] + subTotalVerts);
							geom.Triangles.Add(tPolys[j + 1] + subTotalVerts);
							geom.Triangles.Add(tPolys[j + 2] + subTotalVerts);
							geom.NumTriangles++;
						}
					}
					foreach (var componentsInChild in gameObject.GetComponents/*InChildren*/<MeshFilter>())
					{
						// Only include objects that overlap this tile
						if (Vector3.Distance(componentsInChild.transform.position, bounds.center) > 
						    bounds.extents.magnitude + componentsInChild.mesh.bounds.extents.magnitude) {
							continue;
						}
						/*if (componentsInChild.gameObject.GetComponent<Collider>() != null 
						    && !bounds.Intersects(componentsInChild.gameObject.GetComponent<Collider>().bounds)) {
							continue;
						}*/
						int subTotalVerts = geom.NumVertexes;
						foreach (Vector3 vector3 in componentsInChild.sharedMesh.vertices)
						{
							Vector3 vec = gameObject.transform.TransformPoint(vector3);
							geom.Vertexes.Add(new RecastVertex(vec.x, vec.y, vec.z));
							geom.NumVertexes++;
						}
						for (int j = 0; j < componentsInChild.sharedMesh.triangles.Length; j += 3)
						{
							geom.Triangles.Add(componentsInChild.sharedMesh.triangles[j] + subTotalVerts);
							geom.Triangles.Add(componentsInChild.sharedMesh.triangles[j + 1] + subTotalVerts);
							geom.Triangles.Add(componentsInChild.sharedMesh.triangles[j + 2] + subTotalVerts);
							geom.NumTriangles++;
						}
					}
					foreach (var offMeshConnector in gameObject.GetComponentsInChildren<OffMeshConnector>())
					{
						RecastVertex start = new RecastVertex(offMeshConnector.StartPosition.x, offMeshConnector.StartPosition.y, offMeshConnector.StartPosition.z);
						RecastVertex end = new RecastVertex(offMeshConnector.EndPosition.x, offMeshConnector.EndPosition.y, offMeshConnector.EndPosition.z);
						geom.AddOffMeshConnection(start, end, offMeshConnector.Radius, offMeshConnector.Bidirectional, 5, 8);
					}
				}
			}
		}
#endif
		
		if (geom.NumVertexes != 0)
		{
			geom.CalculateBounds();
			config.CalculateGridSize(geom);
			geom.CreateChunkyTriMesh();
		}
	}
	
	public void BuildTile(int x, int y)
	{
		//RecastVertex bmin = Geometry.MinBounds;
		//RecastVertex bmax = Geometry.MaxBounds;
		//RecastVertex bmin = new RecastVertex(bounds.min.x - 1, bounds.min.y, bounds.min.z - 1);
		//RecastVertex bmax = new RecastVertex(bounds.max.x + 1, bounds.max.y, bounds.max.z + 1);
		RecastVertex bmin = new RecastVertex(navMesh.origin.x, bounds.min.y, navMesh.origin.z);
		RecastVertex bmax = new RecastVertex(navMesh.origin.x, bounds.max.y, navMesh.origin.z);
		float tcs = Config.TileSize * Config.CellSize;
		RecastVertex tileBMin = new RecastVertex();
		RecastVertex tileBMax = new RecastVertex();
		tileBMin.X = bmin.X + x * tcs;
		tileBMin.Y = bmin.Y;
		tileBMin.Z = bmin.Z + y * tcs;
		
		tileBMax.X = bmin.X + (x + 1) * tcs;
		tileBMax.Y = bmax.Y;
		tileBMax.Z = bmin.Z + (y + 1) * tcs;
		
		var builder = BuildTileMesh(x, y, tileBMin, tileBMax);
		//var builder = BuildTileMesh(x, y, bmin, bmax);
		
		// remove/add new tile?
		if (builder != null)
		{
			Detour.AtavismNavTile outBuilder;
			// nav mesh remove tile
			navMesh.NavMesh.RemoveTile(navMesh.NavMesh.GetTileRefAt(x, y, 0), out outBuilder);
			// nav mesh add tile
			long result = 0;
			navMesh.NavMesh.AddTile(builder, Detour.NavMesh.TileFreeData, 0, ref result);
		}
	}
	
	private Detour.AtavismNavTile BuildTileMesh(int tx, int ty, RecastVertex min, RecastVertex max)
	{
		Config.Width = Config.TileSize + Config.BorderSize * 2;
		Config.Height = Config.TileSize + Config.BorderSize * 2;
		Config.MinBounds = min;
		Config.MaxBounds = max;
		Config.MinBounds.X -= Config.BorderSize * Config.CellSize;
		Config.MinBounds.Z -= Config.BorderSize * Config.CellSize;
		
		Config.MaxBounds.X += Config.BorderSize * Config.CellSize;
		Config.MaxBounds.Z += Config.BorderSize * Config.CellSize;
		
		HeightField heightfield = new HeightField(Config.Width, Config.Height, Config.MinBounds.ToArray(), Config.MaxBounds.ToArray(), Config.CellSize, Config.CellHeight);
		
		short[] triAreas = new short[Geometry.ChunkyTriMesh.MaxTrisPerChunk];
		
		float[] tbmin = new float[2], tbmax = new float[2];
		tbmin[0] = Config.MinBounds.X;
		tbmin[1] = Config.MinBounds.Z;
		
		tbmax[0] = Config.MaxBounds.X;
		tbmax[1] = Config.MaxBounds.Z;
		
		int[] cid = new int[512];
		
		int ncid = Geometry.ChunkyTriMesh.GetChunksOverlappingRect(tbmin, tbmax, ref cid, 512);
		
		if (ncid == 0)
			return null;
		
		for (int i = 0; i < ncid; i++)
		{
			ChunkyTriMeshNode node = Geometry.ChunkyTriMesh.Nodes[cid[i]];
			int[] tris = new int[node.n * 3];
			Array.Copy(Geometry.ChunkyTriMesh.Tris, node.i * 3, tris, 0, node.n * 3);
			List<int> ctris = new List<int>(tris);
			int nctris = node.n;
			
			Array.Clear(triAreas, 0, triAreas.Length);
			Geometry.MarkWalkableTriangles(Config.WalkableSlopeAngle, ctris, nctris, ref triAreas);
			
			heightfield.RasterizeTriangles(Geometry, ctris, nctris, triAreas, Config.WalkableClimb);
		}
		
		heightfield.FilterLowHangingWalkableObstacles(Config.WalkableClimb);
		heightfield.FilterLedgeSpans(Config.WalkableHeight, Config.WalkableClimb); // This causes a gap around the tiles
		heightfield.FilterWalkableLowHeightSpans(Config.WalkableHeight);
		
		
		CompactHeightfield compactHeightfield = new CompactHeightfield(Config.WalkableHeight, Config.WalkableClimb, heightfield);
		compactHeightfield.ErodeWalkableArea(Config.WalkableRadius);
		
		// optional convex volumes
		
		compactHeightfield.BuildDistanceField();
		compactHeightfield.BuildRegions(Config.BorderSize, Config.MinRegionArea, Config.MergeRegionArea);
		
		
		ContourSet contourSet = new ContourSet(compactHeightfield, Config.MaxSimplificationError, Config.MaxEdgeLength);
		
		if (contourSet.NConts == 0)
			return null;
		
		PolyMesh polyMesh = new PolyMesh(contourSet, Config.MaxVertexesPerPoly);
		
		DetailPolyMesh detailPolyMesh = new DetailPolyMesh(polyMesh, compactHeightfield, Config.DetailSampleDistance,
		                                                   Config.DetailSampleMaxError);
		
		// Convert the Areas and Flags for path weighting
		for (int i = 0; i < polyMesh.NPolys; i++)
		{
			
			if (polyMesh.Areas[i] == Geometry.WalkableArea)
			{
				polyMesh.Areas[i] = 0; // Sample_polyarea_ground
				polyMesh.Flags[i] = 1; // Samply_polyflags_walk
			}
		}
		NavMeshCreateParams param = new NavMeshCreateParams
		{
			Verts = polyMesh.Verts,
			VertCount = polyMesh.NVerts,
			Polys = polyMesh.Polys,
			PolyAreas = polyMesh.Areas,
			PolyFlags = polyMesh.Flags,
			PolyCount = polyMesh.NPolys,
			Nvp = polyMesh.Nvp,
			DetailMeshes = detailPolyMesh.Meshes,
			DetailVerts = detailPolyMesh.Verts,
			DetailVertsCount = detailPolyMesh.NVerts,
			DetailTris = detailPolyMesh.Tris,
			DetailTriCount = detailPolyMesh.NTris,
			
			// Off Mesh data
			OffMeshConVerts = Geometry.OffMeshConnectionVerts.ToArray(),
			OffMeshConRad = Geometry.OffMeshConnectionRadii.ToArray(),
			OffMeshConDir = Geometry.OffMeshConnectionDirections.ToArray(),
			OffMeshConAreas = Geometry.OffMeshConnectionAreas.ToArray(),
			OffMeshConFlags = Geometry.OffMeshConnectionFlags.ToArray(),
			OffMeshConUserId = Geometry.OffMeshConnectionIds.ToArray(),
			OffMeshConCount = (int)Geometry.OffMeshConnectionCount,
			// end off mesh data
			
			WalkableHeight = Config.WalkableHeight,
			WalkableRadius = Config.WalkableRadius,
			WalkableClimb = Config.WalkableClimb,
			BMin = new float[] { polyMesh.BMin[0], polyMesh.BMin[1], polyMesh.BMin[2] },
			BMax = new float[] { polyMesh.BMax[0], polyMesh.BMax[1], polyMesh.BMax[2] },
			Cs = polyMesh.Cs,
			Ch = polyMesh.Ch,
			BuildBvTree = true,
			TileX = tx,
			TileY = ty,
			TileLayer = 0
		};
		return new Detour.AtavismNavTile(param);
	}
	
	public void BuildGeometry()
	{
		BuildNavMeshGeometry();
		DrawNavMeshGeometry();
	}
	
	#region NavMesh Geometry Functions
	
	/// <summary>
	/// Builds the NavMesh geometry into a Unity3d Mesh to display for debug
	/// </summary>
	public void BuildNavMeshGeometry()
	{
		if (navTile != null)
			DestroyImmediate(navTile.MeshObject);
		if (navMesh.NavMesh != null)
		{
			NavMeshTriangles = new List<int>();
			NavMeshVerts = new List<Vector3>();
			NavMeshUVs = new List<Vector2>();
			NavMeshColors = new List<Color>();
			meshTile = navMesh.NavMesh.GetTileAt(tileX, tileY, 0);
			if (meshTile.Header == null) 
				return;
			navTile = new RecastNavTile();
			long baseId = navMesh.NavMesh.GetPolyRefBase(meshTile);
			for (int j = 0; j < meshTile.Header.PolyCount; j++)
			{
				BuildNavMeshPoly(baseId | (uint)j, duRGBA(0, 0, 64, 128), NavMeshVerts, NavMeshColors, NavMeshUVs, NavMeshTriangles, navTile);
			}
			navTile.x = meshTile.Header.X;
			navTile.y = meshTile.Header.Y;
			navTile.builder = meshTile.Data;
		}
	}
	
	/// <summary>
	/// Builds a Single polygon out of the NavMesh, called by BuildNavMeshGeometry
	/// </summary>
	/// <param name="refId"></param>
	/// <param name="color"></param>
	/// <param name="verts"></param>
	/// <param name="colors"></param>
	/// <param name="uvs"></param>
	/// <param name="tris"></param>
	private void BuildNavMeshPoly(long refId, Color color, List<Vector3> verts, List<Color> colors, List<Vector2> uvs, List<int> tris, RecastNavTile navTile)
	{
		
		MeshTile tile = null; 
		Poly poly = null;
		if ((navMesh.NavMesh.GetTileAndPolyByRef(refId, ref tile, ref poly) & Status.Failure) != 0)
			return;
		
		long ip = 0;
		for (int i = 0; i < tile.Polys.Length; i++)
		{
			if (poly == tile.Polys[i])
				ip = i;
		}
		if (poly.Type == Detour.AtavismNavTile.PolyTypeOffMeshConnection)
		{
		}
		else
		{
			PolyDetail pd = tile.DetailMeshes[ip];
			for (int i = 0; i < pd.TriCount; i++)
			{
				int t = ((int)pd.TriBase + i) * 4;
				for (int j = 0; j < 3; j++)
				{
					if (tile.DetailTris[t + j] < poly.VertCount)
					{
						Vector3 newVerts = new Vector3(tile.Verts[poly.Verts[tile.DetailTris[t + j]] * 3 + 0], tile.Verts[poly.Verts[tile.DetailTris[t + j]] * 3 + 1], tile.Verts[poly.Verts[tile.DetailTris[t + j]] * 3 + 2]);
						verts.Add(newVerts);
						navTile.NavMeshVerts.Add(newVerts);
					}
					else
					{
						Vector3 newVerts = new Vector3(tile.DetailVerts[(pd.VertBase + tile.DetailTris[t + j] - poly.VertCount) * 3 + 0],
						                               tile.DetailVerts[(pd.VertBase + tile.DetailTris[t + j] - poly.VertCount) * 3 + 1],
						                               tile.DetailVerts[(pd.VertBase + tile.DetailTris[t + j] - poly.VertCount) * 3 + 2]);
						verts.Add(newVerts);
						navTile.NavMeshVerts.Add(newVerts);
					}
					uvs.Add(new Vector2());
					navTile.NavMeshUVs.Add(new Vector2());
					colors.Add(color);//duIntToCol((int)ip, 192));
					navTile.NavMeshColors.Add(color);
					tris.Add(tris.Count);
					navTile.NavMeshTriangles.Add(navTile.NavMeshTriangles.Count);
				}
			}
		}
		
	}
	
	/// <summary>
	/// Converts an RGBA of intgers into a Unity3d Color
	/// </summary>
	/// <param name="r"></param>
	/// <param name="g"></param>
	/// <param name="b"></param>
	/// <param name="a"></param>
	/// <returns></returns>
	Color duRGBA(int r, int g, int b, int a)
	{
		return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
	}
	
	/// <summary>
	/// Creates a Mesh Object as a child of this object and builds the mesh from the data built by BuildNavMeshGeometry
	/// </summary>
	public void DrawNavMeshGeometry()
	{
		
		navTile.MeshObject = new GameObject("NavMeshTile");
		navTile.MeshObject.transform.parent = this.transform;
		navTile.MeshObject.AddComponent<MeshFilter>();
		navTile.MeshObject.AddComponent<MeshRenderer>();
		
		navTile.MeshFilter = navTile.MeshObject.GetComponent<MeshFilter>();
		navTile.RecastMesh = new Mesh();
		navTile.RecastMesh.vertices = navTile.NavMeshVerts.ToArray();
		navTile.RecastMesh.triangles = navTile.NavMeshTriangles.ToArray();
		navTile.RecastMesh.colors = navTile.NavMeshColors.ToArray();
		navTile.RecastMesh.uv = navTile.NavMeshUVs.ToArray();
		navTile.RecastMesh.RecalculateNormals();
		navTile.MeshFilter.sharedMesh = navTile.RecastMesh;
		navTile.MeshObject.GetComponent<MeshRenderer>().sharedMaterial = Mat;
		
		Builder = navTile.builder;
		MeshActive = true;
	}
	
	#endregion
	
	public RecastNavTile NavTile {
		get {
			return navTile;
		}
		set {
			navTile = value;
		}
	}
	
	public AtavismNavTile Builder {
		get {
			return builder;
		}
		set {
			builder = value;
		}
	}
}
