﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AtavismRegion))]
public class AtavismRegionEditor : Editor {

	private bool effectsLoaded = false;
	private bool questsLoaded = false;
	private bool tasksLoaded = false;
	private bool instancesLoaded = false;
	string[] interactionTypes;
	
	public override void OnInspectorGUI() {
		AtavismRegion obj = target as AtavismRegion;
		
		EditorGUILayout.LabelField("ID: " + obj.id);
		
		// Read in Interaction options from Database and display a drop down list
		if (interactionTypes == null) {
			interactionTypes = new string[] {"~ none ~"};
			interactionTypes = ServerOptionChoices.LoadAtavismChoiceOptions("Interaction Type", false);
		}
		
		/*int selectedOption = GetPositionOfInteraction(obj.regionType);
		selectedOption = EditorGUILayout.Popup("Region Type:", selectedOption, interactionTypes); 
		obj.regionType = interactionTypes[selectedOption];*/
		obj.regionType = (AtavismRegionType) EditorGUILayout.EnumPopup("Region Type", obj.regionType);
		
		if (!questsLoaded)
			ServerQuests.LoadQuestOptions();
		questsLoaded = true;
		
		if (obj.regionType == AtavismRegionType.Water) {
			if (!effectsLoaded)
				ServerEffects.LoadEffectOptions();
			effectsLoaded = true;
			
			obj.actionID = EditorGUILayout.IntPopup("Breath Effect:", obj.actionID, ServerEffects.effectOptions, ServerEffects.effectIds); 
		} else if (obj.regionType == AtavismRegionType.ApplyEffect) {
			if (!effectsLoaded)
				ServerEffects.LoadEffectOptions();
			effectsLoaded = true;
			
			obj.actionID = EditorGUILayout.IntPopup("Effect:", obj.actionID, ServerEffects.effectOptions, ServerEffects.effectIds); 
		} else if (obj.regionType == AtavismRegionType.StartQuest) {
			obj.actionID = EditorGUILayout.IntPopup("Quest:", obj.actionID, ServerQuests.questOptions, ServerQuests.questIds); 
		} else if (obj.regionType == AtavismRegionType.CompleteTask) {
			if (!tasksLoaded)
				ServerTask.LoadTaskOptions();
			tasksLoaded = true;
			
			obj.actionID = EditorGUILayout.IntPopup("Task:", obj.actionID, ServerTask.taskOptions, ServerTask.taskIds); 
		} else if (obj.regionType == AtavismRegionType.Teleport) {
			if (!instancesLoaded)
				ServerInstances.LoadInstanceOptions();
			instancesLoaded = true;
			
			obj.actionID = EditorGUILayout.IntPopup("Instance:", obj.actionID, ServerInstances.instanceList, ServerInstances.instanceIds); 
			// Need to get a location to teleport to
			Vector3 position = new Vector3();
			float.TryParse(obj.actionData1, out position.x);
			float.TryParse(obj.actionData2, out position.y);
			float.TryParse(obj.actionData3, out position.z);
			position = EditorGUILayout.Vector3Field("Position:", position);
			obj.actionData1 = position.x.ToString();
			obj.actionData2 = position.y.ToString();
			obj.actionData3 = position.z.ToString();
		}
		
		// Quest Required ID
		//obj.questReqID = EditorGUILayout.IntPopup("Quest Required:", obj.questReqID, ServerQuests.questOptions, ServerQuests.questIds); 
		
		// Coord Effect
		//obj.interactCoordEffect = (GameObject) EditorGUILayout.ObjectField("Interact Coord Effect:", obj.interactCoordEffect, typeof(GameObject), false);
		
		// Interaction Time
		//obj.interactTimeReq = EditorGUILayout.FloatField("Interaction Duration:", obj.interactTimeReq);
		
		// Respawn Time
		//obj.refreshDuration = EditorGUILayout.IntField("Respawn Time:", obj.refreshDuration);
		
		// Cursor Icon
		//obj.cursorIcon = (Texture2D) EditorGUILayout.ObjectField("Mouse Over Cursor:", obj.cursorIcon, typeof(Texture2D), false);
		
		// highlight colour
		//obj.highlightColour = EditorGUILayout.ColorField("Highlight Colour:", obj.highlightColour);
	}
}
