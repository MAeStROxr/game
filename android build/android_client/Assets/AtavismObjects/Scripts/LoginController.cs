﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public enum LoginState
{
	Login,
	Register,
	Authenticating,
	CharacterSelect,
	CharacterCreate
}

public class LoginController : MonoBehaviour {
	
	public GameObject loginPanel;
	public GameObject registerPanel;
	public UGUIDialogPopup dialogPanel;
	public GameObject soundMenu;
	public GameObject musicObject;
	public bool useMd5Encryption = true;
	string characterScene = "CharacterSelection";
	
	#region fields
	LoginState loginState;
	
	// Login fields
	string username = "";
	string password = "";
	
	// Registration fields
	string regUsername = "";
	string regPassword = "";
	string password2 = "";
	string email = "";
	string email2 = "";
	
	#endregion fields

	// Use this for initialization
	void Start () {
		loginState = LoginState.Login;
		AtavismEventSystem.RegisterEvent("LOGIN_RESPONSE", this);
		AtavismEventSystem.RegisterEvent("REGISTER_RESPONSE", this);
		// Play music
		SoundSystem.LoadSoundSettings();
		if (musicObject != null)
			SoundSystem.PlayMusic(musicObject.GetComponent<AudioSource>());
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("LOGIN_RESPONSE", this);
		AtavismEventSystem.UnregisterEvent("REGISTER_RESPONSE", this);
	}
	
	public void ShowLoginPanel() {
		loginState = LoginState.Login;
		loginPanel.SetActive(true);
		registerPanel.SetActive(false);
	}
	
	public void ShowRegisterPanel() {
		loginState = LoginState.Register;
		loginPanel.SetActive(false);
		registerPanel.SetActive(true);
	}
	
	public void CancelRegistration() {
		ShowLoginPanel();
	}
	
	public void SetUserName(string username) {
		if (loginState == LoginState.Login) {
			this.username = username;
		} else {
			this.regUsername = username;
		}
	}
	
	public void SetPassword(string password) {
		if (loginState == LoginState.Login) {
			this.password = password;
		} else {
			this.regPassword = password;
		}
	}
	
	public void SetPassword2(string password2) {
		this.password2 = password2;
	}
	
	public void SetEmail(string email) {
		this.email = email;
	}
	
	public void SetEmail2(string email2) {
		this.email2 = email2;
	}
	
	public void Login() {
		if (username == "") {
			ShowDialog("Please enter a username", true);
			return;
		}
		//ShowDialog("Logging In...", false);
		Dictionary<string, object> props = new Dictionary<string, object>();
		/*props.Add("stringprop", "hi");
		props.Add("intprop", 5);
		props.Add("boolprop", true);
		props.Add("floatprop", -0.3f);*/
		if (useMd5Encryption) {
			AtavismClient.Instance.Login(username, AtavismEncryption.Md5Sum(password), props);
		} else {
			AtavismClient.Instance.Login(username, password, props);
		}
	}
	
	public void Register() {
		if (regUsername == "") {
			ShowDialog("Please enter a username", true);
			return;
		}
		if (regUsername.Length < 4) {
			ShowDialog("Your username must be at least 4 characters long", true);
			return;
		}
		foreach(char chr in regUsername) {
			if ( (chr < 'a' || chr > 'z') && (chr < 'A' || chr > 'Z') && (chr < '0' || chr > '9') ) {
				ShowDialog("Your username can only contain letters and numbers", true);
				return;
			}
		}
		if (regPassword == "") {
			ShowDialog("Please enter a password", true);
			return;
		}
		foreach(char chr in regPassword) {
			if (chr == '*' || chr == '\'' || chr == '"' || chr == '/' || chr == '\\' || chr == ' ') {
				ShowDialog("Your password cannot contain * \' \" / \\ or spaces", true);
				return;
			}
		}
		if (regPassword.Length < 6) {
			ShowDialog("Your password must be at least 6 characters long", true);
			return;
		}
		if (regPassword != password2) {
			ShowDialog("Your passwords must match", true);
			return;
		}
		if (email == "") {
			ShowDialog("Please enter an email address", true);
			return;
		}
		if (!ValidateEmail(email)) {
			ShowDialog("Please enter a valid email address", true);
			return;
		}
		if (email != email2) {
			ShowDialog("Your email addresses must match", true);
			return;
		}
		if (useMd5Encryption) {
			AtavismClient.Instance.CreateAccount(regUsername, AtavismEncryption.Md5Sum(regPassword), email);
		} else {
			AtavismClient.Instance.CreateAccount(regUsername, regPassword, email);
		}

	}
	
	private bool ValidateEmail (string email)
	{
		Regex regex = new Regex (@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
		Match match = regex.Match (email);
		if (match.Success)
			return true;
		else
			return false;
	}
	
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "LOGIN_RESPONSE") {
			if (eData.eventArgs[0] == "Success") {
				Application.LoadLevel(characterScene);
			} else {
				string errorType = eData.eventArgs[0];
				string errorMessage = errorType;
				if (errorType == "LoginFailure") {
					errorMessage = "Invalid username or password";
				} else if (errorType == "NoAccessFailure") {
					errorMessage = "Your account does not have access to log in";
				} else if (errorType == "BannedFailure") {
					errorMessage = "Your account has been banned";
				} else if (errorType == "SubscriptionExpiredFailure") {
					errorMessage = "Your account does not have an active subscription";
				}
				ShowDialog(errorMessage, true);
			}
		} else if (eData.eventType == "REGISTER_RESPONSE") {
			if (eData.eventArgs[0] == "Success") {
				ShowLoginPanel();
				ShowDialog("Account created. You can now log in", true);
			} else {
				string errorType = eData.eventArgs[0];
				string errorMessage = errorType;
				if (errorType == "UsernameUsed") {
					errorMessage = "An account with that username already exists";
				} else if (errorType == "EmailUsed") {
					errorMessage = "An account with that email address already exists";
				} else if (errorType == "Unknown") {
					errorMessage = "Unknown error. Please let the Neojac team know";
				} else if (errorType == "MasterTcpConnectFailure") {
					errorMessage = "Unable to connect to the Authentication Server";
				} else if (errorType == "NoAccessFailure") {
					errorMessage = "Account creation has been disabled on this server";
				}
				ShowDialog(errorMessage, true);
			}
		}
	}
	
	void ShowDialog(string message, bool showButton) {
		if (dialogPanel == null)
			return;
		dialogPanel.gameObject.SetActive(true);
		dialogPanel.ShowDialogPopup(message, showButton);
	}
	
	void ShowDialogWithButton(string message) {
		if (dialogPanel == null)
			return;
		dialogPanel.gameObject.SetActive(true);
		dialogPanel.ShowDialogPopup(message, true);
	}
	
	public string CharacterScene {
		get {
			return characterScene;
		}
		set {
			characterScene = value;
		}
	}
}
