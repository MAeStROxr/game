﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using System.Collections;

public class ShootWeapon : MonoBehaviour {

	public KeyCode shootKey;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (ControlFreak2.CF2Input.GetKey(shootKey)) {
			Debug.Log("sending shoot");
			//int id = (int)ClientAPI.GetPlayerObject().GetProperty("combat.autoability");
			int id = 5;
			NetworkAPI.SendTargetedCommand(ClientAPI.GetTargetOid(), "/ability " + id);
			//NetworkAPI.SendAttackMessage (ClientAPI.GetTargetOid(), "strike", true);
			//NetworkAPI.SendAttackMessage (ClientAPI.GetTargetOid(), "strike", false);
		}
	}
}
