﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUICharacterRaceSlot : MonoBehaviour {

	Button button;
	Sprite normalSprite;
	Color normalColor;
	public AtavismRaceData raceData;
	bool selected = false;

	// Use this for initialization
	void Awake () {
		button = GetComponent<Button>();
		button.image.sprite = raceData.raceIcon;
		//normalSprite = button.image.sprite;
		normalColor = button.colors.normalColor;
	}

	// Update is called once per frame
	void Update () {
	
	}
	
	public void SelectRace() {
		CharacterSelectionCreationManager.Instance.SetCharacterRace(raceData);
	}

	public void OnMouseEnter() {
		if (!selected) {
			button.image.color = button.colors.highlightedColor;
		}
	}

	public void OnMouseExit() {
		if (!selected) {
			button.image.color = normalColor;
		}
	}

	public void RaceSelected(AtavismRaceData selectedRace) {
		if (selectedRace.Equals(raceData)) {
			//button.image.sprite = button.spriteState.pressedSprite;
			button.image.color = button.colors.pressedColor;
			selected = true;
		} else {
			//button.image.sprite = normalSprite;
			button.image.color = normalColor;
			selected = false;
		}
	}
}
