﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUICharacterSelectSlot : MonoBehaviour {

	public Text name;
	public Text classText;
	public Text level;
	public Image portrait;
	public Button button;
	public PortraitType portraitType;
	Sprite normalSprite;
	Color normalColor;
	CharacterEntry character;
	bool selected = false;

	// Use this for initialization
	void Awake () {
		//normalSprite = button.image.sprite;
		normalColor = button.colors.normalColor;
	}

	// Update is called once per frame
	void Update () {
	
	}
	
	public void SelectCharacter() {
		CharacterSelectionCreationManager.Instance.CharacterSelected(character);
	}

	public void OnMouseEnter() {
		if (!selected) {
			button.image.color = button.colors.highlightedColor;
		}
	}

	public void OnMouseExit() {
		if (!selected) {
			button.image.color = normalColor;
		}
	}

	public void SetCharacter(CharacterEntry character) {
		this.character = character;
		if (name != null)
			name.text = (string)character["characterName"];
		if (classText != null)
			classText.text = (string)character["aspect"];
		if (level != null) {
			if (character.ContainsKey("level")) {
				level.text = "Level " + (int)character["level"];
			} else {
				level.text = "Level 1";
			}
		}

		string gender = (string)character["gender"];
		Sprite portraitSprite = PortraitManager.Instance.GetCharacterSelectionPortrait(gender, (string)character["race"], 
				(string)character["aspect"], portraitType);
		if (portrait != null && portraitSprite != null) {
			portrait.sprite = portraitSprite;
		}
	}

	public void CharacterSelected(CharacterEntry selectedChar) {
		if (selectedChar.Equals(character)) {
			//button.image.sprite = button.spriteState.pressedSprite;
			button.image.color = button.colors.pressedColor;
			selected = true;
		} else {
			//button.image.sprite = normalSprite;
			button.image.color = normalColor;
			selected = false;
		}
	}
}
