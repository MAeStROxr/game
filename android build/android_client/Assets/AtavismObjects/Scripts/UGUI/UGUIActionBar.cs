﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIActionBar : MonoBehaviour {

	public int barNum;
	public List<UGUIActionBarSlot> slots;

	// Use this for initialization
	void Start () {
		ClientAPI.ScriptObject.GetComponent<Actions>().AddActionBar(gameObject, barNum);
		
		AtavismEventSystem.RegisterEvent("ACTION_UPDATE", this);
		AtavismEventSystem.RegisterEvent("COOLDOWN_UPDATE", this);
		UpdateActions();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("ACTION_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("COOLDOWN_UPDATE", this);
	}

	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "ACTION_UPDATE") {
			UpdateActions();
		} else if (eData.eventType == "COOLDOWN_UPDATE") {
			UpdateActions();
		}
	}

	public void UpdateActions() {
		List<List<AtavismAction>> actionBars = ClientAPI.ScriptObject.GetComponent<Actions>().PlayerActions;
		if (actionBars.Count == 0) {
			return;
		}
		
		List<AtavismAction> actionBar = actionBars[0];
		for (int i = 0; i < slots.Count; i++) {
			if (actionBar.Count > i) {
				slots[i].UpdateActionData(actionBar[i]);
			} else {
				slots[i].UpdateActionData(null);
			}
		}
	}

	public void Toggle() {
		gameObject.SetActive(!gameObject.activeSelf);
	}
}
