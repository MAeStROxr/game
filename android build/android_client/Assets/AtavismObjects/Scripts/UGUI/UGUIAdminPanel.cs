﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public delegate void AdminChooseEntryClicked(int id);

public class UGUIAdminPanel : UIList<UGUIAdminChooseEntry> {

	enum ChooseType {
		Currency,
		Item,
		Skill
	}
	
	static UGUIAdminPanel instance;
	
	public UGUIPanelTitleBar titleBar;
	public Text gmStatusText;
	public Text instanceText;
	public Text positionText;
	
	public RectTransform teleportPanel;
	public RectTransform gainCommandsPanel;
	
	public InputField teleportToPlayerField;
	public InputField summonPlayerField;
	public InputField changeInstanceField;
	public InputField gotoXField;
	public InputField gotoYField;
	public InputField gotoZField;
	
	public InputField expField;
	public Button chooseItemButton;
	public InputField itemCountField;
	public Button chooseCurrencyButton;
	public InputField currencyCountField;
	public Button chooseSkillButton;
	public InputField skillCountField;
	public RectTransform choosePanel;
	public Text chooseTitle;
	public InputField filterInputField;
	
	bool gmActive = false;
	ChooseType chooseType = ChooseType.Currency;
	int currencyID = -1;
	int itemID = -1;
	int skillID = -1;
	
	bool showing = false;

	// Use this for initialization
	void Start () {
		instance = this;
		
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		Hide ();
		
		choosePanel.gameObject.SetActive(false);
		
		ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler("gm", GMHandler);
		if (ClientAPI.GetPlayerObject().PropertyExists("gm")) {
			gmActive = (bool)ClientAPI.GetPlayerObject().GetProperty("gm");
		}
		
		if (gmActive) {
			gmStatusText.text = "Active";
		} else {
			gmStatusText.text = "Inactive";
		}
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		showing = true;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		showing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (showing) {
			instanceText.text = Application.loadedLevelName;
			positionText.text = ClientAPI.GetPlayerObject().Position.x.ToString("n2") + "," 
				+ ClientAPI.GetPlayerObject().Position.y.ToString("n2")
				+ "," + ClientAPI.GetPlayerObject().Position.z.ToString("n2");
		}
	}
	
	public void GMHandler(object sender, PropertyChangeEventArgs args) {
		gmActive = (bool)ClientAPI.GetPlayerObject().GetProperty("gm");
		if (gmActive) {
			gmStatusText.text = "Active";
		} else {
			gmStatusText.text = "Inactive";
		}
	}
	
	public void ToggleGMMode() {
		if (gmActive) {
			NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/gm 0");
		} else {
			NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/gm 1");
		}
	}
	
	public void ShowTeleportOptions() {
		teleportPanel.gameObject.SetActive(true);
		gainCommandsPanel.gameObject.SetActive(false);
	}
	
	public void ShowGainCommands() {
		teleportPanel.gameObject.SetActive(false);
		gainCommandsPanel.gameObject.SetActive(true);
	}
	
	public void ShowSpawner() {
		Camera.main.GetComponentInChildren<MobCreator>().ToggleBuildingModeEnabled();
	}
	
	public void TeleportToPlayer() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/gotoplayer " + teleportToPlayerField.text);
	}
	
	public void SummonPlayer() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/summon " + summonPlayerField.text);
	}
	
	public void ChangeInstance() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/ci " + changeInstanceField.text);
	}
	
	public void GotoPosition() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/gotopos " + gotoXField.text + " " 
			+ gotoYField.text + " " + gotoZField.text);
	}
	
	public void GetExperience() {
		int expAmount = int.Parse(expField.text);
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/getExp " + expAmount);
	}
	
	public void ChooseCurrency() {
		chooseType = ChooseType.Currency;
		choosePanel.gameObject.SetActive(true);
		chooseTitle.text = "Choose Currency";
		// Delete the old list
		ClearAllCells();
		Refresh();
	}
	
	public void ChooseItem() {
		chooseType = ChooseType.Item;
		choosePanel.gameObject.SetActive(true);
		chooseTitle.text = "Choose Item";
		// Delete the old list
		ClearAllCells();
		Refresh();
	}
	
	public void ChooseSkill() {
		chooseType = ChooseType.Skill;
		choosePanel.gameObject.SetActive(true);
		chooseTitle.text = "Choose Skill";
		// Delete the old list
		ClearAllCells();
		Refresh();
	}
	
	public void UpdateChooseFilter(string filterText) {
		// Delete the old list
		ClearAllCells();
		Refresh();
	}
	
	public void CurrencySelected(int id) {
		currencyID = id;
		choosePanel.gameObject.SetActive(false);
		chooseCurrencyButton.GetComponentInChildren<Text>().text = currencyID.ToString();
	}
	
	public void ItemSelected(int id) {
		itemID = id;
		choosePanel.gameObject.SetActive(false);
		chooseItemButton.GetComponentInChildren<Text>().text = itemID.ToString();
	}
	
	public void SkillSelected(int id) {
		skillID = id;
		choosePanel.gameObject.SetActive(false);
		chooseSkillButton.GetComponentInChildren<Text>().text = skillID.ToString();
	}
	
	public void GenerateItem() {
		int count = int.Parse(itemCountField.text);
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/gi " + itemID + " " + count);
	}
	
	public void GetCurrency() {
		int count = int.Parse(currencyCountField.text);
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/getCurrency " + currencyID + " " + count);
	}
	
	public void GainSkill() {
		int count = int.Parse(skillCountField.text);
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/getSkillCurrent " + skillID + " " + count);
	}
	
	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int count = 0;
		if (chooseType == ChooseType.Currency) {
			foreach(Currency currency in Inventory.Instance.Currencies.Values) {
				if (currency.name.Contains(filterInputField.text))
					count++;
			}
		} else if (chooseType == ChooseType.Item) {
			foreach(AtavismInventoryItem item in Inventory.Instance.Items.Values) {
				if (item.name.Contains(filterInputField.text))
					count++;
			}
		} else if (chooseType == ChooseType.Skill) {
			foreach(Skill skill in Skills.Instance.SkillsList.Values) {
				if (skill.skillname.Contains(filterInputField.text))
					count++;
			}
		}
		return count;
	}
	
	public override void UpdateCell (int index, UGUIAdminChooseEntry cell)
	{
		if (chooseType == ChooseType.Currency) {
			List<Currency> activeCurrencies = new List<Currency>();
			foreach(Currency currency in Inventory.Instance.Currencies.Values) {
				if (currency.name.Contains(filterInputField.text))
					activeCurrencies.Add(currency);
			}
			Currency c = activeCurrencies[index];
			cell.SetEntryDetails(CurrencySelected, c.id, c.name, c.icon);
		} else if (chooseType == ChooseType.Item) {
			List<AtavismInventoryItem> activeItems = new List<AtavismInventoryItem>();
			foreach(AtavismInventoryItem item in Inventory.Instance.Items.Values) {
				if (item.name.Contains(filterInputField.text))
					activeItems.Add(item);
			}
			AtavismInventoryItem selectedItem = activeItems[index];
			cell.SetEntryDetails(ItemSelected, selectedItem.templateId, selectedItem.name, selectedItem.icon);
		} else if (chooseType == ChooseType.Skill) {
			List<Skill> activeSkills = new List<Skill>();
			foreach(Skill skill in Skills.Instance.SkillsList.Values) {
				if (skill.skillname.Contains(filterInputField.text))
					activeSkills.Add(skill);
			}
			Skill selectedSkill = activeSkills[index];
			cell.SetEntryDetails(SkillSelected, selectedSkill.id, selectedSkill.skillname, selectedSkill.icon);
		}
	}
	
	#endregion
	
	public static UGUIAdminPanel Instance {
		get {
			return instance;
		}
	}
}
