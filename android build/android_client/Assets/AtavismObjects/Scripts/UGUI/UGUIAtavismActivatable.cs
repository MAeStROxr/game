﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public enum ActivatableType {
	Item,
	Ability,
	Bag,
	Action,
	Other
}

public class UGUIAtavismActivatable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler {

	public Image cooldownImage;
	public Text countText;
	protected bool beingDragged = false;
	protected UGUIDraggableSlot releaseTarget = null;
	protected UGUIDraggableSlot source = null;
	protected Activatable activatableObject;
	protected ActivatableType activatableType;
	protected bool preventDiscard = false;
	protected UGUIAtavismActivatable link;
	
	void Start() {
		if (cooldownImage != null)
			AtavismEventSystem.RegisterEvent("COOLDOWN_UPDATE", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("COOLDOWN_UPDATE", this);
	}
	
	void OnEnable() {
		RunCooldownUpdate();
	}
	
	void OnDisable() {
		StopCoroutine(UpdateCooldown());
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "COOLDOWN_UPDATE") {
			// Update 
			RunCooldownUpdate();
		}
	}
	
	void RunCooldownUpdate() {
		// Check if this is on cooldown
		if (cooldownImage == null)
			return;
			
		cooldownImage.fillAmount = 0;
		
		if (activatableObject == null) 
			return;
			
		if (activatableObject.GetLongestActiveCooldown() == null)
			return;
		
		StartCoroutine(UpdateCooldown());
	}
	
	IEnumerator UpdateCooldown() {
		if (!cooldownImage || activatableObject == null)
			yield break;
			
		Cooldown c = activatableObject.GetLongestActiveCooldown();
		
		while (c != null && Time.time < c.expiration) {
			float total = c.expiration - (c.expiration-c.length);
			float currentTime = c.expiration - Time.time;
			cooldownImage.fillAmount = ((float)currentTime / (float)total);
			yield return new WaitForEndOfFrame();
		}
		
		cooldownImage.fillAmount = 0;
			
		yield return null;
	}

	public void OnBeginDrag(PointerEventData eventData) {
		if (link != null)
			return;
		beingDragged = true;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		transform.SetParent(GameObject.Find("Canvas").transform);
		AtavismCursor.Instance.UguiIconBeingDragged = true;
		UGUITooltip.Instance.Hide();
	}
	
	public void OnDrag(PointerEventData eventData) {
		if (link != null)
			return;
		this.transform.position = eventData.position;
	}
	
	/// <summary>
	/// Raises the end drag event. This is called after the OnDrop is run for the slot.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnEndDrag(PointerEventData eventData) {
		if (link != null && !beingDragged)
			return;
		Debug.Log("Got drag end");
		beingDragged = false;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		AtavismCursor.Instance.UguiIconBeingDragged = false;
		//this.transform.parent = source.transform;
		
		/*if (releaseTarget == null) {
			GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			source.Discarded();
			return;
		}*/
		
		// If the drop target is a reference slot and the source isn't then return a copy back to the source
		if (releaseTarget.SlotBehaviour == DraggableBehaviour.Reference && releaseTarget != source) {
			if (source.SlotBehaviour != DraggableBehaviour.Reference)
				this.transform.SetParent(source.transform, false);
			GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);	
			return;
		}
		
		// If the drop target is a temporarty slot and the source isn't then return a copy back to the source
		if (releaseTarget.SlotBehaviour == DraggableBehaviour.Temporary && releaseTarget != source) {
			if (source.SlotBehaviour != DraggableBehaviour.Temporary) {
				this.transform.SetParent(source.transform, false);
			} else {
				source = releaseTarget;
			}
			GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);	
			return;
		}
		
		this.transform.parent = releaseTarget.transform;
		if (releaseTarget != source) {
			source.ClearChildSlot();	
			GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);	
		} else {
			GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			if (preventDiscard) {
				preventDiscard = false;
				return;
			}
			source.Discarded();
		}
	}
	
	/*public virtual void OnClick() {
		if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
			if (activatableObject is AtavismInventoryItem && source.SlotBehaviour == DraggableBehaviour.Standard) {
				AtavismInventoryItem item = (AtavismInventoryItem)activatableObject;
				Inventory.Instance.CreateSplitStack(item, item.Count / 2);
			}
		}
		source.Activate();
	}*/
	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (ControlFreak2.CF2Input.GetKey(KeyCode.LeftShift) || ControlFreak2.CF2Input.GetKey(KeyCode.RightShift)) {
			if (activatableObject is AtavismInventoryItem && source.SlotBehaviour == DraggableBehaviour.Standard) {
				AtavismInventoryItem item = (AtavismInventoryItem)activatableObject;
				if (eventData.button == PointerEventData.InputButton.Right) {
					Inventory.Instance.CreateSplitStack(item, 1);
				} else {
					Inventory.Instance.CreateSplitStack(item, item.Count / 2);
				}
				
			}
		}
		source.Activate();
	}
	
	public void SetActivatable(Activatable obj, ActivatableType activatableType, UGUIDraggableSlot parent) {
		SetActivatable(obj, activatableType, parent, true);
	}
	
	public void SetActivatable(Activatable obj, ActivatableType activatableType, UGUIDraggableSlot parent, bool showCooldown) {
		if (beingDragged)
			return;
		this.activatableObject = obj;
		this.activatableType = activatableType;
		this.source = parent;
		this.releaseTarget = parent;
		this.GetComponent<Image>().sprite = obj.icon;
		if (this.countText != null && obj is AtavismInventoryItem) {
			AtavismInventoryItem item = (AtavismInventoryItem) obj;
			if (item.Count > 1) {
				countText.text = item.Count.ToString();
			} else {
				countText.text = "";
			}
		}
		
		if (showCooldown) {
			RunCooldownUpdate();
		} else if (cooldownImage != null) {
			cooldownImage.fillAmount = 0;
			StopCoroutine("UpdateCooldown");
		}
	}
	
	/// <summary>
	/// Tells the system to not run the Discard() function
	/// </summary>
	public void PreventDiscard() {
		this.preventDiscard = true;
	}
	
	public void SetDropTarget(UGUIDraggableSlot target) {
		releaseTarget = target;
	}
	
	public void SetLink(UGUIAtavismActivatable link) {
		this.link = link;
		if (link != null) {
			this.GetComponent<Button>().interactable = false;
		} else {
			this.GetComponent<Button>().interactable = true;
		}
	}
	
	public void ShowTooltip(GameObject target) {
		if (activatableObject is AtavismAbility) {
			AtavismAbility ability = (AtavismAbility) activatableObject;
			ability.ShowTooltip(target);
		} else if (activatableObject is AtavismInventoryItem) {
			AtavismInventoryItem item = (AtavismInventoryItem) activatableObject;
			item.ShowTooltip(target);
		}
	}
	
	public Activatable ActivatableObject {
		get {
			return activatableObject;
		}
	}
	
	public ActivatableType ActivatableType {
		get {
			return activatableType;
		}
		set {
			activatableType = value;
		}
	}
	
	public UGUIDraggableSlot Source {
		get {
			return source;
		}
	}
	
	public UGUIAtavismActivatable Link {
		get {
			return link;
		}
	}
}
