﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIBuildObject : MonoBehaviour {

	public Button button;
	public Text nameText;
	public Text requirementsText;
	AtavismBuildObjectTemplate template;

	// Use this for initialization
	void Start () {
	
	}
	
	public void UpdateBuildObjectInfo(AtavismBuildObjectTemplate template) {
		this.template = template;
		button.GetComponent<Image>().sprite = template.icon;
		nameText.text = template.buildObjectName;
		string requirements = "Requires: ";
		for (int i = 0; i < template.itemsReq.Count; i++) {
			AtavismInventoryItem item = Inventory.Instance.GetItemByTemplateID(template.itemsReq[i]);
			requirements += item.name + " x" + template.itemsReqCount[i] + " ";
		}
		requirementsText.text = requirements;
	}
	
	public void BuildObjectClicked() {
		WorldBuilder.Instance.StartPlaceBuildObject(template.id);
	}
}
