﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UGUIBuildObjectsList  : UIList<UGUIBuildObject> {
	
	void Start() {
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int numCells = WorldBuilder.Instance.GetBuildObjectsOfCategory(-1).Count;
		return numCells;
	}
	
	public override void UpdateCell (int index, UGUIBuildObject cell)
	{
		List<AtavismBuildObjectTemplate> templates = WorldBuilder.Instance.GetBuildObjectsOfCategory(-1);
		cell.UpdateBuildObjectInfo(templates[index]);
	}
	
	#endregion
}
