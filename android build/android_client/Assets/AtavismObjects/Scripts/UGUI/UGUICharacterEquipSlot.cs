﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUICharacterEquipSlot : UGUIDraggableSlot {

	public string slotName;
	Button button;
	//public Text countLabel;
	AtavismInventoryItem item;
	bool mouseEntered = false;
	
	void Start() {
		slotBehaviour = DraggableBehaviour.Standard;
	}
	
	/// <summary>
	/// Creates a UGUIAtavismActivatable object to put in this slot if the item is not null.
	/// </summary>
	/// <param name="item">Item.</param>
	public void UpdateEquipItemData(AtavismInventoryItem item) {
		this.item = item;
		if (item == null) {
			if (uguiActivatable != null) {
				Destroy (uguiActivatable.gameObject);
				if (mouseEntered)
					HideTooltip();
			}
		} else {
			if (uguiActivatable == null) {
				uguiActivatable = (UGUIAtavismActivatable) Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			}
			uguiActivatable.SetActivatable(item, ActivatableType.Item, this);
		}
	}

	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		// Apply logic here
		UGUIAtavismActivatable droppedActivatable = eventData.pointerDrag.GetComponent<UGUIAtavismActivatable>();
		
		// Reject any references, temporaries or non item slots
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference || 
		    droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Temporary ||
		    droppedActivatable.ActivatableType != ActivatableType.Item) {
			return;
		}
		//if (item == null && uguiItem == null) {
			// TODO: perhaps change this to store locally
		
			//uguiItem.SetDropTarget(this.transform);
		droppedActivatable.ActivatableObject.Activate();
		droppedActivatable.PreventDiscard();
		//}
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
	}

	public override void Activate() {
		if (item != null)
			item.Activate();
	}

	void ShowTooltip() {
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered) {
				if (uguiActivatable != null) {
					uguiActivatable.ShowTooltip(gameObject);
				} else {
					ShowTooltip();
				}
			} else {
				HideTooltip();
			}
		}
	}
}
