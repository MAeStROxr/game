﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

enum ChatWindowView {
	Chat,
	EventLog,
	CombatLog
}


public static class ColorTypeConverter
{
	public static string ToRGBHex(Color c)
	{
		return string.Format("#{0:X2}{1:X2}{2:X2}", ToByte(c.r), ToByte(c.g), ToByte(c.b));
	}
	
	private static byte ToByte(float f)
	{
		f = Mathf.Clamp01(f);
		return (byte)(f * 255);
	}
}

public class UGUIChatController : MonoBehaviour
{
	public ScrollRect scrollView;
	public Text textWindow;
	public InputField input;
	
	//Public Chat Colors
	public Color chatColor = Color.white;
	public Color eventColor = Color.yellow;
	public Color combatColor = Color.red;
	public Color announcementColor = Color.green;
	public Color whisperColor = Color.cyan;
	public Color groupColor = Color.magenta;
	public Color currentChannelCommandColor = Color.white;
	
	static ChatWindowView windowView = ChatWindowView.Chat;
	static string currentChannelCommand = "/say";
	static string placeholderText = "Say:";
	static List<string> chatMessages = new List<string>();
	static List<string> eventLog = new List<string>();
	static List<string> combatLog = new List<string>();
	
	void Start ()
	{
		AtavismEventSystem.RegisterEvent("CHAT_MSG_SERVER", this);
		AtavismEventSystem.RegisterEvent("CHAT_MSG_SAY", this);
		AtavismEventSystem.RegisterEvent("CHAT_MSG_SYSTEM", this);
		
		NetworkAPI.RegisterExtensionMessageHandler("announcement", HandleAnnouncementMessage);
		AtavismEventSystem.RegisterEvent("INVENTORY_EVENT", this);
		AtavismEventSystem.RegisterEvent("COMBAT_EVENT", this);
		
		// Load in existing channel text
		if (windowView == ChatWindowView.Chat) {
			ViewChat();
		} else if (windowView == ChatWindowView.CombatLog) {
			ViewCombatLog();
		} else if (windowView == ChatWindowView.EventLog) {
			ViewEventLog();
		}
	}
	
	void OnDestroy () {
		AtavismEventSystem.UnregisterEvent("CHAT_MSG_SERVER", this);
		AtavismEventSystem.UnregisterEvent("CHAT_MSG_SAY", this);
		AtavismEventSystem.UnregisterEvent("CHAT_MSG_SYSTEM", this);
		
		NetworkAPI.RemoveExtensionMessageHandler("announcement", HandleAnnouncementMessage);
		AtavismEventSystem.UnregisterEvent("INVENTORY_EVENT", this);
		AtavismEventSystem.UnregisterEvent("COMBAT_EVENT", this);
	}
	
	void Update() {
		if (ControlFreak2.CF2Input.GetKeyDown(KeyCode.Return))
		{
			if (!ClientAPI.UIHasFocus()) {
				// If no other inputs are selected, give focus to this input when enter is pressed.
				EventSystem.current.SetSelectedGameObject(input.gameObject);
			} else if (EventSystem.current.currentSelectedGameObject == input.gameObject) {
				// Clear the input from selection so keys work again for movement
				EventSystem.current.SetSelectedGameObject(null);
			}
		}
		
		if (input != null && input.placeholder.GetComponent<Text>() != null)
			input.placeholder.GetComponent<Text>().text = placeholderText;
	}
	
	public void OnSubmit (string message)
	{
		string text = message; //NGUIText.StripSymbols(mInput.value);
		
		if (!string.IsNullOrEmpty(text))
		{
			// check if it has a / at the front and if it is a new channel option
			if (text.StartsWith("/say ") || text.StartsWith("/s ")) {
				currentChannelCommand = "/say";
				placeholderText = "Say:";
				currentChannelCommandColor = chatColor;
			} else if (text.StartsWith("/group ") || text.StartsWith("/g ")) {
				currentChannelCommand = "/group";
				placeholderText = "Group:";
				currentChannelCommandColor = chatColor;
			} else if (text.StartsWith("/whisper ") || text.StartsWith("/w ")) {
				string[] splitMessage = message.Split(' ');
				currentChannelCommand = "/whisper " + splitMessage[1];
				placeholderText = "Whisper " + splitMessage[1] + ":";
				
				//var len = currentChannelCommand.Length;
				//currentChannelCommandColor = whisperColor;
				//text = currentChannelCommand + " " + GetLine(text.Substring(len, text.Length - len), whisperColor);
				
			} else if (!text.StartsWith("/")) {
				text = currentChannelCommand + " " + text;// GetLine(text, currentChannelCommandColor);
			}
			
			
			// Send to the Atavism Command class
			AtavismCommand.HandleCommand(text); 
			input.text = "";
			//mInput.isSelected = false;
		}
	}
	
	// This method to be called when remote chat message is received
	void AddChatMessage(string message) {
		bool scrollToBottom = false;
		if (scrollView.verticalScrollbar.value == 0f)
			scrollToBottom = true;
		
		var line = GetLine(message, chatColor);
		chatMessages.Add(line);
		if (windowView == ChatWindowView.Chat)
		{
			textWindow.text += "\n" + line;
		}
		
		if (scrollToBottom) {
			scrollView.velocity = new Vector2(0, 1000f);
		}
	}    
	
	// This method to be called when remote chat message is received
	void AddCombatMessage(string message) {
		bool scrollToBottom = false;
		if (scrollView.verticalScrollbar.value == 0f)
			scrollToBottom = true;
		
		var line = GetLine(message, combatColor);
		combatLog.Add(line);
		if (windowView == ChatWindowView.CombatLog)
		{
			textWindow.text += "\n" + line;
		}
		
		if (scrollToBottom) {
			scrollView.velocity = new Vector2(0, 1000f);
		}
	}
	
	public void HandleAnnouncementMessage(Dictionary<string, object> props) {
		bool scrollToBottom = false;
		if (scrollView.verticalScrollbar.value == 0f)
			scrollToBottom = true;
		
		string message = (string) props["AnnouncementText"];
		var line = GetLine(message, eventColor);
		eventLog.Add(message);
		if (windowView == ChatWindowView.EventLog)
		{
			textWindow.text += "\n" + line;
		}
		
		if (scrollToBottom) {
			scrollView.velocity = new Vector2(0, 1000f);
		}
	}
	
	void ProcessInventoryEvent(string[] eventArgs) {
		if (eventArgs[0] == "ItemHarvested") {
			string[] args = new string[1];
			AtavismInventoryItem item = ClientAPI.ScriptObject.GetComponent<Inventory>().
				GetItemByTemplateID(int.Parse(eventArgs[1]));
			
			string message = "Received " + item.name + " x" + eventArgs[2];
			eventLog.Add(message);
			if (windowView == ChatWindowView.EventLog)
				textWindow.text = "\n" + message;
			textWindow.color = announcementColor;
		} else if (eventArgs[0] == "ItemLooted") {
			string[] args = new string[1];
			AtavismInventoryItem item = ClientAPI.ScriptObject.GetComponent<Inventory>().
				GetItemByTemplateID(int.Parse(eventArgs[1]));
			
			string line = GetLine("Received " + item.name + " x" + eventArgs[2], announcementColor);
			eventLog.Add(line);
			if (windowView == ChatWindowView.EventLog)
				textWindow.text = "\n" + line;
		}
	}
	
	void ProcessCombatEvent(string[] eventArgs) {
		string message = "";
		if (eventArgs[0] == "CombatPhysicalDamage") {
			string casterName = GetObjectName(eventArgs[1]);
			string targetName = GetObjectName(eventArgs[2]);
			message = casterName + " hit " + targetName + " for " + eventArgs[3] + " damage.";
		} else if (eventArgs[0] == "CombatMagicalDamage") {
			string casterName = GetObjectName(eventArgs[1]);
			string targetName = GetObjectName(eventArgs[2]);
			message = casterName + " hit " + targetName + " for " + eventArgs[3] + " magical damage.";
		} else if (eventArgs[0] == "CombatBuffGained") {
			string casterName = GetObjectName(eventArgs[1]);
			string targetName = GetObjectName(eventArgs[2]);
			message = casterName + " gained " + eventArgs[3] + ".";
		} else if (eventArgs[0] == "CombatBuffLost") {
			string casterName = GetObjectName(eventArgs[1]);
			string targetName = GetObjectName(eventArgs[2]);
			string effectName = "effect";
			message = casterName + " lost " + eventArgs[3] + ".";
		}
		
		AddCombatMessage(message);
	}
	
	string GetObjectName(string oid) { 
		if (OID.fromString(oid).ToLong() == ClientAPI.GetPlayerOid()) {
			return "You";
		} else {
			AtavismObjectNode caster = ClientAPI.GetObjectNode(OID.fromString(oid).ToLong());
			if (caster != null)
				return caster.Name;
		}
		return "";
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CHAT_MSG_SERVER") {
			AddChatMessage(eData.eventArgs[0]);
		} else if (eData.eventType == "CHAT_MSG_SAY") {
			AtavismLogger.LogDebugMessage("Got chat say event with numargs: " + eData.eventArgs.Length);
			HandleChatMessage(eData.eventArgs); //Process chat message events
		} else if (eData.eventType == "CHAT_MSG_SYSTEM") {
			AtavismLogger.LogDebugMessage("Got system event with numargs: " + eData.eventArgs.Length);
			AddChatMessage(GetLine("[Event]: " + eData.eventArgs[0], announcementColor)); //System Event
		} else if (eData.eventType == "INVENTORY_EVENT") {
			AtavismLogger.LogDebugMessage("Got inventory event with numargs: " + eData.eventArgs.Length);
			// Process the message
			ProcessInventoryEvent(eData.eventArgs); //ProcessInventoryEvent
		} else if (eData.eventType == "COMBAT_EVENT") {
			AtavismLogger.LogDebugMessage("Got combat event with numargs: " + eData.eventArgs.Length);
			// Process the message
			ProcessCombatEvent(eData.eventArgs); //ProcessCombatEvent
		}
	}
	
	void HandleChatMessage(string[] args) {
		//Debug.Log("oid of chat sender: " + args[3]);
		if (args[2] == "4") {
			AddChatMessage(GetLine("[Group]: " + args[0], groupColor)); //Group Message
		}
		else if(args[2] == "1") {
			
			AddChatMessage(GetLine("[" + args[1] + "]: " + args[0], chatColor)); //Chat Message
		}
		else if(args[2] == "6")
		{
			AddChatMessage(GetLine("[" + args[1] + "]: " + args[0], whisperColor)); //Whisper Message
		}
	}
	
	public void StartWhisper(string targetName) { //TODO This is the whisper function I was talking about as well
		EventSystem.current.SetSelectedGameObject (input.gameObject);
		input.Select();
		input.ActivateInputField();
		currentChannelCommand = "/whisper " + targetName;
		placeholderText = "Whisper " + targetName + ":";
	}
	
	public void ViewChat() {
		windowView = ChatWindowView.Chat;
		textWindow.text = "";
		foreach (string message in chatMessages) {
			textWindow.text += "\n" + message;
		}
		scrollView.velocity = new Vector2(0, 1000f);
	}
	
	public void ViewEventLog() {
		windowView = ChatWindowView.EventLog;
		textWindow.text = "";
		foreach (string message in eventLog) {
			textWindow.text += "\n" + message;
		}
		scrollView.velocity = new Vector2(0, 1000f);
	}
	
	public void ViewCombatLog() {
		windowView = ChatWindowView.CombatLog;
		textWindow.text = "";
		foreach (string message in combatLog) {
			textWindow.text += "\n" + message;
		}
		scrollView.velocity = new Vector2(0, 1000f);
	}
	
	string GetLine(string message, Color color)
	{
		return "<color=" + ColorTypeConverter.ToRGBHex(color) + ">" + message + "</color>";
	}
}
