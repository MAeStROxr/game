﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUICurrency : MonoBehaviour {

	public Image image;
	public Text amountText;

	// Use this for initialization
	void Start () {
	
	}
	
	public void UpdateCurrency(Currency c) {
		image.sprite = c.icon;
		amountText.text = c.Current.ToString();
	}
	
	public void SetCurrencyDisplayData(CurrencyDisplay display) {
		amountText.text = display.amount.ToString();
		image.sprite = display.icon;
	}
}
