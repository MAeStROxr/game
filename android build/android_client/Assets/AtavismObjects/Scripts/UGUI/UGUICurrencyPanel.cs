﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUICurrencyPanel : MonoBehaviour {

	public List<UGUICurrency> currencies;

	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("CURRENCY_UPDATE", this);
		UpdateCurrencies();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("CURRENCY_UPDATE", this);
	}
	
	void OnEnable() {
		UpdateCurrencies();
	}
	
	public void OnEvent(AtavismEventData eData)
	{
		if (!enabled)
			return;
		
		if (eData.eventType == "CURRENCY_UPDATE")
		{
			UpdateCurrencies();
		}
	}
	
	void UpdateCurrencies() {
		List<Currency> mainCurrencies = Inventory.Instance.GetMainCurrencies();
		
		for (int i = 0; i < currencies.Count; i++) {
			if (i < Inventory.Instance.GetMainCurrencies().Count) {
				currencies[i].gameObject.SetActive(true);
				currencies[i].UpdateCurrency(Inventory.Instance.GetMainCurrency(Inventory.Instance.GetMainCurrencies().Count - i - 1));
			} else {
				currencies[i].gameObject.SetActive(false);
			}
		}
	}
}
