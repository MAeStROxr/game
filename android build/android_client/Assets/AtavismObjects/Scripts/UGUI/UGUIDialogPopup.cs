﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIDialogPopup : MonoBehaviour {

	public Text textElement;
	public Button okButton;
	public Button yesButton;
	public Button noButton;

	// Use this for initialization
	void Start () {
	
	}
	
	public void ShowDialogPopup(string text, bool showButton) {
		textElement.text = text;
		okButton.gameObject.SetActive(showButton);
		if (yesButton != null)
			yesButton.gameObject.SetActive(false);
		if (noButton != null)
			noButton.gameObject.SetActive(false);
		if (showButton) {
			EventSystem.current.SetSelectedGameObject(okButton.gameObject);
		}
	}
	
	public void ShowDialogOptionPopup(string text) {
		textElement.text = text;
		okButton.gameObject.SetActive(false);
		yesButton.gameObject.SetActive(true);
		noButton.gameObject.SetActive(true);
	}
}
