﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public delegate void DialogueButtonFunction();

public class UGUIDialogueBarButton : MonoBehaviour {

	public Text buttonText;
	DialogueButtonFunction buttonFunction;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SetButtonFunction(DialogueButtonFunction function, string text) {
		this.buttonFunction = function;
		this.buttonText.text = text;
	}
	
	public void DialogueButtonClicked() {
		if (buttonFunction != null)
			buttonFunction();
	}
}
