﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UGUIEffectsPanel : MonoBehaviour {

	public List<UGUIEffect> effectButtons;

	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("EFFECT_UPDATE", this);
		UpdateEffectButtons();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("EFFECT_UPDATE", this);
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "EFFECT_UPDATE") {
			// Update 
			UpdateEffectButtons();
		}
	}
	
	public void UpdateEffectButtons() {
		for (int i = 0; i < effectButtons.Count; i++) {
			if (i < Abilities.Instance.PlayerEffects.Count) {
				effectButtons[i].gameObject.SetActive(true);
				effectButtons[i].SetEffect(Abilities.Instance.PlayerEffects[i], i);
			} else {
				effectButtons[i].gameObject.SetActive(false);
			}
		}
	}
}
