﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIFloatingMobPanel : MonoBehaviour {
	
	public Text nameText;
	public Text levelText;
	public Text combatText;  //TODO Create an object pool of "Text" so more than one combat event can be seen.
	public RectTransform chatPanel;
	public Text chatText;
	public bool usingWorldSpaceCanvas = true;
	public bool faceCamera = true;
    public int targetType;
	public float renderDistance = 50f;
	public float chatDisplayTime = 3f;
    public Button startBattleButton;
	public Color friendlyNameColour = Color.green;
	public Color neutralNameColour = Color.yellow;
	public Color enemyNameColour = Color.red;
	public Color myDamageColour = Color.red;
	public Color targetDamageColour = Color.white;
	public Color myMessageColour = Color.yellow;
	public Color targetMessageColour = Color.yellow;
	AtavismObjectNode mobNode;
	float stopDisplay;
	float stopChatDisplay;
	
	float combatDisplayTime = 3f;
	
	private float initialScaleFactor = 3f;
	public float initialAlphaFactor = 3f;
	public float combatTextSpeed = 50f;
	public float combatColorSpeed = 1f;
	public float combatTextScaleDownSpeed = 3f;
	
	//Set automatically
	Vector3 startPosition;
	Vector3 startScale;
	float currentScale = 1f;
	float currentAlpha = 1f;
	float currentOffset = 0;
	
	
	// Use this for initialization
	void Awake () {
		this.startPosition = combatText.rectTransform.localPosition;
		this.startScale = combatText.rectTransform.localScale;
		this.currentScale = initialScaleFactor;
		this.currentAlpha = initialAlphaFactor;

	}
	
	void OnDestroy()
	{
		mobNode.RemovePropertyChangeHandler("level", LevelHandler);
	}
	
	// Update is called once per frame
	public void RunUpdate () {
        
		//GUI.color = new Color (1.0f, 1.0f, 1.0f, 1.0f - (cameraDistance - fadeDistance) / (hideDistance - fadeDistance));
		Vector3 worldPos = new Vector3(mobNode.Position.x, mobNode.Position.y + mobNode.MobController.nameHeight, mobNode.Position.z);
		if (!usingWorldSpaceCanvas) {
			//GetComponent<CanvasGroup>().alpha = 1f;
			Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
			transform.position = new Vector3(screenPos.x, screenPos.y, screenPos.z);
		} else {
			transform.position = worldPos;
			if (faceCamera) {
				transform.rotation = Camera.main.transform.rotation;
			} else {
				Quaternion cameraRotation = Camera.main.transform.rotation;
				cameraRotation = Quaternion.Euler(0, cameraRotation.eulerAngles.y, 0);
				transform.rotation = cameraRotation;
			}
			
		}
		
		UpdateCombatText();
        UpdateBattleButton();
		if (Time.time > stopChatDisplay) {
			HideChatBubble();
		}
	}
    GameMainframe instance = GameMainframe.GetInstance();
    void UpdateBattleButton() {
        if (((AtavismMobNode)mobNode).GameObject.layer!=24 || targetType > 0 || instance.clientOID==mobNode.Oid || instance.inBattle) {
            startBattleButton.gameObject.SetActive(false);
            return;
        }
        float distance = Vector3.Distance(mobNode.Position, ClientAPI.GetPlayerObject().Position);
        if (4.0 < distance && distance < 16.0) {
            startBattleButton.gameObject.SetActive(true);
        }
    }

    public void StartBattle() {
        if (GameMainframe.GetInstance().inBattle) return;
        StartCoroutine(StartBattleCooldown());
        BattleManager.GetInstance().StartBattle((AtavismMobNode)mobNode);
    }
	IEnumerator StartBattleCooldown() {
        Color col = startBattleButton.image.color;
        startBattleButton.image.color = Color.gray;
        startBattleButton.interactable = false;
        yield return new WaitForSeconds(5.0f);
        startBattleButton.image.color = col;
        startBattleButton.interactable = true;
    }
	void ResetCombatText()
	{
		currentOffset = 0;
		currentAlpha = initialAlphaFactor;
		combatText.text = "";
		currentScale = initialScaleFactor;
		combatText.rectTransform.localPosition = startPosition;
		combatText.rectTransform.localScale = startScale;
		
		combatText.rectTransform.localPosition = startPosition + new Vector3(0, currentOffset, 0);
		
		var color = combatText.color;
		color.a = currentAlpha;
		combatText.color = color;
		
		combatText.rectTransform.localScale = startScale * currentScale;
	}
	
	void UpdateCombatText()
	{
		if (Time.time > stopDisplay)
		{
			combatText.text = "";
			return;
		}
		
		//Update the current position.
		currentOffset += Time.deltaTime * combatTextSpeed;
		combatText.rectTransform.localPosition = startPosition + new Vector3(0, currentOffset, 0);
		
		//Update the current alpha channel.
		currentAlpha = Mathf.Max(0, currentAlpha - Time.deltaTime * combatColorSpeed);
		var color = combatText.color;
		color.a = currentAlpha;
		combatText.color = color;
		
		//Update the current scale.
		currentScale = Mathf.Max(1f, currentScale - Time.deltaTime * combatTextScaleDownSpeed);
		combatText.rectTransform.localScale = startScale * currentScale;
	}
	
	public void SetMobDetails(AtavismObjectNode mobNode, bool showName) {
		this.mobNode = mobNode;
		
		if (showName)
		{
			mobNode.RegisterPropertyChangeHandler("level", LevelHandler);
			mobNode.RegisterPropertyChangeHandler("reaction", TargetTypeHandler);
			mobNode.RegisterPropertyChangeHandler("adminLevel", AdminLevelHandler);
		}
		UpdateNameDisplay(showName);
		
	}
	
	void UpdateNameDisplay(bool showName)
	{
		if (showName)
		{
			if (levelText != null && mobNode.PropertyExists("level"))
			{
				int mobLevel = (int)mobNode.GetProperty("level");
				Color textColor = levelText.color;
				if (ClientAPI.GetPlayerObject().PropertyExists("level"))
				{
					int playerLevel = (int)ClientAPI.GetPlayerObject().GetProperty("level");
					if (mobLevel - playerLevel > 5)
					{
						textColor = Color.red;
					}
					else if (playerLevel - mobLevel > 5)
					{
						textColor = Color.green;
					}
				}
				
				levelText.color = textColor;
				nameText.text = mobNode.Name; 
				levelText.text = "[" + "Level" + " " + mobLevel + "]"; 
			}
			else
			{
				nameText.text = mobNode.Name;
				if (levelText != null)
					levelText.text = "";
			}
			// Set name colour based on target type
			nameText.color = neutralNameColour;
			if (mobNode.PropertyExists("reaction")) {
				targetType = (int)mobNode.GetProperty("reaction");
				if (targetType < 0)
					nameText.color = enemyNameColour;
				else if (targetType > 0)
					nameText.color = friendlyNameColour;
			}
		}
		else
		{
			nameText.text = "";
			if (levelText != null)
				levelText.text = "";
		}
		
		// Show admin Icon?
		/*if (mobNode != null && mobNode.PropertyExists("adminLevel")) {
			int adminLevel = (int)mobNode.GetProperty("adminLevel");
			adminIcon.gameObject.SetActive(adminLevel == 5);
		} else {
			adminIcon.gameObject.SetActive(false);
		}*/
	}
	
	public void ShowCombatText(string message, string eventType) {
		
		ResetCombatText();
		combatText.text = message;
		stopDisplay = Time.time + combatDisplayTime;
		
		// Change colour based on eventType
		if (eventType == "CombatPhysicalDamage" || eventType == "CombatMagicalDamage") {
			if (mobNode is AtavismPlayer) {
				combatText.color = myDamageColour;
			} else {
				combatText.color = targetDamageColour;
			}
		} else if (eventType == "CombatExpGained") {
			combatText.text = "Exp: " + message;
		} else {
			if (mobNode is AtavismPlayer) {
				combatText.color = myMessageColour;
			} else {
				combatText.color = targetMessageColour;
			}
		}
	}
	
	public void LevelHandler(object sender, PropertyChangeEventArgs args)
	{
		UpdateNameDisplay(true);
	}
	
	public void TargetTypeHandler(object sender, PropertyChangeEventArgs args)
	{
		UpdateNameDisplay(true);
	}
	
	public void AdminLevelHandler(object sender, PropertyChangeEventArgs args)
	{
		UpdateNameDisplay(true);
	}
	
	public void ShowChatBubble(string text) {
		if (chatPanel != null)
			chatPanel.gameObject.SetActive(true);
		if (chatText != null) {
			int numLines = text.Length / 60;
			for (int i = 0; i < numLines; i++) {
				int spacePos = text.IndexOf(" ", (i+1)*60);
				if (spacePos > 0)
					text = text.Insert(spacePos, "\n");
			}
			chatText.text = text;
			stopChatDisplay = Time.time + chatDisplayTime;
		}
	}
	
	public void HideChatBubble() {
		if (chatText != null) {
			chatText.text = "";
		}
		if (chatPanel != null)
			chatPanel.gameObject.SetActive(false);
	}

  
}
