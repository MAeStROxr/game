﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UGUIFloatingMobPanelController : MonoBehaviour {

	public UGUIFloatingMobPanel panelPrefab;
	public bool showCombatText = true;
	public bool showFloatingChatBubbles = true;
	public bool showPlayerName = false;
	Dictionary<AtavismObjectNode, UGUIFloatingMobPanel> activePanels = new Dictionary<AtavismObjectNode, UGUIFloatingMobPanel>();

	// Use this for initialization
	void Start () {
		ClientAPI.WorldManager.ObjectAdded += HandleObjectAdded;
		ClientAPI.WorldManager.ObjectRemoved += HandleObjectRemoved;
		
		if (showCombatText)
			AtavismEventSystem.RegisterEvent("COMBAT_EVENT", this);
		if (showFloatingChatBubbles)
			AtavismEventSystem.RegisterEvent("CHAT_MSG_SAY", this);
			
		// Create panel for player
		UGUIFloatingMobPanel mobPanel = (UGUIFloatingMobPanel) Instantiate(panelPrefab);
		mobPanel.transform.SetParent(this.transform, false);
		
		mobPanel.SetMobDetails(ClientAPI.GetPlayerObject(), showPlayerName);
		activePanels.Add(ClientAPI.GetPlayerObject(), mobPanel);
	}
	
	void Update() {
		// Run through all panels
		foreach (AtavismObjectNode mobNode in activePanels.Keys) {
			UGUIFloatingMobPanel floatingMobPanel = activePanels[mobNode];
			
			float distance = Vector3.Distance(mobNode.Position, Camera.main.transform.position);
			
			if (distance > floatingMobPanel.renderDistance) {
				if (floatingMobPanel.gameObject.activeSelf) {
					floatingMobPanel.gameObject.SetActive(false);
				}
			} else {
				if (!mobNode.GameObject.GetComponentInChildren<Renderer>().isVisible) {
					bool rendererVisible = false;
					foreach(Renderer renderer in mobNode.GameObject.GetComponentsInChildren<Renderer>())
					{
						if (renderer.isVisible)
						{
							rendererVisible = true;
							break;
						}
					}
					if (!rendererVisible) {
						if (floatingMobPanel.gameObject.activeSelf) {
							floatingMobPanel.gameObject.SetActive(false);
						}
						continue;
					}
				}
				if (!floatingMobPanel.gameObject.activeSelf) {
					floatingMobPanel.gameObject.SetActive(true);
				}
				floatingMobPanel.RunUpdate();
			}
		}
	}
	
	void OnDestroy() {
		ClientAPI.WorldManager.ObjectAdded -= HandleObjectAdded;
		ClientAPI.WorldManager.ObjectRemoved -= HandleObjectRemoved;
		
		if (showCombatText)
			AtavismEventSystem.UnregisterEvent("COMBAT_EVENT", this);
		if (showFloatingChatBubbles)
			AtavismEventSystem.UnregisterEvent("CHAT_MSG_SAY", this);
	}
	
	void HandleObjectAdded (object sender, AtavismObjectNode objNode)
	{
		//if (objNode.GameObject.GetComponentInChildren<Renderer>() == null)
		//	return;
			
		// Check if the object has a mobcontroller - if not, don't display a name
		if ( objNode.MobController == null || activePanels.ContainsKey(objNode))
			return;
		
		UGUIFloatingMobPanel mobPanel = (UGUIFloatingMobPanel) Instantiate(panelPrefab);
		mobPanel.transform.SetParent(this.transform, false);
		//mobPanel.GetComponent<CanvasGroup>().alpha = 0f;
		
		mobPanel.SetMobDetails(objNode, true);
		activePanels.Add(objNode, mobPanel);
	}
	
	void HandleObjectRemoved (object sender, AtavismObjectNode objNode)
	{
		if (activePanels.ContainsKey(objNode)) {
			Destroy (activePanels[objNode].gameObject);
			activePanels.Remove(objNode);
		}
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "COMBAT_EVENT") {
			OID target = OID.fromString(eData.eventArgs[2]);
			AtavismObjectNode targetNode = ClientAPI.GetObjectNode(target.ToLong());
			if (activePanels.ContainsKey(targetNode)) {
				activePanels[targetNode].ShowCombatText(eData.eventArgs[3], eData.eventArgs[0]);
			}
		} else if (eData.eventType == "CHAT_MSG_SAY") {
			AtavismLogger.LogDebugMessage("Got chat say event with numargs: " + eData.eventArgs.Length);
			string senderOid = eData.eventArgs[3];
			AtavismObjectNode node = ClientAPI.GetObjectNode(OID.fromString(senderOid).ToLong());
			if (node != null) {
				activePanels[node].ShowChatBubble(eData.eventArgs[0]);
			}
		}
	}
}
