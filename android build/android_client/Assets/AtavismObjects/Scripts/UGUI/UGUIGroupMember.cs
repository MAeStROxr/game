﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIGroupMember : MonoBehaviour, IPointerClickHandler {

	public Text name;
	public Text levelText;
	public Image portrait;
	public Slider healthBar;
	public Text healthText;
	public Slider manaBar;
	public Text manaText;
	public RectTransform popupMenu;
	public Image leaderIcon;
	GroupMember member;
	string characterName;
	string gender;
	int classID = -1;
	string level;
	
	// Use this for initialization
	void Awake () {
		if (leaderIcon != null)
			leaderIcon.gameObject.SetActive(false);
	}
	
	public void UpdateGroupMember(GroupMember member) {
		this.member = member;
		name.text = member.name;
		if (levelText != null) {
			if (member.properties.ContainsKey("level")) {
				levelText.text = member.properties["level"].ToString();
			}
		}
		
		/*if (classID != null && gender != null && portrait != null) {
			string spriteName = "ClassPortrait_" + classID + "_" + gender.ToLower();
			Sprite portraitSprite = Resources.Load<Sprite>("Portraits/" + spriteName);
			portrait.sprite = portraitSprite;
		}*/
		
		if (member.properties.ContainsKey("health") && member.properties.ContainsKey("health-max")) {
			if (healthBar != null) {
				int health = (int)member.properties["health"];
				int maxHealth = (int)member.properties["health-max"];
				healthBar.value = (float)health / (float)maxHealth;
			}
			if (healthText != null) {
				healthText.text = member.properties["health"].ToString() + " / " + member.properties["health-max"].ToString();
			}
		}
		if (member.properties.ContainsKey("mana") && member.properties.ContainsKey("mana-max")) {
			if (manaBar != null) {
				int mana = (int)member.properties["mana"];
				int maxMana = (int)member.properties["mana-max"];
				manaBar.value = (float)mana / (float)maxMana;
			}
			if (manaText != null) {
				manaText.text = member.properties["mana"].ToString() + " / " + member.properties["mana-max"].ToString();
			}
		}
		
		if (portrait != null) {
			if (ClientAPI.GetObjectNode(member.oid.ToLong()) != null && ClientAPI.GetObjectNode(member.oid.ToLong()).GameObject != null) {
				Sprite portraitSprite = PortraitManager.Instance.LoadPortrait(ClientAPI.GetObjectNode(member.oid.ToLong()).GameObject.GetComponent<AtavismNode>());
				if (portraitSprite != null)
					portrait.sprite = portraitSprite;
			}
		}
		
		if (leaderIcon != null) {
			if (AtavismGroup.Instance.LeaderOid == member.oid) {
				leaderIcon.gameObject.SetActive(true);
			} else {
				leaderIcon.gameObject.SetActive(false);
			}
		}
	}
	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Right)
			return;
		
		if (popupMenu.gameObject.activeSelf) {
			popupMenu.gameObject.SetActive(false);
			return;
		}
		
		// Verify the player is group leader
		if (AtavismGroup.Instance.LeaderOid.ToLong() != ClientAPI.GetPlayerOid()) 
			return;
		
		// Work out what to put in the popup menu here
		popupMenu.gameObject.SetActive(true);
	}
	
	public void PromoteToLeader() {
		AtavismGroup.Instance.PromoteToLeader(member.oid);
		popupMenu.gameObject.SetActive(false);
	}
	
	public void Kick() {
		AtavismGroup.Instance.RemoveGroupMember(member.oid);
		popupMenu.gameObject.SetActive(false);
	}
}
