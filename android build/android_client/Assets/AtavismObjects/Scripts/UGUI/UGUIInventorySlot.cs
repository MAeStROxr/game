﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIInventorySlot : UGUIDraggableSlot {

	public int bagNum;
	public Text countLabel;
	AtavismInventoryItem item;
	bool mouseEntered = false;

	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.Standard;
	}
	
	/// <summary>
	/// Creates a UGUIAtavismActivatable object to put in this slot if the item is not null.
	/// </summary>
	/// <param name="item">Item.</param>
	public void UpdateInventoryItemData(AtavismInventoryItem item) {
		this.item = item;
		if (item == null) {
			if (uguiActivatable != null) {
				Destroy (uguiActivatable.gameObject);
				if (mouseEntered)
					HideTooltip();
			}
		} else {
			if (uguiActivatable == null) {
				uguiActivatable = (UGUIAtavismActivatable) Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			}
			uguiActivatable.SetActivatable(item, ActivatableType.Item, this);
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		UGUIAtavismActivatable droppedActivatable = eventData.pointerDrag.GetComponent<UGUIAtavismActivatable>();
		
		// Don't allow reference or temporary slots, or non Item/bag slots
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference || 
		    droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Temporary || droppedActivatable.Link != null || 
		    (droppedActivatable.ActivatableType != ActivatableType.Item && droppedActivatable.ActivatableType != ActivatableType.Bag)) {
		    return;
		}
		
		if (item == null && uguiActivatable == null) {
			// If this was a drag from a reference, do nothing
			if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference) {
				return;
			} else if (droppedActivatable.ActivatableType == ActivatableType.Bag) {
				// If it is a bag, send the place bag message
				Inventory.Instance.PlaceBagAsItem(droppedActivatable.Source.slotNum, bagNum, slotNum);
				return;
			}
			this.uguiActivatable = droppedActivatable;
			uguiActivatable.SetDropTarget(this);
			AtavismInventoryItem newItem = (AtavismInventoryItem)uguiActivatable.ActivatableObject;
			Inventory.Instance.PlaceItemInBag(bagNum, slotNum, newItem, newItem.Count);
		} else {
			if (droppedActivatable.Source == this) {
				droppedActivatable.PreventDiscard();
				return;
			}
			// Check if the source is the same type of item
			if (item != null && droppedActivatable.ActivatableType == ActivatableType.Item) {
				AtavismInventoryItem newItem = (AtavismInventoryItem)droppedActivatable.ActivatableObject;
				if (item.templateId == newItem.templateId) {
					Inventory.Instance.PlaceItemInBag(bagNum, slotNum, newItem, newItem.Count);
					droppedActivatable.PreventDiscard();
				} else {
					// Send move item with swap
					Inventory.Instance.PlaceItemInBag(bagNum, slotNum, newItem, newItem.Count, true);
					droppedActivatable.PreventDiscard();
				}
			}
		}
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
	}
	
	public override void Discarded() {
		UGUIConfirmationPanel.Instance.ShowConfirmationBox("Delete " + item.name + "?", item, Inventory.Instance.DeleteItemStack);
	}

	public override void Activate() {
		if (item == null)
			return;
		if (!AtavismCursor.Instance.HandleUGUIActivatableUseOverride(uguiActivatable)) {
			item.Activate();
		}
	}

	void ShowTooltip() {
	}

	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}

	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && uguiActivatable != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
