﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class UGUILootListEntry : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text itemNameText;
	public Image itemIcon;
	public Text countText;
	AtavismInventoryItem item;

	// Use this for initialization
	void Start () {
	
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void LootEntryClicked() {
		NetworkAPI.SendTargetedCommand(Inventory.Instance.LootTarget.ToLong(), "/lootItem " + item.ItemId);
	}
	
	public void SetLootEntryDetails(AtavismInventoryItem item) {
		this.item = item;
		this.itemNameText.text = item.name;
		this.itemIcon.sprite = item.icon;
		if (countText != null) {
			if (item.Count > 1) {
				this.countText.text = item.Count.ToString();
			} else {
				this.countText.text = "";
			}
		}
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		set {
			if (value) {
				item.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
