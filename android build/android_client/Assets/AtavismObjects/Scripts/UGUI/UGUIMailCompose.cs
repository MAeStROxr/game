﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIMailCompose : MonoBehaviour {

	public InputField toText;
	public InputField subjectText;
	public InputField messageText;
	public List<UGUIMailAttachmentSlot> attachmentSlots;
	public List<InputField> currencyFields;
	public List<Image> currencyIcons;

	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("CLOSE_MAIL_WINDOW", this);
		AtavismEventSystem.RegisterEvent("MAIL_SENT", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("CLOSE_MAIL_WINDOW", this);
		AtavismEventSystem.UnregisterEvent("MAIL_SENT", this);
	}
	
	void OnEnable() {
		StartNewMail();
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CLOSE_MAIL_WINDOW") {
			gameObject.SetActive(false);
		} else if (eData.eventType == "MAIL_SENT") {
			gameObject.SetActive(false);
		}
	}
	
	public void StartNewMail() {
		Mailing.Instance.StartComposingMail();
		toText.text = "";
		subjectText.text = "";
		messageText.text = "";
		
		for (int i = 0; i < currencyFields.Count; i++) {
			if (i < Mailing.Instance.MailBeingComposed.currencies.Count) {
				currencyFields[i].gameObject.SetActive(true);
				currencyFields[i].text = "0";
				currencyIcons[i].gameObject.SetActive(true);
				currencyIcons[i].sprite = Inventory.Instance.GetMainCurrency(i).icon;
			} else {
				currencyFields[i].gameObject.SetActive(false);
				currencyIcons[i].gameObject.SetActive(false);
			}
		}
	}
	
	public void StartReplyMail(string to, string subject) {
		StartNewMail();
		toText.text = to;
		subjectText.text = "Re: " + subject;
	}
	
	public void SetMailTo(string to) {
		Mailing.Instance.MailBeingComposed.senderName = to;
	}
	
	public void SetSubject(string subject) {
		Mailing.Instance.MailBeingComposed.subject = subject;
	}
	
	public void SetMessage(string message) {
		Mailing.Instance.MailBeingComposed.message = message;
	}
	
	/// <summary>
	/// Updates the currency amount for the first "main" currency
	/// </summary>
	/// <param name="currencyAmount">Currency amount.</param>
	public void SetCurrency1(string currencyAmount) {
		Mailing.Instance.SetMailCurrencyAmount(0, int.Parse(currencyAmount));
		checkCurrency();
	}
	
	/// <summary>
	/// Updates the currency amount for the first "main" currency
	/// </summary>
	/// <param name="currencyAmount">Currency amount.</param>
	public void SetCurrency2(string currencyAmount) {
		Mailing.Instance.SetMailCurrencyAmount(1, int.Parse(currencyAmount));
		checkCurrency();
	}
	
	/// <summary>
	/// Updates the currency amount for the first "main" currency
	/// </summary>
	/// <param name="currencyAmount">Currency amount.</param>
	public void SetCurrency3(string currencyAmount) {
		Mailing.Instance.SetMailCurrencyAmount(2, int.Parse(currencyAmount));
		checkCurrency();
	}
	
	public void SetSendMoney(bool sendMoney) {
		Mailing.Instance.MailBeingComposed.cashOnDelivery = !sendMoney;
	}
	
	public void SetCOD(bool COD) {
		Mailing.Instance.MailBeingComposed.cashOnDelivery = COD;
	}
	
	public void Send() {
		Mailing.Instance.SendMail();
		// Clear all attachment slots
		foreach(UGUIMailAttachmentSlot attachmentSlot in attachmentSlots) {
			if (attachmentSlot.UguiActivatable != null)
				attachmentSlot.Discarded();
		}
		//gameObject.SetActive(false);
	}
	
	public void Cancel() {
		gameObject.SetActive(false);
	}
	
	bool checkCurrency() {
		List<Vector2> currencies = new List<Vector2>();
		foreach (Currency currency in Mailing.Instance.MailBeingComposed.currencies.Keys) {
			currencies.Add(new Vector2(currency.id, Mailing.Instance.MailBeingComposed.currencies[currency]));
		}
		
		if (Inventory.Instance.DoesPlayerHaveEnoughCurrency(currencies)) {
			Debug.Log("Player does have enough currency");
			return true;
		}
		Debug.Log("Player does not have enough currency");
		return false;
	}
}
