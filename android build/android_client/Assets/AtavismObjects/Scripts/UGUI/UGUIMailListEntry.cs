﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class UGUIMailListEntry : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text senderText;
	public Text subjectText;
	public Image itemIcon;
	MailEntry entry;

	// Use this for initialization
	void Start () {
	
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void MailEntryClicked() {
		Mailing.Instance.SelectedMail = entry;
	}
	
	public void SetMailEntryDetails(MailEntry entry) {
		this.entry = entry;
		this.senderText.text = entry.senderName;
		this.subjectText.text = entry.subject;
		//this.itemIcon.sprite = entry.;
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		set {
		}
	}
}
