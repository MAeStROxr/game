﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIPortrait : MonoBehaviour, IPointerClickHandler {

	public Text name;
	public Text levelText;
	public Image portrait;
	public RectTransform popupMenu;
	public Image leaderIcon;
	string characterName;
	string gender;
	int classID = -1;
	string level;

	// Use this for initialization
	void Start () {
		ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler("gender", GenderHandler);
		ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler("aspect", ClassHandler);
		ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler("level", LevelHandler);
		if (ClientAPI.GetPlayerObject() != null) {
			if (ClientAPI.GetPlayerObject().PropertyExists("gender")) {
				gender = (string)ClientAPI.GetPlayerObject().GetProperty("gender");
			}
			if (ClientAPI.GetPlayerObject().PropertyExists("aspect")) {
				classID = (int)ClientAPI.GetPlayerObject().GetProperty("aspect");
			}
			if (ClientAPI.GetPlayerObject().PropertyExists("level")) {
				level = "" + (int)ClientAPI.GetPlayerObject().GetProperty("level");
			}
			UpdatePortrait();
		}
		
		AtavismEventSystem.RegisterEvent("GROUP_UPDATE", this);
		if (leaderIcon != null)
			leaderIcon.gameObject.SetActive(false);
	}
	
	void OnDestroy() {
		ClientAPI.GetPlayerObject().RemovePropertyChangeHandler("gender", GenderHandler);
		ClientAPI.GetPlayerObject().RemovePropertyChangeHandler("aspect", ClassHandler);
		ClientAPI.GetPlayerObject().RemovePropertyChangeHandler("level", LevelHandler);
		AtavismEventSystem.UnregisterEvent("GROUP_UPDATE", this);
	}

	public void GenderHandler(object sender, PropertyChangeEventArgs args) {
		gender = (string)ClientAPI.GetPlayerObject().GetProperty("gender");
		UpdatePortrait();
	}
	
	public void ClassHandler(object sender, PropertyChangeEventArgs args) {
		classID = (int)ClientAPI.GetPlayerObject().GetProperty("aspect");
		UpdatePortrait();
	}

	public void LevelHandler(object sender, PropertyChangeEventArgs args) {
		level = "" + (int)ClientAPI.GetPlayerObject().GetProperty("level");
		UpdatePortrait();
	}

	public void UpdatePortrait() {
		name.text = ClientAPI.GetPlayerObject().Name;
		if (levelText != null)
			levelText.text = level;

		if (portrait != null) {
			Sprite portraitSprite = PortraitManager.Instance.LoadPortrait(ClientAPI.GetPlayerObject().GameObject.GetComponent<AtavismNode>());
			if (portraitSprite != null)
				portrait.sprite = portraitSprite;
		}
		
		if (leaderIcon != null) {
			if (AtavismGroup.Instance.LeaderOid != null && AtavismGroup.Instance.LeaderOid.ToLong() == ClientAPI.GetPlayerOid()) {
				leaderIcon.gameObject.SetActive(true);
			} else {
				leaderIcon.gameObject.SetActive(false);
			}
		}
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "GROUP_UPDATE") {
			// Update 
			UpdatePortrait();
		}
	}
	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Left) {
			ClientAPI.SetTarget(ClientAPI.GetPlayerOid());
			return;
		}
		if (eventData.button != PointerEventData.InputButton.Right)
			return;
		
		if (popupMenu.gameObject.activeSelf) {
			popupMenu.gameObject.SetActive(false);
			return;
		}
		
		// Verify the player is in a group
		if (AtavismGroup.Instance.Members == null || AtavismGroup.Instance.Members.Count == 0)
			return;
		
		// Work out what to put in the popup menu here
		popupMenu.gameObject.SetActive(true);
	}
	
	public void LeaveGroup() {
		AtavismGroup.Instance.LeaveGroup();
		popupMenu.gameObject.SetActive(false);
	}
}
