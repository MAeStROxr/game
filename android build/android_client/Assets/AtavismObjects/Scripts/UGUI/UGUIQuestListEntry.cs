﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class UGUIQuestListEntry : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text questTitleText;
	QuestLogEntry entry;
	int questPos;
	UGUIQuestList questList;

	// Use this for initialization
	void Start () {
	
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void QuestEntryClicked() {
		Quests.Instance.QuestLogEntrySelected(questPos);
		questList.SetQuestDetails();
	}
	
	public void SetQuestEntryDetails(QuestLogEntry entry, int pos, UGUIQuestList questList) {
		this.entry = entry;
		this.questPos = pos;
		this.questList = questList;
		if (entry.Complete) {
			this.questTitleText.text = entry.Title + " (Complete)";
		} else {
			this.questTitleText.text = entry.Title;
		}
		
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		set {
		}
	}
}
