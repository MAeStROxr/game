using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUISkillsWindow : UIList<UGUISkillButton> {

	public UGUIPanelTitleBar titleBar;
	public bool onlyShowClassSkills = false;
	public bool onlyShowKnownSkills = false;
	public Image skillIcon;
	public Text skillName;
	public Text skillLevel;
	public Button increaseButton;
	public Text costText;
	public Text pointsText;
	public List<UGUISkillButton> skillButtons;
	public UGUIAbilitiesList abilitiesList;
	List<Skill> activeSkills = new List<Skill>();
	int selectedSkill = 0;

	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("ABILITY_UPDATE", this);
		AtavismEventSystem.RegisterEvent("SKILL_UPDATE", this);
		UpdateSkills();
		
		if (titleBar != null) {
			titleBar.SetPanelTitle(ClientAPI.GetPlayerObject().Name);
		}
	}
	
	void OnEnable() {
		UpdateSkills();
	}
	
	void OnDisable() {
		// Delete the old list
		ClearAllCells();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("ABILITY_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("SKILL_UPDATE", this);
	}

	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "ABILITY_UPDATE" || eData.eventType == "SKILL_UPDATE") {
			// Update 
			UpdateSkills();
		}
	}
	
	public void SelectSkill(int skillPos) {
		selectedSkill = skillPos;
		UpdateSkills();
	}
	
	public void IncreaseSkill() {
		Skills.Instance.IncreaseSkill(activeSkills[selectedSkill].id);
	}

	public void UpdateSkills() {
		int classID = (int)ClientAPI.GetPlayerObject().GetProperty("aspect");
		// Get list of skills
		activeSkills.Clear();
		if (onlyShowKnownSkills) {
			foreach (Skill skill in Skills.Instance.PlayerSkills.Values) {
				if ((!onlyShowClassSkills && !skill.mainAspectOnly) || skill.mainAspect == classID) {
					activeSkills.Add(skill);
				}
			}
		} else {
			foreach (Skill skill in Skills.Instance.SkillsList.Values) {
				if ((!onlyShowClassSkills && !skill.mainAspectOnly) || skill.mainAspect == classID) {
					activeSkills.Add(skill);
				}
			}
		}

		
		/*for (int i = 0; i < skillButtons.Count; i++) {
			if (i < activeSkills.Count) {
				skillButtons[i].gameObject.SetActive(true);
				skillButtons[i].SetSkillData(activeSkills[i]);
			} else {
				skillButtons[i].gameObject.SetActive(false);
			}
		}*/
		
		// Update skill info
		if (skillIcon != null)
			skillIcon.sprite = activeSkills[selectedSkill].icon;
		if (skillName != null)
			skillName.text = activeSkills[selectedSkill].skillname;
		if (skillLevel != null) {
			if (Skills.Instance.PlayerSkills.ContainsKey(activeSkills[selectedSkill].id)) {
				Skill playerSkill = Skills.Instance.PlayerSkills[activeSkills[selectedSkill].id];
				skillLevel.text = playerSkill.CurrentLevel + "/" + playerSkill.MaximumLevel;
			} else {
				skillLevel.text = "-";
			}
		} 
		if (costText != null) {
			if (Skills.Instance.PlayerSkills.ContainsKey(activeSkills[selectedSkill].id)) {
				Skill playerSkill = Skills.Instance.PlayerSkills[activeSkills[selectedSkill].id];
				costText.text = "1";
			} else {
				costText.text = "1";
			}
		}
		if (pointsText != null) {
			pointsText.text = Skills.Instance.CurrentSkillPoints.ToString();
		}
			
		
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	
		abilitiesList.UpdateAbilities(activeSkills[selectedSkill]);
		
		/*List<Ability> abilities = ClientAPI.ScriptObject.GetComponent<Abilities>().PlayerAbilities;
		for (int i = 0; i < slots.Count; i++) {
			if (abilities.Count > i) {
				slots[i].UpdateAbilityData(abilities[i]);
			} else {
				slots[i].UpdateAbilityData(null);
			}
		}*/
	}

	public void Toggle() {
		gameObject.SetActive(!gameObject.activeSelf);
	}
	
	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int numCells = activeSkills.Count;
		return numCells;
	}
	
	public override void UpdateCell (int index, UGUISkillButton cell)
	{
		Skill skill = activeSkills[index];
		cell.SetSkillData(skill, this, index);
	}
	
	#endregion
}
