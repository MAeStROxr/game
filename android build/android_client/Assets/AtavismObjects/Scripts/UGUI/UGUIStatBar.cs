﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIStatBar : MonoBehaviour {

	public string prop;
	public string propMax;
	public bool forTarget = false;
	public Text label;
	int value = 1;
	int valueMax = 1;

	// Use this for initialization
	void Start () {
		if (forTarget) {
			AtavismEventSystem.RegisterEvent("PROPERTY_" + prop, this);
			AtavismEventSystem.RegisterEvent("PROPERTY_" + propMax, this);
			AtavismEventSystem.RegisterEvent("PLAYER_TARGET_CHANGED", this);
			
			if (ClientAPI.GetTargetObject() != null) {
				value = (int)ClientAPI.GetTargetObject().GetProperty(prop);
				valueMax = (int)ClientAPI.GetTargetObject().GetProperty(propMax);
				UpdateProgressBar();
			}
		} else {
			ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler(prop, PropHandler);
			ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler(propMax, PropMaxHandler);
			if (ClientAPI.GetPlayerObject() != null) {
				if (ClientAPI.GetPlayerObject().PropertyExists(prop)) {
					value = (int)ClientAPI.GetPlayerObject().GetProperty(prop);
				}
				if (ClientAPI.GetPlayerObject().PropertyExists(propMax)) {
					valueMax = (int)ClientAPI.GetPlayerObject().GetProperty(propMax);
				}
				UpdateProgressBar();
			}
		}
	}
	
	void OnDestroy() {
		if (forTarget) {
			AtavismEventSystem.UnregisterEvent("PROPERTY_" + prop, this);
			AtavismEventSystem.UnregisterEvent("PROPERTY_" + propMax, this);
			AtavismEventSystem.UnregisterEvent("PLAYER_TARGET_CHANGED", this);
		} else {
			ClientAPI.GetPlayerObject().RemovePropertyChangeHandler(prop, PropHandler);
			ClientAPI.GetPlayerObject().RemovePropertyChangeHandler(propMax, PropMaxHandler);
		}
	}

	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "PROPERTY_" + prop) {
			if (eData.eventArgs[0] == "target") {
				value = (int)ClientAPI.GetTargetObject().GetProperty(prop);
			}
			//Debug.Log("Got health property: " + eData.eventArgs.Length + " with unit: " + eData.eventArgs[0]);
		} else if (eData.eventType == "PROPERTY_" + propMax) {
			if (eData.eventArgs[0] == "target") {
				valueMax = (int)ClientAPI.GetTargetObject().GetProperty(propMax);
			}
		} else if (eData.eventType == "PLAYER_TARGET_CHANGED") {
			if (ClientAPI.GetTargetObject() != null) {
				value = (int)ClientAPI.GetTargetObject().GetProperty(prop);
				valueMax = (int)ClientAPI.GetTargetObject().GetProperty(propMax);
				UpdateProgressBar();
			} else {
				value = 100;
				valueMax = 100;
				UpdateProgressBar();
			}
		}
		UpdateProgressBar();
	}

	public void PropHandler(object sender, PropertyChangeEventArgs args) {
		value = (int)ClientAPI.GetPlayerObject().GetProperty(prop);
		UpdateProgressBar();
	}
	
	public void PropMaxHandler(object sender, PropertyChangeEventArgs args) {
		valueMax = (int)ClientAPI.GetPlayerObject().GetProperty(propMax);
		UpdateProgressBar();
	}

	void UpdateProgressBar() {
		
		GetComponent<Slider>().value = (float)value / (float)valueMax;
		if (label != null) {
			label.text = value + " / " + valueMax;
		}
	}
}
