﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIStatText : MonoBehaviour {

	public Text text;
	public string prop;

	// Use this for initialization
	void Start () {
		if (text == null)
			text = GetComponent<Text>();

		ClientAPI.GetPlayerObject().RegisterPropertyChangeHandler(prop, PropHandler);
		if (ClientAPI.GetPlayerObject() != null) {
			if (ClientAPI.GetPlayerObject().PropertyExists(prop)) {
				int value = (int)ClientAPI.GetPlayerObject().GetProperty(prop);
				text.text = "" + value;
			}
		}
	}
	
	void OnDestroy() {
		ClientAPI.GetPlayerObject().RemovePropertyChangeHandler(prop, PropHandler);
	}

	public void PropHandler(object sender, PropertyChangeEventArgs args) {
		int value = (int)ClientAPI.GetPlayerObject().GetProperty(prop);
		text.text = "" + value;
	}
}
