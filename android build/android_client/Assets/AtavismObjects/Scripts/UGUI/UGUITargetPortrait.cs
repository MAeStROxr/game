﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class UGUITargetPortrait : MonoBehaviour, IPointerClickHandler {

	public Text name;
	public Text levelText;
	public Image portrait;
	public bool showPopupOnRightClick = true;
	public Button menuPopupButton;
	public RectTransform popupMenu;
	public List<Button> menuButtons;
	public UGUIChatController chatController;
	public List<UGUIEffect> effectButtons;
	string characterName;
	string gender;
	int classID = -1;
	string level;

	// Use this for initialization
	void Start () {
		Hide();
		AtavismEventSystem.RegisterEvent("PLAYER_TARGET_CHANGED", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("PLAYER_TARGET_CHANGED", this);
	}
	
	void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
	
	void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "PLAYER_TARGET_CHANGED") {
			if (ClientAPI.GetTargetObject() != null) {
				Show ();
				UpdatePortrait();
			} else {
				Hide ();
			}
			popupMenu.gameObject.SetActive(false);
		}
	}

	public void UpdatePortrait() {
		name.text = ClientAPI.GetTargetObject().Name;
		if (levelText != null) {
			int level = (int) ClientAPI.GetTargetObject().GetProperty("level");
			levelText.text = level.ToString();
		}
			
		// Try get other properties
		/*if (ClientAPI.GetTargetObject().PropertyExists("health")) {
			health = (int)ClientAPI.GetTargetObject().GetProperty("health");
		}
		if (ClientAPI.GetTargetObject().PropertyExists("health-max")) {
			maxHealth = (int)ClientAPI.GetTargetObject().GetProperty("health-max");
		}*/

		if (portrait != null) {
			Sprite portraitSprite = PortraitManager.Instance.LoadPortrait(ClientAPI.GetTargetObject().GameObject.GetComponent<AtavismNode>());
			if (portraitSprite != null)
				portrait.sprite = portraitSprite;
		}
		
		// Show/Hide menuPopupButton depending on whether it is an NPC or not
		if (menuPopupButton != null) {
			if (ClientAPI.GetTargetObject().CheckBooleanProperty("combat.userflag")) {
				menuPopupButton.gameObject.SetActive(true);
			} else {
				menuPopupButton.gameObject.SetActive(false);
			}
		}
	}
	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Right)
			return;
				
		if (showPopupOnRightClick)
			TogglePopupMenu();
	}
	
	public void TogglePopupMenu() {
		if (popupMenu.gameObject.activeSelf) {
			popupMenu.gameObject.SetActive(false);
			return;
		}
		
		// Verify the target is a player and is friendly
		if (!ClientAPI.GetTargetObject().CheckBooleanProperty("combat.userflag"))
			return;
		
		int targetType = (int)ClientAPI.GetTargetObject().GetProperty("targetType");
		if (targetType <= 0)
			return;
		
		// Work out what to put in the popup menu here
		popupMenu.gameObject.SetActive(true);
	}
	
	public void MenuButtonClicked(int buttonPos) {
	}
	
	public void TradeOptionClicked() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetTargetOid(), "/trade");
		popupMenu.gameObject.SetActive(false);
	}
	
	public void WhisperOptionClicked() {
		if (chatController != null) {
			chatController.StartWhisper(ClientAPI.GetTargetObject().Name);
		}
		popupMenu.gameObject.SetActive(false);
	}
	
	public void InviteOptionClicked() {
		AtavismGroup.Instance.SendInviteRequestMessage(OID.fromLong(ClientAPI.GetTargetOid()));
		popupMenu.gameObject.SetActive(false);
	}
	
	public void DuelOptionClicked() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetTargetOid(), "/duel");
		popupMenu.gameObject.SetActive(false);
	}
}
