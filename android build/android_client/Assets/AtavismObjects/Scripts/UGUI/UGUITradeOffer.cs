﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUITradeOffer : MonoBehaviour {

	public UGUIItemDisplay itemDisplay;
	public Text nameLabel;

	// Use this for initialization
	void Start () {
	
	}
	
	public void UpdateTradeOfferData(AtavismInventoryItem item) {
		if (item == null) {
			nameLabel.text = "";
			itemDisplay.gameObject.SetActive(false);
		} else {
			nameLabel.text = item.name;
			itemDisplay.gameObject.SetActive(true);
			itemDisplay.SetItemData(item, null);
		}	
	}
}
