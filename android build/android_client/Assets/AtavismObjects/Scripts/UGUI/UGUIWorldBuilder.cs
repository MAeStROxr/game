﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIWorldBuilder : UIList<UGUIClaimPermission> {

	public UGUIPanelTitleBar titleBar;
	public Text claimName;
	public Text size;
	public Text status;
	public Button buyButton;
	public Button sellButton;
	public Button createButton;
	public Button deleteButton;
	public Button permissionsButton;
	public UGUIBuildObjectsList buildObjectList;
	public RectTransform editObjectPanel;
	public Text objectName;
	public Text editMode;
	public Text objectStatus;
	public List<UGUIItemDisplay> requiredItems;
	public RectTransform createClaimPanel;
	public Text claimSizeText;
	public UGUICurrencyInputPanel currencyInputPanel;
	public RectTransform sellClaimPanel;
	public Toggle sellClaimForSaleToggle;
	public UGUICurrencyInputPanel sellClaimCurrencyPanel;
	public RectTransform permissionsPanel;
	public Text permissionLevel;
	public KeyCode toggleKey;
	bool showing = false;
	
	//public KeyCode toggleKey;
	string newClaimName;
	int newClaimSize;
	bool playerOwned = true;
	bool forSale = true;
	int currencyID = 0;
	int cost = 0;
	string playerPermissionName;
	int permissionLevelGiven = 0;
	string[] levels = new string[] {"Add Objects", "Edit Objects", "Add Users", "Manage Users"};

	// Use this for initialization
	void Start () {
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		Hide ();
			
		AtavismEventSystem.RegisterEvent("CLAIM_CHANGED", this);
		AtavismEventSystem.RegisterEvent("CLAIM_OBJECT_SELECTED", this);
		AtavismEventSystem.RegisterEvent("CLAIM_OBJECT_UPDATED", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("CLAIM_CHANGED", this);
		AtavismEventSystem.UnregisterEvent("CLAIM_OBJECT_SELECTED", this);
		AtavismEventSystem.UnregisterEvent("CLAIM_OBJECT_UPDATED", this);
	}
	
	void Update() {
		if (ControlFreak2.CF2Input.GetKeyDown(toggleKey) && !ClientAPI.UIHasFocus()) {
			if (showing)
				Hide ();
			else
				Show ();
		}
	}
	
	public void Toggle() {
		if (showing)
			Hide ();
		else
			Show ();
	}
	
	/*void OnEnable() {
		UpdateClaimDetails();
		WorldBuilder.Instance.ShowClaims = true;
	}*/
	
	/*void OnDisable() {
		WorldBuilder.Instance.ShowClaims = false;
		HidePanels();
	}*/
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		showing = true;
		
		UpdateClaimDetails();
		WorldBuilder.Instance.ShowClaims = true;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		showing = false;
		
		WorldBuilder.Instance.ShowClaims = false;
		HidePanels();
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CLAIM_CHANGED") {
			UpdateClaimDetails();
		} else if (eData.eventType == "CLAIM_OBJECT_SELECTED") {
			ShowEditObject();
		} else if (eData.eventType == "CLAIM_OBJECT_UPDATED") {
			ShowEditObject();
		}
	}
	
	public void UpdateClaimDetails() {
		if (permissionsPanel.gameObject.activeSelf && WorldBuilder.Instance.ActiveClaim != null) {
			PermissionsUpdated();
			
			claimName.text = WorldBuilder.Instance.ActiveClaim.name;
			size.text = WorldBuilder.Instance.ActiveClaim.area.ToString();
			if (WorldBuilder.Instance.ActiveClaim.forSale) {
				status.text = "For Sale";
				if (!WorldBuilder.Instance.ActiveClaim.playerOwned)
					buyButton.gameObject.SetActive(true);
			} else {
				status.text = "Owned";
			}
			
			return;
		} else {
			HidePanels();
			buyButton.gameObject.SetActive(false);
			sellButton.gameObject.SetActive(false);
			deleteButton.gameObject.SetActive(false);
			createButton.gameObject.SetActive(false);
			permissionsButton.gameObject.SetActive(false);
			if (WorldBuilder.Instance.ActiveClaim == null) {
				claimName.text = "-";
				size.text = "-";
				status.text = "-";
				if (ClientAPI.IsPlayerAdmin()) {
					createButton.gameObject.SetActive(true);
				}
				return;
			}
		}
		
		claimName.text = WorldBuilder.Instance.ActiveClaim.name;
		size.text = WorldBuilder.Instance.ActiveClaim.area.ToString();
		if (WorldBuilder.Instance.ActiveClaim.playerOwned) {
			buildObjectList.gameObject.SetActive(true);
			sellButton.gameObject.SetActive(true);
			deleteButton.gameObject.SetActive(true);
			permissionsButton.gameObject.SetActive(true);
		} else if (WorldBuilder.Instance.ActiveClaim.permissionlevel > 0) {
			buildObjectList.gameObject.SetActive(true);
		} else {
			buildObjectList.gameObject.SetActive(false);
		}
		
		if (WorldBuilder.Instance.ActiveClaim.forSale) {
			status.text = "For Sale";
			if (!WorldBuilder.Instance.ActiveClaim.playerOwned)
				buyButton.gameObject.SetActive(true);
		} else {
			status.text = "Owned";
		}
	}
	
	void HidePanels() {
		buildObjectList.gameObject.SetActive(false);
		editObjectPanel.gameObject.SetActive(false);
		createClaimPanel.gameObject.SetActive(false);
		sellClaimPanel.gameObject.SetActive(false);
		permissionsPanel.gameObject.SetActive(false);
	}
	
	public void DeleteClaim() {
		string message = "Are you sure you want to delete your claim: " + WorldBuilder.Instance.ActiveClaim.name;
		UGUIConfirmationPanel.Instance.ShowConfirmationBox(message, null, ConfirmedDeleteClaim);
	}
	
	public void ConfirmedDeleteClaim(object obj, bool accepted) {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", WorldBuilder.Instance.ActiveClaim.id);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.DELETE_CLAIM", props);
		AtavismCursor.Instance.ClearUGUIActivatableClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
	}
	
	public void BuyClaim() {
		string costString = Inventory.Instance.GetCostString(WorldBuilder.Instance.ActiveClaim.currency, 
			WorldBuilder.Instance.ActiveClaim.cost);
		string message = "Are you sure you want to buy claim: " + WorldBuilder.Instance.ActiveClaim.name
			+ " for " + costString + "?";
		UGUIConfirmationPanel.Instance.ShowConfirmationBox(message, null, ConfirmedBuyClaim);
	}
	
	public void ConfirmedBuyClaim(object obj, bool accepted) {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", WorldBuilder.Instance.ActiveClaim.id);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.PURCHASE_CLAIM", props);
		AtavismCursor.Instance.ClearUGUIActivatableClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
	}
	
	#region Create Claim
	public void ShowCreateClaimPanel() {
		HidePanels();
		createClaimPanel.gameObject.SetActive(true);
		currencyInputPanel.SetCurrencies(Inventory.Instance.GetMainCurrencies());
	}
	
	public void SetClaimName(string name) {
		newClaimName = name;
	}
	
	public void SetClaimSize(float size) {
		newClaimSize = (int)size;
		claimSizeText.text = newClaimSize.ToString();
	}
	
	public void SetPlayerOwned(bool owned) {
		playerOwned = owned;
	}
	
	public void SetForSale(bool forSale) {
		this.forSale = forSale;
	}
	
	public void CreateClaim() {
		currencyInputPanel.GetCurrencyAmount(out currencyID, out cost);
		WorldBuilder.Instance.CreateClaim(newClaimName, newClaimSize, playerOwned, forSale, currencyID, cost);
		ShowObjectList();
	}
	#endregion Create Claim
	
	#region Edit Object
	public void ShowObjectList() {
		HidePanels();
		buildObjectList.gameObject.SetActive(true);
	}
	
	public void ShowEditObjectPanel() {
		HidePanels();
		editObjectPanel.gameObject.SetActive(true);
		AtavismCursor.Instance.SetUGUIActivatableClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
	}
	
	public void StartSelectObject() {
		WorldBuilder.Instance.BuildingState = WorldBuildingState.SelectItem;
		AtavismCursor.Instance.ClearUGUIActivatableClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
		WorldBuilderInterface.Instance.StartSelectObject();
	}
	
	void ShowEditObject() {
		AtavismCursor.Instance.SetUGUIActivatableClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
		HidePanels();
		editObjectPanel.gameObject.SetActive(true);
		
		objectName.text = WorldBuilder.Instance.SelectedObject.name;
		if (WorldBuilderInterface.Instance.MouseWheelBuildMode == MouseWheelBuildMode.MoveVertical) {
			editMode.text = "Vertical";
		} else {
			editMode.text = "Rotate";
		}
		if (WorldBuilder.Instance.SelectedObject.Complete) {
			if (WorldBuilder.Instance.SelectedObject.Health < WorldBuilder.Instance.SelectedObject.MaxHealth) {
				string statusText = "Damaged: " + WorldBuilder.Instance.SelectedObject.Health + "/" + WorldBuilder.Instance.SelectedObject.MaxHealth;
				objectStatus.text = statusText;
			} else {
				objectStatus.text = "Complete";
			}
		} else {
			string statusText = "In Construction: " + WorldBuilder.Instance.SelectedObject.Health + "/" + WorldBuilder.Instance.SelectedObject.MaxHealth;
			objectStatus.text = statusText;
		}
		
		AtavismBuildObjectTemplate template = WorldBuilder.Instance.GetBuildObjectTemplate(WorldBuilder.Instance.SelectedObject.TemplateID);
		
		for (int i = 0; i < requiredItems.Count; i++) {
			if (template != null && i < template.upgradeItemsReq.Count) {
				requiredItems[i].gameObject.SetActive(true);
				AtavismInventoryItem item = Inventory.Instance.GetItemByTemplateID(template.upgradeItemsReq[i]);
				requiredItems[i].SetItemData(item, null);
			} else {
				requiredItems[i].gameObject.SetActive(false);
			}
		}
	}
	
	public void StartMoveItem() {
		WorldBuilderInterface.Instance.SetCurrentReticle(WorldBuilder.Instance.SelectedObject.gameObject);
		WorldBuilder.Instance.BuildingState = WorldBuildingState.MoveItem;
	}
	
	public void ChangeEditMode() {
		if (WorldBuilderInterface.Instance.MouseWheelBuildMode == MouseWheelBuildMode.MoveVertical) {
			WorldBuilderInterface.Instance.MouseWheelBuildMode = MouseWheelBuildMode.Rotate;
			editMode.text = "Rotate";
		} else {
			WorldBuilderInterface.Instance.MouseWheelBuildMode = MouseWheelBuildMode.MoveVertical;
			editMode.text = "Vertical";
		}
	}
	
	public void RemoveItem() {
		WorldBuilder.Instance.PickupClaimObject();
		HidePanels();
		buildObjectList.gameObject.SetActive(true);
	}
	
	public void SaveObjectChanges() {
		WorldBuilder.Instance.SendEditObjectPosition(WorldBuilder.Instance.SelectedObject.gameObject);
		HidePanels();
		buildObjectList.gameObject.SetActive(true);
	}
	
	#endregion Edit Object
	
	#region Sell Claim
	public void ShowSellClaimPanel() {
		HidePanels();
		sellClaimPanel.gameObject.SetActive(true);
		sellClaimCurrencyPanel.SetCurrencies(Inventory.Instance.GetMainCurrencies());
		sellClaimCurrencyPanel.SetCurrencyAmounts(WorldBuilder.Instance.ActiveClaim.currency, WorldBuilder.Instance.ActiveClaim.cost);
		sellClaimForSaleToggle.isOn = WorldBuilder.Instance.ActiveClaim.forSale;
	}
	
	public void SaveSellClaimSettings() {
		WorldBuilder.Instance.ActiveClaim.forSale = sellClaimForSaleToggle.isOn;
		sellClaimCurrencyPanel.GetCurrencyAmount(out currencyID, out cost);
		WorldBuilder.Instance.ActiveClaim.currency = currencyID;
		WorldBuilder.Instance.ActiveClaim.cost = cost;
		WorldBuilder.Instance.SendEditClaim();
		ShowObjectList();
	}
	
	#endregion Sell Claim
	
	#region Permissions
	public void ShowPermissionsPanel() {
		HidePanels();
		permissionsPanel.gameObject.SetActive(true);
		playerPermissionName = "";
		permissionLevelGiven = 0;
		permissionLevel.text = levels[permissionLevelGiven].ToString();
		
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	void PermissionsUpdated() {
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	public void SetPermissionPlayerName(string name) {
		playerPermissionName = name;
	}
	
	public void ChangePermissionLevel() {
		permissionLevelGiven++;
		if (permissionLevelGiven >= levels.Length) {
			permissionLevelGiven = 0;
		}
		permissionLevel.text = levels[permissionLevelGiven].ToString();
	}
	
	public void AddPermission() {
		// Add 1 to permission level because I'm an idiot and set it to index 1 on the server
		WorldBuilder.Instance.AddPermission(playerPermissionName, permissionLevelGiven + 1);
	}
	
	
	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int numCells =  WorldBuilder.Instance.ActiveClaim.permissions.Count;
		return numCells;
	}
	
	public override void UpdateCell (int index, UGUIClaimPermission cell)
	{
		cell.SetPermissionDetails(WorldBuilder.Instance.ActiveClaim.permissions[index]);
	}
	
	#endregion
	
	#endregion Permissions
}
