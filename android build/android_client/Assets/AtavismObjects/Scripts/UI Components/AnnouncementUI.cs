﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnnouncementUI : AtavismWindowTemplate {
	string announcement;
	float stopDisplay;

	// Use this for initialization
	void Start () {
		SetupRect();
		
		// Register for 
		AtavismEventSystem.RegisterEvent("ANNOUNCEMENT", this);
		
		NetworkAPI.RegisterExtensionMessageHandler("announcement", HandleAnnouncementMessage);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		GUI.skin = skin;
		if (Time.time < stopDisplay) {
			GUI.Label(uiRect, announcement, skin.GetStyle("AnnouncementText"));
			//GUI.Label(new Rect(Screen.width / 2 - 100, 50, 200, 40), errorMessage);
		}
	}
	
	public void HandleAnnouncementMessage(Dictionary<string, object> props) {
		announcement = (string) props["AnnouncementText"];
		stopDisplay = Time.time + 3;
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "ANNOUNCEMENT") {
			announcement = eData.eventArgs[0];
			stopDisplay = Time.time + 3;
		}
	}
	
}
