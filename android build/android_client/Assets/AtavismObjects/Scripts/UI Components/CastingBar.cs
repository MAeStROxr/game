using UnityEngine;
using System.Collections;

public class CastingBar : AtavismWindowTemplate {

	public GUISkin skin2;
	public GUISkin bar;	
	
	float length;
	float expirationTime = -1;

	// Use this for initialization
	void Start () {
		SetupRect();
		// Register for 
		AtavismEventSystem.RegisterEvent("CASTING_STARTED", this);
		AtavismEventSystem.RegisterEvent("CASTING_CANCELLED", this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnDestroy () {
		AtavismEventSystem.UnregisterEvent("CASTING_STARTED", this);
		AtavismEventSystem.UnregisterEvent("CASTING_CANCELLED", this);
	}
	
	void OnGUI() {

		// Return if we have no target
		if (expirationTime < Time.time)
			return;
			
		GUI.depth = uiLayer;
		GUI.skin = skin;
		// Health
		float timeLeft = expirationTime - Time.time;
		float healthBarLength = ((length-timeLeft) / length) * width * 0.9f;

		GUI.skin = skin2;
		GUI.Box (new Rect (uiRect.x, uiRect.y, width, 15), "");
		
		GUI.skin = bar;
		GUI.color = Color.red;
		GUI.Box (new Rect (uiRect.x + 5, uiRect.y + 3, healthBarLength, 8), "");
		GUI.color = Color.white;
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CASTING_STARTED") {
			length = float.Parse(eData.eventArgs[0]);
			expirationTime = Time.time + length;
		} else if (eData.eventType == "CASTING_CANCELLED") {
			expirationTime = -1;
		}
	}
}
