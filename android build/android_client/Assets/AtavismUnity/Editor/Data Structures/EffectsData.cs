﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;

// Structure of an Atavism Effect
/*
/* Table structure for tables related to effects
/*

CREATE TABLE `effects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `displayName` varchar(64) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `effectType` varchar(64) DEFAULT NULL,
  `effectFamily` int(11) DEFAULT NULL,
  `isBuff` tinyint(1) NOT NULL DEFAULT '0',
  `skillType` int(11) DEFAULT NULL,
  `passive` tinyint(1) DEFAULT NULL,
  `stackLimit` int(11) DEFAULT NULL,
  `allowMultiple` tinyint(1) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `numPulses` int(11) DEFAULT NULL,
  `tooltip` varchar(255) DEFAULT NULL,
  `bonusEffectReq` int(11) DEFAULT NULL,
  `bonusEffectReqConsumed` tinyint(1) DEFAULT NULL,
  `bonusEffect` int(11) NOT NULL DEFAULT '-1',
  `pulseParticle` varchar(32) DEFAULT NULL,
    
*/

public class EffectsData: DataStructure
{
	public int id = 0;					// Database Index
	// General Parameters
	public string name = "name";		// The ability template name
	
	//public string displayName = "";
	public string icon = "";
	public string effectType = "";
	public string effectMainType = "";
	// public int effectFamily = 0;
	public bool isBuff = false;
	public int skillType = 0;
	public float skillLevelMod = 0;
	public bool passive = false;
	public int stackLimit = 0;
	public bool allowMultiple = false;
	public float duration = 0;
	public int pulseCount = 1;
	public string tooltip = "";
	public int bonusEffectReq = 0;
	public bool bonusEffectReqConsumed = false;
	public int bonusEffect = 0;
	public string pulseCoordEffect = "";
	
	public int intValue1 = 0;
	public int intValue2 = 0;
	public int intValue3 = 0;
	public int intValue4 = 0;
	public int intValue5 = 0;
	
	public float floatValue1 = 0f;
	public float floatValue2 = 0f;
	public float floatValue3 = 0f;
	public float floatValue4 = 0f;
	public float floatValue5 = 0f;
	
	public string stringValue1 = "";
	public string stringValue2 = "";
	public string stringValue3 = "";
	public string stringValue4 = "";
	public string stringValue5 = "";
	
	public bool boolValue1 = false;
	public bool boolValue2 = false;
	public bool boolValue3 = false;
	public bool boolValue4 = false;
	public bool boolValue5 = false;
	
	
	public List<ServerEffectType> effectTypes = new List<ServerEffectType>();
	public ServerEffectType effectClass = null;

	// Used locally to determine if a prefab should be created
	public bool createPrefab = false;
	
	public EffectsData ()
	{
		// Database fields
		fields = new Dictionary<string, string> () {
		{"name", "string"},
		//{"displayName", "string"},
		{"icon", "string"},
		{"effectMainType", "string"},
		{"effectType", "string"},
		//{"effectFamily", "int"},
		{"isBuff", "bool"},
		{"skillType", "int"},
		{"skillLevelMod", "float"},
		{"passive", "bool"},
		{"stackLimit", "int"},
		{"allowMultiple", "bool"},
		{"duration", "float"},
		{"pulseCount", "int"},
		{"tooltip", "string"},
		{"bonusEffectReq", "int"},
		{"bonusEffectReqConsumed", "bool"},
		{"bonusEffect", "int"},
		{"pulseCoordEffect", "string"},
		{"intValue1", "int"},
		{"intValue2", "int"},
		{"intValue3", "int"},
		{"intValue4", "int"},
		{"intValue5", "int"},
		{"floatValue1", "float"},
		{"floatValue2", "float"},
		{"floatValue3", "float"},
		{"floatValue4", "float"},
		{"floatValue5", "float"},
		{"stringValue1", "string"},
		{"stringValue2", "string"},
		{"stringValue3", "string"},
		{"stringValue4", "string"},
		{"stringValue5", "string"},
		{"boolValue1", "bool"},
		{"boolValue2", "bool"},
		{"boolValue3", "bool"},
		{"boolValue4", "bool"},
		{"boolValue5", "bool"},
		};
	
		// Add Effect type classes here
		effectTypes.Add(new ServerDamageEffects());
		effectTypes.Add(new ServerRestoreEffects());
		effectTypes.Add(new ServerReviveEffects());
		effectTypes.Add(new ServerStatEffects());
		effectTypes.Add(new ServerStunEffects());
		effectTypes.Add(new ServerImmuneEffects());
		effectTypes.Add(new ServerMorphEffects());
		//effectTypes.Add(new ServerPropertyEffects());
		effectTypes.Add(new ServerTeleportEffects());
		effectTypes.Add(new ServerMountEffects());
		effectTypes.Add(new ServerBuildObjectEffects());
		effectTypes.Add(new ServerTeachAbilityEffects());
		effectTypes.Add(new ServerTeachSkillEffects());
		effectTypes.Add(new ServerTaskEffects());
		effectTypes.Add(new ServerStateEffects());
		effectTypes.Add(new ServerThreatEffects());
	}	
	
	public EffectsData Clone()
	{
		return (EffectsData) this.MemberwiseClone();
	}
		
	public override string GetValue (string fieldKey)
	{
		switch (fieldKey) {
		case "id":
			return id.ToString();
			break;
		case "name":
			return name;
			break;
		//case "displayName":
		//	return displayName;
		//	break;
		case "icon":
			return icon;
			break;
		case "effectMainType":
			return effectMainType;
			break;
		case "effectType":
			return effectType;
			break;
		//case "effectFamily":
			//	return effectFamily.ToString();
			//break;
		case "isBuff":
			return isBuff.ToString();
			break;
		case "skillType":
			return skillType.ToString();
			break;
		case "skillLevelMod":
			return skillLevelMod.ToString();
			break;
		case "passive":
			return passive.ToString();
			break;
		case "stackLimit":
			return stackLimit.ToString();
			break;
		case "allowMultiple":
			return allowMultiple.ToString();
			break;
		case "duration":
			return duration.ToString();
			break;
		case "pulseCount":
			return pulseCount.ToString();
			break;
		case "tooltip":
			return tooltip.ToString();
			break;
		case "bonusEffectReq":
			return bonusEffectReq.ToString();
			break;
		case "bonusEffectReqConsumed":
			return bonusEffectReqConsumed.ToString();
			break;
		case "bonusEffect":
			return bonusEffect.ToString();
			break;
		case "pulseCoordEffect":
			return pulseCoordEffect;
			break;
		case "intValue1":
			return intValue1.ToString();
			break;
		case "intValue2":
			return intValue2.ToString();
			break;
		case "intValue3":
			return intValue3.ToString();
			break;
		case "intValue4":
			return intValue4.ToString();
			break;
		case "intValue5":
			return intValue5.ToString();
			break;
		case "floatValue1":
			return floatValue1.ToString();
			break;
		case "floatValue2":
			return floatValue2.ToString();
			break;
		case "floatValue3":
			return floatValue3.ToString();
			break;
		case "floatValue4":
			return floatValue4.ToString();
			break;
		case "floatValue5":
			return floatValue5.ToString();
			break;
		case "stringValue1":
			return stringValue1;
			break;
		case "stringValue2":
			return stringValue2;
			break;
		case "stringValue3":
			return stringValue3;
			break;
		case "stringValue4":
			return stringValue4;
			break;
		case "stringValue5":
			return stringValue5;
			break;
		case "boolValue1":
			return boolValue1.ToString();
			break;
		case "boolValue2":
			return boolValue2.ToString();
			break;
		case "boolValue3":
			return boolValue3.ToString();
			break;
		case "boolValue4":
			return boolValue4.ToString();
			break;
		case "boolValue5":
			return boolValue5.ToString();
			break;
		}	
		return "";
	}
		
}