﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;

public class GameSettingData: DataStructure
{
	public int id = 0;					// Database Index
	// General Parameters
	public string name = "";
	public string dataType = "";
	public string val = "";
	
	public GameSettingData ()
	{
		// Database fields
		fields = new Dictionary<string, string> () {
		{"name", "string"},
		{"dataType", "string"},
		{"value", "string"}
	};
	}
	
	public GameSettingData Clone ()
	{
		return (GameSettingData)this.MemberwiseClone ();
	}
		
	public override string GetValue (string fieldKey)
	{
		switch (fieldKey) {
		case "id":
			return id.ToString();
			break;
		case "name":
			return name;
			break;
		case "dataType":
			return dataType;
			break;
		case "value":
			return val;
			break;
		}	
		return "";
	}
		
}