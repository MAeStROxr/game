using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;


public class MobSpawnData: DataStructure
{
	// General Parameters
	public string name = "name";
	public int mobTemplate = -1;
	public int numSpawns = 1;
	public int spawnRadius = 0;
	public int respawnTime = 60000; // Milliseconds
	public int corpseDespawnTime = 50000; // Milliseconds
	public bool combat = true;
	public int roamRadius = 0; // Metres
	public string startsQuests = "";
	public string endsQuests = "";
	public string startsDialogues = "";
	public string baseAction = "";
	public bool weaponSheathed = false;
	public int merchantTable = -1;
	public int questOpenLootTable = -1;
	public bool isChest = false;
	public int pickupItem = -1;

	public MobSpawnData ()
	{
		// Database fields
	fields = new Dictionary<string, string> () {
		{"name", "string"},
		{"mobTemplate", "int"},
		{"numSpawns", "int"},
		{"spawnRadius", "int"},
		{"respawnTime", "int"},
		{"corpseDespawnTime", "int"},
		{"combat", "bool"},
		{"roamRadius", "int"},
		{"startsQuests", "string"},
		{"endsQuests", "string"},
		{"startsDialogues", "string"},
		{"baseAction", "string"},
		{"weaponSheathed", "bool"},
		{"merchantTable", "int"},
		{"questOpenLootTable", "int"},
		{"isChest", "bool"},
		{"pickupItem", "int"},
	};
	}
	
	public MobSpawnData Clone()
	{
		return (MobSpawnData) this.MemberwiseClone();
	}
		
	public override string GetValue (string fieldKey)
	{
		switch (fieldKey) {
		case "id":
			return id.ToString();
			break;
		case "name":
			return name;
			break;
		case "mobTemplate":
			return mobTemplate.ToString();
			break;
		case "numSpawns":
			return numSpawns.ToString();
			break;
		case "spawnRadius":
			return spawnRadius.ToString();
			break;
		case "respawnTime":
			return respawnTime.ToString();
			break;
		case "corpseDespawnTime":
			return corpseDespawnTime.ToString();
			break;
		case "combat":
			return combat.ToString();
			break;
		case "roamRadius":
			return roamRadius.ToString();
			break;
		case "startsQuests":
			return startsQuests;
			break;
		case "endsQuests":
			return endsQuests;
			break;
		case "startsDialogues":
			return startsDialogues;
			break;
		case "baseAction":
			return baseAction;
			break;
		case "weaponSheathed":
			return weaponSheathed.ToString();
			break;
		case "merchantTable":
			return merchantTable.ToString();
			break;
		case "questOpenLootTable":
			return questOpenLootTable.ToString();
			break;
		case "isChest":
			return isChest.ToString();
			break;
		case "pickupItem":
			return pickupItem.ToString();
			break;
		}	
		return "";
	}
		
}