﻿using UnityEngine;
using System.Collections;

public class ItemsPlugin : AtavismPlugin {

	// Use this for initialization
	public ItemsPlugin()
	{	
		pluginName = "Item";
		string serverCategory = "AT_button_category_items";
		/*icon = (Texture)Resources.Load (serverCategory, typeof(Texture));
		iconOver = (Texture)Resources.Load (serverCategory + "_over", typeof(Texture));
		iconSelected = (Texture)Resources.Load (serverCategory + "_selected", typeof(Texture));
		icon.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + ".png"));
		iconOver.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + "_over.png"));
		iconSelected.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + "_selected.png"));*/
	}

	void Awake() {
		string serverCategory = "AT_button_category_items";
		icon = (Texture)Resources.Load (serverCategory, typeof(Texture));
		iconOver = (Texture)Resources.Load (serverCategory + "_over", typeof(Texture));
		iconSelected = (Texture)Resources.Load (serverCategory + "_selected", typeof(Texture));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
