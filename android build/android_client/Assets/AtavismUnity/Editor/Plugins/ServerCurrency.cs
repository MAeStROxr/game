﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Effects Configuration
public class ServerCurrency : AtavismDatabaseFunction
{
	
	public Dictionary<int, CurrencyData> dataRegister;
	public CurrencyData editingDisplay;
	public CurrencyData originalDisplay;
	
	// Handles the prefab creation, editing and save
	private CurrencyPrefab prefab = null;
	
	public int[] currencyIds = new int[] {-1};
	public string[] currencyOptions = new string[] {"~ none ~"};
	
	public string[] currencyGroupOptions = new string[] {"~ none ~"};
	
	// Use this for initialization
	public ServerCurrency ()
	{	
		functionName = "Currencies";
		// Database tables name
		tableName = "currencies";
		functionTitle = "Currency Configuration";
		loadButtonLabel = "Load Currencies";
		notLoadedText = "No Currencies loaded.";
		// Init
		dataRegister = new Dictionary<int, CurrencyData> ();
		
		editingDisplay = new CurrencyData ();	
		originalDisplay = new CurrencyData ();	
	}
	
	public override void Activate ()
	{
		linkedTablesLoaded = false;
	}
	
	public void LoadCurrencyOptions ()
	{
		if (!dataLoaded) {
			// Read all entries from the table
			string query = "SELECT id, name FROM currencies where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
			
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			int optionsId = 0;
			if ((rows != null) && (rows.Count > 0)) {
				currencyOptions = new string[rows.Count + 1];
				currencyOptions [optionsId] = "~ none ~"; 
				currencyIds = new int[rows.Count + 1];
				currencyIds [optionsId] = -1;
				foreach (Dictionary<string,string> data in rows) {
					optionsId++;
					currencyOptions [optionsId] = data ["name"]; 
					currencyIds [optionsId] = int.Parse (data ["id"]);
				}
			}
		}
	}
	
	// Load Database Data
	public override void Load ()
	{
		if (!dataLoaded) {
			// Clean old data
			dataRegister.Clear ();
			displayKeys.Clear ();
			
			// Read all entries from the table
			string query = "SELECT " + originalDisplay.GetFieldsString() + " FROM " + tableName + " where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
			
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			if ((rows != null) && (rows.Count > 0)) {
				foreach (Dictionary<string,string> data in rows) {
					//foreach(string key in data.Keys)
					//	Debug.Log("Name[" + key + "]:" + data[key]);
					//return;
					CurrencyData display = new CurrencyData ();
					display.id = int.Parse (data ["id"]);
					display.name = data ["name"]; 
					display.icon = data ["icon"];
					display.description = data ["description"];
					display.maximum = int.Parse (data ["maximum"]);
					display.currencyGroup = int.Parse (data ["currencyGroup"]);
					display.currencyPosition = int.Parse (data ["currencyPosition"]);
					display.external = bool.Parse (data ["external"]);
					
					display.isLoaded = true;
					//Debug.Log("Name:" + display.name  + "=[" +  display.id  + "]");
					dataRegister.Add (display.id, display);
					displayKeys.Add (display.id);
				}
				LoadSelectList ();
			}
			dataLoaded = true;
			foreach (CurrencyData currencyData in dataRegister.Values) {
				LoadCurrencyConversions (currencyData);
			}
		}
	}
	
	void LoadCurrencyConversions (CurrencyData currencyData)
	{
		// Read all entries from the table
		string query = "SELECT " + new CurrencyConversionEntry().GetFieldsString() + " FROM currency_conversion where currencyID = " 
			+ currencyData.id + " AND isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		//Debug.Log("#Rows:"+rows.Count);
		// Read all the data
		if ((rows != null) && (rows.Count > 0)) {
			foreach (Dictionary<string,string> data in rows) {
				CurrencyConversionEntry display = new CurrencyConversionEntry ();
				display.id = int.Parse (data ["id"]);
				display.currencyToID = int.Parse (data ["currencyToID"]);
				display.amount = int.Parse (data ["amount"]);
				display.autoConverts = bool.Parse(data["autoConverts"]);
				currencyData.currencyConversion.Add(display);
			}
		}
	}
	
	public void LoadSelectList ()
	{
		//string[] selectList = new string[dataRegister.Count];
		displayList = new string[dataRegister.Count];
		int i = 0;
		foreach (int displayID in dataRegister.Keys) {
			//selectList [i] = displayID + ". " + dataRegister [displayID].name;
			displayList [i] = displayID + ". " + dataRegister [displayID].name;
			i++;
		}
		//displayList = new Combobox(selectList);
	}	
	
	
	// Draw the loaded list
	public override void DrawLoaded (Rect box)
	{	
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
		
		if (dataRegister.Count <= 0) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "You must create a Currency before editing it.");		
			return;
		}
		
		// Draw the content database info
		ImagePack.DrawLabel (pos.x, pos.y, "Edit Currency");
		
		if (newItemCreated) {
			newItemCreated = false;
			LoadSelectList ();
			newSelectedDisplay = displayKeys.Count - 1;
		}
		
		// Draw data Editor
		if (newSelectedDisplay != selectedDisplay) {
			selectedDisplay = newSelectedDisplay;	
			int displayKey = displayKeys [selectedDisplay];
			editingDisplay = dataRegister [displayKey];		
			originalDisplay = editingDisplay.Clone();
		} 
		
		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.x -= ImagePack.innerMargin;
		pos.y -= ImagePack.innerMargin;
		pos.width += ImagePack.innerMargin;
		
		if (state != State.Loaded) {
			pos.x += ImagePack.innerMargin;
			pos.width /= 2;
			//Draw super magical compound object.
			newSelectedDisplay = ImagePack.DrawDynamicPartialListSelector(pos, "Search Filter: ", ref entryFilterInput, selectedDisplay, displayList);
			
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight * 1.5f;
			ImagePack.DrawLabel (pos.x, pos.y, "Currency Properties:");
			pos.y += ImagePack.fieldHeight * 0.75f;
		}
		
		DrawEditor (pos, false);
		
		pos.y -= ImagePack.fieldHeight;
		//pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
	}
	
	public override void CreateNewData ()
	{
		editingDisplay = new CurrencyData ();		
		originalDisplay = new CurrencyData ();
		selectedDisplay = -1;
	}
	
	// Edit or Create
	public override void DrawEditor (Rect box, bool newItem)
	{
		
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
		
		if (!linkedTablesLoaded) {	
			linkedTablesLoaded = true;
			LoadCurrencyOptions();
			currencyGroupOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Currency Group", true);
		}
		
		// Draw the content database info
		//pos.y += ImagePack.fieldHeight;
		
		if (newItem) {
			ImagePack.DrawLabel (pos.x, pos.y, "Create a new Currency");		
			pos.y += ImagePack.fieldHeight;
		}
		
		editingDisplay.name = ImagePack.DrawField (pos, "Name:", editingDisplay.name, 0.75f);
		pos.y += ImagePack.fieldHeight;
		pos.width /= 2;
		editingDisplay.maximum = ImagePack.DrawField (pos, "Max:", editingDisplay.maximum);
		pos.x += pos.width;
		editingDisplay.icon = ImagePack.DrawSpriteAsset (pos, "Icon:", editingDisplay.icon);		
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.currencyGroup = ImagePack.DrawSelector (pos, "Group:", editingDisplay.currencyGroup, currencyGroupOptions);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.currencyPosition = ImagePack.DrawSelector (pos, "Position:", editingDisplay.currencyPosition, editingDisplay.positionOptions);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.external = ImagePack.DrawToggleBox (pos, "External:", editingDisplay.external);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.description = ImagePack.DrawField (pos, "Description:", editingDisplay.description);
		
		// Conversion area
		pos.y += 1.5f * ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Conversion Options");
		pos.y += 1.5f * ImagePack.fieldHeight;
		if (editingDisplay.currencyConversion.Count == 0) {
			editingDisplay.currencyConversion.Add (new CurrencyConversionEntry (-1, -1, 0));
		}
		for (int i = 0; i < editingDisplay.currencyConversion.Count; i++) {
			editingDisplay.currencyConversion [i].amount = ImagePack.DrawField (pos, "Amount:", editingDisplay.currencyConversion [i].amount);
			pos.x += pos.width;
			int selectedCurrency = GetPositionOfCurrency (editingDisplay.currencyConversion [i].currencyToID);
			selectedCurrency = ImagePack.DrawSelector (pos, "Converts To 1 ", selectedCurrency, currencyOptions);
			editingDisplay.currencyConversion [i].currencyToID = currencyIds [selectedCurrency];
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
			editingDisplay.currencyConversion [i].autoConverts = ImagePack.DrawToggleBox (pos, "Auto Converts:", editingDisplay.currencyConversion [i].autoConverts);
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Remove Conversion")) {
				if (editingDisplay.currencyConversion[i].id > 0)
					editingDisplay.conversionsToBeDeleted.Add(editingDisplay.currencyConversion[i].id);
				editingDisplay.currencyConversion.RemoveAt(i);
			}
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
		}
		if (ImagePack.DrawButton (pos.x, pos.y, "Add Conversion")) {
			editingDisplay.currencyConversion.Add (new CurrencyConversionEntry (-1, -1, 0));
		}
		
		pos.width *= 2;
		
		pos.y += 2.5f * ImagePack.fieldHeight; 
		// Save data
		pos.x -= ImagePack.innerMargin;
		pos.width /= 3;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Data")) {
			if (newItem)
				InsertEntry ();
			else
				UpdateEntry ();
			
			state = State.Loaded;
		}
		
		// Delete data
		if (!newItem) {
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Data")) {
				DeleteEntry ();
				newSelectedDisplay = 0;
				state = State.Loaded;
			}
		}
		
		// Cancel editing
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Cancel")) {
			editingDisplay = originalDisplay.Clone ();
			if (newItem)
				state = State.New;
			else
				state = State.Loaded;
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText (pos, result);
		}
		
		if (!newItem)
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight + 100);
		else
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight);
	}
	
	// Insert new entries into the table
	void InsertEntry ()
	{
		NewResult ("Inserting...");
		// Setup the update query
		string query = "INSERT INTO " + tableName;		
		query += " (" + editingDisplay.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + editingDisplay.FieldList ("?", ", ") + ") ";
		
		int itemID = -1;
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		
		// Update the database
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);
		
		// If the insert failed, don't insert the spawn marker
		if (itemID != -1) {          
			// Update online table to avoid access the database again			
			editingDisplay.id = itemID;
			// Insert the Conversions
			foreach (CurrencyConversionEntry entry in editingDisplay.currencyConversion) {
				if (entry.currencyToID != -1) {
					entry.currencyID = itemID;
					InsertConversion (entry);
				}
			}
			editingDisplay.isLoaded = true;
			//Debug.Log("ID:" + itemID + "ID2:" + editingDisplay.id);
			dataRegister.Add (editingDisplay.id, editingDisplay);
			displayKeys.Add (editingDisplay.id);
			newItemCreated = true;
			// Configure the correponding prefab
			CreatePrefab ();
			LoadCurrencyOptions();
			NewResult ("New entry inserted");
		} else {
			NewResult ("Error occurred, please check the Console");
		}
	}
	
	void InsertConversion (CurrencyConversionEntry entry)
	{
		string query = "INSERT INTO currency_conversion";		
		query += " (" + entry.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + entry.FieldList ("?", ", ") + ") ";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		
		int itemID = -1;
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);
		
		entry.id = itemID;
	}
	
	// Update existing entries in the table based on the iddemo_table
	void UpdateEntry ()
	{
		NewResult ("Updating...");
		
		// Setup the update query
		string query = "UPDATE " + tableName;
		query += " SET ";
		query += editingDisplay.UpdateList ();
		query += " WHERE id=?id";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int));
		
		// Update the database
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
		
		// Insert/Update the currency conversions
		foreach (CurrencyConversionEntry entry in editingDisplay.currencyConversion) {
			if (entry.currencyToID != -1) {
				if (entry.id < 1) {
					// This is a new entry, insert it
					entry.currencyID = editingDisplay.id;
					InsertConversion (entry);
				} else {
					// This is an existing entry, update it
					entry.currencyID = editingDisplay.id;
					UpdateConversion (entry);
				}
			}
		}
		
		// Delete any conversions that are tagged for deletion
		foreach (int conversionID in editingDisplay.conversionsToBeDeleted) {
			DeleteConversion(conversionID);
		}
		
		// Update online table to avoid access the database again			
		dataRegister [displayKeys [selectedDisplay]] = editingDisplay;
		// Configure the correponding prefab
		CreatePrefab ();
		NewResult ("Entry updated");				
	}
	
	void UpdateConversion (CurrencyConversionEntry entry)
	{
		string query = "UPDATE currency_conversion";		
		query += " SET ";
		query += entry.UpdateList ();
		query += " WHERE id=?id";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, entry.id.ToString (), Register.TypesOfField.Int));
		
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
	}
	
	void DeleteConversion(int conversionID) {
		string query = "UPDATE currency_conversion SET isactive = 0 where id = " + conversionID;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
	}
	
	// Delete entries from the table
	void DeleteEntry ()
	{
		// Remove the prefab
		DeletePrefab ();
		
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, tableName, delete);
		string query = "UPDATE " + tableName + " SET isactive = 0 where id = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Delete the item links;
		query = "UPDATE currency_conversion SET isactive = 0 where currencyID = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Update online table to avoid access the database again		
		dataRegister.Remove (displayKeys [selectedDisplay]);
		displayKeys.Remove (displayKeys [selectedDisplay]);
		if (dataRegister.Count > 0)	{
			LoadSelectList();
			selectedDisplay = -1;
			newSelectedDisplay = 0;
		} else {
			displayList = null;
			dataLoaded = false;
		}
		LoadCurrencyOptions();
	}
	
	void CreatePrefab ()
	{
		// Configure the correponding prefab
		prefab = new CurrencyPrefab (editingDisplay, -1, 1);
		
		foreach (CurrencyConversionEntry conversionEntry in editingDisplay.currencyConversion) {
			if (conversionEntry.autoConverts) {
				prefab.convertsTo = conversionEntry.currencyToID;
				prefab.conversionAmountReq = conversionEntry.amount;
				break;
			}
		}	
		prefab.Save ();
	}
	
	void DeletePrefab ()
	{
		prefab = new CurrencyPrefab (editingDisplay, -1, 1);
		
		if (prefab.Load ())
			prefab.Delete ();
	}
	
	public int GetPositionOfCurrency (int currencyID)
	{
		for (int i = 0; i < currencyIds.Length; i++) {
			if (currencyIds [i] == currencyID)
				return i;
		}
		return 0;
	}
}
