﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Effects Configuration
public class ServerInstanceObjects : AtavismFunction
{

	enum InstanceObjectsView {
		Home,
		InteractiveObjects,
		Regions,
		Graveyards
	}
	
	// Result text
	public string result = "";
	public float resultTimeout = -1;
	public float resultDuration = 5;
	
	InstanceObjectsView view = InstanceObjectsView.Home;
	bool showConfirmDelete = false;
	int bulkEntryCount = 100;
	
	// Use this for initialization
	public ServerInstanceObjects ()
	{	
		functionName = "Instance Objects";		
		
		showConfirmDelete = false;		
	}
	
	public override void Activate()
	{
	}
	
	public void LoadSelectList() 
	{
	}	
	
	// Edit or Create
	public override void Draw (Rect box)
	{
		
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
		
		if (view == InstanceObjectsView.Home) {
			DrawHome(pos);
		} else if (view == InstanceObjectsView.InteractiveObjects) {
			DrawInteractiveObjects(pos);
		} else if (view == InstanceObjectsView.Regions) {
			DrawRegions(pos);
		} else if (view == InstanceObjectsView.Graveyards) {
			DrawGraveyards(pos);
		} 
	}
	
	void DrawHome(Rect pos) {
		ImagePack.DrawLabel (pos.x, pos.y, "Instance Objects");
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Manage Interactive Objects")) {
			view = InstanceObjectsView.InteractiveObjects;
		}
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Manage Regions")) {
			view = InstanceObjectsView.Regions;
		}
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Manage Graveyards")) {
			view = InstanceObjectsView.Graveyards;
		}
		pos.y += ImagePack.fieldHeight;
	}
	
	void DrawInteractiveObjects(Rect pos) {
		ImagePack.DrawLabel (pos.x, pos.y, "Save Interactive Objects");
		pos.width /= 2;
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Back")) {
			view = InstanceObjectsView.Home;
		}
		pos.x -= pos.width;
		pos.width *= 2;
		pos.y += ImagePack.fieldHeight * 2;
		ImagePack.DrawText(pos, "Save the Interactive Objects in the current scene by clicking below.");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "When the Interactive Objects have been saved, save your scene again.");
		pos.y += ImagePack.fieldHeight;
		bulkEntryCount = ImagePack.DrawField(pos, "Bulk Entry Count:", bulkEntryCount);
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Interactive Objects")) {
			GetSceneInteractiveObjects();
			showConfirmDelete = false;
		}
		
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "To delete all Interactive Objects data from this Scene in the Database, click");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "Delete Interactive Objects Data.");
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Delete Interactive Objects Data")) {
			showConfirmDelete = true;
		}
		
		if (showConfirmDelete) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, "Are you sure?");
			pos.y += ImagePack.fieldHeight;
			if (ImagePack.DrawButton (pos.x, pos.y, "Yes, delete")) {
				ClearSavedInstanceInteractiveObjectsData();
				showConfirmDelete = false;
			}
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}
	}
	
	void DrawRegions(Rect pos) {
		ImagePack.DrawLabel (pos.x, pos.y, "Save Regions");
		pos.width /= 2;
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Back")) {
			view = InstanceObjectsView.Home;
		}
		pos.x -= pos.width;
		pos.width *= 2;
		pos.y += ImagePack.fieldHeight * 2;
		ImagePack.DrawText(pos, "Save the Regions in the current scene by clicking below.");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "When the Regions have been saved, save your scene again.");
		pos.y += ImagePack.fieldHeight;
		bulkEntryCount = ImagePack.DrawField(pos, "Bulk Entry Count:", bulkEntryCount);
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Regions")) {
			GetSceneRegions();
			showConfirmDelete = false;
		}
		
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "To delete all region data from this Scene in the Database, click");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "Delete Region Data.");
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Delete Region Data")) {
			showConfirmDelete = true;
		}
		
		if (showConfirmDelete) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, "Are you sure?");
			pos.y += ImagePack.fieldHeight;
			if (ImagePack.DrawButton (pos.x, pos.y, "Yes, delete")) {
				ClearSavedInstanceRegionData();
				showConfirmDelete = false;
			}
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}
	}
	
	void DrawGraveyards(Rect pos) {
		ImagePack.DrawLabel (pos.x, pos.y, "Save Graveyards");
		pos.width /= 2;
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Back")) {
			view = InstanceObjectsView.Home;
		}
		pos.x -= pos.width;
		pos.width *= 2;
		pos.y += ImagePack.fieldHeight * 2;
		ImagePack.DrawText(pos, "Save the Graveyards in the current scene by clicking below.");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "When the Graveyards have been saved, save your scene again.");
		pos.y += ImagePack.fieldHeight;
		bulkEntryCount = ImagePack.DrawField(pos, "Bulk Entry Count:", bulkEntryCount);
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Graveyards")) {
			GetSceneGraveyards();
			showConfirmDelete = false;
		}
		
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "To delete all graveyard data from this Scene in the Database, click");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "Delete Graveyard Data.");
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Delete Graveyard Data")) {
			showConfirmDelete = true;
		}
		
		if (showConfirmDelete) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, "Are you sure?");
			pos.y += ImagePack.fieldHeight;
			if (ImagePack.DrawButton (pos.x, pos.y, "Yes, delete")) {
				ClearSavedInstanceGraveyardData();
				showConfirmDelete = false;
			}
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}
	}
	
	#region Interactive Objects Functions
	void GetSceneInteractiveObjects() {
		Debug.Log("Interactive Object saving start time: " + System.DateTime.Now);
		NewResult ("Saving...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		List<InteractiveObject> preexistingNodes = new List<InteractiveObject>();
		List<InteractiveObject> newNodes = new List<InteractiveObject>();
		
		// Find all resource nodes in the scene
		int itemID = -1;
		InteractiveObject[] interactiveObjects = FindObjectsOfType<InteractiveObject>();
		string query = "";
		foreach (InteractiveObject iObj in interactiveObjects) {
			if (iObj.id > 0) {
				preexistingNodes.Add(iObj);
			} else {
				newNodes.Add(iObj);
			}
		}
		
		string insertNodeQuery = "INSERT INTO interactive_object (name, instance, locX, locY, locZ, interactionType, interactionID, interactionData1,"
				+ "interactionData2, interactionData3, questReqID, interactTimeReq, coordEffect, respawnTime)" + " values ";
		
		query = insertNodeQuery;
		int insertCount = 0;
		
		Debug.Log("Before Insert time: " + System.DateTime.Now);
		foreach (InteractiveObject obj in newNodes) {
			string coordEffect = "";
			if (obj.interactCoordEffect != null)
				coordEffect = obj.interactCoordEffect.name;
			// Insert the new resource node
			if (insertCount > 0)
				query += ",";
			query += "('" + obj.name + "'," + instanceID + "," + obj.transform.position.x + "," + obj.transform.position.y 
				+ "," + obj.transform.position.z + ",'" + obj.interactionType + "'," + obj.interactionID + ",'" + obj.interactionData1
					+ "','" + obj.interactionData2 + "','" + obj.interactionData3 + "'," + obj.questReqID
					+ "," + obj.interactTimeReq + ",'" + coordEffect + "'," + obj.refreshDuration + ")";	
			
			insertCount++;
			if (insertCount == bulkEntryCount) {
				DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
				insertCount = 0;
				query = insertNodeQuery;
			}
			
			obj.id = itemID;
			EditorUtility.SetDirty( obj );
			
			Debug.Log("Finished inserting node at: " + System.DateTime.Now);
		}
		
		if (insertCount != 0) {
			DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
			insertCount = 0;
		}
		
		// Now run a select of all id's in the database for this scene
		query = "Select id, locX, locY, locZ from interactive_object where instance = " + instanceID;
		try
		{
			// Open the connection
			DatabasePack.Connect(DatabasePack.contentDatabasePrefix);
			if (DatabasePack.con.State.ToString() != "Open")
				DatabasePack.con.Open();
			// Use the connections to fetch data
			using (DatabasePack.con)
			{
				using (MySqlCommand cmd = new MySqlCommand(query, DatabasePack.con))
				{
					// Execute the query
					MySqlDataReader data = cmd.ExecuteReader();
					// If there are columns
					if (data.HasRows) {
						int fieldsCount = data.FieldCount;
						while (data.Read()) {
							Vector3 loc = new Vector3(data.GetFloat("locX"), data.GetFloat("locY"), data.GetFloat("locZ"));
							foreach (InteractiveObject obj in newNodes) {
								if (Math.Abs(obj.transform.position.x - loc.x) < 0.1f && Math.Abs(obj.transform.position.y - loc.y) < 0.1f
								    && Math.Abs(obj.transform.position.z - loc.z) < 0.1f) {
									obj.id = data.GetInt32("id");
									break;
								}
							}
						}
					}
					data.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			NewResult ("Error occurred deleting old entries");
		}
		finally
		{
		}
		
		// Now deal with preexisting nodes
		foreach (InteractiveObject obj in preexistingNodes) {
			string coordEffect = "";
			if (obj.interactCoordEffect != null)
				coordEffect = obj.interactCoordEffect.name;
			// Update the existing resource node
			query = "UPDATE interactive_object set name = '" + obj.name + "', locX = " + obj.transform.position.x 
				+ ", locY = " + obj.transform.position.y + ", locZ = " + obj.transform.position.z + ", interactionType = '" 
					+ obj.interactionType + "', interactionID = " + obj.interactionID + ", interactionData1 = '" + obj.interactionData1
					+ "', interactionData2 = '" + obj.interactionData2 + "', interactionData3 = '" + obj.interactionData3 + "', questReqID = " 
					+ obj.questReqID + ", interactTimeReq = " + obj.interactTimeReq + ", coordEffect = '" + coordEffect 
					+ "', respawnTime = " + obj.refreshDuration + " where id = " + obj.id;
			DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, new List<Register>());
		}
		
		NewResult ("Nodes saved");
	}
	
	void ClearSavedInstanceInteractiveObjectsData() {
		NewResult ("Deleting...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		// Now delete all resource nodes in this instance
		Register delete2 = new Register ("instance", "?instance", MySqlDbType.Int32, instanceID.ToString(), Register.TypesOfField.Int);
		DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "interactive_object", delete2, true);
		
		// Finally, reset the id for each node in the scene
		UnityEngine.Object[] resourceNodes = FindObjectsOfType<InteractiveObject>();
		foreach (UnityEngine.Object resourceNodeObj in resourceNodes) {
			InteractiveObject obj = (InteractiveObject)resourceNodeObj;
			obj.id = -1;
			EditorUtility.SetDirty(obj);
		}
		NewResult ("Node data deleted");	
	}
	
	#endregion Interactive Objects Functions
	
	#region Region Functions
	void GetSceneRegions() {
		Debug.Log("Region saving start time: " + System.DateTime.Now);
		NewResult ("Saving...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		List<AtavismRegion> preexistingNodes = new List<AtavismRegion>();
		List<AtavismRegion> newNodes = new List<AtavismRegion>();
		
		// Find all resource nodes in the scene
		int itemID = -1;
		AtavismRegion[] resourceNodes = FindObjectsOfType<AtavismRegion>();
		string query = "";
		foreach (AtavismRegion region in resourceNodes) {
			if (region.id > 0) {
				preexistingNodes.Add(region);
			} else {
				newNodes.Add(region);
			}
		}
		
		string insertNodeQuery = "INSERT INTO region (name, instance, locX, locY, locZ, regionType, actionID, actionData1, actionData2, actionData3)" + " values ";
		
		query = insertNodeQuery;
		int insertCount = 0;
		
		Debug.Log("Before Insert time: " + System.DateTime.Now);
		foreach (AtavismRegion region in newNodes) {
			// Insert the new resource node
			if (insertCount > 0)
				query += ",";
			query += "('" + region.name + "'," + instanceID + "," + region.transform.position.x + "," + region.transform.position.y 
				+ "," + region.transform.position.z + ",'" + region.regionType.ToString() + "'," + region.actionID 
					+ ",'" + region.actionData1 + "','" + region.actionData2 + "','" + region.actionData3 + "')";	
			
			insertCount++;
			if (insertCount == bulkEntryCount) {
				DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
				insertCount = 0;
				query = insertNodeQuery;
			}

			region.id = itemID;
			EditorUtility.SetDirty( region );

			Debug.Log("Finished inserting node at: " + System.DateTime.Now);
		}
		
		if (insertCount != 0) {
			DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
			insertCount = 0;
		}
		
		// Now run a select of all id's in the database for this scene
		query = "Select id, locX, locY, locZ from region where instance = " + instanceID;
		try
		{
			// Open the connection
			DatabasePack.Connect(DatabasePack.contentDatabasePrefix);
			if (DatabasePack.con.State.ToString() != "Open")
				DatabasePack.con.Open();
			// Use the connections to fetch data
			using (DatabasePack.con)
			{
				using (MySqlCommand cmd = new MySqlCommand(query, DatabasePack.con))
				{
					// Execute the query
					MySqlDataReader data = cmd.ExecuteReader();
					// If there are columns
					if (data.HasRows) {
						int fieldsCount = data.FieldCount;
						while (data.Read()) {
							Vector3 loc = new Vector3(data.GetFloat("locX"), data.GetFloat("locY"), data.GetFloat("locZ"));
							foreach (AtavismRegion resourceNode in newNodes) {
								if (Math.Abs(resourceNode.transform.position.x - loc.x) < 0.1f && Math.Abs(resourceNode.transform.position.y - loc.y) < 0.1f
								    && Math.Abs(resourceNode.transform.position.z - loc.z) < 0.1f) {
									resourceNode.id = data.GetInt32("id");
									break;
								}
							}
						}
					}
					data.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			NewResult ("Error occurred deleting old entries");
		}
		finally
		{
		}
		
		// Now go through each node again and save the drops
		insertNodeQuery = "INSERT INTO region_shape (regionID, locX, locY, locZ, shape, size1, size2, size3, loc2X, loc2Y, loc2Z, orientX, orientY" 
			+ ", orientZ, orientW) values ";
		query = insertNodeQuery;
		Debug.Log("Before drop insert time: " + System.DateTime.Now);
		foreach (AtavismRegion region in newNodes) {
			foreach (Collider collider in region.GetComponentsInChildren<Collider>()) {
				// Insert the resource drops
				if (!(collider is SphereCollider) && !(collider is BoxCollider) && (!collider is CapsuleCollider)) {
					Debug.LogWarning("Collider skipped for region " + itemID + " as it is the wrong type");
					continue;
				}
				
				if (collider is SphereCollider) {
					if (insertCount > 0)
						query += ",";
					SphereCollider sphereCollider = (SphereCollider) collider;
					query += GetSphereColliderInsertString(region.id, sphereCollider);
				} else if (collider is CapsuleCollider) {
					if (insertCount > 0)
						query += ",";
					CapsuleCollider capsuleCollider = (CapsuleCollider) collider;
					query += GetCapsuleColliderInsertString(region.id, capsuleCollider);
				} else if (collider is BoxCollider) {
					if (insertCount > 0)
						query += ",";
					BoxCollider boxCollider = (BoxCollider) collider;
					query += GetBoxColliderInsertString(region.id, boxCollider);
				}
				insertCount++;
				if (insertCount == bulkEntryCount) {
					DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
					insertCount = 0;
					query = insertNodeQuery;
				}
			}
		}
		
		if (insertCount != 0) {
			DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
			insertCount = 0;
		}
		Debug.Log("After drop insert time: " + System.DateTime.Now);
		
		// Now deal with preexisting nodes
		foreach (AtavismRegion region in preexistingNodes) {
			// Update the existing resource node
			query = "UPDATE region set name = '" + region.name + "', locX = " + region.transform.position.x 
				+ ", locY = " + region.transform.position.y + ", locZ = " + region.transform.position.z + ", regionType = '" 
					+ region.regionType.ToString() + "', actionID = " + region.actionID + ", actionData1 = '" + region.actionData1
					+ "', actionData2 = '" + region.actionData2 + "', actionData3 = '" + region.actionData3 + "' where id = " + region.id;
			DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, new List<Register>());
			
			// Delete the existing resource drops for this node - not the most efficient, but easier to do
			Register delete = new Register ("regionID", "?regionID", MySqlDbType.Int32, region.id.ToString (), Register.TypesOfField.Int);
			DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "region_shape", delete, false);
			
			// And re-insert them
			foreach (Collider collider in region.GetComponentsInChildren<Collider>()) {
				// Insert the resource drops
				if (!(collider is SphereCollider) && !(collider is BoxCollider) && (!collider is CapsuleCollider)) {
					Debug.LogWarning("Collider skipped for region " + itemID + " as it is the wrong type");
					continue;
				}
				
				query = "INSERT INTO region_shape (regionID, locX, locY, locZ, shape, size1, size2, size3, loc2X, loc2Y, loc2Z, orientX, orientY" 
					+ ", orientZ, orientW) values ";
				
				if (collider is SphereCollider) {
					SphereCollider sphereCollider = (SphereCollider) collider;
					query += GetSphereColliderInsertString(region.id, sphereCollider);
				} else if (collider is CapsuleCollider) {
					CapsuleCollider capsuleCollider = (CapsuleCollider) collider;
					query += GetCapsuleColliderInsertString(region.id, capsuleCollider);
				} else if (collider is BoxCollider) {
					BoxCollider boxCollider = (BoxCollider) collider;
					query += GetBoxColliderInsertString(region.id, boxCollider);
				}
				DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, new List<Register>());
			}
		}
		
		NewResult ("Nodes saved");
	}
	
	string GetSphereColliderInsertString(int id, SphereCollider collider) {
		Vector3 pos1 = collider.transform.position + collider.transform.rotation * collider.center;
		return "(" + id + "," + pos1.x + "," + pos1.y + "," + pos1.z 
			+ ",'sphere'," + collider.radius + ",0,0,0,0,0,0,0,0,1)";
	}
	
	string GetCapsuleColliderInsertString(int id, CapsuleCollider collider) {
		Vector3 pos1 = collider.transform.position + collider.transform.rotation * collider.center;
		Vector3 pos2 = collider.transform.position + collider.transform.rotation * collider.center;
		
		if (collider.direction == 0) {
			pos1 += collider.transform.rotation * Vector3.right * (collider.height / 2);
			pos2 -= collider.transform.rotation * Vector3.right * (collider.height / 2);
		} else if (collider.direction == 1) {
			pos1 += collider.transform.rotation * Vector3.up * (collider.height / 2);
			pos2 -= collider.transform.rotation * Vector3.up * (collider.height / 2);
		} else if (collider.direction == 2) {
			pos1 += collider.transform.rotation * Vector3.forward * (collider.height / 2);
			pos2 -= collider.transform.rotation * Vector3.forward * (collider.height / 2);
		}
		
		return "(" + id + "," + pos1.x + "," + pos1.y + "," + pos1.z 
			+ ",'capsule'," + collider.radius + ",0,0," + pos2.x + "," + pos2.y + "," + pos2.z + ",0,0,0,1)";
	}
	
	string GetBoxColliderInsertString(int id, BoxCollider collider) {
		Vector3 pos1 = collider.transform.position + collider.transform.rotation * collider.center;
		return "(" + id + "," + pos1.x + "," + pos1.y + "," + pos1.z 
			+ ",'box'," + collider.size.x + "," + collider.size.y + "," + collider.size.z + ",0,0,0," + collider.transform.rotation.x 
				+ "," + collider.transform.rotation.y + "," + collider.transform.rotation.z + "," + collider.transform.rotation.w + ")";
	}
	
	void ClearSavedInstanceRegionData() {
		NewResult ("Deleting...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		DatabasePack.con = DatabasePack.Connect(DatabasePack.contentDatabasePrefix);
		
		string query = "delete from region_shape where regionID IN (Select id from region where instance = " + instanceID + ")";
		DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
		
		// Now delete all resource nodes in this instance
		Register delete2 = new Register ("instance", "?instance", MySqlDbType.Int32, instanceID.ToString(), Register.TypesOfField.Int);
		DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "region", delete2, true);
		
		// Finally, reset the id for each node in the scene
		UnityEngine.Object[] resourceNodes = FindObjectsOfType<AtavismRegion>();
		foreach (UnityEngine.Object resourceNodeObj in resourceNodes) {
			AtavismRegion resourceNode = (AtavismRegion)resourceNodeObj;
			resourceNode.id = -1;
			EditorUtility.SetDirty(resourceNode);
		}
		NewResult ("Node data deleted");	
	}
	
	#endregion Region Functions
	
	#region Graveyard Functions
	void GetSceneGraveyards() {
		Debug.Log("Graveyard saving start time: " + System.DateTime.Now);
		NewResult ("Saving...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		List<AtavismGraveyard> preexistingNodes = new List<AtavismGraveyard>();
		List<AtavismGraveyard> newNodes = new List<AtavismGraveyard>();
		
		// Find all resource nodes in the scene
		int itemID = -1;
		AtavismGraveyard[] graveyards = FindObjectsOfType<AtavismGraveyard>();
		string query = "";
		foreach (AtavismGraveyard gy in graveyards) {
			if (gy.id > 0) {
				preexistingNodes.Add(gy);
			} else {
				newNodes.Add(gy);
			}
		}
		
		string insertNodeQuery = "INSERT INTO graveyard (name, instance, locX, locY, locZ, factionReq, factionRepReq)" + " values ";
		
		query = insertNodeQuery;
		int insertCount = 0;
		
		Debug.Log("Before Insert time: " + System.DateTime.Now);
		foreach (AtavismGraveyard gy in newNodes) {
			// Insert the new resource node
			if (insertCount > 0)
				query += ",";
			query += "('" + gy.name + "'," + instanceID + "," + gy.transform.position.x + "," + gy.transform.position.y 
				+ "," + gy.transform.position.z + "," + gy.factionID + ",1)";	
			
			insertCount++;
			if (insertCount == bulkEntryCount) {
				DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
				insertCount = 0;
				query = insertNodeQuery;
			}
			
			gy.id = itemID;
			EditorUtility.SetDirty( gy );
			
			Debug.Log("Finished inserting node at: " + System.DateTime.Now);
		}
		
		if (insertCount != 0) {
			DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
			insertCount = 0;
		}
		
		// Now run a select of all id's in the database for this scene
		query = "Select id, locX, locY, locZ from graveyard where instance = " + instanceID;
		try
		{
			// Open the connection
			DatabasePack.Connect(DatabasePack.contentDatabasePrefix);
			if (DatabasePack.con.State.ToString() != "Open")
				DatabasePack.con.Open();
			// Use the connections to fetch data
			using (DatabasePack.con)
			{
				using (MySqlCommand cmd = new MySqlCommand(query, DatabasePack.con))
				{
					// Execute the query
					MySqlDataReader data = cmd.ExecuteReader();
					// If there are columns
					if (data.HasRows) {
						int fieldsCount = data.FieldCount;
						while (data.Read()) {
							Vector3 loc = new Vector3(data.GetFloat("locX"), data.GetFloat("locY"), data.GetFloat("locZ"));
							foreach (AtavismGraveyard gy in newNodes) {
								if (Math.Abs(gy.transform.position.x - loc.x) < 0.1f && Math.Abs(gy.transform.position.y - loc.y) < 0.1f
								    && Math.Abs(gy.transform.position.z - loc.z) < 0.1f) {
									gy.id = data.GetInt32("id");
									break;
								}
							}
						}
					}
					data.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			NewResult ("Error occurred deleting old entries");
		}
		finally
		{
		}
		
		// Now deal with preexisting nodes
		foreach (AtavismGraveyard gy in preexistingNodes) {
			// Update the existing resource node
			query = "UPDATE graveyard set name = '" + gy.name + "', locX = " + gy.transform.position.x 
				+ ", locY = " + gy.transform.position.y + ", locZ = " + gy.transform.position.z + ", factionReq = " 
					+ gy.factionID + ", factionRepReq = 1 where id = " + gy.id;
			DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, new List<Register>());
		}
		
		NewResult ("Nodes saved");
	}
	
	void ClearSavedInstanceGraveyardData() {
		NewResult ("Deleting...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		// Now delete all resource nodes in this instance
		Register delete2 = new Register ("instance", "?instance", MySqlDbType.Int32, instanceID.ToString(), Register.TypesOfField.Int);
		DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "graveyard", delete2, true);
		
		// Finally, reset the id for each node in the scene
		UnityEngine.Object[] resourceNodes = FindObjectsOfType<AtavismGraveyard>();
		foreach (UnityEngine.Object resourceNodeObj in resourceNodes) {
			AtavismGraveyard gy = (AtavismGraveyard)resourceNodeObj;
			gy.id = -1;
			EditorUtility.SetDirty(gy);
		}
		NewResult ("Node data deleted");	
	}
	
	#endregion Graveyard Functions
	
	public void NewResult(string resultMessage) {
		result = resultMessage;
		resultTimeout = Time.realtimeSinceStartup + resultDuration;
	}
}
