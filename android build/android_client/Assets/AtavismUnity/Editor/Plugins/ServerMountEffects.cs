﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Effects Configuration
public class ServerMountEffects : ServerEffectType
{

	public string effectType = "Mount";
	public string[] effectTypeOptions = new string[] {"MountEffect"};
	public string[] mountTypeOptions = new string[] {"Ground", "Swimming", "Flying"};

	// Use this for initialization
	public ServerMountEffects ()
	{	
	}
	
	public override void LoadOptions(EffectsData editingDisplay, bool newItem) {
	}
	
	// Edit or Create
	public override Rect DrawEditor (Rect pos, bool newItem, EffectsData editingDisplay, out bool showTimeFields)
	{
		editingDisplay.stringValue1 = ImagePack.DrawGameObject (pos, "Model: ", editingDisplay.stringValue1, 0.75f);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.intValue1 = ImagePack.DrawSelector (pos, "Mount Type", editingDisplay.intValue1, mountTypeOptions);
		//editingDisplay.intValue1 = ImagePack.DrawField (pos, "Mount Type:", editingDisplay.intValue1);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.intValue2 = ImagePack.DrawField (pos, "Speed Increase %:", editingDisplay.intValue2);
		pos.y += ImagePack.fieldHeight;
		showTimeFields = false;
		editingDisplay.createPrefab = true;
		return pos;
	}
	
	public override string EffectType {
		get {
			return effectType;
		}
	}
	
	public override string[] EffectTypeOptions {
		get {
			return effectTypeOptions;
		}
	}
}
