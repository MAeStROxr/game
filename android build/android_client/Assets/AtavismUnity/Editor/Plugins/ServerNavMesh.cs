﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Detour;
using Recast;
using Recast.Data;

// Handles the Effects Configuration
public class ServerNavMesh : AtavismFunction
{
	
	private Config config;
	private NavMeshSettings settings;
	
	public string TileIndicator;
	public float BuildTime;
	
	public int Verts { get; set; }
	public int Tris { get; set; }
	
	public float resultDuration = 5;
	float resultTimeout;
	string result;
	
	bool generateFromCentre = false;
	
	// Use this for initialization
	public ServerNavMesh ()
	{	
		functionName = "Nav Mesh";		
		// Init
		settings = AtavismNavMesh.GenerateSettings();
	}
	
	public void NewResult(string resultMessage) {
		result = resultMessage;
		resultTimeout = Time.realtimeSinceStartup + resultDuration;
	}
	
	public override void Draw (Rect box)
	{
		
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
		
		// Draw the content database info
		//pos.y += ImagePack.fieldHeight;
		
		ImagePack.DrawLabel (pos.x, pos.y, "Generate Nav Mesh");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "1) Choose the tag of the objects you want to create a NavMesh of.");
		pos.y += ImagePack.fieldHeight;
		pos.width /= 2;
		settings.Tags = ImagePack.DrawMaskField(pos, "Tags", settings.Tags, UnityEditorInternal.InternalEditorUtility.tags);
		pos.y += ImagePack.fieldHeight * 1.5f;
		settings.TileSize = ImagePack.DrawField(pos, "Tile Size", settings.TileSize);
		pos.y += ImagePack.fieldHeight * 1.5f;
		TileIndicator = ImagePack.DrawGameObject(pos, "Tile Indicator", TileIndicator, pos.width);
		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.width *=2;
		ImagePack.DrawText(pos, "2) Click Generate to generate your NavMesh. It can take a few minutes.");
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Generate")) {
			/*Geometry geom = BuildConfig(settings, out config);
			// create nav mesh object and build nav data (build all tiles)
			GameObject oldgo = GameObject.Find("AtavismNavMesh");
			if (oldgo != null)
				DestroyImmediate(oldgo);
			GameObject go = new GameObject("AtavismNavMesh");
			go.tag = "EditorOnly";
			go.AddComponent<AtavismNavMesh>();
			AtavismNavMesh navMesh = go.GetComponent<AtavismNavMesh>();
			navMesh.settings = settings;
			BuildTime = navMesh.BuildAllTiles(config, geom, settings.TileWidth, settings.TileHeight, settings.MaxPolysPerTile, settings.MaxTiles);*/
			GenerateNavMeshGrid(settings);
		}
		
		pos.y += ImagePack.fieldHeight * 1.5f;
		ImagePack.DrawText(pos, "3) After the NavMesh is generated select the AtavismNavMesh object");
		pos.y += ImagePack.fieldHeight * 0.8f;
		ImagePack.DrawText(pos, "in the scene and click Export. The files are located in the NavMesh");
		pos.y += ImagePack.fieldHeight * 0.8f;
		ImagePack.DrawText(pos, "folder in your project root. ");
		pos.y += ImagePack.fieldHeight * 1.5f;
		ImagePack.DrawText(pos, "4) Copy the scene folder to the NavMesh folder on your server.");
		pos.y += ImagePack.fieldHeight;
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}
		
	}
	
	public void GenerateNavMeshGrid(NavMeshSettings settings) {
		Vector3 origin = Vector3.zero;
		Vector3 size = Vector3.zero;
		
		Terrain[] allObjects = UnityEngine.Object.FindObjectsOfType<Terrain>() ;
		if (allObjects.Length > 0) {
			foreach(Terrain go in allObjects) {
				Debug.Log(go + " is an active object") ;
				if (go.terrainData.size.magnitude > size.magnitude) {
					size = go.terrainData.size;
					origin += go.GetPosition();
				}
			}
		} else {
			// Do object check
			MeshFilter[] allMeshes = UnityEngine.Object.FindObjectsOfType<MeshFilter>() ;
			Bounds b = new Bounds();
			foreach(MeshFilter go in allMeshes) {
				Debug.Log(go + " is an active object") ;
				Bounds meshBounds = go.mesh.bounds;
				meshBounds.center = meshBounds.center + go.transform.position;
				b.Encapsulate(meshBounds);
			}
			origin = b.center;
			size = b.size;
			
			origin = new Vector3(origin.x - (size.x / 2), origin.y, origin.z - (size.z / 2));
		}
		
		int w = (int) ((size.x)/settings.CellSize + 0.5f);
		int h = (int) ((size.z)/settings.CellSize + 0.5f);
		
		int ts = settings.TileSize;
		int tw = (w + ts - 1)/ts;
		int th = (h + ts - 1)/ts;
		
		settings.TileWidth = tw;
		settings.TileHeight = th;
		
		GameObject oldgo = GameObject.Find("AtavismNavMesh");
		if (oldgo != null)
			DestroyImmediate(oldgo);
		GameObject navMeshGO = new GameObject("AtavismNavMesh");
		navMeshGO.tag = "EditorOnly";
		navMeshGO.AddComponent<AtavismNavMesh>();
		AtavismNavMesh navMesh = navMeshGO.GetComponent<AtavismNavMesh>();
		navMesh.tileIndicator = (GameObject)AssetDatabase.LoadAssetAtPath(TileIndicator, typeof(GameObject));
		navMesh.GenerateTiles(settings, origin, size);
	}
	
}
