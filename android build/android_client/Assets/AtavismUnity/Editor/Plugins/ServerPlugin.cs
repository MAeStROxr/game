using UnityEngine;
using UnityEditor;
using System.Collections;

public class ServerPlugin : AtavismPlugin {

	// Use this for initialization
	public ServerPlugin()
	{	
		pluginName = "Server";
		string serverCategory = "AT_button_category_server";

        //System.IO.File.ReadAllBytes
        /*icon = (Texture)Resources.Load (serverCategory, typeof(Texture));
		iconOver = (Texture)Resources.Load (serverCategory + "_over", typeof(Texture));
		iconSelected = (Texture)Resources.Load (serverCategory + "_selected", typeof(Texture));
		icon = new Texture2D(1, 1);
		iconOver = new Texture2D(1, 1);
		iconSelected = new Texture2D(1, 1);
		icon.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + ".png"));
		iconOver.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + "_over.png"));
		iconSelected.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + "_selected.png"));*/

        

    }

    void OnEnable() {
        RegisterFunction(new ServerDataBase(), "Server");
        RegisterFunction(new ServerAtavismCloud(), "Server");
        RegisterFunction(new ServerOptionChoices(), "Server");
        RegisterFunction(new ServerGameSetting(), "Server");
        RegisterFunction(new ServerInstances(), "Server");
        RegisterFunction(new ServerAccounts(), "Server");
        RegisterFunction(new ServerTask(), "Server");
        RegisterFunction(new ServerMobs(), "Mob");
        RegisterFunction(new ServerLootTables(), "Mob");
        RegisterFunction(new ServerMerchantTables(), "Mob");
        RegisterFunction(new ServerFactions(), "Mob");
        RegisterFunction(new ServerQuests(), "Mob");
        RegisterFunction(new ServerDialogues(), "Mob");
        RegisterFunction(new ServerPatrolPaths(), "Mob");
        RegisterFunction(new ServerMobSpawnData(), "Mob");
        RegisterFunction(new ServerItems(), "Item");
        RegisterFunction(new ServerCurrency(), "Item");
        RegisterFunction(new ServerCraftingRecipes(), "Item");
        RegisterFunction(new ServerBuildObject(), "Item");
        RegisterFunction(new ServerSkills(), "Combat");
        RegisterFunction(new ServerAbilities(), "Combat");
        RegisterFunction(new ServerEffects(), "Combat");
        RegisterFunction(new ServerCoordEffects(), "Combat");
        RegisterFunction(new ServerStats(), "Combat");
        RegisterFunction(new ServerDamage(), "Combat");
        RegisterFunction(new ServerCharacter(), "Character");
        RegisterFunction(new ServerLevelXp(), "Character");
        RegisterFunction(new ServerResourceNodes(), "World");
        RegisterFunction(new ServerInstanceObjects(), "World");
        RegisterFunction(new ServerNavMesh(), "World");
        //RegisterFunction("Objectives", "Combat");
        //RegisterFunction("Rewards", "Combat");
    }

    void Awake() {
		string serverCategory = "AT_button_category_server";
		icon = (Texture2D)Resources.Load (serverCategory, typeof(Texture));
		iconOver = (Texture2D)Resources.Load (serverCategory + "_over", typeof(Texture));
		iconSelected = (Texture2D)Resources.Load (serverCategory + "_selected", typeof(Texture));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
