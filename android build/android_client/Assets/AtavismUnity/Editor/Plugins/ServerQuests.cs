﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Quests Configuration
public class ServerQuests : AtavismDatabaseFunction
{

	public Dictionary<int, QuestsData> dataRegister;
	public QuestsData editingDisplay;
	public QuestsData originalDisplay;

	public int[] factionIds = new int[] {-1};
	public string[] factionOptions = new string[] {"~ none ~"};
	
	public int[] itemIds = new int[] {-1};
	public string[] itemsList = new string[] {"~ none ~"};
	
	public int[] currencyIds = new int[] {-1};
	public string[] currencyOptions = new string[] {"~ none ~"};
	
	public static int[] questIds = new int[] {-1};
	public static string[] questOptions = new string[] {"~ none ~"};
	
	public int[] raceIds = new int[] {-1};
	public string[] raceOptions = new string[] {"~ none ~"};
	
	public int[] classIds = new int[] {-1};
	public string[] classOptions = new string[] {"~ none ~"};
	
	public int[] skillIds = new int[] {-1};
	public string[] skillOptions = new string[] {"~ none ~"};
	
	public int[] mobIds = new int[] {-1};
	public string[] mobList = new string[] {"~ none ~"};
	
	public int[] taskIds = new int[] {-1};
	public string[] taskList = new string[] {"~ none ~"};
	
	public int[] requirementIds = new int[] {-1};
	public string[] requirementOptions = new string[] {"~ none ~"};
	
	public string[] statOptions = new string[] {"~ none ~"};
	
	public string[] objectiveTypeOptions = new string[] {"~ none ~"};

	Vector2 descriptionScroll = new Vector2();
	Vector2 objectiveScroll = new Vector2();
	Vector2 progressScroll = new Vector2();
	Vector2 completionScroll = new Vector2();
	
	// Use this for initialization
	public ServerQuests ()
	{	
		functionName = "Quests";		
		// Database tables name
		tableName = "quests";
		functionTitle = "Quests Configuration";
		loadButtonLabel = "Load Quests";
		notLoadedText = "No Quest loaded.";
		// Init
		dataRegister = new Dictionary<int, QuestsData> ();

		editingDisplay = new QuestsData ();		
		originalDisplay = new QuestsData ();
	} 

	public override void Activate()
	{
		linkedTablesLoaded = false;
	}

	public void LoadFactionOptions ()
	{
		string query = "SELECT id, name FROM factions where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			factionOptions = new string[rows.Count + 1];
			factionOptions [optionsId] = "~ none ~"; 
			factionIds = new int[rows.Count + 1];
			factionIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				factionOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				factionIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}

	public static void LoadQuestOptions ()
	{
		string query = "SELECT id, name FROM quests where isactive = 1";
		
		// Load data
		List<Dictionary<string,string>> rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			questOptions = new string[rows.Count + 1];
			questOptions [optionsId] = "~ none ~"; 
			questIds = new int[rows.Count + 1];
			questIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				questOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				questIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}

	public void LoadSkillOptions ()
	{
		string query = "SELECT id, name FROM skills where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			skillOptions = new string[rows.Count + 1];
			skillOptions [optionsId] = "~ none ~"; 
			skillIds = new int[rows.Count + 1];
			skillIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				skillOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				skillIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}

	private void LoadItemList ()
	{
		string query = "SELECT id, name FROM item_templates where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			itemsList = new string[rows.Count + 1];
			itemsList [optionsId] = "~ none ~"; 
			itemIds = new int[rows.Count + 1];
			itemIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				itemsList [optionsId] = data ["id"] + ":" + data ["name"]; 
				itemIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadCurrencyOptions ()
	{
		string query = "SELECT id, name FROM currencies where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			currencyOptions = new string[rows.Count + 1];
			currencyOptions [optionsId] = "~ none ~"; 
			currencyIds = new int[rows.Count + 1];
			currencyIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				currencyOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				currencyIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}

	private void LoadMobList ()
	{
		string query = "SELECT id, name FROM mob_templates where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			mobList = new string[rows.Count + 1];
			mobList [optionsId] = "~ none ~"; 
			mobIds = new int[rows.Count + 1];
			mobIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				mobList [optionsId] = data ["id"] + ":" + data ["name"]; 
				mobIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	private void LoadTaskList ()
	{
		string query = "SELECT id, name FROM task where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			taskList = new string[rows.Count + 1];
			taskList [optionsId] = "~ none ~"; 
			taskIds = new int[rows.Count + 1];
			taskIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				taskList [optionsId] = data ["id"] + ":" + data ["name"]; 
				taskIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadStatOptions ()
	{
		if (!dataLoaded) {
			// Read all entries from the table
			string query = "SELECT name FROM stat where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
			
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			int optionsId = 0;
			if ((rows != null) && (rows.Count > 0)) {
				statOptions = new string[rows.Count + 1];
				statOptions [optionsId] = "~ none ~"; 
				foreach (Dictionary<string,string> data in rows) {
					optionsId++;
					statOptions [optionsId] = data ["name"]; 
				}
			}
		}
	}

	// Load Database Data
	public override void Load ()
	{
		if (!dataLoaded) {
			// Clean old data
			dataRegister.Clear ();
			displayKeys.Clear ();

			// Read all entries from the table
			string query = "SELECT " + originalDisplay.GetFieldsString() + " FROM " + tableName + " where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
		
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			if ((rows!=null) && (rows.Count > 0)) {
				foreach (Dictionary<string,string> data in rows) {
					//foreach(string key in data.Keys)
					//	Debug.Log("Name[" + key + "]:" + data[key]);
					//return;
					QuestsData display = new QuestsData ();

					display.id = int.Parse (data ["id"]);
					display.name = data ["name"]; 
										
					display.category = int.Parse (data ["category"]);
					display.faction = int.Parse (data ["faction"]);
					display.chain = data ["chain"];
					display.level = int.Parse (data ["level"]);
					display.zone = data ["zone"];
					display.numGrades = int.Parse (data ["numGrades"]);
					display.repeatable = bool.Parse (data ["repeatable"]);
					display.description = data ["description"];
					display.objectiveText = data ["objectiveText"];
					display.progressText = data ["progressText"];
					display.deliveryItem1 = int.Parse (data ["deliveryItem1"]);
					display.deliveryItem2 = int.Parse (data ["deliveryItem2"]);
					display.deliveryItem3 = int.Parse (data ["deliveryItem3"]);
					display.questPrereq = int.Parse (data ["questPrereq"]);
					display.questStartedReq = int.Parse (data ["questStartedReq"]);
					display.completionText = data ["completionText"]; 
					display.experience = int.Parse (data ["experience"]);
					display.item1 = int.Parse (data ["item1"]);
					display.item1count = int.Parse (data ["item1count"]);
					display.item2 = int.Parse (data ["item2"]);
					display.item2count = int.Parse (data ["item2count"]);
					display.item3 = int.Parse (data ["item3"]);
					display.item3count = int.Parse (data ["item3count"]);
					display.item4 = int.Parse (data ["item4"]);
					display.item4count = int.Parse (data ["item4count"]);
					display.chooseItem1 = int.Parse (data ["chooseItem1"]);
					display.chooseItem1count = int.Parse (data ["chooseItem1count"]);
					display.chooseItem2 = int.Parse (data ["chooseItem2"]);
					display.chooseItem2count = int.Parse (data ["chooseItem2count"]);
					display.chooseItem3 = int.Parse (data ["chooseItem3"]);
					display.chooseItem3count = int.Parse (data ["chooseItem3count"]);
					display.chooseItem4 = int.Parse (data ["chooseItem4"]);
					display.chooseItem4count = int.Parse (data ["chooseItem4count"]);
					display.currency = int.Parse (data ["currency1"]);
					display.currencyCount = int.Parse (data ["currency1count"]);
					display.currency2 = int.Parse (data ["currency2"]);
					display.currency2count = int.Parse (data ["currency2count"]);
					display.rep1 = int.Parse (data ["rep1"]);
					display.rep1gain = int.Parse (data ["rep1gain"]);
					display.rep2 = int.Parse (data ["rep2"]);
					display.rep2gain = int.Parse (data ["rep2gain"]);

					display.isLoaded = true;
					//Debug.Log("Name:" + display.name  + "=[" +  display.id  + "]");
					dataRegister.Add (display.id, display);
					displayKeys.Add (display.id);
				}
				LoadSelectList ();
			}
			dataLoaded = true;
		}
		foreach(QuestsData questData in dataRegister.Values) {
			LoadQuestObjectives(questData);
			LoadQuestRequirements(questData);
		}
	}

	void LoadQuestObjectives(QuestsData questData)
	{
		// Read all entries from the table
		string query = "SELECT " + new QuestsObjectivesData ().GetFieldsString() + " FROM quest_objectives where questID = " 
			+ questData.id + " AND isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		//Debug.Log("#Rows:"+rows.Count);
		// Read all the data
		if ((rows!=null) && (rows.Count > 0)) {
			foreach (Dictionary<string,string> data in rows) {
				QuestsObjectivesData entry = new QuestsObjectivesData ();
				
				entry.id = int.Parse (data ["id"]);
				entry.objectiveType = data ["objectiveType"]; 
				entry.target = int.Parse (data ["target"]);
				entry.targetCount = int.Parse (data ["targetCount"]);
				entry.targetText = data ["targetText"]; 
				questData.questObjectives.Add(entry);
			}
		}
	}
	
	void LoadQuestRequirements (QuestsData questData)
	{
		// Read all entries from the table
		string query = "SELECT " + new QuestRequirementEntry().GetFieldsString() + " FROM quest_requirement where quest_id = " 
			+ questData.id + " AND isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		//Debug.Log("#Rows:"+rows.Count);
		// Read all the data
		if ((rows != null) && (rows.Count > 0)) {
			foreach (Dictionary<string,string> data in rows) {
				QuestRequirementEntry display = new QuestRequirementEntry ();
				display.id = int.Parse (data ["id"]);
				display.editor_option_type_id = int.Parse (data ["editor_option_type_id"]);
				display.editor_option_choice_type_id = data ["editor_option_choice_type_id"];
				display.required_value = int.Parse (data ["required_value"]);
				questData.questRequirements.Add(display);
			}
		}
	}
	
	public void LoadSelectList ()
	{
		displayList = new string[dataRegister.Count];
		int i = 0;
		foreach (int displayID in dataRegister.Keys) {
			displayList [i] = displayID + ". " + dataRegister [displayID].name;
			i++;
		}
	}
	
	// Draw the loaded list
	public override  void DrawLoaded (Rect box)
	{	
		
		breadCrumb = "";

		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
								
		if (dataRegister.Count <= 0) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "You must create a Quest before edit it.");		
			return;
		}

		// Draw the content database info
		ImagePack.DrawLabel (pos.x, pos.y, "Edit Quest");
		

		if (newItemCreated) {
			newItemCreated = false;
			LoadSelectList ();
			newSelectedDisplay = displayKeys.Count - 1;
		}

		// Draw data Editor
		if (newSelectedDisplay != selectedDisplay) {
			selectedDisplay = newSelectedDisplay;	
			int displayKey = displayKeys [selectedDisplay];
			editingDisplay = dataRegister [displayKey];		
			originalDisplay = editingDisplay.Clone();
		} 
		
		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.x -= ImagePack.innerMargin;
		pos.y -= ImagePack.innerMargin;
		pos.width += ImagePack.innerMargin;
		
		if (state != State.Loaded) {
			pos.x += ImagePack.innerMargin;
			pos.width /= 2;
			//Draw super magical compound object.
			newSelectedDisplay = ImagePack.DrawDynamicPartialListSelector(pos, "Search Filter: ", ref entryFilterInput, selectedDisplay, displayList);
			
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight * 1.5f;
			ImagePack.DrawLabel (pos.x, pos.y, "Quest Properties:");
			pos.y += ImagePack.fieldHeight * 0.75f;
		}
		
		DrawEditor (pos, false);
		
		pos.y -= ImagePack.fieldHeight;
		//pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
	}

	public override void CreateNewData ()
	{
		editingDisplay = new QuestsData ();		
		originalDisplay = new QuestsData ();
		selectedDisplay = -1;
	}

	// Edit or Create
	public override void DrawEditor (Rect box, bool newItem)
	{
		
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;

		if (!linkedTablesLoaded) {	
			LoadFactionOptions();
			LoadQuestOptions();
			LoadSkillOptions();
			LoadItemList ();
			LoadCurrencyOptions();
			LoadMobList();
			LoadTaskList();
			LoadStatOptions();
			objectiveTypeOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Quest Objective Type", false);
			ServerOptionChoices.LoadAtavismChoiceOptions("Requirement", false, out requirementIds, out requirementOptions);
			ServerOptionChoices.LoadAtavismChoiceOptions("Race", false, out raceIds, out raceOptions);
			ServerOptionChoices.LoadAtavismChoiceOptions("Class", false, out classIds, out classOptions);
			linkedTablesLoaded = true;
		}

		// Draw the content database info
		
		if (newItem) {
			ImagePack.DrawLabel (pos.x, pos.y, "Create a new Quest");
			pos.y += 1f*ImagePack.fieldHeight;
		}
		pos.y += 0.5f*ImagePack.fieldHeight;

		editingDisplay.name = ImagePack.DrawField (pos, "Name:", editingDisplay.name, 0.75f);
		pos.y += ImagePack.fieldHeight;
		GUI.Label (pos, "Description:", ImagePack.FieldStyle ());
		pos.height *= 2;
		descriptionScroll = GUI.BeginScrollView(pos, descriptionScroll, new Rect(0, 0, pos.width * 0.75f, 100));
		editingDisplay.description = GUI.TextArea (new Rect (115, 0, pos.width * 0.75f, 100), editingDisplay.description, ImagePack.TextAreaStyle ());
		//editingDisplay.description = ImagePack.DrawField (new Rect(0, 0, pos.width * 0.75f, 200), "Description:", editingDisplay.description, 0.75f, 50);
		GUI.EndScrollView();
		pos.y += 2.2f*ImagePack.fieldHeight;
		pos.height /= 2;
		GUI.Label (pos, "Objective Text:", ImagePack.FieldStyle ());
		pos.height *= 2;
		objectiveScroll = GUI.BeginScrollView(pos, objectiveScroll, new Rect(0, 0, pos.width * 0.75f, 100));
		editingDisplay.objectiveText = GUI.TextArea (new Rect (115, 0, pos.width * 0.75f, 100), editingDisplay.objectiveText, ImagePack.TextAreaStyle ());
		GUI.EndScrollView();
		pos.height /= 2;
		pos.y += 2.2f*ImagePack.fieldHeight;
		GUI.Label (pos, "Progress Text:", ImagePack.FieldStyle ());
		pos.height *= 2;
		progressScroll = GUI.BeginScrollView(pos, progressScroll, new Rect(0, 0, pos.width * 0.75f, 100));
		editingDisplay.progressText = GUI.TextArea (new Rect (115, 0, pos.width * 0.75f, 100), editingDisplay.progressText, ImagePack.TextAreaStyle ());
		GUI.EndScrollView();
		pos.height /= 2;
		pos.y += 2.2f*ImagePack.fieldHeight;
		GUI.Label (pos, "Completion Text:", ImagePack.FieldStyle ());
		pos.height *= 2;
		completionScroll = GUI.BeginScrollView(pos, completionScroll, new Rect(0, 0, pos.width * 0.75f, 100));
		editingDisplay.completionText = GUI.TextArea (new Rect (115, 0, pos.width * 0.75f, 100), editingDisplay.completionText, ImagePack.TextAreaStyle ());
		GUI.EndScrollView();
		pos.height /= 2;
		pos.y += 2.2f*ImagePack.fieldHeight;
		pos.width /= 2;
		//editingDisplay.category = ImagePack.DrawField (pos, "Category:", editingDisplay.category);
		//pos.x += pos.width;
		editingDisplay.faction = ImagePack.DrawField (pos, "Faction:", editingDisplay.faction);
		pos.x += pos.width;
		editingDisplay.level = ImagePack.DrawField (pos, "Level:", editingDisplay.level);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.chain = ImagePack.DrawField (pos, "Chain:", editingDisplay.chain);
		pos.x += pos.width;
		//editingDisplay.zone = ImagePack.DrawField (pos, "Zone:", editingDisplay.zone);
		//pos.x += pos.width;
		//editingDisplay.numGrades = ImagePack.DrawField (pos, "Num. Grades:", editingDisplay.numGrades);
		//pos.x += pos.width;
		editingDisplay.repeatable = ImagePack.DrawToggleBox (pos, "Is Repeatable?", editingDisplay.repeatable);
		pos.x -= pos.width;
		//pos.x -= 2*pos.width;	
		pos.width *= 2;
		pos.y += 1.5f*ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Items Given");		
		pos.y += 1.5f*ImagePack.fieldHeight;
		pos.width /= 2;
		int selectedItem = GetOptionPosition (editingDisplay.deliveryItem1, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 1:", selectedItem, itemsList);
		editingDisplay.deliveryItem1 = itemIds[selectedItem];
		pos.x += pos.width;
		selectedItem = GetOptionPosition (editingDisplay.deliveryItem2, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 2:", selectedItem, itemsList);
		editingDisplay.deliveryItem2 = itemIds[selectedItem];
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.deliveryItem3, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 3:", selectedItem, itemsList);
		editingDisplay.deliveryItem3 = itemIds[selectedItem];
		pos.width *= 2;
		pos.y += 1.5f*ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Quest Prerequisites");		
		pos.y += 1.5f*ImagePack.fieldHeight;
		//pos.width *= 3;
		pos.width /= 2;
		int selectedQuest = GetOptionPosition (editingDisplay.questPrereq, questIds); 
		selectedQuest = ImagePack.DrawSelector (pos, "Quest Completed:", selectedQuest, questOptions);
		editingDisplay.questPrereq = questIds [selectedQuest];
		pos.x += pos.width;
		selectedQuest = GetOptionPosition (editingDisplay.questStartedReq, questIds);
		selectedQuest = ImagePack.DrawSelector (pos, "Quest Started:", selectedQuest, questOptions);
		editingDisplay.questStartedReq = questIds [selectedQuest];
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		pos.width *= 2;
		
		// Requirements area
		pos.y += 1.5f * ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Requirements");
		pos.y += 1.5f * ImagePack.fieldHeight;
		pos.width /= 2;
		for (int i = 0; i < editingDisplay.questRequirements.Count; i++) {
			int selectedRequirement = GetOptionPosition (editingDisplay.questRequirements [i].editor_option_type_id, requirementIds);
			selectedRequirement = ImagePack.DrawSelector (pos, "Type:", selectedRequirement, requirementOptions);
			editingDisplay.questRequirements[i].editor_option_type_id = requirementIds [selectedRequirement];
			pos.x += pos.width;
			if (requirementOptions[selectedRequirement] == "Race") {
				int raceID = 0;
				int.TryParse(editingDisplay.questRequirements [i].editor_option_choice_type_id, out raceID);
				int selectedRace = GetOptionPosition (raceID, raceIds);
				selectedRace = ImagePack.DrawSelector (pos, "Race:", selectedRace, raceOptions);
				editingDisplay.questRequirements[i].editor_option_choice_type_id = raceIds [selectedRace].ToString();
			} else if (requirementOptions[selectedRequirement] == "Class") {
				int classID = 0;
				int.TryParse(editingDisplay.questRequirements [i].editor_option_choice_type_id, out classID);
				int selectedClass = GetOptionPosition (classID, classIds);
				selectedClass = ImagePack.DrawSelector (pos, "Class:", selectedClass, classOptions);
				editingDisplay.questRequirements[i].editor_option_choice_type_id = classIds [selectedClass].ToString();
			} else if (requirementOptions[selectedRequirement] == "Level") {
				editingDisplay.questRequirements[i].required_value = ImagePack.DrawField (pos, "Level:", editingDisplay.questRequirements[i].required_value);
			} else if (requirementOptions[selectedRequirement] == "Skill Level") {
				int skillID = 0;
				int.TryParse(editingDisplay.questRequirements [i].editor_option_choice_type_id, out skillID);
				int selectedSkill = GetOptionPosition (skillID, skillIds);
				selectedSkill = ImagePack.DrawSelector (pos, "Skill:", selectedSkill, skillOptions);
				editingDisplay.questRequirements[i].editor_option_choice_type_id = skillIds [selectedSkill].ToString();
				pos.y += ImagePack.fieldHeight;
				editingDisplay.questRequirements[i].required_value = ImagePack.DrawField (pos, "Level:", editingDisplay.questRequirements[i].required_value);
			} else if (requirementOptions[selectedRequirement] == "Stat") {
				editingDisplay.questRequirements[i].editor_option_choice_type_id = ImagePack.DrawSelector (pos, "Stat:", editingDisplay.questRequirements[i].editor_option_choice_type_id, statOptions);
				pos.y += ImagePack.fieldHeight;
				editingDisplay.questRequirements[i].required_value = ImagePack.DrawField (pos, "Value:", editingDisplay.questRequirements[i].required_value);
			} else if (requirementOptions[selectedRequirement] == "Faction") {
				int factionID = 0;
				int.TryParse(editingDisplay.questRequirements [i].editor_option_choice_type_id, out factionID);
				int selectedFaction = GetOptionPosition (factionID, factionIds);
				selectedFaction = ImagePack.DrawSelector (pos, "Faction:", selectedFaction, factionOptions);
				editingDisplay.questRequirements[i].editor_option_choice_type_id = factionIds [selectedFaction].ToString();
				pos.y += ImagePack.fieldHeight;
				//editingDisplay.questRequirements[i].required_value = ImagePack.DrawField (pos, "Reputation:", editingDisplay.questRequirements[i].required_value);
				int selectedStance = FactionData.GetPositionOfStance(editingDisplay.questRequirements[i].required_value);
				selectedStance = ImagePack.DrawSelector (pos, "Stance:", selectedStance, FactionData.stanceOptions);
				editingDisplay.questRequirements[i].required_value = FactionData.stanceValues[selectedStance];
			}
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Remove Requirement")) {
				if (editingDisplay.questRequirements[i].id > 0)
					editingDisplay.questRequirementsToBeDeleted.Add(editingDisplay.questRequirements[i].id);
				editingDisplay.questRequirements.RemoveAt(i);
			}
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
		}
		if (ImagePack.DrawButton (pos.x, pos.y, "Add Requirement")) {
			editingDisplay.questRequirements.Add (new QuestRequirementEntry (-1, -1, ""));
		}
		
		pos.width *= 2;

		pos.y += 1.5f*ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos. x, pos.y, "Objectives");
		pos.y += ImagePack.fieldHeight;
		/*if (editingDisplay.questObjectives.Count == 0) {
			editingDisplay.questObjectives.Add(new QuestsObjectivesData());
		}*/
		for (int i = 0; i < editingDisplay.questObjectives.Count; i++) {
			pos.width /= 2;
			editingDisplay.questObjectives[i].objectiveType = ImagePack.DrawSelector (pos, "Type " + (i+1) + ":", 
				editingDisplay.questObjectives[i].objectiveType, objectiveTypeOptions);
			pos.x += pos.width;
			if (editingDisplay.questObjectives[i].objectiveType == "item") {
				selectedItem = GetOptionPosition (editingDisplay.questObjectives[i].target, itemIds);
				selectedItem = ImagePack.DrawSelector (pos, "Target " + (i+1) + ":", selectedItem, itemsList);
				editingDisplay.questObjectives[i].target = itemIds[selectedItem];
			} else if (editingDisplay.questObjectives[i].objectiveType == "mob") {
				int selectedMob = GetOptionPosition (editingDisplay.questObjectives[i].target, mobIds);
				selectedMob = ImagePack.DrawSelector (pos, "Target " + (i+1) + ":", selectedMob, mobList);
				editingDisplay.questObjectives[i].target = mobIds[selectedMob];
			} else if (editingDisplay.questObjectives[i].objectiveType == "task") {
				int selectedTask = GetOptionPosition (editingDisplay.questObjectives[i].target, taskIds);
				selectedTask = ImagePack.DrawSelector (pos, "Target " + (i+1) + ":", selectedTask, taskList);
				editingDisplay.questObjectives[i].target = taskIds[selectedTask];
			}
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
			editingDisplay.questObjectives[i].targetCount = ImagePack.DrawField (pos, "Count:", editingDisplay.questObjectives[i].targetCount);
			pos.y += ImagePack.fieldHeight;
			pos.width *= 2;
			editingDisplay.questObjectives[i].targetText = ImagePack.DrawField (pos, "Text:", editingDisplay.questObjectives[i].targetText, 1.4f);
			pos.width /= 2;
			pos.y += ImagePack.fieldHeight;
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Objective")) {
				if (editingDisplay.questObjectives[i].id > 0)
					editingDisplay.objectivesToBeDeleted.Add(editingDisplay.questObjectives[i].id);
				editingDisplay.questObjectives.RemoveAt(i);
			}
			pos.x -= pos.width;
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight;
		}
		if (ImagePack.DrawButton (pos.x, pos.y, "Add Objective")) {
			editingDisplay.questObjectives.Add(new QuestsObjectivesData());
		}

		pos.y += 1.5f*ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos. x, pos.y, "Rewards");
		pos.y += ImagePack.fieldHeight;

		pos.width /= 2;
		editingDisplay.experience = ImagePack.DrawField (pos, "Experience:", editingDisplay.experience);
		//pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.item1, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 1:", selectedItem, itemsList);
		editingDisplay.item1 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.item1count = ImagePack.DrawField (pos, "Count:", editingDisplay.item1count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.item2, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 2:", selectedItem, itemsList);
		editingDisplay.item2 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.item2count = ImagePack.DrawField (pos, "Count:", editingDisplay.item2count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.item3, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 3:", selectedItem, itemsList);
		editingDisplay.item3 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.item3count = ImagePack.DrawField (pos, "Count:", editingDisplay.item3count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.item4, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item 4:", selectedItem, itemsList);
		editingDisplay.item4 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.item4count = ImagePack.DrawField (pos, "Count:", editingDisplay.item4count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.chooseItem1, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item Choice 1:", selectedItem, itemsList);
		editingDisplay.chooseItem1 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.chooseItem1count = ImagePack.DrawField (pos, "Count:", editingDisplay.chooseItem1count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.chooseItem2, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item Choice 2:", selectedItem, itemsList);
		editingDisplay.chooseItem2 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.chooseItem2count = ImagePack.DrawField (pos, "Count:", editingDisplay.chooseItem2count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.chooseItem3, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item Choice 3:", selectedItem, itemsList);
		editingDisplay.chooseItem3 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.chooseItem3count = ImagePack.DrawField (pos, "Count:", editingDisplay.chooseItem3count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedItem = GetOptionPosition (editingDisplay.chooseItem4, itemIds);
		selectedItem = ImagePack.DrawSelector (pos, "Item Choice 4:", selectedItem, itemsList);
		editingDisplay.chooseItem4 = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.chooseItem4count = ImagePack.DrawField (pos, "Count:", editingDisplay.chooseItem4count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		int selectedCurrency = GetOptionPosition(editingDisplay.currency, currencyIds);
		selectedCurrency = ImagePack.DrawSelector (pos, "Currency:", selectedCurrency, currencyOptions);
		editingDisplay.currency = currencyIds[selectedCurrency];
		pos.x += pos.width;
		editingDisplay.currencyCount = ImagePack.DrawField (pos, "Count:", editingDisplay.currencyCount);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		selectedCurrency = GetOptionPosition(editingDisplay.currency2, currencyIds);
		selectedCurrency = ImagePack.DrawSelector (pos, "Currency 2:", selectedCurrency, currencyOptions);
		editingDisplay.currency2 = currencyIds[selectedCurrency];
		pos.x += pos.width;
		editingDisplay.currency2count = ImagePack.DrawField (pos, "Count:", editingDisplay.currency2count);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		int repID = GetOptionPosition (editingDisplay.rep1, factionIds);
		repID = ImagePack.DrawSelector (pos, "Faction 1:", repID, factionOptions);
		editingDisplay.rep1 = factionIds[repID];
		pos.x += pos.width;
		editingDisplay.rep1gain = ImagePack.DrawField (pos, "Rep:", editingDisplay.rep1gain);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		repID = GetOptionPosition (editingDisplay.rep2, factionIds);
		repID = ImagePack.DrawSelector (pos, "Faction 2:", repID, factionOptions);
		editingDisplay.rep2 = factionIds[repID];
		pos.x += pos.width;
		editingDisplay.rep2gain = ImagePack.DrawField (pos, "Rep:", editingDisplay.rep2gain);
		pos.x -= pos.width;
		pos.width *= 2;

		// Save data
		pos.x -= ImagePack.innerMargin;
		pos.y += 1.4f * ImagePack.fieldHeight;
		pos.width /= 3;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Data")) {
			if (newItem)
				InsertEntry ();
			else
				UpdateEntry ();
			
			state = State.Loaded;
		}
		
		// Delete data
		if (!newItem) {
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Data")) {
				DeleteEntry ();
				newSelectedDisplay = 0;
				state = State.Loaded;
			}
		}
		
		// Cancel editing
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Cancel")) {
			editingDisplay = originalDisplay.Clone();
			if (newItem)
				state = State.New;
			else
				state = State.Loaded;
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}

		if (!newItem)
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight + 100);
		else
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight);
	}
	
	// Insert new entries into the table
	void InsertEntry ()
	{
		NewResult("Inserting...");
		// Setup the update query
		string query = "INSERT INTO " + tableName;		
		query += " (" + editingDisplay.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + editingDisplay.FieldList ("?", ", ") + ") ";
		
		int itemID = -1;

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));
		}
		
		// Update the database
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);

		// If the insert failed, don't insert the spawn marker
		if (itemID != -1) { 
			// Insert the Requirements
			foreach (QuestRequirementEntry entry in editingDisplay.questRequirements) {
				if (entry.editor_option_type_id != -1) {
					entry.questID = itemID;
					InsertRequirement (entry);
				}
			}
			// Insert the objectives
			foreach(QuestsObjectivesData entry in editingDisplay.questObjectives) {
				if (entry.target != -1) {
					entry.questID = itemID;
					InsertObjective(entry);
				}
			}
			// Update online table to avoid access the database again			
			editingDisplay.id = itemID;
			editingDisplay.isLoaded = true;
			//Debug.Log("ID:" + itemID + "ID2:" + editingDisplay.id);
			dataRegister.Add (editingDisplay.id, editingDisplay);
			displayKeys.Add (editingDisplay.id);
			newItemCreated = true;
			NewResult("New entry inserted");
		} else {
			NewResult("Error occurred, please check the Console");
		}
	}
	
	void InsertRequirement(QuestRequirementEntry entry)
	{
		string query = "INSERT INTO quest_requirement";		
		query += " (" + entry.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + entry.FieldList ("?", ", ") + ") ";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		
		int itemID = -1;
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);
		
		entry.id = itemID;
	}

	void InsertObjective(QuestsObjectivesData entry) 
	{
		string query = "INSERT INTO quest_objectives";		
		query += " (" + entry.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + entry.FieldList ("?", ", ") + ") ";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));
		}
		
		int itemID = -1;
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);
		
		entry.id = itemID;
	}

	// Update existing entries in the table based on the iddemo_table
	void UpdateEntry ()
	{
		NewResult("Updating...");
		// Setup the update query
		string query = "UPDATE " + tableName;
		query += " SET ";
		query += editingDisplay.UpdateList ();
		query += " WHERE id=?id";

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int));
	
		// Update the database
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
		
		// Insert/Update the requirements
		foreach (QuestRequirementEntry entry in editingDisplay.questRequirements) {
			if (entry.editor_option_type_id != -1) {
				if (entry.id < 1) {
					// This is a new entry, insert it
					entry.questID = editingDisplay.id;
					InsertRequirement (entry);
				} else {
					// This is an existing entry, update it
					entry.questID = editingDisplay.id;
					UpdateRequirement (entry);
				}
			}
		}
		
		// Delete any requirements that are tagged for deletion
		foreach (int requirementID in editingDisplay.questRequirementsToBeDeleted) {
			DeleteRequirement(requirementID);
		}

		// Insert/Update the objectives
		foreach(QuestsObjectivesData entry in editingDisplay.questObjectives) {
			if (entry.target != -1) {
				if (entry.id == 0) {
					// This is a new entry, insert it
					entry.questID = editingDisplay.id;
					InsertObjective(entry);
				} else {
					// This is an existing entry, update it
					entry.questID = editingDisplay.id;
					UpdateObjective(entry);
				}
			}
		}
		// And now delete any Objectives that are tagged for deletion
		foreach (int objectiveID in editingDisplay.objectivesToBeDeleted) {
			DeleteObjective(objectiveID);
		}
				
		// Update online table to avoid access the database again			
		dataRegister [displayKeys [selectedDisplay]] = editingDisplay;		
		NewResult("Entry updated");		
	}
	
	void UpdateRequirement (QuestRequirementEntry entry)
	{
		string query = "UPDATE quest_requirement";		
		query += " SET ";
		query += entry.UpdateList ();
		query += " WHERE id=?id";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, entry.id.ToString (), Register.TypesOfField.Int));
		
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
	}
	
	void DeleteRequirement(int requirementID) {
		string query = "UPDATE quest_requirement SET isactive = 0 where id = " + requirementID;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
	}

	void UpdateObjective(QuestsObjectivesData entry) 
	{
		string query = "UPDATE quest_objectives";		
		query += " SET ";
		query += entry.UpdateList ();
		query += " WHERE id=?id";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, entry.id.ToString (), Register.TypesOfField.Int));
		
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
	}
	
	void DeleteObjective(int objectiveID) {
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, objectiveID.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "quest_objectives", delete);
		string query = "UPDATE quest_objectives SET isactive = 0 where id = " + objectiveID;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
	}
	
	// Delete entries from the table
	void DeleteEntry ()
	{
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, tableName, delete);
		string query = "UPDATE " + tableName + " SET isactive = 0 where id = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		query = "UPDATE quest_objectives SET isactive = 0 where questID = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Update online table to avoid access the database again		
		dataRegister.Remove (displayKeys [selectedDisplay]);
		displayKeys.Remove (displayKeys [selectedDisplay]);
		if (dataRegister.Count > 0)	{
			LoadSelectList();
			selectedDisplay = -1;
			newSelectedDisplay = 0;
		} else {
			displayList = null;
			dataLoaded = false;
		}
	}

}
