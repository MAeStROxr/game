﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CurrencyPrefab {
	
	// Prefab Parameters
	public CurrencyData currencyData;
	public int convertsTo = -1;
	public int conversionAmountReq = 1;
	
	// Prefab file information
	private string prefabName;
	private string prefabPath;
	// Common Prefab Prefix and Sufix
	private string itemPrefix = "Currency";
	private string itemSufix = ".prefab";
	// Base path
	private string basePath = "";
	// Example Item Prefab Information
	private string basePrefab = "Example Currency Prefab.prefab";
	private string basePrefabPath;
	
	public CurrencyPrefab() {
		basePath = AtavismUnityUtility.GetAssetPath(basePrefab);
		basePrefabPath = basePath + basePrefab;
	}
	
	public CurrencyPrefab(CurrencyData currencyData, int convertsTo, int conversionAmountReq) {
		this.currencyData = currencyData;
		this.convertsTo = convertsTo;
		this.conversionAmountReq = conversionAmountReq;
		
		basePath = AtavismUnityUtility.GetAssetPath(basePrefab);
		prefabName = itemPrefix + currencyData.name + itemSufix;
		prefabPath = basePath + prefabName;
		basePrefabPath = basePath + basePrefab;
	}
	
	public void Save(CurrencyData currencyData)
	{
		this.currencyData = currencyData;
		this.Save ();
	}
	
	// Save data from the class to the new prefab, creating one if it doesnt exist
	public void Save() {
		DeletePrefabWithIDAndDifferingName(currencyData.id, currencyData.name);
		GameObject item = (GameObject) AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));
		
		// If this is a new prefab
		if (item == null) {
			AssetDatabase.CopyAsset(basePrefabPath, prefabPath);
			AssetDatabase.Refresh();
			item = (GameObject) AssetDatabase.LoadAssetAtPath(prefabPath,  typeof(GameObject));
		}
		
		item.GetComponent<Currency>().id = currencyData.id;
		item.GetComponent<Currency>().name = currencyData.name;
		Sprite icon = (Sprite) AssetDatabase.LoadAssetAtPath(currencyData.icon, typeof(Sprite));
		if (icon != null)
			item.GetComponent<Currency>().icon = icon;
		item.GetComponent<Currency>().group = currencyData.currencyGroup;
		item.GetComponent<Currency>().position = currencyData.currencyPosition;
		
		item.GetComponent<Currency>().convertsTo = convertsTo;
		item.GetComponent<Currency>().conversionAmountReq = conversionAmountReq;
		
		EditorUtility.SetDirty(item);
		AssetDatabase.Refresh();
	}
	
	public void Delete() {
		GameObject item = (GameObject) AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject));
		
		// If this is a new prefab
		if (item != null) {
			AssetDatabase.DeleteAsset(prefabPath);
			AssetDatabase.Refresh();
		}
	}
	
	// Load data from the prefab base on its name
	// return true if the prefab exist and false if there is no prefab
	public bool Load() {
		
		GameObject item = (GameObject) AssetDatabase.LoadAssetAtPath(prefabPath,  typeof(GameObject));
		
		// If this is a new prefab
		if (item == null) 
			return false;
		
		currencyData = new CurrencyData();
		currencyData.id = item.GetComponent<Currency>().id;
		currencyData.name = item.GetComponent<Currency>().name;
		currencyData.currencyGroup = item.GetComponent<Currency>().group;
		currencyData.currencyPosition = item.GetComponent<Currency>().position;
		
		return true;
	}
	
	public static void DeletePrefabWithIDAndDifferingName(int id, string name) {
		CurrencyPrefab temp = new CurrencyPrefab();
		string[] prefabPaths = Directory.GetFiles(temp.basePath, "*.prefab", SearchOption.AllDirectories);
		foreach (string prefabPath in prefabPaths) {
			GameObject item = (GameObject) AssetDatabase.LoadAssetAtPath(prefabPath,  typeof(GameObject));
			if (item.GetComponent<Currency>() != null && item.GetComponent<Currency>().id == id 
					&& item.GetComponent<Currency>().name != name)
				AssetDatabase.DeleteAsset(prefabPath);
		}
		AssetDatabase.Refresh();
	}
}
