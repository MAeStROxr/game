﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class BattleCameras : MonoBehaviour {
    public Camera main, battle;
    public EventSystem eventSystem;
	// Use this for initialization
	void Start () {
        battle.clearFlags = CameraClearFlags.Depth;
        main.clearFlags = CameraClearFlags.SolidColor;
        //DontDestroyOnLoad(main.gameObject);
        //DontDestroyOnLoad(eventSystem.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
