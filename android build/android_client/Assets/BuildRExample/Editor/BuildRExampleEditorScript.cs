﻿// Mapity BuildR Example
// Copyright (c) 2016 Rewind Game Studio http://www.rewindgamestudio.com
// Support contact support@rewindgamestudio.com
//
// This code and information are provided "as is" without warranty of any 
// Kind, either expressed or implied, including but not limited to the
// Implied warranties of merchantability and/or fitness for a
// Particular purpose.

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BuildRExampleScript))]
public class BuildRExampleEditorScript : Editor 
{	
	bool showMapityNotLoadedMessage = false;
    [SerializeField]
    ControlPoint point;
	public override void OnInspectorGUI()
	{
		BuildRExampleScript myTarget = (BuildRExampleScript)target;
		
		if(GUILayout.Button("Create Buildings"))
		{
			if( Mapity.Singleton.HasLoaded() && !Mapity.Singleton.IsLoadingInProgress() )
			{
				showMapityNotLoadedMessage = false;

                myTarget.CreateBuildings();
			}
			else
			{
				showMapityNotLoadedMessage = true;
			}
		}

        point = (ControlPoint)EditorGUILayout.ObjectField("Control Point Prefab", point, typeof(ControlPoint),true);
        ((BuildRExampleScript)target).controlBuilding = point;
        if ( showMapityNotLoadedMessage )
		{
			EditorGUILayout.HelpBox("Map-ity isn't loaded", MessageType.Error );
		}
	}
}