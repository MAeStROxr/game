// Mapity BuildR Example
// Copyright (c) 2015 Rewind Game Studio http://www.rewindgamestudio.com
// Support contact support@rewindgamestudio.com
//
// This code and information are provided "as is" without warranty of any 
// Kind, either expressed or implied, including but not limited to the
// Implied warranties of merchantability and/or fitness for a
// Particular purpose.