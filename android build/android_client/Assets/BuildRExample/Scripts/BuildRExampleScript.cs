﻿// Mapity BuildR Example
// Copyright (c) 2014 Rewind Game Studio http://www.rewindgamestudio.com
// Support contact support@rewindgamestudio.com
//
// This code and information are provided "as is" without warranty of any 
// Kind, either expressed or implied, including but not limited to the
// Implied warranties of merchantability and/or fitness for a
// Particular purpose.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mygame;
using FlatBuffers;
using mygame.proto;
using System.IO;

[ExecuteInEditMode]
public class BuildRExampleScript : MonoBehaviour 
{
#if UNITY_EDITOR
	// BuildR Contraints
	BuildrGenerateConstraints constraints;

	// GameObject to hold a temporary copy of a building
    GameObject model = null;

	// Parent object to store buildings under
	GameObject buildingsGameObject = null;

    /// <summary>
    /// Creates the buildings.
    /// </summary>
    public int maxBuildings = 50;
    public ControlPoint controlBuilding = null;
    public void CreateBuildings()
	{
       
        List<Offset<ControlPointMsg>> pointOffsets = new List<Offset<ControlPointMsg>>();
        FlatBufferBuilder fbb = new FlatBufferBuilder(1000);
        // Create BuildR constraints
        constraints = ScriptableObject.CreateInstance<BuildrGenerateConstraints>();

        // Init the constraints
        constraints.Init();

        // Use the plan we will provided
        constraints.constrainPlanByPlan = true;

        // Destroy old one if it exists
        if ( buildingsGameObject != null )
		{
			DestroyImmediate( buildingsGameObject );
		}

		buildingsGameObject = new GameObject( "Buildings" );
        int m = 0;
		// Loop over all the buildings in Map-ity
		foreach(Mapity.MapBuilding mapBuilding in Mapity.Singleton.mapBuildings.Values )
		{
            m++;
            //if (m > maxBuildings) break;
			// Create a new GameObject for this building with it's id as it's name
            model = Instantiate(controlBuilding.gameObject);
            
            ControlPoint point = model.GetComponent<ControlPoint>();
			// Set the "Buildings" GameObject as it's parent
			model.transform.parent = buildingsGameObject.transform;

			// Add the building script to the building
			BuildingScript buildingScript =  model.AddComponent<BuildingScript>();

            
            string buildName = null;
            point.startingOID = mapBuilding.id;
           




            // Loop over the ways which define this building(usually 1)
            for (int i = 0; i < mapBuilding.buildingWays.Count; i++)
			{
                // Get the way
                Mapity.MapWay buildingWay = (Mapity.MapWay)mapBuilding.buildingWays[i];


                //get info
                Hashtable tags = buildingWay.tags.GetTags();
                if (tags.ContainsKey("name:he")) buildName = tags["name:he"] as string;
                else if (tags.ContainsKey("name")) buildName = tags["name"] as string;
                else if (tags.ContainsKey("addr:housenumber")) {
                    buildName = tags["addr:housenumber"] as string;
                    if (tags.ContainsKey("addr:street")) buildName = tags["addr:street"] as string + ":" + buildName;
                    
                }
                else if (tags.ContainsKey("building:use")) buildName = tags["building:use"] as string;
                else buildName = mapBuilding.id.ToString();
                if (ContainsHebrew(buildName)) buildName = Reverse(buildName);
                if (buildName!=null) point.pointName = buildName;

                // NULL check
                if ( buildingWay != null )
				{
					// Create a BuildRPlan
					BuildrPlan plan = ScriptableObject.CreateInstance<BuildrPlan>();

					// Container for the nodes which make up the floor plan
					List<Vector2z> bounds = new List<Vector2z>();

                    // Cache node count to be used in average height calculation 
                    int nodeCount = buildingWay.wayMapNodes.Count;

                    // Minimum height of a building
                    float mimimumHeight = float.MaxValue;

					// Loop over the nodes
                    for (int j = 0; j < nodeCount-1; j++)
					{
						// Get the node
						Mapity.MapNode node = (Mapity.MapNode)buildingWay.wayMapNodes[j];

                        // Get height of node
                        float nodeHeight = node.position.world.ToVector().y;

                        // Add to total height
                        mimimumHeight = Mathf.Min(mimimumHeight, nodeHeight);

                        Vector2z boundNode = new Vector2z(node.position.world.ToVector().x, node.position.world.ToVector().z);

                        // Add it to the bounds
                        if (!bounds.Contains(boundNode))
                        {
                            bounds.Add(boundNode);
                        }
                    }

					// Add the bounds to the plan
					plan.AddVolume(bounds.ToArray());
                    Vector2z xy = new Vector2z(0,0);
                    bounds.ForEach(b=>xy += b);
                    xy= new Vector2z(xy.x/bounds.Count,xy.y/bounds.Count);
                    // Set the constraints plan to the one we created
                    constraints.plan = plan;

                    // Create the building
                    buildingScript.CreateBuilding( constraints );

                    // Handle no height set, set to 0.0f in this case
                    mimimumHeight = mimimumHeight == float.MaxValue ? 0.0f : mimimumHeight;
                    Vector3 v = new Vector3(xy.x, mimimumHeight, xy.z);
                    RaycastHit hit;
                    // Set height of building
                    
                    if (Physics.Raycast(v, -Vector3.up, out hit, 50000)) {
                        Debug.Log("casting ray from:" + v+" hit:"+hit.point);
                        if (mimimumHeight > hit.point.y) 
                            v = new Vector3(0, hit.point.y, 0);
                    }
                    else  v = new Vector3(0, mimimumHeight, 0);
                    model.transform.position = v;
                    StringOffset name = fbb.CreateString(buildName);
                    ControlPointMsg.StartControlPointMsg(fbb);
                    ControlPointMsg.AddOID(fbb, mapBuilding.id);
                    ControlPointMsg.AddName(fbb, name);
                    ControlPointMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, v.y, v.y, v.z));
                    
                    point.pos = v;
                    pointOffsets.Add(ControlPointMsg.EndControlPointMsg(fbb));
                }
			}

            model.name = buildName;
            GameObject collider = model.GetComponentInChildren<MeshCollider>().gameObject;
            ControlPointListener listener =  collider.AddComponent<ControlPointListener>();
            listener.point = point;
            //model.AddComponent<ControlPoint>();


        }

        VectorOffset pointsOffset = ControlPointDatabase.CreateControlPointsVector(fbb, pointOffsets.ToArray());
        Offset<ControlPointDatabase> offset = ControlPointDatabase.CreateControlPointDatabase(fbb, pointsOffset);
        
        fbb.Finish(offset.Value);
        File.WriteAllBytes("ControlPointDatabase", fbb.DataBuffer.Data);
    }
#endif
    public static string Reverse(string s) {
        char[] charArray = s.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

    /* public static string Reverse(string t) {
         char[] charArray = t.ToCharArray();
         string a = "";
         int last = 0;
         for (int i = 0; i <= charArray.Length - 1; i++) {

             if (!IsHebrew(charArray[i])) {
                 List<char> temp = new List<char>();

                 for (; last < i; last++) {
                     int k = 0;
                     temp.Insert(0, charArray[last]);
                 }

                 foreach (char g in temp) {
                     a += g.ToString();
                 }
                 a += charArray[i];
                 last += 1;
             }
         }

         return a;
     }*/
    private const char FirstHebChar = (char)1488; //א
    private const char LastHebChar = (char)1514; //ת
    private static bool IsHebrew(char c) {
        return c >= FirstHebChar && c <= LastHebChar;
    }

    private bool ContainsHebrew(string t) {
        char[] charArray = t.ToCharArray();
        for (int i = 0; i <= charArray.Length - 1; i++) {
            if (IsHebrew(charArray[i])) return true;
        }
        return false;
    }
}
