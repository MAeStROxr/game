﻿// Mapity BuildR Example
// Copyright (c) 2014 Rewind Game Studio http://www.rewindgamestudio.com
// Support contact support@rewindgamestudio.com
//
// This code and information are provided "as is" without warranty of any 
// Kind, either expressed or implied, including but not limited to the
// Implied warranties of merchantability and/or fitness for a
// Particular purpose.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildRRuntimeExampleScript : MonoBehaviour 
{	
	// BuildR Contraints
    BuildrRuntimeConstraints constraints;

	// GameObject to hold a temporary copy of a building
    GameObject model = null;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
    void OnEnable()
	{
		// Register Map-ity's Loaded Event
		Mapity.MapityLoaded += OnMapityLoaded;

		// Create BuildR constraints
		constraints = ScriptableObject.CreateInstance<BuildrRuntimeConstraints>();
    }

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable()
	{
		// Un-Register Map-ity's Loaded Event
		Mapity.MapityLoaded -= OnMapityLoaded;
	}

    private IEnumerator CreateBuildings()
    {
        // Parent object to store buildings under
        GameObject buildingsGameObject = new GameObject("Buildings");

        // Loop over all the buildings in Map-ity
        foreach (Mapity.MapBuilding mapBuilding in Mapity.Singleton.mapBuildings.Values)
        {
            yield return new WaitForSeconds(0.5f);

            // Create a new GameObject for this building with it's id as it's name
            model = new GameObject(mapBuilding.id.ToString());

            // Set the "Buildings" GameObject as it's parent
            model.transform.parent = buildingsGameObject.transform;

            // Add the building script to the building
            BuildingRuntimeScript buildingRuntimeScript = model.AddComponent<BuildingRuntimeScript>();

            // Loop over the ways which define this building(usually 1)
            for (int i = 0; i < mapBuilding.buildingWays.Count; i++)
            {
                // Get the way
                Mapity.MapWay buildingWay = (Mapity.MapWay)mapBuilding.buildingWays[i];

                // NULL check
                if (buildingWay != null)
                {
                    // Create a BuildRPlan
                    BuildrPlan plan = ScriptableObject.CreateInstance<BuildrPlan>();

                    // Container for the nodes which make up the floor plan
                    List<Vector2z> bounds = new List<Vector2z>();

                    // Loop over the nodes
                    for (int j = 0; j < buildingWay.wayMapNodes.Count-1; j++)
                    {
                        // Get the node
                        Mapity.MapNode node = (Mapity.MapNode)buildingWay.wayMapNodes[j];

                        Vector2z boundNode = new Vector2z(node.position.world.ToVector().x, node.position.world.ToVector().z);

                        // Add it to the bounds
                        if (!bounds.Contains(boundNode))
                        {
                            bounds.Add(boundNode);
                        }
                    }

                    // Add the bounds to the plan
                    plan.AddVolume(bounds.ToArray());

                    // Set the constraints plan to the one we created
                    constraints.plan = plan;

                    // Use the plan we provided
                    constraints.constrainPlanByPlan = true;

                    // Create the building
                    yield return buildingRuntimeScript.CreateBuilding(constraints);
                }
            }
        }
    }

    /// <summary>
    /// Raises the mapity loaded event.
    /// </summary>
    void OnMapityLoaded()
	{
        StartCoroutine ( CreateBuildings() );
    }
}
