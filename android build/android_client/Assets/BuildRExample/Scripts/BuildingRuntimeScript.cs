﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BuildRUtil;

public class BuildingRuntimeScript : MonoBehaviour {

	public enum renderModes
	{
		full,
		lowDetail,
		box
	}
	
	public BuildrData data;

	public DynamicMesh colliderMesh = null;
	public DynamicMesh fullMesh = null;
	public List<DynamicMesh> interiorMeshes = new List<DynamicMesh>();
	
	public List<GameObject> colliderHolders = new List<GameObject>();
	public List<GameObject> meshHolders = new List<GameObject>();
	public List<GameObject> interiorMeshHolders = new List<GameObject>();
	public MeshFilter meshFilt = null;
	public MeshRenderer meshRend = null;
	private Material lowDetailMat = null;
	public List<Material> materials;
	public bool includeCollider = true;

	Bounds buildingBounds = new Bounds();

	void Awake()
	{
		data = gameObject.AddComponent<BuildrData>();
		data.Init();		
		data.generateCollider = BuildrData.ColliderGenerationModes.Simple;
	}

	// Use this for initialization
	void Start () 
	{
		materials = new List<Material>();
	}
	
	public IEnumerator CreateBuilding( BuildrRuntimeConstraints constraints )
	{			
		BuildrRuntimeGenerator.Generate(data,constraints);
		UpdateRender(renderModes.full);
		UpdateCollider();
		
		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
		buildingBounds = new Bounds();
		foreach(Renderer rend in renderers)
		{
			buildingBounds.Encapsulate(rend.bounds);
		}
        yield return null;
	}

    public void UpdateRender(renderModes _mode)
    {
        if (data.plan == null)
            return;
        if (data.floorHeight == 0)
            return;
        if (fullMesh == null)
            fullMesh = new DynamicMesh();

        fullMesh.Clear();

        foreach (DynamicMesh intMesh in interiorMeshes)
        {
            intMesh.Clear();
        }

        switch (_mode)
        {
            case renderModes.full:
                BuildrBuilding.Build(fullMesh, data);
                BuildrRoof.Build(fullMesh, data);
                break;

            case renderModes.lowDetail:
                BuildrBuildingLowDetail2.Build(fullMesh, data);
                fullMesh.CollapseSubmeshes();
                break;

            case renderModes.box:
                BuildrBuildingBox.Build(fullMesh, data);
                break;
        }

        fullMesh.Build();

        while (meshHolders.Count > 0)
        {
            GameObject destroyOld = meshHolders[0];
            meshHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }

        int numberOfMeshes = fullMesh.meshCount;
        Mesh[] meshes = fullMesh.meshes;
        for (int i = 0; i < numberOfMeshes; i++)
        {
            GameObject newMeshHolder = new GameObject("model " + (i + 1));
            newMeshHolder.transform.parent = transform;
            newMeshHolder.transform.localPosition = Vector3.zero;
            meshFilt = newMeshHolder.AddComponent<MeshFilter>();
            meshRend = newMeshHolder.AddComponent<MeshRenderer>();
            meshFilt.mesh = meshes[i];
            meshHolders.Add(newMeshHolder);
        }

        while (interiorMeshHolders.Count > 0)
        {
            GameObject destroyOld = interiorMeshHolders[0];
            interiorMeshHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }

        switch (_mode)
        {
            case renderModes.full:
                UpdateInteriors();
                UpdateTextures();
                break;

            case renderModes.lowDetail:
                meshRend.sharedMaterials = new Material[0];
                lowDetailMat.mainTexture = data.LODTextureAtlas;
                meshRend.sharedMaterial = lowDetailMat;
                break;

            case renderModes.box:
                meshRend.sharedMaterials = new Material[0];
                lowDetailMat.mainTexture = data.textures[0].texture;
                meshRend.sharedMaterial = lowDetailMat;
                break;
        }
    }

    public void UpdateTextures()
    {
        int numberOfMaterials = data.textures.Count;
        if (materials == null)
            materials = new List<Material>(numberOfMaterials);
        materials.Clear();
        for (int m = 0; m < numberOfMaterials; m++)
        {
            materials.Add(data.textures[m].material);
            materials[m].name = data.textures[m].name;
            materials[m].mainTexture = data.textures[m].texture;
        }
        //meshRend.sharedMaterials = materials.ToArray();

        int numberOfMeshes = fullMesh.meshCount;
        for (int i = 0; i < numberOfMeshes; i++)
            meshHolders[i].GetComponent<MeshRenderer>().sharedMaterials = materials.ToArray();

        int numberOfInterior = interiorMeshHolders.Count;
        for (int i = 0; i < numberOfInterior; i++)
            interiorMeshHolders[i].GetComponent<MeshRenderer>().sharedMaterials = materials.ToArray();
    }

    public void UpdateCollider()
    {
        if (data.generateCollider != BuildrData.ColliderGenerationModes.None)
        {
            if (data.floorHeight == 0)
                return;
            if (colliderMesh == null)
                colliderMesh = new DynamicMesh();

            colliderMesh.Clear();
            //            colliderMesh.subMeshCount = 1;
            BuildrBuildingCollider.Build(colliderMesh, data);
            colliderMesh.Build();

            Mesh[] meshes = colliderMesh.meshes;
            int numberOfMeshes = meshes.Length;
            for (int i = 0; i < numberOfMeshes; i++)
            {
                string meshName = "collider";
                if (numberOfMeshes > 1) meshName += " mesh " + (i + 1);
                GameObject newMeshHolder = new GameObject(meshName);
                newMeshHolder.transform.parent = transform;
                MeshCollider meshCollider = newMeshHolder.AddComponent<MeshCollider>();
                meshCollider.sharedMesh = colliderMesh[i].mesh;
                colliderHolders.Add(newMeshHolder);
            }
        }
    }

    public void UpdateInteriors()
    {
        while (interiorMeshHolders.Count > 0)
        {
            GameObject destroyOld = interiorMeshHolders[0];
            interiorMeshHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }

        interiorMeshes.Clear();

        if (data.renderInteriors)
        {
            int numberOfVolumes = data.plan.numberOfVolumes;
            for (int v = 0; v < numberOfVolumes; v++)
            {
                DynamicMesh interiorMesh = new DynamicMesh();
                //                interiorMesh.subMeshCount = data.textures.Count;
                BuildrInteriors.Build(interiorMesh, data, v);
                interiorMesh.Build();

                int numberOfInteriorMeshes = interiorMesh.meshCount;
                Mesh[] meshes = interiorMesh.meshes;
                for (int i = 0; i < numberOfInteriorMeshes; i++)
                {
                    string meshName = "model interior";
                    if (numberOfVolumes > 0) meshName += " volume " + (v + 1);
                    if (numberOfInteriorMeshes > 1) meshName += " mesh " + (i + 1);
                    GameObject newMeshHolder = new GameObject(meshName);
                    newMeshHolder.transform.parent = transform;
                    newMeshHolder.transform.localPosition = Vector3.zero;
                    meshFilt = newMeshHolder.AddComponent<MeshFilter>();
                    meshRend = newMeshHolder.AddComponent<MeshRenderer>();
                    meshFilt.mesh = meshes[i];
                    interiorMeshHolders.Add(newMeshHolder);
                }
            }
        }
    }
}
