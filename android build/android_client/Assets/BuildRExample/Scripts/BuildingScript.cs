﻿using UnityEngine;
using System.Collections.Generic;
using BuildRUtil;


[ExecuteInEditMode]
public class BuildingScript : MonoBehaviour 
{	
#if UNITY_EDITOR	
	public BuildrData data;
    public DynamicMesh colliderMesh = null;
    public DynamicMesh fullMesh = null;
    public DynamicMesh detailMesh = null;
    public List<DynamicMesh> interiorMeshes = new List<DynamicMesh>();
    public List<GameObject> bayModels = new List<GameObject>();
    public List<GameObject> colliderHolders = new List<GameObject>();
	public List<GameObject> meshHolders = new List<GameObject>();
	public List<GameObject> interiorMeshHolders = new List<GameObject>();
    public GameObject bayModelHolder;
    public MeshFilter meshFilt = null;
	public MeshRenderer meshRend = null;
	public Material lowDetailMat ;
	public List<Material> materials;
    public GameObject[] details;
    public bool includeCollider = true;
    	
	Bounds buildingBounds = new Bounds();
	
	public void CreateBuilding( BuildrGenerateConstraints constraints )
	{		
		data = gameObject.AddComponent<BuildrData>();
		data.Init();
		data.generateCollider = BuildrData.ColliderGenerationModes.Simple;
		materials = new List<Material>();

		ModifiedBuildrBuildingGenerator.Generate(data, constraints);
		UpdateRender();
		UpdateCollider();
		
		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
		buildingBounds = new Bounds();
		foreach(Renderer rend in renderers)
		{
			buildingBounds.Encapsulate(rend.bounds);
		}
	}
    BuildrEditMode.renderModes _mode = BuildrEditMode.renderModes.box;
    public void UpdateRender()
    {
        lowDetailMat = new Material(Shader.Find("Diffuse"));
        if (data.plan == null)
            return;
        if (data.floorHeight == 0)
            return;
        if (fullMesh == null)
            fullMesh = new DynamicMesh();

        fullMesh.Clear();

        foreach (DynamicMesh intMesh in interiorMeshes)
        {
            intMesh.Clear();
        }

        switch (_mode) {
            case BuildrEditMode.renderModes.full:
                //                    if(data.oneDrawCall)
                //                    {
                //                        BuildrBuildingOneDrawCall.Build(fullMesh, data);
                //                    }
                //                    else
                //                    {
                BuildrBuilding.Build(fullMesh, data);
                BuildrRoof.Build(fullMesh, data);
                //                    }
                break;

            case BuildrEditMode.renderModes.lowDetail:
                BuildrBuildingLowDetail2.Build(fullMesh, data);
                fullMesh.CollapseSubmeshes();
                break;

            case BuildrEditMode.renderModes.box:
                BuildrBuildingBox.Build(fullMesh, data);
                break;
        }


        fullMesh.Build();

        while (meshHolders.Count > 0)
        {
            GameObject destroyOld = meshHolders[0];
            meshHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }
        while (colliderHolders.Count > 0)
        {
            GameObject destroyOld = colliderHolders[0];
            colliderHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }

        int numberOfMeshes = fullMesh.meshCount;
        Mesh[] meshes = fullMesh.meshes;
        for (int i = 0; i < numberOfMeshes; i++)
        {
            GameObject newMeshHolder = new GameObject("model " + (i + 1));
            newMeshHolder.transform.parent = transform;
            newMeshHolder.transform.localPosition = Vector3.zero;
            meshFilt = newMeshHolder.AddComponent<MeshFilter>();
            meshRend = newMeshHolder.AddComponent<MeshRenderer>();
            meshFilt.mesh = meshes[i];
            meshHolders.Add(newMeshHolder);
        }

        while (interiorMeshHolders.Count > 0)
        {
            GameObject destroyOld = interiorMeshHolders[0];
            interiorMeshHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }


        switch (_mode) {
            case BuildrEditMode.renderModes.full:
                
                UpdateInteriors();
                UpdateTextures();
                UpdateDetails();
                UpdateBayModels();
                break;

            case BuildrEditMode.renderModes.lowDetail:
                
                meshRend.sharedMaterials = new Material[0];
                lowDetailMat.mainTexture = data.LODTextureAtlas;
                meshRend.sharedMaterial = lowDetailMat;
                

                UpdateDetails();
                break;

            case BuildrEditMode.renderModes.box:
                
                meshRend.sharedMaterials = new Material[0];
                //lowDetailMat = new Material(data.textures[0].usedMaterial);
                lowDetailMat.mainTexture = data.textures[0].texture;
                meshRend.sharedMaterial = lowDetailMat;
                UpdateDetails();
                break;
        }

      
    }

    public void UpdateTextures()
    {
        int numberOfMeshes = fullMesh.meshCount;
        List<Material> buildMaterials = new List<Material>();
        for (int i = 0; i < numberOfMeshes; i++)
        {
            buildMaterials.Clear();
            int[] materialIndexList = fullMesh[i].materialList;
            for (int m = 0; m < materialIndexList.Length; m++)
                buildMaterials.Add(data.textures[materialIndexList[m]].usedMaterial);
            meshHolders[i].GetComponent<MeshRenderer>().sharedMaterials = buildMaterials.ToArray();
        }
    }

    public void UpdateCollider()
    {
        if (data.generateCollider != BuildrData.ColliderGenerationModes.None)
        {
            if (data.floorHeight == 0)
                return;
            if (colliderMesh == null)
                colliderMesh = new DynamicMesh();
            colliderMesh.Clear();
            BuildrBuildingCollider.Build(colliderMesh, data);
            colliderMesh.Build();

            Mesh[] meshes = colliderMesh.meshes;
            int numberOfMeshes = meshes.Length;
            for (int i = 0; i < numberOfMeshes; i++)
            {
                if (meshes[i].triangles.Length == 0)
                    continue;
                string meshName = "collider";
                if (numberOfMeshes > 1) meshName += " mesh " + (i + 1);
                GameObject newMeshHolder = new GameObject(meshName);
                newMeshHolder.transform.parent = transform;
                newMeshHolder.transform.localPosition = Vector3.zero;
                MeshCollider meshCollider = newMeshHolder.AddComponent<MeshCollider>();
                meshCollider.sharedMesh = meshes[i];
                colliderHolders.Add(newMeshHolder);
            }
        }
    }

    public void UpdateInteriors()
    {
        while (interiorMeshHolders.Count > 0)
        {
            GameObject destroyOld = interiorMeshHolders[0];
            interiorMeshHolders.RemoveAt(0);
            DestroyImmediate(destroyOld);
        }

        interiorMeshes.Clear();

        if (data.renderInteriors)
        {
            int numberOfVolumes = data.plan.numberOfVolumes;
            for (int v = 0; v < numberOfVolumes; v++)
            {
                DynamicMesh interiorMesh = new DynamicMesh();
                BuildrVolume volume = data.plan.volumes[v];
                BuildrInteriors.Build(interiorMesh, data, v);
                interiorMesh.Build();
                int numberOfInteriorMeshes = interiorMesh.meshCount;

                List<Material> interiorMaterials = new List<Material>();
                for (int i = 0; i < numberOfInteriorMeshes; i++)
                {
                    int[] materialIndexList = interiorMesh[i].materialList;
                    for (int m = 0; m < materialIndexList.Length; m++)
                        interiorMaterials.Add(data.textures[materialIndexList[m]].usedMaterial);
                }


                Mesh[] meshes = interiorMesh.meshes;
                for (int i = 0; i < numberOfInteriorMeshes; i++)
                {
                    string meshName = "model interior";
                    if (numberOfVolumes > 0) meshName += " volume " + (v + 1);
                    if (numberOfInteriorMeshes > 1) meshName += " mesh " + (i + 1);
                    GameObject newMeshHolder = new GameObject(meshName);
                    newMeshHolder.transform.parent = transform;
                    newMeshHolder.transform.localPosition = Vector3.zero;
                    meshFilt = newMeshHolder.AddComponent<MeshFilter>();
                    meshRend = newMeshHolder.AddComponent<MeshRenderer>();
                    meshFilt.mesh = meshes[i];
                    interiorMeshHolders.Add(newMeshHolder);
                    meshRend.sharedMaterials = interiorMaterials.ToArray();
                }
                interiorMeshes.Add(interiorMesh);

                if (!volume.generateStairs) continue;

                DynamicMesh stairwellMesh = new DynamicMesh();
                BuildrStairs.Build(stairwellMesh, data, v, true, data.stairMode);
                stairwellMesh.Build();

                List<Material> stairMaterials = new List<Material>();
                int numberOfStairMeshes = stairwellMesh.meshCount;
                for (int i = 0; i < numberOfStairMeshes; i++)
                {
                    int[] materialIndexList = stairwellMesh[i].materialList;
                    for (int m = 0; m < materialIndexList.Length; m++)
                        stairMaterials.Add(data.textures[materialIndexList[m]].usedMaterial);
                }

                meshes = stairwellMesh.meshes;
                for (int i = 0; i < numberOfStairMeshes; i++)
                {
                    string meshName = "model stairs";
                    if (numberOfVolumes > 0) meshName += " volume " + (v + 1);
                    if (numberOfStairMeshes > 1) meshName += " mesh " + (i + 1);
                    GameObject newMeshHolder = new GameObject(meshName);
                    newMeshHolder.transform.parent = transform;
                    newMeshHolder.transform.localPosition = volume.stairBaseVector[i];
                    meshFilt = newMeshHolder.AddComponent<MeshFilter>();
                    meshRend = newMeshHolder.AddComponent<MeshRenderer>();
                    meshFilt.mesh = meshes[i];
                    interiorMeshHolders.Add(newMeshHolder);
                    meshRend.sharedMaterials = stairMaterials.ToArray();
                }
                interiorMeshes.Add(stairwellMesh);
            }
        }
    }

    public void UpdateDetails()
    {
        int numberOfDetails = 0;
        if (details != null)
            numberOfDetails = details.Length;
        for (int i = 0; i < numberOfDetails; i++)
            DestroyImmediate(details[i]);

        if (data.plan == null)
            return;
        if (data.floorHeight == 0)
            return;
        if (data.details.Count == 0)
            return;
        if (detailMesh == null)
            detailMesh = new DynamicMesh();
        
        details = BuildrBuildingDetails.Render(detailMesh, data);
        numberOfDetails = details.Length;

        for (int i = 0; i < numberOfDetails; i++)
        {
            details[i].transform.parent = transform;
            details[i].transform.localPosition = Vector3.zero;
            details[i].transform.localRotation = Quaternion.identity;
        }
    }

    public void UpdateBayModels()
    {
        while (bayModels.Count > 0)
        {
            DestroyImmediate(bayModels[0]);
            bayModels.RemoveAt(0);
        }
        GameObject[] newModels = BuildrBuildingBayModels.Place(data);
        foreach (GameObject newModel in newModels)
        {
            newModel.transform.parent = bayModelHolder.transform;
            newModel.transform.position += transform.position;
        }
        bayModels.AddRange(newModels);
    }
#endif
}

