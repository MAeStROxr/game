﻿// BuildR
// Available on the Unity3D Asset Store
// Copyright (c) 2015 Jasper Stocker http://support.jasperstocker.com
// For support contact email@jasperstocker.com
//
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BuildRUtil
{
    public class DynamicMesh
    {
        public Mesh mesh = new Mesh();
        public List<Vector3> vertices;
        public List<Vector2> uv;
        public List<int> triangles;
        public Bounds bounds;
        public List<Vector3> normals; 
        public List<Vector4> tangents;
//        private int _subMeshes = 1;
        private Dictionary<int, List<int>> subTriangles;
        private DynamicMesh _overflow;
        private List<Vector2> _minWorldUVSize = new List<Vector2>();
        private List<Vector2> _maxWorldUVSize = new List<Vector2>();
        private List<int> _usedSubmeshes = new List<int>();
        private bool _hasLightmaps;

        public DynamicMesh(string newName = "dynmesh")
        {
            mesh.name = newName;
            vertices = new List<Vector3>();
            uv = new List<Vector2>();
            triangles = new List<int>();
            tangents = new List<Vector4>();
            normals = new List<Vector3>();
            subTriangles = new Dictionary<int, List<int>>();
            subTriangles.Add(0, new List<int>());
            bounds = new Bounds();
        }

        public string name
        {
            get { return mesh.name; }
            set { mesh.name = value; }
        }

        public void Build()
        {
            BuildThisMesh();
            if (_overflow != null)
                _overflow.Build();
        }

        private void BuildThisMesh()
        {
            mesh.Clear();
            mesh.vertices = vertices.ToArray();
            mesh.uv = uv.ToArray();
            mesh.uv2 = new Vector2[0];
            mesh.tangents = tangents.ToArray();
            mesh.normals = normals.ToArray();
            mesh.bounds = bounds;

            mesh.subMeshCount = 0;//set base submesh count to 1 so we can get started
                                  //            _subMeshes = 0;
                                  //            _unusedSubmeshes.Clear();
                                  //            for (int i = 0; i < subMeshCount; i++)
                                  //                _unusedSubmeshes.Add(i);//prepopulate unused submesh array


            int submeshCount = 0;
            foreach (KeyValuePair<int, List<int>> kvp in subTriangles)
            {
                int subemeshIndex = kvp.Key;
                int[] subTris = kvp.Value.ToArray();
                if (subTris.Length == 0)
                    continue;

                if (mesh.subMeshCount < submeshCount + 1) mesh.subMeshCount = submeshCount + 1;
                mesh.SetTriangles(subTris, submeshCount);
                _usedSubmeshes.Add(subemeshIndex);
                submeshCount++;
            }



//            int highestSubmesh = 0;
//            foreach (KeyValuePair<int, List<int>> triData in subTriangles)
//                if(highestSubmesh < triData.Key) highestSubmesh = triData.Key;
//
//            _usedSubmeshes.Clear();
//            for (int i = 0; i < highestSubmesh+1; i++)
//            {
//                if(!subTriangles.ContainsKey(i))
//                    continue;
//
//                int[] tris = subTriangles[i].ToArray();
//                if(tris.Length == 0)
//                    continue;
//
//                if(mesh.subMeshCount < submeshCount + 1) mesh.subMeshCount = submeshCount + 1;
//                mesh.SetTriangles(tris, submeshCount);
//                _usedSubmeshes.Add(i);
//                submeshCount++;
//            }

//            foreach (KeyValuePair<int, List<int>> triData in subTriangles)
//            {
//                int submeshNumber = triData.Key;
//                int[] tris = triData.Value.ToArray();
//                if (tris.Length == 0)
//                    continue;
//                if (submeshCount <= submeshNumber)//increase the submesh count if it exceeds the current count
//                {
//                    submeshCount = submeshNumber + 1;
//                    mesh.subMeshCount = submeshCount;
//                }
//
////                _unusedSubmeshes.Remove(submeshNumber);//submesh is used - remove entry
//                mesh.SetTriangles(tris, submeshNumber);
//            }
//
//            for(int sm = 0; sm < submeshCount; sm++)
//            {
//                if(mesh.GetTriangles(sm).Length == 0)
//                    _unusedSubmeshes.Add(sm);//prepopulate unused submesh array
//            }
        }

        /// <summary>
        /// Clears the mesh data, ready for nextNormIndex new mesh build
        /// </summary>
        public void Clear()
        {
            vertices.Clear();
            uv.Clear();
            triangles.Clear();
            tangents.Clear();
            normals.Clear();
            bounds.center = Vector3.zero;
            bounds.size = Vector3.zero;
            subTriangles.Clear();
            subTriangles.Add(0, new List<int>());
//            _subMeshes = 1;
            _overflow = null;
            _usedSubmeshes.Clear();
            _minWorldUVSize.Clear();
            _maxWorldUVSize.Clear();
        }

        public DynamicMesh this[int index]
        {
            get
            {
                if(index == 0) return this;
                if(_overflow != null)
                {
                    index--;
                    return _overflow[index];
                }
                return this;
            }
        }

        public int vertexCount { get { return vertices.Count; } }

        public int triangleCount
        {
            get
            {
                if (triangles == null)
                    return 0;
                return triangles.Count;
            }
        }

        public bool lightmapUvsCalculated
        {
            get {return _hasLightmaps;}
            set {_hasLightmaps = value;}
        }

        public int[] materialList
        {
            get {return _usedSubmeshes.ToArray();}
        }

//        public bool SubmeshUsed(int index)
//        {
//            if (_usedSubmeshes.Contains(index)) return true;
//            //            if(_overflow != null && _overflow.SubmeshUsed(index)) return true;
//            return false;
//        }
//
//        /// <summary>
//        /// Has this or any child dynamic meshes used the submesh index
//        /// Used for compling a full list of textures
//        /// </summary>
//        /// <param name="index"></param>
//        /// <returns></returns>
//        public bool SubmeshUsedDeep(int index)
//        {
//            if (_usedSubmeshes.Contains(index)) return true;
//            if(_overflow != null && _overflow.SubmeshUsed(index)) return true;
//            return false;
//        }

//        public List<int> usedSubmeshes
//        {
//            get { return _usedSubmeshes; }
//        }

        public int subMeshCount { get { return subTriangles.Count; } }

        public int meshCount
        {
            get
            {
                if(vertexCount == 0)
                    return 0;
                int output = 1;
                if (_overflow != null)
                    output += _overflow.meshCount;
                return output;
            }
        }

        public Mesh[] meshes
        {
            get
            {
                List<Mesh> output = new List<Mesh>();
                output.Add(mesh);
                if (_overflow != null)
                    output.AddRange(_overflow.meshes);
                return output.ToArray();
            }
        }

        public bool isEmpty
        {
            get { return vertexCount == 0; }
        }

        /// <summary>
        /// Add new mesh data - all arrays are ordered together
        /// </summary>
        /// <param customName="verts">And array of verticies</param>
        /// <param customName="uvs">And array of uvs</param>
        /// <param customName="tris">And array of triangles</param>
        /// <param customName="subMesh">The submesh to add the data into</param>
        public void AddData(Vector3[] verts, Vector2[] uvs, int[] tris, Vector3[] norms, Vector4[] tan, int subMesh)
        {
            if (MeshOverflow(verts.Length))
            {
                _overflow.AddData(verts, uvs, tris, norms, tan, subMesh);
                return;
            }

            int indiceBase = vertices.Count;
            vertices.AddRange(verts);
            uv.AddRange(uvs);
            CheckUVSize(uvs, subMesh);
            tangents.AddRange(tan);
            normals.AddRange(norms);

            if (!subTriangles.ContainsKey(subMesh))
                subTriangles.Add(subMesh, new List<int>());

            int newTriCount = tris.Length;
            for (int t = 0; t < newTriCount; t++)
            {
                int newTri = (indiceBase + tris[t]);
                triangles.Add(newTri);
                subTriangles[subMesh].Add(newTri);
            }
        }

        public void AddPlane(Vector3[] verts, Vector2[] uvs, int submesh)
        {
            if (verts.Length != 4)
                return;
            AddPlane(verts[0], verts[1], verts[2], verts[3], uvs[0], uvs[1], uvs[2], uvs[3], submesh);
        }

        public void AddPlane(Vector3[] verts, Vector2[] uvs, int[] tris, int submesh)
        {
            if (verts.Length != 4)
                return;
            AddPlane(verts[0], verts[1], verts[2], verts[3], uvs[0], uvs[1], uvs[2], uvs[3], tris[0], tris[1], tris[2], tris[3], tris[4], tris[5], submesh);
        }

        /// <summary>
        /// Adds the plane to the generic dynamic mesh without specifying UV coords.
        /// </summary>
        /// <param customName='p0,p1,p2,p3'>
        /// 4 Verticies that define the plane
        /// <param customName='subMesh'>
        /// The sub mesh to attch this plan to - in order of Texture library indicies
        /// </param>
        public void AddPlane(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, int subMesh)
        {
            Vector2 uv0 = new Vector2(p0.x, p0.y);
            Vector2 uv1 = new Vector2(p3.x, p3.y);
            AddPlane(p0, p1, p2, p3, uv0, uv1, subMesh);
        }

        /// <summary>
        /// Adds the plane to the generic dynamic mesh by specifying min and max UV coords.
        /// </summary>
        /// <param customName='p0,p1,p2,p3'>
        /// 4 Verticies that define the plane
        /// </param>
        /// <param customName='minUV'>
        /// the minimum vertex UV coord.
        /// </param>
        /// </param>
        /// <param customName='maxUV'>
        /// the maximum vertex UV coord.
        /// </param>
        /// <param customName='subMesh'>
        /// The sub mesh to attch this plan to - in order of Texture library indicies
        /// </param>
        public void AddPlane(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, Vector2 minUV, Vector2 maxUV, int subMesh)
        {
            Vector2 uv0 = new Vector2(minUV.x, minUV.y);
            Vector2 uv1 = new Vector2(maxUV.x, minUV.y);
            Vector2 uv2 = new Vector2(minUV.x, maxUV.y);
            Vector2 uv3 = new Vector2(maxUV.x, maxUV.y);

            AddPlane(p0, p1, p2, p3, uv0, uv1, uv2, uv3, subMesh);
        }

        /// <summary>
        /// Adds the plane to the generic dynamic mesh.
        /// </summary>
        /// <param customName='p0,p1,p2,p3'>
        /// 4 Verticies that define the plane
        /// </param>
        /// <param customName='uv0,uv1,uv2,uv3'>
        /// the vertex UV coords.
        /// </param>
        /// <param customName='subMesh'>
        /// The sub mesh to attch this plan to - in order of Texture library indicies
        /// </param>
        public void AddPlane(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, Vector2 uv0, Vector2 uv1, Vector2 uv2, Vector2 uv3, int subMesh)
        {
            AddPlane(p0, p1, p2, p3, uv0, uv1, uv2, uv3, 0, 2, 1, 1, 2, 3, subMesh);
        }

        public void AddPlane(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, Vector2 uv0, Vector2 uv1, Vector2 uv2, Vector2 uv3, int tri0, int tri1, int tri2, int tri3, int tri4, int tri5, int subMesh)
        {
            if (MeshOverflow(4))
            {
                _overflow.AddPlane(p0, p1, p2, p3, uv0, uv1, uv2, uv3, subMesh);
                return;
            }
             
            int indiceBase = vertices.Count;
            vertices.Add(p0);
            vertices.Add(p1);
            vertices.Add(p2);
            vertices.Add(p3);

            uv.Add(uv0);
            uv.Add(uv1);
            uv.Add(uv2);
            uv.Add(uv3);

            CheckUVSize(new[] { uv0, uv1, uv2, uv3 }, subMesh);

            if (!subTriangles.ContainsKey(subMesh))
                subTriangles.Add(subMesh, new List<int>());

            subTriangles[subMesh].Add(indiceBase + tri0);
            subTriangles[subMesh].Add(indiceBase + tri1);
            subTriangles[subMesh].Add(indiceBase + tri2);

            subTriangles[subMesh].Add(indiceBase + tri3);
            subTriangles[subMesh].Add(indiceBase + tri4);
            subTriangles[subMesh].Add(indiceBase + tri5);

            triangles.Add(indiceBase + tri0);
            triangles.Add(indiceBase + tri1);
            triangles.Add(indiceBase + tri2);

            triangles.Add(indiceBase + tri3);
            triangles.Add(indiceBase + tri4);
            triangles.Add(indiceBase + tri5);

            Vector3 tangentDirection = (p1 - p0).normalized;
            Vector4 tangent = CalculateTangent(tangentDirection);
            tangents.Add(tangent);
            tangents.Add(tangent);
            tangents.Add(tangent);
            tangents.Add(tangent);

            Vector3 upDirection = (p2 - p0).normalized;
            Vector3 normal = -Vector3.Cross(tangentDirection, upDirection);
            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);
            normals.Add(normal);

        }

        /// <summary>
        /// Check if the vertex count exceeds Unity's 65000 limit
        /// If it does, we create an overflow that continues the mesh construction
        /// If there is already an overflow, we've already exceeded the mesh count
        /// </summary>
        /// <param name="numberOfNewVerts">Number of verts being added to current total</param>
        /// <returns></returns>
        private bool MeshOverflow(int numberOfNewVerts)
        {
            if (_overflow != null)
                return true;
            if (numberOfNewVerts + vertexCount >= 65000)
            {
                _overflow = new DynamicMesh(string.Format("{0}{1}", mesh.name, "i"));
                return true;
            }
            return false;
        }


        /// <summary>
        /// Checks the Max UV values used in this model for each texture.
        /// </summary>
        /// <param name='data'>
        /// BuildR Data.
        /// </param>
        public void CheckMaxTextureUVs(BuildrData data)
        {
            Vector2[] subMeshUVOffsets = new Vector2[subMeshCount];
            int[] subMeshIDs = new List<int>(subTriangles.Keys).ToArray();
            int numberOfSubmeshIDs = subMeshIDs.Length;
            for (int sm = 0; sm < numberOfSubmeshIDs; sm++)
            {
                int subMeshID = subMeshIDs[sm];
                if (subTriangles.ContainsKey(subMeshID))
                {
                    int[] submeshIndices = subTriangles[subMeshID].ToArray();
                    subMeshUVOffsets[sm] = Vector2.zero;
                    foreach (int index in submeshIndices)
                    {
                        if (uv[index].x < subMeshUVOffsets[sm].x)
                            subMeshUVOffsets[sm].x = uv[index].x;
                        if (uv[index].y < subMeshUVOffsets[sm].y)
                            subMeshUVOffsets[sm].y = uv[index].y;
                    }

                    List<int> UVsOffset = new List<int>();
                    foreach (int uvindex in subTriangles[subMeshID])
                    {
                        if (!UVsOffset.Contains(uvindex))
                        {
                            uv[uvindex] += -subMeshUVOffsets[sm];//offset the UV to ensure it isn't negative
                            UVsOffset.Add(uvindex);
                        }
                        data.textures[subMeshID].CheckMaxUV(uv[uvindex]);
                    }
                }
                else
                {
                    Debug.Log("Mesh does not contain key for texture " + data.textures[subMeshID].name);
                }
            }
        }

        public Vector2 MinWorldUvSize(int submesh)
        {
            submesh = Mathf.Clamp(submesh, 0, _minWorldUVSize.Count - 1);
            return _minWorldUVSize[submesh];
        }
        public Vector2 MaxWorldUvSize(int submesh)
        {
            submesh = Mathf.Clamp(submesh, 0, _maxWorldUVSize.Count - 1);
            return _maxWorldUVSize[submesh];
        }

//        public List<Vector2> minWorldUvSize { get { return _minWorldUVSize; } }
//        public List<Vector2> maxWorldUvSize { get { return _maxWorldUVSize; } }

        private void CheckUVSize(Vector2[] uvs, int subMesh)
        {
            while (_minWorldUVSize.Count < subMesh + 1)
                _minWorldUVSize.Add(Vector2.zero);
            while (_maxWorldUVSize.Count < subMesh + 1)
                _maxWorldUVSize.Add(Vector2.one);

            Vector2 minUVSize = _minWorldUVSize[subMesh];
            Vector2 maxUVSize = _maxWorldUVSize[subMesh];
            int vertCount = uvs.Length;
            for (int i = 0; i < vertCount - 1; i++)
            {
                Vector2 thisuv = uvs[i];
                if (thisuv.x < minUVSize.x)
                    minUVSize.x = thisuv.x;
                if (thisuv.y < minUVSize.y)
                    minUVSize.y = thisuv.y;

                if (thisuv.x > maxUVSize.x)
                    maxUVSize.x = thisuv.x;
                if (thisuv.y > maxUVSize.y)
                    maxUVSize.y = thisuv.y;
            }
            _minWorldUVSize[subMesh] = minUVSize;
            _maxWorldUVSize[subMesh] = maxUVSize;
        }

        /// <summary>
        ///  Atlas the entire mesh using newTextureCoords and textures.
        /// </summary>
        /// <param name="newTextureCoords"></param>
        /// <param name="textures"></param>
        public void Atlas(Rect[] newTextureCoords, BuildrTexture[] textures)
        {
            List<int> keys = new List<int>(subTriangles.Keys);
            Atlas(keys.ToArray(), newTextureCoords, textures);
        }

        /// <summary>
        /// Atlas the specified modifySubmeshes using newTextureCoords and textures.
        /// </summary>
        /// <param name='modifySubmeshes'>
        /// Submeshes indicies to atlas.
        /// </param>
        /// <param name='newTextureCoords'>
        /// New texture coords generated from Pack Textures.
        /// </param>
        /// <param name='textures'>
        /// BuildR Textures library reference.
        /// </param>
        public void Atlas(int[] modifySubmeshes, Rect[] newTextureCoords, BuildrTexture[] textures)
        {
            if (modifySubmeshes.Length == 0)
            {
                Debug.Log("No submeshes to atlas!");
                return;
            }
            List<int> atlasedSubmesh = new List<int>();
            int numberOfSubmeshesToModify = modifySubmeshes.Length;
            for (int s = 0; s < numberOfSubmeshesToModify; s++)
            {
                int submeshIndex = modifySubmeshes[s];
                Rect textureRect = newTextureCoords[s];
                if (!subTriangles.ContainsKey(submeshIndex))
                    continue;
                int[] submeshIndices = subTriangles[submeshIndex].ToArray();
                subTriangles.Remove(submeshIndex);
                atlasedSubmesh.AddRange(submeshIndices);

                BuildrTexture bTexture = textures[submeshIndex];
                List<int> ModifiedUVs = new List<int>();
                foreach (int index in submeshIndices)
                {
                    if (ModifiedUVs.Contains(index))
                        continue;//don't move the UV more than once
                    Vector2 uvCoords = uv[index];
                    float xUV = uvCoords.x / bTexture.maxUVTile.x;
                    float yUV = uvCoords.y / bTexture.maxUVTile.y;
                    if (xUV > 1)
                    {
                        bTexture.maxUVTile.x = uvCoords.x;
                        xUV = 1.0f;
                    }
                    if (yUV > 1)
                    {
                        bTexture.maxUVTile.y = uvCoords.y;
                        yUV = 1;
                    }

                    uvCoords.x = Mathf.Lerp(textureRect.xMin, textureRect.xMax, xUV);
                    uvCoords.y = Mathf.Lerp(textureRect.yMin, textureRect.yMax, yUV);

                    if (float.IsNaN(uvCoords.x))
                    {
                        uvCoords.x = 1;
                    }
                    if (float.IsNaN(uvCoords.y))
                    {
                        uvCoords.y = 1;
                    }
                    uv[index] = uvCoords;
                    ModifiedUVs.Add(index);//keep a record of moved UVs
                }
            }
//            subMeshCount = subMeshCount - modifySubmeshes.Length + 1;
            subTriangles.Add(modifySubmeshes[0], atlasedSubmesh);
        }

        /// <summary>
        /// Atlas the entire mesh, specifying specific submeshes that have been atlased
        /// </summary>
        /// <param name="modifySubmeshes">Specified submeshes for the atlased coords</param>
        /// <param name="newTextureCoords">New texture coords generated from Pack Textures.</param>
        public void Atlas(int[] modifySubmeshes, Rect[] newTextureCoords)
        {
            if (modifySubmeshes.Length == 0)
            {
                Debug.Log("No submeshes to atlas!");
                return;
            }
            List<int> atlasedSubmesh = new List<int>();//triangles from all the submeshes
            List<int> modifySubmeshList = new List<int>(modifySubmeshes);
            int subMeshNumber = subTriangles.Count;
            for (int s = 0; s < subMeshNumber; s++)//loop through the submeshes
            {
                if (!subTriangles.ContainsKey(s))//if the submesh is empty/does not exist - skip it
                    continue;

                int[] submeshIndices = subTriangles[s].ToArray();//array of submesh triangles
                subTriangles.Remove(s);//remove submesh to be atlased
                atlasedSubmesh.AddRange(submeshIndices);//add the submesh triangles to the list

                if (modifySubmeshList.Contains(s))//if we are to modify this submesh
                {
                    Rect textureRect = newTextureCoords[s];//new UV range in the atlased texture to map the model UV to
                    List<int> ModifiedUVs = new List<int>();
                    foreach (int index in submeshIndices)
                    {
                        if (ModifiedUVs.Contains(index))
                            continue;//don't move the UV more than once
                        Vector2 uvCoords = uv[index];
                        uvCoords.x = Mathf.Lerp(textureRect.xMin, textureRect.xMax, uvCoords.x);
                        uvCoords.y = Mathf.Lerp(textureRect.yMin, textureRect.yMax, uvCoords.y);
                        uv[index] = uvCoords;
                        ModifiedUVs.Add(index);//keep a record of moved UVs
                    }
                }
                else
                {
                    List<int> ModifiedUVs = new List<int>();
                    foreach (int index in submeshIndices)
                    {
                        if (ModifiedUVs.Contains(index))
                            continue;//don't move the UV more than once
                        uv[index] = Vector2.zero;
                        ModifiedUVs.Add(index);//keep a record of moved UVs
                    }
                }
            }
            //subMeshCount = subMeshCount - modifySubmeshes.Length + 1;
//            subMeshCount = 1;
            subTriangles.Add(modifySubmeshes[0], atlasedSubmesh);
        }

        public void ForceNewMesh()
        {
            if (_overflow != null)
                _overflow.ForceNewMesh();
            else
                _overflow = new DynamicMesh(string.Format("{0}{1}", mesh.name, "i"));
        }

        /// <summary>
        /// Collapse all the submeshes into a single submesh
        /// </summary>
        public void CollapseSubmeshes()
        {
            List<int> singleSubmesh = new List<int>();

            foreach (KeyValuePair<int, List<int>> kvp in subTriangles)
                singleSubmesh.AddRange(kvp.Value);

                //            int numberOfSubmeshesToModify = subTriangles.Count;
                //            for (int s = 0; s < numberOfSubmeshesToModify; s++)
                //            {
                //                if(subTriangles.ContainsKey(s))
                //                {
                //                    int[] submeshIndices = subTriangles[s].ToArray();
                //                    singleSubmesh.AddRange(submeshIndices);
                //                }
                ////                foreach(KeyValuePair<int,List<int>> kvp in subTriangles)
                ////                {
                ////                    int[] submeshIndices = kvp.Value.ToArray();
                ////                    singleSubmesh.AddRange(submeshIndices);
                ////                }
                //            }
            subTriangles.Clear();
            subTriangles.Add(0, singleSubmesh);
        }

        /// <summary>
        /// Calcaulte the Tangent from a direction
        /// </summary>
        /// <param name="tangentDirection">the normalised right direction of the tangent</param>
        /// <returns></returns>
        public static Vector4 CalculateTangent(Vector3 tangentDirection)
        {
            Vector4 tangent = new Vector4();
            tangent.x = tangentDirection.x;
            tangent.y = tangentDirection.y;
            tangent.z = tangentDirection.z;
            tangent.w = 1;//TODO: Check whether we need to flip the bi normal - I don't think we do with these planes
            return tangent;
        }

        /// <summary>
        /// Calculate the normal of a triangle
        /// </summary>
        /// <param name="points">Only three points will be used in calculation</param>
        /// <returns></returns>
        public static Vector3 CalculateNormal(Vector3[] points)
        {
            if(points.Length < 3) return Vector3.down;//most likely to look wrong
            return CalculateNormal(points[0], points[1], points[2]);
        }

        /// <summary>
        /// Calculate the normal of a triangle
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static Vector3 CalculateNormal(Vector3 p0, Vector3 p1, Vector3 p2)
        {
            return Vector3.Cross((p1 - p0).normalized, (p2 - p0).normalized).normalized;
        }
    }
}