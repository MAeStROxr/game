﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using UnityEngine;
using TBTK;

namespace NTTest {
    [TestFixture]
    [Category("Tactical Local Controller Tests")]
    public class TacticalLocalControllerTests : MonoBehaviour {
        public GameObject obj = new GameObject();
        public ScriptInstantiator instantiator = new ScriptInstantiator();
        Faction player , AI ;
        FactionManager facMngr;
        public Battle battle;
        [SetUp]
        public void Init() {
            // obj.AddComponent<ScriptInstantiatior>();
            // instantiator = obj.GetComponent<ScriptInstantiatior>();
            battle = instantiator.InstantiateScript<Battle>();
            facMngr = battle.factionManager;
            player = facMngr.factionList[0];
            AI = facMngr.factionList[1];

        }
        [TearDown]
        public void TearDown() {
            //Destroy(battle);
        }


        [Test]
        public void InstantiateBattlePrefabTest() {
      
            
            Assert.AreNotEqual(null, battle.gameControl);
            Assert.AreEqual(2, battle.factionManager.factionList.Count);
            
            
        }
        [Test]
        public void InstantiateBattleFactionManagerTest() {
            Assert.AreNotEqual(null, facMngr);
            Assert.AreEqual(true, player.isPlayerFaction);
            Assert.AreEqual(false, AI.isPlayerFaction);
            Assert.AreEqual(false, player.isNetworkFaction);
            Assert.AreEqual(false, AI.isNetworkFaction);

            int playerUnitCount = 3;
            int AIUnitCount = 3;
            Assert.AreEqual(playerUnitCount, player.startingUnitList.Count);
            Assert.AreEqual(AIUnitCount, AI.allUnitList.Count);
        }

        [Test]
        public void AutoDeployTest() {

            Assert.AreEqual(0, player.allUnitList.Count);
            int playerUnitCount = 3;
            for (int i = 0; i < playerUnitCount; i++) {
                Assert.AreEqual(-1, player.startingUnitList[i].tileId);
                Assert.AreEqual(null, player.startingUnitList[i].tile);
            }
            battle.factionManager.AutoDeployCurrentFaction();
           
            Assert.AreEqual(playerUnitCount, player.allUnitList.Count);
            
            for (int i = 0; i < playerUnitCount; i++) { 
                Assert.AreNotEqual(-1, player.allUnitList[i].tileId);
                Assert.AreNotEqual(null, player.allUnitList[i].tile);
            }

        }
        /*
        [Test]
        public void BattleStartedTest() {
            Assert.Fail();
        }

        [Test]
        public void JoinArmiesTest() {
            Assert.Fail();
        }

        [Test]
        public void ArmiesJoinedTest() {
            Assert.Fail();
        }

        [Test]
        public void DeployArmiesTest() {
            Assert.Fail();
        }

        [Test]
        public void ArmiesDeployedTest() {
            Assert.Fail();
        }

        [Test]
        public void ArmyJoinedBattleTest() {
            Assert.Fail();
        }

        [Test]
        public void GetArmyDataTest() {
            Assert.Fail();
        }
        /*
        [Test]
        public void EndBattleTest() {
            Assert.Fail();
        }

          [Test]
        public void SetUnitAndAbilityTest() {
            Assert.Fail();
        }

        [Test]
        public void BattleEndedTest() {
            Assert.Fail();
        }

        [Test]
        public void ClearBattleDataTest() {
            Assert.Fail();
        }

        [Test]
        public void ClearFactionDataTest() {
            Assert.Fail();
        }

        [Test]
        public void UseFactionAbilityTest() {
            Assert.Fail();
        }

        [Test]
        public void FactionAbilityUsedTest() {
            Assert.Fail();
        }

        [Test]
        public void UseUnitAbilityTest() {
            Assert.Fail();
        }

        [Test]
        public void UnitAbilityUsedTest() {
            Assert.Fail();
        }

        [Test]
        public void UnitAbilityTest() {
            Assert.Fail();
        }

        [Test]
        public void MoveUnitTest() {
            Assert.Fail();
        }

        [Test]
        public void UnitMovedTest() {
            Assert.Fail();
        }

        [Test]
        public void AttackUnitTest() {
            Assert.Fail();
        }

        [Test]
        public void UnitAttackedTest() {
            Assert.Fail();
        }

        [Test]
        public void EndTurnTest() {
            Assert.Fail();
        }

        [Test]
        public void TurnEndedTest() {
            Assert.Fail();
        }
        */
    }
}