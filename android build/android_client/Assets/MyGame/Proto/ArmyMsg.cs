// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class ArmyMsg : Table {
  public static ArmyMsg GetRootAsArmyMsg(ByteBuffer _bb) { return GetRootAsArmyMsg(_bb, new ArmyMsg()); }
  public static ArmyMsg GetRootAsArmyMsg(ByteBuffer _bb, ArmyMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public ArmyMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID { get { int o = __offset(4); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.PutLong(o + bb_pos, OID); return true; } else { return false; } }
  public UnitMsg GetArmyUnits(int j) { return GetArmyUnits(new UnitMsg(), j); }
  public UnitMsg GetArmyUnits(UnitMsg obj, int j) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int ArmyUnitsLength { get { int o = __offset(6); return o != 0 ? __vector_len(o) : 0; } }
  public string ArmyName { get { int o = __offset(8); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetArmyNameBytes() { return __vector_as_arraysegment(8); }
  public DataAction Action { get { int o = __offset(10); return o != 0 ? (DataAction)bb.GetSbyte(o + bb_pos) : DataAction.NONE; } }
  public bool MutateAction(DataAction action) { int o = __offset(10); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)action); return true; } else { return false; } }
  public bool IsNetworkPlayer { get { int o = __offset(12); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsNetworkPlayer(bool isNetworkPlayer) { int o = __offset(12); if (o != 0) { bb.Put(o + bb_pos, (byte)(isNetworkPlayer ? 1 : 0)); return true; } else { return false; } }
  public bool IsPlayerFaction { get { int o = __offset(14); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsPlayerFaction(bool isPlayerFaction) { int o = __offset(14); if (o != 0) { bb.Put(o + bb_pos, (byte)(isPlayerFaction ? 1 : 0)); return true; } else { return false; } }
  public int FactionID { get { int o = __offset(16); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateFactionID(int factionID) { int o = __offset(16); if (o != 0) { bb.PutInt(o + bb_pos, factionID); return true; } else { return false; } }
  public int DataID { get { int o = __offset(18); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateDataID(int dataID) { int o = __offset(18); if (o != 0) { bb.PutInt(o + bb_pos, dataID); return true; } else { return false; } }
  public bool IsMainArmy { get { int o = __offset(20); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)true; } }
  public bool MutateIsMainArmy(bool isMainArmy) { int o = __offset(20); if (o != 0) { bb.Put(o + bb_pos, (byte)(isMainArmy ? 1 : 0)); return true; } else { return false; } }
  public bool RequireDeployment { get { int o = __offset(22); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateRequireDeployment(bool requireDeployment) { int o = __offset(22); if (o != 0) { bb.Put(o + bb_pos, (byte)(requireDeployment ? 1 : 0)); return true; } else { return false; } }
  public TileMsg GetDeployableTiles(int j) { return GetDeployableTiles(new TileMsg(), j); }
  public TileMsg GetDeployableTiles(TileMsg obj, int j) { int o = __offset(24); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int DeployableTilesLength { get { int o = __offset(24); return o != 0 ? __vector_len(o) : 0; } }
  public Vec3 Position { get { return GetPosition(new Vec3()); } }
  public Vec3 GetPosition(Vec3 obj) { int o = __offset(26); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }
  public Vec3 Color { get { return GetColor(new Vec3()); } }
  public Vec3 GetColor(Vec3 obj) { int o = __offset(28); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }
  public long ControllerOid { get { int o = __offset(30); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateControllerOid(long controllerOid) { int o = __offset(30); if (o != 0) { bb.PutLong(o + bb_pos, controllerOid); return true; } else { return false; } }
  public bool IsHostile { get { int o = __offset(32); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsHostile(bool isHostile) { int o = __offset(32); if (o != 0) { bb.Put(o + bb_pos, (byte)(isHostile ? 1 : 0)); return true; } else { return false; } }

  public static void StartArmyMsg(FlatBufferBuilder builder) { builder.StartObject(15); }
  public static void AddOID(FlatBufferBuilder builder, long OID) { builder.AddLong(0, OID, 0); }
  public static void AddArmyUnits(FlatBufferBuilder builder, VectorOffset armyUnitsOffset) { builder.AddOffset(1, armyUnitsOffset.Value, 0); }
  public static VectorOffset CreateArmyUnitsVector(FlatBufferBuilder builder, Offset<UnitMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartArmyUnitsVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddArmyName(FlatBufferBuilder builder, StringOffset armyNameOffset) { builder.AddOffset(2, armyNameOffset.Value, 0); }
  public static void AddAction(FlatBufferBuilder builder, DataAction action) { builder.AddSbyte(3, (sbyte)action, 0); }
  public static void AddIsNetworkPlayer(FlatBufferBuilder builder, bool isNetworkPlayer) { builder.AddBool(4, isNetworkPlayer, false); }
  public static void AddIsPlayerFaction(FlatBufferBuilder builder, bool isPlayerFaction) { builder.AddBool(5, isPlayerFaction, false); }
  public static void AddFactionID(FlatBufferBuilder builder, int factionID) { builder.AddInt(6, factionID, 0); }
  public static void AddDataID(FlatBufferBuilder builder, int dataID) { builder.AddInt(7, dataID, 0); }
  public static void AddIsMainArmy(FlatBufferBuilder builder, bool isMainArmy) { builder.AddBool(8, isMainArmy, true); }
  public static void AddRequireDeployment(FlatBufferBuilder builder, bool requireDeployment) { builder.AddBool(9, requireDeployment, false); }
  public static void AddDeployableTiles(FlatBufferBuilder builder, VectorOffset deployableTilesOffset) { builder.AddOffset(10, deployableTilesOffset.Value, 0); }
  public static VectorOffset CreateDeployableTilesVector(FlatBufferBuilder builder, Offset<TileMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartDeployableTilesVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddPosition(FlatBufferBuilder builder, Offset<Vec3> positionOffset) { builder.AddStruct(11, positionOffset.Value, 0); }
  public static void AddColor(FlatBufferBuilder builder, Offset<Vec3> colorOffset) { builder.AddStruct(12, colorOffset.Value, 0); }
  public static void AddControllerOid(FlatBufferBuilder builder, long controllerOid) { builder.AddLong(13, controllerOid, 0); }
  public static void AddIsHostile(FlatBufferBuilder builder, bool isHostile) { builder.AddBool(14, isHostile, false); }
  public static Offset<ArmyMsg> EndArmyMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<ArmyMsg>(o);
  }
};


}
