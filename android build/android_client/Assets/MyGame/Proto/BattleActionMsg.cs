// automatically generated, do not modify

namespace mygame.proto
{

public enum BattleActionMsg : byte
{
 NONE = 0,
 UnitMoveMsg = 1,
 UnitAbilityMsg = 2,
 UnitAttackMsg = 3,
};


}
