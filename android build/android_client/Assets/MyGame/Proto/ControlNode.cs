// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class ControlNode : Table {
  public static ControlNode GetRootAsControlNode(ByteBuffer _bb) { return GetRootAsControlNode(_bb, new ControlNode()); }
  public static ControlNode GetRootAsControlNode(ByteBuffer _bb, ControlNode obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public ControlNode __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID { get { int o = __offset(4); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.PutLong(o + bb_pos, OID); return true; } else { return false; } }
  public string ControlName { get { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetControlNameBytes() { return __vector_as_arraysegment(6); }
  public ControlNode GetControlled(int j) { return GetControlled(new ControlNode(), j); }
  public ControlNode GetControlled(ControlNode obj, int j) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int ControlledLength { get { int o = __offset(8); return o != 0 ? __vector_len(o) : 0; } }
  public ControlPointMsg GetControlPoints(int j) { return GetControlPoints(new ControlPointMsg(), j); }
  public ControlPointMsg GetControlPoints(ControlPointMsg obj, int j) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int ControlPointsLength { get { int o = __offset(10); return o != 0 ? __vector_len(o) : 0; } }
  public ControlNode Parent { get { return GetParent(new ControlNode()); } }
  public ControlNode GetParent(ControlNode obj) { int o = __offset(12); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int TaxRate { get { int o = __offset(14); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateTaxRate(int taxRate) { int o = __offset(14); if (o != 0) { bb.PutInt(o + bb_pos, taxRate); return true; } else { return false; } }
  public int TreeId { get { int o = __offset(16); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateTreeId(int treeId) { int o = __offset(16); if (o != 0) { bb.PutInt(o + bb_pos, treeId); return true; } else { return false; } }

  public static Offset<ControlNode> CreateControlNode(FlatBufferBuilder builder,
      long OID = 0,
      StringOffset controlNameOffset = default(StringOffset),
      VectorOffset controlledOffset = default(VectorOffset),
      VectorOffset controlPointsOffset = default(VectorOffset),
      Offset<ControlNode> parentOffset = default(Offset<ControlNode>),
      int taxRate = 0,
      int treeId = 0) {
    builder.StartObject(7);
    ControlNode.AddOID(builder, OID);
    ControlNode.AddTreeId(builder, treeId);
    ControlNode.AddTaxRate(builder, taxRate);
    ControlNode.AddParent(builder, parentOffset);
    ControlNode.AddControlPoints(builder, controlPointsOffset);
    ControlNode.AddControlled(builder, controlledOffset);
    ControlNode.AddControlName(builder, controlNameOffset);
    return ControlNode.EndControlNode(builder);
  }

  public static void StartControlNode(FlatBufferBuilder builder) { builder.StartObject(7); }
  public static void AddOID(FlatBufferBuilder builder, long OID) { builder.AddLong(0, OID, 0); }
  public static void AddControlName(FlatBufferBuilder builder, StringOffset controlNameOffset) { builder.AddOffset(1, controlNameOffset.Value, 0); }
  public static void AddControlled(FlatBufferBuilder builder, VectorOffset controlledOffset) { builder.AddOffset(2, controlledOffset.Value, 0); }
  public static VectorOffset CreateControlledVector(FlatBufferBuilder builder, Offset<ControlNode>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartControlledVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddControlPoints(FlatBufferBuilder builder, VectorOffset controlPointsOffset) { builder.AddOffset(3, controlPointsOffset.Value, 0); }
  public static VectorOffset CreateControlPointsVector(FlatBufferBuilder builder, Offset<ControlPointMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartControlPointsVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddParent(FlatBufferBuilder builder, Offset<ControlNode> parentOffset) { builder.AddOffset(4, parentOffset.Value, 0); }
  public static void AddTaxRate(FlatBufferBuilder builder, int taxRate) { builder.AddInt(5, taxRate, 0); }
  public static void AddTreeId(FlatBufferBuilder builder, int treeId) { builder.AddInt(6, treeId, 0); }
  public static Offset<ControlNode> EndControlNode(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<ControlNode>(o);
  }
};


}
