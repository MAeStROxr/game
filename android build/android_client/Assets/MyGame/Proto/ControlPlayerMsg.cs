// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class ControlPlayerMsg : Table {
  public static ControlPlayerMsg GetRootAsControlPlayerMsg(ByteBuffer _bb) { return GetRootAsControlPlayerMsg(_bb, new ControlPlayerMsg()); }
  public static ControlPlayerMsg GetRootAsControlPlayerMsg(ByteBuffer _bb, ControlPlayerMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public ControlPlayerMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public ControlPlayerCmd Cmd { get { int o = __offset(4); return o != 0 ? (ControlPlayerCmd)bb.GetSbyte(o + bb_pos) : ControlPlayerCmd.PLAYER_CONTROL_INFO; } }
  public bool MutateCmd(ControlPlayerCmd cmd) { int o = __offset(4); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)cmd); return true; } else { return false; } }
  public long SubjectOID { get { int o = __offset(6); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateSubjectOID(long subjectOID) { int o = __offset(6); if (o != 0) { bb.PutLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public int TaxRate { get { int o = __offset(8); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateTaxRate(int taxRate) { int o = __offset(8); if (o != 0) { bb.PutInt(o + bb_pos, taxRate); return true; } else { return false; } }
  public ControlNode NodeMsg { get { return GetNodeMsg(new ControlNode()); } }
  public ControlNode GetNodeMsg(ControlNode obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public string SubjectName { get { int o = __offset(12); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetSubjectNameBytes() { return __vector_as_arraysegment(12); }
  public long TargetOID { get { int o = __offset(14); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateTargetOID(long targetOID) { int o = __offset(14); if (o != 0) { bb.PutLong(o + bb_pos, targetOID); return true; } else { return false; } }
  public int FactionId { get { int o = __offset(16); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateFactionId(int factionId) { int o = __offset(16); if (o != 0) { bb.PutInt(o + bb_pos, factionId); return true; } else { return false; } }

  public static Offset<ControlPlayerMsg> CreateControlPlayerMsg(FlatBufferBuilder builder,
      ControlPlayerCmd cmd = ControlPlayerCmd.PLAYER_CONTROL_INFO,
      long subjectOID = 0,
      int taxRate = 0,
      Offset<ControlNode> nodeMsgOffset = default(Offset<ControlNode>),
      StringOffset subjectNameOffset = default(StringOffset),
      long targetOID = 0,
      int factionId = 0) {
    builder.StartObject(7);
    ControlPlayerMsg.AddTargetOID(builder, targetOID);
    ControlPlayerMsg.AddSubjectOID(builder, subjectOID);
    ControlPlayerMsg.AddFactionId(builder, factionId);
    ControlPlayerMsg.AddSubjectName(builder, subjectNameOffset);
    ControlPlayerMsg.AddNodeMsg(builder, nodeMsgOffset);
    ControlPlayerMsg.AddTaxRate(builder, taxRate);
    ControlPlayerMsg.AddCmd(builder, cmd);
    return ControlPlayerMsg.EndControlPlayerMsg(builder);
  }

  public static void StartControlPlayerMsg(FlatBufferBuilder builder) { builder.StartObject(7); }
  public static void AddCmd(FlatBufferBuilder builder, ControlPlayerCmd cmd) { builder.AddSbyte(0, (sbyte)cmd, 0); }
  public static void AddSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.AddLong(1, subjectOID, 0); }
  public static void AddTaxRate(FlatBufferBuilder builder, int taxRate) { builder.AddInt(2, taxRate, 0); }
  public static void AddNodeMsg(FlatBufferBuilder builder, Offset<ControlNode> nodeMsgOffset) { builder.AddOffset(3, nodeMsgOffset.Value, 0); }
  public static void AddSubjectName(FlatBufferBuilder builder, StringOffset subjectNameOffset) { builder.AddOffset(4, subjectNameOffset.Value, 0); }
  public static void AddTargetOID(FlatBufferBuilder builder, long targetOID) { builder.AddLong(5, targetOID, 0); }
  public static void AddFactionId(FlatBufferBuilder builder, int factionId) { builder.AddInt(6, factionId, 0); }
  public static Offset<ControlPlayerMsg> EndControlPlayerMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<ControlPlayerMsg>(o);
  }
};


}
