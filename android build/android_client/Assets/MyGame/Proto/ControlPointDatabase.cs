// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class ControlPointDatabase : Table {
  public static ControlPointDatabase GetRootAsControlPointDatabase(ByteBuffer _bb) { return GetRootAsControlPointDatabase(_bb, new ControlPointDatabase()); }
  public static ControlPointDatabase GetRootAsControlPointDatabase(ByteBuffer _bb, ControlPointDatabase obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public ControlPointDatabase __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public ControlPointMsg GetControlPoints(int j) { return GetControlPoints(new ControlPointMsg(), j); }
  public ControlPointMsg GetControlPoints(ControlPointMsg obj, int j) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int ControlPointsLength { get { int o = __offset(4); return o != 0 ? __vector_len(o) : 0; } }

  public static Offset<ControlPointDatabase> CreateControlPointDatabase(FlatBufferBuilder builder,
      VectorOffset controlPointsOffset = default(VectorOffset)) {
    builder.StartObject(1);
    ControlPointDatabase.AddControlPoints(builder, controlPointsOffset);
    return ControlPointDatabase.EndControlPointDatabase(builder);
  }

  public static void StartControlPointDatabase(FlatBufferBuilder builder) { builder.StartObject(1); }
  public static void AddControlPoints(FlatBufferBuilder builder, VectorOffset controlPointsOffset) { builder.AddOffset(0, controlPointsOffset.Value, 0); }
  public static VectorOffset CreateControlPointsVector(FlatBufferBuilder builder, Offset<ControlPointMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartControlPointsVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static Offset<ControlPointDatabase> EndControlPointDatabase(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<ControlPointDatabase>(o);
  }
};


}
