// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class TacticalPlayerMsg : Table {
  public static TacticalPlayerMsg GetRootAsTacticalPlayerMsg(ByteBuffer _bb) { return GetRootAsTacticalPlayerMsg(_bb, new TacticalPlayerMsg()); }
  public static TacticalPlayerMsg GetRootAsTacticalPlayerMsg(ByteBuffer _bb, TacticalPlayerMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public TacticalPlayerMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public TacticalPlayerCmd Cmd { get { int o = __offset(4); return o != 0 ? (TacticalPlayerCmd)bb.GetSbyte(o + bb_pos) : TacticalPlayerCmd.PLAYER_TACTICAL_INFO; } }
  public bool MutateCmd(TacticalPlayerCmd cmd) { int o = __offset(4); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)cmd); return true; } else { return false; } }
  public long SubjectOID { get { int o = __offset(6); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateSubjectOID(long subjectOID) { int o = __offset(6); if (o != 0) { bb.PutLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public ControlNode PlayerControl { get { return GetPlayerControl(new ControlNode()); } }
  public ControlNode GetPlayerControl(ControlNode obj) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public DataAction Action { get { int o = __offset(10); return o != 0 ? (DataAction)bb.GetSbyte(o + bb_pos) : DataAction.NONE; } }
  public bool MutateAction(DataAction action) { int o = __offset(10); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)action); return true; } else { return false; } }
  public string SubjectName { get { int o = __offset(12); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetSubjectNameBytes() { return __vector_as_arraysegment(12); }
  public ArmyMsg Army { get { return GetArmy(new ArmyMsg()); } }
  public ArmyMsg GetArmy(ArmyMsg obj) { int o = __offset(14); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static Offset<TacticalPlayerMsg> CreateTacticalPlayerMsg(FlatBufferBuilder builder,
      TacticalPlayerCmd cmd = TacticalPlayerCmd.PLAYER_TACTICAL_INFO,
      long subjectOID = 0,
      Offset<ControlNode> playerControlOffset = default(Offset<ControlNode>),
      DataAction action = DataAction.NONE,
      StringOffset subjectNameOffset = default(StringOffset),
      Offset<ArmyMsg> armyOffset = default(Offset<ArmyMsg>)) {
    builder.StartObject(6);
    TacticalPlayerMsg.AddSubjectOID(builder, subjectOID);
    TacticalPlayerMsg.AddArmy(builder, armyOffset);
    TacticalPlayerMsg.AddSubjectName(builder, subjectNameOffset);
    TacticalPlayerMsg.AddPlayerControl(builder, playerControlOffset);
    TacticalPlayerMsg.AddAction(builder, action);
    TacticalPlayerMsg.AddCmd(builder, cmd);
    return TacticalPlayerMsg.EndTacticalPlayerMsg(builder);
  }

  public static void StartTacticalPlayerMsg(FlatBufferBuilder builder) { builder.StartObject(6); }
  public static void AddCmd(FlatBufferBuilder builder, TacticalPlayerCmd cmd) { builder.AddSbyte(0, (sbyte)cmd, 0); }
  public static void AddSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.AddLong(1, subjectOID, 0); }
  public static void AddPlayerControl(FlatBufferBuilder builder, Offset<ControlNode> playerControlOffset) { builder.AddOffset(2, playerControlOffset.Value, 0); }
  public static void AddAction(FlatBufferBuilder builder, DataAction action) { builder.AddSbyte(3, (sbyte)action, 0); }
  public static void AddSubjectName(FlatBufferBuilder builder, StringOffset subjectNameOffset) { builder.AddOffset(4, subjectNameOffset.Value, 0); }
  public static void AddArmy(FlatBufferBuilder builder, Offset<ArmyMsg> armyOffset) { builder.AddOffset(5, armyOffset.Value, 0); }
  public static Offset<TacticalPlayerMsg> EndTacticalPlayerMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<TacticalPlayerMsg>(o);
  }
};


}
