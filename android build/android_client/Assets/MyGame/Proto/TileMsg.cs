// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class TileMsg : Table {
  public static TileMsg GetRootAsTileMsg(ByteBuffer _bb) { return GetRootAsTileMsg(_bb, new TileMsg()); }
  public static TileMsg GetRootAsTileMsg(ByteBuffer _bb, TileMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public TileMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public int TileId { get { int o = __offset(4); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateTileId(int tileId) { int o = __offset(4); if (o != 0) { bb.PutInt(o + bb_pos, tileId); return true; } else { return false; } }
  public bool Walkable { get { int o = __offset(6); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateWalkable(bool walkable) { int o = __offset(6); if (o != 0) { bb.Put(o + bb_pos, (byte)(walkable ? 1 : 0)); return true; } else { return false; } }

  public static Offset<TileMsg> CreateTileMsg(FlatBufferBuilder builder,
      int tileId = 0,
      bool walkable = false) {
    builder.StartObject(2);
    TileMsg.AddTileId(builder, tileId);
    TileMsg.AddWalkable(builder, walkable);
    return TileMsg.EndTileMsg(builder);
  }

  public static void StartTileMsg(FlatBufferBuilder builder) { builder.StartObject(2); }
  public static void AddTileId(FlatBufferBuilder builder, int tileId) { builder.AddInt(0, tileId, 0); }
  public static void AddWalkable(FlatBufferBuilder builder, bool walkable) { builder.AddBool(1, walkable, false); }
  public static Offset<TileMsg> EndTileMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<TileMsg>(o);
  }
};


}
