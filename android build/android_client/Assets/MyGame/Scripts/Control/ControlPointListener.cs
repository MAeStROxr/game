﻿using UnityEngine;
using System.Collections;
using TBTK;

public class ControlPointListener : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
    public ControlPoint point;
    bool mouseOver = false;

    void Update() {
        //friendly = IsFriendly(GameMainframe.clientOID);
        if (mouseOver && ControlFreak2.CF2Input.GetMouseButtonDown(1)) {
            point.ShowGUI();
        }

    }

    void OnMouseOver() {
        mouseOver = true;
    }

    void OnMouseExit() {
        mouseOver = false;
    }
}
