﻿using UnityEngine;
using System.Collections;
using mygame.proto;
using System.Collections.Generic;
using NT;
using System;
using FlatBuffers;
using TBTK;
#pragma warning disable 0114
public class ControlPointManager : MonoBehaviour
{
    static private Dictionary<long, ControlPoint> controlPoints = new Dictionary<long, ControlPoint>();
    private long clientOID { get { return GameMainframe.GetInstance().clientOID; } }
    private string clientName;
    static ControlPointManager instance;
    public PlayerManager playerManager;
    Proto proto = new Proto();
    bool isConnected = false;
    void Awake() {
        instance = this;
        foreach (KeyValuePair<long, ControlPoint> pair in controlPoints) {
            pair.Value.pointManager = this;
        }
        clientName = "player";
        
    }

    public void Start() {
        NetworkAPI.RegisterExtensionMessageHandler
              (Control.CONTROL_POINT.ToString(), RecievedPointMsg);
        
    }

    public void OnLevelWasLoaded(int level) {
        if (level == 1) {

            clientName = GameMainframe.GetInstance().clientName;
            isConnected = true;
        }
    }


    public List<ArmyMsg> pointGarrisonedArmies(ControlPointMsg pointMsg) {
        List<ArmyMsg> armies = new List<ArmyMsg>();
        for (int i = 0; i < pointMsg.GarrisonedArmiesLength; i++)
            armies.Add(pointMsg.GetGarrisonedArmies(i));
        return armies;
    }
    public static ControlPointManager GetInstance() { return instance; }
    public void RecievedPointMsg(Dictionary<string, object> props) {
        byte[] data = (byte[])props["ControlPointMsg"];
        ByteBuffer buffer = new ByteBuffer(data);
        buffer.Position = (int)props["BufferPosition"];
        ControlPointMsg pointMsg = ControlPointMsg.GetRootAsControlPointMsg(buffer);
        Debug.Log("Recieved ControlPointMsg: pointOID-" + pointMsg.OID + " subjectOID-" + pointMsg.SubjectOID + " pointCmd-" + pointMsg.Cmd.ToString());
        ProcessPointMsg(pointMsg);
    }

    public void ProcessPointMsg(ControlPointMsg pointMsg) {
        ControlPoint cp = controlPoints[pointMsg.OID];
        switch (pointMsg.Cmd) {
            case PointCmd.POINT_INFO:
            case PointCmd.POINT_ARMY_GARRISON:
            case PointCmd.POINT_UNIT_GARRISON:
                UpdatePoint(pointMsg,cp);
                break;
            case PointCmd.POINT_CAPTURE:
                PointCaptured(pointMsg, cp);
                break;
            case PointCmd.POINT_ENABLE:
                PointEnabled(pointMsg, cp);
                break;
            case PointCmd.POINT_SIEGE_RESULT:
                SiegeResult(pointMsg,cp);
                break;
        }
    }

    private void SiegeResult(ControlPointMsg pointMsg, ControlPoint cp) {
       
        List<ArmyMsg> newArmiesGarrison = proto.getPointArmiesGarrison(pointMsg);
        
        UpdatePoint(pointMsg,cp);
        

    }

    public void UpdatePoint(ControlPointMsg pointMsg, ControlPoint cp) {
        Debug.Log("Update Point Info Received: " + pointMsg);
        switch (pointMsg.Cmd) {

            case PointCmd.POINT_INFO:
                addMsgPlayers(pointMsg);
                List<ArmyMsg> armies = pointGarrisonedArmies(pointMsg);
                armies.ForEach(a => cp.GeneralGarrisoned(a));
                cp.SetGarrison(pointMsg.Garrison);

                cp.UpdateControlPoint(playerManager.GetPlayer(pointMsg.ControllingOID),
                    playerManager.GetPlayer(pointMsg.EnablingOID), pointMsg.Capturable);
                cp.hostile = pointMsg.ControllingOID != clientOID;
                cp.friendly = pointMsg.ControllingOID == clientOID;
                break;

            case PointCmd.POINT_ARMY_GARRISON:
                if (pointMsg.Action == DataAction.REMOVE) {
                    List<ArmyMsg> garrisoned = cp.GetGarrisonedArmies(),msgArmies = pointGarrisonedArmies(pointMsg);
                    garrisoned.ForEach(garrisonedArmy => 
                    {
                        if (!msgArmies.Exists(msgArmy => msgArmy.OID == garrisonedArmy.OID))
                            cp.GeneralRemoved(garrisonedArmy.OID);
                    });

                }
                if (pointMsg.Action == DataAction.ADD) {
                    pointGarrisonedArmies(pointMsg).ForEach(army => cp.GeneralGarrisoned(army));
                }
                break;
            case PointCmd.POINT_UNIT_GARRISON:
                cp.SetGarrison(pointMsg.Garrison);
                break;
                

        }
        cp.pointUI.updateGarrisonUI(cp.GetGarrisonTotal());
    }

    internal static void GetPointInfo(ControlPoint controlPoint) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        StringOffset pointName = fbb.CreateString(controlPoint.name);
        StringOffset subjectName = fbb.CreateString(GameMainframe.GetInstance().clientName);
        Proto.addControlPoint(fbb, PointCmd.POINT_INFO, controlPoint.pointOID, GameMainframe.GetInstance().clientOID);
        ControlPointMsg.AddName(fbb, pointName);
        ControlPointMsg.AddSubjectName(fbb, pointName);
        ControlPointMsg.AddAction(fbb, DataAction.GET);
        SendControlPointMsg(fbb);
    }
    public void UnitsGarrisonUpdated(ControlPointMsg pointMsg, ControlPoint cp) {
        cp.SetGarrison(pointMsg.Garrison);
    }
    public void UpdateUnitsGarrison(ControlPoint point, List<UnitMsg> newInv, List<UnitMsg> newGarrison) {
        List<UnitMsg> oldInv = BattleManager.getUnitsMsgsFromInv();

        List<UnitMsg> list = new List<UnitMsg>();
        foreach (UnitMsg u in newInv) {
            if (!oldInv.Exists(unit => unit.OID == u.OID))
                list.Add(u);
        }
        BattleManager.addUnitsToInventory(list);
        list.Clear();
        foreach (UnitMsg un in oldInv) {
            if (!newInv.Exists(unit => unit.OID == un.OID))
                list.Add(un);
        }
        BattleManager.DeleteUnitsFromInv(list);
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<ArmyMsg> myArmy = Proto.addArmy(fbb, clientOID,
                    clientName, newGarrison);
        Proto.addControlPoint(fbb, PointCmd.POINT_UNIT_GARRISON, point.pointOID, clientOID);
        ControlPointMsg.AddGarrison(fbb, myArmy);
        ControlPointMsg.AddAction(fbb, DataAction.REPLACE);
        SendControlPointMsg(fbb);

    }

    public void GarrisonGeneral(ControlPoint point, DataAction action) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);

        Proto.addArmy(fbb, clientOID,
                    clientName, BattleManager.getUnitsMsgsFromInv(), false);
        Vector3 p=GameMainframe.GetInstance().GetClientObject().transform.position;
        ArmyMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, p.x, p.y, p.z));
        ArmyMsg.AddIsMainArmy(fbb,true);
        Offset<ArmyMsg> myArmy = ArmyMsg.EndArmyMsg(fbb);
        VectorOffset vec = ControlPointMsg.CreateGarrisonedArmiesVector(fbb, new Offset<ArmyMsg>[] { myArmy });
        Proto.addControlPoint(fbb, PointCmd.POINT_ARMY_GARRISON, point.pointOID, clientOID);
        ControlPointMsg.AddGarrisonedArmies(fbb, vec);

        ControlPointMsg.AddAction(fbb, action);
        SendControlPointMsg(fbb);
    }

    static void SendControlPointMsg(FlatBufferBuilder fbb) {
        Offset<ControlPointMsg> capMsgOffset = ControlPointMsg.EndControlPointMsg(fbb);
        fbb.Finish(capMsgOffset.Value);
        ControlPointMsg pointMsg = ControlPointMsg.GetRootAsControlPointMsg(fbb.DataBuffer);
        if (GameMainframe.GetInstance().debug) {
            instance.ProcessPointMsg(pointMsg);
            return;
        }
        Debug.Log("Sending control point msg:" + pointMsg.Cmd.ToString() + " pointOID:" + pointMsg.OID + " subject:" + pointMsg.SubjectOID + " " + pointMsg.SubjectName);
        Proto.SendExtensionMessage("ControlPointMsg", pointMsg.ByteBuffer.Data,
                 pointMsg.ByteBuffer.Position, "control." + Control.CONTROL_POINT.ToString());
    }

    public void CapturePoint(ControlPoint point) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Proto.addControlPoint(fbb, PointCmd.POINT_CAPTURE, point.pointOID, clientOID);
        ControlPointMsg.AddCaptured(fbb, true);
        ControlPointMsg.AddCapturable(fbb, false);
        SendControlPointMsg(fbb);
    }

    public void SiegePoint(ControlPoint point) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        

        BattleManager battleMngr = BattleManager.GetInstance();

        List<DataUnit> invUnits = battleMngr.getUnitsDataFromInv();
        List<Tile> tilesData = battleMngr.battlePrefab.factionManager.factionList[0].deployableTileList;
        Offset<ArmyMsg>[] armies = new Offset<ArmyMsg>[1];
        int i = 0;
        bool requireDeployment = false;
        if (invUnits.Count > 0) requireDeployment = true;
        VectorOffset unitsOffset = ArmyMsg.CreateArmyUnitsVector(fbb, proto.addDataUnitList(fbb, invUnits));
        ArmyMsg.StartArmyMsg(fbb);
        ArmyMsg.AddOID(fbb, GameMainframe.GetInstance().clientOID);
        ArmyMsg.AddArmyUnits(fbb, unitsOffset);
        
        ArmyMsg.AddDataID(fbb, 0);
        fbb.ForceDefaults = true;
        ArmyMsg.AddFactionID(fbb, 0);
        ArmyMsg.AddIsNetworkPlayer(fbb, false);
        ArmyMsg.AddIsPlayerFaction(fbb, true);
        ArmyMsg.AddIsMainArmy(fbb, true);
        ArmyMsg.AddRequireDeployment(fbb, requireDeployment);
        fbb.ForceDefaults = false;
        Vector3 p = GameMainframe.GetInstance().GetClientObject().transform.position;
        Offset<Vec3> playerPosition = Vec3.CreateVec3(fbb, p.x, p.y, p.z);
        ArmyMsg.AddPosition(fbb, playerPosition);
        armies[i++] = ArmyMsg.EndArmyMsg(fbb);
        VectorOffset armiesOffset = ControlPointMsg.CreateGarrisonedArmiesVector(fbb, armies);
        Proto.addControlPoint(fbb, PointCmd.POINT_SIEGE, point.pointOID, clientOID);
        Vector3 v = point.transform.position;
        ControlPointMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, v.x, v.y, v.z));
        ControlPointMsg.AddCaptured(fbb, true);
        ControlPointMsg.AddCapturable(fbb, false);
        ControlPointMsg.AddArmies(fbb, armiesOffset);


        SendControlPointMsg(fbb);
    }

    private void addMsgPlayers(ControlPointMsg msg) {
        if (msg.SubjectOID > 0) PlayerManager.AddPlayer(msg.SubjectOID, msg.SubjectName);
        if (msg.ControllingOID > 0) PlayerManager.AddPlayer(msg.ControllingOID, msg.ControllingName);
        if (msg.EnablingOID > 0) PlayerManager.AddPlayer(msg.EnablingOID, msg.EnablingName);
    }
    public void PointCaptured(ControlPointMsg msg, ControlPoint cp) {
        Debug.Log("Captured Point Received: captured-" + msg.Captured + " capturable-" + msg.Capturable);
        bool pointCaptured = msg.Captured;
        //&& msg.SubjectOID == clientOID
        if (!pointCaptured) {
            Debug.Log("Unable to capture point " + msg.OID);
            return;
        }
        addMsgPlayers(msg);
        //msg.MutateSubjectOID(GameMainframe.clientOID++);
        Player capturer = playerManager.GetPlayer(msg.SubjectOID);

        if (capturer.PlayerNode == null) playerManager.AddPlayerToTree(capturer);
        capturer.CapturePoint(cp);
        cp.UpdateControlPoint(playerManager.GetPlayer(msg.ControllingOID), playerManager.GetPlayer(msg.EnablingOID), msg.Capturable);
        playerManager.UpdateTree();
        Debug.Log("point captured: " + msg.OID);
        //ControlNetwork.SendUpdateTree();
    }
    public static void RegisterControlPoint(ControlPoint point) {
        if (!controlPoints.ContainsKey(point.pointOID))
            controlPoints.Add(point.pointOID, point);
        if (instance != null)
            point.pointManager = instance;
        GetPointInfo(point);
    }


    internal void PointEnabled(ControlPointMsg msg, ControlPoint cp) {
        throw new NotImplementedException();
    }

    internal void SendPointInfo(ControlPoint controlPoint, Control cONTROL_POINT, ArmyMsg army, DataAction rEPLACE) {
        throw new NotImplementedException();
    }

    internal bool IsFriendly(ControlPoint point, long playerOID) {

        return playerManager.AreFriendly(point.GetControllingPlayer().getOID(), playerOID);
    }
}
