﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using NT;
using TBTK;
using System.Linq;
using mygame.proto;
using FlatBuffers;

namespace TBTK
{

    public class ControlPointUI : MonoBehaviour
    {
        // Inventory inv;
        private int selectedIdFrom = 0;
        private int selectedIdTo = 0;
        public Transform anchor;

        public List<UnityButton> unitIconList = new List<UnityButton>();
        public List<Unit> unitsFrom = new List<Unit>();
        public List<Unit> unitsTo = new List<Unit>();
        public List<UnityButton> unitsToList = new List<UnityButton>();

        private bool transferInProgress = false, showGUI = true;

        public GameObject uiTransfer, pointStatus;

        public GridLayoutGroup garrisonGrid;
       
        public ControlPoint point;
        public bool debug = false;
        public List<GameObject> garrisonObjects = new List<GameObject>();

        public UnityButton ok, cancel, garrisonUnits, garrisonGeneral
            , capture, enable, garrisonDisplayButton, buttonAutoDoneObject, X;


        public Text garrisonGeneralText, captureText, pointStatusText, fromCount, toCount,pointTitle;
        long clientOID { get { return GameMainframe.GetInstance().clientOID; } }
        public void Awake() {

            
            //uiTransfer = gameObject;

            for (int i = 0; i < unitIconList.Count; i++) {
                unitIconList[i].Init();
            }
            for (int i = 0; i < unitsToList.Count; i++) {
                unitsToList[i].Init();
            }

            UnityButton[] buttons = new UnityButton[] { ok, cancel, garrisonUnits, garrisonGeneral
            , capture, enable,  buttonAutoDoneObject, garrisonDisplayButton,X };


            foreach (UnityButton b in buttons) b.Init();
            //garrisonDisplayButton = X.Clone("grid display", X.rootT.position);
            //garrisonDisplayButton.button.onClick.RemoveAllListeners();

        }

        void Start() {
            SetPoint(point);
            showGUI = false;
            Hide();
            ok.rootObj.SetActive(false);
            cancel.rootObj.SetActive(false);

            // anchor.position = new Vector3(Screen.width / 2, 0, 0);
        }

        void Update() {

            pointTitle.text = point.name == null? "NULL":point.name;
            if (point.friendly && !point.inSiege) {

                if (point.GetControllingPlayer() !=null && clientOID == point.GetControllingPlayer().getOID()) {
                    garrisonUnits.rootObj.SetActive(true);
                }
                else
                    garrisonUnits.rootObj.SetActive(false);



                if (point.stationed)
                    garrisonGeneralText.text = "Remove\nGeneral";
                else
                    garrisonGeneralText.text = "Garrison\nGeneral";
                garrisonGeneral.rootObj.SetActive(true);

            }
            else {
                garrisonGeneral.rootObj.SetActive(false);
                garrisonUnits.rootObj.SetActive(false);
            }



            if (point.capturable && !point.inSiege) {
                capture.rootObj.SetActive(true);
                if (point.NeedSiege())
                    captureText.text = "Siege";
                else
                    captureText.text = "Capture";
            }
            else
                capture.rootObj.SetActive(false);
            string controlling = point.GetControllingPlayer() == null ? "No Controller" : point.GetControllingPlayer().GetName();
            string enabling = point.GetEnablingPlayer() == null ? "No Controller" : point.GetEnablingPlayer().GetName();

            pointStatusText.text = "In Control: " + controlling +
                "\nDeployed: " + enabling + "\nCapturable: " + point.capturable;
        }
        public void updateGarrisonUI(List<ArmyMsg> armies) {
            //if (!showGUI) return;
            clearGarrisonUI();
            fillGarrisonUI(armies);
        }
        public void SetPoint(ControlPoint point) {
            //debug = point.battleMngr.debug;

            this.point = point;
        }

        public void fillGarrisonUI(List<ArmyMsg> armies) {

            foreach (ArmyMsg army in armies) {
                List<Unit> units = BattleManager.GetUnits(Proto.getArmyUnits(army));
                foreach (Unit u in units) {
                    UnityButton newButton = garrisonDisplayButton.Clone(u.name, new Vector3());
                    newButton.rootT.SetParent(garrisonGrid.transform);

                    newButton.label.text = u.name;
                    newButton.imageIcon.sprite = u.iconSprite;

                    //newButton.imageBG.color = Color.clear;
                    //newButton.button.onClick.RemoveAllListeners();
                    garrisonObjects.Add(newButton.rootObj);
                    newButton.rootObj.SetActive(true);
                    //garrisonObjects.Add(u.gameObject);
                }
            }
        }

        public void clearGarrisonUI() {

            foreach (GameObject o in garrisonObjects) Destroy(o);
            garrisonObjects.Clear();

        }




        public void GarrisonUnits() {
            List<UnitMsg> invUnits = BattleManager.getUnitsMsgsFromInv();
            //SetLocation(gameObject.transform);
            pointStatus.SetActive(false);
            uiTransfer.SetActive(true);
            ok.rootObj.SetActive(true);
            cancel.rootObj.SetActive(true);
            TransferUnits(invUnits, Proto.getArmyUnits((point.GetGarrison())), point.OnTransferComplete, point.pointOID);

            //uiTransfer.SetActive(true);
        }

        public void GarrisonGeneral() {
            if (point.stationed) {
                Debug.Log("Clicked the control point remove garrison button");
                point.RemoveGeneral();

            }
            else {
                Debug.Log("Clicked the control point garrison general button");


                //myArmy.ArmyUnits = point.battleMngr.getUnitsDataFromInv().Cast<UnitMsg>().ToList();
                point.GarrisonGeneral();

            }
        }

        public void CapturePoint() {
            Debug.Log("Clicked the control point capture button");
            point.Capture();
        }

        public int currentUnitID = 0;



        void OnEnable() {
            // FactionManager.onUnitDeployedE += OnUnitDeployed;
        }
        void OnDisable() {
            //FactionManager.onUnitDeployedE -= OnUnitDeployed;
        }

        public void OnUnitDeployed(Unit unit) {
            // UpdateView();
        }

        public void OnNextButton() {
            if (unitsFrom.Count == 0) {
                selectedIdFrom = 0;
                return;
            }
            selectedIdFrom++;
            if (selectedIdFrom >= unitsFrom.Count) selectedIdFrom = 0;
            UpdateView(true);
            //UpdateView(unitsTo, unitsFrom);
        }
        public void OnPrevButton() {
            if (unitsFrom.Count == 0) {
                selectedIdFrom = 0;
                return;
            }
            selectedIdFrom--;
            if (selectedIdFrom < 0) selectedIdFrom = unitsFrom.Count - 1;
            UpdateView(true);
            //UpdateView(unitsTo, unitsFrom);
        }

        public void OnNextToButton() {
            if (unitsTo.Count == 0) {
                selectedIdFrom = 0;
                return;
            }
            selectedIdTo++;
            if (selectedIdTo >= unitsTo.Count) selectedIdTo = 0;
            //UpdateView(unitsFrom, unitsTo);
            UpdateView(false);
        }
        public void OnPrevToButton() {
            if (unitsTo.Count == 0) {
                selectedIdFrom = 0;
                return;
            }
            selectedIdTo--;
            if (selectedIdTo < 0) selectedIdTo = unitsTo.Count - 1;
            //UpdateView(unitsFrom, unitsTo);
            UpdateView(false);
        }
        public void OnCompleteTransfer() {
            List<UnitMsg> newFrom = new List<UnitMsg>(), newTo = new List<UnitMsg>();

            unitsFrom.ForEach(unit => newFrom.Add(unit.GetData().GetMsg()));
            unitsTo.ForEach(unit => newTo.Add(unit.GetData().GetMsg()));

            //Hide();
            if (OnCompleteEvent!=null) OnCompleteEvent.Invoke(newFrom, newTo);
            destroyObjects();
            OnCompleteEvent = null;

            ok.rootObj.SetActive(false);
            cancel.rootObj.SetActive(false);
            pointStatus.SetActive(true);
            transferInProgress = false;
            uiTransfer.SetActive(transferInProgress);
        }

        public void OnCancelTransfer() {
            Hide();
            destroyObjects();
            uiTransfer.SetActive(false);
            pointStatus.SetActive(true);

        }

        private void destroyObjects() {
            unitsFrom.ForEach(unit => Destroy(unit.gameObject));
            unitsTo.ForEach(unit => Destroy(unit.gameObject));
            unitsFrom.Clear();
            unitsTo.Clear();
        }
        public  void SetLocation(Transform t) {
            gameObject.transform.parent.position = t.transform.position + new Vector3(0, 2f, -2.8f);
            gameObject.transform.rotation = t.transform.rotation;
            //this.unitsFromPanel = unitsFromPanel;
            //this.unitsToPanel = unitsToPanel;
        }

        public delegate void TransferComplete(List<UnitMsg> newGarrison, List<UnitMsg> newInv);
        public event TransferComplete OnCompleteEvent;

        public  void TransferUnits(List<UnitMsg> unitsFrom, List<UnitMsg> unitsTo, TransferComplete onComplete, long invoker) {

            /*
            if (transferInProgress && invoker != lastInvoker) {
                //destroyObjects();
            }
            else if (transferInProgress) {
                //destroyObjects();
                Hide();
                return;
            }*/

            transferInProgress = true;
            uiTransfer.SetActive(transferInProgress);

            OnCompleteEvent += onComplete;
            this.unitsFrom.Clear();
            this.unitsTo.Clear();
            this.unitsFrom.AddRange(BattleManager.GetUnits(unitsFrom, true));
            this.unitsTo.AddRange(BattleManager.GetUnits(unitsTo, true));
            selectedIdFrom = 0;
            selectedIdTo = 0;

            UpdateView(true);
            UpdateView(false);

        }
        private UnityEngine.Events.UnityAction buttonClick(bool isFrom, Unit unit) {
            UnityEngine.Events.UnityAction action = (() =>
            {
                string unitName = unit.name;
                Debug.Log("Unit " + name + " Clicled");
                moveUnit(isFrom, unit);

            });
            return action;
        }
        private void moveUnit(bool isFrom, Unit unit) {
            if (isFrom) {
                unitsFrom.Remove(unit);
                unitsTo.Add(unit);
                unitsFrom.RemoveAll(u => u == null);
            }
            else {
                unitsTo.Remove(unit);
                unitsFrom.Add(unit);
                unitsTo.RemoveAll(u => u == null);
            }
            selectedIdFrom = 0;
            selectedIdTo = 0;
            UpdateView(false);
            UpdateView(true);
        }

        private void setButtonUnit(UnityButton button, Unit unit) {
            button.button.onClick.RemoveAllListeners();
            if (unit == null) {
                button.imageIcon.sprite = null;
                button.rootObj.SetActive(false);
                return;
            }
            if (unitsFrom.Contains(unit)) {
                button.button.onClick.AddListener
                    (buttonClick(true, unit));
                button.rootObj.SetActive(true);
                button.imageIcon.sprite = unit.iconSprite;

            }
            else if (unitsTo.Contains(unit)) {
                button.button.onClick.AddListener
                    (buttonClick(false, unit));
                button.rootObj.SetActive(true);
                button.imageIcon.sprite = unit.iconSprite;
            }
            else {
                button.imageIcon.sprite = null;
                button.rootObj.SetActive(false);
            }
        }

        int listSize = 5;
        public void UpdateView(bool isFrom) {
            List<Unit> unitList, unitListTo;
            List<UnityButton> _unitIconList;
            int index = 0;

            if (isFrom) {
                unitList = unitsFrom;
                unitListTo = unitsTo;
                _unitIconList = this.unitIconList;
                index = selectedIdFrom;
                fromCount.text = unitsFrom.Count.ToString();
            }
            else {
                unitList = unitsTo;
                unitListTo = unitsFrom;
                _unitIconList = this.unitsToList;
                index = selectedIdTo;
                toCount.text = unitsTo.Count.ToString();
            }
            int unitID = index;
            //  lbUndeployedCount.text = unitList.Count.ToString();
            if (unitList.Count == 0) {
                for (int i = 0; i < _unitIconList.Count; i++) {
                    _unitIconList[i].rootObj.SetActive(false);
                    _unitIconList[i].button.onClick.RemoveAllListeners();
                }
                return;
            }

            List<Unit> newList = new List<Unit>();
            for (int i = 0; i < unitList.Count; i++) {

                newList.Add(unitList[unitID]);
                unitID += 1;
                if (unitID >= unitList.Count) unitID = 0;
            }
            unitList = newList;

            int point = Mathf.Min(3, unitList.Count / 2 + 1);
            int count = unitList.Count >= 5 ? 2 : 1;
            newList = new List<Unit>();
            for (int i = 0; i < Mathf.Min(5, unitList.Count); i++) {
                if (i < point) {
                    newList.Add(unitList[i]);
                }
                else {
                    newList.Add(unitList[unitList.Count - count]);
                    count -= 1;
                }
            }
            unitList = newList;
            for (int i = 0; i < unitList.Count; i++) {
                setButtonUnit(_unitIconList[i], unitList[i]);
                _unitIconList[i].rootObj.SetActive(true);
            }
            for (int i = unitList.Count; i < 5; i++) _unitIconList[i].rootObj.SetActive(false);
            /*
            if (unitList.Count <= 2) {
                for (int i = 0; i < unitList.Count; i++) {
                    setButtonUnit(unitIconList[i], unitList[i]);
                }
                for (int i = unitList.Count; i < 5; i++) unitIconList[i].rootObj.SetActive(false);
            }
            else if (unitList.Count == 3) {
                for (int i = 0; i < 2; i++) {
                    setButtonUnit(unitIconList[i], unitList[i]);
                }

                setButtonUnit(unitIconList[4], unitList[2]);

                unitIconList[2].rootObj.SetActive(false);
                unitIconList[3].rootObj.SetActive(false);
            }
            else if (unitList.Count == 4) {
                for (int i = 0; i < 3; i++) {
                    setButtonUnit(unitIconList[i], unitList[i]);
                }

                setButtonUnit(unitIconList[4], unitList[3]);

                unitIconList[3].rootObj.SetActive(false);
            }
            else if (unitList.Count >= 5) {
                for (int i = 0; i < 5; i++) {
                    if (i < unitList.Count) {
                        setButtonUnit(unitIconList[i], unitList[i]);
                    }
                    else {
                        unitIconList[i].rootObj.SetActive(false);
                    }
                }
            }
            */
            buttonAutoDoneObject.label.text = "Done";




        }



        public void Show() {
            gameObject.SetActive(true);

        }

        public void Hide() {
            gameObject.SetActive(false);
            if (transferInProgress) {
                transferInProgress = false;
                uiTransfer.SetActive(transferInProgress);

                OnCompleteEvent = null;
                unitsFrom.Clear();
                unitsTo.Clear();
            }
            //    ok.gameObject.SetActive(false);
            //    cancel.gameObject.SetActive(false);
        }

    }

}