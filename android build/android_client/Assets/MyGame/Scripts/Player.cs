﻿
using mygame.proto;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace NT
{

    public class Player
    {

        private long oid;
        private List<ControlPoint> controlledPoints = new List<ControlPoint>();
        private List<ControlPoint> enabledPoints = new List<ControlPoint>();
        private Battle currentBattle;
        private ArmyMsg army;
        private Dictionary<long, UnitMsg> allUnits = new Dictionary<long, UnitMsg>();

        private int taxRate = 0;
        private String name;

        public int accumulatedGold = 0;

        public Player(long playerOid, String playerName) {
            oid = playerOid;
            name = playerName;

        }

        public long getOID() {
            return oid;
        }
        public List<ControlPoint> GetControlledPoints() {
            return controlledPoints;
        }

        public PlayerNode<Player> PlayerNode { get; set; }



        public String GetName() {
            return name;
        }

        public int GetTax() {
            return taxRate;
        }

        public void SetTax(int rate) {
            taxRate = rate;
        }

        public bool IsPlayerInTree(Player other) {
            if (PlayerNode == null || other.PlayerNode == null)
                return false;
            return PlayerNode.IsAncestor(other.PlayerNode) || other.PlayerNode.IsAncestor(PlayerNode);

        }

        public int GetTaxRate() {
            return taxRate;
        }

        public List<ControlPoint> GetEnabledPoints() {
            return enabledPoints;
        }



        public void EnablePoint(ControlPoint controlPoint) {
            enabledPoints.Add(controlPoint);
            Player currentEnabler = controlPoint.GetEnablingPlayer();
            if (currentEnabler != null) {
                currentEnabler.enabledPoints.Remove(controlPoint);
                
            }
            controlPoint.SetEnabling(this);
        }

        public void DisablePoint(ControlPoint controlPoint) {
            enabledPoints.Remove(controlPoint);
        }

        public void DiscapturePoint(ControlPoint controlPoint) {
            controlledPoints.Remove(controlPoint);
        }

        public void CapturePoint(ControlPoint controlPoint) {
            

            controlledPoints.Add(controlPoint);

            Player controller = controlPoint.GetControllingPlayer(), enabler = controlPoint.GetEnablingPlayer();
            controlPoint.SetControlling(this);
            
            Debug.Log("player capturePoint, pointInfo before:" );
            String s = "No Controller"; if (controller != null) s = controller.getOID().ToString();
            Debug.Log(" controller:" + s);
            s = "No Enabler"; if (enabler != null) s = enabler.getOID().ToString();
            Debug.Log(" enabler:" + s);
            Debug.Log(" capturing player:" + getOID().ToString());


            if (controller != null) controller.DiscapturePoint(controlPoint);
            if (enabler != null) {
                Debug.Log("enabler exists");
                if (enabler.GetEnabledPoints().Count <= 1) {
                    Debug.Log(" enabler must stay, current tree:" + PlayerNode.Root.PrintSubTree());
                    ControlPlayer(enabler);
                    //controlPlayerConsumer.accept(this, enabler);
                    Debug.Log(" enabler must stay, after tree change:" + PlayerNode.Root.PrintSubTree());
                    return;
                }
                else if (controller != null && controller.CanEnable(controlPoint)) {
                    Debug.Log("enabler can go and controller can enable");
                    enabler.DisablePoint(controlPoint);
                    controller.EnablePoint(controlPoint);
                }
            }
            else {
                Debug.Log("no enabler");
                if (controller != null && controller.CanEnable(controlPoint)) {
                    Debug.Log("controller can enable");
                    controller.EnablePoint(controlPoint);
                }
                else {
                    Debug.Log("controller is missing or can't enable");
                    if (CanEnable(controlPoint)) {
                        Debug.Log("capturing player can enable");
                        EnablePoint(controlPoint);
                    }
                }
            }

        }


        public bool CanEnable(ControlPoint point) {
            return true;
        }
        public void ControlPlayer(Player controlled) {
            PlayerNode<Player> node = controlled.PlayerNode;
            PlayerNode.MutateAddChild(node);
        }
        public bool InControl(Player isControlled) {
            return isControlled.PlayerNode.IsAncestor(PlayerNode);
        }



        public Battle GetBattle() {
            return currentBattle;
        }

        public void SetBattle(Battle battle) {
            currentBattle = battle;
        }

        public void AddUnits(List<UnitMsg> units) {
            foreach (UnitMsg unit in units)
                allUnits.Add(unit.OID, unit);
        }

        public void RemoveAll() {
            allUnits.Clear();
        }

        public void removeUnits(List<long> unitsOID) {
            foreach (long unitOID in unitsOID)
                allUnits.Remove(unitOID);
        }

        public void setArmy(ArmyMsg armyData) {

            army = armyData;
        }

        public void deployArmy(ArmyMsg army2) {
            // TODO Auto-generated method stub

        }


    }
}
