﻿using UnityEngine;
using System.Collections;
using mygame.proto;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using FlatBuffers;
using System.Linq;

namespace NT
{

    public class PlayerManager : MonoBehaviour
    {
        Dictionary<long, Player> players = new Dictionary<long, Player>();
        public WMG_Hierarchical_Tree UIcontrolTree;
        public PlayerNode<Player> controlTree = new PlayerNode<Player>(new Player(-1, "Root"));
        public ScrollRect treeView;
        private static PlayerManager Instance;
        // Use this for initialization
        void Awake() {
            if (Instance==null)
                Instance = this;
        }
        void Start() {
            //treeView.gameObject.SetActive(false);
            players[-1] = controlTree.Value;
            controlTree.Value.PlayerNode = controlTree;
            
        }
        
        // Update is called once per frame
        void Update() {

        }

        public void OnLevelWasLoaded(int level) {
            if (level == 1) {
                long p = ClientAPI.GetPlayerOid();
                string n = ClientAPI.GetPlayerObject().Name;
                AddPlayer(p,n);
                NetworkAPI.RegisterExtensionMessageHandler
               (Tactical.PLAYER_TACTICAL.ToString(), RecievedTacticalPlayerMsg);
                NetworkAPI.RegisterExtensionMessageHandler
               (Control.PLAYER_CONTROL.ToString(), RecievedControlPlayerMsg);
               SendControlUpdateRequest();
               SendTacticalUpdateRequest();
            }
           /* if (level == 2) {
                GameMainframe.clientOID = ClientAPI.GetPlayerOid();
                SendControlUpdateRequest();
                SendTacticalUpdateRequest();
            }*/
        }

        public void RecievedTacticalPlayerMsg(Dictionary<string, object> props) {
            byte[] data = (byte[])props["TacticalPlayerMsg"];
            ByteBuffer buffer = new ByteBuffer(data);
            buffer.Position = (int)props["BufferPosition"];
            TacticalPlayerMsg msg = TacticalPlayerMsg.GetRootAsTacticalPlayerMsg(buffer);
            Debug.Log("Recieved TacticalPlayerMsg: subjectOID-" + msg.SubjectOID + " Cmd-" + msg.Cmd.ToString());
            switch (msg.Cmd) {
            
                case TacticalPlayerCmd.PLAYER_UNITS:
                    PlayerUnits(msg);
                    break;
            }
        }
        public void RecievedControlPlayerMsg(Dictionary<string, object> props) {
            byte[] data = (byte[])props["ControlPlayerMsg"];
            ByteBuffer buffer = new ByteBuffer(data);
            buffer.Position = (int)props["BufferPosition"];
            ControlPlayerMsg msg = ControlPlayerMsg.GetRootAsControlPlayerMsg(buffer);
            Debug.Log("Recieved ControlPlayerMsg: subjectOID-" + msg.SubjectOID + " Cmd-" + msg.Cmd.ToString());

            switch (msg.Cmd) {
                case ControlPlayerCmd.PLAYER_CONTROL_INFO:
                    
                    PlayerControlInfo(msg.NodeMsg);
                    break;
                case ControlPlayerCmd.PLAYER_TAX:
                    PlayerTax(msg);
                    break;
       
            }
        }
        public void SendControlUpdateRequest() {
            ControlPlayerMsg controlMsg = Proto.createControlUpdateRequest();
            Proto.SendExtensionMessage("ControlPlayerMsg", controlMsg.ByteBuffer.Data, controlMsg.ByteBuffer.Position, "control." + Control.PLAYER_CONTROL.ToString());
        }
        public void SendTacticalUpdateRequest() {
            TacticalPlayerMsg tacticalMsg = Proto.createTacticalUpdateRequest();
            Proto.SendExtensionMessage("TacticalPlayerMsg", tacticalMsg.ByteBuffer.Data,
                tacticalMsg.ByteBuffer.Position, "tactical." + Tactical.PLAYER_TACTICAL.ToString());
        }

        public void ShowControlUI() {
            treeView.gameObject.SetActive(!treeView.gameObject.activeSelf);
        }

        public Player GetPlayer(long playerOID) {
            return players[playerOID];
        }
        public static void AddPlayer(long playerOID, string name) {
            Instance._AddPlayer(playerOID, name);
        }
        public void _AddPlayer(long playerOID, string name) {
            if (players.ContainsKey(playerOID)) return;
            Player p = new Player(playerOID, name);
            players[playerOID] = p;
            AddPlayerToTree(p);
        }

        internal void PlayerControlInfo(ControlNode msg) {
            TreeControlNode n = new TreeControlNode(msg);
            n.Traverse(cn => {
                if (cn.OID == -1 || cn.ControlName == "Root") return;
                AddPlayer(cn.OID, cn.ControlName);
            });
            Debug.Log("Tree Recieved:\n"+n.PrintSubTree());
            n.Traverse(controlNode =>
            {
                Player p = GetPlayer(controlNode.OID),controlled;
                for (int i = 0; i < controlNode.ControlledLength; i++) {
                    controlled = GetPlayer(controlNode.GetControlled(i).OID);
                    p.ControlPlayer(controlled);
                }
            });
            //InitPlayerTree(msg.NodeMsg);
            UpdateTree();
        }

        internal void PlayerTax(ControlPlayerMsg msg) {
            players[msg.SubjectOID].SetTax(msg.TaxRate);
        }

        public void ResetGraph() {
            UIcontrolTree.treeNodes.ForEach(g =>
            {
                if (g != null) {
                    WMG_Node n = g.GetComponent<WMG_Node>();
                    if (n != null) UIcontrolTree.DeleteNode(n);
                }
            });
            UIcontrolTree.treeLinks.ForEach(g =>
            {
                if (g != null) {
                    WMG_Link l = g.GetComponent<WMG_Link>();
                    if (l != null) UIcontrolTree.DeleteLink(l);
                }
            });
            UIcontrolTree.treeNodes.Clear();
            UIcontrolTree.treeLinks.Clear();
            UIcontrolTree.numNodes = 1;
            UIcontrolTree.numLinks = 0;
            maxCol = 0;
            UIcontrolTree.nodePrefabs.Clear();
            UIcontrolTree.nodeColumns.Clear();
            UIcontrolTree.nodeRows.Clear();
            UIcontrolTree.linkNodeFromIDs.Clear();
            UIcontrolTree.linkNodeToIDs.Clear();
        }

        public void TreeGraph(ControlNode root) {


            ResetGraph();
            InitPlayerTree(root);
            UIcontrolTree.Start();
            updateTree(root);
        }
        private void updateTree(ControlNode node) {
            if (node == null) return;

            GameObject obj = UIcontrolTree.treeNodes[node.TreeId - 1];
            Debug.Log("updating " + node.ControlName);
            Text text = obj.GetComponentInChildren<Text>();
            text.text = node.ControlName + "\nTax: " + node.TaxRate;
            for (int i = 0; i < node.ControlledLength; i++)
                updateTree(node.GetControlled(i));
            //controlTree.nodePrefabs.Add( newNode);
        }

        private void UpdateTree(PlayerNode<Player> node) {
            if (node == null) return;

            GameObject obj = UIcontrolTree.treeNodes[node.TreeId - 1];
            Debug.Log("updating " + node.Value.GetName());
            Text text = obj.GetComponentInChildren<Text>();
            text.text = node.Value.GetName() + "\nTax: " + node.Value.GetTax();
            for (int i = 0; i < node.Children.Count; i++)
                UpdateTree(node[i]);
            //controlTree.nodePrefabs.Add( newNode);
        }

        public void UpdateTree() {
            ResetGraph();
            InitTree(controlTree);
            UIcontrolTree.Start();
            UpdateTree(controlTree);
        }
        private int maxCol = 0;
        public void InitTree(PlayerNode<Player> node, int row = 1, int col = 1) {
            Debug.Log("entering update " + node.Value.GetName());
            UIcontrolTree.nodeColumns.Add(col + maxCol);
            UIcontrolTree.nodeRows.Add(row);
            node.TreeId = UIcontrolTree.numNodes;
            int childCount = node.Children.Count;
            for (int i = 0; i < node.Children.Count; i++) {
                UIcontrolTree.linkNodeFromIDs.Add(node.TreeId);
                UIcontrolTree.numNodes++;
                UIcontrolTree.linkNodeToIDs.Add(UIcontrolTree.numNodes);
                UIcontrolTree.numLinks++;
                Debug.Log("entering child " + node.Value.GetName() + " will be on colum maxCol:" + maxCol + " i: " + i + " col:" + col);
                InitTree(node[i], row + 1, i + col);
            }
            if (childCount > 1)
                maxCol += childCount - 1;
            // return childCount+node.Controlled.Count;


        }

        
        private int InitPlayerTree(ControlNode node) {
            Debug.Log("entering update " + node.ControlName);
            AddPlayer(node.OID, node.ControlName);
            Player parent = GetPlayer(node.OID);
            int childCount = node.ControlledLength;
            for (int i = 0; i < node.ControlledLength; i++) {
                
                Debug.Log("entering child " + node.ControlName + " oid-"+node.OID);
                InitPlayerTree(node.GetControlled(i));
                Player child = GetPlayer(node.GetControlled(i).OID);
                parent.ControlPlayer(child);
            }
            
            
            return 0;


        }

        internal void PlayerTacticalInfo(TacticalPlayerMsg msg) {
            throw new NotImplementedException();
        }

        internal void PlayerUnits(TacticalPlayerMsg msg) {
            throw new NotImplementedException();
        }

        public void AddPlayerToTree(Player player) {

            player.PlayerNode = controlTree.AddChild(player);
        }


        public bool AreFriendly(long player1OID, long player2OID) {
            if (!players.ContainsKey(player1OID) || !players.ContainsKey(player2OID)) return false;
            Player p1 = players[player1OID], p2 = players[player2OID];
            if (p1.PlayerNode == null || p2.PlayerNode == null) return false;
            return p1.PlayerNode.SearchTreeBFS(p => p.Value.getOID() == player2OID) != null;

        }
    }
}