﻿using FlatBuffers;
using mygame.proto;
using NT;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using TBTK;
using UnityEngine;

namespace NT
{


    public class Proto
    {

        public Offset<UnitMsg> addUnitMsgBasic(FlatBufferBuilder fbb, Unit unit) {
            StringOffset nameOffset = fbb.CreateString(unit.name);
            UnitMsg.StartUnitMsg(fbb);
            UnitMsg.AddOID(fbb, unit.oid);
            UnitMsg.AddUnitName(fbb, nameOffset);
            return UnitMsg.EndUnitMsg(fbb);
        }
        public static Offset<UnitMsg> addExistingUnit(FlatBufferBuilder fbb, UnitMsg unit) {

            StringOffset nameOffset = fbb.CreateString(unit.UnitName);

            Offset<UnitMsg> unitOffset = UnitMsg.CreateUnitMsg(fbb, unit.OID, nameOffset, unit.PrefabId, unit.DataId,
                        unit.TileId, unit.FactionID, unit.Level, unit.HP, unit.AP, unit.TurnPriority,
                        unit.MoveRange, unit.AttackRange, unit.HitChance, unit.DodgeChance, unit.DamageMin,
                        unit.DamageMax, unit.CritChance, unit.CritAvoidance, unit.CritMultiplier, unit.StunChance,
                        unit.StunAvoidance, unit.StunDuration, unit.SilentChance, unit.SilentAvoidance,
                        unit.SilentDuration, unit.HPPerTurn, unit.APPerTurn, unit.DefaultHP, unit.DefaultAP);
            return unitOffset;
        }
        public Offset<UnitMsg> addDataUnit(FlatBufferBuilder fbb, DataUnit unit) {
            StringOffset nameOffset = fbb.CreateString(unit.name);

            Offset<UnitMsg> unitOffset = UnitMsg.CreateUnitMsg(fbb, unit.OID, nameOffset, unit.PrefabID, unit.DataId,
                        unit.tile_id, unit.FactionID, unit.Level, unit.HP, unit.AP, unit.TurnPriority,
                        unit.MoveRange, unit.AttackRange, unit.HitChance, unit.DodgeChance, unit.DamageMin,
                        unit.DamageMax, unit.CritChance, unit.CritAvoidance, unit.CritMultiplier, unit.StunChance,
                        unit.StunAvoidance, unit.StunDuration, unit.SilentChance, unit.SilentAvoidance,
                        unit.SilentDuration, unit.HPPerTurn, unit.APPerTurn, unit.DefaultHP, unit.DefaultAP);
            return unitOffset;
        }

        public static void addArmyMsgBasic(FlatBufferBuilder fbb, long armyOID, VectorOffset unitsVec = default(VectorOffset), StringOffset armyNameOffset = default(StringOffset)) {
            ArmyMsg.StartArmyMsg(fbb);
            ArmyMsg.AddArmyUnits(fbb, unitsVec);
            ArmyMsg.AddOID(fbb, armyOID);
            ArmyMsg.AddArmyName(fbb, armyNameOffset);
            fbb.ForceDefaults = true;
            ArmyMsg.AddIsPlayerFaction(fbb, true);
            ArmyMsg.AddIsNetworkPlayer(fbb, true);
            ArmyMsg.AddRequireDeployment(fbb, true);
            ArmyMsg.AddIsHostile(fbb, true);
            ArmyMsg.AddIsMainArmy(fbb,true);
            fbb.ForceDefaults = false;
        }
        public void addBattle(FlatBufferBuilder fbb, Battle battle, BattleCmd cmd, long subjectOID, int actionOffset = 0) {
            //subjectOID = battle.factionManager.selectedFactionID;

            VectorOffset factionsVec = addFactionArmies(fbb, battle, cmd);
            
            BattleMsg.StartBattleMsg(fbb);
            BattleMsg.AddBattleAction(fbb, actionOffset);
            fbb.ForceDefaults = true;
            BattleMsg.AddOID(fbb, battle.battleOID);
            fbb.ForceDefaults = false;
            BattleMsg.AddSubjectOID(fbb, subjectOID);
            BattleMsg.AddCmd(fbb, cmd);
            BattleMsg.AddArmies(fbb, factionsVec);
        }
        internal VectorOffset addFactionArmies(FlatBufferBuilder fbb, Battle battle, BattleCmd cmd) {

            FactionManager factionManager = battle.factionManager;
            List<Faction> factionList = factionManager.factionList;

            Offset<ArmyMsg> factionOffset = default(Offset<ArmyMsg>);
            List<int> factionIDs = factionList.FindAll(f => !f.isNetworkFaction).ConvertAll(f => f.ID);
            Offset<ArmyMsg>[] factions = new Offset<ArmyMsg>[factionIDs.Count];
            int i = 0;
            switch (cmd) {
                case BattleCmd.PLAYER_STARTED_BATTLE:
                    factions = new Offset<ArmyMsg>[factionList.Count];
                    if (battle.gameControl.GetGamePhase() == _GamePhase.Play)
                        factionIDs = factionList.ConvertAll(f => f.ID);
                    factionList.FindAll(f => !factionIDs.Contains(f.ID)).ForEach(f =>
                    {
                        addFaction(fbb, f);
                        ArmyMsg.AddAction(fbb, DataAction.GET);
                        factions[i++] = ArmyMsg.EndArmyMsg(fbb);
                    });
                    foreach (int factionID in factionIDs) {
                        factionList[factionID].dataID = factionList[factionID].ID;
                        factionOffset = addFactionArmy(fbb, factionList[factionID], _GamePhase.Initialization);
                        factions[i++] = factionOffset;
                    }
                    break;
                case BattleCmd.PLAYER_JOINED_BATTLE:
                    foreach (int factionID in factionIDs) {
                        factionList[factionID].dataID = factionList[factionID].ID;
                        factionOffset = addFactionArmy(fbb, factionList[factionID], _GamePhase.Initialization);
                        factions[i++] = factionOffset;
                    }
                    break;

                case BattleCmd.PLAYER_DEPLOYED_ARMY:
                    Faction deploying = factionList[factionManager.deployingFactionID];
                    List<Unit> tempAll = deploying.allUnitList;
                    deploying.allUnitList = factionManager.deployedUnitList;
                    deploying.dataID = deploying.ID;
                    factionOffset = addFactionArmy(fbb, deploying, _GamePhase.UnitDeployment);
                    factions = new Offset<ArmyMsg>[1] { factionOffset };
                    deploying.allUnitList = tempAll;
                    break;
                default:
                    return default(VectorOffset);
            }

            return BattleMsg.CreateArmiesVector(fbb, factions);
        }

        internal Offset<ArmyMsg> addFactionArmy(FlatBufferBuilder fbb, Faction fac, _GamePhase phase) {
            List<DataUnit> unitsData = new List<DataUnit>();
            List<Tile> tilesData = null;
            bool requireDeployment = true;
            switch (phase) {
                case _GamePhase.Initialization:
                    tilesData = fac.deployableTileList;
                    //if (fac.isNetworkFaction) break;
                    
                    fac.startingUnitList.ForEach(u => unitsData.Add(u.GetData()));
                    if (unitsData.Count > 0) requireDeployment = true;
                    fac.allUnitList.ForEach(u => unitsData.Add(u.GetData()));

                    break;
                case _GamePhase.UnitDeployment:
                    fac.allUnitList.ForEach(u => unitsData.Add(u.GetData()));
                    break;
                case _GamePhase.Play:
                    fac.allUnitList.ForEach(u => unitsData.Add(u.GetData()));
                    break;
            }
            VectorOffset tilesOffset = default(VectorOffset);
            if (tilesData != null)
                tilesOffset = ArmyMsg.CreateDeployableTilesVector(fbb, addTileList(fbb, tilesData));
            VectorOffset unitsOffset = ArmyMsg.CreateArmyUnitsVector(fbb, addDataUnitList(fbb, unitsData));
            addFaction(fbb, fac);
            ArmyMsg.AddAction(fbb, DataAction.SET);
            ArmyMsg.AddArmyUnits(fbb, unitsOffset);
            ArmyMsg.AddDeployableTiles(fbb, tilesOffset);
            ArmyMsg.AddRequireDeployment(fbb, requireDeployment);
            ArmyMsg.AddAction(fbb, DataAction.SET);
            ArmyMsg.AddOID(fbb, fac.armyOID);
            Offset<ArmyMsg> offset = ArmyMsg.EndArmyMsg(fbb);
            return offset;
        }
        public void addFaction(FlatBufferBuilder fbb, Faction fac) {
            ArmyMsg.StartArmyMsg(fbb);
            
            ArmyMsg.AddDataID(fbb, fac.dataID);
            
            fbb.ForceDefaults = true;
            ArmyMsg.AddIsNetworkPlayer(fbb, fac.isNetworkFaction);
            ArmyMsg.AddFactionID(fbb, fac.ID);
            ArmyMsg.AddIsHostile(fbb, false);
            ArmyMsg.AddIsPlayerFaction(fbb, fac.isPlayerFaction);
            fbb.ForceDefaults = false;
            
            ArmyMsg.AddOID(fbb, fac.armyOID);
        }


        public Offset<ArmyMsg> addExistingArmyMsg(FlatBufferBuilder fbb, ArmyMsg army) {

            Offset<UnitMsg>[] unitsVec = new Offset<UnitMsg>[army.ArmyUnitsLength];
            for (int i = 0; i < army.ArmyUnitsLength; i++)
                unitsVec[i] = addExistingUnit(fbb, army.GetArmyUnits(i));
            VectorOffset unitsOffset = ArmyMsg.CreateArmyUnitsVector(fbb, unitsVec);
            StringOffset nameOffset = fbb.CreateString(army.ArmyName);
            ArmyMsg.StartArmyMsg(fbb);
            ArmyMsg.AddArmyUnits(fbb, unitsOffset);
            ArmyMsg.AddOID(fbb, army.OID);
            ArmyMsg.AddArmyName(fbb, nameOffset);
            return ArmyMsg.EndArmyMsg(fbb);

        }

        public Offset<ArmyMsg>[] addExistingArmies(FlatBufferBuilder fbb, List<ArmyMsg> armiesList) {
            Offset<ArmyMsg>[] armies = new Offset<ArmyMsg>[armiesList.Count];
            int i = 0;
            foreach (ArmyMsg army in armiesList) {
                armies[i] = addExistingArmyMsg(fbb, army);
                i++;
            }
            return armies;

        }

        public VectorOffset addPointExistingArmies(FlatBufferBuilder fbb, List<ArmyMsg> armiesList) {
            Offset<ArmyMsg>[] armiesVec = addExistingArmies(fbb, armiesList);
            return ControlPointMsg.CreateGarrisonedArmiesVector(fbb, armiesVec);

        }
        public static List<UnitMsg> DataToMsgs(List<DataUnit> units) {
            List<UnitMsg> unitsMsgs = new List<UnitMsg>();
            units.ForEach(u => unitsMsgs.Add(u.GetMsg()));
            return unitsMsgs;
        }

        public static List<DataUnit> MsgsToData(List<UnitMsg> units) {
            List<DataUnit> unitsData = new List<DataUnit>();
            units.ForEach(u => unitsData.Add(new DataUnit(u)));
            return unitsData;
        }

        public static Offset<ArmyMsg> addArmy(FlatBufferBuilder fbb, long armyOID, string armyName, List<DataUnit> units,bool finishMsg=true) {
            return addArmy(fbb,armyOID, armyName, DataToMsgs(units), finishMsg);
        }

        public static Offset<ArmyMsg> addArmy(FlatBufferBuilder fbb,long armyOID, string armyName, List<UnitMsg> units,bool finishMsg = true) {
          
            Offset<UnitMsg>[] unitsVec = addExistingArmyUnits(fbb, units);

            VectorOffset unitsVecOffset = ArmyMsg.CreateArmyUnitsVector(fbb, unitsVec);
            StringOffset nameOffset = fbb.CreateString(armyName);
            addArmyMsgBasic(fbb, armyOID, unitsVecOffset, nameOffset);
            if (finishMsg) return ArmyMsg.EndArmyMsg(fbb);
            else return default(Offset<ArmyMsg>);
        }


        public Offset<UnitMsg>[] addDataUnitList(FlatBufferBuilder fbb, List<DataUnit> units) {
            Offset<UnitMsg>[] unitsVec = new Offset<UnitMsg>[units.Count];
            for (int i = 0; i < units.Count; i++)
                unitsVec[i] = addDataUnit(fbb, units[i]);
            return unitsVec;
        }

        public static Offset<UnitMsg>[] addExistingArmyUnits(FlatBufferBuilder fbb, List<UnitMsg> units) {
            Offset<UnitMsg>[] unitsVec = new Offset<UnitMsg>[units.Count];
            for (int i = 0; i < units.Count; i++)
                unitsVec[i] = addExistingUnit(fbb, units[i]);
            return unitsVec;
        }


        public Offset<ArmyMsg> addArmyStartInfo(FlatBufferBuilder fbb, long oid, List<DataUnit> units, int factionID, Vector3 p,Faction template) {
            bool requireDeployment = false;
            Faction fac = template;
            if (units.Count > 0) requireDeployment = true;
            VectorOffset unitsOffset = ArmyMsg.CreateArmyUnitsVector(fbb, addDataUnitList(fbb, units));
            VectorOffset tilesOffset = ArmyMsg.CreateDeployableTilesVector(fbb, addTileList(fbb, fac.deployableTileList));
            ArmyMsg.StartArmyMsg(fbb);
            
            ArmyMsg.AddOID(fbb, oid);
            ArmyMsg.AddArmyUnits(fbb, unitsOffset);
            ArmyMsg.AddIsMainArmy(fbb, true);
            ArmyMsg.AddDeployableTiles(fbb, tilesOffset);
            ArmyMsg.AddFactionID(fbb, factionID);
            ArmyMsg.AddDataID(fbb, factionID);
            ArmyMsg.AddColor(fbb, Vec3.CreateVec3(fbb, fac.color.r, fac.color.g, fac.color.b));
            fbb.ForceDefaults = true;
            ArmyMsg.AddIsHostile(fbb, false);
            ArmyMsg.AddIsNetworkPlayer(fbb, true);
            ArmyMsg.AddIsPlayerFaction(fbb, true);
            ArmyMsg.AddRequireDeployment(fbb, true);
            fbb.ForceDefaults = false;
            ArmyMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, p.x, p.y, p.z));
            return ArmyMsg.EndArmyMsg(fbb);
        }


        public Offset<ControlPointMsg> addPointMsg(bool endMsg, FlatBufferBuilder fbb, long pointOID, PointCmd command = PointCmd.POINT_INFO, long subjectOID = 0, Offset<ArmyMsg> garrisonOffset = default(Offset<ArmyMsg>), VectorOffset armiesOffset = default(VectorOffset), DataAction action = DataAction.NONE, string pointName = null) {


            StringOffset nameOffset = fbb.CreateString(pointName);
            ControlPointMsg.StartControlPointMsg(fbb);
            ControlPointMsg.AddSubjectOID(fbb, subjectOID);
            ControlPointMsg.AddName(fbb, nameOffset);
            ControlPointMsg.AddAction(fbb, action);
            ControlPointMsg.AddOID(fbb, pointOID);
            ControlPointMsg.AddGarrison(fbb, garrisonOffset);
            ControlPointMsg.AddGarrisonedArmies(fbb, armiesOffset);
            ControlPointMsg.AddCmd(fbb, command);
            if (!endMsg) return default(Offset<ControlPointMsg>);
            return ControlPointMsg.EndControlPointMsg(fbb);
        }

        public ControlPointMsg createPointMsg(FlatBufferBuilder fbb, long pointOID, PointCmd command, long subjectOID, Offset<ArmyMsg> garrisonOffset, VectorOffset armiesOffset, DataAction action) {
            Offset<ControlPointMsg> pointMsgOffset = addPointMsg(true, fbb, pointOID, command, subjectOID, garrisonOffset, armiesOffset, action);
            fbb.Finish(pointMsgOffset.Value);
            ControlPointMsg pointMsg = ControlPointMsg.GetRootAsControlPointMsg(fbb.DataBuffer);
            return pointMsg;
        }

        public static List<UnitMsg> getArmyUnits(ArmyMsg army) {
            List<UnitMsg> units = new List<UnitMsg>();
            if (army == null) return units;
            for (int i = 0; i < army.ArmyUnitsLength; i++)
                units.Add(army.GetArmyUnits(i));
            return units;
        }

        public static List<ArmyMsg> getBattleArmies(BattleMsg battle) {
            List<ArmyMsg> armies = new List<ArmyMsg>();
            for (int i = 0; i < battle.ArmiesLength; i++)
                armies.Add(battle.GetArmies(i));
            armies.Sort((a1, a2) => a1.FactionID.CompareTo(a2.FactionID));
            return armies;
        }


        public List<ArmyMsg> getPointArmiesGarrison(ControlPointMsg pointMsg) {
            List<ArmyMsg> armies = new List<ArmyMsg>();
            for (int i = 0; i < pointMsg.GarrisonedArmiesLength; i++)
                armies.Add(pointMsg.GetGarrisonedArmies(i));
            return armies;
        }
        public ControlPointMsg updatePointGarrison(ControlPointMsg pointMsg, ArmyMsg garrison) {
            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            Offset<ControlPointMsg> pointMsgOffset = addPointMsg(true, fbb, pointMsg.OID, pointMsg.Cmd, pointMsg.SubjectOID, garrison, getPointArmiesGarrison(pointMsg), pointMsg.Action);
            fbb.Finish(pointMsgOffset.Value);
            return ControlPointMsg.GetRootAsControlPointMsg(fbb.DataBuffer);
        }

        public Offset<UnitAttackMsg> addUnitAttack(FlatBufferBuilder fbb, Unit attacking, Unit attackedUnit, AttackInstance attInst,int shotsFired) {
            Offset<UnitMsg> attackingOffset = addUnitMsg(fbb, attacking), attackedOffset = addUnitMsg(fbb, attackedUnit);
            Offset<AttackInstanceMsg> attackOffset = addAttackInst(fbb, attInst);
            UnitAttackMsg.StartUnitAttackMsg(fbb);
            UnitAttackMsg.AddAtt(fbb, attackOffset);
            UnitAttackMsg.AddAttacked(fbb, attackedOffset);
            UnitAttackMsg.AddAttacking(fbb, attackingOffset);
            UnitAttackMsg.AddShotsFired(fbb, Math.Max(1,shotsFired));
            Offset<UnitAttackMsg> offset = UnitAttackMsg.EndUnitAttackMsg(fbb);
            return offset;
        }

        public Offset<UnitAbilityMsg> addUnitAbility(FlatBufferBuilder fbb, Unit abilityUnit, int unitAbilityId, Tile targetedTile, AttackInstance attInst) {
            Offset<UnitMsg> unitOffset = addUnitMsg(fbb, abilityUnit);
            Offset<TileMsg> tileOffset = addTileMsg(fbb, targetedTile);
            Offset<AttackInstanceMsg> attackOffset = addAttackInst(fbb, attInst);
            UnitAbilityMsg.StartUnitAbilityMsg(fbb);
            UnitAbilityMsg.AddAtt(fbb, attackOffset);
            UnitAbilityMsg.AddAbilityID(fbb, (short)unitAbilityId);
            UnitAbilityMsg.AddTargetedTile(fbb, tileOffset);
            UnitAbilityMsg.AddActivator(fbb, unitOffset);
            Offset<UnitAbilityMsg> offset = UnitAbilityMsg.EndUnitAbilityMsg(fbb);
            return offset;
        }
        public Offset<UnitMoveMsg> addUnitMove(FlatBufferBuilder fbb, Unit unit, Tile tile) {
            Offset<UnitMsg> unitOffset = addUnitMsg(fbb, unit);
            Offset<TileMsg> tileOffset = addTileMsg(fbb, tile);
            UnitMoveMsg.StartUnitMoveMsg(fbb);
            UnitMoveMsg.AddTargetedTile(fbb, tileOffset);
            UnitMoveMsg.AddUnit(fbb, unitOffset);
            return UnitMoveMsg.EndUnitMoveMsg(fbb);
        }


        public Offset<UnitMsg> addUnitMsg(FlatBufferBuilder fbb, Unit attacking) {
            return UnitMsg.CreateUnitMsg(fbb, attacking.oid, fbb.CreateString(attacking.name),
                (short)attacking.prefabId, (short)attacking.dataID, (short)attacking.tileId,
                (short)attacking.factionID, (short)attacking.GetLevel(), attacking.HP);
        }

        public Offset<ControlPointMsg> addPointMsg(bool endMsg, FlatBufferBuilder fbb, long pointOID, PointCmd command, long subjectOID, ArmyMsg garrison, List<ArmyMsg> armies, DataAction action) {
            Offset<ArmyMsg> garrisonOffset = addExistingArmyMsg(fbb, garrison);
            VectorOffset armiesOffset = addPointExistingArmies(fbb, armies);
            return addPointMsg(endMsg, fbb, pointOID, command, subjectOID, garrisonOffset, armiesOffset, action);
        }

        public ControlPointMsg updatePointArmies(ControlPointMsg pointMsg, List<ArmyMsg> armies) {
            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            Offset<ControlPointMsg> pointMsgOffset = addPointMsg(true, fbb, pointMsg.OID, pointMsg.Cmd, pointMsg.SubjectOID, pointMsg.Garrison, armies, pointMsg.Action);
            fbb.Finish(pointMsgOffset.Value);
            return ControlPointMsg.GetRootAsControlPointMsg(fbb.DataBuffer);
        }

        public Offset<TileMsg>[] addTileList(FlatBufferBuilder fbb, List<Tile> tiles) {
            Offset<TileMsg>[] tilesVec = new Offset<TileMsg>[tiles.Count];
            for (int i = 0; i < tiles.Count; i++)
                tilesVec[i] = addTileMsg(fbb, tiles[i]);
            return tilesVec;
        }

        public Offset<TileMsg> addTileMsg(FlatBufferBuilder fbb, Tile tile) {
            return TileMsg.CreateTileMsg(fbb, tile.tileId, tile.walkable);
        }

        public Offset<ControlPointMsg> createPointMsg(bool endMsg, FlatBufferBuilder fbb, ControlPoint point, PointCmd command, long subjectOID) {
            string controllingName = point.GetControllingPlayer() == null ? "Neutral Control" : point.GetControllingPlayer().GetName();
            string enablingName = point.GetEnablingPlayer() == null ? "Unactivated" : point.GetEnablingPlayer().GetName();
            StringOffset controllingNameOffset = fbb.CreateString(controllingName);
            StringOffset enablingNameOffset = fbb.CreateString(enablingName);
            long controllingOID = point.GetControllingPlayer() == null ? -1 : point.GetControllingPlayer().getOID();
            long enablingOID = point.GetEnablingPlayer() == null ? -1 : point.GetEnablingPlayer().getOID();

            addPointMsg(false, fbb, point.pointOID, command, subjectOID, point.GetGarrison(), point.GetGarrisonedArmies(), DataAction.NONE);
            ControlPointMsg.AddControllingName(fbb, controllingNameOffset);
            ControlPointMsg.AddEnablingName(fbb, enablingNameOffset);
            if (controllingOID > 0) ControlPointMsg.AddControllingOID(fbb, controllingOID);
            if (enablingOID > 0) ControlPointMsg.AddEnablingOID(fbb, enablingOID);
            if (!endMsg) return default(Offset<ControlPointMsg>);
            return ControlPointMsg.EndControlPointMsg(fbb);

        }



        internal Offset<AttackInstanceMsg> addAttackInst(FlatBufferBuilder fbb, AttackInstance att) {
            Offset<UnitMsg> srcUnit = addUnitMsg(fbb, att.srcUnit), tgtUnit = addUnitMsg(fbb, att.tgtUnit);

            VectorOffset unitsVec = default(VectorOffset);
            if (att.UnitList != null) unitsVec = AttackInstanceMsg.CreateUnitListVector(fbb, addDataUnitList(fbb, att.UnitList));
            return AttackInstanceMsg.CreateAttackInstanceMsg(fbb, att.Processed, att.IsAbility,
                true, srcUnit, tgtUnit, (short)att.UnitAbilityID, att.IsMelee, att.IsCounter, att.Missed,
                att.Critical, att.Stunned, att.Silenced, att.Flanked, att.Destroyed, att.Damage, (short)att.Stun,
                (short)att.Silent, att.DamageTableModifier, att.FlankingBonus, unitsVec);

        }

        public ControlNode getNodeMsg(PlayerNode<Player> requested) {
            PlayerNode<Player> playerNode = requested;

            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            Offset<ControlNode> offset = new Offset<ControlNode>(fbb.Offset);
            Offset<ControlNode> controlNodeOffset = addNodeRecursive(fbb, requested, offset);
            fbb.Finish(controlNodeOffset.Value);
            ControlNode root = ControlNode.GetRootAsControlNode(fbb.DataBuffer);
            return root;
        }



        public Offset<ControlPointMsg> addPointBuffer(FlatBufferBuilder fbb, ControlPoint point) {
            ControlPointMsg.StartControlPointMsg(fbb);
            ControlPointMsg.AddOID(fbb, point.pointOID);
            return ControlPointMsg.EndControlPointMsg(fbb);
        }

        public Offset<ControlNode> addNodeRecursive(FlatBufferBuilder fbb, PlayerNode<Player> playerNode, Offset<ControlNode> parentOffset) {
            Player player = playerNode.Value;
            ControlNode.StartControlNode(fbb);
            Offset<ControlNode> offset = new Offset<ControlNode>(fbb.Offset);
            List<ControlPoint> playerPoints = player.GetControlledPoints();
            Offset<ControlPointMsg>[] points = new Offset<ControlPointMsg>[playerPoints.Count];
            if (points.Length > 0) {
                for (int i = 0; i < playerPoints.Count; i++) {
                    points[i] = addPointBuffer(fbb, playerPoints[i]);
                }
            }
            Offset<ControlNode>[] children = new Offset<ControlNode>[playerNode.Children.Count];
            if (playerNode.Children.Count > 0) {
                for (int i = 0; i < playerNode.Children.Count; i++) {
                    children[i] = addNodeRecursive(fbb, playerNode.Children[i], offset);
                }

            }
            VectorOffset controlledOffset = ControlNode.CreateControlledVector(fbb, children);
            VectorOffset controlPointsOffset = ControlNode.CreateControlPointsVector(fbb, points);
            StringOffset controlNameOffset = fbb.CreateString(player.GetName());
            ControlNode.CreateControlNode(fbb, player.getOID(), controlNameOffset, controlledOffset, controlPointsOffset,
                    parentOffset, (short)player.GetTaxRate(), (short)0);
            return ControlNode.EndControlNode(fbb);

        }

        public static void addControlPoint(FlatBufferBuilder fbb, PointCmd cmd, long pointOID, long subjectOID) {
            ControlPointMsg.StartControlPointMsg(fbb);
            ControlPointMsg.AddCmd(fbb, cmd);
            ControlPointMsg.AddOID(fbb, pointOID);
            ControlPointMsg.AddSubjectOID(fbb, subjectOID);
        }

        public static ControlPlayerMsg createControlUpdateRequest() {
            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            StringOffset name = fbb.CreateString(ClientAPI.GetPlayerObject().Name);
            ControlPlayerMsg.StartControlPlayerMsg(fbb);
            ControlPlayerMsg.AddCmd(fbb, ControlPlayerCmd.PLAYER_CONTROL_INFO);
            ControlPlayerMsg.AddSubjectOID(fbb, ClientAPI.GetPlayerOid());
            ControlPlayerMsg.AddSubjectName(fbb, name);
            Offset<ControlPlayerMsg> msgOffset = ControlPlayerMsg.EndControlPlayerMsg(fbb);
            fbb.Finish(msgOffset.Value);
            ControlPlayerMsg controlMsg = ControlPlayerMsg.GetRootAsControlPlayerMsg(fbb.DataBuffer);
            return controlMsg;
        }
        public static TacticalPlayerMsg createTacticalUpdateRequest() {
            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            StringOffset name = fbb.CreateString(ClientAPI.GetPlayerObject().Name);
            TacticalPlayerMsg.StartTacticalPlayerMsg(fbb);
            TacticalPlayerMsg.AddCmd(fbb, TacticalPlayerCmd.PLAYER_TACTICAL_INFO);
            TacticalPlayerMsg.AddSubjectOID(fbb, ClientAPI.GetPlayerOid());
            TacticalPlayerMsg.AddSubjectName(fbb, name);
            Offset<TacticalPlayerMsg> msgOffset = TacticalPlayerMsg.EndTacticalPlayerMsg(fbb);
            fbb.Finish(msgOffset.Value);
            TacticalPlayerMsg tacticalMsg = TacticalPlayerMsg.GetRootAsTacticalPlayerMsg(fbb.DataBuffer);
            return tacticalMsg;
        }

        public static void SendExtensionMessage(string protoName,byte[] array,int position, string extension_subtype) {
            Dictionary<string, object> props = new Dictionary<string, object>();
            props.Add(protoName, array);
            props.Add("BufferPosition", position);
            NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, extension_subtype, props);
        }

        /*
        public ControlMsg createControlPoint(FlatBufferBuilder fbb, Offset<ControlPointMsg> pointMsgOffset, Control command = Control.CONTROL_POINT) {
            ControlMsg.StartControlMsg(fbb);
            ControlMsg.AddPointMsg(fbb, pointMsgOffset);
            ControlMsg.AddCmd(fbb, command);
            Offset<ControlMsg> msgOffset = ControlMsg.EndControlMsg(fbb);
            fbb.Finish(msgOffset.Value);
            return ControlMsg.GetRootAsControlMsg(fbb.DataBuffer);
        }
        public ControlMsg createControl(ControlNode playerNodeMsg) {
            // TODO Auto-generated method stub
            return null;
        }*/
        /*
        public static TacticalMsg createTacticalBattle(BattleMsg battleMsg) {
            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            VectorOffset off = TacticalMsg.CreateBattleMsgVector(fbb, battleMsg.ByteBuffer.Data);
            TacticalMsg.StartTacticalMsg(fbb);
            TacticalMsg.AddCmd(fbb,Tactical.BATTLE_COMMAND);
            TacticalMsg.AddBattleMsg(fbb, off);
            Offset< TacticalMsg> msgOffset = TacticalMsg.EndTacticalMsg(fbb);
            fbb.Finish(msgOffset.Value);
            return TacticalMsg.GetRootAsTacticalMsg(fbb.DataBuffer);
        }
        */

    }
}
