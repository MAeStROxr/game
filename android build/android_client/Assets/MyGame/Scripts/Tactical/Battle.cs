﻿using UnityEngine;
using System.Collections;
using TBTK;
using System.Collections.Generic;
using System;

public class Battle : MonoBehaviour {
    public GameControl gameControl;
    public GridManager gridManager;
    public FactionManager factionManager;
    public TurnControl turnControl;
    public AbilityManagerFaction abilityManagerFaction;
    public AbilityManagerUnit abilityManagerUnit;
    public AIManager aiManager;
    public PerkManager perkManager;
    public Data data = new Data();
    public UnitListener unitListeners ;
    public CoverSystem coverSystem ;
    public UIUnitDeployment deploymentUI;
    public long battleOID;
 
    public bool debug = true;
    public List<Unit> allUnitList;
    

    public int waitingPlayers=0;
    public Unit abilityUnit;
    public int unitAbilityId = -1;
    public Tile targetedTile;
    public AttackInstance preparedAttack;
    
    public delegate void BattleTargetSelected(Tile tile);

   
    public void prepareUnitAbility(Tile tile) {
        AttackInstance att = new AttackInstance(this, abilityUnit, unitAbilityId);
        UnitAbility ability = abilityUnit.abilityList[unitAbilityId];
        abilityUnit.abilityTargetedTile = tile;
       
        att.Processed = true;
        att.ShowAttack = true;
        preparedAttack = att;
        battleController.UnitAbility(this, abilityUnit, ability, unitAbilityId, tile, att);
   
    }
   
    public GridManager.TargetModeSelectedCallBack UnitAbilityDelegate(Unit unit,int abilityId) {
        abilityUnit=unit;
        unitAbilityId = abilityId;
 
        return prepareUnitAbility;
}

    public ITacticalController battleController {  get {  return controllerContainer.Result;  }
        set {  controllerContainer.Result = value;   } }
    [SerializeField] private ITacticalControllerContainer controllerContainer;
    // Use this for initialization
  
    void Awake() {
        //battleOID = battleOIDs++;
        coverSystem = new CoverSystem(this);
        
    }
    void Start() {
        BattleManager.RegisterBattle(this);
    }/*
    public void StartBattle() {
       
        gameControl.Init();
        gameControl.StartGamePhases();
    }*/

    // Update is called once per frame
    void Update() {

    }

    

    public bool ReadyToStart {
        get { 
        if (factionManager.factionList.Exists(f => !f.deployed))
            return false;
        if (waitingPlayers > 0) return false;
        return true;
        }
    }
}
