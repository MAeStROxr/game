﻿using UnityEngine;
using System.Collections.Generic;
using TBTK;
using System.IO;
using System;
using System.Linq;
using mygame.proto;
using NT;
using FlatBuffers;
using System.Collections;

public class BattleException : Exception { }
public class BattleManager : MonoBehaviour
{
    public ITacticalController battleController
    {
        get
        {
            return controllerContainer.Result;
        }
        set
        {
            controllerContainer.Result = value;
        }
    }
    [SerializeField]
    private ITacticalControllerContainer controllerContainer;
    public TacticalNetworkController networkController;

    //Dictionary<long, Battle> battles = new Dictionary<long, Battle>();

    [Serializable]
    public class BattleDic : SerializableDic<long, Battle> { }
    public BattleDic battles = new BattleDic();
    Proto proto = new Proto();
    private static BattleManager instance;
    public Battle battlePrefab, emptyBattlePrefab;
    public List<Unit> allUnitList = new List<Unit>();
    public GameObject battleCollider;
    private long clientOID = 0;

    public Dictionary<int, Unit> unitsDic = new Dictionary<int, Unit>();
    Inventory inv;
    // Use this for initialization
    void Awake() {
        instance = this;
        allUnitList.ForEach(u => u.gameObject.SetActive(false));
        foreach (Unit u in allUnitList)
            unitsDic[u.prefabId] = u;

        // foreach (Unit u in allUnitList) { Debug.Log(u.name + " " + u.prefabId); }

    }
    void OnLevelWasLoaded(int level) {
        if (level == 1) {
            if (!GameMainframe.GetInstance().debug) inv = ClientAPI.ScriptObject.GetComponent<Inventory>();
            
        }
    }

    void Start() {
        DontDestroyOnLoad(this);
        NetworkAPI.RegisterExtensionMessageHandler
               (Tactical.BATTLE_COMMAND.ToString(), RecievedBattleMsg);
        //if (debug) DebugStartBattle();
    }
    public static BattleManager GetInstance() { return instance; }

    public int debugPlayers = 2;

    public static void RegisterBattle(Battle battleData) {
        if (instance.battles.ContainsKey(battleData.battleOID)) return;
        instance.battles[battleData.battleOID] = battleData;
        battleData.battleController = instance.battleController;
    }

    public void SendBattleMessage(BattleMsg battleMsg) {
        Debug.Log("Sending Battle Message with command:" + battleMsg.Cmd.ToString());
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg a = battleMsg.GetArmies(i);
            Debug.Log("Army-" + i + " in battleMsg, armyOid-" + a.OID+" networkPlayer-"+a.IsNetworkPlayer+" units size:"+a.ArmyUnitsLength);
        }
        Proto.SendExtensionMessage("BattleMsg", battleMsg.ByteBuffer.Data,
                battleMsg.ByteBuffer.Position, "tactical." + Tactical.BATTLE_COMMAND.ToString());
    }
    public void RecievedBattleMsg(Dictionary<string, object> props) {
        byte[] data = (byte[])props["BattleMsg"];
        ByteBuffer buffer = new ByteBuffer(data);
        buffer.Position = (int)props["BufferPosition"];
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(buffer);
        Debug.Log("Recieved TacticalPlayerMsg: battleOid-" + msg.OID + " subjectOID-" + msg.SubjectOID + " Cmd-" + msg.Cmd.ToString());
        for (int i = 0; i < msg.ArmiesLength; i++) {
            ArmyMsg a = msg.GetArmies(i);
            Debug.Log("Army-" + i + " in battleMsg, armyOid-" + a.OID + " networkPlayer-" + a.IsNetworkPlayer + " units size:" + a.ArmyUnitsLength);
        }
        ProcessBattleMsg(msg);
    }


    public void ProcessBattleMsg(BattleMsg msg) {

        switch (msg.Cmd) {
            case BattleCmd.BATTLE_INFO:
                BattleInfo(msg);
                    break;
            case BattleCmd.PLAYER_STARTED_BATTLE:
                BattleStarted(msg);
                break;
            
            case BattleCmd.PLAYER_JOINED_BATTLE:
                BattleJoined(msg);
                break;
            case BattleCmd.PLAYER_DEPLOYED_ARMY:
                BattleDeployed(msg);
                break;
            case BattleCmd.BATTLE_OVER:
                BattleOver(msg);
                break;
            case BattleCmd.PLAYER_LEFT_BATTLE:
                BattleLeft(msg);
                break;
            case BattleCmd.PLAYER_ENDED_TURN:
                BattleTurnOver(msg);
                break;
            case BattleCmd.BATTLE_COMMENCED:
                BattleCommenced(msg);
                break;
            case BattleCmd.PLAYER_RETREAT_BATTLE:
                BattleRetreat(msg);
                break;
            case BattleCmd.UNIT_ABILITY:
            case BattleCmd.UNIT_ATTACK:
            case BattleCmd.UNIT_MOVE:
                BattleAction(msg);
                break;
            case BattleCmd.EXECUTE_FAILURE:
                Debug.Log("Battle command failed, cmd:"+msg.Cmd.ToString()+" error:"+msg.Text);
                break;
        }

    }

    public long GetClientOID() {
        return clientOID;
    }
    internal void BattleJoined(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.ArmiesJoined(battle, msg);
        // battle.battleController.ArmiesJoined(battle, msg);
    }

    internal void BattleDeployed(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.ArmiesDeployed(battle, msg);
    }

    internal void BattleLeft(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        //  battle.battleController.Ba(battle, msg);
    }

    internal void BattleTurnOver(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.TurnEnded(battle);
    }

    internal void BattleCommenced(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.BattleCommenced(battle, msg);
    }

    internal void BattleOver(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        GameMainframe mainframe = GameMainframe.GetInstance();
        if (msg.OID== mainframe.currentBattleOID) {
            mainframe.currentBattleOID = 0;
            mainframe.inBattle = false;
        }
        networkController.BattleEnded(battle);
    }


    internal Battle GetBattle(long battleOID) {
        if (battles.ContainsKey(battleOID))
            return battles[battleOID];
        else return null;
    }

    internal void BattleRetreat(BattleMsg msg) {
        throw new NotImplementedException();
    }



    internal void BattleAction(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        switch (msg.BattleActionType) {
            case BattleActionMsg.UnitMoveMsg:
                UnitMoveMsg moveMsg = msg.GetBattleAction(new UnitMoveMsg());
                networkController.UnitMoved(battle, moveMsg);
                break;
            case BattleActionMsg.UnitAttackMsg:
                UnitAttackMsg attackMsg = msg.GetBattleAction(new UnitAttackMsg());
                networkController.UnitAttacked(battle, attackMsg);
                break;
            case BattleActionMsg.UnitAbilityMsg:
                UnitAbilityMsg abilityMsg = msg.GetBattleAction(new UnitAbilityMsg());
                networkController.UnitAbilityUsed(battle, abilityMsg);
                break;
            default:
                throw new BattleException();
        }

    }

    private int unitCount = 0;

    public static void DeleteUnitsFromInv(List<UnitMsg> units) {
        if (GameMainframe.GetInstance().debug) {
            units.ForEach(u => instance.unitsData.RemoveAll(un => u.OID == un.OID));
            return;
        }
        //need to fix
        units.ForEach(u => instance.inv.DeleteItemWithName(u.UnitName));
    }


    public static void addUnitsToInventory(List<UnitMsg> units) {

        if (GameMainframe.GetInstance().debug) {
            instance.unitsData.AddRange(units);
            return;
        }
        units.ForEach(u => NetworkAPI.SendTargetedCommand(instance.clientOID, "/gi " + u.PrefabId));
    }

    public  List<DataUnit> getUnitsDataFromInv() {
        return Proto.MsgsToData(getUnitsMsgsFromInv());
    }

    List<UnitMsg> unitsData = null;

    public static List<UnitMsg> getUnitsMsgsFromInv() {
        Dictionary<int, Unit> dic = instance.unitsDic;

        Unit u;
        if (GameMainframe.GetInstance().debug) {
            if (instance.unitsData == null)
                instance.unitsData = new List<UnitMsg>();
            else
                return instance.unitsData;
            int count = UnityEngine.Random.Range(3, 6);
            for (int i = 0; i < count; i++) {
                int index = UnityEngine.Random.Range(0, dic.Count);
                dic.TryGetValue(index, out u);
                if (u != null) {
                    DataUnit d = new DataUnit();
                    d.Setup(u);
                    d.OID = instance.unitCount++;
                    d.PrefabID = u.prefabId;
                    instance.unitsData.Add(d.GetMsg());
                }
            }
            //invUnits = unitsData.Cast<UnitMsg>().ToList();


            return instance.unitsData;
        }

        List<UnitMsg> unitsData = new List<UnitMsg>();
        foreach (Bag bag in instance.inv.Bags.Values) {
            foreach (AtavismInventoryItem item in bag.items.Values) {
                int tempId = item.templateId;
                dic.TryGetValue(tempId, out u);
                if (u != null) {
                    DataUnit d = new DataUnit();
                    d.Setup(u);
                    d.OID = item.ItemId.ToLong();
                    d.PrefabID = item.templateId;
                    d.name = item.name;
                    unitsData.Add(d.GetMsg());
                }
            }
        }
        return unitsData;
    }

    public static List<Unit> GetUnits(List<UnitMsg> unitsData, bool clone = false) {
        List<Unit> clones = new List<Unit>();
        unitsData.ForEach(d =>
        {
            //Debug.Log(d.PrefabId);
            Unit u = instance.unitsDic[d.PrefabId];
            if (clone) { u = Instantiate(u); u.name = d.UnitName; }
            clones.Add(u);
            u.ModifyStatsToData(new DataUnit(d), d.DataId);
        });
        return clones;
    }
    public static List<Unit> GetUnits(List<DataUnit> unitsData, bool instantiate) {
        List<Unit> prefabs = new List<Unit>();
        unitsData.ForEach(d =>
        {
            //Debug.Log(d.name + " " + d.PrefabID);
            Unit u = instance.unitsDic[d.PrefabID];
            prefabs.Add(u);
            d.unit = u;

        });
        if (instantiate) {
            List<Unit> instantiated = new List<Unit>();
            foreach (DataUnit d in unitsData) {
                d.unit = Instantiate(d.unit);
                d.unit.ModifyStatsToData(d, d.DataId);

                instantiated.Add(d.unit);
            }
            return instantiated;
        }
        return prefabs;
    }
    public List<GameObject> debugObjects = new List<GameObject>();
    public void DebugStartBattle() {
        Dictionary<long, GameObject> targets = new Dictionary<long, GameObject>();
        for (int i = 0; i < debugObjects.Count; i++) {
            targets.Add(i, debugObjects[i]);
        }
        //TacticalNetwork.SendStartBattle(targets);
    }


    public void StartBattle(AtavismMobNode mobNode) {
        Dictionary<long, AtavismMobNode> targets = new Dictionary<long, AtavismMobNode>();
        targets.Add(mobNode.Oid, mobNode);

        List<long> worldObjOIDs = ClientAPI.WorldManager.GetObjectOidList();
        foreach (AtavismMobNode mob in ClientAPI.WorldManager.GetMobNodes()) {
            float distance = Vector3.Distance(mob.Position, ClientAPI.GetPlayerObject().Position);
            if (mob.CheckBooleanProperty("attackable") &&
                4.0 < distance && distance < 16.0 && targets.Count < GameMainframe.GetInstance().MaxTacticalPlayers)
                targets.Add(mob.Oid, mob);
        }
        StartBattle(targets);
    }
    long battleHeightOffset =0;

    


    public void StartBattle(Dictionary<long, AtavismMobNode> targets) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(100);
        Vector3 battlePosition = new Vector3(0, 0, 0);
        foreach (AtavismObjectNode obj in targets.Values) {
            battlePosition += obj.Position;

        }
        battlePosition /= targets.Count;
        battlePosition.y += battleHeightOffset;
        if (Physics.CheckBox(battlePosition, battleCollider.transform.localScale / 2)) {
            battleHeightOffset+=2;
            battleCollider.transform.position = battlePosition;
            StartCoroutine(NewBattleCollides());
            //return;
        }
        battleHeightOffset = 0;

        int factionIDs = 0;
        List<DataUnit> invUnits = getUnitsDataFromInv(), empty = new List<DataUnit>();
        List<Tile> tilesData = battlePrefab.factionManager.factionList[0].deployableTileList;
        Offset<ArmyMsg>[] armies = new Offset<ArmyMsg>[targets.Count + 1];
        int i = 0;
        
        Vector3 p = GameMainframe.GetInstance().GetClientObject().transform.position;
        Faction fac = battlePrefab.factionManager.factionList[factionIDs];
        armies[i++] = proto.addArmyStartInfo(fbb, GameMainframe.GetInstance().clientOID, invUnits,factionIDs++,p, fac);

        foreach (KeyValuePair<long, AtavismMobNode> player in targets) {
            p = player.Value.Position;
            fac = battlePrefab.factionManager.factionList[factionIDs];
            armies[i++] = proto.addArmyStartInfo(fbb, player.Key, empty, factionIDs++, p,fac);
        }

        VectorOffset armiesVec = BattleMsg.CreateArmiesVector(fbb, armies);
        BattleMsg.StartBattleMsg(fbb);
        BattleMsg.AddSubjectOID(fbb, targets.Keys.First());
        BattleMsg.AddOID(fbb, 666);
        BattleMsg.AddArmies(fbb, armiesVec);
        BattleMsg.AddCmd(fbb, BattleCmd.PLAYER_STARTED_BATTLE);
        BattleMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, battlePosition.x, battlePosition.y, battlePosition.z));
        BattleMsg.AddSubjectOID(fbb, clientOID);
        Offset<BattleMsg> battleOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(battleOffset.Value);
        BattleMsg battleMsg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(battleMsg);
    }

    IEnumerator NewBattleCollides() {
        yield return null;
        battleCollider.SetActive(true);
        yield return new WaitForSeconds(4f);
        battleCollider.SetActive(false);
        yield return null;
    }



    public Battle InstantiateBattleTemplate(Vector3 battlePosition = default(Vector3)) {
        
        GameObject newBattle = (GameObject)Instantiate(emptyBattlePrefab.gameObject, battlePosition, Quaternion.identity);
        Battle battle = newBattle.GetComponent<Battle>();
        battle.battleController = battleController;
        return battle;
    }


    public void BattleInfo(BattleMsg battleMsg) {
        Battle battle = GetBattle(battleMsg.OID);
        // Debug.Log("BattleManager started battle:  " + battleMsg.OID + " started");
        if (battle == null) {
            Vec3 pos = battleMsg.Position;
            Vector3 battlePosition = new Vector3(pos.X, pos.Y, pos.Z);
            battle = InstantiateBattleTemplate(battlePosition);
            battle.battleOID = battleMsg.OID;
            battle.gameObject.SetActive(true);
            DontDestroyOnLoad(battle);
        }

        networkController.BattleStarted(battle, battleMsg);
        networkController.ArmiesJoined(battle, battleMsg);
        networkController.ArmiesDeployed(battle, battleMsg);
        if (battleMsg.BattlePhase == _GamePhase.Play.GetHashCode())
            networkController.BattleCommenced(battle, battleMsg);

    }


    public void BattleStarted(BattleMsg battleMsg) {
        Battle battle = GetBattle(battleMsg.OID);
        // Debug.Log("BattleManager started battle:  " + battleMsg.OID + " started");
        if (battle == null) {
            Vec3 pos = battleMsg.Position;
            Vector3 battlePosition = new Vector3(pos.X, pos.Y, pos.Z);
            battle = InstantiateBattleTemplate(battlePosition);
            battle.battleOID = battleMsg.OID;
            battle.gameObject.SetActive(true);
        }
        networkController.BattleStarted(battle, battleMsg);
        bool joinArmies = false;
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg a = battleMsg.GetArmies(i);
            if (a.OID == GameMainframe.GetInstance().clientOID) {
                GameMainframe.GetInstance().inBattle = true;
                GameMainframe.GetInstance().currentBattleOID = battleMsg.OID;
                if (!battle.factionManager.GetFaction(a.FactionID).joined)
                    joinArmies = true;
            }
        }
        if (joinArmies)
                networkController.JoinArmies(battle);

    }




    void Update() {

    }
}
