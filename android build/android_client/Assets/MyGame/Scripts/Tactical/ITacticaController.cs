﻿using UnityEngine;
using System.Collections;
using TBTK;
using mygame.proto;

public interface ITacticalController {
      GridManager.TargetModeSelectedCallBack SetUnitAndAbility(Battle battle,Unit unit, int abilityId);
    void StartBattle(Battle battle);
    void JoinArmies(Battle battle);
    void DeployArmies(Battle battle);
   void CommenceBattle(Battle battle);
   void EndBattle(Battle battle);
    void EndTurn(Battle battle);
    void MoveUnit(Battle battle,Unit unit, Tile tile);
    void AttackUnit(Battle battle, Unit unit, Unit attackedUnit, bool isCounter = false);
    void UnitAbility(Battle battle, Unit unit, UnitAbility ability,  int abilityId, Tile tile, AttackInstance att);
    void FactionAbility(Battle battle, Tile tile);


}
