﻿using UnityEngine;
using System.Collections;
using System;
using TBTK;
using System.Collections.Generic;
using System.IO;
using mygame.proto;
using FlatBuffers;
using NT;

[System.Serializable]
public class TacticalLocalController : MonoBehaviour, ITacticalController {
    protected BattleManager battleManager;
    protected Proto proto = new Proto();

    // Use this for initialization
    public bool debug;
    public Dictionary<int, Unit> unitsDic = new Dictionary<int, Unit>();
    public void Start() {
        battleManager = BattleManager.GetInstance();
        /* foreach (Unit u in battleData.allUnitList)
             unitsDic[u.prefabId] = u;
        */
    }

    // Update is called once per frame
    void Update() {

    }



    public GridManager.TargetModeSelectedCallBack SetUnitAndAbility(Battle battle, Unit unit, int abilityId) {

        return battle.UnitAbilityDelegate(unit, abilityId);

    }
    

    virtual public void StartBattle(Battle battle) {
        battle.gameControl.Init();
        battle.gameControl.StartGamePhases();
    }

   

   

    virtual public void JoinArmies(Battle battle) {
        
    }
    
    
    virtual public void DeployArmies(Battle battle) {
        FactionManager facMngr = battle.factionManager;
        facMngr.CompleteDeployment();
    }

   


    /*
    private void LoadAndDeploy(Battle battle, ArmyMsg army, Faction fac) {
        battle.factionManager.LoadFactionData(army.FactionID);
        if (!army.RequireDeployment) DeployArmy(battle, army);
    }*/

    virtual public void CommenceBattle(Battle battle) {
        battle.gameControl.StartGame();
    }

    


    //adds army to faction manager
    





    public virtual void EndBattle(Battle battle) {
        Destroy(battle.gameObject);
    }

    



    public void ClearBattleData(Battle battle) {

        FactionManager facMngr = battle.factionManager;
        foreach (Faction f in facMngr.factionList) {
            ClearFactionData(battle, f.ID);
        }
        facMngr.allUnitList.ForEach(u => { if (u != null) Destroy(u.gameObject); });
        facMngr.allUnitList.Clear();
        facMngr.playerFactionIDList.Clear();
        facMngr.selectedFactionID = -1;
        facMngr.selectedUnitID = -1;
        battle.data.ClearLoadData();
    }

    public void ClearFactionData(Battle battle, int factionID) {
        FactionManager facMngr = battle.factionManager;

        if (factionID >= facMngr.factionList.Count || facMngr.factionList[factionID] == null) return;
        Faction f = facMngr.factionList[factionID];
        f.ClearUnit();
        /*if (f.ID <= mgr.selectedFactionID)
         {
             if (f.ID == mgr.selectedFactionID)
                 mgr._SelectNextFaction();
             mgr.selectedFactionID--;

         }*/
        //mgr.factionList.Remove(f);
    }



    virtual public void FactionAbility(Battle battle, Tile tile) {
        throw new NotImplementedException();
    }

    virtual public void FactionAbilityUsed(Battle battle, int abilityId, Tile tile) {
        throw new NotImplementedException();
    }

  


    virtual public void UnitAbility(Battle battle, Unit unit, UnitAbility ability, int abilityId, Tile tile, AttackInstance att) {
        if (ability.shoot)
            att.srcUnit.AbilityShootObjectHit(att);
        unit.selectedAbilityID = abilityId;
        unit.AbilityTargetSelected(tile, att);
    }

    virtual public void MoveUnit(Battle battle, Unit unit, Tile tile) {
        MoveUnitRoutine(battle, unit, tile);
    }

    

    virtual public void AttackUnit(Battle battle, Unit unit, Unit attackedUnit,bool isCounter=false) {
        AttackInstance att = new AttackInstance(battle, unit, attackedUnit, isCounter);

        UnitAttacked(battle, unit, attackedUnit, att);
    }

    virtual public void UnitAttacked(Battle battle, Unit unit, Unit attackedUnit, AttackInstance attInstance) {
        if (attInstance.IsCounter) StartCoroutine(CounterUnitRoutine(battle, unit, attackedUnit, attInstance));
        else StartCoroutine(AttackUnitRoutine(battle, unit, attackedUnit, attInstance));
    }



    virtual public void EndTurn(Battle battle) {
        battle.gameControl.EndTurn();
    }

 

    void MoveUnitRoutine(Battle battle, Unit unit, Tile targetTile) {
        //first move to the targetTile
        unit.Move(targetTile);
        StartCoroutine(EndMoveUnitRoutine(battle));

    }

    IEnumerator AttackUnitRoutine(Battle battle, Unit unit, Unit attackedUnit, AttackInstance attInstance) {
        //first move to the targetTile
        unit.Attack(attackedUnit, attInstance);
        yield return null;
    }

    IEnumerator CounterUnitRoutine(Battle battle, Unit unit, Unit attackedUnit, AttackInstance attInstance) {
        if (!attInstance.Processed) yield return null;
        //first move to the targetTile
        unit.Counter(attackedUnit, attInstance);
        yield return null;
    }

    IEnumerator EndMoveUnitRoutine(Battle battle) {
        while (!battle.turnControl.ClearToProceed()) yield return null;
        yield return null;
    }


}
