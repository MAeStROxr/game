﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TBTK;
using NT;
using System.IO;
using UnityEngine.UI;
using mygame.proto;
using FlatBuffers;

namespace NT {

    public class TacticalNetwork : MonoBehaviour {

        private static TacticalNetwork instance;
        private BattleManager battleManager;
        private PlayerManager playerManager;

        public GameObject nodePrefab;
        public long clientOID = 0;
        public String clientName;
        Proto proto = new Proto();
        void Awake() {

        }

        // Use this for initialization
        void Start() {
            instance = this;


        }
        public void OnLevelWasLoaded(int level) {
            if (level == 1) {
                clientOID = ClientAPI.GetPlayerOid();
                clientName = ClientAPI.GetPlayerObject().Name;

            }
        }

        // Update is called once per frame
        void Update() {

        }

        public void RegisterTacticalListener() {
        /*    NetworkAPI.RegisterExtensionMessageHandler
               (Tactical.BATTLE_COMMAND.ToString(), RecievedTacticalMsg);
            NetworkAPI.RegisterExtensionMessageHandler
               (Tactical.PLAYER_TACTICAL_INFO.ToString(), RecievedTacticalMsg);
            NetworkAPI.RegisterExtensionMessageHandler
               (Tactical.PLAYER_UNITS.ToString(), RecievedTacticalMsg);
            */


        }

        /* public void SendBattleMsg(BattleMsg battleOffset) {
             TacticalMsg.StartTacticalMsg(fbb);
             //TacticalMsg.AddCmd(fbb,Tac );
             TacticalMsg.AddBattleMsg(fbb, battleOffset);
             Offset<TacticalMsg> offset = TacticalMsg.CreateTacticalMsg(fbb, Tactical.BATTLE_COMMAND, clientOID, battleOffset);
             fbb.Finish(offset.Value);
             TacticalMsg msg = TacticalMsg.GetRootAsTacticalMsg(fbb.DataBuffer);
         }

         public void SendTacticalMsg(TacticalMsg tacticalMsg) {
             Dictionary<string, object> props = new Dictionary<string, object>();
             props.Add("TacticalMsg", tacticalMsg.ByteBuffer.Data);
             NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "tactical." + tacticalMsg.Cmd.ToString(), props);
         }*/


        /*public void RecievedTacticalMsg(Dictionary<string, object> props) {
            byte[] data = (byte[])props["ControlMsg"];
            ByteBuffer buffer = new ByteBuffer(data);
            TacticalMsg msg = TacticalMsg.GetRootAsTacticalMsg(buffer);
            switch (msg.Cmd) {
                case Tactical.BATTLE_COMMAND:
                    battleManager.RecievedBattleMsg(msg.Battle);
                    break;
                case Tactical.PLAYER_TACTICAL_INFO:
                    playerManager.PlayerTacticalInfo(msg);
                    break;
                case Tactical.PLAYER_UNITS:
                    playerManager.PlayerUnits(msg);
                    break;
            }

        }*/

        public void SendAddPlayer() { instance._SendAddPlayer(); }
        public void _SendAddPlayer() {
            Dictionary<string, object> props = new Dictionary<string, object>();
            props.Add("player_oid", OID.fromLong(ClientAPI.GetPlayerOid()));
            props.Add("player_name", ClientAPI.GetPlayerObject().Name);
            NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "controlpoint.PLAYER_ADD", props);


        }




        /*  public static void SendStartBattle(Dictionary<long, GameObject> targets) {
              instance._SendStartBattle(targets);
          }
          public void _SendStartBattle(Dictionary<long, GameObject> targets) {
              FlatBufferBuilder fbb = new FlatBufferBuilder(0);
              Offset<BattleMsg> battleOffset = battleManager.StartBattle(fbb, targets);
              TacticalMsg.StartTacticalMsg(fbb);
              TacticalMsg.AddCmd(fbb, Tactical.BATTLE_COMMAND);
            //  TacticalMsg.AddBattle(fbb, battleOffset);
              Offset<TacticalMsg> msgOffset = TacticalMsg.EndTacticalMsg(fbb);
              fbb.Finish(msgOffset.Value);
              TacticalMsg msg = TacticalMsg.GetRootAsTacticalMsg(fbb.DataBuffer);
              SendTacticalMsg(msg);





          }*/
    }
}