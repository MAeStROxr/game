﻿using UnityEngine;
using System.Collections;
using TBTK;
using System.Collections.Generic;
using System.IO;
using System;
using mygame.proto;
using FlatBuffers;
using NT;

public class TacticalNetworkController : TacticalLocalController {
  

     // Update is called once per frame
    void Update() {
        //debug = battleData.debug;
    }

    new void Start() {
        base.Start();

        //BattleManager.RegisterBattle(battleData);
    }

    public void SendBattleMessage(BattleMsg battleMsg) {
        BattleManager.GetInstance().SendBattleMessage(battleMsg);

        //else
        //BattleManager.GetInstance().SendBattleMessage(battleData.battleOID, subtype, props);
        
    }
    
    public long clientOID { get { return GameMainframe.GetInstance().clientOID; } }
    public void DirectMessageBySubtype(Battle battle, BattleMsg battleMsg) {
        //string prefix = "tactical.";
        //Tactical cmd = (Tactical)Enum.Parse(typeof(Tactical), subtype);
        
        switch (battleMsg.Cmd) {
            case BattleCmd.UNIT_MOVE:
                UnitMoveMsg moveMsg = new UnitMoveMsg();
                UnitMoved(battle,battleMsg.GetBattleAction(moveMsg));
                break;
            case BattleCmd.UNIT_ATTACK:
                UnitAttackMsg attackMsg = new UnitAttackMsg();
                UnitAttacked(battle, battleMsg.GetBattleAction(attackMsg));
                break;
            case BattleCmd.UNIT_ABILITY:
                UnitAbilityMsg abilityMsg = new UnitAbilityMsg();
                UnitAbilityUsed(battle, battleMsg.GetBattleAction(abilityMsg));
                break;
            case BattleCmd.PLAYER_JOINED_BATTLE:
                ArmiesJoined(battle, battleMsg);
                break;
            case BattleCmd.PLAYER_STARTED_BATTLE:
                BattleStarted(battle, battleMsg);
                break;
            case BattleCmd.PLAYER_DEPLOYED_ARMY:
                ArmiesDeployed(battle, battleMsg);
                break;
            case BattleCmd.PLAYER_ENDED_TURN:
                TurnEnded(battle);
                break;
            case BattleCmd.BATTLE_COMMENCED:
                BattleCommenced(battle, battleMsg);
                break;
        }
        
    }


    public BattleMsg GetBattleMsg(Battle battle, BattleCmd cmd) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);   
        proto.addBattle(fbb, battle, cmd, clientOID);
        if (cmd == BattleCmd.PLAYER_STARTED_BATTLE)
            BattleMsg.AddCurrentFactionID(fbb, battle.factionManager.selectedFactionID);
        Offset<BattleMsg> offset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(offset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        return msg;
    }
    
    override public void StartBattle(Battle battle) {
        List<DataUnit> invUnits = BattleManager.GetInstance().getUnitsDataFromInv();
        
        for (int i = 0; i < invUnits.Count; i++) invUnits[i].DataId = i;
        battle.factionManager.factionList[0].startingUnitList = BattleManager.GetUnits(invUnits,true);
        battle.factionManager.factionList[0].startingUnitList.ForEach(u => u.battleData = battle);
        BattleMsg battleMsg = GetBattleMsg(battle, BattleCmd.PLAYER_STARTED_BATTLE);
        battle.factionManager.factionList[0].ClearUnit(true);
        //
        SendBattleMessage(battleMsg);
        //return battleMsg;
    }



    virtual public void BattleStarted(Battle battle, BattleMsg battleMsg) {
        //battle.battleOID = battleMsg.OID;
        battle.waitingPlayers = battleMsg.ArmiesLength;
        battle.factionManager.ClearUnit(false);
        battle.factionManager.selectedFactionID = Math.Max(battleMsg.CurrentFactionID-1,-1);
        //Debug.Log("Battle Started " + battle.battleOID);
        bool addOwnFaction = false;
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {      
            ArmyMsg army = battleMsg.GetArmies(i);
            if (!army.IsNetworkPlayer) addOwnFaction = true;
            AddArmy(battle, army.FactionID);
            SetArmy(battle, army);
            if (JoinArmy(battle, army)) { 
                    battle.waitingPlayers--;
            }

        }
        /*if (addOwnFaction) {
            battle.waitingPlayers++;
            AddArmy(battle);
            
        }*/
        if (battle.waitingPlayers==0)
            base.StartBattle(battle);
    }


    override public void JoinArmies(Battle battle) {
        int myFactionId = battle.factionManager.factionList.Find(f => !f.isNetworkFaction && f.isPlayerFaction).ID;
        List<DataUnit> invUnits = BattleManager.GetInstance().getUnitsDataFromInv();
        for (int i = 0; i < invUnits.Count; i++) {
            invUnits[i].DataId = i;
            invUnits[i].FactionID = myFactionId;
        }
        battle.factionManager.factionList[myFactionId].startingUnitList = BattleManager.GetUnits(invUnits,true);
        BattleMsg battleJoinMsg = GetBattleMsg(battle, BattleCmd.PLAYER_JOINED_BATTLE);
        //base.JoinArmies(battle, battleMsg);
        battle.factionManager.factionList[myFactionId].ClearUnit(true);
        SendBattleMessage(battleJoinMsg);
        // return newBattleMsg;
    }

    virtual public void ArmiesJoined(Battle battle, BattleMsg battleMsg) {
        List<ArmyMsg> armies = new List<ArmyMsg>();
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg army = battleMsg.GetArmies(i);
            armies.Add(army);
            if (JoinArmy(battle, army))
                battle.waitingPlayers--;

            if (battle.gameControl.GetGamePhase() == _GamePhase.Play) { 
                battle.factionManager.InitFaction(army.FactionID);
                
            }
            //else 
            
        }
        base.JoinArmies(battle);
        if (battle.gameControl.GetGamePhase() == _GamePhase.Initialization &&
            battle.waitingPlayers <= 0) {
           base.StartBattle(battle);
        } 
    }

    public bool JoinArmy(Battle battle, ArmyMsg army) {
        FactionManager facMngr = battle.factionManager;
        //AddArmy(battle, army.FactionID);
        

        Faction fac = facMngr.factionList[army.FactionID];
        if (army.ArmyUnitsLength == 0 || fac==null || fac.joined) return false;
        fac.joined = true;
        //SetArmy(battle, army);
        List<DataUnit> deployable = new List<DataUnit>();
        List<DataUnit> deployed = new List<DataUnit>();
        for (int i = 0; i < army.ArmyUnitsLength; i++) {
            DataUnit dataUnit = new DataUnit(army.GetArmyUnits(i));
            if (dataUnit.tile_id < 0)
                deployable.Add(dataUnit);
            else
                deployed.Add(dataUnit);
            //Debug.Log("unit " + army.GetArmyUnits(i).UnitName + " " + army.GetArmyUnits(i).PrefabId);
        }

        if (deployable.Count > 0) {
            fac.loadFromData = true;
            //battle.factionManager.deployingFactionID;
            //fac.startingUnitList = BattleManager.GetUnits(deployable);
            BattleManager.GetUnits(deployable,false);
            battle.data.SetLoadData(army.FactionID, deployable);
        }

        List<Tile> tileList = battle.gridManager.GetGrid().tileList;
        List<Unit> deployedUnits = BattleManager.GetUnits(deployed,false);
        deployedUnits = battle.factionManager.CloneUnitList(deployedUnits);
        facMngr.ModifyUnitsData(deployedUnits, deployed);
        //if (army.IsNetworkPlayer) deployedUnits.ForEach(u => u.gameObject.SetActive(true));
        fac.allUnitList = deployedUnits;
        //if (loadAndDeploy) LoadAndDeploy(army, fac);
        return true;
        
    }

    private Color getFactionColor(int factionID) {
        switch (factionID) {
            case 0: return Color.red;
            case 1: return Color.yellow;
            case 2: return Color.blue;
            case 3: return Color.black;
            case 4: return Color.green;
            default: return Color.white;
        }
    }

    private List<Tile> getFactionDeployableTiles(Battle battle,int factionID,int tilesAmount=10) {
        GridManager gridMngr = battle.gridManager;
        Grid grid = gridMngr.GetGrid();
        float seedX=0, seedY=0;
        int delta=1;
        switch (factionID) {
            /*
            case 0:
                seedX = 0.5f;
                seedY = 0;  
                break;
            case 1:
                seedX = 0.5f;
                seedY = 1;
                delta = gridMngr.length;
                break;
                    case 2:
                seedX = 0;
                seedY = 0.5f;
                break;
            case 3:
                seedX = 1;
                seedY = 0.5f;
                delta = gridMngr.length;
            */
            case 0:
                seedX = 0.5f;
                seedY = 0.5f;  
                break;
            case 1:
                seedX = 0.5f;
                seedY = 0.5f;
                delta = gridMngr.length;
                break;
                    case 2:
                seedX = 0.5f;
                seedY = 0.5f;
                break;
            case 3:
                seedX = 0.5f;
                seedY = 0.5f;
                delta = gridMngr.length;
                break;
        }
        int startFromTile = (int)(seedX * gridMngr.length + seedY * (gridMngr.width-1)* gridMngr.length),offset = -5;
        startFromTile += offset;
        List<Tile> deployableTiles = new List<Tile>();
        for (int i = 0; i < tilesAmount; i++) {
            deployableTiles.Add(grid.tileList[startFromTile++]);
        }
        deployableTiles.ForEach(t => t.deployAreaID = factionID);
        return deployableTiles;

    }

    public void AddArmy(Battle battle, int factionID=-1) {
        
        List<Faction> facList = battle.factionManager.factionList;
        if (factionID < 0)
            factionID = facList.Count;
        //Faction newFac = new Faction();
        if (factionID >= facList.Count) {
            while (facList.Count <= factionID) {
                Faction fac = new Faction();
                fac.isPlayerFaction = true;
                fac.isNetworkFaction = false;
                fac.requireDeployment = true;
                fac.isHostile = false;
                fac.loadFromData = false;
                fac.ID = facList.Count;
                fac.dataID = facList.Count;
                fac.color = getFactionColor(fac.ID);
                fac.deployableTileList = getFactionDeployableTiles(battle, fac.ID); 
                facList.Add(fac);
            }
        }
    }

    public void SetArmy(Battle battle, ArmyMsg army) {
        
        Faction fac = battle.factionManager.factionList[army.FactionID];
        if (fac.armyOID == army.OID) return;
        fac.armyOID = army.OID;
        fac.isPlayerFaction = army.IsPlayerFaction;
        fac.isNetworkFaction = army.IsNetworkPlayer;
        fac.requireDeployment = army.RequireDeployment;
        fac.isHostile = army.IsHostile;
        fac.loadFromData = false;
        fac.ID = army.FactionID;
        fac.dataID = army.DataID;
        fac.color = army.Color==null? getFactionColor(army.FactionID) : new Color(army.Color.X, army.Color.Y, army.Color.Z);
        List<Tile> deployableTiles = new List<Tile>();
        for (int i = 0; i < army.DeployableTilesLength; i++) {
            TileMsg tileMsg = army.GetDeployableTiles(i);
            Tile tile = getTile(battle, tileMsg);
            deployableTiles.Add(tile);
        }
        if (deployableTiles.Count>0) fac.deployableTileList = deployableTiles;
    }




    /*
    public void ArmiesJoinedRecieved(Battle battle, BattleMsg battleMsg) {

        ArmiesJoined(battle, battleMsg);
    }*/
    public override void DeployArmies(Battle battle) {
        BattleMsg battleDeployMsg = GetBattleMsg(battle, BattleCmd.PLAYER_DEPLOYED_ARMY);
        SendBattleMessage(battleDeployMsg);

    }


    public void ArmiesDeployed(Battle battle, BattleMsg battleMsg) {
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg army = battleMsg.GetArmies(i);

            DeployArmy(battle, army);
            
            if (battle.gameControl.GetGamePhase() == _GamePhase.Play) {
                battle.factionManager.SetupFaction(army.FactionID);
            }
        }

        if (battle.factionManager.factionList.TrueForAll(f => f.deployed)) {
            battle.deploymentUI.Hide();
            battle.factionManager.CompleteDeployment();
        }
            

    }

    public void DeployArmy(Battle battle, ArmyMsg army) {
        FactionManager facMngr = battle.factionManager;
        Faction deployingFac = facMngr.GetFaction(army.FactionID);
        List<Tile> tileList = battle.gridManager.GetGrid().tileList;

        if (deployingFac.deployed) return;
        
        for (int i = 0; i < army.ArmyUnitsLength; i++) {

            UnitMsg unitMsg = army.GetArmyUnits(i);
            if (unitMsg.TileId < 0) continue;
            Unit foundUnit = deployingFac.startingUnitList.Find(u => u.dataID == unitMsg.DataId);
            List<Unit> removeFrom = deployingFac.startingUnitList;

            if (foundUnit == null) {
                foundUnit = battle.factionManager.deployedUnitList.Find(u => u.dataID == unitMsg.DataId);
                removeFrom = battle.factionManager.deployedUnitList;
            }
            if (foundUnit == null) {
                foundUnit = deployingFac.allUnitList.Find(u => u.dataID == unitMsg.DataId);
                removeFrom = deployingFac.allUnitList;
            }

            if (foundUnit == null)
                throw new BattleException();


            //if (battle.turnControl.GetTurnMode() == _TurnMode.UnitPerTurn)
            
            //else if (battle.turnControl.GetTurnMode() == _TurnMode.FactionUnitPerTurn)
             //   facMngr.allUnitList.Add(foundUnit);

            facMngr.DeployUnit(foundUnit, new DataUnit(unitMsg), tileList);
            removeFrom.Remove(foundUnit);
            deployingFac.allUnitList.Add(foundUnit);
            foundUnit.factionID = deployingFac.ID;
            
            foundUnit.gameObject.SetActive(true);
            deployingFac.deployed = true;


        }

        /*
            List<Unit> tempDeployed = facMngr.deployedUnitList;
            facMngr.deployedUnitList = new List<Unit>();
            facMngr.AutoDeployFaction(army.FactionID);
            facMngr.deployedUnitList.Clear();
            facMngr.deployedUnitList = tempDeployed;
            */
    }
    public override void CommenceBattle(Battle battle) {
        BattleMsg battleCommenceMsg = GetBattleMsg(battle, BattleCmd.BATTLE_COMMENCED);
        SendBattleMessage(battleCommenceMsg);
    }
    public void BattleCommenced(Battle battle, BattleMsg battleMsg) {
        if (battle.gameControl.GetGamePhase()==_GamePhase.UnitDeployment)
            base.CommenceBattle(battle);
    }
    public override void EndBattle(Battle battle) {
        BattleMsg battleEndMsg = GetBattleMsg(battle, BattleCmd.BATTLE_OVER);
        SendBattleMessage(battleEndMsg);
    }
    public void BattleEnded(Battle battle) {
        base.EndBattle(battle);
    }


    override public void MoveUnit(Battle battle, Unit unit, Tile tile) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<UnitMoveMsg> moveOffset = proto.addUnitMove(fbb, unit, tile);
        proto.addBattle(fbb, battle, BattleCmd.UNIT_MOVE,clientOID, moveOffset.Value);
        BattleMsg.AddBattleActionType(fbb, BattleActionMsg.UnitMoveMsg);
        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }



    public void UnitMoved(Battle battle, UnitMoveMsg moveMsg) {

        Unit u = getUnit(battle, moveMsg.Unit);
        Tile t = getTile(battle, moveMsg.TargetedTile);
        base.MoveUnit(battle, u, t);
    }

    override public void AttackUnit(Battle battle, Unit unit, Unit attackedUnit, bool isCounter = false) {
        AttackInstance attInst = new AttackInstance(battle, unit, attackedUnit, isCounter);
        attInst.Process();
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<UnitAttackMsg> attackOffset = proto.addUnitAttack(fbb, unit, attackedUnit, attInst,unit.shootPointList.Count);
        proto.addBattle(fbb, battle, BattleCmd.UNIT_ATTACK,clientOID, attackOffset.Value);
        BattleMsg.AddBattleActionType(fbb, BattleActionMsg.UnitAttackMsg);
        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }

    virtual public void UnitAttacked(Battle battle, UnitAttackMsg attackMsg) {
        Unit attacking = getUnit(battle, attackMsg.Att.SrcUnit);
        Unit attacked = getUnit(battle, attackMsg.Att.TgtUnit);
        AttackInstance attInst = new AttackInstance(battle, attacking, attacked, attackMsg.Att.IsCounter);
        attInst.Process();
        attInst.SetData(attackMsg.Att);
        attInst.ShowAttack = true;
        base.UnitAttacked(battle, attacking, attacked, attInst);
    }

   

    override public void UnitAbility(Battle battle, Unit unit, UnitAbility ability, int abilityId, Tile tile, AttackInstance att) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<UnitAbilityMsg> abilityOffset = proto.addUnitAbility(fbb, unit, abilityId, tile, att);
        proto.addBattle(fbb, battle, BattleCmd.UNIT_ABILITY, clientOID, abilityOffset.Value);
        BattleMsg.AddBattleActionType(fbb,BattleActionMsg.UnitAbilityMsg);
        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }

    public void UnitAbilityUsed(Battle battle, UnitAbilityMsg abilityMsg) {
        Unit unit = getUnit(battle, abilityMsg.Activator);
        Tile t = getTile(battle, abilityMsg.TargetedTile);
        int abilityId = abilityMsg.AbilityID;
        UnitAbility ability = unit.abilityList[abilityId];
        AttackInstance attInst = new AttackInstance(battle, unit, abilityId);
        attInst.SetData(abilityMsg.Att);
        unit.abilityTargetedTile = t;
        base.UnitAbility(battle, unit, ability, abilityId, t, attInst);

    }

    public override void EndTurn(Battle battle) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        proto.addBattle(fbb, battle, BattleCmd.PLAYER_ENDED_TURN, clientOID);

        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }

    public void TurnEnded(Battle battle) {
        base.EndTurn(battle);
    }

    private Unit getUnit(Battle battle, UnitMsg msg) {
        List<Unit> units = battle.factionManager.GetAllUnitsOfFaction(msg.FactionID);
        return units.Find(u => msg.OID == u.oid);
    }

    private Tile getTile(Battle battle, TileMsg msg) {
        List<Tile> tiles = battle.gridManager.grid.tileList;
        return tiles[msg.TileId];
    }
    // Use this for initialization

}
