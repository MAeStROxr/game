﻿using UnityEngine;
using System.Collections;
using TBTK;
public class UnitListener : MonoBehaviour {
    public Battle battleData;
    TurnControl turnControl {
        get {
            return battleData.turnControl;
        }
    }
    public void actionCamHandler(Unit unit, Vector3 targetPos, float chance) {
        if (onActionCamE != null) onActionCamE(unit, targetPos, chance);
    }
    public void unitDestroyedHandler(Unit unit) {
        if (onUnitDestroyedE != null) onUnitDestroyedE(unit);
    }
    public void unitSelectedHandler(Unit unit) {
        if (onUnitSelectedE != null) onUnitSelectedE(unit);
    }
    public void unitMoveDepletedHandler(Unit unit) {
        if (onMoveDepletedE != null) onMoveDepletedE(unit);
    }

    public void unitMoveHandler(Unit unit, Tile tile) {
         
        if (onUnitMoveE != null) onUnitMoveE(unit, tile);
    }

    public void unitAttackHandler(Unit unit, Unit attackedUnit, AttackInstance attInstance = null) {
         if (onUnitAttackE != null) onUnitAttackE(unit, attackedUnit, attInstance);
    }

    public void unitAbilityHandler(Unit unit, Tile tile,int abilityId) {
        unit.selectedAbilityID = abilityId;
        unit.AbilityTargetSelected(tile);
        if (onUnitAbilityE != null) onUnitAbilityE(unit, tile);
    }

    public event ActionCamHandler onActionCamE;
    public delegate void ActionCamHandler(Unit unit, Vector3 targetPos, float chance);

    public event UnitDestroyedHandler onUnitDestroyedE;      //fire when the unit is destroyed
    public delegate void UnitDestroyedHandler(Unit unit);

    public event UnitSelectedHandler onUnitSelectedE;            //listen by UI only  & UIOverlay(for display cover icon)
    public delegate void UnitSelectedHandler(Unit unit);

    public event UnitMoveDepletedHandler onMoveDepletedE;        //fire when the unit depleted all available action
    public delegate void UnitMoveDepletedHandler(Unit unit);                //listen by MoveUnitIndicatorOnly

    public event UnitMoveHandler onUnitMoveE;
    public delegate void UnitMoveHandler(Unit unit, Tile tile);

    public event UnitAttackHandler onUnitAttackE;
    public delegate void UnitAttackHandler(Unit unit, Unit attackedUnit, AttackInstance attInst = null);

    public event UnitAbilityHandler onUnitAbilityE;
    public delegate void UnitAbilityHandler(Unit unit, Tile tile);

    bool movingUnit = false;
  

    // Use this for initialization
    void Start() {
        battleData = gameObject.transform.root.GetComponentInChildren<Battle>();
    }

    // Update is called once per frame
    void Update() {

    }
}
