﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TBTK;

namespace TBTK {

    public class FactionManager : MonoBehaviour {
        public Battle battleData;
        GameControl gameControl {
            get {
                return battleData.gameControl;
            }
        }
        GridManager gridManager {
            get {
                return battleData.gridManager;
            }

        }
        TurnControl turnControl {
            get {
                return battleData.turnControl;
            }

        }
        Data savedData {
            get {
                return battleData.data;
            }
        }


        public delegate void UnitDeploymentHandler(bool start);     //true when start deployment, false when all deployment is done
        public event UnitDeploymentHandler onUnitDeploymentPhaseE;

        public delegate void UnitDeployedHandler(Unit unit);
        public event UnitDeployedHandler onUnitDeployedE;       //fire when a unit is deployed on the map, for UI to update deployment UI

        public delegate void InsertUnitHandler(Unit unit);
        public event InsertUnitHandler onInsertUnitE;   //fire when a unit is insert to the grid in mid-game, for UI to create a new overlay


        public bool generateUnitOnStart = false;



        public bool hasAIInGame = true;
        public List<int> playerFactionIDList = new List<int>();
        public List<int> GetPlayerFactionID() { return instance.playerFactionIDList; }

        public int selectedFactionID = -1;
        public List<Faction> factionList = new List<Faction>();
        public int GetTotalFactionCount() { return instance.factionList.Count; }
        public List<Faction> GetFactionList() { return instance.factionList; }
        public int GetSelectedFactionID() { return instance.selectedFactionID; }

        //[HideInInspector]
        public int selectedUnitID = -1; //only use in UnitPerTurn
        
        public int totalStartingUnits {
            get {
                int sum = 0;
                factionList.ForEach(f => sum += f.startingUnitList.Count);
                return sum;
            }
        }

        public int totalUnitCount {
            get {
                int sum = 0;
                factionList.ForEach(f => sum += f.allUnitList.Count);
                return sum;
            }
        }
        public List<Unit> allUnitList = new List<Unit>();   //all unit from all faction
        public int GetTotalUnitCount() { return instance.totalUnitCount; }

        public FactionManager instance;
        public FactionManager GetInstance() { return instance; }
        public Transform GetTransform() { return instance != null ? instance.transform : null; }



        void Awake() {
            if (instance == null) instance = this;
            battleData = gameObject.GetComponentInParent<Battle>();
        }

        public void GameOver() { instance._GameOver(); }
        public void _GameOver() {
            //save the faction back to data if it's loaded from data
            for (int i = 0; i < factionList.Count; i++) {
                Faction fac = factionList[i];
                if (!fac.loadFromData) continue;

                List<DataUnit> loadList = savedData.GetLoadData(fac.dataID);
                List<DataUnit> list = new List<DataUnit>();

                for (int m = 0; m < loadList.Count; m++) {
                    for (int n = 0; n < fac.allUnitList.Count; n++) {
                        if (fac.allUnitList[n].GetDataID() == m) {
                            DataUnit data = fac.allUnitList[n].GetData();

                            data.unit = loadList[m].unit;
                            list.Add(data);

                            fac.allUnitList.RemoveAt(n);
                            break;
                        }
                    }
                }

                savedData.SetEndData(fac.dataID, list);
            }
        }
        /**********/
        public void InitFaction(int factionID) {

            foreach (Unit u in factionList[factionID].allUnitList) {
                u.battleData = battleData;
                u.InitUnitAbility();
                u.isAIUnit = !factionList[factionID].isPlayerFaction && !factionList[factionID].isNetworkFaction;
                u.gameObject.SetActive(true);
            }
            LoadFactionData(factionID);
            foreach (Unit u in  factionList[factionID].startingUnitList) { 
                u.battleData = battleData;
                
                u.gameObject.SetActive(true);
            }
           


        }

        public void Init() {
            if (instance == null) instance = this;

            //if (generateUnitOnStart) GenerateUnit();

            //setup all the unit in the game
            for (int i = 0; i < factionList.Count; i++) {
                InitFaction(i);
            }


           
            

            if (!gameControl.EnableManualUnitDeployment()) {
                for (int i = 0; i < factionList.Count; i++) {
                    if (factionList[i].startingUnitList.Count <= 0 || !factionList[i].requireDeployment) continue;

                    AutoDeployFaction(i);
                    for (int n = 0; n < deployedUnitList.Count; n++) {
                        deployedUnitList[n].factionID = factionList[i].ID;
                        factionList[i].allUnitList.Add(deployedUnitList[n]);
                    }
                    deployedUnitList = new List<Unit>();
                }
            }

        }


        public void DeployUnit(Unit unit, DataUnit unitData,List<Tile> tileList) {
            if (unitData.tile_id == -1) return;
            if (unitData.tile_id < 0 || unitData.tile_id >= tileList.Count || unit.dataID != unitData.DataId) throw new BattleException();
            Tile tile = tileList.Find(t=>t.tileId==unitData.tile_id);
            unit.tile = tile;
            unit.tileId = unitData.tile_id;
            tile.unit = unit;
            unit.transform.position = tile.GetPos();
            onInsertUnitE(unit);
            //unit.gameObject.SetActive(true);
        }

        public void ModifyUnitsData(List<Unit> units, List<DataUnit> unitsData) {
            if (units.Count != unitsData.Count)
                throw new BattleException();
            /*for (int i = 0; i < units.Count; i++) {
                Debug.Log(units[i].ToString());
                Debug.Log(unitsData[i].ToString());
            }*/
            List<Tile> tileList = battleData.gridManager.GetGrid().tileList;
            for (int i = 0; i < units.Count; i++) {
                units[i].ModifyStatsToData(unitsData[i], unitsData[i].DataId);
                DeployUnit(units[i], unitsData[i], tileList);
            }
        }

        public void LoadFactionData(int factionID) {
            
            Faction fac = factionList[factionID];
            //Debug.Log("loading fac data, fac id:" + fac.dataID + " loadFromData:" + fac.loadFromData);
            fac.ID = factionID;
            //if load from data, then load the list from data and then put it to startingUnitList
            if (fac.loadFromData) {
                fac.startingUnitList = new List<Unit>();
                fac.dataList = savedData.GetLoadData(factionID);
                //Debug.Log("loading fac data, fac id:" + fac.dataID + " unit count:" + fac.dataList.Count);
                if (fac.dataList == null) {
                    Debug.LogWarning("TBTK faction's data not setup properly", this);
                    return;
                }
                //Debug.Log("unit from data: " + fac.dataList.Count);
                for (int n = 0; n < fac.dataList.Count; n++) fac.startingUnitList.Add(fac.dataList[n].unit);

                //put the data list back into the end data first, to save the current starting lineup for next menu loading
                //in case the player didnt finish the level and GameOver is not called
                savedData.SetEndData(factionID, fac.dataList);
            }
            else {
                //if using default startingUnitList, make sure none of the element in startingUnitList is empty
                for (int n = 0; n < fac.startingUnitList.Count; n++) {
                    if (fac.startingUnitList[n] == null) { fac.startingUnitList.RemoveAt(n); n -= 1; }

                    //fac.startingUnitList[n].battleData = battleData;
                }
            }
            fac.startingUnitList = CloneUnitList(fac.startingUnitList);

            InstantiateUnitList(fac.startingUnitList, fac);
            if (fac.loadFromData)
                ModifyUnitsData(fac.startingUnitList, fac.dataList);
            InstantiateUnitList(fac.allUnitList, fac);
            for (int i = 0; i < fac.allUnitList.Count; i++)
                fac.allUnitList[i].gameObject.SetActive(true);

            if ( !fac.isNetworkFaction) {
                if (fac.isPlayerFaction && deployingFactionID == -1) {
                    deployingFactionID = factionID;
                    requireDeployment = true;
                }
                fac.requireDeployment = true;
                    
                
            }
        }
        public List<Unit> CloneUnitList(List<Unit> clonedFrom) {
            List<Unit> clonedList = new List<Unit>();
            foreach (Unit u in clonedFrom) clonedList.Add(CloneUnit(u,transform));
            return clonedList;
        }

        public Unit CloneUnit(Unit clonedFrom, Transform parent, Vector3 pos = default(Vector3), Quaternion rot = default(Quaternion)) {
            bool clonedFromActive = clonedFrom.gameObject.activeSelf;
            clonedFrom.gameObject.SetActive(false);
            GameObject clonedObj = (GameObject)Instantiate(clonedFrom.gameObject, pos, rot);
            Unit clonedUnit = clonedObj.GetComponent<Unit>();
           // if (clonedFrom.battleData != null) clonedUnit.battleData = clonedFrom.battleData;
            clonedObj.transform.parent = parent;
            clonedObj.SetActive(false);
            clonedFrom.gameObject.SetActive(clonedFromActive);
            return clonedUnit;
        }

        public void InstantiateUnitList(List<Unit> units,Faction fac) {
            for (int n = 0; n < units.Count; n++) {
                units[n].battleData = battleData;
                units[n].InitUnitAbility();
                units[n].isAIUnit = !fac.isPlayerFaction && !fac.isNetworkFaction;
                units[n].dataID = n;
              
            }
        }



        /*****
        //called by GameControl to initiate the factions, 
        //load from data when needed, spawn the startingUnit, initiate the unit (abillities), check if unit deployment is required....
        public void Init(){
			if(instance==null) instance=this;
			
			if(generateUnitOnStart) GenerateUnit();
			
			//setup all the unit in the game
			for(int i=0; i<factionList.Count; i++){
				for(int n=0; n<factionList[i].allUnitList.Count; n++){
					factionList[i].allUnitList[n].InitUnitAbility();
					factionList[i].allUnitList[n].isAIUnit=!factionList[i].isPlayerFaction;
				}
			}
			
			Vector3 pos=new Vector3(0, 99999, 0);
			Quaternion rot=Quaternion.identity;
			for(int i=0; i<factionList.Count; i++){
				Faction fac=factionList[i];
				
				//if load from data, then load the list from data and then put it to startingUnitList
				if(fac.loadFromData){ 
					fac.startingUnitList=new List<Unit>();
					fac.dataList= savedData.GetLoadData(fac.dataID);
					if(fac.dataList==null){
						Debug.LogWarning("TBTK faction's data not setup properly", this);
						continue;
					}
					Debug.Log("unit from data: "+fac.dataList.Count);
					for(int n=0; n<fac.dataList.Count; n++) fac.startingUnitList.Add(fac.dataList[n].unit);

                    //put the data list back into the end data first, to save the current starting lineup for next menu loading
                    //in case the player didnt finish the level and GameOver is not called
                    savedData.SetEndData(fac.dataID, fac.dataList);
				}
				else{	
					//if using default startingUnitList, make sure none of the element in startingUnitList is empty
					for(int n=0; n<fac.startingUnitList.Count; n++){
						if(fac.startingUnitList[n]==null){ fac.startingUnitList.RemoveAt(n); n-=1; }
					}
				}
				
				for(int n=0; n<fac.startingUnitList.Count; n++){
					GameObject unitObj=(GameObject)Instantiate(fac.startingUnitList[n].gameObject, pos, rot);
                    Unit unit = unitObj.GetComponent<Unit>();
                    unit.battleData = battleData;
                    fac.startingUnitList[n]= unit;
					fac.startingUnitList[n].InitUnitAbility();
					fac.startingUnitList[n].isAIUnit=!fac.isPlayerFaction;
					unitObj.transform.parent=transform;
					unitObj.SetActive(false);

                    if (fac.loadFromData) fac.startingUnitList[n].ModifyStatsToData(fac.dataList[n], n);
                    else fac.startingUnitList[n].dataID = n;
                }
				
				if(fac.isPlayerFaction && fac.startingUnitList.Count>0 && !requireDeployment){
					if(deployingFactionID==-1) deployingFactionID=i;
					requireDeployment=true;
				}
			}
			
			if(!gameControl.EnableManualUnitDeployment()){
				for(int i=0; i<factionList.Count; i++){
					if(factionList[i].startingUnitList.Count<=0) continue;
					
					AutoDeployFaction(i);
					for(int n=0; n<deployedUnitList.Count; n++){
						deployedUnitList[n].factionID=factionList[i].ID;
						factionList[i].allUnitList.Add(deployedUnitList[n]);
					}
					deployedUnitList=new List<Unit>();
				}
			}
		}
		*****/
        //called by GameControl just before the game start to initiate all the faction, after unit deployment is done
        //sort out the unit move order, reset trigger status and what not.
        public void SetupFaction(int factionID) { instance._SetupFaction(factionID); }
        void _SetupFaction(int factionID) {
            Faction faction = factionList[factionID];
            
                if (faction.isPlayerFaction && !faction.isNetworkFaction) {
                    playerFactionIDList.Add(faction.ID);

                    for (int n = 0; n < faction.allUnitList.Count; n++) {
                    faction.allUnitList[n].battleData = battleData;
                        UpdateHostileUnitTriggerStatus(faction.allUnitList[n]);
                    }
                }

                if (turnControl.GetTurnMode() != _TurnMode.UnitPerTurn) {
                    if (turnControl.GetMoveOrder() != _MoveOrder.Random) {
                    faction.allUnitList = ArrangeUnitListToMovePriority(faction.allUnitList);
                    }
                }

            faction.ResetFactionTurnData(turnControl);

              //  totalUnitCount += factionList[i].allUnitList.Count;
            

            if (factionList.Count == playerFactionIDList.Count) hasAIInGame = false;

            if (turnControl.GetTurnMode() == _TurnMode.UnitPerTurn) {
                for (int i = 0; i < factionList.Count; i++) {
                    for (int n = 0; n < factionList[i].allUnitList.Count; n++) allUnitList.Add(factionList[i].allUnitList[n]);
                }

                allUnitList = ArrangeUnitListToMovePriority(allUnitList);
            }
        }




        void OnEnable() {
            battleData.unitListeners.onUnitDestroyedE += OnUnitDestroyed;
        }
        void OnDisable() {
            battleData.unitListeners.onUnitDestroyedE -= OnUnitDestroyed;
        }


        public void StartUnitDeploymentPhase() {
            gridManager.DeployingFaction(instance.deployingFactionID);
            if (onUnitDeploymentPhaseE != null) onUnitDeploymentPhaseE(true);
        }


        void OnUnitDestroyed(Unit unit) {
           // totalUnitCount -= 1;

            //assume isObjectUnit is the boolean flag
            if (unit.isObjectUnit) {
                battleData.battleController.EndBattle(battleData);   //assume 0 is the player unit faction
                return;
            }

            //remove unit from allUnitList so it no longers reserve a turn
            if (turnControl.GetTurnMode() == _TurnMode.UnitPerTurn) {
                int ID = allUnitList.IndexOf(unit);
                if (ID <= selectedUnitID) {
                    selectedUnitID -= 1;
                }
                allUnitList.Remove(unit);
            }

            //remove the unit from the faction, and if the faction has no active unit remain, the faction itself is remove (out of the game)
            for (int i = 0; i < factionList.Count; i++) {
                if (factionList[i].ID == unit.factionID) {
                    factionList[i].RemoveUnit(unit);
                    if (factionList[i].allUnitList.Count == 0) {
                        factionList.RemoveAt(i);
                        if (selectedFactionID > i) selectedFactionID -= 1;
                        else if (selectedFactionID == i) turnControl.EndTurn();
                    }
                    break;
                }
            }

            //if there's only 1 faction remain (since faction with no active unit will be removed), then faction has won the game
            if (factionList.Count == 1) battleData.battleController.EndBattle(battleData);
        }

        //called when a unit has its turn priority changed, to update the move order
        public void UnitTurnPriorityChanged(List<int> facIDList) { instance._UnitTurnPriorityChanged(facIDList); }
        public void _UnitTurnPriorityChanged(List<int> facIDList) {
            if (turnControl.GetTurnMode() == _TurnMode.UnitPerTurn) {
                allUnitList = ArrangeUnitListToMovePriority(allUnitList);
            }
            else if (turnControl.GetMoveOrder() != _MoveOrder.Random) {
                for (int i = 0; i < factionList.Count; i++) {
                    if (facIDList.Contains(factionList[i].ID)) {
                        factionList[i].allUnitList = ArrangeUnitListToMovePriority(factionList[i].allUnitList);
                    }
                }
            }
        }

        //generic function to sort a unit-list based on the unit turn priority
        public List<Unit> ArrangeUnitListToMovePriority(List<Unit> list) {
            List<Unit> newList = new List<Unit>();

            while (list.Count > 0) {
                float highest = 0;
                int highestID = 0;
                for (int i = 0; i < list.Count; i++) {
                    float priority = list[i].GetTurnPriority();
                    if (priority > highest) {
                        highest = priority;
                        highestID = i;
                    }
                }

                newList.Add(list[highestID]);
                list.RemoveAt(highestID);
            }

            return newList;
        }


        //called to update AI unit trigger status whenever a player unit moved to a new tile
        public void UpdateHostileUnitTriggerStatus(Unit unit) {
            if (!instance.hasAIInGame) return;

            List<Unit> unitList = GetAllHostileUnit(unit.factionID);
            for (int i = 0; i < unitList.Count; i++) {
                if (gridManager.GetDistance(unit.tile, unitList[i].tile) <= unitList[i].GetSight()) {
                    unitList[i].trigger = true;
                }
            }
        }


        //functional but not really required atm
        //moved unit to movedUnitList in faction
        public void UnitMoveDepleted(Unit unit) { instance._UnitMoveDepleted(unit); }
        public void _UnitMoveDepleted(Unit unit) {
            if (turnControl.GetTurnMode() != _TurnMode.FactionPerTurn) {
                for (int i = 0; i < factionList.Count; i++) {
                    if (factionList[i].ID == unit.factionID) {
                        factionList[i].UnitMoveDepleted(unit);
                    }
                }
            }
        }


        //used in FactionPerTurn mode only
        //switch to next faction in turn and select a unit based on the moveOrder
        public void SelectNextFaction() { instance._SelectNextFaction(); }
        public void _SelectNextFaction() {
            gameControl.ClearSelectedUnit();

            selectedFactionID += 1;
            if (selectedFactionID >= factionList.Count) selectedFactionID = 0;
            factionList[selectedFactionID].ResetFactionTurnData(turnControl);

            if (factionList[selectedFactionID].isPlayerFaction) {   //if it's a player's faction, select a unit
                if (turnControl.GetMoveOrder() == _MoveOrder.Free) {
                    factionList[selectedFactionID].SelectFirstAvailableUnit(gameControl);
                }
                else factionList[selectedFactionID].SelectNextUnitInQueue(gameControl, true);
            }
            else if (factionList[selectedFactionID].isNetworkFaction) {
                gameControl.DisplayMessage("Enemy's Turn");
            }
            else {                                                          //if it's a AI's faction, execute AI move
                                                                            //Debug.Log("AIManager.MoveFaction ------------------------" );
                if (turnControl.GetMoveOrder() == _MoveOrder.Free) {
                    gameControl.DisplayMessage("AI's Turn");
                    battleData.aiManager.MoveFaction(factionList[selectedFactionID]);
                }
                else {
                    gameControl.DisplayMessage("AI's Turn");
                    SelectNextUnitInFaction();  //when an AI unit pass to gameControl.Select(), AI routine will be called
                }
            }
        }

        //used in FactionPerTurn mode only, select the next unit in faction
        public void SelectNextUnitInFaction() { instance._SelectNextUnitInFaction(); }
        public void _SelectNextUnitInFaction() {
            if (selectedFactionID < 0) selectedFactionID = 0;

            if (turnControl.GetMoveOrder() == _MoveOrder.Free) {
                bool unitAvailable = factionList[selectedFactionID].SelectFirstAvailableUnit(gameControl);
                if (!unitAvailable) _SelectNextFaction();
            }
            else {
                //pass true to SelectNextUnitInQueue() so when the all unit in the faction has been selected before, the function will return true
                bool allUnitCycled = factionList[selectedFactionID].SelectNextUnitInQueue(gameControl, true);
                //original line
                //bool allUnitCycled=factionList[selectedFactionID].SelectNextUnitInQueue(gameControl,true);	
                if (allUnitCycled) _SelectNextFaction();
            }
        }

        //used in FactionUnitPerTurn mode only
        public void SelectNextUnitInNextFaction() { instance._SelectNextUnitInNextFaction(); }
        public void _SelectNextUnitInNextFaction() {
            selectedFactionID += 1;
            if (selectedFactionID >= factionList.Count) selectedFactionID = 0;
            factionList[selectedFactionID].SelectNextUnitInQueue(gameControl);
        }

        //used in UnitPerTurn mode only, select the next unit in turn
        public void SelectNextUnit() { instance._SelectNextUnit(); }
        public void _SelectNextUnit() {
            selectedUnitID += 1;
            if (selectedUnitID >= allUnitList.Count) selectedUnitID = 0;

            allUnitList[selectedUnitID].ResetUnitTurnData();
            gameControl.SelectUnit(allUnitList[selectedUnitID]);

            selectedFactionID = allUnitList[selectedUnitID].factionID;
        }



        public bool IsPlayerTurn() {
            return (playerFactionIDList.Contains(selectedFactionID)
                && !IsNetworkFaction(selectedFactionID)) ? true : false;
        }
        public bool IsPlayerFaction(int factionID) {
            return (playerFactionIDList.Contains(factionID)) ? true : false;
        }
        public bool IsNetworkFaction(int factionID) {
            return (factionList[factionID].isNetworkFaction) ? true : false;
        }
        //get the number of active unit on the grid
        public int GetAllUnitCount() { return GetAllUnit().Count; }
        public List<Unit> GetAllUnit() { return instance._GetAllUnit(); }
        public List<Unit> _GetAllUnit() {
            if (turnControl.GetTurnMode() == _TurnMode.UnitPerTurn) return allUnitList;
            else {
                List<Unit> list = new List<Unit>();
                for (int i = 0; i < factionList.Count; i++) {
                    for (int n = 0; n < factionList[i].allUnitList.Count; n++) list.Add(factionList[i].allUnitList[n]);
                }
                return list;
            }
        }

        //get all units that is hostile based on a factionID (basically all units that has different factionID)
        public List<Unit> GetAllHostileUnit(int factionID) { return instance._GetAllHostileUnit(factionID); }
        public List<Unit> _GetAllHostileUnit(int factionID) {
            List<Unit> list = new List<Unit>();
            for (int i = 0; i < factionList.Count; i++) {
                if (factionID == factionList[i].ID) continue;
                for (int n = 0; n < factionList[i].allUnitList.Count; n++) list.Add(factionList[i].allUnitList[n]);
            }
            return list;
        }

        //get all units that belong to all player factions
        public List<Unit> GetAllPlayerUnits() { return instance._GetAllPlayerUnits(); }
        public List<Unit> _GetAllPlayerUnits() {
            List<Unit> list = new List<Unit>();
            for (int i = 0; i < factionList.Count; i++) {
                if (!factionList[i].isPlayerFaction || factionList[i].isNetworkFaction) continue;
                for (int n = 0; n < factionList[i].allUnitList.Count; n++) list.Add(factionList[i].allUnitList[n]);
            }
            return list;
        }

        //get all unit that belong to a certain faction based on the factionID
        public List<Unit> GetAllUnitsOfFaction(int factionID) { return instance._GetAllUnitsOfFaction(factionID); }
        public List<Unit> _GetAllUnitsOfFaction(int factionID) {
            for (int i = 0; i < factionList.Count; i++) {
                if (factionID == factionList[i].ID) return factionList[i].allUnitList;
            }
            return new List<Unit>();
        }

        //get a certain faction based on factionID
        public Faction GetFaction(int factionID) { return instance._GetFaction(factionID); }
        public Faction _GetFaction(int factionID) {
            for (int i = 0; i < factionList.Count; i++) {
                if (factionList[i].ID == factionID) return factionList[i];
            }
            Debug.LogWarning("Faction with ID: " + factionID + " doesnt exist");
            return null;
        }

        //how many player's faction are there in the game
        public int GetPlayerFactionCount() { return instance.playerFactionIDList.Count; }


        //insert a unit to the grid in the middle of the game
        public void InsertUnit(Unit unit, int factionID = 0) { instance._InsertUnit(unit, factionID); }
        public void _InsertUnit(Unit unit, int factionID = 0) {
            unit.factionID = factionID;

            unit.InitUnitAbility();

            for (int i = 0; i < factionList.Count; i++) {
                if (factionList[i].ID == factionID) {
                    factionList[i].allUnitList.Add(unit);
                    unit.isAIUnit = !factionList[i].isPlayerFaction;
                }
            }

            if (turnControl.GetTurnMode() == _TurnMode.UnitPerTurn) allUnitList.Add(unit);

            _UnitTurnPriorityChanged(new List<int> { unit.factionID });

            unit.UpdateVisibility();

            if (onInsertUnitE != null) onInsertUnitE(unit);
        }








        //********************************************************************************************************************************
        //these section are related to starting unit deployment of the factions

        private bool requireDeployment = false; //set to true when user need to manually deploy the starting units
        public bool RequireManualUnitDeployment() { return gameControl.EnableManualUnitDeployment() & instance.requireDeployment; }


        public int deployingFactionID = -1;     //Index of current faction which units are being deployed
        [HideInInspector]
        public int deployingUnitID = 0;         //index of the unit currently being deployed in the startingUnitList of the faction
        public int GetDeployingFactionID() { return instance.deployingFactionID; }
        public int GetDeployingUnitID() { return instance.deployingUnitID; }
        public List<Unit> deployedUnitList = new List<Unit>();  //a list of unit store the deployed unit before the deployment is complete, 
                                                                //this is to keep track of which unit is deployed and which unit is not so the deployed unit can be remove from the grid to be deployed as well
                                                                //the list will be cleared as soon as the deployment for the faction is done

        public void PrevDeployingUnitID() { instance._PrevDeployingUnitID(); }  //rewind deployingUnitID
        public void _PrevDeployingUnitID() {
            deployingUnitID -= 1;
            if (deployingUnitID < 0) deployingUnitID = factionList[deployingFactionID].startingUnitList.Count - 1;
        }
        public void NextDeployingUnitID() { instance._NextDeployingUnitID(); }  //next deployingUnitID
        public void _NextDeployingUnitID() {
            deployingUnitID += 1;
            if (deployingUnitID >= factionList[deployingFactionID].startingUnitList.Count) deployingUnitID = 0;
        }

        public void DeployUnitOnTile(Tile tile) { instance._DeployUnitOnTile(tile); }
        public void _DeployUnitOnTile(Tile tile) {
            if (tile.deployAreaID != factionList[deployingFactionID].ID) return;
            if (factionList[deployingFactionID].startingUnitList.Count <= 0) return;
            Unit unit = factionList[deployingFactionID].startingUnitList[deployingUnitID];
            deployedUnitList.Add(unit);
            tile.unit = unit;
            unit.tileId = gridManager.grid.tileList.IndexOf(tile);
            unit.tile = tile;
            unit.transform.position = tile.GetPos();
            unit.gameObject.SetActive(true);
            factionList[deployingFactionID].startingUnitList.RemoveAt(deployingUnitID);
            if (factionList[deployingFactionID].startingUnitList.Count <= 0) deployingUnitID = 0;
            else {
                if (deployingUnitID >= factionList[deployingFactionID].startingUnitList.Count) deployingUnitID -= 1;
            }
            if (onUnitDeployedE != null) onUnitDeployedE(null);
        }
        public void UndeployUnit(Unit unit) { instance._UndeployUnit(unit); }
        public void _UndeployUnit(Unit unit) {
            if (!deployedUnitList.Contains(unit)) return;
            deployedUnitList.Remove(unit);
            unit.tile.unit = null;
            unit.tile = null;
            unit.gameObject.SetActive(false);
            if (deployingUnitID == 0) factionList[deployingFactionID].startingUnitList.Add(unit);
            else factionList[deployingFactionID].startingUnitList.Insert(deployingUnitID - 1, unit);
            if (onUnitDeployedE != null) onUnitDeployedE(null);
        }

        //called to complete unit deployment of a particular faction
        //move on to next faction needs unit deployment
        public void CompleteDeployment() {
            instance._CompleteDeployment();
        }
        public void _CompleteDeployment() {
            if (!_IsDeploymentComplete()) return;
            if (deployingFactionID >= 0) {
                for (int i = 0; i < deployedUnitList.Count; i++) {
                    deployedUnitList[i].factionID = factionList[deployingFactionID].ID;
                    factionList[deployingFactionID].allUnitList.Add(deployedUnitList[i]);
                }
                //if (factionList[deployingFactionID].deployed == false)
                   // battleData.waitingPlayers--;
                factionList[deployingFactionID].deployed = true;
               
            }
            deployedUnitList = new List<Unit>();
            deployingUnitID = -1;
            gridManager.FactionDeploymentComplete();


            //iterate thru all faction, get the next faction needs unit deployment
            for (int i = deployingFactionID + 1; i < factionList.Count; i++) {
                if (factionList[i].deployed) continue;
                if (!factionList[i].isNetworkFaction) {
                    if (factionList[i].isPlayerFaction) {
                        deployingFactionID = i;
                        deployingUnitID = 0;    //set to >=0 to indicate there's a player faction needs deployment
                        break;
                    }
                    else  {  //if it's an AI faction, automatically deploy it
                        gridManager.DeployingFaction(i);
                        AutoDeployFaction(i);

                        for (int n = 0; n < deployedUnitList.Count; n++) {
                            deployedUnitList[n].factionID=factionList[i].ID;
                            factionList[i].allUnitList.Add(deployedUnitList[n]);
                        }
                    }
                }
            }


            if (deployingUnitID == -1) {    //no more faction needs deloyment, start the game

                if (onUnitDeploymentPhaseE != null) onUnitDeploymentPhaseE(false);
                //battleData.battleController.DeployArmies(battleData);
            }
            else {                          //another player's faction needs deloyment, initiate the process
                gridManager.DeployingFaction(deployingFactionID);
                if (onUnitDeploymentPhaseE != null) onUnitDeploymentPhaseE(true);
            }
        }

        //automatically deployed the unit for the current deploying faction, used for both AI and player faction
        public void AutoDeployCurrentFaction() { 
            instance.AutoDeployFaction(); }
        public void AutoDeployFaction(int factionID = -1) {
            if (factionID == -1) factionID = deployingFactionID;

            //if (factionList[factionID].deployed == false) battleData.waitingPlayers--;
            factionList[factionID].deployed = true;
            bool setToInvisible = gameControl.EnableFogOfWar() & (!factionList[factionID].isPlayerFaction || factionList[factionID].isNetworkFaction);

            List<Unit> unitList = factionList[factionID].startingUnitList;
            List<Tile> tileList = gridManager.GetDeployableTileList(factionList[factionID].ID);

            for (int i = 0; i < tileList.Count; i++) {
                if (!tileList[i].walkable || tileList[i].unit != null || tileList[i].obstacleT != null) {
                    tileList.RemoveAt(i); i -= 1;
                }
            }

            int count = 0;
            for (int i = 0; i < unitList.Count; i++) {
                if (tileList.Count == 0) break;
                Unit unit = unitList[i];

                int index = Random.Range(0, tileList.Count);
                Tile tile = tileList[index];
                if (unit.tileId >= 0) {
                    Tile t = gridManager.grid.tileList[unit.tileId];
                    index = tileList.IndexOf(t);
                    tile = t;
                }
                else {
                    unit.tileId = gridManager.grid.tileList.IndexOf(tile);
                }
                tileList.RemoveAt(index);
                tile.unit = unit;
                unit.tile = tile;
                unit.transform.position = tile.GetPos();
                unit.gameObject.SetActive(true);

                deployedUnitList.Add(unit);

                if (setToInvisible && !tile.IsVisible()) {
                    unit.gameObject.layer = LayerManager.GetLayerUnitInvisible();
                    Utilities.SetLayerRecursively(unit.transform, LayerManager.GetLayerUnitInvisible());
                }

                count += 1;

                unit.factionID = factionList[factionID].ID;
               
            }
            
            for (int i = 0; i < count; i++) unitList.RemoveAt(0);
        }

        //check if a faction has deployed all its startingUnitList,
        //the deployment is considered complete when eitehr the startingUnitList is empty or there are no more tile available
        public bool IsDeploymentComplete() { return instance._IsDeploymentComplete(); }
        public bool _IsDeploymentComplete() {
            bool flag3 = factionList[deployingFactionID].deployed;
            bool flag1 = factionList[deployingFactionID].startingUnitList.Count > 0;
            bool flag2 = gridManager.GetDeployableTileListCount() > 0;
            if (flag1 && flag2) return false;
            return true;
        }

        public List<Unit> GetDeployingUnitList() {
            return instance.factionList[instance.deployingFactionID].startingUnitList;
        }

        //end faction deployment mode related function
        //**************************************************************************************





        public void GenerateUnit() { instance._GenerateUnit(); }
        public void _GenerateUnit() {
            for (int i = 0; i < factionList.Count; i++) factionList[i].Spawn(this);
        }
        public void ClearUnit(bool destroyStarting=true) {
            
            for (int i = 0; i < factionList.Count; i++) factionList[i].ClearUnit(destroyStarting);
        }

        //use in edit mode only
        public void RecordSpawnTilePos() {
            for (int i = 0; i < factionList.Count; i++) factionList[i].RecordSpawnTilePos();
        }
        //use in edit mode only
        public void SetStartingTileListBaseOnPos(float tileSize = 1) {
            for (int i = 0; i < factionList.Count; i++) factionList[i].SetStartingTileListBaseOnPos(gridManager, tileSize);
        }



        private float gizmoSize1 = 0.25f;
        private float gizmoSize2 = 0.35f;

        void OnDrawGizmos() {
            for (int i = 0; i < factionList.Count; i++) {

                Faction fac = factionList[i];
                Gizmos.color = fac.color;

                for (int n = 0; n < fac.spawnInfoList.Count; n++) {
                    for (int m = 0; m < fac.spawnInfoList[n].startingTileList.Count; m++) {
                        Vector3 pos = fac.spawnInfoList[n].startingTileList[m].GetPos();
                        Gizmos.DrawSphere(pos, gizmoSize1);
                    }
                }

                for (int n = 0; n < fac.deployableTileList.Count; n++) {
                    Vector3 pos = fac.deployableTileList[n].GetPos();
                    Gizmos.DrawLine(pos + new Vector3(gizmoSize2, 0, gizmoSize2), pos + new Vector3(-gizmoSize2, 0, -gizmoSize2));
                    Gizmos.DrawLine(pos + new Vector3(-gizmoSize2, 0, gizmoSize2), pos + new Vector3(gizmoSize2, 0, -gizmoSize2));
                }
            }
        }

    }

}