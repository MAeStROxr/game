﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using TBTK;

namespace TBTK{

	public class UI : MonoBehaviour {
        GameControl gameControl
        {
            get
            {
               return battleData.gameControl;
            }
        }
        FactionManager factionManager
        {
            get
            {
                return battleData.factionManager;
            }

        }
        public Battle battleData;
        TurnControl turnControl
        {
            get
            {
                return battleData.turnControl;
            }
        }
        public UIUnitDeployment deployment;
        public UIPerkMenu uiPerkMenu;
        public float scaleFactor=1;
		public  float GetScaleFactor(){ return instance.scaleFactor; }
		
		public GameObject endTurnButtonObj;
		
		public bool disablePerkMenu=false;
		
		private  UI instance;
		
		void Awake(){
			instance=this;
			//transform.position=Vector3.zero;
            battleData = gameObject.GetComponentInParent<Battle>();

        }

        // Use this for initialization
        void Start () {
			endTurnButtonObj.SetActive(false);
			
			if(disablePerkMenu) uiPerkMenu.Disable();
		}
		
		void OnEnable(){
			factionManager.onUnitDeploymentPhaseE += OnUnitDeploymentPhase;
            battleData.unitListeners.onUnitSelectedE += OnUnitSelected;
			
			gameControl.onGameOverE += OnGameOver;
		}
		void OnDisable(){
			factionManager.onUnitDeploymentPhaseE -= OnUnitDeploymentPhase;
			battleData.unitListeners.onUnitSelectedE -= OnUnitSelected;
			
			gameControl.onGameOverE -= OnGameOver;
		}
		
		
		void OnUnitDeploymentPhase(bool flag){
            if(flag) deployment.Show();
			else deployment.Hide();
			//StartCoroutine(_OnUnitDeploymentPhase());
		}
		IEnumerator _OnUnitDeploymentPhase(){
			yield return null;
			
		}
		
		
		void OnUnitSelected(Unit unit){
			if(unit!=null && factionManager.IsPlayerTurn()) endTurnButtonObj.SetActive(true);
			else endTurnButtonObj.SetActive(false);
		}
		
		
		public void OnEndTurnButton(){
			if(!turnControl.ClearToProceed()) return;
            if (!factionManager.IsPlayerTurn()) return;
            battleData.battleController.EndTurn(battleData);
		}
		
		
		
		public void OnGameOver(int factionID){ StartCoroutine(ShowGameOverScreen(factionID)); }
		IEnumerator ShowGameOverScreen(int factionID){
			yield return new WaitForSeconds(2);
			//UIGameOver.Show(factionID);
		}
		
		
		// Update is called once per frame
		void Update () {
		
		}
		
	}

}