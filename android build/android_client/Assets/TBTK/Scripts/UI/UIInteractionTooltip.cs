﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

using TBTK;

namespace TBTK{

	public class UIInteractionTooltip : MonoBehaviour {
        Battle battleData;
        GameControl gameControl
        {
            get
            {
                return battleData.gameControl;
            }
        }
        FactionManager factionManager
        {
            get
            {
                return battleData.factionManager;
            }

        }
        GridManager gridManager
        {
            get
            {
                return battleData.gridManager;
            }
        }
        public UI ui;
        public Transform anchorLeft;
		public Transform anchorRight;
		
		public Text lbMoveAPCost;
		
		public GameObject attackTooltipObj;
		private Transform attackTooltipObjT;
		private Unit tgtUnit;
		public Text lbAP;
		public Text lbStats;
		public Text lbSpecial;
		public Text lbCover;
		
		// Use this for initialization
		void Start () {
            battleData = gameObject.transform.root.GetComponentInChildren<Battle>();

            attackTooltipObjT = attackTooltipObj.transform;
			
			attackTooltipObj.SetActive(false);
			
			//anchorLeft.position=Vector3.zero;
			//anchorRight.position=Vector3.zero;

			lbMoveAPCost.gameObject.GetComponent<RectTransform>().sizeDelta=new Vector2(1, 1);
		}
		
		// Update is called once per frame
		void Update () {
			if(tgtUnit!=null && attackTooltipObj.activeInHierarchy){
				UpdateAttackTooltipPos();
			}
		}
		
		void OnEnable(){
            battleData = gameObject.transform.root.GetComponentInChildren<Battle>();

            gridManager.onHoverAttackableTileE += OnHoverAttackableTile;
			gridManager.onExitAttackableTileE += OnExitAttackableTile;
			
			gridManager.onHoverWalkableTileE += OnHoverWalkableTile;
			gridManager.onExitWalkableTileE += OnExitWalkableTile;
		}
		void OnDisable(){
			gridManager.onHoverAttackableTileE -= OnHoverAttackableTile;
			gridManager.onExitAttackableTileE -= OnExitAttackableTile;
			
			gridManager.onHoverWalkableTileE -= OnHoverWalkableTile;
			gridManager.onExitWalkableTileE -= OnExitWalkableTile;
		}
		
		
		void OnHoverWalkableTile(Tile tile){
			if(!gameControl.UseAPForMove()) return;
			Vector3 screenPos = Camera.main.WorldToScreenPoint(tile.GetPos());
			lbMoveAPCost.transform.localPosition=(screenPos+new Vector3(0, 0, 0))/ui.GetScaleFactor();
			lbMoveAPCost.text=(gameControl.selectedUnit.GetMoveAPCost()*tile.distance)+"AP";
		}
		void OnExitWalkableTile(){
			lbMoveAPCost.text="";
		}
		
		
		void OnHoverAttackableTile(Tile tile){
			
			Unit srcUnit=gameControl.selectedUnit;
			if(srcUnit==null){
				OnExitAttackableTile();
				return;
			}
			
			tgtUnit=tile.unit;
			
			AttackInstance attInstance=new AttackInstance(battleData, srcUnit, tgtUnit);
			attInstance.Process();
			
			if(srcUnit.isHybridUnit){
				attInstance.IsMelee=gridManager.GetDistance(srcUnit.tile, tile)<=srcUnit.GetMeleeRange();
			}
			
			
			lbAP.text=srcUnit.GetAttackAPCost()+"AP";
			
			
			float dmgMin=srcUnit.GetDamageMin()*attInstance.DamageTableModifier*attInstance.FlankingBonus;
			float dmgMax=srcUnit.GetDamageMax()*attInstance.DamageTableModifier*attInstance.FlankingBonus;
			
			string damage=dmgMin.ToString("f0")+" - "+dmgMax.ToString("f0")+"\n";
			string hitChance=(attInstance.HitChance*100).ToString("f0")+"%\n";
			string critChance=(attInstance.CritChance*100).ToString("f0")+"%\n";
			lbStats.text=damage+hitChance+critChance;
			
			
			lbSpecial.text="";
			if(attInstance.StunChance>0){
				lbSpecial.text+=(attInstance.StunChance*100).ToString("f0")+"% chance to stun\n";
			}
			
			bool canCounter=tgtUnit.CanCounter(srcUnit);
			if(canCounter) lbSpecial.text+="Target can counter\n";
			
			
			lbCover.text="";
			if(gameControl.EnableCover()){
				if(attInstance.coverType==CoverSystem._CoverType.None) lbCover.text="Target not in cover\n";
				else if(attInstance.coverType==CoverSystem._CoverType.Half) lbCover.text="Target in half cover\n";
				else if(attInstance.coverType==CoverSystem._CoverType.Full) lbCover.text="Target in full cover\n";
			}
			
			if(gameControl.EnableFlanking() && attInstance.Flanked) lbCover.text+="Flanking Attack";
			
			
			UpdateAttackTooltipPos();
			
			attackTooltipObj.SetActive(true);
		}
		void OnExitAttackableTile(){
			tgtUnit=null;
			attackTooltipObj.SetActive(false);
		}
		
		void UpdateAttackTooltipPos(){
			Vector3 screenPos = Camera.main.WorldToScreenPoint(tgtUnit.thisT.position)/ ui.GetScaleFactor();
			
			float posX=0;
			if(screenPos.x>(Screen.width/ ui.GetScaleFactor())/2) posX=screenPos.x-230/2-60;
			else posX=screenPos.x+230/2+60;
			
			float posY=screenPos.y;
			if(screenPos.y<(Screen.height/ ui.GetScaleFactor())/2) posY=screenPos.y+190;
			
			attackTooltipObjT.localPosition=new Vector3(posX, posY, 0);
		}
		
	}

}
