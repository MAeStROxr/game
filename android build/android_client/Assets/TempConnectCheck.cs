﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System;

public class TempConnectCheck : MonoBehaviour {
    string hostname = "localhost";
    int port = 5050;
    // Use this for initialization
    bool UseTCP = false;
    IMessageHandler messageHandlerWorld;
    void Start() {
        Debug.Log("trying to connect:" + NetworkStatus().ToString());
    }
	 NetworkHelperStatus NetworkStatus() {
     /*   try {
            AtavismLogger.LogInfoMessage(string.Concat(new object[] { "Connecting to rdp world server at ", hostname, ":", port }));
            IPAddress address = GetIPv4Address(hostname);
            if (address == null) {
                AtavismLogger.LogWarning("No valid IPv4 address for " + hostname);
                return NetworkHelperStatus.WorldConnectFailure;
            }
            IPEndPoint remote = new IPEndPoint(address, port);
            MessageDispatcher dispatcher = new DefragmentingMessageDispatcher();
            if (this.UseTCP) {
                this.messageHandlerWorld = new TcpWorldMessageHandler(remote, dispatcher);
            }
            else {
                this.messageHandlerWorld = new RdpWorldMessageHandler(remote, dispatcher);
            }
            this.messageHandlerWorld.BeginListen();
            
        }
        catch (Exception exception) {
            AtavismLogger.LogWarning(string.Concat(new object[] { "Exception connecting to rdp world server ", hostname, ":", port, ":", exception }));
            //this.RdpWorldDisconnect();
            AtavismLogger.LogError(string.Concat(new object[] { "Failed to connect to rdp world server at ", hostname, ":", port }));
            return NetworkHelperStatus.WorldConnectFailure;
        }
        AtavismRdpConnection conn = base.GetConnection(base.udpConn, remoteEP, false);
        try {
            if (!conn.WaitForState(ConnectionState.Open, millisecondsTimeout)) {
                throw new TimeoutException();
            }
        }
        catch (Exception) {
            base.ReleaseConnection(conn);
            throw;
        }*/
        return NetworkHelperStatus.Success;
    }

    private static IPAddress GetIPv4Address(string hostname) {
        IPAddress address = null;
        if (!IPAddress.TryParse(hostname, out address)) {
            foreach (IPAddress address2 in Dns.GetHostEntry(hostname).AddressList) {
                if (address2.AddressFamily == AddressFamily.InterNetwork) {
                    return address2;
                }
            }
        }
        return address;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
