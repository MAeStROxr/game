﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
/// Credit Ralph Barbagallo (www.flarb.com /www.ralphbarbagallo.com / @flarb)
/// Sourced from - http://forum.unity3d.com/threads/vr-cursor-possible-unity-4-6-gui-bug-or-is-it-me

using System.Collections;
namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu("UI/Extensions/VR Cursor")]
    public class VRCursor : MonoBehaviour
    {
        public float xSens;
        public float ySens;

        private Collider currentCollider;

        // Update is called once per frame
        void Update()
        {
            Vector3 thisPosition;

            thisPosition.x = ControlFreak2.CF2Input.mousePosition.x * xSens;
            thisPosition.y = ControlFreak2.CF2Input.mousePosition.y * ySens - 1;
            thisPosition.z = transform.position.z;

            transform.position = thisPosition;

            VRInputModule.cursorPosition = transform.position;

            if (ControlFreak2.CF2Input.GetMouseButtonDown(0) && currentCollider)
            {
                VRInputModule.PointerSubmit(currentCollider.gameObject);
            }

        }

        void OnTriggerEnter(Collider other)
        {
            //print("OnTriggerEnter other " + other.gameObject);
            VRInputModule.PointerEnter(other.gameObject);
            currentCollider = other;
        }

        void OnTriggerExit(Collider other)
        {
            //print("OnTriggerExit other " + other.gameObject);
            VRInputModule.PointerExit(other.gameObject);
            currentCollider = null;
        }
    }
}