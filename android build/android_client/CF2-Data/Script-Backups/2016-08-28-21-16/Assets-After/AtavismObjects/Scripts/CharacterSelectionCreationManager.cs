﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public enum CreationState
{
	Body,
	Head,
	Face,
	Hair
}

/// <summary>
/// Handles the selection and creation of the players characters. This script must be added
/// as a component in the Character Creation / Selection scene.
/// </summary>
public class CharacterSelectionCreationManager : MonoBehaviour
{
	protected static CharacterSelectionCreationManager instance;
	
	public List<GameObject> createUI;
	public List<GameObject> selectUI;
	public List<UGUICharacterSelectSlot> characterSlots;
	public GameObject enterUI;
	public Button createButton;
	public Text nameUI;
	public Button deleteButton;
	public Text serverNameText;
	public UGUIServerList serverListUI;
	public GameObject characterCamera;
	public UGUIDialogPopup dialogWindow;
	public Transform spawnPosition;
	
	protected GameObject character;
	protected List<CharacterEntry> characterEntries;
	protected CreationState creationState;
	protected LoginState loginState;
	protected string dialogMessage = "";
	protected string errorMessage = "";
	
	// Character select fields
	protected CharacterEntry characterSelected = null;
	protected string characterName = "";
	protected AtavismRaceData race;
	protected AtavismClassData aspect;
	protected string gender = "Male";
	public List<UGUICharacterRaceSlot> races;
	public List<UGUICharacterClassSlot> classes;
	
	public Image raceIcon;
	public Text raceTitle;
	public Text raceDescription;
	
	public Image classIcon;
	public Text classTitle;
	public Text classDescription;
	
	
	// Camera fields
	public Vector3 cameraInLoc = new Vector3( 0.193684f, 2.4f, 4.743689f);
	public Vector3 cameraOutLoc = new Vector3( 0.4418643f, 1.21f, 6.72f);
	protected bool zoomingIn = false;
	protected bool zoomingOut = false;
	public float characterRotationSpeed = 250.0f;
	protected float x = 180;
	protected float y = 0;

	// Use this for initialization
	void Start ()
	{
		instance = this;
		
		StartCharacterSelection ();
		if (characterEntries.Count == 0) {
			StartCharacterCreation ();
		}
		if (characterCamera != null)
			characterCamera.SetActive(true);
	}
	
	// Update is called once per frame
	void Update ()
	{
		float moveRate = 1.0f;
		if (zoomingIn) {
			characterCamera.transform.position = Vector3.Lerp (characterCamera.transform.position, cameraInLoc, Time.deltaTime * moveRate);
		} else if (zoomingOut) {
			characterCamera.transform.position = Vector3.Lerp (characterCamera.transform.position, cameraOutLoc, Time.deltaTime * moveRate);
		}
	}
	
	/// <summary>
	/// Handles character rotation if the mouse button is down and the player is dragging it.
	/// </summary>
	void LateUpdate () {
		//TODO: currently this is artificially limited to only work in the middle 1/3 of the screen, this restriction should be removed
		if (character && ControlFreak2.CF2Input.GetMouseButton(0) && (ControlFreak2.CF2Input.mousePosition.x > Screen.width / 3) && (ControlFreak2.CF2Input.mousePosition.x < Screen.width / 3 * 2)) {
			x -= ControlFreak2.CF2Input.GetAxis("Mouse X") * characterRotationSpeed * 0.02f;
 		       
        	Quaternion rotation = Quaternion.Euler(y, x, 0);
        
        	//position.y = height;
        	character.transform.rotation = rotation;
    	}
	}
	
	#region Character Selection
	public void StartCharacterSelection ()
	{
		characterEntries = ClientAPI.GetCharacterEntries ();
		ShowSelectionUI ();
		
		if (characterEntries.Count > 0) {
			CharacterSelected (characterEntries [0]);
			if (enterUI != null) {
				enterUI.SetActive(true);
			}
			if (nameUI != null) {
				nameUI.gameObject.SetActive(true);
			}
			if (deleteButton != null) {
				deleteButton.gameObject.SetActive(true);
			}
		} else {
			if (enterUI != null) {
				enterUI.SetActive(false);
			}
			if (nameUI != null) {
				nameUI.gameObject.SetActive(false);
			}
			if (deleteButton != null) {
				deleteButton.gameObject.SetActive(false);
			}
		}
		if (characterEntries.Count < characterSlots.Count) {
			createButton.gameObject.SetActive(true);
		} else {
			createButton.gameObject.SetActive(false);
		}
		
		// Set the slots up
		for (int i = 0; i < characterSlots.Count; i++) {
			if (characterEntries.Count > i) {
				// Set slot data
				characterSlots[i].gameObject.SetActive(true);
				characterSlots[i].SetCharacter(characterEntries[i]);
				characterSlots[i].CharacterSelected(characterSelected);
			} else {
				// Set slot data to null
				characterSlots[i].gameObject.SetActive(false);
				//characterSlots[i].SendMessage("SetCharacter", null);
			}
		}
		
		loginState = LoginState.CharacterSelect;
	}

	public void StartCharacterSelection(List<CharacterEntry> characterEntries) 
	{
		this.characterEntries = characterEntries;
		StartCharacterSelection ();
	}
	
	public virtual void CharacterSelected (CharacterEntry entry)
	{
		characterSelected = entry;
		foreach (UGUICharacterSelectSlot charSlot in characterSlots) {
			charSlot.CharacterSelected(characterSelected);
		}
		if (character != null)
			Destroy (character);
		race = GetRaceDataByName((string)characterSelected ["race"]);
		gender = (string)characterSelected ["gender"];
		Dictionary<string, object> appearanceProps = new Dictionary<string, object>();
		foreach (string key in entry.Keys) {
			if (key.StartsWith("custom:appearanceData:")) {
				appearanceProps.Add(key.Substring(23), entry[key]);
			}
		}
		// Dna settings
		string prefabName = (string)characterSelected ["model"];
		if (prefabName.Contains(".prefab"))
		{
			int resourcePathPos = prefabName.IndexOf("Resources/");
			prefabName = prefabName.Substring(resourcePathPos + 10);
			prefabName = prefabName.Remove(prefabName.Length - 7);
		}
		GameObject prefab = (GameObject)Resources.Load (prefabName);
		if (prefab != null)
			character = (GameObject) Instantiate(prefab, spawnPosition.position, spawnPosition.rotation);
		//SetCharacter (prefab);
		// Set equipment
		if (characterSelected.ContainsKey("weaponDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("weaponDisplayID", (string)characterSelected["weaponDisplayID"]);
		}
		if (characterSelected.ContainsKey("weapon2DisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("weapon2DisplayID", (string)characterSelected["weapon2DisplayID"]);
		}
		// UMA-equipment properties
		if (characterSelected.ContainsKey("legDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("legDisplayID", (string)characterSelected["legDisplayID"]);
		}
		if (characterSelected.ContainsKey("chestDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("chestDisplayID", (string)characterSelected["chestDisplayID"]);
		}
		if (characterSelected.ContainsKey("headDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("headDisplayID", (string)characterSelected["headDisplayID"]);
		}
		if (characterSelected.ContainsKey("feetDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("feetDisplayID", (string)characterSelected["feetDisplayID"]);
		}
		if (characterSelected.ContainsKey("handDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("handDisplayID", (string)characterSelected["handDisplayID"]);
		}
		if (characterSelected.ContainsKey("capeDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("capeDisplayID", (string)characterSelected["capeDisplayID"]);
		}
		if (characterSelected.ContainsKey("shoulderDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("shoulderDisplayID", (string)characterSelected["shoulderDisplayID"]);
		}
		if (characterSelected.ContainsKey("beltDisplayID")) {
			character.GetComponent<AtavismMobAppearance>().UpdateEquipDisplay("beltDisplayID", (string)characterSelected["beltDisplayID"]);
		}
		// Name
		if (nameUI != null)
			nameUI.text = (string)entry["characterName"];
	}
	
	public void Play() {
		dialogMessage = "Entering World...";
		AtavismClient.Instance.EnterGameWorld(characterSelected.CharacterId);
	}
	
	public void DeleteCharacter() {
		dialogWindow.gameObject.SetActive(true);
		dialogWindow.ShowDialogOptionPopup("Do you want to delete character: " + characterSelected["characterName"]);
	}

	public virtual void DeleteCharacterConfirmed() {
		Dictionary<string, object> attrs = new Dictionary<string, object>();
		attrs.Add("characterId", characterSelected.CharacterId);
		NetworkAPI.DeleteCharacter(attrs);
		foreach (CharacterEntry charEntry in characterEntries) {
			if (charEntry != characterSelected) {
				CharacterSelected(charEntry);
				characterSelected = charEntry;
				StartCharacterSelection ();
				return;
			}
		}
		StartCharacterCreation();
	}
	#endregion Character Selection
	
	#region Character Creation
	public virtual void StartCharacterCreation ()
	{
		ShowCreationUI ();
		if (enterUI != null)
			enterUI.SetActive(false);
			
		race = races[0].raceData;
		foreach(UGUICharacterRaceSlot raceSlot in races) {
			raceSlot.RaceSelected(race);
		}
		UpdateRaceDetails();
		aspect = classes[0].classData;
		foreach(UGUICharacterClassSlot classSlot in classes) {
			classSlot.ClassSelected(aspect);
		}
		
		
		if (character != null)
			Destroy (character);
		characterName = "";
		
		int randomResult = UnityEngine.Random.Range (0, 2);
		if (randomResult == 0) {
			gender = "Male";
		} else {
			gender = "Female";
		}
		// Do this after gender so the icons can be updated 
		UpdateClassDetails();
		
		ResetModel();
		
		/*if (classes.Count > 0)
			aspect = classes [0];
		SetCharacter ();*/
		loginState = LoginState.CharacterCreate;
		creationState = CreationState.Body;
	}
	
	void ZoomCameraIn() {
		zoomingIn = true;
		zoomingOut = false;
	}
	
	void ZoomCameraOut() {
		zoomingOut = true;
		zoomingIn = false;
	}
	
	public void ToggleAnim() {
		Animator anim = character.GetComponentInChildren<Animator>();
		if (anim.speed == 0)
			anim.speed = 1;
		else
			anim.speed = 0;
	}
	
	public void SetCharacterName (string characterName)
	{
		this.characterName = characterName;
	}

	public void CreateCharacterWithName(string characterName) 
	{
		this.characterName = characterName;
		CreateCharacter (); 
	}
	
	/// <summary>
	/// Sets the characters race resulting in a new UMA model being generated.
	/// </summary>
	/// <param name="race">Race.</param>
	public virtual void SetCharacterRace(AtavismRaceData race) {
		this.race = race;
		ResetModel();
		foreach(UGUICharacterRaceSlot raceSlot in races) {
			raceSlot.RaceSelected(race);
		}
		UpdateRaceDetails();
	}
	
	protected void UpdateRaceDetails() {
		if (raceIcon != null)
			raceIcon.sprite = race.raceIcon;
		if (raceTitle != null)
			raceTitle.text = race.raceName;
		if (raceDescription != null)
			raceDescription.text = race.description;
	}
	
	public virtual void SetCharacterClass(AtavismClassData classData) {
		this.aspect = classData;
		foreach(UGUICharacterClassSlot classSlot in classes) {
			classSlot.ClassSelected(aspect);
		}
		UpdateClassDetails();
	}
	
	protected void UpdateClassDetails() {
		if (classIcon != null) {
			if (gender == "Male")
				classIcon.sprite = aspect.maleClassIcon;
			else if (gender == "Female") 
				classIcon.sprite = aspect.femaleClassIcon;
		}
		if (classTitle != null)
			classTitle.text = aspect.className;
		if (classDescription != null)
			classDescription.text = aspect.description;
			
		foreach(UGUICharacterClassSlot classSlot in classes) {
			classSlot.GenderChanged(gender);
		}
	}
	
	/// <summary>
	/// Sends the Create Character message to the server with a collection of properties
	/// to save to the new character.
	/// </summary>
	public virtual void CreateCharacter ()
	{
		if (characterName == "")
			return;
		Dictionary<string, object> properties = new Dictionary<string, object> ();
		properties.Add ("characterName", characterName);
		if (gender == "Male") {
			properties.Add ("prefab", race.maleCharacterPrefab.name);
		} else if (gender == "Female") {
			properties.Add ("prefab", race.femaleCharacterPrefab.name);
		}
		properties.Add ("race", race.raceName);
		properties.Add ("aspect", aspect.className);
		properties.Add ("gender", gender);
		if (PortraitManager.Instance.portraitType == PortraitType.Class) {
			Sprite portraitSprite = PortraitManager.Instance.GetCharacterSelectionPortrait(gender, race.raceName, aspect.className, PortraitType.Class);
			properties.Add ("custom:portrait", portraitSprite.name);
		}
		
		/*foreach (AtavismUMADnaProperty dnaProperty in character.GetComponent<UMAMobAppearance>().dnaProperties) {
			if (dnaProperty.propertyType == AtavismUmaDnaPropertyType.Colour) {
				properties.Add("custom:umaData:" + dnaProperty.name, dnaProperty.colour.r + "," + dnaProperty.colour.g 
				               + "," + dnaProperty.colour.b + "," + dnaProperty.colour.a);
			} else {
				properties.Add("custom:umaData:" + dnaProperty.name, dnaProperty.Value);
			}
		}*/
			
		dialogMessage = "Please wait...";
		errorMessage = "";
		characterSelected = AtavismClient.Instance.NetworkHelper.CreateCharacter (properties);
		if (characterSelected == null) {
			errorMessage = "Unknown Error";
		} else {
			if (!characterSelected.Status) {
				if (characterSelected.ContainsKey ("errorMessage")) {
					errorMessage = (string)characterSelected ["errorMessage"];
				}
			}
		}
		dialogMessage = "";
		if (errorMessage == "") {
			StartCharacterSelection();
			//nameUI.text = characterName;
			// Have to rename all the properties. This seems kind of pointless.
			Dictionary<string, object> newProps = new Dictionary<string, object>();
			foreach (string prop in properties.Keys) {
				if (prop.Contains(":")) {
					string[] newPropParts = prop.Split(':');
					string newProp = "uma" + newPropParts[2];
					newProps.Add(newProp, properties[prop]);
				}
			}
			foreach (string prop in newProps.Keys) {
				if (!characterSelected.ContainsKey(prop))
					characterSelected.Add(prop, newProps[prop]);
			}
		} else {
			ShowDialog(errorMessage, true);
		}
	}

	/*public void SetRace(string race, string gender) 
	{
		SetRace (race + gender);
		this.gender = gender;
	}*/

	/// <summary>
	/// Cancels character creation and returns back to the selection screen
	/// </summary>
	public virtual void CancelCharacterCreation ()
	{	
		Destroy (character);
		if (characterSelected != null) {
			race = GetRaceDataByName((string)characterSelected ["race"]);
			gender = (string)characterSelected ["gender"];
			CharacterSelected(characterSelected);
		}
		//ShowSelectionUI ();
		StartCharacterSelection();
	}

	void ShowSelectionUI() {
		loginState = LoginState.CharacterSelect;
		foreach (GameObject ui in selectUI) {
			ui.SetActive(true);
		}
		foreach(GameObject ui in createUI) {
			ui.SetActive(false);
		}
		if (serverNameText != null) {
			serverNameText.text = AtavismClient.Instance.WorldId;
		}
	}
	
	void ShowCreationUI() {
		foreach (GameObject ui in selectUI) {
			ui.SetActive(false);
		}
		foreach(GameObject ui in createUI) {
			ui.SetActive(true);
		}
	}

	protected void ShowDialog(string message, bool showButton) {
		if (dialogWindow == null)
			return;
		dialogWindow.gameObject.SetActive(true);
		dialogWindow.ShowDialogPopup(message, showButton);
	}
	
	public virtual void SetGenderMale() {
		if (gender == "Male")
			return;
		gender = "Male";
		UpdateClassDetails();
		ResetModel();
	}
	
	public virtual void SetGenderFemale() {
		if (gender == "Female")
			return;
		gender = "Female";
		UpdateClassDetails();
		ResetModel();
	}
	
	void ResetModel() {
		if (character != null)
			Destroy (character);
		if (gender == "Male") {
			character = (GameObject) Instantiate(race.maleCharacterPrefab, spawnPosition.position, spawnPosition.rotation);
		} else if (gender == "Female") {
			character = (GameObject) Instantiate(race.femaleCharacterPrefab, spawnPosition.position, spawnPosition.rotation);
		}
		x = 180;
	}
	
	public void DialogYesClicked() {
		DeleteCharacterConfirmed();
		dialogWindow.gameObject.SetActive(false);
	}
	
	public void DialogNoClicked() {
		dialogWindow.gameObject.SetActive(false);
	}
	
	#endregion Character Creation
	
	public AtavismRaceData GetRaceDataByName(string raceName) {
		foreach(UGUICharacterRaceSlot raceSlot in races) {
			if (raceSlot.raceData.raceName == raceName) {
				return raceSlot.raceData;
			}
		}
		return null;
	}
	
	public AtavismClassData GetClassDataByName(string className) {
		foreach(UGUICharacterClassSlot classSlot in classes) {
			if (classSlot.classData.className == className) {
				return classSlot.classData;
			}
		}
		return null;
	}
	
	public void ChangeScene(string sceneName) {
		Application.LoadLevel(sceneName);
	}
	
	public void Quit() {
		Application.Quit();
	}
	
	public static CharacterSelectionCreationManager Instance {
		get {
			return instance;
		}
	}

	public string DialogMessage {
		get {
			return dialogMessage;
		}
		set {
			dialogMessage = value;
		}
	}

	public string ErrorMessage {
		get {
			return errorMessage;
		}
		set {
			errorMessage = value;
		}
	}

	public LoginState State {
		get {
			return loginState;
		}
	}
	
	public GameObject Character {
		get {
			return character;
		}
	}
}
