﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClaimObject : MonoBehaviour {

	int id;
	int templateID;
	int claimID;
	public Texture2D cursorIcon;
	public Color selectedColor = Color.cyan;
	public List<GameObject> coordEffects;
	string currentState = "";
	int health;
	int maxHealth;
	bool complete;
	Dictionary<int, int> itemReqs = new Dictionary<int, int>();
	bool ready = false;
	Color initialColor;
	bool active = false;
	Renderer[] renderers;
	Color[] initialColors;
	bool mouseOver = false;
	
	// Use this for initialization
	void Start () {
		if (GetComponent<Renderer>() != null) {
			initialColor = GetComponent<Renderer>().material.color;
		} else {
			renderers = GetComponentsInChildren<Renderer>();
			initialColors = new Color[renderers.Length];
			for (int i = 0; i < renderers.Length; i++) {
				initialColors[i] = renderers[i].material.color;
			}
		}
		// Add child component to all children with colliders
		foreach(Collider child in GetComponentsInChildren<Collider>()) {
			if (child.gameObject != gameObject)
				child.gameObject.AddComponent<ClaimObjectChild>();
		}
		
		gameObject.AddComponent<AtavismNode>();
		ready = true;
	}
	
	void OnMouseOver()
	{
		mouseOver = true;
		AtavismCursor.Instance.SetMouseOverObject(GetComponent<AtavismNode>(), cursorIcon, 4);
		if (active) {
			Highlight();
		}
	}
	
	void OnMouseExit()
	{
		AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
		if (WorldBuilder.Instance.SelectedObject != this)
			ResetHighlight();
		mouseOver = false;
	}
	
	void OnMouseDown ()
	{
		if (AtavismCursor.Instance.IsMouseOverUI())
			return;
		WorldBuilder.Instance.SelectedObject = this;
		if (AtavismCursor.Instance.CursorHasItem()) {
			WorldBuilder.Instance.ImproveBuildObject(gameObject, 
			    AtavismCursor.Instance.GetCursorItem(), 1);
			return;
		}
		int nextPos = 0;
		foreach (GameObject coordEffect in coordEffects) {
			nextPos++;
			if (coordEffect.name == currentState || currentState == "") {
				if (nextPos == coordEffects.Count) {
					nextPos = 0;
				}
				//currentState = coordEffects[nextPos].name;
				Dictionary<string, object> props = new Dictionary<string, object>();
				props.Add("action", "state");
				props.Add("claimID", claimID);
				props.Add("objectID", id);
				props.Add("state", coordEffects[nextPos].name);
				NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.EDIT_CLAIM_OBJECT", props);
				return;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (mouseOver && ControlFreak2.CF2Input.GetMouseButtonDown(1)) {
			WorldBuilder.Instance.SelectedObject = this;
			if (AtavismCursor.Instance.CursorHasItem()) {
				WorldBuilder.Instance.ImproveBuildObject(gameObject, 
				        AtavismCursor.Instance.GetCursorItem(), 1);
			}
		}
	}
	
	public void StateUpdated (string state) {
		if (state == null || state == "null" || state == currentState)
			return;
		currentState = state;
		Dictionary<string, object> props = new Dictionary<string, object>();
		props["gameObject"] = gameObject;
		CoordinatedEffectSystem.ExecuteCoordinatedEffect(currentState, props);
	}
	
	public void Highlight() {
		if (!ready)
			Start ();
		if (GetComponent<Renderer>() != null) {
			GetComponent<Renderer>().material.color = selectedColor;
		} else {
			for (int i = 0; i < renderers.Length; i++) {
				renderers[i].material.color = selectedColor;
			}
		}
	}
	
	public void ResetHighlight() {
		if (GetComponent<Renderer>() != null) {
			GetComponent<Renderer>().material.color = initialColor;
		} else {
			for (int i = 0; i < renderers.Length; i++) {
				renderers[i].material.color = initialColors[i];
			}
		}
	}
	
	public int ID {
		set {
			id = value;
		}
		get {
			return id;
		}
	}
	
	public int TemplateID {
		set {
			templateID = value;
		}
		get {
			return templateID;
		}
	}
	
	public int ClaimID {
		set {
			claimID = value;
		}
		get {
			return claimID;
		}
	}
	
	public bool Active
	{
		set {
			active = value;
			if (!active && WorldBuilder.Instance.SelectedObject != this) {
				if (GetComponent<Renderer>() != null) 
					ResetHighlight();
			}
		}
		get {
			return active;
		}
	}
	
	public int Health {
		get {
			return health;
		}
		set {
			health = value;
		}
	}
	
	public int MaxHealth {
		get {
			return maxHealth;
		}
		set {
			maxHealth = value;
		}
	}
	
	public bool Complete {
		get {
			return complete;
		}
		set {
			complete = value;
		}
	}
	
	public Dictionary<int, int> ItemReqs {
		set {
			itemReqs = value;
		}
		get {
			return itemReqs;
		}
	}
}
