﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIActionBarSlot : UGUIDraggableSlot {

	Button button;
	AtavismAction action;
	bool mouseEntered = false;
	public KeyCode activateKey;
	float cooldownExpiration = -1;

	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.Reference;
	}
	
	// Update is called once per frame
	void Update () {
		if (ControlFreak2.CF2Input.GetKeyDown(activateKey) && !ClientAPI.UIHasFocus()) {
			Activate();
		}
	}
	
	public void UpdateActionData(AtavismAction action) {
		this.action = action;
		if (action == null || action.actionObject == null) {
			if (uguiActivatable != null) {
				DestroyImmediate (uguiActivatable.gameObject);
			}
		} else {
			if (uguiActivatable == null) {
				if (action.actionType == ActionType.Ability) {
					uguiActivatable = (UGUIAtavismActivatable) Instantiate(Abilities.Instance.uguiAtavismAbilityPrefab);
				} else {
					uguiActivatable = (UGUIAtavismActivatable) Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				}
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			} else {
				//TODO: something to update the count text?
			}
			uguiActivatable.SetActivatable(action.actionObject, ActivatableType.Action, this);
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		UGUIAtavismActivatable droppedActivatable = eventData.pointerDrag.GetComponent<UGUIAtavismActivatable>();
		
		// Reject any temporaries or bag slots
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Temporary || droppedActivatable.Link != null
			|| droppedActivatable.ActivatableType == ActivatableType.Bag) {
			return;
		}
		
		if (uguiActivatable != null && uguiActivatable != droppedActivatable) {
			// Delete existing child
			DestroyImmediate(uguiActivatable.gameObject);
		} else if (uguiActivatable == droppedActivatable) {
			droppedOnSelf = true;
		}
	
		// If the source was a reference slot, clear it
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference) {
			droppedActivatable.Source.ClearChildSlot();
			uguiActivatable = droppedActivatable;
			
			uguiActivatable.transform.SetParent(transform, false);
			uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
		}
		
		droppedActivatable.SetDropTarget(this);
		ClientAPI.ScriptObject.GetComponent<Actions>().SetAction(0, slotNum, droppedActivatable.ActivatableObject);
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
		action = null;
		ClientAPI.ScriptObject.GetComponent<Actions>().SetAction(0, slotNum, null);
	}
	
	public override void Discarded() {
		if (droppedOnSelf) {
			droppedOnSelf = false;
			return;
		}
		DestroyImmediate(uguiActivatable.gameObject);
		ClearChildSlot();
	}

	public override void Activate() {
		if (action.actionObject is AtavismInventoryItem) {
			AtavismInventoryItem item = (AtavismInventoryItem) action.actionObject;
			if (item.ItemId == null) {
				AtavismInventoryItem matchingItem = Inventory.Instance.GetInventoryItem(item.templateId);
				if (matchingItem == null)
					return;
				action.actionObject = matchingItem;
			}
		}
		if (action != null)
			action.Activate();
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && action != null && action.actionObject != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
