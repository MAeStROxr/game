﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UGUILootList : UIList<UGUILootListEntry> {

	public UGUIPanelTitleBar titleBar;

	void Awake() {
		Hide ();
		AtavismEventSystem.RegisterEvent("LOOT_UPDATE", this);
		AtavismEventSystem.RegisterEvent("CLOSE_LOOT_WINDOW", this);
	}
	
	void Start() {
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("LOOT_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("CLOSE_LOOT_WINDOW", this);
	}
	
	void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		transform.position = ControlFreak2.CF2Input.mousePosition;
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "LOOT_UPDATE") {
			if (Inventory.Instance.Loot.Count > 0) {
				Show();
			} else {
				GetComponent<CanvasGroup>().alpha = 0f;
				GetComponent<CanvasGroup>().blocksRaycasts = false;
			}
		} else if (eData.eventType == "CLOSE_LOOT_WINDOW") {
			Hide();
		}
	}
	
	public void LootAll() {
		NetworkAPI.SendTargetedCommand(Inventory.Instance.LootTarget.ToLong(), "/lootAll");
	}

	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int numCells = Inventory.Instance.Loot.Count;
		return numCells;
	}
	
	public override void UpdateCell (int index, UGUILootListEntry cell)
	{
		cell.SetLootEntryDetails(Inventory.Instance.Loot[index]);
	}
	
	#endregion
}
