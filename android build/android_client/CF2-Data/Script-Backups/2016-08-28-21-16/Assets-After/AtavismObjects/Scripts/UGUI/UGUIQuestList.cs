﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIQuestList : UIList<UGUIQuestListEntry> {

	public UGUIPanelTitleBar titleBar;
	public RectTransform questDetailsPanel;
	public Text questTitle;
	public Text questObjective;
	public List<Text> objectiveTexts;
	public Text questDescription;
	public Text rewardTitle;
	public RectTransform rewardPanel;
	public List<UGUIItemDisplay> itemRewards;
	public Text chooseTitle;
	public RectTransform choosePanel;
	public List<UGUIItemDisplay> chooseRewards;
	public List<UGUICurrency> currency1;
	public List<UGUICurrency> currency2;
	public Button abandonButton;
	public KeyCode toggleKey;
	bool showing = false;

	void Awake() {
		AtavismEventSystem.RegisterEvent("QUEST_LOG_UPDATE", this);
		
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		Hide ();
	}
	
	void Start() {
		// Delete the old list
		ClearAllCells();
		Refresh();
		
		SetQuestDetails();
	}
	
	void OnEnable() {
		// Delete the old list
		ClearAllCells();
		Refresh();
		
		SetQuestDetails();
	}
	
	void OnDestroy()
	{
		AtavismEventSystem.UnregisterEvent("QUEST_LOG_UPDATE", this);
	}
	
	public void Show() {
		showing = true;
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	public void Hide() {
		showing = false;
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	
	void Update() {
		if (ControlFreak2.CF2Input.GetKeyDown(toggleKey) && !ClientAPI.UIHasFocus()) {
			if (showing)
				Hide ();
			else
				Show ();
		}
	}
	
	public void Toggle() {
		if (showing)
			Hide ();
		else
			Show ();
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "QUEST_LOG_UPDATE") {
			// Delete the old list
			ClearAllCells();
			Refresh();
			
			SetQuestDetails();
		}
	}
	
	public void SetQuestDetails() {
		QuestLogEntry selectedQuest = Quests.Instance.GetSelectedQuestLogEntry();
		if (selectedQuest == null) {
			if (questDetailsPanel.GetComponent<CanvasGroup>() != null) {
				questDetailsPanel.GetComponent<CanvasGroup>().alpha = 0f;
			} else {
				questDetailsPanel.gameObject.SetActive(false);
			}
			abandonButton.interactable = false;
			return;
		}
		abandonButton.interactable = true;
		
		if (questDetailsPanel.GetComponent<CanvasGroup>() != null) {
			questDetailsPanel.GetComponent<CanvasGroup>().alpha = 1f;
		} else {
			questDetailsPanel.gameObject.SetActive(true);
		}
		
		if (questTitle != null) {
			questTitle.text = selectedQuest.Title;
		}
		
		questObjective.text = selectedQuest.Objective;
		for (int i = 0; i < objectiveTexts.Count; i++) {
			if (i < selectedQuest.gradeInfo[0].objectives.Count) {
				objectiveTexts[i].gameObject.SetActive(true);
				objectiveTexts[i].text = selectedQuest.gradeInfo[0].objectives[i];
			} else {
				objectiveTexts[i].gameObject.SetActive(false);
			}
		}
		
		questDescription.text = selectedQuest.Description;
		
		// Item Rewards
		for (int i = 0; i < itemRewards.Count; i++) {
			if (i < selectedQuest.gradeInfo[0].rewardItems.Count) {
				itemRewards[i].gameObject.SetActive(true);
				itemRewards[i].SetItemData(selectedQuest.gradeInfo[0].rewardItems[i].item, null);
			} else {
				itemRewards[i].gameObject.SetActive(false);
			}
		}
		if (rewardPanel != null) {
			if (selectedQuest.gradeInfo[0].rewardItems.Count == 0) 
				rewardPanel.gameObject.SetActive(false);
			else
				rewardPanel.gameObject.SetActive(true);
		}
		if (rewardTitle != null) {
			if (selectedQuest.gradeInfo[0].rewardItems.Count == 0 && selectedQuest.gradeInfo[0].currencies.Count == 0) 
				rewardTitle.gameObject.SetActive(false);
			else
				rewardTitle.gameObject.SetActive(true);
		}
		
		// Item Choose Rewards
		for (int i = 0; i < chooseRewards.Count; i++) {
			if (i < selectedQuest.gradeInfo[0].RewardItemsToChoose.Count) {
				chooseRewards[i].gameObject.SetActive(true);
				chooseRewards[i].SetItemData(selectedQuest.gradeInfo[0].RewardItemsToChoose[i].item, null);
			} else {
				chooseRewards[i].gameObject.SetActive(false);
			}
		}
		if (choosePanel != null) {
			if (selectedQuest.gradeInfo[0].RewardItemsToChoose.Count == 0) 
				choosePanel.gameObject.SetActive(false);
			else
				choosePanel.gameObject.SetActive(true);
		}
		if (chooseTitle != null) {
			if (selectedQuest.gradeInfo[0].RewardItemsToChoose.Count == 0) 
				chooseTitle.gameObject.SetActive(false);
			else
				chooseTitle.gameObject.SetActive(true);
		}
		
		// Currency Rewards
		if (selectedQuest.gradeInfo[0].currencies.Count > 0) {
			List<CurrencyDisplay> currencyDisplayList = Inventory.Instance.GenerateCurrencyListFromAmount(selectedQuest.gradeInfo[0].currencies[0].id, 
			                                                                                              selectedQuest.gradeInfo[0].currencies[0].count);
			for (int i = 0; i < currency1.Count; i++) {
				if (i < currencyDisplayList.Count) {
					currency1[i].gameObject.SetActive(true);
					currency1[i].SetCurrencyDisplayData(currencyDisplayList[i]);
				} else {
					currency1[i].gameObject.SetActive(false);
				}
			}
		} else {
			for (int i = 0; i < currency1.Count; i++) {
				currency1[i].gameObject.SetActive(false);
			}
		}
		if (selectedQuest.gradeInfo[0].currencies.Count > 1) {
			List<CurrencyDisplay> currencyDisplayList = Inventory.Instance.GenerateCurrencyListFromAmount(selectedQuest.gradeInfo[0].currencies[1].id, 
			                                                                                              selectedQuest.gradeInfo[0].currencies[1].count);
			for (int i = 0; i < currency2.Count; i++) {
				if (i < currencyDisplayList.Count) {
					currency2[i].gameObject.SetActive(true);
					currency2[i].SetCurrencyDisplayData(currencyDisplayList[i]);
				} else {
					currency2[i].gameObject.SetActive(false);
				}
			}
		} else {
			for (int i = 0; i < currency2.Count; i++) {
				currency2[i].gameObject.SetActive(false);
			}
		}
	}
	
	public void AbandonQuest() {
		Quests.Instance.AbandonQuest();
	}

	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int numCells = Quests.Instance.QuestLogEntries.Count;
		return numCells;
	}
	
	public override void UpdateCell (int index, UGUIQuestListEntry cell)
	{
		cell.SetQuestEntryDetails(Quests.Instance.QuestLogEntries[index], index, this);
	}
	
	#endregion
}
