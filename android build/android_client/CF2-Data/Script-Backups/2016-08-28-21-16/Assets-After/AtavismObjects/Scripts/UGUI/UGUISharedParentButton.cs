﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using System.Collections;

public class UGUISharedParentButton : MonoBehaviour {

	public GameObject targetFrame;
	public KeyCode toggleKey;

	// Use this for initialization
	void Start () {
	
	}
	
	void Update() {
		if (ControlFreak2.CF2Input.GetKeyDown(toggleKey) && !ClientAPI.UIHasFocus()) {
			if (targetFrame.activeSelf) {
				targetFrame.SetActive(false);
			} else {
				targetFrame.SetActive(true);
			}
		}
	}
	
	public void ShowFrame() {
		targetFrame.SetActive(true);
	}
}
