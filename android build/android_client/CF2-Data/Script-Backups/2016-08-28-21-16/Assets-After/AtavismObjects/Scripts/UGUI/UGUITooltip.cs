﻿// Code auto-converted by Control Freak 2 on Sunday, August 28, 2016!
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AttributeInfo
{
	public string value;
	public string text;
	public Color textColour;
	public bool singleColumnRow;
	public RectOffset margin;
}

public class UGUITooltip : MonoBehaviour {
	
	static UGUITooltip instance;
	
	public Text title;
	public Text type;
	public Text description;
	public Color attributeTextColour = Color.white;
	public GameObject attributeRow;
	public List<Color> itemGradeColors;
	public Color itemTypeColour = Color.white;
	public Color itemStatColour = Color.green;
	public Color abilityRangeColour = Color.white;
	public Color abilityCostColour = Color.white;
	public Color abilityCastTimeColour = Color.white;
	
	CanvasGroup canvasGroup = null;
	Color defaultTitleColor;
	List<AttributeInfo> attributes = new List<AttributeInfo>();
	List<GameObject> attributesRows = new List<GameObject>();
	GameObject target;
	bool showing = false;
	
	void Awake() {
		instance = this;
	}
	
	// Use this for initialization
	void Start () {
		canvasGroup = GetComponent<CanvasGroup>();
		defaultTitleColor = title.color;
		Hide ();
	}
	
	void OnDestroy()
	{
		instance = null;
	}
	
	void Update() {
		if (!showing)
			return;
			
		if (this.target == null || !this.target.activeInHierarchy
				|| target.GetComponent<CanvasRenderer>().GetAlpha() == 0) {
			Hide ();
			return;
		}
		
		// Reposition the tooltip if it is showing
		/*RectTransform rect = GetComponent<RectTransform>();
		rect.pivot = GetComponent<RectTransform>().pivot;
		Vector2 position = rect.anchoredPosition;
		if (rect.anchoredPosition.y + rect.sizeDelta.y > Screen.height) {
			position.y = Screen.height - rect.sizeDelta.y;
		}
		if (rect.anchoredPosition.x + rect.sizeDelta.x > Screen.width) {
			position.x = Screen.width - rect.sizeDelta.x;
		}
		
		rect.anchoredPosition = position;*/
		// Save the position where the tooltip should appear
		Vector3 tooltipPosition = ControlFreak2.CF2Input.mousePosition;
		// Add a button width/height to the tooltip position
		tooltipPosition += new Vector3(5, 5, 0);
		// Check if the position will be out of the screen
		if (tooltipPosition.x + GetComponent<RectTransform>().sizeDelta.x > Screen.width) {
			tooltipPosition.x = Screen.width - GetComponent<RectTransform>().sizeDelta.x;
		}
		if (tooltipPosition.y + GetComponent<RectTransform>().sizeDelta.y > Screen.height) {
			tooltipPosition.y = Screen.height - GetComponent<RectTransform>().sizeDelta.y;
		}
		transform.position = tooltipPosition;
	}
	
	public void SetTitle(string titleText)
	{
		title.text = titleText;
		title.color = defaultTitleColor;
	}
	
	public void SetTitleColour(Color color) {
		title.color = color;
	}
	
	public void SetType(string typeText)
	{
		type.text = typeText;
	}
	
	public void SetTypeColour(Color color) {
		type.color = color;
	}
	
	public void SetDescription(string descriptionText)
	{
		description.text = descriptionText;
		if (string.IsNullOrEmpty(descriptionText)) {
			description.enabled = false;
		} else {
			description.enabled = true;
		}
	}
	
	public void AddAttribute(string value, string text, bool singleColumn)
	{
		// Create new attribute info
		AttributeInfo info = new AttributeInfo();
		info.value = value;
		info.text = text;
		info.singleColumnRow = singleColumn;
		info.margin = new RectOffset();
		
		// Add it to the attribute list
		instance.attributes.Add(info);
	}
	
	public void AddAttribute(string value, string text, bool singleColumn, Color colour)
	{
		// Create new attribute info
		AttributeInfo info = new AttributeInfo();
		string colourText = string.Format("#{0:X2}{1:X2}{2:X2}ff", ToByte(colour.r), ToByte(colour.g), ToByte(colour.b));
		info.value = "<color=" + colourText + ">" + value;
		info.text = text + "</color>";
		info.singleColumnRow = singleColumn;
		info.margin = new RectOffset();
		
		// Add it to the attribute list
		instance.attributes.Add(info);
	}
	
	private static byte ToByte(float f)
	{
		f = Mathf.Clamp01(f);
		return (byte)(f * 255);
	}
	
	public void Show(GameObject target)
	{
		canvasGroup.alpha = 1f;
		
		// Cleanup any attributes left, if any at all
		Cleanup();
		
		// Save the position where the tooltip should appear
		Vector3 tooltipPosition = ControlFreak2.CF2Input.mousePosition;
		// Add a button width/height to the tooltip position
		tooltipPosition += new Vector3(5, 5, 0);
		// Check if the position will be out of the screen
		if (tooltipPosition.x + GetComponent<RectTransform>().sizeDelta.x > Screen.width) {
			tooltipPosition.x = Screen.width - GetComponent<RectTransform>().sizeDelta.x;
		}
		if (tooltipPosition.y + GetComponent<RectTransform>().sizeDelta.y > Screen.height) {
			tooltipPosition.y = Screen.height - GetComponent<RectTransform>().sizeDelta.y;
		}
		transform.position = tooltipPosition;
		
		// Prepare the attributes
		if (attributes.Count > 0 && attributeRow != null)
		{
			bool isLeft = true;
			UGUITooltipAttributeRow lastRow = null;
			int lastDepth = title.depth;
			
			// Loop the attributes
			foreach (AttributeInfo info in attributes)
			{
				// Force left column in case it's a single column row
				if (info.singleColumnRow)
					isLeft = true;
				
				if (isLeft)
				{
					// Instantiate a prefab
					GameObject obj = (GameObject)Instantiate(this.attributeRow);
					
					// Apply parent
					obj.transform.SetParent(transform, false);
					
					// Fix position and scale
					obj.transform.localScale = Vector3.one;
					obj.transform.localPosition = Vector3.zero;
					obj.transform.localRotation = Quaternion.identity;
					
					// Increase the depth
					lastDepth = lastDepth + 1;
					
					// Get the attribute row script referrence
					lastRow = obj.GetComponent<UGUITooltipAttributeRow>();
					
					// Make some changes if it's a single column row
					if (info.singleColumnRow)
					{
						// Destroy the right column
						Destroy(lastRow.rightText.gameObject);
					}
					
					// Add it to the instanced objects list
					attributesRows.Add(obj);
				}
				
				// Check if we have a row object to work with
				if (lastRow != null)
				{
					Text text = (isLeft) ? lastRow.leftText : lastRow.rightText;
					
					// Check if we have the label
					if (text != null)
					{
						// Set the label text
						text.text = info.value + info.text;
						
						// Flip is left
						if (!info.singleColumnRow)
							isLeft = !isLeft;
					}
				}
			}
			
			// Clear the attributes list, we no longer need it
			attributes.Clear();
		}
		description.transform.SetAsLastSibling();
		
		showing = true;
		this.target = target;
	}
	
	public void Hide() {
		canvasGroup.alpha = 0f;
		showing = false;
	}
	
	private void Cleanup()
	{
		//Destroy the attributes
		foreach (GameObject obj in attributesRows)
		{
			if (obj != null)
				DestroyImmediate(obj);
		}
		
		// Clear the list
		attributesRows.Clear();
	}
	
	public static UGUITooltip Instance {
		get {
			return instance;
		}
		
	}
}
