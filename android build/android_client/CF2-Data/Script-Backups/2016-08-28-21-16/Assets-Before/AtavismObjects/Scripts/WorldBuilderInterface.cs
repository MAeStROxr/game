﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MouseWheelBuildMode {
	Rotate,
	MoveVertical
}

/// <summary>
/// Interface class for World Builder. Contains the functions for placing and moving world building objects.
/// If you want to change how players make their buildings, this is the class you will want to change.
/// </summary>
public class WorldBuilderInterface : MonoBehaviour {

	static WorldBuilderInterface instance;

	GameObject currentReticle;
	private Vector3 hitPoint;
	private Vector3 hitNormal;
	private RaycastHit hit;
	
	AtavismInventoryItem itemBeingPlaced;
	AtavismBuildObjectTemplate buildTemplate;
	string buildObjectGameObject = "";
	
	public bool snap = true;
	public float snapGap = 0.5f;
	public float rotationAmount = 90;
	public MouseWheelBuildMode mouseWheelBuildMode = MouseWheelBuildMode.Rotate;
	public float verticalMovementAmount = 0.5f;
	public LayerMask collisionPlacementLayer;
	float currentVerticalOffset = 0f;
	ClaimObject hoverObject = null;
	
	// Use this for initialization
	void Start () {
		instance = this;
		
		// Register for 
		AtavismEventSystem.RegisterEvent("PLACE_CLAIM_OBJECT", this);
		AtavismEventSystem.RegisterEvent("CLAIM_OBJECT_CLICKED", this);
		AtavismEventSystem.RegisterEvent("START_BUILD_CLAIM_OBJECT", this);
	}
	
	// Update is called once per frame
	void Update () {
		if (AtavismCursor.Instance == null || AtavismCursor.Instance.IsMouseOverUI())
			return;
			
		if (GetBuildingState() == WorldBuildingState.SelectItem) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			// Casts the ray and get the first game object hit
			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
				ClaimObject cObject = hit.transform.gameObject.GetComponent<ClaimObject>();
				if (cObject == null)
					cObject = hit.transform.gameObject.GetComponentInChildren<ClaimObject>();
				if (cObject == null && hit.transform.parent != null)
					cObject = hit.transform.parent.GetComponent<ClaimObject>();
				if (cObject != null) {
					int objectID = cObject.ID;
					if (WorldBuilder.Instance.ActiveClaim.claimObjects.ContainsKey(objectID)) {
						if (hoverObject != cObject) {
							if (hoverObject != null) {
								hoverObject.ResetHighlight();
							}
							hoverObject = cObject;
							cObject.Highlight();
						}
						if (Input.GetMouseButtonDown(0)) {
							StopSelectObject();
							//SetBuildingState(WorldBuildingState.EditItem);
							//objectBeingEdited = activeClaim.claimObjects[objectID].gameObject;
							ClientAPI.ScriptObject.GetComponent<WorldBuilder>().RequestClaimObjectInfo(objectID);
							cObject.Highlight();
							ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject = cObject;
						}
					}
				} else if (hoverObject != null) {
					hoverObject.ResetHighlight();
					hoverObject = null;
				}
			} else if (hoverObject != null) {
				hoverObject.ResetHighlight();
				hoverObject = null;
			}
		}
		
		if ((GetBuildingState() == WorldBuildingState.PlaceItem || GetBuildingState() == WorldBuildingState.MoveItem) && currentReticle != null) {
			GetHitLocation();
			float delta = Input.GetAxis ("Mouse ScrollWheel");
			if (mouseWheelBuildMode == MouseWheelBuildMode.Rotate) {
				if (delta > 0)
					currentReticle.transform.Rotate (new Vector3 (0, -rotationAmount, 0));
				else if (delta < 0)
					currentReticle.transform.Rotate (new Vector3 (0, rotationAmount, 0));
			} else if (mouseWheelBuildMode == MouseWheelBuildMode.MoveVertical) {
				if (delta > 0) {
					currentVerticalOffset += verticalMovementAmount;
				} else if (delta < 0) {
					currentVerticalOffset -= verticalMovementAmount;
				}
			}
			
			if ((currentReticle != null) && (WorldBuilder.Instance.ActiveClaim != null) && 
			    (WorldBuilder.Instance.InsideClaimArea (WorldBuilder.Instance.ActiveClaim, hitPoint))) {
				if (snap) {
					float newX = Mathf.Round(hitPoint.x * (1 / snapGap)) / (1 / snapGap);
					float newZ = Mathf.Round(hitPoint.z * (1 / snapGap)) / (1 / snapGap);
					hitPoint = new Vector3(newX, hitPoint.y + currentVerticalOffset, newZ);
				}
				currentReticle.transform.position = hitPoint;
			}
			
			if (Input.GetKeyDown(KeyCode.LeftShift)) {
				if (mouseWheelBuildMode == MouseWheelBuildMode.MoveVertical) {
					mouseWheelBuildMode = MouseWheelBuildMode.Rotate;
					ClientAPI.Write("Changed to Rotate Mode");
				} else {
					mouseWheelBuildMode = MouseWheelBuildMode.MoveVertical;
					ClientAPI.Write("Changed to Move Vertical Mode");
				}
			}
			
			if (Input.GetMouseButtonDown (0) && !AtavismUiSystem.IsMouseOverFrame()) {
				if (GetBuildingState() == WorldBuildingState.PlaceItem) {
					ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SendPlaceClaimObject(buildTemplate.id, itemBeingPlaced, currentReticle.transform);
					ClientAPI.Write("Send place claim object message");
					ClearCurrentReticle(true);
					itemBeingPlaced = null;
					SetBuildingState(WorldBuildingState.Standard);
					AtavismCursor.Instance.ClearItemClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
				} else if (GetBuildingState() == WorldBuildingState.MoveItem) {
					WorldBuilder.Instance.SendEditObjectPosition(WorldBuilder.Instance.SelectedObject.gameObject);
					//SetBuildingState(WorldBuildingState.EditItem);
					SetBuildingState(WorldBuildingState.Standard);
					ClearCurrentReticle(false);
					itemBeingPlaced = null;
					//objectBeingEdited.GetComponent<ClaimObject>().ResetHighlight();
					//objectBeingEdited = null;
				}
				
			}
		}
	}
	
	bool GetHitLocation ()
	{	
		//Debug.Log("GetHitLocation");
		Ray ray = Camera.main.ScreenPointToRay (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
		bool hitSomething = Physics.Raycast (ray, out hit, 200f, collisionPlacementLayer);
		if (WorldBuilder.Instance.ActiveClaim != null && ((WorldBuilder.Instance.ActiveClaim.area == 0) ||
		        WorldBuilder.Instance.InsideClaimArea (WorldBuilder.Instance.ActiveClaim, hit.point))) {
			hitNormal = hit.normal;
			hitPoint = hit.point;
			return hitSomething;
		} else {
			return false;
		}
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "PLACE_CLAIM_OBJECT") {
			//if (GetBuildingState() != WorldBuildingState.PlaceItem)
			//	return;
			
			OID itemOID = OID.fromString(eData.eventArgs[0]);
			itemBeingPlaced = Inventory.Instance.GetInventoryItem(itemOID);
			// Does the item have a ClaimObject effect?
			List<int> effectPositions = itemBeingPlaced.GetEffectPositionsOfTypes("ClaimObject");
			if (effectPositions.Count == 0) {
				itemBeingPlaced = null;
				return;
			} else {
				SetBuildingState(WorldBuildingState.PlaceItem);
				int buildObjectID = int.Parse(itemBeingPlaced.itemEffectValues[effectPositions[0]]);
				buildTemplate = ClientAPI.ScriptObject.GetComponent<WorldBuilder>().GetBuildObjectTemplate(buildObjectID); 
				string prefabName = buildTemplate.gameObject.Remove(0, 17);
				prefabName = prefabName.Remove(prefabName.Length - 7);
				// Create an instance of the game Object
				GameObject prefab = (GameObject)Resources.Load(prefabName);
				GetHitLocation();
				SetCurrentReticle((GameObject)UnityEngine.Object.Instantiate(prefab, hitPoint, Quaternion.identity));
			}
		} else if (eData.eventType == "START_BUILD_CLAIM_OBJECT") {
			
			SetBuildingState(WorldBuildingState.PlaceItem);
			itemBeingPlaced = null;
			int buildTemplateID = int.Parse(eData.eventArgs[0]);
			StartPlaceBuildObject(buildTemplateID);
		} else if (eData.eventType == "CLAIM_OBJECT_CLICKED") {
			int objectID = int.Parse(eData.eventArgs[0]);
			if (GetBuildingState() == WorldBuildingState.SelectItem && WorldBuilder.Instance.ActiveClaim.claimObjects.ContainsKey(objectID)) {
				SetCurrentReticle(WorldBuilder.Instance.ActiveClaim.claimObjects[objectID].gameObject);
			} 
		}
	}
	
	public void StartPlaceBuildObject(int buildTemplateID) {
		buildTemplate = ClientAPI.ScriptObject.GetComponent<WorldBuilder>().GetBuildObjectTemplate(buildTemplateID);
		if (buildTemplate == null)
			return;
		string prefabName = buildTemplate.gameObject;
		prefabName = prefabName.Remove(0, 17);
		prefabName = prefabName.Remove(prefabName.Length - 7);
		// Create an instance of the game Object
		GameObject prefab = (GameObject)Resources.Load(prefabName);
		SetBuildingState(WorldBuildingState.PlaceItem);
		GetHitLocation();
		SetCurrentReticle((GameObject)UnityEngine.Object.Instantiate(prefab, hitPoint, Quaternion.identity));
	}
	
	public void SetCurrentReticle(GameObject obj) {
		currentReticle = obj;
		Collider[] colliders = currentReticle.GetComponents<Collider> ();
		foreach (Collider col in colliders)
			col.enabled = false;
		colliders = currentReticle.GetComponentsInChildren<Collider> ();
		foreach (Collider col in colliders)
			col.enabled = false;
		// Disable mouse wheel scroll
		ClientAPI.GetInputController().MouseWheelDisabled = true;
	}
	
	public void ClearCurrentReticle(bool destroyObj) {
		if (destroyObj) {
			DestroyImmediate(currentReticle);
		} else if (currentReticle != null) {
			Collider[] colliders = currentReticle.GetComponents<Collider> ();
			foreach (Collider col in colliders)
				col.enabled = true;
			colliders = currentReticle.GetComponentsInChildren<Collider> ();
			foreach (Collider col in colliders)
				col.enabled = true;
		}
		currentReticle = null;
		ClientAPI.GetInputController().MouseWheelDisabled = false;
	}
	
	public void StartSelectObject() {
		if (WorldBuilder.Instance.ActiveClaim != null && WorldBuilder.Instance.ActiveClaim.playerOwned){
			SetBuildingState(WorldBuildingState.SelectItem);
			foreach (ClaimObject cObject in WorldBuilder.Instance.ActiveClaim.claimObjects.Values) {
				cObject.Active = true;
				cObject.ResetHighlight();
			}
		}
	}
	
	void StopSelectObject() {
		foreach (ClaimObject cObject in WorldBuilder.Instance.ActiveClaim.claimObjects.Values) {
			cObject.Active = false;
		}
	}
	
	/// <summary>
	/// Gets the BuildingState from the World Builder
	/// </summary>
	/// <returns>The building state.</returns>
	WorldBuildingState GetBuildingState() {
		return WorldBuilder.Instance.BuildingState;
	}
	
	/// <summary>
	/// Tells the WorldBuilder to update the BuildingState
	/// </summary>
	/// <param name="newState">New state.</param>
	void SetBuildingState(WorldBuildingState newState) {
		WorldBuilder.Instance.BuildingState = newState;
	}
	
	public static WorldBuilderInterface Instance {
		get {
			return instance;
		}
	}
	
	public MouseWheelBuildMode MouseWheelBuildMode {
		get {
			return mouseWheelBuildMode;
		}
		set {
			mouseWheelBuildMode = value;
		}
	}
}
