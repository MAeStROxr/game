﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TBTK;

namespace TBTK {

    [RequireComponent(typeof(EffectTracker))]
    public class AbilityManagerUnit : MonoBehaviour {
        GameControl gameControl {
            get {
                return battleData.gameControl;
            }
        }
        FactionManager factionManager {
            get {
                return battleData.factionManager;
            }

        }
        GridManager gridManager {
            get {
                return battleData.gridManager;
            }

        }
        Battle battleData;

        public delegate void IterateCooldownHandler();
        public event IterateCooldownHandler onIterateAbilityCooldownE;

        public delegate void AbilityActivatedHandler();
        public event AbilityActivatedHandler onAbilityActivatedE;           //listen by AudioManager only



        public AbilityManagerUnit instance;

        private List<UnitAbility> unitAbilityDBList = new List<UnitAbility>();

        void Awake() {
            if (instance == null) instance = this;
            battleData = gameObject.transform.root.GetComponentInChildren<Battle>();


        }

        public void Init() {
            if (instance == null) instance = this;
            battleData = gameObject.transform.root.GetComponentInChildren<Battle>();

            unitAbilityDBList = UnitAbilityDB.LoadClone(battleData);
        }


        void OnEnable() {
            gameControl.onIterateTurnE += _IterateAbilityCooldown;
        }
        void OnDisable() {
            gameControl.onIterateTurnE -= _IterateAbilityCooldown;
        }

        public void IterateAbilityCooldown() { instance._IterateAbilityCooldown(); }
        public void _IterateAbilityCooldown() {
            if (onIterateAbilityCooldownE != null) onIterateAbilityCooldownE();
        }



        void Update() {
#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
				if(Input.touchCount==2){
					if(gameControl.selectedUnit!=null && gameControl.selectedUnit.GetSelectedAbilityID()>=0){
						gameControl.selectedUnit.ClearSelectedAbility();
					}
				}
#else
            if (Input.GetMouseButtonDown(1)) {
                if (gameControl.selectedUnit != null && gameControl.selectedUnit.GetSelectedAbilityID() >= 0) {
                    gameControl.selectedUnit.ClearSelectedAbility();
                }
            }
#endif
        }



        public void AbilityActivated() {
            if (onAbilityActivatedE != null) onAbilityActivatedE();
        }


        public void PerkUnlockNewAbility(int unitID, int abID, int abXID) { if (instance != null) instance._PerkUnlockNewAbility(unitID, abID, abXID); }
        public void _PerkUnlockNewAbility(int unitID, int abID, int abXID) {
            int abIndex = -1;
            if (abID >= 0) {
                for (int i = 0; i < unitAbilityDBList.Count; i++) {
                    if (unitAbilityDBList[i].prefabID == abID) {
                        abIndex = i; break;
                    }
                }
            }

            int abXIndex = -1;
            if (abXID >= 0) {
                for (int i = 0; i < unitAbilityDBList.Count; i++) {
                    if (unitAbilityDBList[i].prefabID == abXID) {
                        abXIndex = i; break;
                    }
                }
            }

            if (abIndex == -1 && abXIndex == -1) return;

            List<Unit> unitList = factionManager.GetAllUnit();
            for (int i = 0; i < unitList.Count; i++) {
                Unit unit = unitList[i];
                if (unit.isAIUnit) continue;
                if (unit.prefabId == unitID) {
                    int replaceIndex = -1;
                    if (abXIndex >= 0) {
                        for (int n = 0; n < unit.abilityIDList.Count; n++) {
                            if (unit.abilityIDList[n] == abXIndex) {
                                if (abIndex >= 0) replaceIndex = n;
                                else {
                                    unit.abilityIDList.RemoveAt(n);
                                    unit.abilityList.RemoveAt(n);
                                }
                                break;
                            }
                        }
                    }

                    if (abIndex >= 0) {
                        UnitAbility unitAbility = unitAbilityDBList[abIndex].Clone(true, battleData);
                        unitAbility.SetUnit(unit);
                        if (replaceIndex >= 0) {
                            unit.abilityIDList[replaceIndex] = abID;
                            unit.abilityList[replaceIndex] = unitAbility;
                        }
                        else {
                            unit.abilityIDList.Add(abID);
                            unit.abilityList.Add(unitAbility);
                        }
                    }
                }
            }

            if (gameControl.selectedUnit != null) gameControl.selectedUnit.Select();    //this will call onUnitSelected on the unit which will refresh the UI
        }




   
        private void applyAbillity(Unit srcOrTarget, UnitAbility a, Tile t, AttackInstance att) {
         
        Unit u = null;
            bool sendAffectedUnits = !att.Processed;
            if (sendAffectedUnits) {
                // u = Instantiate(srcOrTarget);
                u = srcOrTarget;
               // u.gameObject.SetActive(false);
              //  u.battleData = battleData;
                switch (a.type) {
                    case UnitAbility._AbilityType.Generic:
                        u.ApplyEffect(a.Clone(false),att);
                        break;
                    case UnitAbility._AbilityType.Teleport:
                        break;
                    case UnitAbility._AbilityType.SpawnNew:
                        break;
                    case UnitAbility._AbilityType.ScanFogOfWar:
                        break;
                }
                //affectedUnits.Add(u.GetData());
               // Destroy(u.gameObject);
               
            }
            else {
                u = srcOrTarget;
                switch (a.type) {
                    case UnitAbility._AbilityType.Generic:
                        u.ApplyEffect(a.Clone(false), att);
                        // DataUnit d = affectedUnits.Find(data => data.dataId == u.GetDataID());
                        // if (d != null)
                        //     u.ModifyStatsToData(d, d.dataId);
                        break;
                    case UnitAbility._AbilityType.Teleport:
                        u.SetNewTile(t);
                        gameControl.ClearSelectedUnit();
                        gameControl.SelectUnit(u);
                        break;
                    case UnitAbility._AbilityType.SpawnNew:
                        GameObject unitObj = (GameObject)Instantiate(a.spawnUnit, t.GetPos(), u.thisT.rotation);
                        Unit unit = unitObj.GetComponent<Unit>();
                        unit.SetNewTile(t);
                        factionManager.InsertUnit(unit, u.factionID);
                        if (gridManager.GetDistance(t, u.tile) <= u.GetMoveRange()) gameControl.SelectUnit(u);
                        break;
                    case UnitAbility._AbilityType.ScanFogOfWar:
                        List<Tile> targetTileList = gridManager.GetTilesWithinDistance(t, a.GetAOERange());
                        targetTileList.Add(t);
                        for (int i = 0; i < targetTileList.Count; i++) targetTileList[i].ForceVisible(a.duration);
                        break;
                }
            }
        }

        public void ApplyAbilityEffect(Unit srcUnit, Tile targetTile, UnitAbility ability, AttackInstance att) {
            instance.StartCoroutine(instance._ApplyAbilityEffect(srcUnit, targetTile, ability, att));

        }

        IEnumerator _ApplyAbilityEffect(Unit srcUnit, Tile targetTile, UnitAbility ability, AttackInstance att) {
            if (att.ShowAttack && ability.effectObject != null) Instantiate(ability.effectObject, targetTile.GetPos(), Quaternion.identity);
            bool effectOnTarget = (ability.effectObjectOnTarget != null) && att.Processed;
            if (ability.delayDuration > 0) yield return new WaitForSeconds(ability.delayDuration);

            if (ability.type == UnitAbility._AbilityType.Generic) {

                List<Tile> tileList = new List<Tile>();
                if (ability.aoeRange > 0) tileList = gridManager.GetTilesWithinDistance(targetTile, ability.aoeRange);
                tileList.Add(targetTile);

                if (ability.targetType == _TargetType.AllUnit) {
                    for (int i = 0; i < tileList.Count; i++) {
                        if (tileList[i].unit != null) {
                            applyAbillity(tileList[i].unit, ability, targetTile, att);
                            if (effectOnTarget) Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
                        }
                    }
                }
                if (ability.targetType == _TargetType.HostileUnit) {
                    for (int i = 0; i < tileList.Count; i++) {
                        if (tileList[i].unit != null && tileList[i].unit.factionID != srcUnit.factionID) {
                            applyAbillity(tileList[i].unit, ability, targetTile, att);
                            if (effectOnTarget) Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
                        }
                    }
                }
                if (ability.targetType == _TargetType.FriendlyUnit) {
                    for (int i = 0; i < tileList.Count; i++) {
                        if (tileList[i].unit != null && tileList[i].unit.factionID == srcUnit.factionID) {
                            applyAbillity(tileList[i].unit, ability, targetTile, att);
                            if (effectOnTarget) Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
                        }
                    }
                }
                if (ability.targetType == _TargetType.AllTile) {
                    for (int i = 0; i < tileList.Count; i++) {
                        applyAbillity(tileList[i].unit, ability, targetTile, att);
                        if (effectOnTarget) Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
                    }
                }
                if (ability.targetType == _TargetType.Tile) {
                    for (int i = 0; i < tileList.Count; i++) {
                        if (tileList[i].unit == null) {
                            applyAbillity(tileList[i].unit, ability, targetTile, att);
                            if (effectOnTarget) Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
                        }
                    }
                }

            }
            else if (ability.type == UnitAbility._AbilityType.Teleport) {
                //GameControl.ClearSelectedUnit();
                //NT
                //srcUnit.SetNewTile(targetTile);
                applyAbillity(srcUnit, ability, targetTile,att);

            }
            else if (ability.type == UnitAbility._AbilityType.SpawnNew) {
                applyAbillity(srcUnit, ability, targetTile, att);
            }
            else if (ability.type == UnitAbility._AbilityType.ScanFogOfWar) {
                applyAbillity(srcUnit, ability, targetTile, att);
            }

            yield return null;
        }


        /******************
     
        public void ApplyAbilityEffect(Unit srcUnit, Tile targetTile, UnitAbility ability){
			instance.StartCoroutine(instance._ApplyAbilityEffect(srcUnit, targetTile, ability));
		}
		IEnumerator _ApplyAbilityEffect(Unit srcUnit, Tile targetTile, UnitAbility ability){
			if(ability.effectObject!=null)	Instantiate(ability.effectObject, targetTile.GetPos(), Quaternion.identity);
			
			if(ability.delayDuration>0) yield return new WaitForSeconds(ability.delayDuration);
			
			if(ability.type==UnitAbility._AbilityType.Generic){
			
				List<Tile> tileList=new List<Tile>();
				if(ability.aoeRange>0) tileList=gridManager.GetTilesWithinDistance(targetTile, ability.aoeRange);
				tileList.Add(targetTile);
				
				if(ability.targetType==_TargetType.AllUnit){
					for(int i=0; i<tileList.Count; i++){
						if(tileList[i].unit!=null){
							tileList[i].unit.ApplyEffect(ability.Clone(false));
							if(ability.effectObjectOnTarget!=null)	Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
						}
					}
				}
				if(ability.targetType==_TargetType.HostileUnit){
					for(int i=0; i<tileList.Count; i++){
						if(tileList[i].unit!=null && tileList[i].unit.factionID!=srcUnit.factionID){
							tileList[i].unit.ApplyEffect(ability.Clone(false));
							if(ability.effectObjectOnTarget!=null)	Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
						}
					}
				}
				if(ability.targetType==_TargetType.FriendlyUnit){
					for(int i=0; i<tileList.Count; i++){
						if(tileList[i].unit!=null && tileList[i].unit.factionID==srcUnit.factionID){
							tileList[i].unit.ApplyEffect(ability.Clone(false));
							if(ability.effectObjectOnTarget!=null)	Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
						}
					}
				}
				if(ability.targetType==_TargetType.AllTile){
					for(int i=0; i<tileList.Count; i++){
						tileList[i].ApplyEffect(ability.Clone(false));
						if(ability.effectObjectOnTarget!=null)	Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
					}
				}
				if(ability.targetType==_TargetType.Tile){
					for(int i=0; i<tileList.Count; i++){
						if(tileList[i].unit==null){
							tileList[i].unit.ApplyEffect(ability.Clone(false));
							if(ability.effectObjectOnTarget!=null)	Instantiate(ability.effectObjectOnTarget, tileList[i].GetPos(), Quaternion.identity);
						}
					}
				}
				
			}
			else if(ability.type==UnitAbility._AbilityType.Teleport){
				gameControl.ClearSelectedUnit();
				srcUnit.SetNewTile(targetTile);
				gameControl.SelectUnit(srcUnit);
			}
			else if(ability.type==UnitAbility._AbilityType.SpawnNew){
				GameObject unitObj=(GameObject)Instantiate(ability.spawnUnit, targetTile.GetPos(), srcUnit.thisT.rotation);
				Unit unit=unitObj.GetComponent<Unit>();
				
				unit.SetNewTile(targetTile);
				
				factionManager.InsertUnit(unit, srcUnit.factionID);
				if(gridManager.GetDistance(targetTile, srcUnit.tile)<=srcUnit.GetMoveRange()) gameControl.SelectUnit(srcUnit);
			}
			else if(ability.type==UnitAbility._AbilityType.ScanFogOfWar){
				List<Tile> targetTileList=gridManager.GetTilesWithinDistance(targetTile, ability.GetAOERange());
				targetTileList.Add(targetTile);
				
				for(int i=0; i<targetTileList.Count; i++) targetTileList[i].ForceVisible(ability.duration);
			}
			
			yield return null;
		}
		 ***************/



        //to setup abilities of individual unit
        public List<UnitAbility> GetAbilityListBasedOnIDList(List<int> IDList) {
            if (instance == null) return null;
            return instance._GetAbilityListBasedOnIDList(IDList);
        }
        public List<UnitAbility> _GetAbilityListBasedOnIDList(List<int> IDList) {
            List<UnitAbility> newList = new List<UnitAbility>();
            for (int i = 0; i < IDList.Count; i++) {
                for (int n = 0; n < unitAbilityDBList.Count; n++) {
                    if (unitAbilityDBList[n].prefabID == IDList[i]) {
                        //if(!unitAbilityDBList[n].onlyAvailableViaPerk) 
                        newList.Add(unitAbilityDBList[n].Clone(true, battleData));
                        break;
                    }
                }
            }
            return newList;
        }

    }

}
