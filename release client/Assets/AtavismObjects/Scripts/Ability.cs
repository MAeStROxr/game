using UnityEngine;
using System.Collections;

public class AtavismAbility : Activatable {
	
	public int id = 0;
    //public string command = "";
    //public string style = "";
    public string rank = "";
   	public int cost = 0;
	public string costProperty = "mana";
    public int distance = 0;
    public int castTime = 0;
    public bool globalcd = false;
    public bool weaponcd = false;
    public string cooldownType = "";
    public float cooldownLength = 0;
    public string weaponReq = "";
    public int reagentReq = -1;
    //public string stancereq = "";
    public TargetType targetType;

	// Use this for initialization
	void Start () {
	
	}
	
	public AtavismAbility Clone(GameObject go) {
		AtavismAbility clone = go.AddComponent<AtavismAbility>();
		clone.id = id;
		clone.name = name;
		clone.icon = icon;
		//clone.style = style;
		clone.rank = rank;
		clone.cost = cost;
		clone.distance = distance;
		clone.castTime = castTime;
		clone.globalcd = globalcd;
		clone.weaponcd = weaponcd;
		clone.cooldownType = cooldownType;
		clone.cooldownLength = cooldownLength;
		clone.weaponReq = weaponReq;
		clone.targetType = targetType;
		clone.tooltip = tooltip;
		return clone;
	}
	
	public override bool Activate() {
		// TODO: Enhance the target/self setting based on whether the current
		// target is friendly or an enemy
		if (ClientAPI.GetTargetObject() != null && targetType != TargetType.Self)
			NetworkAPI.SendTargetedCommand(ClientAPI.GetTargetOid(), "/ability " + id);
		else
			NetworkAPI.SendTargetedCommand(ClientAPI.GetPlayerOid(), "/ability " + id);
		return true;
	}

	/// <summary>
	/// Draws the tooltip via the old GUI system
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public override void DrawTooltip(float x, float y) {
		int width = 150;
		int height = 50;
		Rect tooltipRect = new Rect(x, y - height, width, height);
		GUI.Box(tooltipRect, "");
		GUI.Label(new Rect(tooltipRect.x + 5, tooltipRect.y + 5, 140, 20), name);
		GUI.Label(new Rect(tooltipRect.x + 5, tooltipRect.y + 25, 140, 20), cost + " " + costProperty);
	}
	
	public override Cooldown GetLongestActiveCooldown() {
		if (cooldownType == "") {
			return Abilities.Instance.GetCooldown(id.ToString(), true);
		} else {
			return Abilities.Instance.GetCooldown(cooldownType, true);
		}
	}
	
	/// <summary>
	/// Shows the tooltip using the new UGUI system.
	/// </summary>
	/// <param name="target">Target.</param>
	public void ShowTooltip(GameObject target) {
		UGUITooltip.Instance.SetTitle(name);
		if (castTime == 0) {
			UGUITooltip.Instance.SetType("Instant");
		} else {
			UGUITooltip.Instance.SetType(castTime + " seconds");
		}
		UGUITooltip.Instance.SetTypeColour(UGUITooltip.Instance.abilityCastTimeColour);
		if (cost > 0) {
			UGUITooltip.Instance.AddAttribute(cost.ToString(), " " + costProperty, false, UGUITooltip.Instance.abilityCostColour);
		}
		if (distance > 4) {
			UGUITooltip.Instance.AddAttribute(distance.ToString(), "m", false, UGUITooltip.Instance.abilityRangeColour);
		} else {
			UGUITooltip.Instance.AddAttribute("Melee", "", false, UGUITooltip.Instance.abilityRangeColour);
		}
		UGUITooltip.Instance.SetDescription(tooltip);
		UGUITooltip.Instance.Show(target);
	}
}
