﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtavismCombat : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void ClientReady() {
		NetworkAPI.RegisterExtensionMessageHandler("combat_event", HandleCombatEvent);
		NetworkAPI.RegisterExtensionMessageHandler("Duel_Challenge", HandleDuelChallenge);
		NetworkAPI.RegisterExtensionMessageHandler("Duel_Challenge_End", HandleDuelChallengeEnd);
		
		AtavismClient.Instance.WorldManager.RegisterObjectPropertyChangeHandler("state", HandleState);
	}
	
	public void HandleCombatEvent(Dictionary<string, object> props) {
		string eventType = (string)props["event"];
		OID caster = (OID)props["caster"];
		OID target = (OID)props["target"];
		int abilityID = (int)props["abilityID"];
		int effectID = (int)props["effectID"];
		string value1 = "" + (int)props["value1"];
		string value2 = "" + (int)props["value2"];
		
		//ClientAPI.Write("Got Combat Event: " + eventType);
		int messageType = 2;
		
		if (eventType == "CombatPhysicalDamage") {
			messageType = 1;
		} else if (eventType == "CombatMagicalDamage") {
			messageType = 1;
		} else if (eventType == "CombatMissed") {
			value1 = "Missed";
		} else if (eventType == "CombatDodged") {
			value1 = "Dodged";
		} else if (eventType == "CombatBlocked") {
			value1 = "Blocked";
		} else if (eventType == "CombatParried") {
			value1 = "Parried";
		} else if (eventType == "CombatEvaded") {
			value1 = "Evaded";
		} else if (eventType == "CombatImmune") {
			value1 = "Immune";
		} else if (eventType == "CombatBuffGained" || eventType == "CombatDebuffGained") {
			AtavismEffect e = Abilities.Instance.GetEffect (effectID);
			if (e != null) {
				value1 = e.name;
			} else {
				value1 = "";
			}
		} else if (eventType == "CombatBuffLost" || eventType == "CombatDebuffLost") {
			AtavismEffect e = Abilities.Instance.GetEffect (effectID);
			if (e != null) {
				value1 = e.name;
			} else {
				value1 = "";
			}
		} else if (eventType == "CastingStarted") {
			if (int.Parse(value1) > 0) {
				string[] csArgs = new string[2];
				csArgs[0] = value1;
				csArgs[1] = caster.ToString();
				AtavismEventSystem.DispatchEvent("CASTING_STARTED", csArgs);
			}
			return;
		} else if (eventType == "CastingCancelled") {
			string[] ccArgs = new string[2];
			ccArgs[0] = abilityID.ToString();
			ccArgs[1] = caster.ToString();
			AtavismEventSystem.DispatchEvent("CASTING_CANCELLED", ccArgs);
			return;
		}
		
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[7];
		args[0] = eventType;
		args[1] = caster.ToString();
		args[2] = target.ToString();
		args[3] = value1;
		args[4] = value2;
		args[5] = abilityID.ToString();
		args[6] = effectID.ToString();
		AtavismEventSystem.DispatchEvent("COMBAT_EVENT", args);
		
		//ClientAPI.GetObjectNode(target.ToLong()).GameObject.GetComponent<MobController3D>().GotDamageMessage(messageType, value1);
	}
	
	public void HandleDuelChallenge(Dictionary<string, object> props) {
		string challenger = (string)props["challenger"];
		UGUIConfirmationPanel.Instance.ShowConfirmationBox(challenger + " has challenged you to a Duel. Do you accept the challenge?", null, DuelChallengeResponse);
	}
	
	public void DuelChallengeResponse(object item, bool accepted)
	{
		if (accepted) {
			NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/duelAccept");
		} else {
			NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/duelDecline");
		}
	}
	
	public void HandleDuelChallengeEnd(Dictionary<string, object> props) {
		UGUIConfirmationPanel.Instance.Hide();
	}
	
	public void HandleState(object sender, ObjectPropertyChangeEventArgs args) {
		if (args.Oid == ClientAPI.GetPlayerOid())
			return;
			
		string state = (string) ClientAPI.GetObjectProperty(args.Oid, args.PropName);
		if (state == "spirit") {
			ClientAPI.GetObjectNode(args.Oid).GameObject.SetActive(false);
		} else {
			ClientAPI.GetObjectNode(args.Oid).GameObject.SetActive(true);
		}
	}
}
