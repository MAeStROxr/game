﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtavismEventMessageHandler : MonoBehaviour {

	static AtavismEventMessageHandler instance;

	// Use this for initialization
	void Start () {
		instance = this;
		
		NetworkAPI.RegisterExtensionMessageHandler("general_event", HandleGeneralEvent);
		NetworkAPI.RegisterExtensionMessageHandler("error_event", HandleErrorEvent);
		NetworkAPI.RegisterExtensionMessageHandler("requirement_failed_event", HandleRequirementFailedEvent);
	}
	
	public void HandleGeneralEvent(Dictionary<string, object> props) {
		string eventType = (string)props["event"];
		int val = (int)props["val"];
		string data = (string)props["data"];
		
		string errorMessage = "";
		//string errorMessage = eventType;
		if (eventType == "ReputationChanged") {
			errorMessage = "Reputation for faction " + val + " increased by: " + data;
		} else if (eventType == "DuelCountdown") {
			errorMessage = "" + val;
		} else if (eventType == "DuelStart") {
			errorMessage = "Fight!";
		} else if (eventType == "DuelVictory") {
			string[] results = data.Split(',');
			string winner = results[0];
			string loser = results[1];
			string[] cArgs = new string[3];
			cArgs[0] = winner + " has defeated " + loser + " in a duel";
			cArgs[1] = "System";
			cArgs[2] = "";
			AtavismEventSystem.DispatchEvent("CHAT_MSG_SYSTEM", cArgs);
			if (winner == ClientAPI.GetPlayerObject().Name) {
				errorMessage = "Victory!";
			} else {
				return;
			}
		} else if (eventType == "DuelDefeat") {
			if (data == ClientAPI.GetPlayerObject().Name) {
				errorMessage = "Defeat!";
			} else {
				return;
			}
		} else if (eventType == "DuelOutOfBounds") {
			errorMessage = "You have left the Duel Area, if you do not return you will forfeit the duel";
		} else if (eventType == "DuelNotOutOfBounds") {
			errorMessage = "You have returned to the Duel Area";
		}
		
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[1];
		args[0] = errorMessage;
		AtavismEventSystem.DispatchEvent("ANNOUNCEMENT", args);
	}
	
	public void HandleErrorEvent(Dictionary<string, object> props) {
		string eventType = (string)props["event"];
		int val = (int)props["val"];
		string data = (string)props["data"];
		ProcessErrorEvent(eventType, val, data);
	}
	
	public void ProcessErrorEvent(string eventType, int val, string data) {
		
		string errorMessage = "";
		//string errorMessage = eventType;
		if (eventType == "NotEnoughCurrency") {
			errorMessage = "You do not have enough currency to perform that action";
		} else if (eventType == "InvalidTradeCurrency") {
			errorMessage = "You cannot trade more currency than you have";
		} else if (eventType == "TooFarAway") {
			errorMessage = "You are too far away from the object to use it";
		} else if (eventType == "SkillLevelTooLow") {
			if (int.Parse(data) == 0) {
				errorMessage = "Requires Skill: " + Skills.Instance.GetSkillByID(val).skillname;
			} else {
				errorMessage = "Requires level " + data + " " + Skills.Instance.GetSkillByID(val).skillname;
			}
		} else if (eventType == "EquipMissing") {
			string weaponReq = data.ToLower();
			if (data.StartsWith("a") || data.StartsWith("e") || data.StartsWith("i") || 
			    data.StartsWith("o") || data.StartsWith("u")) {
				errorMessage = "Requires an " + data + " equipped in your Main Hand";
			} else {
				errorMessage = "Requires a " + data + " equipped in your Main Hand";
			}
		} else if (eventType == "ResourceNodeBusy") {
			errorMessage = "That Resource Node is currently being used by another player";
		} else if (eventType == "ResourceHarvestFailed") {
			errorMessage = "You failed to harvest the Resource Node";
		} else if (eventType == "InstanceRequiresGroup") {
			errorMessage = "You must be in a group to enter this Instance";
		} else if (eventType == "SkillAlreadyKnown") {
			errorMessage = "You already know the " + Skills.Instance.GetSkillByID(val).skillname + " Skill";
		} else if (eventType == "CannotSellItem") {
			errorMessage = "You cannot sell that Item";
		}
		
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[1];
		args[0] = errorMessage;
		AtavismEventSystem.DispatchEvent("ERROR_MESSAGE", args);
	}
	
	public void HandleRequirementFailedEvent(Dictionary<string, object> props) {
		string eventType = (string)props["event"];
		int val = (int)props["val"];
		string data = (string)props["data"];
		
		string errorMessage = eventType;
		
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[1];
		args[0] = errorMessage;
		AtavismEventSystem.DispatchEvent("ERROR_MESSAGE", args);
	}
	
	public static AtavismEventMessageHandler Instance {
		get {
			return instance;
		}
	}
}
