﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroupMember {
	public OID oid;
	public string name;
	public Dictionary<string, object> properties = new Dictionary<string, object>();
	public int raceID;
	public int classID;
}

public class AtavismGroup : MonoBehaviour {

	static AtavismGroup instance;

	List<GroupMember> members = new List<GroupMember>();
	OID leaderOid;

	// Use this for initialization
	void Start () {
		instance = this;
		
		NetworkAPI.RegisterExtensionMessageHandler("ao.GROUP_INVITE_REQUEST", HandleGroupInviteRequest);
		NetworkAPI.RegisterExtensionMessageHandler("ao.GROUP_UPDATE", HandleGroupUpdate);
		NetworkAPI.RegisterExtensionMessageHandler("ao.GROUP_PROPERTY_UPDATE", HandleGroupPropertyUpdate);
	}
	
	public void GroupInviteResponse(object inviter, bool accepted)
	{
		if (accepted)
			SendInviteResponseMessage((OID) inviter, "accept");
		else
			SendInviteResponseMessage((OID) inviter, "decline");
	}
	
	#region Message Senders
	public void SendInviteRequestMessage(OID targetOid) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("target", targetOid);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "ao.GROUP_INVITE", props);
	}
	
	public void SendInviteResponseMessage(OID targetOid, string response) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("groupLeaderOid", targetOid);
		props.Add("response", response);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "ao.GROUP_INVITE_RESPONSE", props);
	}
	
	public void SendGroupChatMessage(string message) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("message", message);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "ao.GROUP_CHAT", props);
	}
	
	public void RemoveGroupMember(int slotId) {
		RemoveGroupMember(members[slotId].oid);
	}
	
	public void RemoveGroupMember(OID oid) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("target", oid);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "ao.GROUP_REMOVE_MEMBER", props);
	}
	
	public void PromoteToLeader(OID oid) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("target", oid);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "ao.GROUP_PROMOTE_LEADER", props);
	}
	
	public void LeaveGroup() {
		Dictionary<string, object> props = new Dictionary<string, object>();
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "ao.GROUP_LEAVE", props);
	}
	#endregion Message Senders
	
	#region Message Handlers
	public void HandleGroupInviteRequest(Dictionary<string, object> props) {
		OID groupLeaderOid = (OID)props["groupLeaderOid"];
		string groupLeaderName = (string)props["groupLeaderName"];
		UGUIConfirmationPanel.Instance.ShowConfirmationBox(groupLeaderName + " has invited you to join their group", groupLeaderOid, GroupInviteResponse);
	}
	
	public void HandleGroupUpdate(Dictionary<string, object> props) {
		members.Clear();
		leaderOid = null;
		
		foreach (string propName in props.Keys) {
			if (props[propName] is Dictionary<string, object>) {
				Dictionary<string, object> memberInfo = (Dictionary<string, object>)props[propName];
				GroupMember groupMember = new GroupMember();
				groupMember.oid = (OID)memberInfo["memberOid"];
				if (groupMember.oid.ToLong() == ClientAPI.GetPlayerOid())
					continue;
				groupMember.name = (string)memberInfo["name"];
				int propCount = (int)memberInfo["statCount"];
				for (int i = 0; i < propCount; i++) {
					string statName = (string)memberInfo["stat" + i];
					groupMember.properties[statName] = memberInfo["stat" + i + "Value"];
				}
				//groupMember.level = (int)memberInfo["level"];
				//groupMember.health = (int)memberInfo["health"];
				//groupMember.maxHealth = (int)memberInfo["health-max"];
				members.Add(groupMember);
			}
		}
		
		if (members.Count > 0) {
			leaderOid = (OID)props["groupLeaderOid"];
		}
		
		// dispatch a ui event to tell the rest of the system
		string[] event_args = new string[1];
		AtavismEventSystem.DispatchEvent("GROUP_UPDATE", event_args);
	}
	
	public void HandleGroupPropertyUpdate(Dictionary<string, object> props) {
		OID memberOid = (OID)props["memberOid"];
		foreach (GroupMember member in members) {
			if (member.oid == memberOid) {
				int propCount = (int)props["statCount"];
				for (int i = 0; i < propCount; i++) {
					string statName = (string)props["stat" + i];
					member.properties[statName] = props["stat" + i + "Value"];
				}
			}
		}
		
		// dispatch a ui event to tell the rest of the system
		string[] event_args = new string[1];
		AtavismEventSystem.DispatchEvent("GROUP_UPDATE", event_args);
	}
	#endregion Message Handlers
	
	public static AtavismGroup Instance {
		get {
			return instance;
		}
	}
	
	public List<GroupMember> Members {
		get {
			return members;
		}
	}
	
	public OID LeaderOid {
		get {
			return leaderOid;
		}
	}
}
