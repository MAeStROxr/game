﻿using UnityEngine;
using System.Collections;

public class AtavismMecanimMobController3D : MobController3D {

	private Animator _animator;
	string overrideAnimationName;
	float overrideAnimationExpires;
	
	protected string actionState = "";
	string weaponType;

	// Use this for initialization
	void Start () {
		_animator = (Animator)GetComponentInChildren (typeof(Animator));
	}
	
	void ObjectNodeReady() {
		base.ObjectNodeReady();
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("weaponType", HandleWeaponType);
		// Get weaponType property
		if (GetComponent<AtavismNode>().PropertyExists("weaponType")) {
			weaponType = (string)GetComponent<AtavismNode>().GetProperty("weaponType");
			/*if (weaponType != null && weaponType != "" && weaponType != "Unarmed") {
				_animator.SetBool (weaponType, true);
			}*/
		}
	}
	
	void OnDestroy () {
		AtavismNode aNode = GetComponent<AtavismNode>();
		if (aNode != null) {
			aNode.RemoveObjectPropertyChangeHandler("weaponType", HandleWeaponType);
			aNode.RemoveObjectPropertyChangeHandler("deadstate", HandleDeadState);
			aNode.RemoveObjectPropertyChangeHandler("combatstate", HandleCombatState);
			aNode.RemoveObjectPropertyChangeHandler("movement_state", MovementStateHandler);
			aNode.RemoveObjectPropertyChangeHandler("mount", HandleMount);
			aNode.RemoveObjectPropertyChangeHandler("waterHeight", HandleWaterHeight);
		}
	}
	
	// Update is called once per frame
	void Update () {
		base.DoMovement();
		CharacterController controller = GetComponent <CharacterController>();
		if (_animator == null)
		{
			_animator = (Animator)GetComponentInChildren(typeof(Animator));
		}
		// Debug.Log("Using animator for mob: " + name);
		if (_animator && (dead && state != "spirit"))
		{
			_animator.SetBool ("Dead", true);
			_animator.SetBool ("SpecialAttack2", false);
			_animator.SetBool ("Attack", false);
		} else if (_animator) {
			_animator.SetInteger("MovementState", movementState);
			_animator.SetBool ("Dead", false);
			_animator.SetFloat ("Speed", controller.velocity.magnitude);
			
			if (controller.velocity.magnitude > 0.1f) {
				// Direction setting if you want to use it
				float dot = Vector3.Dot(transform.forward, movement.normalized);
				//Debug.Log("dot:" + dot);
				if(dot > 0.8) {
					// going forward direction
					_animator.SetFloat ("Direction", 0);
				} else if (dot < - 0.8) { 
					// going backwards
					_animator.SetFloat ("Direction", 45);
				} else {
					Vector3 cross = Vector3.Cross(transform.forward, movement.normalized);
					//Debug.Log("cross:" + cross);
					if(cross.y < 0) {
						// going left 
						_animator.SetFloat ("Direction", 225);
					} else {
						// going right 
						_animator.SetFloat ("Direction", 135);
					}
				}
			} else {
				_animator.SetFloat ("Direction", 0);
				_animator.SetFloat ("RotatingDirection", rotatingDirection);
			}
			
			if (mount != null) {
				_animator.SetBool (mountAnim, true);
			} else {
				_animator.SetBool (mountAnim, false);
			}
			if (jumping) {
				_animator.SetBool ("Jump", true);
			} else {
				_animator.SetBool ("Jump", false);
			}
			_animator.SetBool ("Combat", inCombat);
			if (weaponType != null && weaponType != "" && weaponType != "Unarmed") {
				foreach (AnimatorControllerParameter param in _animator.parameters)
				{
					if (param.name == weaponType)
						_animator.SetBool (weaponType, true);
				}
			}
			if (overrideAnimationName == "waving") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool ("Waving", false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool ("Waving", true);
				}
			} else if (overrideAnimationName == "mining") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool ("Mining", false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool ("Mining", true);
				}
			} else if (overrideAnimationName == "attack_normal") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool ("Attack", false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool ("Wound", false);
					_animator.SetBool ("SpecialAttack2", false);
					_animator.SetBool ("Attack", true);
				}
			} else if (overrideAnimationName == "attack_special") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool ("SpecialAttack", false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool ("SpecialAttack", true);
				}
			} else if (overrideAnimationName == "attack_special2") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool ("SpecialAttack2", false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool ("Wound", false);
					_animator.SetBool ("Attack", false);
					_animator.SetBool ("SpecialAttack2", true);
				}
			} else if (overrideAnimationName == "wound") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool ("Wound", false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool ("Wound", true);
				}
			} else if (overrideAnimationName != null && overrideAnimationName != "") {
				if (Time.time > overrideAnimationExpires) {
					_animator.SetBool (overrideAnimationName, false);
					overrideAnimationName = "";
				} else {
					_animator.SetBool (overrideAnimationName, true);
				}
			} else if (actionState != "") {
				_animator.SetBool(actionState, true);
			}
		}
	}
	
	public void HandleWeaponType (object sender, PropertyChangeEventArgs args)
	{
		if (weaponType != null && weaponType != "" && weaponType != "Unarmed") {
			_animator.SetBool (weaponType, false);
		}
		weaponType = (string)AtavismClient.Instance.WorldManager.GetObjectNode(oid).GetProperty("weaponType");
		if (_animator != null && weaponType != "" && weaponType != "Unarmed") {
			AtavismLogger.LogDebugMessage("setting combat state for animator");
			_animator.SetBool (weaponType, true);
		}
	}
	
	public void ActionStateHandler(object sender, PropertyChangeEventArgs args) {
		AtavismLogger.LogDebugMessage("Got actionstate");
		AtavismObjectNode node = (AtavismObjectNode)sender;
		string newState = (string)GetComponent<AtavismNode> ().GetProperty (args.PropertyName);
		if (_animator != null && newState != actionState) {
			AtavismLogger.LogDebugMessage("clearing old actionstate");
			_animator.SetBool (actionState, false);
		}
		actionState = newState;
	}
	
	public override void PlayMeleeAttackAnimation (string attackType, string result)
	{
		if (attackType == "normal") {
			overrideAnimationName = "attack_normal";
		} else if (attackType == "special") {
			overrideAnimationName = "attack_special";
		} else if (attackType == "special2") {
			overrideAnimationName = "attack_special2";
		}
		overrideAnimationExpires = Time.time + 1.0f; //overrideAnimation.length;
	}
	
	public override void PlayMeleeRecoilAnimation (string result)
	{
		overrideAnimationName = "wound";
		overrideAnimationExpires = Time.time + 0.5f;
	}
	
	public override void PlayAnimation(string animationName, float length) {
		if (_animator != null && overrideAnimationName != null && overrideAnimationName != "") {
			AtavismLogger.LogDebugMessage("clearing old animation");
			_animator.SetBool (overrideAnimationName, false);
		}
		overrideAnimationName = animationName;
		overrideAnimationExpires = Time.time + length;
	}
}
