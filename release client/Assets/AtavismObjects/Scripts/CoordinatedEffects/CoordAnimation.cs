using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoordAnimation : CoordinatedEffect {
	
	public string animationName;
	public float animationLength;
	public AudioClip soundClip;
	public bool soundClip3D = true;
	public float soundVolume = 1.0f;
	public float soundDelay = 0f;
	public bool loopSound = false;
	public float soundRepeatTime = 0f;
	public AttachmentSocket slot;
	public GameObject particle;
	public float particleDelay = 0f;
	public bool loopParticle = false;
	public float particleRepeatTime = 0f;
	public bool attachParticleToSocket = true;
	public bool interruptWhenMoving = false;
	bool running = false;
	float nextSoundLoopTime;
	float nextParticleLoopTime;
	AtavismObjectNode node;
	
	GameObject effectParticle;
	GameObject soundObject;
	string propName;
	int abilityID = -1;
	
	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("CASTING_CANCELLED", this);
	}
	
	void OnDestroy() {
		if (node != null && propName != null)
			node.RemovePropertyChangeHandler(propName, CancelPropHandler);
		AtavismEventSystem.UnregisterEvent("CASTING_CANCELLED", this);
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CASTING_CANCELLED") {
			if (abilityID > 0 && int.Parse(eData.eventArgs[0]) != abilityID)
				return;
			if (OID.fromString(eData.eventArgs[1]).ToLong() == node.Oid)
				CancelCoordEffect();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (activationTime != 0 && Time.time > activationTime && !running) {
			Run();
		}
		
		if (running && loopSound && Time.time > nextSoundLoopTime) {
			PlaySound(soundRepeatTime);
			if (soundRepeatTime > 0)
				nextSoundLoopTime = Time.time + soundRepeatTime;
		}
		if (running && loopParticle && Time.time > nextParticleLoopTime) {
			PlayParticle(particleRepeatTime);
			if (particleRepeatTime > 0)
				nextParticleLoopTime = Time.time + particleRepeatTime;
		}
		
		if (running && interruptWhenMoving && node.GameObject.GetComponent<CharacterController>() != null) {
			if (node.GameObject.GetComponent<CharacterController>().velocity.magnitude > 0.5f) {
				node.GameObject.GetComponent<AtavismMobController>().PlayAnimation("", 0);
				running = false;
			}
		}
	}
	
	public override void Execute(Dictionary<string, object> props) {
		if (!enabled)
			return;
		base.props = props;
		AtavismLogger.LogDebugMessage("Executing CoordAnimationEffect with num props: " + props.Count);
		/*foreach (string prop in props.Keys) {
			Logger.LogDebugMessage(prop + ":" + props[prop]);
		}*/
		
		if (props.ContainsKey("length")) {
			animationLength = (float) props["length"];
			// I think we should set that as well
			duration = animationLength;
		}
		
		if (props.ContainsKey("abilityID")) {
			abilityID = (int) props["abilityID"];
		}
		
		if (activationDelay == 0) {
			Run ();
		} else {
			activationTime = Time.time + activationDelay;
		}
	}
	
	public void Run() {
		
		if (target == CoordinatedEffectTarget.Caster) {
			node = ClientAPI.WorldManager.GetObjectNode((OID)props["sourceOID"]);
		} else {
			node = ClientAPI.WorldManager.GetObjectNode((OID)props["targetOID"]);
		}
		
		if (props.ContainsKey("cancelOnProp")) {
			propName = (string) props["cancelOnProp"];
			node.RegisterPropertyChangeHandler(propName, CancelPropHandler);
		}
		
		// Play attack animation
		if (animationName != null && animationName != "")
			node.GameObject.GetComponent<AtavismMobController>().PlayAnimation(animationName, animationLength);
		
		if (soundClip != null && soundDelay == 0) {
			if (loopSound)
				PlaySound(soundRepeatTime);
			else
				PlaySound(duration);
		}
		
		if (particle != null && particleDelay == 0) {
			if (loopParticle)
				PlayParticle(particleRepeatTime);
			else
				PlayParticle(duration);
		}
		
		// Now destroy this object
		if (destroyWhenFinished && duration != -1)
			Destroy(gameObject, duration + soundDelay);
		
		if (loopSound && (soundRepeatTime > 0 || soundDelay > 0)) {
			if (soundDelay == 0) {
				nextSoundLoopTime = Time.time + soundRepeatTime;
			} else {
				nextSoundLoopTime = Time.time + soundDelay;
			}
		}
		if (loopParticle && (particleRepeatTime > 0 || particleDelay > 0)) {
			if (particleDelay == 0) {
				nextParticleLoopTime = Time.time + particleRepeatTime;
			} else {
				nextParticleLoopTime = Time.time + particleDelay;
			}
		}
		running = true;
	}
	
	void PlaySound(float duration) {
		// Play sound clip on caster
		Transform slotTransform = node.GameObject.GetComponent<AtavismMobAppearance>().GetSocketTransform(AttachmentSocket.Root);
		soundObject = new GameObject();
		soundObject.transform.position = slotTransform.position;
		soundObject.transform.parent = slotTransform;
		AudioSource audioSource = soundObject.AddComponent<AudioSource>();
		audioSource.clip = soundClip;
		if (soundClip3D)
			audioSource.spatialBlend = 1.0f;
		audioSource.volume = SoundSystem.SoundEffectVolume * soundVolume;
		audioSource.Play();
		if (duration > 0)
			Destroy(soundObject, duration);
	}
	
	void PlayParticle(float duration) {
		Transform slotTransform = node.GameObject.GetComponent<AtavismMobAppearance>().GetSocketTransform(slot);
		
		effectParticle = (GameObject)Instantiate (particle, slotTransform.position, slotTransform.rotation);
		if (attachParticleToSocket)
			effectParticle.transform.parent = slotTransform;
		if (duration > 0)
			Destroy(effectParticle, duration);
	}
	
	public void CancelPropHandler(object sender, PropertyChangeEventArgs args) {
		CancelCoordEffect();
	}
	
	void CancelCoordEffect() {
		node.GameObject.GetComponent<AtavismMobController>().PlayAnimation("", 0);
		if (soundObject != null)
			DestroyImmediate(soundObject);
		if (effectParticle != null)
			DestroyImmediate(effectParticle);
		DestroyImmediate(gameObject);
	}
}
