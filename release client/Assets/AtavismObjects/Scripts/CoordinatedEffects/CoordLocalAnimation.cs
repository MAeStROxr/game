using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoordLocalAnimation : CoordinatedEffect {
	
	public AnimationClip animationClip;
	public float animationLength;
	public AudioClip soundClip;
	public bool soundClip3D = true;
	public float soundDelay = 0f;
	public bool loopSound = false;
	public float soundRepeatTime = 0f;
	public AttachmentSocket slot;
	public ParticleSystem particle;
	public float particleDelay = 0f;
	public bool loopParticle = false;
	public float particleRepeatTime = 0f;
	public bool attachParticleToSocket = true;
	public bool interruptWhenMoving = false;
	bool running = false;
	float nextSoundLoopTime;
	float nextParticleLoopTime;
	GameObject obj;
	
	GameObject effectParticle;
	GameObject soundObject;
	string propName;

	// Use this for initialization
	void Start () {
	
	}
	
	void OnDestroy() {
	}
	
	// Update is called once per frame
	void Update () {
		if (activationTime != 0 && Time.time > activationTime && !running) {
			Run();
		}

		if (running && loopSound && Time.time > nextSoundLoopTime) {
			PlaySound(soundRepeatTime);
			if (soundRepeatTime > 0)
				nextSoundLoopTime = Time.time + soundRepeatTime;
		}
		if (running && loopParticle && Time.time > nextParticleLoopTime) {
			PlayParticle(particleRepeatTime);
			if (particleRepeatTime > 0)
				nextParticleLoopTime = Time.time + particleRepeatTime;
		}
	}
	
	public override void Execute(Dictionary<string, object> props) {
		if (!enabled)
			return;
		base.props = props;
		AtavismLogger.LogDebugMessage("Executing CoordAnimationEffect with num props: " + props.Count);
		/*foreach (string prop in props.Keys) {
			Logger.LogDebugMessage(prop + ":" + props[prop]);
		}*/

		if (props.ContainsKey("length")) {
			animationLength = (float) props["length"];
			// I think we should set that as well
			duration = animationLength;
		}
		
		if (activationDelay == 0) {
			Run ();
		} else {
			activationTime = Time.time + activationDelay;
		}
	}
	
	public void Run() {

		obj = (GameObject)props["gameObject"];
		gameObject.transform.position = obj.transform.position;
		
		// Play animation
		if (animationClip != null) {
			obj.GetComponent<Animation>().clip = animationClip;
			obj.GetComponent<Animation>().Play();
		}
		
		if (soundClip != null && soundDelay == 0) {
			if (loopSound)
				PlaySound(soundRepeatTime);
			else
				PlaySound(duration);
		}

		if (particle != null && particleDelay == 0) {
			if (loopParticle)
				PlayParticle(particleRepeatTime);
			else
				PlayParticle(duration);
		}
		
		// Now destroy this object
		if (destroyWhenFinished && duration != -1)
			Destroy(gameObject, duration + soundDelay);

		if (loopSound && (soundRepeatTime > 0 || soundDelay > 0)) {
			if (soundDelay == 0) {
				nextSoundLoopTime = Time.time + soundRepeatTime;
			} else {
				nextSoundLoopTime = Time.time + soundDelay;
			}
		}
		if (loopParticle && (particleRepeatTime > 0 || particleDelay > 0)) {
			if (particleDelay == 0) {
				nextParticleLoopTime = Time.time + particleRepeatTime;
			} else {
				nextParticleLoopTime = Time.time + particleDelay;
			}
		}
		running = true;
	}

	void PlaySound(float duration) {
		// Play sound clip on caster
		Transform slotTransform = obj.transform; //node.GameObject.GetComponent<AtavismMobAppearance>().GetSocketTransform(AttachmentSockets.Root);
		soundObject = new GameObject("CoordSound");
		soundObject.transform.position = slotTransform.position;
		//soundObject.transform.parent = slotTransform;
		AudioSource audioSource = soundObject.AddComponent<AudioSource>();
		audioSource.clip = soundClip;
		if (soundClip3D)
			audioSource.spatialBlend = 1.0f;
		audioSource.Play();
		if (duration > 0)
			Destroy(soundObject, duration);
	}

	void PlayParticle(float duration) {
		Transform slotTransform = obj.transform; //node.GameObject.GetComponent<AtavismMobAppearance>().GetSocketTransform(slot);
		
		effectParticle = (GameObject)Instantiate (particle.gameObject, slotTransform.position, slotTransform.rotation);
		if (attachParticleToSocket)
			effectParticle.transform.parent = slotTransform;
		if (duration > 0)
			Destroy(effectParticle, duration);
	}
}
