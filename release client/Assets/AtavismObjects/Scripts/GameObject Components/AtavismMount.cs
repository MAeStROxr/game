﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtavismMount : MonoBehaviour {

	public Transform mountSocket;
	public AttachmentSocket characterSocket;
	public string riderAnimation = "Mount";
	long riderOid;
	
	public AnimationClip idleAnimation;
	public AnimationClip walkAnimation;
	public AnimationClip runAnimation;
	public AnimationClip jumpPoseAnimation;
	public AnimationClip swimIdleAnimation;
	public AnimationClip swimAnimation;
	public float walkMaxAnimationSpeed = 0.75f;
	public float trotMaxAnimationSpeed = 1.0f;
	public float runMaxAnimationSpeed = 1.0f;
	public float jumpAnimationSpeed = 1.15f;
	public float landAnimationSpeed = 1.0f;
	private bool useAnimator = false;
	public float runThreshold = 2.5f;
	bool jumping = false;
	bool jumpingReachedApex = false;
	int movementState = 0;
	
	private GameObject mountCoordEffect;
	private GameObject dismountCoordEffect;
	float destroyTime = -1;

	// Use this for initialization
	void Start () {
		Animation _animation = (Animation)GetComponent ("Animation");
		if (!_animation) {
			useAnimator = true;
		}
	
		foreach (AtavismMobNode mNode in ClientAPI.WorldManager.GetMobNodes()) {
			if (mNode.GameObject != null && mNode.GameObject.GetComponent<Collider>() != null && GetComponent<Collider>() != mNode.GameObject.GetComponent<Collider>())
				Physics.IgnoreCollision (GetComponent<Collider>(), mNode.GameObject.GetComponent<Collider>());
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (destroyTime != -1 && destroyTime < Time.time) {
			ClientAPI.GetObjectNode(riderOid).GameObject.transform.parent = null;
			ClientAPI.GetObjectNode(riderOid).Parent = null;
			DestroyImmediate(gameObject);
		}
	
		CharacterController controller = GetComponent <CharacterController>();
		
		// ANIMATION sector
		if (!useAnimator) {
			Animation _animation = GetComponent<Animation>();
			//Debug.Log("Using animation for mob: " + name);
			if (jumping) {
				if (!jumpingReachedApex) {
					_animation [jumpPoseAnimation.name].speed = jumpAnimationSpeed;
					_animation [jumpPoseAnimation.name].wrapMode = WrapMode.ClampForever;
					_animation.CrossFade (jumpPoseAnimation.name);
				} else {
					_animation [jumpPoseAnimation.name].speed = -landAnimationSpeed;
					_animation [jumpPoseAnimation.name].wrapMode = WrapMode.ClampForever;
					_animation.CrossFade (jumpPoseAnimation.name);				
				}
			} else if (movementState == 2) {
				if (controller.velocity.sqrMagnitude > 0.1) {
					_animation [swimAnimation.name].speed = Mathf.Clamp (controller.velocity.magnitude, 0.0f, runMaxAnimationSpeed);
					_animation.CrossFade (swimAnimation.name);
				} else {
					_animation.CrossFade (swimIdleAnimation.name);
				}
			} else {
				if (controller.velocity.sqrMagnitude > 0.1) {
					if (controller.velocity.magnitude > runThreshold) {
						_animation [runAnimation.name].speed = Mathf.Clamp (controller.velocity.magnitude, 0.0f, runMaxAnimationSpeed);
						_animation.CrossFade (runAnimation.name);
					} else {
						_animation [walkAnimation.name].speed = Mathf.Clamp (controller.velocity.magnitude, 0.0f, walkMaxAnimationSpeed);
						_animation.CrossFade (walkAnimation.name);	
					}
				} else {
					_animation.CrossFade (idleAnimation.name);
				}
			}
		} else if (useAnimator) {
			Animator _animator = GetComponent<Animator>(); 
			if (_animator == null)
			{
				_animator = (Animator)GetComponentInChildren(typeof(Animator));
			}
			// Debug.Log("Using animator for mob: " + name);
			if (_animator) {
				_animator.SetInteger("MovementState", movementState);
				_animator.SetFloat ("Speed", controller.velocity.magnitude);
				if (jumping) {
					_animator.SetBool ("Jump", true);
				} else {
					_animator.SetBool ("Jump", false);
				}
			}
		}
	}
	
	public void SetRider(long rider) {
		this.riderOid = rider;
		StartMount();
	}
	
	public bool IsGrounded (CollisionFlags collisionFlags)
	{
		return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
	}
	
	public void SetJumping(bool jumping) {
		this.jumping = jumping;
	}
	
	public void SetJumpingReachedApex(bool jumpingReachedApex) {
		this.jumpingReachedApex = jumpingReachedApex;
	}
	
	public void StartMount() {
		if (mountCoordEffect != null) {
			Dictionary<string, object> props = new Dictionary<string, object>();
			props.Add("sourceOID", OID.fromLong(riderOid));
			CoordinatedEffectSystem.ExecuteCoordinatedEffect(mountCoordEffect.name, props);
		}
	}
	
	public bool StartDismount() {
		if (dismountCoordEffect != null) {
			Dictionary<string, object> props = new Dictionary<string, object>();
			props.Add("sourceOID", OID.fromLong(riderOid));
			CoordinatedEffectSystem.ExecuteCoordinatedEffect(dismountCoordEffect.name, props);
			destroyTime = Time.time + 1.0f;
			return true;
		}
		return false;
	}
}
