﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AtavismGraveyard))]
public class AtavismGraveyardEditor : Editor {

	private bool factionsLoaded = false;
	
	public override void OnInspectorGUI() {
		AtavismGraveyard gy = target as AtavismGraveyard;
		
		EditorGUILayout.LabelField("ID: " + gy.id);
		
		// Read in Faction options from Database and display a drop down list
		if (!factionsLoaded)
			ServerFactions.LoadFactionOptions();
		
		gy.factionID = EditorGUILayout.IntPopup("Faction Req:", gy.factionID, ServerFactions.factionOptions, ServerFactions.factionIds); 
	}
}
