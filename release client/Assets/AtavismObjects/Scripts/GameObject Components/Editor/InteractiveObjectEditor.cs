﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(InteractiveObject))]
public class InteractiveObjectEditor : Editor {
	
	private bool effectsLoaded = false;
	private bool questsLoaded = false;
	private bool tasksLoaded = false;
	private bool instancesLoaded = false;
	string[] interactionTypes;
	
	public override void OnInspectorGUI() {
		InteractiveObject obj = target as InteractiveObject;
		
		EditorGUILayout.LabelField("ID: " + obj.id);
		
		// Read in Interaction options from Database and display a drop down list
		if (interactionTypes == null) {
			interactionTypes = new string[] {"~ none ~"};
			interactionTypes = ServerOptionChoices.LoadAtavismChoiceOptions("Interaction Type", false);
		}
		
		int selectedOption = GetPositionOfInteraction(obj.interactionType);
		selectedOption = EditorGUILayout.Popup("InteractionType:", selectedOption, interactionTypes); 
		obj.interactionType = interactionTypes[selectedOption];
		
		if (!questsLoaded)
			ServerQuests.LoadQuestOptions();
		questsLoaded = true;
		
		if (obj.interactionType.Contains("ApplyEffect")) {
			if (!effectsLoaded)
				ServerEffects.LoadEffectOptions();
			effectsLoaded = true;
			
			obj.interactionID = EditorGUILayout.IntPopup("Effect:", obj.interactionID, ServerEffects.effectOptions, ServerEffects.effectIds); 
		} else if (obj.interactionType.Contains("Quest")) {
			obj.interactionID = EditorGUILayout.IntPopup("Quest:", obj.interactionID, ServerQuests.questOptions, ServerQuests.questIds); 
		} else if (obj.interactionType.Contains("Task")) {
			if (!tasksLoaded)
				ServerTask.LoadTaskOptions();
			tasksLoaded = true;
			
			obj.interactionID = EditorGUILayout.IntPopup("Task:", obj.interactionID, ServerTask.taskOptions, ServerTask.taskIds); 
		} else if (obj.interactionType.Contains("Instance")) {
			if (!instancesLoaded)
				ServerInstances.LoadInstanceOptions();
			instancesLoaded = true;
			
			obj.interactionID = EditorGUILayout.IntPopup("Instance:", obj.interactionID, ServerInstances.instanceList, ServerInstances.instanceIds); 
			// Need to get a location to teleport to
			Vector3 position = new Vector3();
			float.TryParse(obj.interactionData1, out position.x);
			float.TryParse(obj.interactionData2, out position.y);
			float.TryParse(obj.interactionData3, out position.z);
			position = EditorGUILayout.Vector3Field("Position:", position);
			obj.interactionData1 = position.x.ToString();
			obj.interactionData2 = position.y.ToString();
			obj.interactionData3 = position.z.ToString();
		} else if (obj.interactionType.Contains("CoordEffect")) {
			EditorGUILayout.LabelField("Coord Effects:");
			if (obj.activateCoordEffects == null)
				obj.activateCoordEffects = new System.Collections.Generic.List<GameObject>();
			//obj.activateCoordEffects = (GameObject) EditorGUILayout.ObjectField("Coord Effect:", obj.activateCoordEffect, typeof(GameObject), false);
			for(int i = 0; i < obj.activateCoordEffects.Count; i++) {
				obj.activateCoordEffects[i] = (GameObject) EditorGUILayout.ObjectField(obj.activateCoordEffects[i], typeof(GameObject), false);
			}
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Add")) {
				obj.activateCoordEffects.Add(null);
			}
			if (GUILayout.Button("Remove")) {
				obj.activateCoordEffects.RemoveAt(obj.activateCoordEffects.Count-1);
			}
			EditorGUILayout.EndHorizontal();
		} 
		
		// Quest Required ID
		obj.questReqID = EditorGUILayout.IntPopup("Quest Required:", obj.questReqID, ServerQuests.questOptions, ServerQuests.questIds); 
		
		// Coord Effect
		obj.interactCoordEffect = (GameObject) EditorGUILayout.ObjectField("Interact Coord Effect:", obj.interactCoordEffect, typeof(GameObject), false);
		
		// Interaction Time
		obj.interactTimeReq = EditorGUILayout.FloatField("Interaction Duration:", obj.interactTimeReq);
		
		// Respawn Time
		obj.refreshDuration = EditorGUILayout.IntField("Respawn Time:", obj.refreshDuration);
		
		// Cursor Icon
		obj.cursorIcon = (Texture2D) EditorGUILayout.ObjectField("Mouse Over Cursor:", obj.cursorIcon, typeof(Texture2D), false);
		
		// highlight colour
		obj.highlightColour = EditorGUILayout.ColorField("Highlight Colour:", obj.highlightColour);
	}
	
	private int GetPositionOfInteraction (string interactionType)
	{
		for (int i = 0; i < interactionTypes.Length; i++) {
			if (interactionTypes [i] == interactionType)
				return i;
		}
		return 0;
	}
}
