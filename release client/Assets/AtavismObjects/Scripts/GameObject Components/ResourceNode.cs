﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum HarvestType {
	Axe,
	Pickaxe,
	None
}

public class ResourceNode : MonoBehaviour {

	public int id = -1;
	public List<ResourceDrop> resources;
	public int resourceCount = 1;
	int currentResourceCount;
	public HarvestType harvestTool;
	bool toolMustBeEquipped = true;
	public float timeToHarvest = 0;
	public int skillType = 0;
	public int reqSkillLevel = 0;
	public int skillLevelMax = 10;
	public float cooldown = 2;
	float cooldownEnds;
	public float refreshDuration = 60;
	public Texture2D cursorIcon;
	public bool highlight = true;
	public Color highlightColour = Color.cyan;
	public Sprite selectedIcon;
	
	public GameObject harvestCoordEffect;
	public GameObject activateCoordEffect;
	public GameObject deactivateCoordEffect;
	
	public bool isLODChild = false;

	Color initialColor;
	bool active = true;
	bool selected = false;
	Renderer[] renderers;
	Color[] initialColors;
	bool mouseOver = false;

	// Use this for initialization
	void Start () {
		cooldownEnds = Time.time;
		currentResourceCount = resourceCount;
		gameObject.AddComponent<AtavismNode>();
		GetComponent<AtavismNode>().AddLocalProperty("harvestType", harvestTool);
		GetComponent<AtavismNode>().AddLocalProperty("targetable", false);
		GetComponent<AtavismNode>().AddLocalProperty("active", active);

		if (GetComponent<Renderer>() != null) {
			if (GetComponent<Renderer>().material.HasProperty("_Color"))
				initialColor = GetComponent<Renderer>().material.color;
		} else {
			renderers = GetComponentsInChildren<Renderer>();
			initialColors = new Color[renderers.Length];
			for (int i = 0; i < renderers.Length; i++) {
				initialColors[i] = renderers[i].material.color;
			}
		}
		
		// Add child component to all children with colliders
		foreach(Collider child in GetComponentsInChildren<Collider>()) {
			if (child.gameObject != gameObject)
				child.gameObject.AddComponent<ObjectChildMouseDetector>();
		}
		
		Crafting.Instance.RegisterResourceNode(this);
	}
	
	void OnDestroy() {
		if (ClientAPI.ScriptObject != null)
			Crafting.Instance.RemoveResourceNode(id);
		AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
	}
	
	void OnDisable() {
		AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
	}
	
	// Update is called once per frame
	void Update () {
		if (mouseOver) {
			if (AtavismCursor.Instance.IsMouseOverUI()) {
				ResetHighlight();
				AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
			} else {
				Highlight();
				AtavismCursor.Instance.SetMouseOverObject(GetComponent<AtavismNode>(), cursorIcon, 4);
			}
			if (Input.GetMouseButtonDown(1) && !AtavismCursor.Instance.IsMouseOverUI()) {
				HarvestResource();
			}
		}
	}
	
	void OnMouseDown() 
	{
		if (!AtavismCursor.Instance.IsMouseOverUI() && !(ClientAPI.GetInputController() is ClickToMoveInputController))
			HarvestResource();
	}
	
	void OnMouseOver()
	{
		if (active) {
			AtavismCursor.Instance.SetMouseOverObject(GetComponent<AtavismNode>(), cursorIcon, 4);
			Highlight();
		}
		mouseOver = true;
	}
	
	void OnMouseExit()
	{
		AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
		if (!selected)
			ResetHighlight();
		mouseOver = false;
	}

	public void HarvestResource() {
		if (Time.time < cooldownEnds) {
			// Send error message
			string[] args = new string[1];
			args[0] = "You cannot perform that action yet.";
			AtavismEventSystem.DispatchEvent("ERROR_MESSAGE", args);
		} else {
			Crafting.Instance.HarvestResource(id);
			cooldownEnds = Time.time + cooldown;
			Debug.Log("Sending harvest resource");
		}
	}

	public void Highlight() {
		if (!highlight)
			return;
		if (GetComponent<Renderer>() != null) {
			GetComponent<Renderer>().material.color = highlightColour;
		} else {
			for (int i = 0; i < renderers.Length; i++) {
				renderers[i].material.color = highlightColour;
			}
		}
	}
	
	public void ResetHighlight() {
		if (GetComponent<Renderer>() != null) {
			GetComponent<Renderer>().material.color = initialColor;
		} else {
			for (int i = 0; i < renderers.Length; i++) {
				renderers[i].material.color = initialColors[i];
			}
		}
	}
	
	public int ID {
		set {
			id = value;
		}
	}
	
	public bool ToolMustBeEquipped {
		get {
			return toolMustBeEquipped;
		}
		set {
			toolMustBeEquipped = value;
		}
	}

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="ResourceNode"/> is active.
    /// </summary>
    /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
    public bool Active
    {
        get
        {
            return active;
        }
        set
        {
        	if (value != active) {
				if (value && activateCoordEffect != null) {
					Dictionary<string, object> props = new Dictionary<string, object>();
					props.Add("gameObject", gameObject);
					CoordinatedEffectSystem.ExecuteCoordinatedEffect(activateCoordEffect.name, props);
				} else if (!value && deactivateCoordEffect != null) {
					Dictionary<string, object> props = new Dictionary<string, object>();
					props.Add("gameObject", gameObject);
					CoordinatedEffectSystem.ExecuteCoordinatedEffect(deactivateCoordEffect.name, props);
				}
        	}
            active = value;
            GetComponent<AtavismNode>().AddLocalProperty("active", active);
            if (GetComponent<MeshRenderer>() != null)
            {
                GetComponent<MeshRenderer>().enabled = active;
                GetComponent<Collider>().enabled = active;
                foreach (Transform child in GetComponent<Transform>())
                {
                    if (child.GetComponent<MeshRenderer>() != null)
                    {
                        child.GetComponent<MeshRenderer>().enabled = active;
                    }

                    if (child.GetComponent<Collider>() != null)
                    {
                        child.GetComponent<Collider>().enabled = active;
                    }

                    child.gameObject.SetActive(active);
                }
            }
        }
    }

}
