﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The main 3D mob movement class. Manages movement properties such as speed and state,
/// handles jumping and gravity and calls controller.Move() when DoMovement() is called.
/// </summary>
public abstract class MobController3D : AtavismMobController {
	
	#region Animation Fields
	protected bool inCombat = false;
	protected string mountAnim = "Mount";
	
	#endregion Animation Fields

	#region Movement Fields
	protected Vector3 movement = Vector3.zero;
	
	protected bool dead = false;
	protected string state = "";
	// Is Walk on
	protected bool walk = false;
	// The speed when walking
	float walkSpeed = 2.0f;
	public float runThreshold = 2.5f;
	
	// The gravity for the character
	float gravity = 20.0f;
	// The gravity in controlled descent mode
	float speedSmoothing = 10.0f;
	float rotateSpeed = 5.0f; // was 250
	float trotAfterSeconds = 3.0f;
	
	// The current vertical speed
	private float verticalSpeed = 0.0f;
	// The current x-z move speed
	private float moveSpeed = 0.0f;
	
	// The last collision flags returned from controller.Move
	private CollisionFlags collisionFlags; 
	
	// Are we moving backwards (This locks the camera to not do a 180 degree spin)
	private bool movingBack = false;
	
	#region Jumping fields
	// How high do we jump when pressing jump and letting go immediately
	public float jumpHeight = 1.5f;
	// Last time we performed a jump
	private float lastJumpTime = -1.0f;
	bool canJump = true;
	private float jumpRepeatTime = 0.05f;
	private float jumpTimeout = 0.15f;
	private float groundedTimeout = 0.25f;
	private Vector3 inAirVelocity = Vector3.zero;
	private float lastGroundedTime = 0.0f;
	private bool isControllable = true;
	
	protected float fallingDistance = 0;
	protected float fallStartHeight = float.MinValue;
	#endregion Jumping fields
	
	public LayerMask groundLayers = 0;
	
	protected GameObject mount;
	
	protected float waterHeight = float.MinValue;
	bool underWater = false;
	
	protected int movementState = MOVEMENT_STATE_WALKING; // Used to switch between running (1), swimming (2) and flying (3)
	protected const int MOVEMENT_STATE_WALKING = 1;
	protected const int MOVEMENT_STATE_SWIMMING = 2;
	protected const int MOVEMENT_STATE_FLYING = 3;
	#endregion Movement Fields
	
	public GameObject friendlyTargetDecal;
	public GameObject neutralTargetDecal;
	public GameObject enemyTargetDecal;
	GameObject targetDecal;
	
	
	// Use this for initialization
	void Start ()
	{
	}
	
	protected void ObjectNodeReady ()
	{
		this.oid = GetComponent<AtavismNode> ().Oid;
		GetComponent<AtavismNode>().SetMobController(this);
		moveSpeed = runSpeed;
		
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler ("deadstate", HandleDeadState);
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler ("state", HandleState);
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("combatstate", HandleCombatState);
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("waterHeight", HandleWaterHeight);
		// Get deadstate property
		if (GetComponent<AtavismNode>().PropertyExists("deadstate")) {
			dead = (bool)GetComponent<AtavismNode>().GetProperty("deadstate");
		}
		// Get state property
		if (GetComponent<AtavismNode>().PropertyExists("state")) {
			state = (string)GetComponent<AtavismNode>().GetProperty("state");
		}
		// Get MovementState property
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("movement_state", MovementStateHandler);
		if (GetComponent<AtavismNode>().PropertyExists("movement_state")) {
			movementState = (int)GetComponent<AtavismNode>().GetProperty("movement_state");
		}
		// Get WaterHeight property
		if (GetComponent<AtavismNode>().PropertyExists("waterHeight")) {
			waterHeight = (float)GetComponent<AtavismNode>().GetProperty("waterHeight");
		}
		// Get Mount property
		GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("mount", HandleMount);
		if (GetComponent<AtavismNode>().PropertyExists("mount")) {
			string mountProp = (string)GetComponent<AtavismNode>().GetProperty("mount");
			if (mountProp != "") {
				StartMount(mountProp);
			} else {
				StopMount();
			}
		}
		//GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler("currentAction", null);
		transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
		
		// Get this mob to ignore collisions with other mobs/players
		foreach (AtavismMobNode mNode in ClientAPI.WorldManager.GetMobNodes()) {
			if (mNode.GameObject != null && mNode.GameObject.GetComponent<Collider>() != null && GetComponent<Collider>() != mNode.GameObject.GetComponent<Collider>())
				Physics.IgnoreCollision (GetComponent<Collider>(), mNode.GameObject.GetComponent<Collider>());
		}
		//if (go != gameObject)
		if (ClientAPI.GetPlayerObject() != null && !isPlayer)
			Physics.IgnoreCollision (GetComponent<Collider>(), ClientAPI.GetPlayerObject().GameObject.GetComponent<Collider>());
		
		string[] args = new string[1];
		args[0] = oid.ToString();
		AtavismEventSystem.DispatchEvent("MOB_CONTROLLER_ADDED", args);
	}
	
	void OnDestroy () {
		string[] args = new string[1];
		args[0] = oid.ToString();
		AtavismEventSystem.DispatchEvent("MOB_CONTROLLER_REMOVED", args);
		AtavismNode aNode = GetComponent<AtavismNode>();
		if (aNode != null) {
			aNode.RemoveObjectPropertyChangeHandler("deadstate", HandleDeadState);
			aNode.RemoveObjectPropertyChangeHandler("state", HandleState);
			aNode.RemoveObjectPropertyChangeHandler("combatstate", HandleCombatState);
			aNode.RemoveObjectPropertyChangeHandler("movement_state", MovementStateHandler);
			aNode.RemoveObjectPropertyChangeHandler("mount", HandleMount);
			aNode.RemoveObjectPropertyChangeHandler("waterHeight", HandleWaterHeight);
		}
		//StopMount();
		if (mount != null)
			Destroy(mount);
	}
	
	protected void DoMovement ()
	{
		if (isPlayer) {
			if ((!dead || state == "spirit") && ClientAPI.WorldManager.Player.CanMove()) {
				if (ClientAPI.WorldManager.sceneLoaded)
					movement = MovePlayer ();
			} else {
				// Still need to get player movement even when dead
				AtavismInputController inputManager = ClientAPI.GetInputController();
				inputManager.GetPlayerMovement ();
				if (transform.position.y > waterHeight) {
					ApplyGravity ();
				}
				movement = new Vector3(0, verticalSpeed * Time.deltaTime, 0) + inAirVelocity;
			}
		} else if (!dead) {
			movement = MoveMob ();
		} else {
			bool isAboveTerrain = IsAboveTerrain();
			int attempts = 0;
			while (!isAboveTerrain && attempts < 20 && groundLayers > 0) {
				transform.position += new Vector3(0, 1.5f, 0);
				isAboveTerrain = IsAboveTerrain();
				attempts++;
			}
			if (transform.position.y > waterHeight) {
				ApplyGravity ();
			}
			movement = new Vector3(0, verticalSpeed * Time.deltaTime, 0) + inAirVelocity;
		}
		// Move the controller
		CharacterController controller = GetComponent <CharacterController>();
		if (controller == null)
			return;
		if (mount != null) {
			controller = mount.GetComponent <CharacterController>();
			//transform.rotation = Quaternion.identity;
			transform.localPosition = Vector3.zero;
			//transform.localRotation = Quaternion.identity;
		}
		collisionFlags = controller.Move (movement);
		
		// Update facing if needed
		if (!isPlayer && target != -1 && !dead) {
			AtavismMobNode mobNode = (AtavismMobNode)AtavismClient.Instance.WorldManager.GetObjectNode (oid);
			AtavismObjectNode targetNode = AtavismClient.Instance.WorldManager.GetObjectNode (target);
			if (targetNode != null && mobNode.CanTurn() && movement.x == 0 && movement.z == 0) {
				Vector3 facingTarget = new Vector3(targetNode.GameObject.transform.position.x, 
				                                   transform.position.y, targetNode.GameObject.transform.position.z);
				transform.LookAt (facingTarget);
			}
		} else if (isPlayer && ClientAPI.GetInputController() is ClickToMoveInputController) {
			if (inCombat && movement.magnitude < 0.2f) {
				AtavismMobNode mobNode = (AtavismMobNode)AtavismClient.Instance.WorldManager.GetObjectNode (oid);
				AtavismMobNode targetNode = ClientAPI.GetTargetObject();
				if (targetNode != null && mobNode.CanTurn() && movement.x == 0 && movement.z == 0) {
					Vector3 facingTarget = new Vector3(targetNode.GameObject.transform.position.x, 
					                                   transform.position.y, targetNode.GameObject.transform.position.z);
					transform.LookAt (facingTarget);
				}
			}
		}
		
		// Set rotation to the move direction
		/*if (IsGrounded ()) {
			transform.rotation = rotation;
			//transform.rotation = Quaternion.LookRotation (moveDirection);
			
		} else {
			var xzMove = movement;
			xzMove.y = 0;
			if (xzMove.sqrMagnitude > 0.001) {
				transform.rotation = Quaternion.LookRotation (xzMove);
			}
		}*/	
		
		// We are in jump mode but just became grounded
		if (IsGrounded ()) {
			lastGroundedTime = Time.time;
			inAirVelocity = Vector3.zero;
			if (jumping) {
				jumping = false;
				SendMessage ("DidLand", SendMessageOptions.DontRequireReceiver);
				if (mount != null) {
					mount.GetComponent<AtavismMount>().SetJumping(false);
				}
			}
		}
		
		// Update camera if it is the player
		if (isPlayer) {
			AtavismInputController inputManager = ClientAPI.GetInputController();
			if (inputManager != null)
				inputManager.RunCameraUpdate();
		}
	}
	
	public override Vector3 MoveMob ()
	{
		float timeDifference = (Time.time - lastLocTimestamp);
		if (pathInterpolator != null) {
			PathLocAndDir locAndDir = pathInterpolator.Interpolate (Time.time);
			float interpolateSpeed = pathInterpolator.Speed;
			//UnityEngine.Debug.Log("MobNode.ComputePosition: oid " + oid + ", followTerrain " + followTerrain 
			//	+ ", pathInterpolator ");// + (locAndDir == null) ? "null" : locAndDir.ToString ());
			if (locAndDir != null) {
				/*if (locAndDir.LengthLeft > 0.25f) {
					transform.forward = locAndDir.Direction;
					//transform.rotation = Quaternion.LookRotation(locAndDir.Direction);
					//transform.rotation = Quaternion.LookRotation(LastDirection.normalized);
					//UnityEngine.Debug.Log("Set rotation to: " + transform.rotation);
				}*/
				lastDirection = locAndDir.Direction;
				lastDirTimestamp = Time.time;
				lastLocTimestamp = Time.time;
				Vector3 loc = locAndDir.Location;
				if (AtavismMobNode.useMoveMobNodeForPathInterpolator) {
					Vector3 diff = loc - transform.position;
					diff.y = 0;
					//desiredDisplacement = diff * timeDifference;
					if (diff.magnitude > 1)
						diff = diff.normalized;
					desiredDisplacement = diff * interpolateSpeed * timeDifference;
					//UnityEngine.Debug.Log("displacement: " + desiredDisplacement + " with loc: " + loc + " and current position: " + transform.position);
					if (desiredDisplacement != Vector3.zero) {
						//transform.forward = locAndDir.Direction;
						transform.rotation = Quaternion.LookRotation (desiredDisplacement);
					}
				} else {
					desiredDisplacement = Vector3.zero;
				}
			} else {
				// This interpolator has expired, so get rid of it
				pathInterpolator = null;
				lastDirection = Vector3.zero;
				desiredDisplacement = Vector3.zero;
				//UnityEngine.Debug.Log("Path interpolator for mob: " + oid + " has expired");
			}
		} else {
			lastLocTimestamp = Time.time;
			if (mount != null) {
				Vector3 pos = mount.transform.position + (Time.deltaTime * lastDirection);
				desiredDisplacement = pos - mount.transform.position;
			} else {
				Vector3 pos = transform.position + (Time.deltaTime * lastDirection);
				desiredDisplacement = pos - transform.position;
			}
			if (rotatingDirection != 0)
				transform.RotateAround(Vector3.up, rotatingDirection * UnityEngine.Time.deltaTime);
		}
		
		// Apply gravity
		// - extra power jump modifies gravity
		// - controlledDescent mode modifies gravity
		//if (!grounded) {
		if (transform.position.y > waterHeight) {
			ApplyGravity ();
		} else {
			verticalSpeed = 0.0f;
		}
		//UnityEngine.Debug.Log("Mob grounded? false");
		//} else {
		//	verticalSpeed = 0.0f;
		//}
		
		// Apply jumping logic
		ApplyJumping ();
		
		// Calculate actual motion
		// Multiply inAirVelocity by delta time as we don't multiply the whole movement
		inAirVelocity *= Time.deltaTime;
		//if (verticalSpeed > 0)
		//	UnityEngine.Debug.Log("Vertical speed: " + verticalSpeed);
		Vector3 movement = desiredDisplacement + new Vector3 (0, verticalSpeed * Time.deltaTime, 0) + inAirVelocity;
		//movement *= Time.deltaTime;
		
		return movement;
	}
	
	public override Vector3 MovePlayer ()
	{
		/*if (Input.GetButtonDown ("Jump")) {
			lastJumpButtonTime = Time.time;
			AtavismClient.Instance.WorldManager.SendJumpStarted ();
		}*/
		AtavismInputController inputManager = ClientAPI.GetInputController();
		if (inputManager == null)
			return Vector3.zero;
		Vector3 direction = inputManager.GetPlayerMovement ();
		
		// state check
		if (transform.position.y < waterHeight) {
			if (movementState != MOVEMENT_STATE_SWIMMING) {
				// start swimming and send state update to server
				movementState = MOVEMENT_STATE_SWIMMING;
				if (isPlayer) {
					SendMovementState();
				}
			}
			followTerrain = false;
		} else if (waterRegions.Count == 0 && movementState == MOVEMENT_STATE_SWIMMING) {
			movementState = MOVEMENT_STATE_WALKING;
			followTerrain = true;
			if (isPlayer) {
				SendMovementState();
			}
		}/* else if (movementState != MOVEMENT_STATE_FLYING) {
			followTerrain = true;
		}*/
		
		if (transform.position.y < (waterHeight - 1)) {
			if (!underWater) {
				underWater = true;
			}
		} else if (underWater) {
			underWater = false;
		}
		
		// Apply gravity if not swimming or flying
		//if (followTerrain) {
			// - extra power jump modifies gravity
			// - controlledDescent mode modifies gravity
			ApplyGravity ();
		
			// Apply jumping logic
			ApplyJumping ();
		/*} else {
			verticalSpeed = 0;
		}*/
		
		// Calculate actual motion
		direction.Normalize ();
		float speed = runSpeed;
		if (walk || movingBack)
			speed = walkSpeed;
		//Vector3 displacement = (transform.rotation * direction) * speed;
		Vector3 displacement = (direction * speed);
		Vector3 movement = displacement + new Vector3 (0, verticalSpeed, 0) + inAirVelocity;
		movement *= Time.deltaTime;
		
		// Update player direction - Used for MMO
		if (mount != null) {
			AtavismClient.Instance.WorldManager.Player.SetDirection (displacement, mount.transform.position, Time.time);
			AtavismClient.Instance.WorldManager.Player.Orientation = mount.transform.rotation;
		} else {
			AtavismClient.Instance.WorldManager.Player.SetDirection (displacement, transform.position, Time.time);
			AtavismClient.Instance.WorldManager.Player.Orientation = transform.rotation;
		}
		
		return movement;
	}
	
	void SendMovementState() {
		/*Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("movement_state", movementState);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "ao.MOVEMENT_STATE", props);*/
	}
	
	#region Movement
	public void StartJump() {
		lastJumpButtonTime = Time.time;
		AtavismClient.Instance.WorldManager.SendJumpStarted ();
	}
	
	public void ApplyJumping ()
	{
		// Prevent jumping too fast after each other
		if (lastJumpTime + jumpRepeatTime > Time.time) {
			return;
		}
		
		if (IsGrounded () || !followTerrain) {
			// Jump
			// - Only when pressing the button down
			// - With a timeout so you can press the button slightly before landing		
			if (canJump && Time.time < lastJumpButtonTime + jumpTimeout) {
				jumpingReachedApex = false;
				AtavismLogger.LogDebugMessage ("Applying jump for mob: " + name);
				verticalSpeed = CalculateJumpVerticalSpeed (jumpHeight);
				SendMessage ("DidJump", SendMessageOptions.DontRequireReceiver);
				jumping = true;
				if (mount != null) {
					mount.GetComponent<AtavismMount>().SetJumpingReachedApex(false);
					mount.GetComponent<AtavismMount>().SetJumping(true);
				}
			}
		}
	}
	
	public void ApplyGravity ()
	{
		if (isControllable) {	// don't move player at all if not controllable.
			
			// When we reach the apex of the jump we send out a message
			if (jumping && !jumpingReachedApex && verticalSpeed <= 0.0) {
				jumpingReachedApex = true;
				SendMessage ("DidJumpReachApex", SendMessageOptions.DontRequireReceiver);
				if (mount != null) {
					mount.GetComponent<AtavismMount>().SetJumpingReachedApex(true);
				}
			}
			
			bool grounded = false;
			if (mount != null) {
				grounded = mount.GetComponent<AtavismMount>().IsGrounded (collisionFlags);
			} else {
				grounded = IsGrounded ();
			}
			
			if (grounded || !followTerrain) {
				verticalSpeed = 0.0f;
				jumping = false;
				if (mount != null) {
					mount.GetComponent<AtavismMount>().SetJumping(false);
				}
				fallStartHeight = float.MinValue;
				fallingDistance = 0;
			} else {
				verticalSpeed -= gravity * Time.deltaTime;
				if (fallStartHeight == float.MinValue) {
					fallStartHeight = transform.position.y;
				} else {
					fallingDistance = fallStartHeight - transform.position.y;
				}
			}
		}
	}
	
	public float CalculateJumpVerticalSpeed (float targetJumpHeight)
	{
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt (2 * targetJumpHeight * gravity);
	}
	
	public void OnControllerColliderHit (ControllerColliderHit hit)
	{
		//	Debug.DrawRay(hit.point, hit.normal);
		if (hit.moveDirection.y > 0.01) 
			return;
	}
	
	public float GetSpeed ()
	{
		return moveSpeed;
	}
	
	public bool IsMovingBackwards ()
	{
		return movingBack;
	}
	
	public bool IsMoving ()
	{
		return Mathf.Abs (Input.GetAxisRaw ("Vertical")) + Mathf.Abs (Input.GetAxisRaw ("Horizontal")) > 0.5;
	}
	
	public bool IsJumping ()
	{
		return jumping;
	}
	
	public bool IsGrounded ()
	{
		return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
	}
	
	public bool HasJumpReachedApex ()
	{
		return jumpingReachedApex;
	}
	
	public bool IsGroundedWithTimeout ()
	{
		return lastGroundedTime + groundedTimeout > Time.time;
	}
	
	bool IsAboveTerrain() {
		// Make sure the corpse isn't underground
		Ray ray = new Ray(transform.position, Vector3.down);
		RaycastHit hit;
		// Casts the ray and get the first game object hit
		return Physics.Raycast (ray, out hit, Mathf.Infinity, groundLayers);
	}
	
	#endregion Movement
	
	#region Other Functions
	public override void PlayMeleeAttackAnimation (string attackType, string result)
	{
	}
	
	public override void PlayMeleeRecoilAnimation (string result)
	{
	}
	
	public override void PlayAnimation(string animationName, float length) {
	}
	
	public override void WaterRegionEntered(int id) {
		if (!waterRegions.Contains(id))
			waterRegions.Add(id);
			
		followTerrain = false;
		movementState = MOVEMENT_STATE_SWIMMING;
	}
	
	public override void WaterRegionLeft(int id) {
		waterRegions.Remove(id);
		
		if (waterRegions.Count == 0 && transform.position.y > waterHeight) {
			followTerrain = true;
			movementState = MOVEMENT_STATE_WALKING;
		}
	}
	
	/// <summary>
	/// Called when this mob is now targeted by the player.
	/// </summary>
	public void StartTarget() {
		int targetType = 0;
		if (GetComponent<AtavismNode>().PropertyExists("reaction")) {
			targetType = (int)GetComponent<AtavismNode>().GetProperty("reaction");
		}
		
		if (targetType > 0 && friendlyTargetDecal != null) {
			targetDecal = (GameObject)Instantiate(friendlyTargetDecal, transform.position, transform.rotation);
			targetDecal.transform.parent = transform;
		} else if (targetType == 0 && neutralTargetDecal != null) {
			targetDecal = (GameObject)Instantiate(neutralTargetDecal, transform.position, transform.rotation);
			targetDecal.transform.parent = transform;
		} else if (enemyTargetDecal != null) {
			targetDecal = (GameObject)Instantiate(enemyTargetDecal, transform.position, transform.rotation);
			targetDecal.transform.parent = transform;
		}
	}
	
	/// <summary>
	/// Called when this mob is no longer targeted by the player
	/// </summary>
	public void StopTarget() {
		if (targetDecal != null) {
			Destroy(targetDecal);
			targetDecal = null;
		}
	}
	
	public void SetWalk(bool walk) {
		this.walk = walk;
	}
	
	#endregion Other Functions
	
	#region Property Handlers
	public void HandleDeadState (object sender, PropertyChangeEventArgs args)
	{
		//Debug.Log ("Got dead update: " + oid);
		dead = (bool)AtavismClient.Instance.WorldManager.GetObjectNode(oid).GetProperty("deadstate");
		if (dead) {
			target = -1;
		}
		//Debug.Log ("Set dead state to: " + dead);
	}
	
	public void HandleState (object sender, PropertyChangeEventArgs args)
	{
		//Debug.Log ("Got dead update: " + oid);
		state = (string)AtavismClient.Instance.WorldManager.GetObjectNode(oid).GetProperty("state");
		//Debug.Log ("Set dead state to: " + dead);
	}
	
	public void HandleCombatState (object sender, PropertyChangeEventArgs args)
	{
		inCombat = (bool)AtavismClient.Instance.WorldManager.GetObjectNode(oid).GetProperty("combatstate");
	}
	
	public void MovementStateHandler(object sender, PropertyChangeEventArgs args) {
		AtavismLogger.LogDebugMessage("Got movementstate");
		AtavismObjectNode node = (AtavismObjectNode)sender;
		int state = (int)GetComponent<AtavismNode> ().GetProperty (args.PropertyName);
		if (isPlayer && state != MOVEMENT_STATE_SWIMMING && transform.position.y < waterHeight) {
			movementState = MOVEMENT_STATE_SWIMMING;
		} else {
			movementState = state;
		}
	}
	
	public void HandleWaterHeight(object sender, PropertyChangeEventArgs args) {
		AtavismLogger.LogDebugMessage("Got waterheight");
		AtavismObjectNode node = (AtavismObjectNode)sender;
		waterHeight = (float)GetComponent<AtavismNode> ().GetProperty (args.PropertyName);
	}
	
	public void HandleMount(object sender, PropertyChangeEventArgs args) {
		AtavismObjectNode node = (AtavismObjectNode)sender;
		string mount = (string)AtavismClient.Instance.WorldManager.GetObjectNode(oid).GetProperty (args.PropertyName);
		AtavismLogger.LogDebugMessage("Got mount: " + mount);
		if (mount != "") {
			StartMount(mount);
		} else {
			StopMount ();
		}
	}
	
	void StartMount(string mountPrefab) {
		int resourcePathPos = mountPrefab.IndexOf("Resources/");
		mountPrefab = mountPrefab.Substring(resourcePathPos + 10);
		mountPrefab = mountPrefab.Remove(mountPrefab.Length - 7);
		GameObject prefab = (GameObject)Resources.Load(mountPrefab);
		mount = (GameObject)UnityEngine.Object.Instantiate(prefab, transform.position, transform.rotation);
		DontDestroyOnLoad(mount);
		AtavismMount mountComp = mount.GetComponent<AtavismMount>();
		if (mountComp != null) {
			Transform characterSocket = GetComponent<AtavismMobAppearance>().GetSocketTransform(mount.GetComponent<AtavismMount>().characterSocket);
			transform.position = mount.GetComponent<AtavismMount>().mountSocket.position;
			transform.parent = mount.GetComponent<AtavismMount>().mountSocket;
			transform.localPosition = transform.position - characterSocket.position;
			transform.localRotation = Quaternion.identity;
			//ClientAPI.GetInputController().Target = mount.transform;
			ClientAPI.GetObjectNode(oid).Parent = mount;
			mountComp.SetRider(oid);
			mountAnim = mountComp.riderAnimation;
		}
	}
	
	void StopMount() {
		if (mount != null) {
			if (!mount.GetComponent<AtavismMount>().StartDismount()) {
				transform.parent = null;
				ClientAPI.GetObjectNode(oid).Parent = null;
				//ClientAPI.GetInputController().Target = transform;
				DestroyImmediate(mount);
			}
			mount = null;
		}
	}
	
	#endregion Property Handlers
	
	#region Properties
	public bool Walking {
		get {
			return walk;
		}
		set {
			walk = value;
		}
	}
	
	public float MobYaw {
		get {
			float yaw;
			yaw = transform.rotation.eulerAngles.y;
			return yaw;
		}
		set {
			Vector3 pitchYawRoll = transform.eulerAngles;
			pitchYawRoll.y = value;
			transform.eulerAngles = pitchYawRoll;
		}
	}
	#endregion Properties
}
