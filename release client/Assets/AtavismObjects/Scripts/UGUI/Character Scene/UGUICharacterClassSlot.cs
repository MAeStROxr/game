﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUICharacterClassSlot : MonoBehaviour {

	Button button;
	Sprite normalSprite;
	Color normalColor;
	public AtavismClassData classData;
	bool selected = false;

	// Use this for initialization
	void Awake () {
		button = GetComponent<Button>();
		button.image.sprite = classData.maleClassIcon;
		//normalSprite = button.image.sprite;
		normalColor = button.colors.normalColor;
	}

	// Update is called once per frame
	void Update () {
	
	}
	
	public void GenderChanged(string gender) {
		if (gender == "Male") {
			button.image.sprite = classData.maleClassIcon;
		} else if (gender == "Female") {
			button.image.sprite = classData.femaleClassIcon;
		}
	}
	
	public void SelectClass() {
		CharacterSelectionCreationManager.Instance.SetCharacterClass(classData);
	}

	public void OnMouseEnter() {
		if (!selected) {
			button.image.color = button.colors.highlightedColor;
		}
	}

	public void OnMouseExit() {
		if (!selected) {
			button.image.color = normalColor;
		}
	}

	public void ClassSelected(AtavismClassData selectedClass) {
		if (selectedClass.Equals(classData)) {
			//button.image.sprite = button.spriteState.pressedSprite;
			button.image.color = button.colors.pressedColor;
			selected = true;
		} else {
			//button.image.sprite = normalSprite;
			button.image.color = normalColor;
			selected = false;
		}
	}
}
