﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIServerListEntry : MonoBehaviour {

	public Text serverName;
	public Text serverType;
	public Text serverPopulation;
	WorldServerEntry entry;
	UGUIServerList serverList;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SetServerDetails(WorldServerEntry entry, UGUIServerList serverList) {
		this.entry = entry;
		if (entry.Name == AtavismClient.Instance.WorldId) {
			this.serverName.text = entry.Name + "(current)";
			GetComponent<Button>().interactable = false;
		} else {
			string status = (string)entry["status"];
			if (status != "Online") {
				this.serverName.text = entry.Name + " (" + status + ")";
				GetComponent<Button>().interactable = false;
			} else {
				this.serverName.text = entry.Name;
				GetComponent<Button>().interactable = true;
			}
		}
		
		this.serverType.text = "";
		this.serverPopulation.text = "";
		this.serverList = serverList;
	}
	
	public void ServerSelected() {
		serverList.SelectEntry(entry);
		//EventSystem.current.SetSelectedGameObject(gameObject);
	}
}
