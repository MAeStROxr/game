﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIAbility : MonoBehaviour {

	public int slotNum;
	public Text name;
	public Text description;
	public Text levelReq;
	public UGUIAbilitySlot abilitySlot;
	AtavismAbility ability;

	// Use this for initialization
	void Start () {
	}

	/*public void OnPointerClick(PointerEventData data)
	{
		switch(data.button)
		{
		case PointerEventData.InputButton.Left:
			PickUpOrPlaceItem();
			break;
		case PointerEventData.InputButton.Right:
			Activate();
			break;
		case PointerEventData.InputButton.Middle:
			break;
		}
	}*/

	public void UpdateAbilityData(AtavismAbility ability) {
		this.ability = ability;
		abilitySlot.UpdateAbilityData(ability);
		if (ability ==  null) {
			name.text = "";
			description.text = "";
			return;
		}
		name.text = ability.name;
		description.text = ability.tooltip;
		if (levelReq != null)
		{
			levelReq.text = "";
			Skill skill = Skills.Instance.GetSkillOfAbility(ability.id);
			if (skill != null)
			{
				for (int i = 0; i < skill.abilities.Count; i++)
				{
					if (skill.abilities[i] == ability.id)
					{
						levelReq.text = skill.abilityLevelReqs[i].ToString();
						break;
					}
				}
			}
		}
		
		// If the player doesn't know this ability, disable this panel
		if (!Abilities.Instance.PlayerAbilities.Contains(ability)) {
			GetComponent<Image>().color = Color.gray;
		} else {
			GetComponent<Image>().color = Color.white;
		}
	}

}
