﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIAbilitySlot : UGUIDraggableSlot {

	AtavismAbility ability;
	bool mouseEntered = false;

	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.SourceOnly;
	}
	
	public void UpdateAbilityData(AtavismAbility ability) {
		this.ability = ability;
		if (ability == null) {
			if (uguiActivatable != null) {
				Destroy (uguiActivatable.gameObject);
			}
		} else if (Abilities.Instance.PlayerAbilities.Contains(ability)){
			if (uguiActivatable == null) {
				uguiActivatable = (UGUIAtavismActivatable) Instantiate(Abilities.Instance.uguiAtavismAbilityPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			}
			uguiActivatable.SetActivatable(ability, ActivatableType.Ability, this);
			// Set background Image - HACK to still show ability when it is being dragged
			if (GetComponent<Image>() != null) {
				GetComponent<Image>().sprite = ability.icon;
			}
		} else {
			// Set background Image - HACK to still show ability when it is being dragged
			if (GetComponent<Image>() != null) {
				GetComponent<Image>().sprite = ability.icon;
			}
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		Debug.Log("On Drop");
		// Do nothing
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
	}
	
	public override void Discarded() {
	}

	
	public override void Activate() {
		if (ability == null)
			return;
		ability.Activate();
	}
	
	void ShowTooltip() {
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && uguiActivatable != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
