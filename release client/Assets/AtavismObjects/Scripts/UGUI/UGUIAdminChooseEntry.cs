﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIAdminChooseEntry : MonoBehaviour {

	public Text text;
	public Image icon; 
	AdminChooseEntryClicked entryClicked;
	int id;
	
	public void SetEntryDetails(AdminChooseEntryClicked entryClicked, int id, string name, Sprite image) {
		this.entryClicked = entryClicked;
		this.id = id;
		text.text = name;
		icon.sprite = image;
	}
	
	public void EntryClicked() {
		entryClicked(id);
	}
}
