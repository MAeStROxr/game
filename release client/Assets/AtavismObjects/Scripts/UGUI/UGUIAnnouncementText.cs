﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class UGUIAnnouncementText : MonoBehaviour {

	class AnnouncementTextInfo {
		public RectTransform rectTransform;
		public float endTime;
		public Vector2 position;
		public float offset;
	}

	public Text announcementPrefab;
	public float displayTime = 3f;
	public float verticalTravelDistance = 100f;
	public float spacingReq = 20f;
	float stopDisplay;
	bool showing = false;
	
	List<AnnouncementTextInfo> activeAnnouncements = new List<AnnouncementTextInfo>();
	AnnouncementTextInfo transformToRemove = null;
	
	// Use this for initialization
	void Start () {
		Hide();
		AtavismEventSystem.RegisterEvent("ANNOUNCEMENT", this);
		AtavismEventSystem.RegisterEvent("INVENTORY_EVENT", this);
		
		NetworkAPI.RegisterExtensionMessageHandler("announcement", HandleAnnouncementMessage);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("ANNOUNCEMENT", this);
		AtavismEventSystem.UnregisterEvent("INVENTORY_EVENT", this);
		
		NetworkAPI.RemoveExtensionMessageHandler("announcement", HandleAnnouncementMessage);
	}
	
	// Update is called once per frame
	void Update () {
		/*if (showing && Time.time > stopDisplay) {
			Hide ();
		}*/
		
		foreach (AnnouncementTextInfo announcementInfo in activeAnnouncements) {
			if (announcementInfo.endTime < Time.time) {
				transformToRemove = announcementInfo;
			}/* else {
				// work out movement
				float timePercent = (announcementInfo.endTime - Time.time) / displayTime;
				
				announcementInfo.position = new Vector2(0, verticalTravelDistance * (1.0f - timePercent) + announcementInfo.offset);
				announcementInfo.rectTransform.anchoredPosition = announcementInfo.position;
			}*/
		}
		
		if (transformToRemove != null) {
			DestroyImmediate(transformToRemove.rectTransform.gameObject);
			activeAnnouncements.Remove(transformToRemove);
			transformToRemove = null;
		}
		
		// Check if any elements are overlapping
		bool overlapping = true;
		//while (overlapping) {
		//	overlapping = false;
			
			for (int i = 1; i < activeAnnouncements.Count; i++) {
				if (Mathf.Abs(activeAnnouncements[i].position.y - activeAnnouncements[i-1].position.y) < spacingReq) {
					activeAnnouncements[i-1].offset += spacingReq;
					activeAnnouncements[i-1].position = new Vector2(0, activeAnnouncements[i-1].position.y + activeAnnouncements[i-1].offset);
					activeAnnouncements[i-1].rectTransform.anchoredPosition = activeAnnouncements[i-1].position;
					overlapping = true;
				}
			}
		//}
	}
	
	void Show(string message) {
		GameObject announcementGO = Instantiate(announcementPrefab.gameObject);
		RectTransform announcementText = announcementGO.GetComponent<RectTransform>();
		announcementText.parent = transform;
		announcementText.anchoredPosition = Vector2.zero;
		announcementText.GetComponent<Text>().text = message;
		AnnouncementTextInfo announcementInfo = new AnnouncementTextInfo();
		announcementInfo.rectTransform = announcementText;
		announcementInfo.endTime = Time.time + displayTime;
		announcementInfo.position = Vector2.zero;
		activeAnnouncements.Add(announcementInfo);
		//GetComponent<CanvasGroup>().alpha = 1f;
		//stopDisplay = Time.time + 3;
		//showing = true;
	}
	
	public void Hide() {
		//GetComponent<CanvasGroup>().alpha = 0f;
		//showing = false;
	}
	
	public void HandleAnnouncementMessage(Dictionary<string, object> props) {
		Show((string) props["AnnouncementText"]);
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "ANNOUNCEMENT") {
			Show(eData.eventArgs[0]);
		} else if (eData.eventType == "INVENTORY_EVENT") {
			if (eData.eventArgs[0] == "ItemHarvested") {
				string[] args = new string[1];
				AtavismInventoryItem item = Inventory.Instance.GetItemByTemplateID(int.Parse(eData.eventArgs[1]));
			
				string message = "Received " + item.name + " x" + eData.eventArgs[2];
				Show(message);
			}
		}
	}
}
