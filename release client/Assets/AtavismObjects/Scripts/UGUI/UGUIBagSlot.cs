﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class UGUIBagSlot : UGUIDraggableSlot {
	
	UGUIBag bagPanel;
	Button button;
	public Text cooldownLabel;
	Bag bag;
	bool mouseEntered = false;
	public KeyCode activateKey;
	float cooldownExpiration = -1;
	
	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.Standard;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(activateKey) && !ClientAPI.UIHasFocus()) {
			Activate();
		}
	}
	
	public void UpdateBagData(Bag bag, UGUIBag bagPanel) {
		this.bag = bag;
		this.bagPanel = bagPanel;
		if (bag == null) {
			if (uguiActivatable != null) {
				Destroy (uguiActivatable.gameObject);
			}
		} else {
			if (bag.itemTemplate == null) {
				// Do nothing, hard coded first bag?
			} else if (uguiActivatable == null) {
				uguiActivatable = Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				uguiActivatable.SetActivatable(bag.itemTemplate, ActivatableType.Bag, this);
			}
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		if (!allowOverwrite)
			return;
			
		UGUIAtavismActivatable droppedActivatable = eventData.pointerDrag.GetComponent<UGUIAtavismActivatable>();
		
		// Reject any references, temporaries or non item/bag slots
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference || 
		    droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Temporary ||  droppedActivatable.Link != null ||
		    (droppedActivatable.ActivatableType != ActivatableType.Item && droppedActivatable.ActivatableType != ActivatableType.Bag)) {
			return;
		}
		
		if (droppedActivatable.ActivatableType == ActivatableType.Item) {
			Inventory.Instance.PlaceItemAsBag((AtavismInventoryItem)droppedActivatable.ActivatableObject, slotNum);
		} else if (droppedActivatable.ActivatableType == ActivatableType.Bag) {
			Inventory.Instance.MoveBag(droppedActivatable.Source.slotNum, slotNum);
		}
		droppedActivatable.PreventDiscard();
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
		bag = null;
	}
	
	public void OnClick() {
		Activate();
	}
	
	public override void Activate() {
		if (bag == null)
			return;
		bagPanel.gameObject.SetActive(!bagPanel.gameObject.activeSelf);
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && uguiActivatable != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}