﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUICastingBar : MonoBehaviour {

	static UGUICastingBar instance;

	public Image icon;
	float startTime;
	float endTime = -1;

	// Use this for initialization
	void Start () {
		instance = this;
		
		Hide ();
		AtavismEventSystem.RegisterEvent("CASTING_STARTED", this);
		AtavismEventSystem.RegisterEvent("CASTING_CANCELLED", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("CASTING_STARTED", this);
		AtavismEventSystem.UnregisterEvent("CASTING_CANCELLED", this);
	}
	
	// Update is called once per frame
	void Update () {
		if (endTime != -1 && endTime > Time.time) {
			float total = endTime - startTime;
			float currentTime = endTime - Time.time;
			GetComponent<Slider>().value = 1 - ((float)currentTime / (float)total);
		} else {
			Hide ();
		}
	}
	
	void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
	}

	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CASTING_STARTED") {
			if (eData.eventArgs.Length > 1 && OID.fromString(eData.eventArgs[1]).ToLong() != ClientAPI.GetPlayerOid())
				return;
			Show();
			startTime = Time.time;
			endTime = Time.time + float.Parse(eData.eventArgs[0]);
		} else if (eData.eventType == "CASTING_CANCELLED") {
			if (eData.eventArgs.Length > 1 && OID.fromString(eData.eventArgs[1]).ToLong() != ClientAPI.GetPlayerOid())
				return;
			Hide ();
			endTime = -1;
		}
	}
	
	public static UGUICastingBar Instance {
		get {
			return instance;
		}
	}
}
