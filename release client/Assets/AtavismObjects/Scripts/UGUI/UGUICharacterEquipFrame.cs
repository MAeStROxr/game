﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUICharacterEquipFrame : MonoBehaviour {
	
	public UGUIPanelTitleBar titleBar;
	public List<UGUICharacterEquipSlot> slots;
	public UGUIItemDisplay ammoSlot;

	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("EQUIPPED_UPDATE", this);
		AtavismEventSystem.RegisterEvent("INVENTORY_UPDATE", this);
		UpdateEquipSlots();
		
		if (titleBar != null) {
			titleBar.SetPanelTitle(ClientAPI.GetPlayerObject().Name);
		}
	}
	
	void OnEnable() {
		UpdateEquipSlots();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("EQUIPPED_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("INVENTORY_UPDATE", this);
	}

	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "EQUIPPED_UPDATE" || eData.eventType == "INVENTORY_UPDATE") {
			// Update 
			UpdateEquipSlots();
		}
	}

	public void UpdateEquipSlots() {
		for (int i = 0; i < slots.Count; i++) {
			AtavismInventoryItem item = GetItemInSlot(slots[i].slotName);
			slots[i].UpdateEquipItemData(item);		
		}
		
		if (Inventory.Instance.EquippedAmmo != null) {
			ammoSlot.gameObject.SetActive(true);
			ammoSlot.SetItemData(Inventory.Instance.EquippedAmmo, Inventory.Instance.UnequipAmmo);
			if (ammoSlot.countText != null)
				ammoSlot.countText.text = Inventory.Instance.GetCountOfItem(Inventory.Instance.EquippedAmmo.templateId).ToString();
		} else {
			ammoSlot.gameObject.SetActive(false);
		}
	}

	AtavismInventoryItem GetItemInSlot(string slotName) {
		foreach (AtavismInventoryItem item in Inventory.Instance.EquippedItems.Values) {
			if (item.slot == slotName) {
				return item;
			} else if (item.slot == "Two Hand" && slotName == "Main Hand") {
				return item;
			}
			
		}
		return null;
	}

	public void Toggle() {
		gameObject.SetActive(!gameObject.activeSelf);
	}
}
