﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIClaimPermission : MonoBehaviour {
	
	public Text permissionText;
	ClaimPermission permission;
	
	// Use this for initialization
	void Start () {
		
	}
	
	public void SetPermissionDetails(ClaimPermission permission) {
		string[] levels = new string[] {"Add Objects", "Edit Objects", "Add Users", "Manage Users"};
		this.permission = permission;
		this.permissionText.text = permission.playerName + " (" + levels[permission.permissionLevel-1] + ")";
	}
	
	public void RemoveClaimPermission() {
		WorldBuilder.Instance.RemovePermission(permission.playerName);
	}
	
}
