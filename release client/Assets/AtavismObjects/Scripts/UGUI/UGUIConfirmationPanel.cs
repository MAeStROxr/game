﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIConfirmationPanel : MonoBehaviour {

	static UGUIConfirmationPanel instance;

	public UGUIPanelTitleBar titleBar;
	public Text confirmationText;
	public Button yesButton;
	public Button cancelButton;
	
	object confirmationObject;
	ConfirmationResponse confirmationResponse;

	// Use this for initialization
	void Start () {
		instance = this;
	
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		if (titleBar != null)
			titleBar.SetOnPanelClose(CancelClicked);
	}
	
	void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	
	public void ShowConfirmationBox(string message, object confirmObject, ConfirmationResponse responseMethod)
	{
		Show();
		confirmationText.text = message;
		this.confirmationObject = confirmObject;
		this.confirmationResponse = responseMethod;
	}
	
	public void YesClicked() {
		confirmationResponse(confirmationObject, true);
		Hide ();
	}
	
	public void CancelClicked() {
		confirmationResponse(confirmationObject, false);
		Hide ();
	}
	
	public static UGUIConfirmationPanel Instance {
		get {
			return instance;
		}
	}
}
