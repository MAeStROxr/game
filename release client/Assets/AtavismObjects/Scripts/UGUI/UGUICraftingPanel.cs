﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUICraftingPanel : MonoBehaviour {

	public UGUIPanelTitleBar titleBar;
	public List<UGUICraftingSlot> craftingSlots;
	public List<UGUIItemDisplay> resultSlots;
	public Button craftButton;
	bool showing = false;

	// Use this for initialization
	void Start () {
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		Hide ();
		
		AtavismEventSystem.RegisterEvent("CRAFTING_GRID_UPDATE", this);
		AtavismEventSystem.RegisterEvent("CRAFTING_START", this);
		AtavismEventSystem.RegisterEvent("CLOSE_CRAFTING_STATION", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("CRAFTING_GRID_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("CRAFTING_START", this);
		AtavismEventSystem.UnregisterEvent("CLOSE_CRAFTING_STATION", this);
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		showing = true;
		
		if (titleBar != null)
			titleBar.SetPanelTitle(Crafting.Instance.StationType.ToString());
			
		UpdateCraftingGrid();
		AtavismCursor.Instance.SetUGUIActivatableClickedOverride(PlaceCraftingItem);
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		showing = false;
		
		// Set all referenced items back to non referenced
		for (int i = 0; i < craftingSlots.Count; i++) {
			craftingSlots[i].ResetSlot();
		}
		
		Crafting.Instance.ClearGrid();
		AtavismCursor.Instance.ClearUGUIActivatableClickedOverride(PlaceCraftingItem);
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CRAFTING_GRID_UPDATE") {
			// Update 
			UpdateCraftingGrid();
		} else if (eData.eventType == "CRAFTING_START") {
			if (!showing) {
				Show ();
			}
		} else if (eData.eventType == "CLOSE_CRAFTING_STATION") {
			Hide();
		}
	}
	
	void UpdateCraftingGrid() {
		for (int i = 0; i < craftingSlots.Count; i++) {
			if (i < Crafting.Instance.GridItems.Count && Crafting.Instance.GridItems[i].item != null) {
				craftingSlots[i].UpdateCraftingSlotData(Crafting.Instance.GridItems[i]);
			} else {
				craftingSlots[i].UpdateCraftingSlotData(null);
			}
		}
		
		for (int i = 0; i < resultSlots.Count; i++) {
			if (i < Crafting.Instance.ResultItems.Count) {
				resultSlots[i].gameObject.SetActive(true);
				resultSlots[i].SetItemData(Crafting.Instance.ResultItems[i], null); 
			} else {
				resultSlots[i].gameObject.SetActive(false);
			}
		}
		
		if (Crafting.Instance.ResultItems.Count > 0) {
			craftButton.interactable = true;
		} else {
			craftButton.interactable = false;
		}
	}
	
	public void DoCraft() {
		Crafting.Instance.CraftItem();
	}
	
	public void PlaceCraftingItem(UGUIAtavismActivatable activatable) {
		if (activatable.Link != null)
			return;
		for (int i = 0; i < craftingSlots.Count; i++) {
			if (i < Crafting.Instance.GridItems.Count && Crafting.Instance.GridItems[i].item == null) {
				
				craftingSlots[i].SetActivatable(activatable);
				return;
			}
		}
	}
}
