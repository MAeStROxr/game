﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUICraftingSlot : UGUIDraggableSlot {
	
	Button button;
	public Text cooldownLabel;
	CraftingComponent component;
	bool mouseEntered = false;
	float cooldownExpiration = -1;
	
	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.Temporary;
	}
	
	public void UpdateCraftingSlotData(CraftingComponent component) {
		this.component = component;
		if (component == null) {
			if (uguiActivatable != null) {
				DestroyImmediate (uguiActivatable.gameObject);
			}
		} else {
			if (uguiActivatable == null) {
				uguiActivatable = (UGUIAtavismActivatable) Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			}
			uguiActivatable.SetActivatable(component.item, ActivatableType.Item, this);
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		UGUIAtavismActivatable droppedActivatable = eventData.pointerDrag.GetComponent<UGUIAtavismActivatable>();
		
		// Reject any references or non item slots
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference || droppedActivatable.Link != null
		    || droppedActivatable.ActivatableType != ActivatableType.Item) {
			return;
		}
		
		SetActivatable(droppedActivatable);
	}
	
	public void SetActivatable(UGUIAtavismActivatable newActivatable) {
		if (uguiActivatable != null && uguiActivatable != newActivatable) {
			// Delete existing child
			DestroyImmediate(uguiActivatable.gameObject);
			if (backLink != null) {
				backLink.SetLink(null);
			}
		} else if (uguiActivatable == newActivatable) {
			droppedOnSelf = true;
		}
		
		// If the source was a temporary slot, clear it
		if (newActivatable.Source.SlotBehaviour == DraggableBehaviour.Temporary) {
			newActivatable.Source.ClearChildSlot();
			uguiActivatable = newActivatable;
			
			uguiActivatable.transform.SetParent(transform, false);
			uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			backLink = newActivatable.Source.BackLink;
		} else {
			// Create a duplicate
			uguiActivatable = Instantiate(newActivatable);
			uguiActivatable.transform.SetParent(transform, false);
			uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			uguiActivatable.GetComponent<CanvasGroup>().blocksRaycasts = true;
			uguiActivatable.SetActivatable(newActivatable.ActivatableObject, ActivatableType.Item, this);
			
			// Set the back link
			backLink = newActivatable;
			newActivatable.SetLink(uguiActivatable);
		}
		
		newActivatable.SetDropTarget(this);
		
		Crafting.Instance.SetGridItem(slotNum, (AtavismInventoryItem)newActivatable.ActivatableObject);
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
		Crafting.Instance.SetGridItem(slotNum, null);
	}
	
	public override void Discarded() {
		if (droppedOnSelf) {
			droppedOnSelf = false;
			return;
		}
		DestroyImmediate(uguiActivatable.gameObject);
		if (backLink != null) {
			backLink.SetLink(null);
		}
		backLink = null;
		ClearChildSlot();
	}
	
	public override void Activate() {
		// Unlink item?
		Discarded ();
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && component != null && component.item != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}