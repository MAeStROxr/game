﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIDeathPopup : MonoBehaviour {

	bool dead = false;
	string state = "";

	// Use this for initialization
	void Start () {
		Hide ();
		ClientAPI.GetPlayerObject().GameObject.GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler ("deadstate", HandleDeadState);
		ClientAPI.GetPlayerObject().GameObject.GetComponent<AtavismNode>().RegisterObjectPropertyChangeHandler ("state", HandleState);
		
		// The player may have changed scenes, but their stats were not sent back down, so let's take a look
		if (ClientAPI.GetPlayerObject() != null) {
			if (ClientAPI.GetPlayerObject().PropertyExists("deadstate")) {
				dead = (bool)ClientAPI.GetPlayerObject().GetProperty("deadstate");
			}
			if (ClientAPI.GetPlayerObject().PropertyExists("state")) {
				state = (string)ClientAPI.GetPlayerObject().GetProperty("state");
			}
		}
		
		UpdateShowState();
	}
	
	void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	
	public void HandleDeadState (object sender, PropertyChangeEventArgs args)
	{
		dead = (bool)ClientAPI.GetPlayerObject().GameObject.GetComponent<AtavismNode>().GetProperty("deadstate");
		UpdateShowState();
	}
	
	public void HandleState (object sender, PropertyChangeEventArgs args)
	{
		state = (string)ClientAPI.GetPlayerObject().GameObject.GetComponent<AtavismNode>().GetProperty("state");
		UpdateShowState();
	}
	
	public void UpdateShowState() {
		if (dead && state != "spirit") {
			Show ();
		} else {
			Hide ();
		}
	}
	
	public void ReleaseClicked() {
		NetworkAPI.SendTargetedCommand (ClientAPI.GetPlayerOid(), "/release");
	}
	
	public void ReleaseToSpiritClicked()
	{
		NetworkAPI.SendTargetedCommand(ClientAPI.GetPlayerOid(), "/releaseToSpirit");
	}
}
