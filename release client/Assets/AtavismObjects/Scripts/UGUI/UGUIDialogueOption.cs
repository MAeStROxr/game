﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIDialogueOption : MonoBehaviour {

	public Image icon;
	public Text optionText;
	public Sprite newQuestSprite;
	public Sprite progressQuestSprite;
	public Sprite dialogueSprite;
	public Sprite merchantSprite;
	
	NpcInteractionEntry interaction;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SetNpcInteraction(NpcInteractionEntry interaction) {
		this.interaction = interaction;
		this.optionText.text = interaction.interactionTitle;
		
		if (interaction.interactionType == "offered_quest" || interaction.interactionType == "Quest") {
			icon.sprite = newQuestSprite;
		} else if (interaction.interactionType == "progress_quest") {
			icon.sprite = progressQuestSprite;
		} else if (interaction.interactionType == "dialogue" || interaction.interactionType == "Ability") {
			icon.sprite = dialogueSprite;
		} else if (interaction.interactionType == "merchant") {
			icon.sprite = merchantSprite;
		}
	}
	
	public void ActionClicked() {
		interaction.StartInteraction();
	}
}
