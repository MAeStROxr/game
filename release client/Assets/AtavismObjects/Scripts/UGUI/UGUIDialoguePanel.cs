﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIDialoguePanel : MonoBehaviour {

	public UGUIPanelTitleBar titleBar;
	public RectTransform dialogueContentPanel;
	public Text dialogueText;
	public List<UGUIDialogueOption> interactionOptions;
	public List<UGUIDialogueBarButton> bottomBarButtons;
	public UGUIQuestOffer questOfferPanel;
	public UGUIQuestProgress questProgressPanel;
	public UGUIQuestConclude questConcludePanel;

	// Use this for initialization
	void Start () {
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
			
		Hide ();
		// Register for 
		AtavismEventSystem.RegisterEvent("NPC_INTERACTIONS_UPDATE", this);
		AtavismEventSystem.RegisterEvent("DIALOGUE_UPDATE", this);
		AtavismEventSystem.RegisterEvent("QUEST_OFFERED_UPDATE", this);
		AtavismEventSystem.RegisterEvent("QUEST_PROGRESS_UPDATE", this);
		AtavismEventSystem.RegisterEvent("CLOSE_NPC_DIALOGUE", this);
	}
	
	void OnDestroy () {
		AtavismEventSystem.UnregisterEvent("NPC_INTERACTIONS_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("DIALOGUE_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("QUEST_OFFERED_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("QUEST_PROGRESS_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("CLOSE_NPC_DIALOGUE", this);
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		
		if (titleBar != null) {
			if (NpcInteraction.Instance.NpcId != null && ClientAPI.GetObjectNode(NpcInteraction.Instance.NpcId.ToLong()) != null) {
				titleBar.SetPanelTitle(ClientAPI.GetObjectNode(NpcInteraction.Instance.NpcId.ToLong()).Name);
			} else {
				titleBar.SetPanelTitle("");
			}
		}
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		
		dialogueContentPanel.gameObject.SetActive(false);
		questOfferPanel.gameObject.SetActive(false);
		questProgressPanel.gameObject.SetActive(false);
		questConcludePanel.gameObject.SetActive(false);
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "NPC_INTERACTIONS_UPDATE") {
			dialogueContentPanel.gameObject.SetActive(true);
			ShowOptions();
		} else if (eData.eventType == "DIALOGUE_UPDATE") {
			dialogueContentPanel.gameObject.SetActive(true);
			ShowChat();
		} else if (eData.eventType == "QUEST_OFFERED_UPDATE") {
			Show ();
			dialogueContentPanel.gameObject.SetActive(false);
			questOfferPanel.gameObject.SetActive(true);
			questOfferPanel.UpdateQuestOfferDetails();
			HideButtonBars();
			if (bottomBarButtons.Count > 0) {
				bottomBarButtons[0].gameObject.SetActive(true);
				bottomBarButtons[0].SetButtonFunction(questOfferPanel.AcceptQuest, "Accept");
			}
			if (bottomBarButtons.Count > 1) {
				bottomBarButtons[1].gameObject.SetActive(true);
				bottomBarButtons[1].SetButtonFunction(questOfferPanel.DeclineQuest, "Decline");
			}
		} else if (eData.eventType == "QUEST_PROGRESS_UPDATE") {
			Show ();
			dialogueContentPanel.gameObject.SetActive(false);
			questProgressPanel.gameObject.SetActive(true);
			questProgressPanel.UpdateQuestProgressDetails();
			HideButtonBars();
			if (bottomBarButtons.Count > 0) {
				bottomBarButtons[0].gameObject.SetActive(true);
				bottomBarButtons[0].SetButtonFunction(questProgressPanel.Continue, "Continue");
				if (Quests.Instance.GetQuestProgressInfo(0).Complete) { 
					bottomBarButtons[0].GetComponent<Button>().interactable = true;
				} else {
					bottomBarButtons[0].GetComponent<Button>().interactable = false;
				}
			}
			if (bottomBarButtons.Count > 1) {
				bottomBarButtons[1].gameObject.SetActive(true);
				bottomBarButtons[1].SetButtonFunction(questProgressPanel.Cancel, "Cancel");
			}
		} else if (eData.eventType == "CLOSE_NPC_DIALOGUE") {
			Hide ();
		}
	}
	
	void ShowOptions() {
		Show ();
		
		Dialogue d = NpcInteraction.Instance.Dialogue;
		if (d != null) {
			dialogueText.text = d.text;
		}
		
		for (int i = 0; i < interactionOptions.Count; i++) {
			if (i < NpcInteraction.Instance.InteractionOptions.Count) {
				interactionOptions[i].gameObject.SetActive(true);
				interactionOptions[i].SetNpcInteraction(NpcInteraction.Instance.InteractionOptions[i]);
			} else {
				interactionOptions[i].gameObject.SetActive(false);
			}
		}
		
		// Only show one bottom bar, close
		HideButtonBars();
		if(bottomBarButtons.Count > 0) {
			bottomBarButtons[0].gameObject.SetActive(true);
			bottomBarButtons[0].SetButtonFunction(Hide, "Close");
		}
	}
	
	void ShowChat() {
		Show ();
		
		Dialogue d = NpcInteraction.Instance.Dialogue;
		if (d != null) {
			dialogueText.text = d.text;
		}
		
		for (int i = 0; i < interactionOptions.Count; i++) {
			if (i < NpcInteraction.Instance.Dialogue.actions.Count) {
				interactionOptions[i].gameObject.SetActive(true);
				interactionOptions[i].SetNpcInteraction(NpcInteraction.Instance.Dialogue.actions[i]);
			} else {
				interactionOptions[i].gameObject.SetActive(false);
			}
		}
		
		// Only show one bottom bar, close
		HideButtonBars();
		if(bottomBarButtons.Count > 0) {
			bottomBarButtons[0].gameObject.SetActive(true);
			bottomBarButtons[0].SetButtonFunction(Hide, "Close");
		}
	}
	
	public void ShowConcludeQuestPanel() {
		questConcludePanel.gameObject.SetActive(true);
		questConcludePanel.UpdateQuestConcludeDetails();
		questProgressPanel.gameObject.SetActive(false);
		
		HideButtonBars();
		if (bottomBarButtons.Count > 0) {
			bottomBarButtons[0].gameObject.SetActive(true);
			bottomBarButtons[0].SetButtonFunction(questConcludePanel.CompleteQuest, "Complete");
		}
		if (bottomBarButtons.Count > 1) {
			bottomBarButtons[1].gameObject.SetActive(true);
			bottomBarButtons[1].SetButtonFunction(questProgressPanel.Cancel, "Cancel");
		}
	}
	
	void HideButtonBars() {
		for (int i = 0; i < bottomBarButtons.Count; i++) {
			bottomBarButtons[i].gameObject.SetActive(false);
		}
	}
}
