﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text timeText;
	AtavismEffect effect;
	int effectPos;

	// Use this for initialization
	void Start () {
	
	}
	
	void RunTimerUpdate() {
		// Check if this is on cooldown
		if (timeText == null)
			return;
		
		timeText.text = "";
		
		if (effect == null) 
			return;
		
		if (effect.Expiration < Time.time)
			return;
		
		StartCoroutine(UpdateTimer());
	}
	
	IEnumerator UpdateTimer() {
		if (!timeText || effect == null)
			yield break;
		
		while (effect != null && Time.time < effect.Expiration) {
			float timeLeft =  effect.Expiration - Time.time;
			timeText.text = "" + (int)timeLeft;
			yield return new WaitForSeconds(1);
		}
		
		timeText.text = "";
		
		yield return null;
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void SetEffect(AtavismEffect effect, int pos) {
		this.effect = effect;
		this.effectPos = pos;
		GetComponent<Image>().sprite = effect.icon;
		RunTimerUpdate();
	}
	
	public void RemoveEffect() {
		Abilities.Instance.RemoveBuff(effect, effectPos);
	}
	
	void ShowTooltip() {
		UGUITooltip.Instance.SetTitle(effect.name);
		UGUITooltip.Instance.SetType("");
		UGUITooltip.Instance.SetDescription(effect.tooltip);
		UGUITooltip.Instance.Show(gameObject);
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		set {
			if (value && effect != null) {
				ShowTooltip();
			} else {
				HideTooltip();
			}
		}
	}
}
