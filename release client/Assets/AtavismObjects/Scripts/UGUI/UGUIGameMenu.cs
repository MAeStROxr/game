﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIGameMenu : MonoBehaviour {
	
	bool showing = false;

	// Use this for initialization
	void Start () {
		Hide ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (showing) {
				Hide();
			} else {
				Show();
			}
			
		}
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1.0f;
		GetComponent<CanvasGroup>().interactable = true;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		showing = true;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().interactable = false;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		showing = false;
	}
	
	public void Quit() {
		Application.Quit();
	}
}
