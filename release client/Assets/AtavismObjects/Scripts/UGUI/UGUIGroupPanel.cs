﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UGUIGroupPanel : MonoBehaviour {
	
	public List<UGUIGroupMember> memberPanels;
	
	// Use this for initialization
	void Start () {
		Hide ();
		AtavismEventSystem.RegisterEvent("GROUP_UPDATE", this);
		UpdateGroupMembers();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("GROUP_UPDATE", this);
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().interactable = true;
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().interactable = false;
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "GROUP_UPDATE") {
			// Update 
			UpdateGroupMembers();
		}
	}
	
	public void UpdateGroupMembers() {
		if (AtavismGroup.Instance.Members.Count == 0) {
			Hide ();
			return;
		} else {
			Show();
		}
		
		for (int i = 0; i < memberPanels.Count; i++) {
			if (i < AtavismGroup.Instance.Members.Count) {
				memberPanels[i].gameObject.SetActive(true);
				memberPanels[i].UpdateGroupMember(AtavismGroup.Instance.Members[i]);
			} else {
				memberPanels[i].gameObject.SetActive(false);
			}
		}
	}
}