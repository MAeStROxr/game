﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public delegate void OnItemClicked(AtavismInventoryItem item);

/// <summary>
/// Handles the display of an item in UGUI, such as setting the texture and the count label. 
/// This script can only be used on a button.
/// </summary>
public class UGUIItemDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text countText;
	OnItemClicked itemClickedFunction;
	AtavismInventoryItem item;
	int slotNum;
	bool mouseEntered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void ItemClicked() {
		if (itemClickedFunction != null)
			itemClickedFunction(item);
	}
	
	public void SetItemData(AtavismInventoryItem item, OnItemClicked itemClickedFunction) {
		this.item = item;
		if (GetComponent<Button>() != null && item != null) {
			GetComponent<Button>().image.sprite = item.icon;
		}
		
		if (countText != null && item != null) {
			countText.text = item.Count.ToString();
		}
		this.itemClickedFunction = itemClickedFunction;
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && item != null) {
				item.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
