﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class UGUIMailAttachment : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text countText;
	AtavismInventoryItem item;
	int slotNum;
	bool mouseEntered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void SetMailAttachmentData(AtavismInventoryItem item, int count, int slot) {
		this.item = item;
		if (GetComponent<Button>() != null) {
			GetComponent<Button>().image.sprite = item.icon;
		}
		
		if (countText != null) {
			countText.text = count.ToString();
		}
		this.slotNum = slot;
	}
	
	public void TakeMailAttachment() {
		Mailing.Instance.TakeMailItem(slotNum);
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && item != null) {
				item.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
