﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UGUIMailList : UIList<UGUIMailListEntry> {

	public UGUIPanelTitleBar titleBar;
	public UGUIMailRead mailReadPanel;
	public KeyCode toggleKey;
	bool showing = false;

	void Awake() {
		Hide ();
		AtavismEventSystem.RegisterEvent("SHOW_MAIL", this);
		AtavismEventSystem.RegisterEvent("MAIL_UPDATE", this);
		AtavismEventSystem.RegisterEvent("MAIL_SENT", this);
		AtavismEventSystem.RegisterEvent("CLOSE_MAIL_WINDOW", this);
		AtavismEventSystem.RegisterEvent("MAIL_SELECTED", this);
	}
	
	void Start() {
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		// Delete the old list
		ClearAllCells();
		
		Refresh();
		
		Mailing.Instance.RequestMailList();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("SHOW_MAIL", this);
		AtavismEventSystem.UnregisterEvent("MAIL_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("MAIL_SENT", this);
		AtavismEventSystem.UnregisterEvent("CLOSE_MAIL_WINDOW", this);
		AtavismEventSystem.UnregisterEvent("MAIL_SELECTED", this);
	}
	
	void OnEnable() {
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	public void Show() {
		showing = true;
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		// Delete the old list
		ClearAllCells();
		
		Refresh();
	}
	
	public void Hide() {
		showing = false;
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}
	
	void Update() {
		if (Input.GetKeyDown(toggleKey) && !ClientAPI.UIHasFocus()) {
			if (showing) {
				Hide ();
			} else {
				Mailing.Instance.RequestMailList();
				Show ();
			}
		}
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "SHOW_MAIL") {
			Show ();
		} else if (eData.eventType == "MAIL_UPDATE") {
			if (!showing)
				return;
			// Delete the old list
			ClearAllCells();
			Refresh();
		} else if (eData.eventType == "CLOSE_MAIL_WINDOW") {
			Hide();
		} else if (eData.eventType == "MAIL_SELECTED") {
			Hide();
			mailReadPanel.StartReadingMail();
		}
	}

	#region implemented abstract members of UIList
	
	public override int NumberOfCells ()
	{
		int numCells = ClientAPI.ScriptObject.GetComponent<Mailing>().MailList.Count;
		return numCells;
	}
	
	public override void UpdateCell (int index, UGUIMailListEntry cell)
	{
		cell.SetMailEntryDetails(ClientAPI.ScriptObject.GetComponent<Mailing>().GetMailEntry(index));
	}
	
	#endregion
}
