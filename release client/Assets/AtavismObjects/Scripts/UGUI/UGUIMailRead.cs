﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIMailRead : MonoBehaviour {

	public Text nameText;
	public Text subjectText;
	public Text messageText;
	public List<UGUIMailAttachment> itemSlots;
	public List<UGUICurrency> currencyDisplays;
	public Button takeCurrencyButton;
	public UGUIMailCompose composePanel;
	public UGUIMailList mailList;
	MailEntry mailBeingRead;

	// Use this for initialization
	void Start () {
		AtavismEventSystem.RegisterEvent("MAIL_UPDATE", this);
		AtavismEventSystem.RegisterEvent("CLOSE_MAIL_WINDOW", this);
		AtavismEventSystem.RegisterEvent("MAIL_SELECTED", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("MAIL_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("CLOSE_MAIL_WINDOW", this);
		AtavismEventSystem.UnregisterEvent("MAIL_SELECTED", this);
	}
	
	public void OnEvent(AtavismEventData eData)
	{
		if (!enabled)
			return;
			
		if (eData.eventType == "MAIL_UPDATE")
		{
			UpdateAttachmentsDisplay();
		} else if (eData.eventType == "CLOSE_MAIL_WINDOW") {
			gameObject.SetActive(false);
		} else if (eData.eventType == "MAIL_SELECTED") {
			StartReadingMail();
		}
	}
	
	public void StartReadingMail() {
		mailBeingRead = ClientAPI.ScriptObject.GetComponent<Mailing>().SelectedMail;
		if (mailBeingRead != null) {
			gameObject.SetActive(true);
			nameText.text = mailBeingRead.senderName;
			subjectText.text = mailBeingRead.subject;
			messageText.text = mailBeingRead.message;
			UpdateAttachmentsDisplay();
			if (mailList != null)
				mailList.Hide();
		} else {
		}
	}
	
	void UpdateAttachmentsDisplay() {
		mailBeingRead = ClientAPI.ScriptObject.GetComponent<Mailing>().SelectedMail;
		// Items
		for (int i = 0; i < itemSlots.Count; i++) {
			if (mailBeingRead.items.Count > i && mailBeingRead.items[i].item != null) {
				itemSlots[i].gameObject.SetActive(true);
				itemSlots[i].SetMailAttachmentData(mailBeingRead.items[i].item, mailBeingRead.items[i].count, i);
			} else {
				itemSlots[i].gameObject.SetActive(false);
			}
		}
		// Currency
		Currency c = mailBeingRead.GetMainCurrency();
		List<CurrencyDisplay> currencyDisplayList = new List<CurrencyDisplay>();
		if (c != null && mailBeingRead.currencies[c] > 0) {
			currencyDisplayList = Inventory.Instance.GenerateCurrencyListFromAmount(c.id, mailBeingRead.currencies[c]);
			takeCurrencyButton.gameObject.SetActive(true);
		} else {
			takeCurrencyButton.gameObject.SetActive(false);
		} 
		
		for (int i = 0; i < currencyDisplays.Count; i++) {
			if (i < currencyDisplayList.Count) {
				currencyDisplays[i].gameObject.SetActive(true);
				currencyDisplays[i].SetCurrencyDisplayData(currencyDisplayList[i]);
			} else {
				currencyDisplays[i].gameObject.SetActive(false);
			}
		}
	}
	
	public void TakeCurrency() {
		Mailing.Instance.TakeMailCurrency(mailBeingRead);
	}
	
	public void DeleteMail() {
		Mailing.Instance.DeleteMail(mailBeingRead);
		gameObject.SetActive(false);
		mailList.Show();
	}
	
	public void Reply() {
		if (composePanel == null)
			return;
		
		gameObject.SetActive(false);
		composePanel.gameObject.SetActive(true);
		composePanel.StartReplyMail(mailBeingRead.senderName, mailBeingRead.subject);
	}
	
	public void Return() {
		mailList.Show();
	}
	
	public void Close() {
		gameObject.SetActive(false);
	}
}
