﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIMerchantFrame : MonoBehaviour {

	public UGUIPanelTitleBar titleBar;
	public List<UGUIMerchantItemEntry> entries;
	public Button prevButton;
	public Button nextButton;
	public RectTransform purchaseCountPanel;
	public InputField purchaseCountText;
	public Button minusButton;
	public Button plusButton;
	UGUIMerchantItemSlot selectedPurchaseItem;
	int multiplePurchaseCount = 1;
	int currentPage = 0;
	bool showing = false;

	// Use this for initialization
	void Start () {
		if (titleBar != null)
			titleBar.SetOnPanelClose(Hide);
		AtavismEventSystem.RegisterEvent("MERCHANT_UPDATE", this);
		AtavismEventSystem.RegisterEvent("CLOSE_NPC_DIALOGUE", this);
		Hide ();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("MERCHANT_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("CLOSE_NPC_DIALOGUE", this);
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		showing = true;
		currentPage = 0;
		AtavismCursor.Instance.SetUGUIActivatableClickedOverride(SellItemToMerchant);
		
		if (titleBar != null)
			titleBar.SetPanelTitle(ClientAPI.GetObjectNode(NpcInteraction.Instance.NpcId.ToLong()).Name);
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		showing = false;
		AtavismCursor.Instance.ClearUGUIActivatableClickedOverride(SellItemToMerchant);
		
		if (purchaseCountPanel != null) {
			purchaseCountPanel.gameObject.SetActive(false);
		}
	}
	
	public void ShowPrevPage() {
		if (currentPage > 0) {
			currentPage--;
		}
		UpdateMerchantFrame();
	}
	
	public void ShowNextPage() {
		currentPage++;
		UpdateMerchantFrame();
	}
	
	void UpdateMerchantFrame() {
		for (int i = 0; i < entries.Count; i++) {
			if ((currentPage * entries.Count) + i < NpcInteraction.Instance.MerchantItems.Count) {
				entries[i].gameObject.SetActive(true);
				entries[i].UpdateMerchantItemData(NpcInteraction.Instance.GetMerchantItem((currentPage * entries.Count) + i), this);
			} else {
				entries[i].gameObject.SetActive(false);
			}
		}
	
		// Update visibility of prev and next buttons
		if (currentPage > 0) {
			prevButton.gameObject.SetActive(true);
		} else {
			prevButton.gameObject.SetActive(false);
		}
		
		if (NpcInteraction.Instance.MerchantItems.Count > (currentPage + 1) * entries.Count) {
			nextButton.gameObject.SetActive(true);
		} else {
			nextButton.gameObject.SetActive(false);
		}
		
		if (purchaseCountPanel != null) {
			purchaseCountPanel.gameObject.SetActive(false);
		}
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "MERCHANT_UPDATE") {
			if (!showing)
				Show ();
			UpdateMerchantFrame();
		} else if (eData.eventType == "CLOSE_NPC_DIALOGUE") {
			Hide ();
		}
	}
	
	public bool ShowPurchaseCountPanel(UGUIMerchantItemSlot mItemEntry) {
		if (purchaseCountPanel != null) {
			purchaseCountPanel.gameObject.SetActive(true);
			multiplePurchaseCount = 1;
			if (purchaseCountText != null) {
				purchaseCountText.text = multiplePurchaseCount.ToString();
			}
			if (minusButton != null)
				minusButton.gameObject.SetActive(false);
			selectedPurchaseItem = mItemEntry;
			return true;
		}
		return false;
	}
	
	public void ReduceMultipleCount() {
		multiplePurchaseCount--;
		if (multiplePurchaseCount < 2) {
			multiplePurchaseCount = 1;
			if (minusButton != null)
				minusButton.gameObject.SetActive(false);
		}
		if (purchaseCountText != null) {
			purchaseCountText.text = multiplePurchaseCount.ToString();
		}
	}
	
	public void IncreaseMultipleCount() {
		multiplePurchaseCount++;
		if (minusButton != null)
			minusButton.gameObject.SetActive(true);
		if (purchaseCountText != null) {
			purchaseCountText.text = multiplePurchaseCount.ToString();
		}
	}
	
	public void PurchaseMultiple() {
		if (purchaseCountText != null) {
			int count = int.Parse(purchaseCountText.text);
			selectedPurchaseItem.StartPurchase(count);
		}
	}
	
	public void SellItemToMerchant(UGUIAtavismActivatable activatable) {
		NpcInteraction.Instance.SellItemToMerchant((AtavismInventoryItem)activatable.ActivatableObject);
	}
}
