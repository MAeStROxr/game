﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIMerchantItemEntry : MonoBehaviour {

	public Text name;
	public UGUIMerchantItemSlot itemSlot;
	public List<UGUICurrency> currencyDisplays;
	MerchantItem merchantItem;
	
	// Use this for initialization
	void Start () {
	}
	
	public void UpdateMerchantItemData(MerchantItem merchantItem, UGUIMerchantFrame merchantFrame) {
		this.merchantItem = merchantItem;
		itemSlot.UpdateMerchantItemData(merchantItem, merchantFrame);
		name.text = Inventory.Instance.GetItemByTemplateID(merchantItem.itemID).name;
		List<CurrencyDisplay> currencyDisplayList = Inventory.Instance.GenerateCurrencyListFromAmount(merchantItem.purchaseCurrency, merchantItem.cost);
		for (int i = 0; i < currencyDisplays.Count; i++) {
			if (i < currencyDisplayList.Count) {
				currencyDisplays[i].gameObject.SetActive(true);
				currencyDisplays[i].SetCurrencyDisplayData(currencyDisplayList[i]);
			} else {
				currencyDisplays[i].gameObject.SetActive(false);
			}
		}
	}
}
