﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUIMerchantItemSlot : UGUIDraggableSlot {
	
	UGUIMerchantFrame merchantFrame;
	MerchantItem merchantItem;
	bool mouseEntered = false;
	Color defaultColour = Color.white;
	
	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.SourceOnly;
	}
	
	public void UpdateMerchantItemData(MerchantItem merchantItem, UGUIMerchantFrame merchantFrame) {
		this.merchantItem = merchantItem;
		this.merchantFrame = merchantFrame;
		if (merchantItem == null) {
			if (uguiActivatable != null) {
				Destroy (uguiActivatable.gameObject);
			}
		} else {
			if (uguiActivatable == null) {
				uguiActivatable = (UGUIAtavismActivatable) Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
				uguiActivatable.SetActivatable(Inventory.Instance.GetItemByTemplateID(merchantItem.itemID), ActivatableType.Item, this, false);
				defaultColour = uguiActivatable.GetComponent<Image>().color;
			}
			
			uguiActivatable.SetActivatable(Inventory.Instance.GetItemByTemplateID(merchantItem.itemID), ActivatableType.Item, this, false);
			if (merchantItem.count > 0) {
				uguiActivatable.countText.text = merchantItem.count.ToString();
			}
			if (merchantItem.count == 0) {
				uguiActivatable.GetComponent<Image>().color = Color.gray;
			} else {
				uguiActivatable.GetComponent<Image>().color = defaultColour;
			}
			
			// Set background Image - HACK to still show item when it is being dragged
			if (GetComponent<Image>() != null) {
				GetComponent<Image>().sprite = Inventory.Instance.GetItemByTemplateID(merchantItem.itemID).icon;
			}
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		// Do nothing
	}
	
	public override void ClearChildSlot() {
		uguiActivatable = null;
	}
	
	public override void Discarded() {
		
	}
	
	public override void Activate() {
		if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
			if (merchantFrame.ShowPurchaseCountPanel(this))
				return;
		}
		StartPurchase(1);
	}
	
	public void StartPurchase(int count) {
		MerchantItem mItem = new MerchantItem();
		mItem.itemID = merchantItem.itemID;
		mItem.count = count;
		AtavismInventoryItem aiItem = Inventory.Instance.GetItemByTemplateID(merchantItem.itemID);
		string costString = Inventory.Instance.GetCostString(merchantItem.purchaseCurrency, merchantItem.cost * mItem.count);
		string confirmationString = "";
		if (mItem.count == 1)
			confirmationString = "Purchase " + aiItem.name + " for " + costString + "?";
		else
			confirmationString = "Purchase " + aiItem.name + " (x" + mItem.count + ") for " + costString + "?";
		UGUIConfirmationPanel.Instance.ShowConfirmationBox(confirmationString, 
		                                                   mItem, NpcInteraction.Instance.PurchaseItemConfirmed);
	}
	
	void ShowTooltip() {
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && uguiActivatable != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
