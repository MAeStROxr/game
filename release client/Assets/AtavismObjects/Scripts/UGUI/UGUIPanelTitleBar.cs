﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public delegate void OnPanelClose();

public class UGUIPanelTitleBar : MonoBehaviour {

	public Text titleText;
	public bool draggable = false;
	OnPanelClose closeFunction;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SetPanelTitle(string text) {
		titleText.text = text;
	}
	
	public void SetOnPanelClose(OnPanelClose closeFunction) {
		this.closeFunction = closeFunction;
	}
	
	public void Close() {
		if (closeFunction != null) {
			closeFunction();
		} else {
			transform.parent.gameObject.SetActive(false);
		}
	}
}
