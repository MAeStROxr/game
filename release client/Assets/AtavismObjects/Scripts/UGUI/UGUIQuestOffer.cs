﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIQuestOffer : MonoBehaviour {

	public UGUIDialoguePanel dialoguePanel;
	public Text questTitle;
	public Text questDescription;
	public Text questObjective;
	public Text rewardTitle;
	public RectTransform rewardPanel;
	public List<UGUIItemDisplay> itemRewards;
	public Text chooseTitle;
	public RectTransform choosePanel;
	public List<UGUIItemDisplay> chooseRewards;
	public List<UGUICurrency> currency1;
	public List<UGUICurrency> currency2;

	// Use this for initialization
	void Start () {
	
	}
	
	public void UpdateQuestOfferDetails() {
		UpdateQuestOfferDetails(0);
	}
	
	public void UpdateQuestOfferDetails(int questPos) {
		QuestLogEntry selectedQuest = Quests.Instance.GetQuestOfferedInfo(questPos);
	
		questTitle.text = selectedQuest.Title;
		questDescription.text = selectedQuest.Description;
		questObjective.text = selectedQuest.Objective;
		
		// Item Rewards
		for (int i = 0; i < itemRewards.Count; i++) {
			if (i < selectedQuest.gradeInfo[0].rewardItems.Count) {
				itemRewards[i].gameObject.SetActive(true);
				itemRewards[i].SetItemData(selectedQuest.gradeInfo[0].rewardItems[i].item, null);
			} else {
				itemRewards[i].gameObject.SetActive(false);
			}
		}
		if (rewardPanel != null) {
			if (selectedQuest.gradeInfo[0].rewardItems.Count == 0) 
				rewardPanel.gameObject.SetActive(false);
			else
				rewardPanel.gameObject.SetActive(true);
		}
		if (rewardTitle != null) {
			if (selectedQuest.gradeInfo[0].rewardItems.Count == 0 && selectedQuest.gradeInfo[0].currencies.Count == 0) 
				rewardTitle.gameObject.SetActive(false);
			else
				rewardTitle.gameObject.SetActive(true);
		}
		
		// Item Choose Rewards
		for (int i = 0; i < chooseRewards.Count; i++) {
			if (i < selectedQuest.gradeInfo[0].RewardItemsToChoose.Count) {
				chooseRewards[i].gameObject.SetActive(true);
				chooseRewards[i].SetItemData(selectedQuest.gradeInfo[0].RewardItemsToChoose[i].item, null);
			} else {
				chooseRewards[i].gameObject.SetActive(false);
			}
		}
		if (choosePanel != null) {
			if (selectedQuest.gradeInfo[0].RewardItemsToChoose.Count == 0) 
				choosePanel.gameObject.SetActive(false);
			else
				choosePanel.gameObject.SetActive(true);
		}
		if (chooseTitle != null) {
			if (selectedQuest.gradeInfo[0].RewardItemsToChoose.Count == 0) 
				chooseTitle.gameObject.SetActive(false);
			else
				chooseTitle.gameObject.SetActive(true);
		}
		
		// Currency Rewards
		if (selectedQuest.gradeInfo[0].currencies.Count > 0) {
			List<CurrencyDisplay> currencyDisplayList = Inventory.Instance.GenerateCurrencyListFromAmount(selectedQuest.gradeInfo[0].currencies[0].id, 
				                                                                                              selectedQuest.gradeInfo[0].currencies[0].count);
			for (int i = 0; i < currency1.Count; i++) {
				if (i < currencyDisplayList.Count) {
					currency1[i].gameObject.SetActive(true);
					currency1[i].SetCurrencyDisplayData(currencyDisplayList[i]);
				} else {
					currency1[i].gameObject.SetActive(false);
				}
			}
		} else {
			for (int i = 0; i < currency1.Count; i++) {
				currency1[i].gameObject.SetActive(false);
			}
		}
		if (selectedQuest.gradeInfo[0].currencies.Count > 1) {
			List<CurrencyDisplay> currencyDisplayList = Inventory.Instance.GenerateCurrencyListFromAmount(selectedQuest.gradeInfo[0].currencies[1].id, 
			                                                                                              selectedQuest.gradeInfo[0].currencies[1].count);
			for (int i = 0; i < currency2.Count; i++) {
				if (i < currencyDisplayList.Count) {
					currency2[i].gameObject.SetActive(true);
					currency2[i].SetCurrencyDisplayData(currencyDisplayList[i]);
				} else {
					currency2[i].gameObject.SetActive(false);
				}
			}
		} else {
			for (int i = 0; i < currency2.Count; i++) {
				currency2[i].gameObject.SetActive(false);
			}
		}
	}
	
	public void AcceptQuest() {
		Quests.Instance.AcceptQuest(0);
		if (dialoguePanel != null) {
			dialoguePanel.Hide();
		}
	}
	
	public void DeclineQuest() {
		Quests.Instance.DeclineQuest(0);
		if (dialoguePanel != null) {
			dialoguePanel.Hide();
		}
	}
}
