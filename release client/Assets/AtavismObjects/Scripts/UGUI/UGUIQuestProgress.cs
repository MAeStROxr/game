﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUIQuestProgress : MonoBehaviour {

	public UGUIDialoguePanel dialoguePanel;
	public Text questTitle;
	public Text questProgress;
	//public Button continueButton;

	// Use this for initialization
	void Start () {
	
	}
	
	public void UpdateQuestProgressDetails() {
		UpdateQuestProgressDetails(0);
	}
	
	public void UpdateQuestProgressDetails(int questPos) {
		QuestLogEntry selectedQuest = Quests.Instance.GetQuestProgressInfo(questPos);
		
		questTitle.text = selectedQuest.Title;
		questProgress.text = selectedQuest.ProgressText;
		
		// Currently controlled by the UGUIDialoguePanel instead
		/*if (selectedQuest.Complete && continueButton != null) {
			continueButton.interactable = true;
		} else if (continueButton != null) {
			continueButton.interactable = false;
		}*/
	}
	
	public void Continue() {
		if (dialoguePanel != null) {
			dialoguePanel.ShowConcludeQuestPanel();
		}
	}
	
	public void Cancel() {
		if (dialoguePanel != null) {
			dialoguePanel.Hide();
		}
	}
}
