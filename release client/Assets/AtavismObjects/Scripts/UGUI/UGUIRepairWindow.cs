﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UGUIRepairWindow : MonoBehaviour {

    public List<UGUICurrency> costCurrencies;
    public List<UGUIRepairSlot> repairSlots;

    // Use this for initialization
    void Start () {
        AtavismEventSystem.RegisterEvent("REPAIR_COMPLETE", this);
    }

    void OnDestroy()
    {
        AtavismEventSystem.UnregisterEvent("REPAIR_COMPLETE", this);
    }

    void OnDisable()
    {
        foreach (UGUIRepairSlot repairSlot in repairSlots)
        {
            repairSlot.UpdateRepairSlotData(null);
        }
    }

    public void OnEvent(AtavismEventData eData)
    {
        if (eData.eventType == "REPAIR_COMPLETE")
        {
            // Clear slots 
            foreach (UGUIRepairSlot repairSlot in repairSlots)
            {
                repairSlot.UpdateRepairSlotData(null);
            }
        }
    }

    public void RepairListUpdated(UGUIRepairSlot slot)
    {
        //TODO: update currency display
    }

    public void Repair()
    {
        List<AtavismInventoryItem> itemsToRepair = new List<AtavismInventoryItem>();
        foreach(UGUIRepairSlot repairSlot in repairSlots)
        {
            if (repairSlot != null && repairSlot.UguiActivatable != null)
            {
                itemsToRepair.Add((AtavismInventoryItem)repairSlot.UguiActivatable.ActivatableObject);
            }
        }
        NpcInteraction.Instance.RepairItems(itemsToRepair);
    }

    public void RepairAll()
    {
        NpcInteraction.Instance.RepairAllItems();
    }

    public void Toggle()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        } else
        {
            gameObject.SetActive(true);
        }
    }
}
