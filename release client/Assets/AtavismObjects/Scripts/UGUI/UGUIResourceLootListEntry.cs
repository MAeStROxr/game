﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class UGUIResourceLootListEntry : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Text itemNameText;
	public Image itemIcon;
	public Text countText;
	ResourceItem resource;

	// Use this for initialization
	void Start () {
	
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public void LootEntryClicked() {
		Crafting.Instance.LootResource(resource.item);
	}
	
	public void SetResourceLootEntryDetails(ResourceItem resourceItem) {
		this.resource = resourceItem;
		this.itemNameText.text = resource.item.name;
		this.itemIcon.sprite = resource.item.icon;
		if (countText != null) {
			if (resource.count > 1) {
				this.countText.text = resource.count.ToString();
			} else {
				this.countText.text = "";
			}
		}
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		set {
			if (value) {
				resource.item.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}
