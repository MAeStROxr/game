﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUISkillButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler  {

	Skill skill;
	UGUISkillsWindow skillsWindow;
	int pos;

	// Use this for initialization
	void Start () {
	
	}
	
	public void SetSkillData(Skill skill, UGUISkillsWindow skillsWindow, int pos) {
		this.skill = skill;
		GetComponent<Image>().sprite = skill.icon;
		this.skillsWindow = skillsWindow;
		this.pos = pos;
	}
	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (skillsWindow != null) {
			skillsWindow.SelectSkill(pos);
		}
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	void ShowTooltip() {
		UGUITooltip.Instance.SetTitle(skill.skillname);
		UGUITooltip.Instance.SetType("");
		UGUITooltip.Instance.SetDescription("");
		UGUITooltip.Instance.Show(gameObject);
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		set {
			if (value) {
				ShowTooltip();
			} else {
				HideTooltip();
			}
		}
	}
}
