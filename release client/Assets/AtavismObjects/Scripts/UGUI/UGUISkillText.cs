﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UGUISkillText : MonoBehaviour {

	public Text text;
	public int skillID;
	public bool showMax = false;

	// Use this for initialization
	void Start () {
		if (text == null)
			text = GetComponent<Text>();

		AtavismEventSystem.RegisterEvent("SKILL_UPDATE", this);
		
		UpdateSkillText();
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("SKILL_UPDATE", this);
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "SKILL_UPDATE") {
			// Update 
			UpdateSkillText();
		}
	}
	
	void UpdateSkillText() {
		if (showMax && Skills.Instance.PlayerSkills.ContainsKey(skillID)) {
			text.text = "" + Skills.Instance.GetPlayerSkillLevel(skillID) + "/" + Skills.Instance.PlayerSkills[skillID].MaximumLevel;
		} else {
			text.text = "" + Skills.Instance.GetPlayerSkillLevel(skillID);
		}
		
	}
}
