﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UGUITradeSlot : UGUIDraggableSlot {
	
	Button button;
	public Text nameLabel;
	public Text cooldownLabel;
	AtavismInventoryItem item;
	bool mouseEntered = false;
	float cooldownExpiration = -1;
	
	// Use this for initialization
	void Start () {
		slotBehaviour = DraggableBehaviour.Temporary;
	}
	
	public void UpdateTradeSlotData(AtavismInventoryItem item) {
		this.item = item;
		if (item == null) {
			if (uguiActivatable != null) {
				DestroyImmediate (uguiActivatable.gameObject);
			}
			nameLabel.text = "";
		} else {
			if (uguiActivatable == null) {
				uguiActivatable = (UGUIAtavismActivatable) Instantiate(Inventory.Instance.uguiAtavismItemPrefab);
				uguiActivatable.transform.SetParent(transform, false);
				uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			}
			uguiActivatable.SetActivatable(item, ActivatableType.Item, this);
			nameLabel.text = item.name;
		}
	}
	
	public override void OnPointerEnter(PointerEventData eventData) {
		MouseEntered = true;
	}
	
	public override void OnPointerExit(PointerEventData eventData) {
		MouseEntered = false;
	}
	
	public override void OnDrop(PointerEventData eventData) {
		UGUIAtavismActivatable droppedActivatable = eventData.pointerDrag.GetComponent<UGUIAtavismActivatable>();
		
		// Reject any references or non item slots
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Reference || droppedActivatable.Link != null
		    || droppedActivatable.ActivatableType != ActivatableType.Item) {
			return;
		}
		
		if (uguiActivatable != null && uguiActivatable != droppedActivatable) {
			// Delete existing child
			DestroyImmediate(uguiActivatable.gameObject);
			if (backLink != null) {
				backLink.SetLink(null);
			}
		} else if (uguiActivatable == droppedActivatable) {
			droppedOnSelf = true;
		}
		
		// If the source was a temporary slot, clear it
		if (droppedActivatable.Source.SlotBehaviour == DraggableBehaviour.Temporary) {
			droppedActivatable.Source.ClearChildSlot();
			uguiActivatable = droppedActivatable;
			
			uguiActivatable.transform.SetParent(transform, false);
			uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			backLink = droppedActivatable.Source.BackLink;
		} else {
			// Create a duplicate
			uguiActivatable = Instantiate(droppedActivatable);
			uguiActivatable.transform.SetParent(transform, false);
			uguiActivatable.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
			uguiActivatable.GetComponent<CanvasGroup>().blocksRaycasts = true;
			uguiActivatable.SetActivatable(droppedActivatable.ActivatableObject, ActivatableType.Item, this);
			
			// Set the back link
			backLink = droppedActivatable;
			droppedActivatable.SetLink(uguiActivatable);
		}
		
		droppedActivatable.SetDropTarget(this);
		
		AtavismTrade.Instance.ItemPlacedInTradeSlot((AtavismInventoryItem)droppedActivatable.ActivatableObject, slotNum);
	}
	
	public override void ClearChildSlot() {
		if (item == null)
			return;
		uguiActivatable = null;
		AtavismTrade.Instance.ItemPlacedInTradeSlot(null, slotNum);
	}
	
	public override void Discarded() {
		if (droppedOnSelf) {
			droppedOnSelf = false;
			return;
		}
		DestroyImmediate(uguiActivatable.gameObject);
		if (backLink != null) {
			backLink.SetLink(null);
		}
		backLink = null;
		ClearChildSlot();
	}
	
	public override void Activate() {
		// Do nothing
	}
	
	void HideTooltip() {
		UGUITooltip.Instance.Hide();
	}
	
	public bool MouseEntered { 
		get {
			return mouseEntered;
		}
		set {
			mouseEntered = value;
			if (mouseEntered && item != null) {
				uguiActivatable.ShowTooltip(gameObject);
			} else {
				HideTooltip();
			}
		}
	}
}