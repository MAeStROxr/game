﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class UGUITradeWindow : MonoBehaviour {

	public UGUIPanelTitleBar titleBar;
	public Text myName;
	public Text partnerName;
	public List<UGUITradeSlot> myOfferEntries;
	public List<Image> myCurrencyIcons;
	public List<InputField> myCurrencyInput;
	public List<UGUITradeOffer> partnerOfferEntries;
	public List<UGUICurrency> partnerOfferedCurrencies;
	public Text myStatus;
	public Text partnerStatus;
	bool showing = false;
	
	// Use this for initialization
	void Start () {
		if (titleBar != null)
			titleBar.SetOnPanelClose(CancelTrade);
		Hide ();
		AtavismEventSystem.RegisterEvent("TRADE_START", this);
		AtavismEventSystem.RegisterEvent("TRADE_OFFER_UPDATE", this);
		AtavismEventSystem.RegisterEvent("TRADE_COMPLETE", this);
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("TRADE_START", this);
		AtavismEventSystem.UnregisterEvent("TRADE_OFFER_UPDATE", this);
		AtavismEventSystem.UnregisterEvent("TRADE_COMPLETE", this);
	}
	
	public void Show() {
		GetComponent<CanvasGroup>().alpha = 1f;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
		showing = true;
		
		//if (titleBar != null)
		//	titleBar.SetPanelTitle(ClientAPI.GetObjectNode(AtavismTrade.Instance.TradePartnerOid.ToLong()).Name);
		myName.text = ClientAPI.GetPlayerObject().Name;
		partnerName.text = ClientAPI.GetObjectNode(AtavismTrade.Instance.TradePartnerOid.ToLong()).Name;
		
		AtavismTrade.Instance.NewTradeStarted();
		
		// Handle currency
		for (int i = 0; i < myCurrencyInput.Count; i++) {
			myCurrencyInput[i].text = "";
		}
	}
	
	public void Hide() {
		GetComponent<CanvasGroup>().alpha = 0f;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
		showing = false;
		
		// Set all referenced items back to non referenced
		for (int i = 0; i < myOfferEntries.Count; i++) {
			myOfferEntries[i].ResetSlot();
		}
	}
	
	void UpdateTradeWindow() {
		for (int i = 0; i < myOfferEntries.Count; i++) {
			myOfferEntries[i].UpdateTradeSlotData(AtavismTrade.Instance.MyOffers[i]);
		}
		for (int i = 0; i < partnerOfferEntries.Count; i++) {
			partnerOfferEntries[i].UpdateTradeOfferData(AtavismTrade.Instance.TheirOffers[i]);
		}
		
		// Handle currency
		for (int i = 0; i < myCurrencyIcons.Count; i++) {
			if (i < Inventory.Instance.GetMainCurrencies().Count) {
				myCurrencyIcons[i].sprite = Inventory.Instance.GetMainCurrency(i).icon;
			}
		}
		
		for (int i = 0; i < partnerOfferedCurrencies.Count; i++) {
			if (i < AtavismTrade.Instance.TheirCurrencyOffers.Count) {
				partnerOfferedCurrencies[i].gameObject.SetActive(true);
				partnerOfferedCurrencies[i].SetCurrencyDisplayData(AtavismTrade.Instance.TheirCurrencyOffers[i]);
			} else {
				partnerOfferedCurrencies[i].gameObject.SetActive(false);
			}
		}
		
		// If accepted set the colour of the panels
		if (AtavismTrade.Instance.AcceptedByMe) {
			myStatus.text = "Accepted";
		} else {
			myStatus.text = "Pending...";
		}
		
		if (AtavismTrade.Instance.AcceptedByPartner) {
			partnerStatus.text = "Accepted";
		} else {
			partnerStatus.text = "Pending...";
		}
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "TRADE_START") {
			if (!showing)
				Show ();
			UpdateTradeWindow();
		} else if (eData.eventType == "TRADE_OFFER_UPDATE") {
			UpdateTradeWindow();
		} else if (eData.eventType == "TRADE_COMPLETE") {
			Hide ();
		}
	}
	
	/// <summary>
	/// Updates the currency amount for the first "main" currency
	/// </summary>
	/// <param name="currencyAmount">Currency amount.</param>
	public void SetCurrency1(string currencyAmount) {
		AtavismTrade.Instance.SetCurrencyAmount(Inventory.Instance.GetMainCurrency(0).id, int.Parse(currencyAmount));
	}
	
	/// <summary>
	/// Updates the currency amount for the first "main" currency
	/// </summary>
	/// <param name="currencyAmount">Currency amount.</param>
	public void SetCurrency2(string currencyAmount) {
		AtavismTrade.Instance.SetCurrencyAmount(Inventory.Instance.GetMainCurrency(1).id, int.Parse(currencyAmount));
	}
	
	/// <summary>
	/// Updates the currency amount for the first "main" currency
	/// </summary>
	/// <param name="currencyAmount">Currency amount.</param>
	public void SetCurrency3(string currencyAmount) {
		AtavismTrade.Instance.SetCurrencyAmount(Inventory.Instance.GetMainCurrency(2).id, int.Parse(currencyAmount));
	}
	
	bool checkCurrency() {
		List<Vector2> currencies = new List<Vector2>();
		foreach (string currencyID in AtavismTrade.Instance.MyCurrencyOffers.Keys) {
			currencies.Add(new Vector2(int.Parse(currencyID), AtavismTrade.Instance.MyCurrencyOffers[currencyID]));
		}
		
		if (Inventory.Instance.DoesPlayerHaveEnoughCurrency(currencies)) {
			Debug.Log("Player does have enough currency");
			return true;
		}
		Debug.Log("Player does not have enough currency");
		return false;
	}
	
	public void AcceptTrade() {
		AtavismTrade.Instance.AcceptTrade();
	}
	
	public void CancelTrade() {
		AtavismTrade.Instance.CancelTrade();
		Hide();
	}
}
