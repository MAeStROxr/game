﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldBuilderUI : AtavismWindowTemplate {
	
	private Claim activeClaim = null;
	private Claim newClaim = null;
	
	public KeyCode toggleButton;
	
	
	Vector2 scrollPosition = Vector2.zero;
	string playerName = "";
	int permissionLevel = 1;
	
	public GUISkin skin2;
	public GUISkin bar;

	// Use this for initialization
	void Start () {
		SetupRect();
	
		Dictionary<string, object> props = new Dictionary<string, object> ();
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.GET_RESOURCES", props);
	}
	
	void OnGUI() {
		if (ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject != null) {
			ClaimObject selectedObject = ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject;
			if (selectedObject.MaxHealth > 0) {
				float barLength = 150;
				float healthBarLength = ((float)selectedObject.Health / (float)selectedObject.MaxHealth) * barLength;
				Rect uiRect = new Rect(Screen.width / 2 - (barLength / 2), 100, barLength, 10);
			
				GUI.skin = skin2;
				GUI.Box (new Rect (uiRect.x, uiRect.y, barLength, 10), "");
			
				GUI.skin = bar;
				GUI.color = Color.red;
				GUI.Box (new Rect (uiRect.x, uiRect.y, healthBarLength, 10), "");
				GUI.color = Color.white;
				GUI.Label(new Rect(uiRect.x, uiRect.y, barLength, 25),  + selectedObject.Health + "/" + selectedObject.MaxHealth);
			
				if (selectedObject.Complete) {
					Rect buttonRect = new Rect(Screen.width / 2 - 16, 115, 32, 32);
					if (GUI.Button(buttonRect, AtavismCursor.Instance.attackCursor)) {
						ClientAPI.ScriptObject.GetComponent<WorldBuilder>().AttackClaimObject(selectedObject.gameObject);
					}
				}
			}
		}
	
		if (!open)
			return; 
		
		GUI.depth = uiLayer;
		GUI.skin = skin;
		
		if (GetBuildingState() == WorldBuildingState.Standard || GetBuildingState() == WorldBuildingState.PlaceItem || 
		    GetBuildingState() == WorldBuildingState.SelectItem) {
			DrawClaimControlWindow();
		} else if (GetBuildingState() == WorldBuildingState.CreateClaim) {
			DrawCreateClaimWindow();
		} else if (GetBuildingState() == WorldBuildingState.EditItem || GetBuildingState() == WorldBuildingState.MoveItem) {
			DrawEditItemWindow();
		} else if (GetBuildingState() == WorldBuildingState.CreateObject) {
			DrawCreateObjectWindow();
		} else if (GetBuildingState() == WorldBuildingState.Admin) {
			DrawClaimPermissionsWindow();
		} else if (GetBuildingState() == WorldBuildingState.SellClaim) {
			DrawSellClaimWindow();
		}
	}
	
	void DrawClaimControlWindow() {
		GUI.Box(uiRect, "");
		GUILayout.BeginArea (uiRect, skin.GetStyle ("Window"));
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("World Builder");
		if (GUILayout.Button("x", GUILayout.Width(20))) {
			ToggleOpen();
		}
		GUILayout.EndHorizontal();
		
		// If no active claim, show nothing
		if (activeClaim == null) {
			GUILayout.Label("Active Claim: None");
			GUILayout.FlexibleSpace();
			// Check if user is an admin
			int adminLevel = (int) ClientAPI.GetObjectProperty(ClientAPI.GetPlayerOid(), "adminLevel");
			if (adminLevel == 5) {
				GUILayout.Label("Claim Admin:");
				if (GUILayout.Button("Create claim", GUILayout.Width(120))) {
					SetBuildingState(WorldBuildingState.CreateClaim);
					newClaim = new Claim();
				}
			}
		} else {
			GUILayout.Label("Active Claim: " + activeClaim.name);
			if (activeClaim.playerOwned || activeClaim.permissionlevel > 0) {
				// If the player owns the claim, show a sell button
				/*if (GUILayout.Button("Sell", GUILayout.Width(80))) {
					// Do stuff
				}*/
				// Place Item Button
				if (GUILayout.Button("Place Item", GUILayout.Width(120))) {
					SetBuildingState(WorldBuildingState.PlaceItem);
					// Open the main bag
					gameObject.GetComponent<BagBar>().OpenBag(0);
					// Set WorldBuilder State
					AtavismCursor.Instance.SetItemClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
				}
				// Select object
				if (GUILayout.Button("Create Object", GUILayout.Width(120))) {
					SetBuildingState(WorldBuildingState.CreateObject);
					AtavismCursor.Instance.ClearItemClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
				}
				// Select object
				//if (activeClaim.permissionlevel >= WorldBuilder.PERMISSION_ADD_DELETE) {
					if (GUILayout.Button("Select Object", GUILayout.Width(120))) {
						SetBuildingState(WorldBuildingState.SelectItem);
						AtavismCursor.Instance.ClearItemClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
						WorldBuilderInterface.Instance.StartSelectObject();
					}
				//}
				
				if (activeClaim.permissionlevel >= WorldBuilder.PERMISSION_ADD_USERS || activeClaim.playerOwned) {
					GUILayout.Label("Claim Admin:");
					if (GUILayout.Button("Permissions", GUILayout.Width(100))) {
						SetBuildingState(WorldBuildingState.Admin);
						playerName = "";
						permissionLevel = 1;
					}
				}
				
				GUILayout.BeginHorizontal();
				if (activeClaim.playerOwned) {
					if (GUILayout.Button("Sell", GUILayout.Width(60))) {
						SetBuildingState(WorldBuildingState.SellClaim);
					}
				}
				GUILayout.FlexibleSpace();
				if (activeClaim.playerOwned) {
					if (GUILayout.Button("Delete", GUILayout.Width(80))) {
						string message = "Are you sure you want to delete your claim: " + activeClaim.name;
						AtavismUIManager.Instance.ShowConfirmationBox(message, null, ConfirmedDeleteClaim);
					}
				}
				GUILayout.EndHorizontal();
			} else if (activeClaim.forSale) {
				// if not player owned, show purchase button if for sale
				GUILayout.Label("Claim is for sale!");
				string currencyName = ClientAPI.ScriptObject.GetComponent<Inventory>().GetCurrencyName(activeClaim.currency);
				GUILayout.Label("Cost: " + activeClaim.cost + " " + currencyName);
				if (GUILayout.Button("Buy")) {
					Dictionary<string, object> props = new Dictionary<string, object> ();
					props.Add("claimID", activeClaim.id);
					NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.PURCHASE_CLAIM", props);
					//ClientAPI.Write("Sent buy claim message");
				}
			} else {
				GUILayout.Label("Claim is not for sale.");
				GUILayout.FlexibleSpace();
				int adminLevel = (int) ClientAPI.GetObjectProperty(ClientAPI.GetPlayerOid(), "adminLevel");
				if (adminLevel == 5) {
					GUILayout.Label("Claim Admin:");
					if (GUILayout.Button("Delete", GUILayout.Width(80))) {
						string message = "Are you sure you want to delete your claim: " + activeClaim.name;
						AtavismUIManager.Instance.ShowConfirmationBox(message, null, ConfirmedDeleteClaim);
					}
				}
			}
		}
		
		GUILayout.EndArea();
	}
	
	void DrawEditItemWindow() {
		if (ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject == null) {
			SetBuildingState(WorldBuildingState.Standard);
			WorldBuilderInterface.Instance.ClearCurrentReticle(false);
			GUILayout.EndArea();
			return;
		}
		
		GUI.Box(uiRect, "");
		GUILayout.BeginArea (uiRect, skin.GetStyle ("Window"));
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Edit Claim Object");
		if (GUILayout.Button("x", GUILayout.Width(20))) {
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Name: " + ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.name);
		GUILayout.EndHorizontal();
		
		if(GetBuildingState() == WorldBuildingState.MoveItem) {
			if (WorldBuilderInterface.Instance.MouseWheelBuildMode == MouseWheelBuildMode.MoveVertical) {
				if (GUILayout.Button("Rotate Object", GUILayout.Width(120))) {
					WorldBuilderInterface.Instance.MouseWheelBuildMode = MouseWheelBuildMode.Rotate;
				}
			} else {
				if (GUILayout.Button("Move Vertically", GUILayout.Width(120))) {
					WorldBuilderInterface.Instance.MouseWheelBuildMode = MouseWheelBuildMode.MoveVertical;
				}
			}
		} else {
			if (GUILayout.Button("Move", GUILayout.Width(120))) {
				WorldBuilderInterface.Instance.SetCurrentReticle(ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.gameObject);
				SetBuildingState(WorldBuildingState.MoveItem);
			}
		}
		if (GUILayout.Button("Remove", GUILayout.Width(120))) {
			Dictionary<string, object> props = new Dictionary<string, object>();
			props.Add("action", "convert");
			props.Add("claimID", activeClaim.id);
			props.Add("objectID", ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.ID);
			NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.EDIT_CLAIM_OBJECT", props);
			SetBuildingState(WorldBuildingState.Standard);
			WorldBuilderInterface.Instance.ClearCurrentReticle(false);
		}
		
		
		if (ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.ItemReqs.Count > 0) {
			// Show the items required to improve the item
			GUILayout.Label("Items to improve:");
			foreach(int itemID in ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.ItemReqs.Keys) {
				AtavismInventoryItem item = ClientAPI.ScriptObject.GetComponent<Inventory>().GetItemByTemplateID(itemID);
				GUILayout.BeginHorizontal ();
				if (GUILayout.Button("", GUILayout.Width(32), GUILayout.Height(32))) {
					if (AtavismCursor.Instance.CursorHasItem() && AtavismCursor.Instance.GetCursorItem().templateId == item.templateId) {
						ClientAPI.ScriptObject.GetComponent<WorldBuilder>().ImproveBuildObject(ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.gameObject, 
							AtavismCursor.Instance.GetCursorItem(), 1);
						AtavismCursor.Instance.ResetCursor();
					}
				}
				GUILayout.Button(item.icon.texture, GUILayout.Width(32), GUILayout.Height(32));
				GUILayout.Label(item.name + " x" + ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.ItemReqs[itemID]);
				GUILayout.EndHorizontal();
			}
		}
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Save", GUILayout.Width(120))) {
			Dictionary<string, object> props = new Dictionary<string, object>();
			props.Add("action", "save");
			props.Add("claimID", activeClaim.id);
			props.Add("objectID", ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.ID);
			props.Add("loc", ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.transform.position);
			props.Add("orient", ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SelectedObject.transform.rotation);
			NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.EDIT_CLAIM_OBJECT", props);
			SetBuildingState(WorldBuildingState.Standard);
			WorldBuilderInterface.Instance.ClearCurrentReticle(false);
		}
		
		GUILayout.EndArea();
	}
	
	void DrawCreateClaimWindow() {
		GUI.Box(uiRect, "");
		GUILayout.BeginArea (uiRect, skin.GetStyle ("Window"));
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Create Claim");
		if (GUILayout.Button("x", GUILayout.Width(20))) {
			ToggleOpen();
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Name:");
		newClaim.name = GUILayout.TextField(newClaim.name, GUILayout.Width(100));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Size:");
		newClaim.area = int.Parse(GUILayout.TextField("" + newClaim.area));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		newClaim.playerOwned = GUILayout.Toggle(newClaim.playerOwned, "Owned");
		newClaim.forSale = GUILayout.Toggle(newClaim.forSale, "For Sale");
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Cost:");
		newClaim.cost = int.Parse(GUILayout.TextField("" + newClaim.cost));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Currency:");
		newClaim.currency = int.Parse(GUILayout.TextField("" + newClaim.currency));
		GUILayout.EndHorizontal();
		if (GUILayout.Button("Place Claim Here")) {
			Dictionary<string, object> props = new Dictionary<string, object> ();
			props.Add("loc", ClientAPI.GetPlayerObject().Position);
			props.Add("name", newClaim.name);
			props.Add("size", newClaim.area);
			props.Add("owned", newClaim.playerOwned);
			props.Add("forSale", newClaim.forSale);
			props.Add("cost", newClaim.cost);
			props.Add("currency", newClaim.currency);
			NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.CREATE_CLAIM", props);
			ClientAPI.Write("Sent create claim message");
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndArea();
	}
	
	void DrawCreateObjectWindow() {
		GUI.Box(uiRect, "");
		GUILayout.BeginArea (uiRect, skin.GetStyle ("Window"));
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Create Object");
		if (GUILayout.Button("x", GUILayout.Width(20))) {
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndHorizontal();
		scrollPosition = GUILayout.BeginScrollView(scrollPosition, skin.GetStyle("Window"), GUILayout.Width (width-20));
		foreach (AtavismBuildObjectTemplate tmpl in ClientAPI.ScriptObject.GetComponent<WorldBuilder>().BuildObjectTemplates.Values) {
			if (tmpl.onlyAvailableFromItem)
				continue;
			GUILayout.BeginHorizontal ();
			if (GUILayout.Button(tmpl.icon.texture, GUILayout.Width(32), GUILayout.Height(32))) {
				WorldBuilderInterface.Instance.StartPlaceBuildObject(tmpl.id);
			}
			GUILayout.Label(tmpl.buildObjectName);
			GUILayout.EndHorizontal();
		}
		GUILayout.EndScrollView();
		GUILayout.EndArea();
	}
	
	void DrawClaimPermissionsWindow() {
		GUI.Box(uiRect, "");
		GUILayout.BeginArea (uiRect, skin.GetStyle ("Window"));
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Claim Permissions");
		if (GUILayout.Button("x", GUILayout.Width(20))) {
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndHorizontal();
		GUILayout.Label("Permissions:");
		string[] levels = new string[] {"Add Objects", "Edit Objects", "Add Users", "Manage Users"};
		scrollPosition = GUILayout.BeginScrollView(scrollPosition, skin.GetStyle("Window"), GUILayout.Width (180));
		foreach (ClaimPermission per in ClientAPI.ScriptObject.GetComponent<WorldBuilder>().ActiveClaim.permissions) {
			GUILayout.BeginHorizontal();
			GUILayout.Label(per.playerName + " (" + levels[per.permissionLevel-1] + ")");
			if (activeClaim.playerOwned || activeClaim.permissionlevel >= WorldBuilder.PERMISSION_MANAGE_USERS) {
				if (GUILayout.Button("Delete")) {
					ClientAPI.ScriptObject.GetComponent<WorldBuilder>().RemovePermission(per.playerName);
				}
			}
			GUILayout.EndHorizontal();
		}
		GUILayout.EndScrollView();
		GUILayout.Label("Add User Permission:");
		GUILayout.BeginHorizontal ();
		GUILayout.Label("Player Name:");
		playerName = GUILayout.TextField(playerName, GUILayout.Width(90));
		GUILayout.EndHorizontal();
		GUILayout.Label ("Rights: " + levels[permissionLevel-1]);
		int maxLevel = activeClaim.permissionlevel-1;
		if (activeClaim.playerOwned)
			maxLevel = 4;
		permissionLevel = (int)GUILayout.HorizontalSlider((float)permissionLevel, 1, maxLevel);
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button("Add")) {
			ClientAPI.ScriptObject.GetComponent<WorldBuilder>().AddPermission(playerName, permissionLevel);
		}
		if (GUILayout.Button("Back")) {
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}
	
	void DrawSellClaimWindow() {
		GUI.Box(uiRect, "");
		GUILayout.BeginArea (uiRect, skin.GetStyle ("Window"));
		GUILayout.BeginHorizontal ();
		GUILayout.Label ("Sell Claim");
		if (GUILayout.Button("x", GUILayout.Width(20))) {
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Name: " + activeClaim.name);
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Size: " + activeClaim.area);
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		activeClaim.forSale = GUILayout.Toggle(activeClaim.forSale, "For Sale");
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Cost:");
		activeClaim.cost = int.Parse(GUILayout.TextField("" + activeClaim.cost));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Currency:");
		activeClaim.currency = int.Parse(GUILayout.TextField("" + activeClaim.currency));
		GUILayout.EndHorizontal();
		if (GUILayout.Button("Save")) {
			ClientAPI.ScriptObject.GetComponent<WorldBuilder>().SendEditClaim();
			SetBuildingState(WorldBuildingState.Standard);
		}
		if (GUILayout.Button("Back")) {
			SetBuildingState(WorldBuildingState.Standard);
		}
		GUILayout.EndArea();
	}
	
	public void ConfirmedDeleteClaim(object obj, bool accepted) {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", activeClaim.id);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.DELETE_CLAIM", props);
		AtavismCursor.Instance.ClearItemClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
	}
	
	public void ToggleOpen() {
		open = !open;
		if (open) {
			AtavismUiSystem.AddFrame(frameName, uiRect);
			SetBuildingState(WorldBuildingState.Standard);
			ClientAPI.ScriptObject.GetComponent<WorldBuilder>().ShowClaims = true;
			
		} else {
			AtavismUiSystem.RemoveFrame(frameName, new Rect(0, 0, 0, 0));
			SetBuildingState(WorldBuildingState.None);
			ClientAPI.ScriptObject.GetComponent<WorldBuilder>().ShowClaims = false;
			WorldBuilderInterface.Instance.ClearCurrentReticle(true);
			AtavismCursor.Instance.ClearItemClickedOverride(WorldBuilder.Instance.StartPlaceClaimObject);
		}
	}
	
	/// <summary>
	/// Gets the BuildingState from the World Builder
	/// </summary>
	/// <returns>The building state.</returns>
	WorldBuildingState GetBuildingState() {
		return WorldBuilder.Instance.BuildingState;
	}
	
	/// <summary>
	/// Tells the WorldBuilder to update the BuildingState
	/// </summary>
	/// <param name="newState">New state.</param>
	void SetBuildingState(WorldBuildingState newState) {
		WorldBuilder.Instance.BuildingState = newState;
	}
}
