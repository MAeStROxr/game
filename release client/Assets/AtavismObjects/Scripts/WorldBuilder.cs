﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClaimPermission {
	public OID playerOid;
	public string playerName;
	public int permissionLevel;
}

public class Claim {
	public int id;
	public string name = "";
	public string ownerName = "";
	public Vector3 loc;
	public int area = 30;
	public bool playerOwned;
	public bool forSale;
	public int cost;
	public int currency = 1;
	public int permissionlevel = 0;
	public Dictionary<int, int> resources = new Dictionary<int, int>();
	public Dictionary<int, ClaimObject> claimObjects = new Dictionary<int, ClaimObject>();
	public List<ClaimPermission> permissions = new List<ClaimPermission>();
}

public enum WorldBuildingState {
	PlaceItem,
	SelectItem,
	EditItem,
	MoveItem,
	SellClaim,
	PurchaseClaim,
	CreateClaim,
	CreateObject,
	Standard,
	Admin,
	None
}

public struct ClaimObjectData {
	public int objectID; 
	public int templateID;
	public int claimID; 
	public string prefabName; 
	public Vector3 loc; 
	public Quaternion orient;
	public string state;
	public int health;
	public int maxHealth;
	public bool complete;
}

public class WorldBuilder : MonoBehaviour {

	static WorldBuilder instance;
	
	Dictionary<int, AtavismBuildObjectTemplate> buildObjectTemplates;
	
	WorldBuildingState buildingState = WorldBuildingState.None;
	private List<Claim> claims = new List<Claim>();
	private Claim activeClaim = null;
	bool showClaims = false;
	List<GameObject> claimGameObjects = new List<GameObject>();
	Dictionary<int, int> buildingResources = new Dictionary<int, int>();
	ClaimObject selectedObject;
	int selectedID = -1;
	
	List<ClaimObjectData> objectsToLoad = new List<ClaimObjectData>();
	int frameCount = 0;

	// Use this for initialization
	void Start () {
		if (instance != null) {
			return;
		}
		instance = this;
		
		AtavismEventSystem.RegisterEvent("CLAIM_ADDED", this);
		AtavismEventSystem.RegisterEvent("CLAIMED_REMOVED", this);
	
		// Register for messages relating to the claim system
		NetworkAPI.RegisterExtensionMessageHandler("claim_data", ClaimIDMessage);
		NetworkAPI.RegisterExtensionMessageHandler("remove_claim_data", ClaimRemoveDataMessage);
		NetworkAPI.RegisterExtensionMessageHandler("claim_updated", ClaimUpdatedMessage);
		NetworkAPI.RegisterExtensionMessageHandler("remove_claim", RemoveClaimMessage);
		NetworkAPI.RegisterExtensionMessageHandler("claim_deleted", RemoveClaimMessage);
		NetworkAPI.RegisterExtensionMessageHandler("claim_made", ClaimMadeMessage);
		
		NetworkAPI.RegisterExtensionMessageHandler("claim_object", ClaimObjectMessage);
		NetworkAPI.RegisterExtensionMessageHandler("claim_object_bulk", ClaimObjectBulkMessage);
		NetworkAPI.RegisterExtensionMessageHandler("move_claim_object", MoveClaimObjectMessage);
		NetworkAPI.RegisterExtensionMessageHandler("update_claim_object_state", UpdateClaimObjectStateMessage);
		NetworkAPI.RegisterExtensionMessageHandler("claim_object_info", ClaimObjectInfoMessage);
		NetworkAPI.RegisterExtensionMessageHandler("remove_claim_object", RemoveClaimObjectMessage);
		NetworkAPI.RegisterExtensionMessageHandler("buildingResources", HandleBuildingResources);
		NetworkAPI.RegisterExtensionMessageHandler("start_build_object", HandleStartBuildObject);
		NetworkAPI.RegisterExtensionMessageHandler("start_build_task", HandleStartBuildTask);
		NetworkAPI.RegisterExtensionMessageHandler("build_task_interrupted", HandleInterruptBuildTask);
		
		// Load in items
		buildObjectTemplates = new Dictionary<int, AtavismBuildObjectTemplate>();
		Object[] prefabs = Resources.LoadAll("Content/BuildObjects");
		foreach (Object displayPrefab in prefabs) {
			GameObject go = (GameObject) displayPrefab;
			AtavismBuildObjectTemplate displayData = go.GetComponent<AtavismBuildObjectTemplate>();
            //if (displayData == null) continue;
            if (!buildObjectTemplates.ContainsKey(displayData.id) && displayData.id > 0
					&& !displayData.onlyAvailableFromItem) {
				buildObjectTemplates.Add(displayData.id, displayData);
			}
		}
	}
	
	void OnDestroy() {
		AtavismEventSystem.UnregisterEvent("CLAIM_ADDED", this);
		AtavismEventSystem.UnregisterEvent("CLAIMED_REMOVED", this);
	}
	
	// Update is called once per frame
	void Update () {
		if (activeClaim == null /*&& buildingState != WorldBuildingState.None*/) {
			// Check against claims to see if a region has been entered
			foreach (Claim claim in claims) {
				if (InsideClaimArea(claim, ClientAPI.GetPlayerObject().Position)) {
					activeClaim = claim;
					// dispatch a ui event to tell the rest of the system
					string[] args = new string[1];
					AtavismEventSystem.DispatchEvent("CLAIM_CHANGED", args);
					break;
				}
			}
		} else /*if (buildingState != WorldBuildingState.None) */{
			// Check if the player has left the claim
			if (!InsideClaimArea(activeClaim, ClientAPI.GetPlayerObject().Position)) {
				activeClaim = null;
				// dispatch a ui event to tell the rest of the system
				string[] args = new string[1];
				AtavismEventSystem.DispatchEvent("CLAIM_CHANGED", args);
			}
		}
		
		// Check distance for selected object - if too far away, unselect it
		if (selectedObject != null && buildingState != WorldBuildingState.MoveItem)
		{
			if (Vector3.Distance(ClientAPI.GetPlayerObject().Position, selectedObject.transform.position) > 6)
			{
				SelectedObject = null;
			}
		}
		
		if (frameCount == 3 && objectsToLoad.Count > 0) {
			SpawnClaimObject(objectsToLoad[0]);
			objectsToLoad.RemoveAt(0);
		}
		frameCount++;
		if (frameCount > 3)
			frameCount = 0;
	}
	
	public void OnEvent(AtavismEventData eData) {
		if (eData.eventType == "CLAIM_ADDED") {
			GameObject claim = GameObject.Find(eData.eventArgs[0]);
			claimGameObjects.Add(claim);
			claim.SetActive(showClaims);
		} else if (eData.eventType == "CLAIM_REMOVED") {
			GameObject claim = GameObject.Find(eData.eventArgs[0]);
			claimGameObjects.Remove(claim);
		}
	}
	
	public bool InsideClaimArea (Claim claim, Vector3 point)
	{
		if (InRange (point.x, claim.loc.x - claim.area / 2, claim.loc.x + claim.area / 2) && 
		    InRange (point.z, claim.loc.z - claim.area / 2, claim.loc.z + claim.area / 2)) {
			return true;
		}
		return false;
	}
	
	bool InRange (float val, float min, float max)
	{
		return ((val >= min) && (val <= max));
	}
	
	public Claim GetClaim(int claimID) {
		foreach (Claim claim in claims) {
			if (claim.id == claimID)
				return claim;
		}
		return null;
	}
	
	
	#region Message Senders
	public void CreateClaim(string name, int size, bool playerOwned, bool forSale, int currencyID, int cost) {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("loc", ClientAPI.GetPlayerObject().Position);
		props.Add("name", name);
		props.Add("size", size);
		props.Add("owned", playerOwned);
		props.Add("forSale", forSale);
		props.Add("cost", cost);
		props.Add("currency", currencyID);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.CREATE_CLAIM", props);
	}
	
	public void AddPermission(string playerName, int permissionLevel) {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", activeClaim.id);
		props.Add("playerName", playerName);
		props.Add("action", "Add");
		props.Add("permissionLevel", permissionLevel);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.CLAIM_PERMISSION", props);
		//ClientAPI.Write("Sent add permission message");
	}
	
	public void RemovePermission(string playerName) {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", activeClaim.id);
		props.Add("playerName", playerName);
		props.Add("action", "Remove");
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.CLAIM_PERMISSION", props);
		//ClientAPI.Write("Sent remove permission message");
	}
	
	public void SendEditClaim() {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", activeClaim.id);
		props.Add("name", activeClaim.name);
		props.Add("forSale", activeClaim.forSale);
		props.Add("cost", activeClaim.cost);
		props.Add("currency", activeClaim.currency);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.EDIT_CLAIM", props);
		//ClientAPI.Write("Sent edit claim message");
	}
	
	public void PurchaseClaim() {
		Dictionary<string, object> props = new Dictionary<string, object> ();
		props.Add("claimID", activeClaim.id);
		NetworkAPI.SendExtensionMessage (ClientAPI.GetPlayerOid (), false, "voxel.PURCHASE_CLAIM", props);
		//ClientAPI.Write("Sent purchase claim message");
	}
	
	public void SendPlaceClaimObject(int buildObjectID, AtavismInventoryItem item, Transform transform) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("claim", activeClaim.id);
		props.Add("loc", transform.position);
		props.Add("orient", transform.rotation);
		
		if (item != null) {
			props.Add("buildObjectTemplateID", buildObjectID);
			props.Add("itemID", item.templateId);
			props.Add("itemOID", item.ItemId);
		} else {
			props.Add("buildObjectTemplateID", buildObjectID);
			props.Add("itemID", -1);
			props.Add("itemOID", null);
		}
		
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.PLACE_CLAIM_OBJECT", props);
	}
	
	public void RequestClaimObjectInfo(int claimObjectID) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("claimID", activeClaim.id);
		props.Add("objectID", claimObjectID);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.GET_CLAIM_OBJECT_INFO", props);
	}
	
	public void SendEditObjectPosition(GameObject objectBeingEdited) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("action", "save");
		props.Add("claimID", activeClaim.id);
		props.Add("objectID", objectBeingEdited.GetComponent<ClaimObject>().ID);
		props.Add("loc", objectBeingEdited.transform.position);
		props.Add("orient", objectBeingEdited.transform.rotation);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.EDIT_CLAIM_OBJECT", props);
	}
	
	public void ImproveBuildObject(GameObject objectBeingEdited, AtavismInventoryItem item, int count) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("claimID", activeClaim.id);
		props.Add("objectID", objectBeingEdited.GetComponent<ClaimObject>().ID);
		props.Add("itemID", item.templateId);
		props.Add("itemOID", item.ItemId);
		props.Add("count", count);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.UPGRADE_BUILDING_OBJECT", props);
	}
	
	public void PickupClaimObject() {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("action", "convert");
		props.Add("claimID", activeClaim.id);
		props.Add("objectID", SelectedObject.ID);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.EDIT_CLAIM_OBJECT", props);
		BuildingState = WorldBuildingState.Standard;
		WorldBuilderInterface.Instance.ClearCurrentReticle(false);
	}
	#endregion Message Senders
	
	public void StartPlaceClaimObject(UGUIAtavismActivatable activatable) {
		AtavismInventoryItem item = (AtavismInventoryItem)activatable.ActivatableObject;
		StartPlaceClaimObject(item);
	}
	
	public void StartPlaceClaimObject(AtavismInventoryItem item) {
		string[] args = new string[1];
		args[0] = item.ItemId.ToString();
		AtavismEventSystem.DispatchEvent("PLACE_CLAIM_OBJECT", args);
	}
	
	void SpawnClaimObject(ClaimObjectData objectData) {
		// Add the gameObject to the claim
		Claim claim = GetClaim(objectData.claimID);
		if (claim == null) {
			AtavismLogger.LogWarning("No Claim found for Claim Object");
			return;
		}
		
		if (claim.claimObjects.ContainsKey(objectData.objectID)) {
			return;
		}
		// Spawn the object in the world
		string prefabName = objectData.prefabName;
		int resourcePathPos = prefabName.IndexOf("Resources/");
		prefabName = prefabName.Substring(resourcePathPos + 10);
		prefabName = prefabName.Remove(prefabName.Length - 7);
		GameObject prefab = (GameObject)Resources.Load(prefabName);
		GameObject claimObject = (GameObject)UnityEngine.Object.Instantiate(prefab, objectData.loc + claim.loc, objectData.orient);
		// Get the Claim Object Component
		ClaimObject cObject = claimObject.GetComponent<ClaimObject>();
		if (cObject == null) {
			cObject = claimObject.GetComponentInChildren<ClaimObject>();
			if (cObject == null) {
				cObject = claimObject.AddComponent<ClaimObject>();
			}
		}
		cObject.ClaimID = objectData.claimID;
		cObject.StateUpdated(objectData.state);
		cObject.ID = objectData.objectID;
		cObject.TemplateID = objectData.templateID;
		cObject.Health = objectData.health;
		cObject.MaxHealth = objectData.maxHealth;
		cObject.Complete = objectData.complete;
		
		claim.claimObjects.Add(objectData.objectID, cObject);
		
		if (cObject.ID == selectedID) {
			SelectedObject = cObject;
		}
	}
	
	public void AttackClaimObject(GameObject obj) {
		Dictionary<string, object> props = new Dictionary<string, object>();
		props.Add("claimID", obj.GetComponent<ClaimObject>().ClaimID);
		props.Add("objectID", obj.GetComponent<ClaimObject>().ID);
		NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "voxel.ATTACK_BUILDING_OBJECT", props);
		//ClientAPI.Write("Sent attack claim object");
	}
	
	public List<AtavismBuildObjectTemplate> GetBuildObjectsOfCategory(int category) {
		List<AtavismBuildObjectTemplate> buildObjects = new List<AtavismBuildObjectTemplate>();
		foreach (AtavismBuildObjectTemplate template in buildObjectTemplates.Values) {
			if (template.category == category || category == -1) 
				buildObjects.Add(template);
		}
		
		return buildObjects;
	}
	
	#region Message Handlers
	
	/// <summary>
	/// Handles the Claim Action Message from the server. Passes the data onto the voxel editor.
	/// </summary>
	/// <param name="props">Properties.</param>
	public void ClaimObjectMessage(Dictionary<string, object> props) {
		int objectID = (int)props["id"];
		int templateID = (int)props["templateID"];
		string prefabName = (string)props["gameObject"];
		Vector3 loc = (Vector3)props["loc"];
		Quaternion orient = (Quaternion)props["orient"];
		int claimID = (int)props["claimID"];
		//string state = (string)props["state"];
		
		AtavismLogger.LogDebugMessage("Got claim object: " + gameObject);
		//SpawnClaimObject(objectID, claimID, prefabName, loc, orient);
		ClaimObjectData objectData = new ClaimObjectData();
		objectData.objectID = objectID;
		objectData.claimID = claimID;
		objectData.templateID = templateID;
		objectData.prefabName = prefabName;
		objectData.loc = loc;
		objectData.orient = orient;
		objectData.state = "";
		objectData.health = (int)props["health"];
		objectData.maxHealth = (int)props["maxHealth"];
		objectData.complete = (bool)props["complete"];
		objectsToLoad.Add(objectData);
	}
	
	/// <summary>
	/// Handles the Claim Action Bulk Message which is used to transfer large amounts of actions at once.
	/// </summary>
	/// <param name="props">Properties.</param>
	public void ClaimObjectBulkMessage(Dictionary<string, object> props) {
		int numObjects = (int)props["numObjects"];
		AtavismLogger.LogDebugMessage("Got numObjects: " + numObjects);
		for (int i = 0; i < numObjects; i++) {
			string actionString = (string)props["object_" + i];
			string[] actionData = actionString.Split(';');
			string objectID = actionData[0];
			string templateID = actionData[1];
			string gameObject = actionData[2];
			string[] locData = actionData[3].Split(',');
			Vector3 loc = new Vector3(float.Parse(locData[0]), float.Parse(locData[1]), float.Parse(locData[2]));
			string[] normalData = actionData[4].Split(',');
			Quaternion orient = new Quaternion(float.Parse(normalData[0]), float.Parse(normalData[1]), float.Parse(normalData[2]), float.Parse(normalData[3]));
			string state = actionData[5];
			int health = int.Parse(actionData[6]);
			int maxHealth = int.Parse(actionData[7]);
			bool complete = bool.Parse(actionData[8]);
			//SpawnClaimObject(int.Parse(objectID), int.Parse(claimID), gameObject, loc, orient);
			ClaimObjectData objectData = new ClaimObjectData();
			objectData.objectID = int.Parse(objectID);
			objectData.templateID = int.Parse(templateID);
			objectData.claimID = (int)props["claimID"];
			objectData.prefabName = gameObject;
			objectData.loc = loc;
			objectData.orient = orient;
			objectData.state = state;
			objectData.health = health;
			objectData.maxHealth = maxHealth;
			objectData.complete = complete;
			objectsToLoad.Add(objectData);
		}
	}
	
	public void MoveClaimObjectMessage(Dictionary<string, object> props) {
		int objectID = (int)props["id"];
		Vector3 loc = (Vector3)props["loc"];
		Quaternion orient = (Quaternion)props["orient"];
		int claimID = (int)props["claimID"];
		
		Claim claim = GetClaim(claimID);
		if (claim != null) {
			claim.claimObjects[objectID].transform.position = loc + claim.loc;
			claim.claimObjects[objectID].transform.rotation = orient;
		}
		
		//Debug.Log("Got claim object: " + gameObject);
		//SpawnClaimObject(objectID, claimID, prefabName, loc, orient);
	}
	
	public void UpdateClaimObjectStateMessage(Dictionary<string, object> props) {
		int objectID = (int)props["id"];
		string state = (string)props["state"];
		int claimID = (int)props["claimID"];
		
		Claim claim = GetClaim(claimID);
		if (claim != null) {
			claim.claimObjects[objectID].StateUpdated(state);
		}
		
		//Debug.Log("Got claim object: " + gameObject);
		//SpawnClaimObject(objectID, claimID, prefabName, loc, orient);
	}
	
	public void ClaimObjectInfoMessage(Dictionary<string, object> props) {
		int objectID = (int)props["id"];
		int claimID = (int)props["claimID"];
		int health = (int)props["health"];
		int maxHealth = (int)props["maxHealth"];
		int itemCount = (int)props["itemCount"];
		bool complete = (bool)props["complete"];
		
		Claim claim = GetClaim(claimID);
		if (claim != null) {
			if (!claim.claimObjects.ContainsKey(objectID)) {
				return;
			}
			claim.claimObjects[objectID].Health = health;
			claim.claimObjects[objectID].MaxHealth = maxHealth;
			claim.claimObjects[objectID].Complete = complete;
			Dictionary<int, int> itemReqs = new Dictionary<int, int>();
			for (int i = 0; i < itemCount; i++) {
				itemReqs.Add((int)props["item" + i], (int)props["itemCount" + i]);
			}
			claim.claimObjects[objectID].ItemReqs = itemReqs;
		}
		
		// Dispatch event to be picked up by UI
		string[] args = new string[1];
		args[0] = "";
		AtavismEventSystem.DispatchEvent("CLAIM_OBJECT_UPDATED", args);
	}
	
	public void RemoveClaimObjectMessage(Dictionary<string, object> props) {
		int objectID = (int)props["id"];
		int claimID = (int)props["claimID"];
		
		Claim claim = GetClaim(claimID);
		if (claim != null) {
			if (selectedObject != null && objectID == selectedObject.ID) {
				selectedID = objectID;
				selectedObject = null;
				// Dispatch event to be picked up by UI
				string[] args = new string[1];
				args[0] = "";
				AtavismEventSystem.DispatchEvent("CLAIM_OBJECT_UPDATED", args);
			}
			DestroyImmediate(claim.claimObjects[objectID].gameObject);
			claim.claimObjects.Remove(objectID);
		}
	}
	
	public void ClaimIDMessage(Dictionary<string, object> props) {
		int claimID = (int)props["claimID"];
		Claim claim = new Claim();
		if (GetClaim(claimID) != null) {
			claim = GetClaim(claimID);
		} else {
			claim.id = claimID;
		}
		
		claim.name = (string)props["claimName"];
		claim.area = (int)props["claimArea"];
		claim.loc = (Vector3)props["claimLoc"];
		claim.forSale = (bool)props["forSale"];
		claim.permissionlevel = (int)props["permissionLevel"];
		if (claim.forSale) {
			claim.cost = (int)props["cost"];
			claim.currency = (int)props["currency"];
		}
		claim.playerOwned = (bool)props["myClaim"];
		// Player permissions
		claim.permissions.Clear();
		int permissionCount = (int)props["permissionCount"];
		for (int i = 0; i < permissionCount; i++) {
			ClaimPermission per = new ClaimPermission();
			per.playerName = (string)props["permission_" + i];
			per.permissionLevel = (int)props["permissionLevel_" + i];
			claim.permissions.Add(per);
		}
		
		if (GetClaim(claimID) == null) {
			claims.Add(claim);
		}
		
		if (claim == activeClaim) {
			string[] args = new string[1];
			AtavismEventSystem.DispatchEvent("CLAIM_CHANGED", args);
		}
		AtavismLogger.LogDebugMessage("Got new claim data: " + claim.id);
	}
	
	public void ClaimRemoveDataMessage(Dictionary<string, object> props) {
		int claimID = (int)props["claimID"];
		if (GetClaim(claimID) != null) {
			claims.Remove(GetClaim (claimID));
		}
		AtavismLogger.LogDebugMessage("Removed claim data: " + claimID);
	}
	
	public void ClaimUpdatedMessage(Dictionary<string, object> props) {
		int claimID = (int)props["claimID"];
		Claim claim = GetClaim(claimID);
		claim.forSale = (bool)props["forSale"];
		if (claim.forSale) {
			claim.cost = (int)props["cost"];
			claim.currency = (int)props["currency"];
		}
		claim.playerOwned = (bool)props["myClaim"];
		claims.Add(claim);
		AtavismLogger.LogDebugMessage("Got claim update data");
		if (claim == activeClaim) {
			string[] args = new string[1];
			AtavismEventSystem.DispatchEvent("CLAIM_CHANGED", args);
		}
	}
	
	/// <summary>
	/// Handles the Remove Claim Message which means a player is no longer in the radius for a claim
	/// so the client no longer needs to check if they are in its edit radius.
	/// </summary>
	/// <param name="props">Properties.</param>
	public void RemoveClaimMessage(Dictionary<string, object> props) {
		int claimID = (int)props["claimID"];
		Claim claimToRemove = null;
		foreach (Claim claim in claims) {
			if (claim.id == claimID) {
				/*int itemID = (int)props["resource"];
				int count = (int)props["resourceCount"];
				claim.resources[itemID] = count;*/
				foreach (ClaimObject cObject in claim.claimObjects.Values) {
					DestroyImmediate(cObject.gameObject);
				}
				claimToRemove = claim;
				break;
			}
		}
		if (claimToRemove != null) {
			if (claimToRemove == activeClaim) {
				activeClaim = null;
				string[] args = new string[1];
				AtavismEventSystem.DispatchEvent("CLAIM_CHANGED", args);
			}
			claims.Remove(claimToRemove);
		}
		
		AtavismLogger.LogDebugMessage("Got remove claim data");
	}
	
	/// <summary>
	/// Temporary hack to remove the claim deed item
	/// </summary>
	/// <param name="props">Properties.</param>
	public void ClaimMadeMessage(Dictionary<string, object> props) {
		// Something to be doing?
		
	}
	
	public void HandleBuildingResources(Dictionary<string, object> props) {
		buildingResources.Clear();
		int numResources = (int)props["numResources"];
		for (int i = 0; i < numResources; i++) {
			string resourceID = (string)props["resource" + i + "ID"];
			int resourceCount = (int)props["resource" + i + "Count"];
			buildingResources.Add(int.Parse(resourceID), resourceCount);
		}
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[1];
		AtavismEventSystem.DispatchEvent("RESOURCE_UPDATE", args);
	}
	
	public void HandleStartBuildObject(Dictionary<string, object> props) {
		// Make sure player is in a claim they own
		if (activeClaim == null)
			return;
		
		// Get props and send event out to start the placement of the object
		int buildObjectTemplate = (int)props["buildObjectTemplate"];
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[1];
		args[0] = "" + buildObjectTemplate;
		AtavismEventSystem.DispatchEvent("START_BUILD_CLAIM_OBJECT", args);
	}
	
	public void HandleStartBuildTask(Dictionary<string, object> props) {
		//ClientAPI.Write("Starting build task with length: " + (float)props["length"]);
		float length = (float)props["length"];
		string[] csArgs = new string[2];
		csArgs[0] = length.ToString();
		csArgs[1] = OID.fromLong(ClientAPI.GetPlayerOid()).ToString();
		AtavismEventSystem.DispatchEvent("CASTING_STARTED", csArgs);
	}
	
	public void HandleInterruptBuildTask(Dictionary<string, object> props) {
		int objectID = (int)props["id"];
		int claimID = (int)props["claimID"];
		
		// What do we do now?
		string[] args = new string[1];
		args[0] = "";
		args[1] = OID.fromLong(ClientAPI.GetPlayerOid()).ToString();
		AtavismEventSystem.DispatchEvent("CASTING_CANCELLED", args);
		
		ClientAPI.GetPlayerObject().GameObject.GetComponent<AtavismMobController>().PlayAnimation("", 0);
	}
	
	#endregion Message Handlers
	
	public void StartPlaceBuildObject(int buildObjectTemplateID) {
		// Make sure player is in a claim they own
		if (activeClaim == null)
			return;
		
		// dispatch a ui event to tell the rest of the system
		string[] args = new string[1];
		args[0] = "" + buildObjectTemplateID;
		AtavismEventSystem.DispatchEvent("START_BUILD_CLAIM_OBJECT", args);
	}
	
	public AtavismBuildObjectTemplate GetBuildObjectTemplate(int templateID) {
		if (!buildObjectTemplates.ContainsKey(templateID)) {
			return null;
		}
		
		return buildObjectTemplates[templateID];
	}
	
	public int GetBuildingMaterialCount(int materialID) {
		if (buildingResources.ContainsKey(materialID)) {
			return buildingResources[materialID];
		} else {
			return 0;
		}
	}
	
	public void ClaimAppeared(GameObject claim) {
		claimGameObjects.Add(claim);
		claim.SetActive(showClaims);
	}
	
	public void ClaimRemoved(GameObject claim) {
		claimGameObjects.Remove(claim);
	}
	
	#region Properties
	public static WorldBuilder Instance {
		get {
			return instance;
		}
	}
	
	public List<Claim> Claims {
		get {
			return claims;
		}
	}
	
	public Claim ActiveClaim {
		get {
			return activeClaim;
		}
	}
	
	public bool ShowClaims {
		get {
			return showClaims;
		}
		set {
			showClaims = value;
			foreach (GameObject claim in claimGameObjects) {
				claim.SetActive(showClaims);
			}
		}
	}
	
	public WorldBuildingState BuildingState {
		get {
			return buildingState;
		}
		set {
			buildingState = value;
		}
	}
	
	public Dictionary<int, AtavismBuildObjectTemplate> BuildObjectTemplates {
		get {
			return buildObjectTemplates;
		}
	}
	
	public ClaimObject SelectedObject {
		get {
			return selectedObject;
		}
		set {
			if (selectedObject != null) {
				selectedObject.ResetHighlight();
			}
			if (buildingState == WorldBuildingState.EditItem && selectedObject != value) {
				buildingState = WorldBuildingState.None;
			}
			
			selectedObject = value;
			if (selectedObject != null)
				selectedObject.Highlight();
			if (buildingState == WorldBuildingState.SelectItem) {
				RequestClaimObjectInfo(selectedObject.ID);
				buildingState = WorldBuildingState.EditItem;
			}
			
			// Dispatch event to be picked up by UI
			string[] args = new string[1];
			args[0] = "";
			AtavismEventSystem.DispatchEvent("CLAIM_OBJECT_SELECTED", args);
		}
	}
	
	#endregion Properties
	
	public const int PERMISSION_ADD_ONLY = 1;
	public const int PERMISSION_ADD_DELETE = 2;
	public const int PERMISSION_ADD_USERS = 3;
	public const int PERMISSION_MANAGE_USERS = 4;
	public const int PERMISSION_OWNER = 5;
}
