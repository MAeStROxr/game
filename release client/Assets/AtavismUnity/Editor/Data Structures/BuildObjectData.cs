﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;

public class BuildObjectItemEntry
{
	public BuildObjectItemEntry(int itemId, int count) {
		this.itemId = itemId;
		this.count = count;
	}
	
	public int itemId;
	public int count = 1;
}

public class BuildObjectStage : DataStructure
{

	public BuildObjectStage() : this(-1, "", 0, -1) {
		
	}
	
	public BuildObjectStage(int entryID, string gameObject, int buildTimeReq, int nextStage) {
		this.id = entryID;
		this.gameObject = gameObject;
		this.buildTimeReq = buildTimeReq;
		this.nextStage = nextStage;
		
		fields = new Dictionary<string, string> () {
			{"gameObject", "string"},
			{"buildTimeReq", "float"},
			{"nextStage", "int"},
			{"itemReq1", "int"},
			{"itemReq1Count", "int"},
			{"itemReq2", "int"},
			{"itemReq2Count", "int"},
			{"itemReq3", "int"},
			{"itemReq3Count", "int"},
			{"itemReq4", "int"},
			{"itemReq4Count", "int"},
			{"itemReq5", "int"},
			{"itemReq5Count", "int"},
			{"itemReq6", "int"},
			{"itemReq6Count", "int"}
		};
	}
	
	public string gameObject = "";
	public float buildTimeReq = 0f;
	public int nextStage = -1;
	public int maxEntries = 6;
	public List<BuildObjectItemEntry> entries = new List<BuildObjectItemEntry>();
	
	public BuildObjectStage Clone()
	{
		return (BuildObjectStage) this.MemberwiseClone();
	}
	
	public override string GetValue (string fieldKey)
	{
		switch (fieldKey) {
		case "gameObject":
			return gameObject;
			break;
		case "buildTimeReq":
			return buildTimeReq.ToString();
			break;
		case "nextStage":
			return nextStage.ToString();
			break;
		case "itemReq1":
			return entries[0].itemId.ToString();
			break;
		case "itemReq1Count":
			return entries[0].count.ToString();
			break;
		case "itemReq2":
			return entries[1].itemId.ToString();
			break;
		case "itemReq2Count":
			return entries[1].count.ToString();
			break;
		case "itemReq3":
			return entries[2].itemId.ToString();
			break;
		case "itemReq3Count":
			return entries[2].count.ToString();
			break;
		case "itemReq4":
			return entries[3].itemId.ToString();
			break;
		case "itemReq4Count":
			return entries[3].count.ToString();
			break;
		case "itemReq5":
			return entries[4].itemId.ToString();
			break;
		case "itemReq5Count":
			return entries[4].count.ToString();
			break;
		case "itemReq6":
			return entries[5].itemId.ToString();
			break;
		case "itemReq6Count":
			return entries[5].count.ToString();
			break;
		}
		return "";
	}
}

public class BuildObjectData : DataStructure
{
	public int id = 0;					// Database Index
	// General Parameters
	public string icon = "";
	
	public int category = 0;
	public int skill = -1;
	public int skillLevelReq = 0;
	public string weaponReq = "";
	public float distanceReq = 2f;
	public int firstStageID = 0;
	public bool availableFromItemOnly = false;
	public List<BuildObjectStage> stages = new List<BuildObjectStage>();
	public List<int> stagesToBeDeleted = new List<int>();
	
	public BuildObjectData ()
	{
		// Database fields
	fields = new Dictionary<string, string> () {
		{"name", "string"},
		{"icon", "string"},
		{"category", "int"},
		{"skill", "int"},
		{"skillLevelReq", "int"},
		{"weaponReq", "string"},
		{"distanceReq", "float"},
		{"firstStageID", "int"},
		{"availableFromItemOnly", "bool"},
	};
	}
	
	public BuildObjectData Clone()
	{
		return (BuildObjectData) this.MemberwiseClone();
	}
		
	public override string GetValue (string fieldKey)
	{
		switch (fieldKey) {
		case "name":
			return name;
			break;
		case "icon":
			return icon;
			break;
		case "category":
			return category.ToString();
			break;
		case "skill":
			return skill.ToString();
			break;
		case "skillLevelReq":
			return skillLevelReq.ToString();
			break;	
		case "weaponReq":
			return weaponReq;
			break;	
		case "distanceReq":
			return distanceReq.ToString();
			break;
		case "firstStageID":
			return firstStageID.ToString();
			break;
		case "availableFromItemOnly":
			return availableFromItemOnly.ToString();
			break;
		}
		
		return "";
	}
		
}