﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Collections.Generic;

public class CurrencyConversionEntry : DataStructure
{
	public CurrencyConversionEntry() : this(-1, -1, 0) {
	}
	
	public CurrencyConversionEntry(int currencyID, int currencyTo, int amount) {
		this.currencyID = currencyID;
		this.currencyToID = currencyTo;
		this.amount = amount;
		
		fields = new Dictionary<string, string> () {
			{"currencyID", "int"},
			{"currencyToID", "int"},
			{"amount", "int"},
			{"autoConverts", "bool"},
		};
	}
	
	public int currencyID;
	public int currencyToID;
	public int amount = 1;
	public bool autoConverts = true;
	
	public CurrencyConversionEntry Clone()
	{
		return (CurrencyConversionEntry) this.MemberwiseClone();
	}
	
	public override string GetValue (string fieldKey)
	{
		if (fieldKey == "id") {
			return id.ToString();
		} else if (fieldKey == "currencyID") {
			return currencyID.ToString();
		} else if (fieldKey == "currencyToID") {
			return currencyToID.ToString();
		} else if (fieldKey == "amount") {
			return amount.ToString();
		} else if (fieldKey == "autoConverts") {
			return autoConverts.ToString();
		}
		return "";
	}
}

public class CurrencyData: DataStructure
{
	public int id = 0;					// Database Index
	// General Parameters
	public string name = "name";		// The item template name
	public int category = 1;			//leave this as 1
	public string icon = "";			// The item icon
	public string description = "";		// A description of the currency (optional)
	public int maximum = 999999;		// The maximum amount of the currency a player can have
	public int currencyGroup = 1;
	public int currencyPosition = 0;
	public string[] positionOptions = {"1", "2", "3" };
	public bool external = false;		// Can be modified outside the game (such as being purchased from an online store
	
	public List<CurrencyConversionEntry> currencyConversion = new List<CurrencyConversionEntry>();
	
	public List<int> conversionsToBeDeleted = new List<int>();
	
	public CurrencyData ()
	{
		// Database fields
		fields = new Dictionary<string, string> () {
			{"name", "string"},
			{"category", "int"},
			{"icon", "string"},
			{"description", "string"},
			{"maximum", "int"},
			{"currencyGroup", "int"},
			{"currencyPosition", "int"},
			{"external", "bool"}, 
		};
	}
	
	public CurrencyData Clone()
	{
		return (CurrencyData) this.MemberwiseClone();
	}
	
	public override string GetValue (string fieldKey)
	{
		switch (fieldKey) {
		case "id":
			return id.ToString();
			break;
		case "name":
			return name;
			break;
		case "category":
			return category.ToString();
			break;
		case "icon":
			return icon;
			break;
		case "description":
			return description;
			break;
		case "maximum":
			return maximum.ToString();
			break;	
		case "currencyGroup":
			return currencyGroup.ToString();
			break;	
		case "currencyPosition":
			return currencyPosition.ToString();
			break;	
		case "external":
			return external.ToString();
			break;
		}	
		return "";
	}
	
}