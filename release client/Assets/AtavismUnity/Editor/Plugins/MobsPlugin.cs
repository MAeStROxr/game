﻿using UnityEngine;
using System.Collections;

public class MobsPlugin : AtavismPlugin {

	// Use this for initialization
	public MobsPlugin()
	{	
		pluginName = "Mob";
		string serverCategory = "AT_button_category_mob";
		/*icon = (Texture)Resources.Load (serverCategory, typeof(Texture));
		iconOver = (Texture)Resources.Load (serverCategory + "_over", typeof(Texture));
		iconSelected = (Texture)Resources.Load (serverCategory + "_selected", typeof(Texture));*/
		/*icon = new Texture2D(1, 1);
		iconOver = new Texture2D(1, 1);
		iconSelected = new Texture2D(1, 1);
		icon.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + ".png"));
		iconOver.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + "_over.png"));
		iconSelected.LoadImage(System.IO.File.ReadAllBytes("Assets\\AtavismUnity\\Editor\\Resources\\" + serverCategory + "_selected.png"));*/
	}

	void Awake() {
		string serverCategory = "AT_button_category_mob";
		icon = (Texture2D)Resources.Load (serverCategory, typeof(Texture));
		iconOver = (Texture2D)Resources.Load (serverCategory + "_over", typeof(Texture));
		iconSelected = (Texture2D)Resources.Load (serverCategory + "_selected", typeof(Texture));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
