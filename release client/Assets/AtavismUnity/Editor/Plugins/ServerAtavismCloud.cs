﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Server connection and setup function
public class ServerAtavismCloud : AtavismFunction
{
	
	List<AtavismCloudServer> servers;
	private int selectedServerID = -1;		// Connection test type (Content or Admin)
	bool serversLoaded = false;
	string serverResponse = "";
	
	// Tab selection
	public int selected = 1;
	
	// Use this for initialization
	public ServerAtavismCloud ()
	{	
		functionName = "Atavism Cloud";		// Set the function name
	}
	
	
	// Update is called once per frame
	void Update ()
	{
	}
	
	private int SelectTab (Rect pos, int sel)
	{
		pos.y += ImagePack.tabTop;
		pos.x += ImagePack.tabLeft;
		bool edit = false;
		bool doc = false;
		
		switch (sel) { 
		case 1:
			edit = true;
			break;
		case 2:
			doc = true;
			break;			
		}
		
		pos.x += ImagePack.tabMargin;
		if (edit)
			pos.y += ImagePack.tabSpace;
		if (ImagePack.DrawTabEdit (pos, edit))
			return 1;
		if (edit)
			pos.y -= ImagePack.tabSpace;
		pos.x += ImagePack.tabMargin;
		if (doc)
			pos.y += ImagePack.tabSpace;
		if (ImagePack.DrawTabDoc (pos, doc))
			return 2;
		if (doc)
			pos.y -= ImagePack.tabSpace;
		
		return sel;
	}
	
	// Draw the function inspector
	// box: Rect representing the inspector area
	public override void Draw (Rect box)
	{
		
		// Draw the Control Tabs
		selected = SelectTab (box, selected);
		
		if (!serversLoaded) {
			servers = AtavismUnity.Instance.GetServers();
			serversLoaded = true;
		}
		
		if (selected == 1) {
			// Set the drawing layout
			Rect pos = box;
			pos.x += ImagePack.innerMargin;
			pos.y += ImagePack.innerMargin;
			pos.width -= ImagePack.innerMargin;
			pos.height = ImagePack.fieldHeight;
			
			// Draw the content database info
			ImagePack.DrawLabel (pos.x, pos.y, "Atavism Cloud Configuration");
			DrawServers (pos);
			
			ImagePack.DrawScrollBar (box.x + box.width, box.y, box.height - 14);
		} else if (selected == 2) {
			DrawHelp (box);	
		} 	
	}
	
	// Basic Database Draw
	void DrawServers (Rect pos)
	{
		// Layout
		float posX = pos.x + ImagePack.innerMargin;
		float posY = pos.y;
		float width = pos.width - 2 * ImagePack.innerMargin;
		float height = ImagePack.fieldHeight;
		
		pos.y += 1.5f * ImagePack.fieldHeight;
		
		ImagePack.DrawLabel (pos.x, pos.y, "Available Servers:");
		pos.y += ImagePack.fieldHeight;
		
		if (servers.Count == 0) {
			ImagePack.DrawText (pos, "You do not have any active Atavism Cloud Servers");
			pos.y += ImagePack.fieldHeight;
			if (ImagePack.DrawButton (pos.x, pos.y, "Get Atavism Cloud")) {	
				Application.OpenURL("http://www.neojac.com");
			}
			return;
		}
		
		pos.width /= 3;
		
		ImagePack.DrawText (pos, "Name");
		pos.x += pos.width;
		ImagePack.DrawText (pos, "Address");
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		
		foreach(AtavismCloudServer server in servers) {
			ImagePack.DrawText (pos, server.Name);
			pos.x += pos.width;
			ImagePack.DrawMultilineText (pos, server.DnsName);
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Restart")) {	
				serverResponse = "Please Wait...";
				serverResponse = server.RestartServer();
			}
			pos.x -= pos.width;
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
		}
		
		pos.width *= 3;
		
		pos.y += ImagePack.fieldHeight;
		Color temp = GUI.color;
		GUI.color = Color.red;
		ImagePack.DrawMultilineText(pos, serverResponse);
		GUI.color = temp;
	}
	
}

