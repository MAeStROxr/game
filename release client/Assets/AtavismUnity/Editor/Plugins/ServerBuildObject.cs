﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Build Object Template Configuration
public class ServerBuildObject : AtavismDatabaseFunction 
{
	
	public Dictionary<int, BuildObjectData> dataRegister;
	public BuildObjectData editingDisplay;
	public BuildObjectData originalDisplay;
	
	public int[] categoryIds = new int[] {-1};
	public string[] categoryOptions = new string[] {"~ none ~"};
	
	public int[] itemIds = new int[] {-1};
	public string[] itemsList = new string[] {"~ none ~"};
	public int[] skillIds = new int[] {-1};
	public string[] skillOptions = new string[] {"~ none ~"};
	public string[] weaponTypeOptions = new string[] {"~ none ~"};
	
	// Handles the prefab creation, editing and save
	private BuildObjectPrefab item = null;

	// Use this for initialization
	public ServerBuildObject ()
	{
		functionName = "Build Object";
		// Database tables name
	    tableName = "build_object_template";
		functionTitle =  "Build Object Configuration";
		loadButtonLabel =  "Load Build Object";
		notLoadedText =  "No Build Objects loaded.";
		// Init
		dataRegister = new Dictionary<int, BuildObjectData> ();

		editingDisplay = new BuildObjectData ();	
		originalDisplay = new BuildObjectData ();	
	}

	public override void Activate()
	{
		linkedTablesLoaded = false;
	}

	private void LoadItemList ()
	{
		string query = "SELECT id, name FROM item_templates where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			itemsList = new string[rows.Count + 1];
			itemsList [optionsId] = "~ none ~"; 
			itemIds = new int[rows.Count + 1];
			itemIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				itemsList [optionsId] = data ["id"] + ":" + data ["name"]; 
				itemIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}
	
	public void LoadSkillOptions ()
	{
		string query = "SELECT id, name FROM skills where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			skillOptions = new string[rows.Count + 1];
			skillOptions [optionsId] = "~ none ~"; 
			skillIds = new int[rows.Count + 1];
			skillIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				skillOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				skillIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	// Load Database Data
	public override void Load ()
	{
		if (!dataLoaded) {
			// Clean old data
			dataRegister.Clear ();
			displayKeys.Clear ();
			
			// Load in stages first
			Dictionary<int, BuildObjectStage> stages = LoadBuildObjectStages();

			// Read all entries from the table
			string query = "SELECT " + originalDisplay.GetFieldsString() + " FROM " + tableName + " where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
		
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			if ((rows!=null) && (rows.Count > 0)) {
				foreach (Dictionary<string,string> data in rows) {
					//foreach(string key in data.Keys)
					//	Debug.Log("Name[" + key + "]:" + data[key]);
					//return;
					BuildObjectData display = new BuildObjectData ();
					display.id = int.Parse (data ["id"]);
					display.name = data ["name"];
					display.icon = data ["icon"];
					display.skill = int.Parse(data["skill"]);
					display.skillLevelReq = int.Parse(data["skillLevelReq"]);
					display.weaponReq = data["weaponReq"];
					display.distanceReq = float.Parse(data["distanceReq"]);
					display.firstStageID = int.Parse(data["firstStageID"]);
					display.availableFromItemOnly = bool.Parse(data["availableFromItemOnly"]);
					
					int stageID = display.firstStageID;
					// Load in stages
					while (stageID > 0) {
						if (stages.ContainsKey(stageID)) {
							display.stages.Add(stages[stageID]);
							stageID = stages[stageID].nextStage;
						} else {
							stageID = -1;
						}
					}
											
					display.isLoaded = true;
					//Debug.Log("Name:" + display.name  + "=[" +  display.id  + "]");
					dataRegister.Add (display.id, display);
					displayKeys.Add (display.id);
				}
				LoadSelectList();
			}
			
			dataLoaded = true;
		}
	}
	
	Dictionary<int, BuildObjectStage> LoadBuildObjectStages ()
	{
		Dictionary<int, BuildObjectStage> stages = new Dictionary<int, BuildObjectStage>();
		// Read all entries from the table
		string query = "SELECT " + new BuildObjectStage ().GetFieldsString() + " FROM build_object_stage where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		//Debug.Log("#Rows:"+rows.Count);
		// Read all the data
		if ((rows != null) && (rows.Count > 0)) {
			foreach (Dictionary<string,string> data in rows) {
				BuildObjectStage stage = new BuildObjectStage ();
				stage.id = int.Parse(data["id"]);
				stage.gameObject = data["gameObject"];
				stage.buildTimeReq = float.Parse(data["buildTimeReq"]);
				stage.nextStage = int.Parse(data["nextStage"]);
				for (int i = 1; i <= stage.maxEntries; i++) {
					int itemId = int.Parse (data ["itemReq" + i]);
					int count = int.Parse (data ["itemReq" + i + "Count"]);
					BuildObjectItemEntry entry = new BuildObjectItemEntry(itemId, count);
					stage.entries.Add(entry);
				}
				stages.Add(stage.id, stage);
			}
		}
		return stages;
	}
	
	public void LoadSelectList() 
	{
			//string[] selectList = new string[dataRegister.Count];
			displayList =  new string[dataRegister.Count];
			int i = 0;
			foreach (int displayID in dataRegister.Keys) {
				//selectList [i] = displayID + ". " + dataRegister [displayID].name;
				displayList [i] = displayID + ". " + dataRegister [displayID].name;
				i++;
			}
			//displayList = new Combobox(selectList);
	}	
	
	// Draw the loaded list
	public override void DrawLoaded (Rect box)
	{	
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
								
		if (dataRegister.Count <= 0) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "You must create a Build Object before editing it.");		
			return;
		}
		

		// Draw the content database info
		ImagePack.DrawLabel (pos.x, pos.y, "Edit Build Object");
		
		if (newItemCreated) {
			newItemCreated = false;
			LoadSelectList();
			newSelectedDisplay = displayKeys.Count - 1;
		}

		// Draw data Editor
		if (newSelectedDisplay != selectedDisplay) {
			selectedDisplay = newSelectedDisplay;	
			int displayKey = displayKeys [selectedDisplay];
			editingDisplay = dataRegister [displayKey];		
			originalDisplay = editingDisplay.Clone();
		} 
		
		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.x -= ImagePack.innerMargin;
		pos.y -= ImagePack.innerMargin;
		pos.width += ImagePack.innerMargin;
		
		if (state != State.Loaded) {
			pos.x += ImagePack.innerMargin;
			pos.width /= 2;
			//Draw super magical compound object.
			newSelectedDisplay = ImagePack.DrawDynamicPartialListSelector(pos, "Search Filter: ", ref entryFilterInput, selectedDisplay, displayList);
			
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight * 1.5f;
			ImagePack.DrawLabel (pos.x, pos.y, "Build Object Properties:");
			pos.y += ImagePack.fieldHeight * 0.75f;
		}
		
		DrawEditor (pos, false);
		
		pos.y -= ImagePack.fieldHeight;
		//pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
	}
	
	public override void CreateNewData()
	{
		editingDisplay = new BuildObjectData ();		
		originalDisplay = new BuildObjectData ();	
		selectedDisplay = -1;
	}
	
	// Edit or Create
	public override void DrawEditor (Rect box, bool newItem)
	{
		if (!linkedTablesLoaded) {
			// Load items
			LoadItemList();
			LoadSkillOptions();
			ServerOptionChoices.LoadAtavismChoiceOptions("Building Category", true, out categoryIds, out categoryOptions);
			weaponTypeOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Weapon Type", true);
			linkedTablesLoaded = true;
		}
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;

		// Draw the content database info		
		if (newItem) {
			ImagePack.DrawLabel (pos.x, pos.y, "Create a new build object template");		
			pos.y += ImagePack.fieldHeight;
		}
		
		editingDisplay.name = ImagePack.DrawField (pos, "Name:", editingDisplay.name, 0.5f);
		//pos.x += pos.width;
		//editingDisplay.icon = ImagePack.DrawTextureAsset (pos, "Icon:", editingDisplay.icon);	
		//pos.x -= pos.width;
		pos.width /= 2;
		pos.y += ImagePack.fieldHeight;
		int selectedSkill = GetOptionPosition (editingDisplay.skill, skillIds);
		selectedSkill = ImagePack.DrawSelector (pos, "Skill:", selectedSkill, skillOptions);
		editingDisplay.skill = skillIds [selectedSkill];
		pos.x += pos.width;
		editingDisplay.icon = ImagePack.DrawSpriteAsset (pos, "Icon:", editingDisplay.icon);		
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		int selectedCategory = GetOptionPosition (editingDisplay.category, categoryIds);
		selectedCategory = ImagePack.DrawSelector (pos, "Category:", selectedCategory, categoryOptions);
		editingDisplay.category = categoryIds [selectedCategory];
		pos.y += ImagePack.fieldHeight;
		editingDisplay.skillLevelReq = ImagePack.DrawField (pos, "Skill Level Req:", editingDisplay.skillLevelReq);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.weaponReq = ImagePack.DrawSelector (pos, "Weapon Req:", editingDisplay.weaponReq, weaponTypeOptions);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.distanceReq = ImagePack.DrawField (pos, "Max Distance:", editingDisplay.distanceReq);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.availableFromItemOnly = ImagePack.DrawToggleBox (pos, "Available as Item Only?", editingDisplay.availableFromItemOnly);
		
		// Loop through stages
		if (editingDisplay.stages.Count == 0) {
			editingDisplay.stages.Add (new BuildObjectStage());
		}
		for (int i = 0; i < editingDisplay.stages.Count; i++) {
			pos.y += 1.5f * ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "Stage " + (i+1) + ":");
			pos.y += ImagePack.fieldHeight;
			pos.width = pos.width * 2;
			editingDisplay.stages[i].gameObject = ImagePack.DrawGameObject (pos, "Prefab: ", editingDisplay.stages[i].gameObject, 0.75f);
			pos.width /= 2;
			pos.y += ImagePack.fieldHeight;
			editingDisplay.stages[i].buildTimeReq = ImagePack.DrawField (pos, "Time to Build:", editingDisplay.stages[i].buildTimeReq);
			pos.y += 1.5f * ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "Items Required:");		
			pos.y += ImagePack.fieldHeight;
			for (int j = 0; j < editingDisplay.stages[i].maxEntries; j++) {
				if (editingDisplay.stages[i].entries.Count <= j)
					editingDisplay.stages[i].entries.Add(new BuildObjectItemEntry(-1, 0));
				int selectedItem = GetOptionPosition (editingDisplay.stages[i].entries[j].itemId, itemIds);
				selectedItem = ImagePack.DrawSelector (pos, "Item " + (j+1) + ":", selectedItem, itemsList);
				editingDisplay.stages[i].entries[j].itemId = itemIds[selectedItem];
				pos.x += pos.width;
				editingDisplay.stages[i].entries[j].count = ImagePack.DrawField (pos, "Count:", editingDisplay.stages[i].entries[j].count);
				pos.x -= pos.width;
				pos.y += ImagePack.fieldHeight;
			}
			
			if (editingDisplay.stages.Count > 1) {
				pos.x += pos.width;
				if (ImagePack.DrawButton (pos.x, pos.y, "Remove Stage")) {
					if (editingDisplay.stages[i].id > 0)
						editingDisplay.stagesToBeDeleted.Add(editingDisplay.stages[i].id);
					editingDisplay.stages.RemoveAt(i);
				}
				pos.x -= pos.width;
			}
		}
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Add Stage")) {
			editingDisplay.stages.Add (new BuildObjectStage());
		}
		
		pos.width = pos.width * 2;
		
		// Save data		
		pos.x -= ImagePack.innerMargin;
		pos.y += 1.4f * ImagePack.fieldHeight;
		pos.width /=3;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Data")) {
			if (newItem)
				InsertEntry ();
			else
				UpdateEntry ();
			
			state = State.Loaded;
		}
		
		// Delete data
		if (!newItem) {
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Data")) {
				DeleteEntry ();
				newSelectedDisplay = 0;
				state = State.Loaded;
			}
		}
		
		// Cancel editing
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Cancel")) {
			editingDisplay = originalDisplay.Clone();
			if (newItem)
				state = State.New;
			else
				state = State.Loaded;
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}
		
		if (!newItem)
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight + 100);
		else
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight);
	}
	
	// Insert new entries into the table
	void InsertEntry ()
	{
		NewResult("Inserting...");
		// Insert the items - reverse the order so the next Ids can be set
		for (int i = editingDisplay.stages.Count-1; i >= 0; i--) {
			InsertStage (editingDisplay.stages[i]);
			if (i > 0) {
				editingDisplay.stages[i-1].nextStage = editingDisplay.stages[i].id;
			}
		}
		// Set the first stage ID 
		editingDisplay.firstStageID = editingDisplay.stages[0].id;
		
		// Setup the update query
		string query = "INSERT INTO " + tableName;		
		query += " (" + editingDisplay.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + editingDisplay.FieldList ("?", ", ") + ") ";
		
		int mobID = -1;

		// Setup the register data
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));
		}
		
		// Update the database
		mobID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);

		// If the insert failed, don't insert the stages
		if (mobID != -1) {
			// Update online table to avoid access the database again			
			editingDisplay.id = mobID;
			editingDisplay.isLoaded = true;
			//Debug.Log("ID:" + mobID + "ID2:" + editingDisplay.id);
			dataRegister.Add (editingDisplay.id, editingDisplay);
			displayKeys.Add (editingDisplay.id);
			newItemCreated = true;
			
			// Configure the correponding prefab
			CreatePrefab();
			NewResult("New entry inserted");
		} else {
			NewResult("Error occurred, please check the Console");
		}
	}
	
	void InsertStage (BuildObjectStage entry)
	{
		string query = "INSERT INTO build_object_stage";			
		query += " (" + entry.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + entry.FieldList ("?", ", ") + ") ";
		
		// Setup the register data
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));
		}
		
		entry.id = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);
	}

	// Update existing entries in the table based on the iddemo_table
	void UpdateEntry ()
	{
		NewResult("Updating...");
		// Insert/Update the items - reverse the order so the next Ids can be set
		for (int i = editingDisplay.stages.Count-1; i >= 0; i--) {
			if (editingDisplay.stages[i].id < 1) {
				InsertStage (editingDisplay.stages[i]);
			} else {
				UpdateStage (editingDisplay.stages[i]);
			}
			if (i > 0) {
				editingDisplay.stages[i-1].nextStage = editingDisplay.stages[i].id;
			}
		}
		
		// Set the first stage ID 
		editingDisplay.firstStageID = editingDisplay.stages[0].id;
		
		// Setup the update query
		string query = "UPDATE " + tableName;
		query += " SET ";
		query += editingDisplay.UpdateList ();
		query += " WHERE id=?id";

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) { 
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int));
	
		// Update the database
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
				
		// Update online table to avoid access the database again			
		dataRegister [displayKeys [selectedDisplay]] = editingDisplay;	
		
		// Delete any stages that are tagged for deletion
		foreach (int stageID in editingDisplay.stagesToBeDeleted) {
			DeleteStage(stageID);
		}
		
		// Configure the correponding prefab
		CreatePrefab();
		NewResult("Entry updated");			
	}
	
	void UpdateStage (BuildObjectStage entry)
	{
		string query = "UPDATE build_object_stage";		
		query += " SET ";
		query += entry.UpdateList ();
		query += " WHERE id=?id";
		
		// Setup the register data
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, entry.id.ToString (), Register.TypesOfField.Int));
		
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
	}
	
	// Delete entries from the table
	void DeleteEntry ()
	{
		// Remove the prefab
		DeletePrefab();
		
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, tableName, delete);
		string query = "UPDATE " + tableName + " SET isactive = 0 where id = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Update online table to avoid access the database again			
		// Update online table to avoid access the database again		
		dataRegister.Remove (displayKeys [selectedDisplay]);
		displayKeys.Remove (displayKeys [selectedDisplay]);
		if (dataRegister.Count > 0)	{
			LoadSelectList();
			selectedDisplay = -1;
			newSelectedDisplay = 0;
		} else {
			displayList = null;
			dataLoaded = false;
		}
	}
	
	void DeleteStage(int stageID) {
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, stageID.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "build_object_stage", delete);
		string query = "UPDATE build_object_stage SET isactive = 0 where id = " + stageID;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
	}
	
	void CreatePrefab()
	{
		// Configure the correponding prefab
		item = new BuildObjectPrefab(editingDisplay);
		item.Save(editingDisplay);
	}
	
	void DeletePrefab()
	{
		item = new BuildObjectPrefab(editingDisplay);
		
		if (item.Load())
			item.Delete();
	}
	
}
