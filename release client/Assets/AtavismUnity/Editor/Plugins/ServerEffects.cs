﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Effects Configuration
public class ServerEffects : AtavismDatabaseFunction
{

	private static ServerEffects instance;

	public Dictionary<int, EffectsData> dataRegister;
	public EffectsData editingDisplay;
	public EffectsData originalDisplay;
	public int[] skillIds = new int[] {-1};
	public string[] skillOptions = new string[] {"~ none ~"};
	public static int[] effectIds = new int[] {-1};
	public static string[] effectOptions = new string[] {"~ none ~"};
	
	// Handles the prefab creation, editing and save
	private EffectPrefab prefab = null;

	// Use this for initialization
	public ServerEffects ()
	{	
		instance = this;
		functionName = "Effects";
		// Database tables name
		tableName = "effects";
		functionTitle = "Effects Configuration";
		loadButtonLabel = "Load Effects";
		notLoadedText = "No Effect loaded.";
		// Init
		dataRegister = new Dictionary<int, EffectsData> ();

		editingDisplay = new EffectsData ();
		originalDisplay = new EffectsData ();
	}

	public override void Activate ()
	{
		linkedTablesLoaded = false;
	}

	public void LoadSkillOptions ()
	{
		string query = "SELECT id, name FROM skills where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			skillOptions = new string[rows.Count + 1];
			skillOptions [optionsId] = "~ none ~"; 
			skillIds = new int[rows.Count + 1];
			skillIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				skillOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				skillIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}

	public static void LoadEffectOptions ()
	{
		// Read all entries from the table
		string query = "SELECT id, name FROM effects where isactive = 1";
			
		// Load data
		List<Dictionary<string,string>> rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			effectOptions = new string[rows.Count + 1];
			effectOptions [optionsId] = "~ none ~"; 
			effectIds = new int[rows.Count + 1];
			effectIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				effectOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				effectIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}

	// Load Database Data
	public override void Load ()
	{
		if (!dataLoaded) {
			// Clean old data
			dataRegister.Clear ();
			displayKeys.Clear ();

			// Read all entries from the table
			string query = "SELECT " + originalDisplay.GetFieldsString() + " FROM " + tableName + " where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
		
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			if ((rows != null) && (rows.Count > 0)) {
				foreach (Dictionary<string,string> data in rows) {
					//foreach(string key in data.Keys)
					//	Debug.Log("Name[" + key + "]:" + data[key]);
					//return;
					EffectsData display = new EffectsData ();

					display.id = int.Parse (data ["id"]);
					display.name = data ["name"]; 

					//display.displayName = data ["displayName"];
					display.icon = data ["icon"];
					display.effectMainType = data ["effectMainType"];
					// Set the effect class
					foreach(ServerEffectType effectClass in editingDisplay.effectTypes) {
						if (effectClass.EffectType == display.effectMainType) {
							display.effectClass = effectClass;
							display.effectMainType = effectClass.EffectType;
						}	
					}
					display.effectType = data ["effectType"];
					//display.effectFamily = int.Parse (data ["effectFamily"]);
					display.isBuff = bool.Parse (data ["isBuff"]);
					display.skillType = int.Parse (data ["skillType"]);
					display.skillLevelMod = float.Parse (data["skillLevelMod"]);
					display.passive = bool.Parse (data ["passive"]);
					display.stackLimit = int.Parse (data ["stackLimit"]);
					display.allowMultiple = bool.Parse (data ["allowMultiple"]);
					display.duration = float.Parse (data ["duration"]);
					display.pulseCount = int.Parse (data ["pulseCount"]);
					display.tooltip = data ["tooltip"];
					display.bonusEffectReq = int.Parse (data ["bonusEffectReq"]);
					display.bonusEffectReqConsumed = bool.Parse (data ["bonusEffectReqConsumed"]);
					display.bonusEffect = int.Parse (data ["bonusEffect"]);
					display.pulseCoordEffect = data ["pulseCoordEffect"];
					
					display.intValue1 = int.Parse (data ["intValue1"]);
					display.intValue2 = int.Parse (data ["intValue2"]);
					display.intValue3 = int.Parse (data ["intValue3"]);
					display.intValue4 = int.Parse (data ["intValue4"]);
					display.intValue5 = int.Parse (data ["intValue5"]);
					
					display.floatValue1 = float.Parse (data ["floatValue1"]);
					display.floatValue2 = float.Parse (data ["floatValue2"]);
					display.floatValue3 = float.Parse (data ["floatValue3"]);
					display.floatValue4 = float.Parse (data ["floatValue4"]);
					display.floatValue5 = float.Parse (data ["floatValue5"]);
					
					display.stringValue1 = data ["stringValue1"];
					display.stringValue2 = data ["stringValue2"];
					display.stringValue3 = data ["stringValue3"];
					display.stringValue4 = data ["stringValue4"];
					display.stringValue5 = data ["stringValue5"];
					
					display.boolValue1 = bool.Parse (data ["boolValue1"]);
					display.boolValue2 = bool.Parse (data ["boolValue2"]);
					display.boolValue3 = bool.Parse (data ["boolValue3"]);
					display.boolValue4 = bool.Parse (data ["boolValue4"]);
					display.boolValue5 = bool.Parse (data ["boolValue5"]);

					display.isLoaded = true;
					//Debug.Log("Name:" + display.name  + "=[" +  display.id  + "]");
					dataRegister.Add (display.id, display);
					displayKeys.Add (display.id);
				}
				LoadSelectList ();
			}
			dataLoaded = true;
		}
	}
	
	public void LoadSelectList ()
	{
		//string[] selectList = new string[dataRegister.Count];
		displayList = new string[dataRegister.Count];
		int i = 0;
		foreach (int displayID in dataRegister.Keys) {
			//selectList [i] = displayID + ". " + dataRegister [displayID].name;
			displayList [i] = displayID + ". " + dataRegister [displayID].name;
			i++;
		}
		//displayList = new Combobox(selectList);
	}	
	
	
	// Draw the loaded list
	public override  void DrawLoaded (Rect box)
	{	
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
						
		if (dataRegister.Count <= 0) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "You must create an Effect before editing it.");		
			return;
		}
		
		// Draw the content database info
		ImagePack.DrawLabel (pos.x, pos.y, "Edit Effect");

		if (newItemCreated) {
			newItemCreated = false;
			LoadSelectList ();
			newSelectedDisplay = displayKeys.Count - 1;
		}

		// Draw data Editor
		if (newSelectedDisplay != selectedDisplay) {
			selectedDisplay = newSelectedDisplay;	
			int displayKey = displayKeys [selectedDisplay];
			editingDisplay = dataRegister [displayKey];		
			originalDisplay = editingDisplay.Clone ();
			if (editingDisplay.effectClass != null)
				editingDisplay.effectClass.LoadOptions(editingDisplay, false);
		} 
		
		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.x -= ImagePack.innerMargin;
		pos.y -= ImagePack.innerMargin;
		pos.width += ImagePack.innerMargin;
		
		if (state != State.Loaded) {
			pos.x += ImagePack.innerMargin;
			pos.width /= 2;
			//Draw super magical compound object.
			newSelectedDisplay = ImagePack.DrawDynamicPartialListSelector(pos, "Search Filter: ", ref entryFilterInput, selectedDisplay, displayList);
			
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight * 1.5f;
			ImagePack.DrawLabel (pos.x, pos.y, "Effect Properties:");
			pos.y += ImagePack.fieldHeight * 0.75f;
		}
		
		DrawEditor (pos, false);
		
		pos.y -= ImagePack.fieldHeight;
		//pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
	}

	public override void CreateNewData ()
	{
		editingDisplay = new EffectsData ();		
		originalDisplay = new EffectsData ();
		selectedDisplay = -1;
	}
	
	// Edit or Create
	public override void DrawEditor (Rect box, bool newItem)
	{
		
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;

		// Draw the content database info
		if (!linkedTablesLoaded) {	
			LoadSkillOptions ();
			LoadEffectOptions ();
			linkedTablesLoaded = true;
		}

		if (newItem) {
			ImagePack.DrawLabel (pos.x, pos.y, "Create a new Effect");		
			pos.y += ImagePack.fieldHeight;
			
			if (editingDisplay.effectClass == null) {
				foreach(ServerEffectType effectClass in editingDisplay.effectTypes) {
					if (ImagePack.DrawButton (pos.x, pos.y, effectClass.EffectType)) {
						editingDisplay.effectClass = effectClass;
						editingDisplay.effectMainType = effectClass.EffectType;
						effectClass.LoadOptions(editingDisplay, true);
					}	
					pos.y += ImagePack.fieldHeight;
				}
				return;
			}
		}

		int selectedEffect = -1;
		
		editingDisplay.name = ImagePack.DrawField (pos, "Name:", editingDisplay.name, 0.75f);
		pos.y += ImagePack.fieldHeight;
		//editingDisplay.displayName = ImagePack.DrawField (pos, "Display Name:", editingDisplay.displayName, 0.75f);
		//pos.y += ImagePack.fieldHeight;
		//editingDisplay.icon = ImagePack.DrawField (pos, "Icon Name:", editingDisplay.icon, 0.75f);
		pos.width /= 2;
		// We only allowing changing of the effect main type when it is a new item
		ImagePack.DrawText (pos, "Effect Type: " + editingDisplay.effectMainType);
		pos.x += pos.width;
		editingDisplay.icon = ImagePack.DrawSpriteAsset (pos, "Icon:", editingDisplay.icon);		
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight * 1.5f;
		// Allow selection of effect type from the effect sub class
		editingDisplay.effectType = ImagePack.DrawSelector (pos, "Effect Subtype:", editingDisplay.effectType, editingDisplay.effectClass.EffectTypeOptions);
		pos.y += ImagePack.fieldHeight;
		// Draw items from the effectClass
		bool showTimeFields;
		pos = editingDisplay.effectClass.DrawEditor(pos, newItem, editingDisplay, out showTimeFields);

		//editingDisplay.effectFamily = ImagePack.DrawField (pos, "Family:", editingDisplay.effectFamily);
		//
		editingDisplay.isBuff = ImagePack.DrawToggleBox (pos, "Is Buff?", editingDisplay.isBuff);
		pos.x += pos.width;
		editingDisplay.passive = ImagePack.DrawToggleBox (pos, "Is Passive?", editingDisplay.passive);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		int selectedSkill = GetPositionOfSkill (editingDisplay.skillType);
		selectedSkill = ImagePack.DrawSelector (pos, "Skill Type:", selectedSkill, skillOptions);
		editingDisplay.skillType = skillIds [selectedSkill];
		pos.x += pos.width;
		editingDisplay.skillLevelMod = ImagePack.DrawField (pos, "Skill Mod:", editingDisplay.skillLevelMod);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		
		if (showTimeFields) {
			editingDisplay.stackLimit = ImagePack.DrawField (pos, "Stack Limit:", editingDisplay.stackLimit);
			pos.x += pos.width;
			editingDisplay.allowMultiple = ImagePack.DrawToggleBox (pos, "Allow Multiple?", editingDisplay.allowMultiple);
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
		
			editingDisplay.duration = ImagePack.DrawField (pos, "Duration (secs):", editingDisplay.duration);
			pos.x += pos.width;
			editingDisplay.pulseCount = ImagePack.DrawField (pos, "Num. Pulses", editingDisplay.pulseCount);
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
			pos.width *= 2;
			pos.width /= 3;
			pos.width *= 2;
			editingDisplay.pulseCoordEffect = ImagePack.DrawGameObject (pos, "Pulse CoordEffect:", editingDisplay.pulseCoordEffect, 0.6f);
			pos.width /= 2;
			//pos.x -= pos.width;
			pos.width *= 3;
			pos.y += ImagePack.fieldHeight;
			
		} else {
			pos.width *= 2;
		}
		
		editingDisplay.tooltip = ImagePack.DrawField (pos, "Tooltip:", editingDisplay.tooltip, 0.75f, 60);
		pos.y += 3 * ImagePack.fieldHeight;

		ImagePack.DrawLabel (pos.x, pos.y, "Bonus Effects");		
		pos.y += ImagePack.fieldHeight;
		pos.width /= 2;
		selectedEffect = GetPositionOfEffect (editingDisplay.bonusEffectReq);
		selectedEffect = ImagePack.DrawSelector (pos, "Required Effect:", selectedEffect, effectOptions);
		editingDisplay.bonusEffectReq = effectIds [selectedEffect];
		pos.x += pos.width;
		selectedEffect = GetPositionOfEffect (editingDisplay.bonusEffect);
		selectedEffect = ImagePack.DrawSelector (pos, "Bonus Effect:", selectedEffect, effectOptions);
		editingDisplay.bonusEffect = effectIds [selectedEffect];
		pos.x -= pos.width;		
		pos.y += ImagePack.fieldHeight;
		editingDisplay.bonusEffectReqConsumed = ImagePack.DrawToggleBox (pos, "Is Consumed?", editingDisplay.bonusEffectReqConsumed);
		pos.width *= 2;

		pos.y += ImagePack.fieldHeight;
		// Save data
		pos.x -= ImagePack.innerMargin;
		pos.width /= 3;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Data")) {
			if (newItem)
				InsertEntry ();
			else
				UpdateEntry ();
			
			state = State.Loaded;
		}
		
		// Delete data
		if (!newItem) {
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Data")) {
				DeleteEntry ();
				newSelectedDisplay = 0;
				state = State.Loaded;
			}
		}
		
		// Cancel editing
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Cancel")) {
			editingDisplay = originalDisplay.Clone ();
			if (newItem)
				state = State.New;
			else
				state = State.Loaded;
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText (pos, result);
		}

		if (!newItem)
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight + 100);
		else
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight);

	}
	
	// Insert new entries into the table
	void InsertEntry ()
	{
		NewResult ("Inserting...");
		// Setup the update query
		string query = "INSERT INTO " + tableName;		
		query += " (" + editingDisplay.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + editingDisplay.FieldList ("?", ", ") + ") ";
		
		int itemID = -1;

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		
		// Update the database
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);

		// If the insert failed, don't insert the spawn marker
		if (itemID != -1) {    
			editingDisplay.id = itemID;
			// Update online table to avoid access the database again			
			editingDisplay.id = itemID;
			editingDisplay.isLoaded = true;
			//Debug.Log("ID:" + itemID + "ID2:" + editingDisplay.id);
			dataRegister.Add (editingDisplay.id, editingDisplay);
			displayKeys.Add (editingDisplay.id);
			newItemCreated = true;
			LoadEffectOptions ();
			
			// Configure the correponding prefab
			CreatePrefab ();
			NewResult ("New entry inserted");
		} else {
			NewResult ("Error occurred, please check the Console");
		}
	}

	// Update existing entries in the table based on the iddemo_table
	void UpdateEntry ()
	{
		NewResult ("Updating...");
		// Setup the update query
		string query = "UPDATE " + tableName;
		query += " SET ";
		query += editingDisplay.UpdateList ();
		query += " WHERE id=?id";

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int));
	
		// Update the database
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
		
		// Update online table to avoid access the database again			
		dataRegister [displayKeys [selectedDisplay]] = editingDisplay;	
		
		// Configure the correponding prefab
		CreatePrefab ();
		NewResult ("Entry updated");			
	}
	
	// Delete entries from the table
	void DeleteEntry ()
	{
		// Remove the prefab
		DeletePrefab ();
		
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, tableName, delete);
		string query = "UPDATE " + tableName + " SET isactive = 0 where id = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Update online table to avoid access the database again		
		dataRegister.Remove (displayKeys [selectedDisplay]);
		displayKeys.Remove (displayKeys [selectedDisplay]);
		if (dataRegister.Count > 0)	{
			LoadSelectList();
			selectedDisplay = -1;
			newSelectedDisplay = 0;
		} else {
			displayList = null;
			dataLoaded = false;
		}
		
		LoadEffectOptions ();
	}
	
	void CreatePrefab ()
	{
		// Only create prefab is duration > 0 or passive
		if (editingDisplay.duration < 1 && !editingDisplay.passive && !editingDisplay.createPrefab)
			return;
		// Configure the correponding prefab
		prefab = new EffectPrefab (editingDisplay);
		prefab.Save (editingDisplay);
	}
	
	void DeletePrefab ()
	{
		prefab = new EffectPrefab (editingDisplay);
		
		if (prefab.Load ())
			prefab.Delete ();
	}

	public int GetPositionOfSkill (int skillID)
	{
		for (int i = 0; i < skillIds.Length; i++) {
			if (skillIds [i] == skillID)
				return i;
		}
		return 0;
	}

	public int GetPositionOfEffect (int effectID)
	{
		for (int i = 0; i < effectIds.Length; i++) {
			if (effectIds [i] == effectID)
				return i;
		}
		return 0;
	}
	
	public static ServerEffects Instance {
		get {
			return instance;
		}
	}
}
