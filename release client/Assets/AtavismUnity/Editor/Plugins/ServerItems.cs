using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Item Configuration
public class ServerItems : AtavismDatabaseFunction
{

	public Dictionary<int, ItemData> dataRegister;
	public ItemData editingDisplay;
	public ItemData originalDisplay;

	public int[] abilityIds = new int[] {-1};
	public string[] abilityOptions = new string[] {"~ none ~"};
	
	public int[] currencyIds = new int[] {-1};
	public string[] currencyOptions = new string[] {"~ none ~"};
	
	public string[] itemTypeOptions = new string[] {"~ none ~"};
	public string[] weaponTypeOptions = new string[] {"~ none ~"};
	public string[] armorTypeOptions = new string[] {"~ none ~"};
	
	public string[] damageOptions = new string[] {"~ none ~"};
	
	public string[] itemEffectTypeOptions = new string[] {"~ none ~"};
	public string[] statOptions = new string[] {"~ none ~"};
	
	public int[] buildObjectIds = new int[] {-1};
	public string[] buildObjectOptions = new string[] {"~ none ~"};
	
	public int[] questIds = new int[] {-1};
	public string[] questOptions = new string[] {"~ none ~"};
	
	public int[] requirementIds = new int[] {-1};
	public string[] requirementOptions = new string[] {"~ none ~"};
	
	public int[] classIds = new int[] {-1};
	public string[] classOptions = new string[] {"~ none ~"};
	
	public int[] raceIds = new int[] {-1};
	public string[] raceOptions = new string[] {"~ none ~"};
	
	public int[] skillIds = new int[] {-1};
	public string[] skillOptions = new string[] {"~ none ~"};
	
	public int[] factionIds = new int[] {-1};
	public string[] factionOptions = new string[] {"~ none ~"};
	
	public int[] ammoIds = new int[] {-1};
	public string[] ammoOptions = new string[] {"~ none ~"};

	// Handles the prefab creation, editing and save
	private ItemPrefab item = null;

	// Filter/Search inputs
    private string currencySearchInput = "";
	private List<string> effectSearchInput = new List<string>();
	
	// Use this for initialization
	public ServerItems ()
	{	
		functionName = "Items";		
		// Database tables name
		tableName = "item_templates";
		functionTitle = "Item Configuration";
		loadButtonLabel = "Load Items";
		notLoadedText = "No Item loaded.";
		// Init
		dataRegister = new Dictionary<int, ItemData> ();
					
		editingDisplay = new ItemData ();	
		originalDisplay = new ItemData ();
	}

	public override void Activate()
	{
		linkedTablesLoaded = false;
	}

	public void LoadAbilityOptions ()
	{
		string query = "SELECT id, name FROM abilities where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			abilityOptions = new string[rows.Count + 1];
			abilityOptions [optionsId] = "~ none ~"; 
			abilityIds = new int[rows.Count + 1];
			abilityIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				abilityOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				abilityIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}
	
	public void LoadCurrencyOptions ()
	{
		string query = "SELECT id, name FROM currencies where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			currencyOptions = new string[rows.Count + 1];
			currencyOptions [optionsId] = "~ none ~"; 
			currencyIds = new int[rows.Count + 1];
			currencyIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				currencyOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				currencyIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}
	
	public void LoadDamageOptions ()
	{
		if (!dataLoaded) {
			// Read all entries from the table
			string query = "SELECT name FROM damage_type where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
			
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			int optionsId = 0;
			if ((rows != null) && (rows.Count > 0)) {
				damageOptions = new string[rows.Count];
				foreach (Dictionary<string,string> data in rows) {
					optionsId++;
					damageOptions [optionsId - 1] = data ["name"]; 
				}
			}
		}
	}
	
	public void LoadStatOptions ()
	{
		if (!dataLoaded) {
			// Read all entries from the table
			string query = "SELECT name FROM stat where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
			
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			int optionsId = 0;
			if ((rows != null) && (rows.Count > 0)) {
				statOptions = new string[rows.Count + 1];
				statOptions [optionsId] = "~ none ~"; 
				foreach (Dictionary<string,string> data in rows) {
					optionsId++;
					statOptions [optionsId] = data ["name"]; 
				}
			}
		}
	}
	
	public void LoadBuildObjectOptions ()
	{
		string query = "SELECT id, name FROM build_object_template where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			buildObjectOptions = new string[rows.Count + 1];
			buildObjectOptions [optionsId] = "~ none ~"; 
			buildObjectIds = new int[rows.Count + 1];
			buildObjectIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				buildObjectOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				buildObjectIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadQuestOptions ()
	{
		string query = "SELECT id, name FROM quests where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			questOptions = new string[rows.Count + 1];
			questOptions [optionsId] = "~ none ~"; 
			questIds = new int[rows.Count + 1];
			questIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				questOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				questIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadSkillOptions ()
	{
		string query = "SELECT id, name FROM skills where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			skillOptions = new string[rows.Count + 1];
			skillOptions [optionsId] = "~ none ~"; 
			skillIds = new int[rows.Count + 1];
			skillIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				skillOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				skillIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadFactionOptions ()
	{
		string query = "SELECT id, name FROM factions where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			factionOptions = new string[rows.Count + 1];
			factionOptions [optionsId] = "~ none ~"; 
			factionIds = new int[rows.Count + 1];
			factionIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				factionOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				factionIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}

	// Load Database Data
	public override void Load ()
	{
		if (!dataLoaded) {
			// Clean old data
			dataRegister.Clear ();
			displayKeys.Clear ();

			// Read all entries from the table
			string query = "SELECT " + originalDisplay.GetFieldsString() + " FROM " + tableName + " where isactive = 1";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
		
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			if ((rows!=null) && (rows.Count > 0)) {
				foreach (Dictionary<string,string> data in rows) {
					//foreach(string key in data.Keys)
					//	Debug.Log("Name[" + key + "]:" + data[key]);
					//return;
					ItemData display = new ItemData ();

					display.id = int.Parse (data ["id"]);
					display.name = data ["name"]; 
					display.icon = data ["icon"]; 
					display.category = data ["category"]; 
					display.subcategory = data ["subcategory"]; 
					display.itemType = data ["itemType"]; 
					display.subType = data ["subType"]; 
					display.slot = data ["slot"]; 
					display.display = data ["display"];
					display.itemQuality = int.Parse (data ["itemQuality"]); 
					display.binding = int.Parse (data ["binding"]); 
					display.isUnique = bool.Parse (data ["isUnique"]); 
					display.stackLimit = int.Parse (data ["stackLimit"]);
					display.purchaseCurrency = int.Parse (data ["purchaseCurrency"]); 
					display.purchaseCost = int.Parse (data ["purchaseCost"]);
					display.sellable = bool.Parse (data ["sellable"]); 
					display.damage = int.Parse (data ["damage"]);
					display.damageType = data ["damageType"]; 
					display.delay = float.Parse (data ["delay"]);
					display.toolTip = data ["toolTip"]; 
					for (int i = 1; i <= display.maxEffectEntries; i++) {
						string effectType = data ["effect" + i + "type"]; 
						if (effectType != null && effectType != "") {
							string effectName = data ["effect" + i + "name"];
							string effectValue = data ["effect" + i + "value"];
							ItemEffectEntry entry = new ItemEffectEntry(effectType, effectName, effectValue);
							display.effects.Add(entry);
						}
					}
											
					display.isLoaded = true;
					//Debug.Log("Name:" + display.name  + "=[" +  display.id  + "]");
					dataRegister.Add (display.id, display);
					displayKeys.Add (display.id);
				}
				LoadSelectList ();
			}
			dataLoaded = true;
			foreach (ItemData itemData in dataRegister.Values) {
				LoadItemTemplateOptions (itemData);
			}
		}
	}
	
	void LoadItemTemplateOptions (ItemData itemData)
	{
		// Read all entries from the table
		string query = "SELECT " + new ItemTemplateOptionEntry().GetFieldsString() + " FROM item_templates_options where item_id = " 
			+ itemData.id + " AND isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		//Debug.Log("#Rows:"+rows.Count);
		// Read all the data
		if ((rows != null) && (rows.Count > 0)) {
			foreach (Dictionary<string,string> data in rows) {
				ItemTemplateOptionEntry display = new ItemTemplateOptionEntry ();
				display.id = int.Parse (data ["id"]);
				display.editor_option_type_id = int.Parse (data ["editor_option_type_id"]);
				display.editor_option_choice_type_id = data ["editor_option_choice_type_id"];
				display.required_value = int.Parse (data ["required_value"]);
				itemData.itemTemplateOptions.Add(display);
			}
		}
	}
	
	public void LoadSelectList ()
	{
		//string[] selectList = new string[dataRegister.Count];
		displayList = new string[dataRegister.Count];
		int i = 0;
		foreach (int displayID in dataRegister.Keys) {
			//selectList [i] = displayID + ". " + dataRegister [displayID].name;
			displayList [i] = displayID + ". " + dataRegister [displayID].name;
			i++;
		}
		//displayList = new Combobox(selectList);
	}	
	
	
	// Draw the loaded list
	public override  void DrawLoaded (Rect box)
	{	
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
								
		if (dataRegister.Count <= 0) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "You must create an Item before edit it.");		
			return;
		}
		

		// Draw the content database info
		ImagePack.DrawLabel (pos.x, pos.y, "Edit Item Template");
		
		//****
		if (newItemCreated) {
			newItemCreated = false;
			LoadSelectList ();
			newSelectedDisplay = displayKeys.Count - 1;
			// Debug.Log ("New Item Created:" + newSelectedDisplay);
		}
		
		// Draw data Editor
		if (newSelectedDisplay != selectedDisplay) {
			// Debug.Log ("(Load)I:" + newSelectedDisplay + "C:" + displayKeys.Count + "Dl:" + displayList.listBox.Length + "St:" + state);
			selectedDisplay = newSelectedDisplay;	
			int displayKey = displayKeys [selectedDisplay];
			editingDisplay = dataRegister [displayKey];	
			originalDisplay = editingDisplay.Clone();			
		} 

		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.x -= ImagePack.innerMargin;
		pos.y -= ImagePack.innerMargin;
		pos.width += ImagePack.innerMargin;
		
		if (state != State.Loaded) {
			pos.x += ImagePack.innerMargin;
			pos.width /= 2;
			//Draw super magical compound object.
			newSelectedDisplay = ImagePack.DrawDynamicPartialListSelector(pos, "Search Filter: ", ref entryFilterInput, selectedDisplay, displayList);
			
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight * 1.5f;
			ImagePack.DrawLabel (pos.x, pos.y, "Template Properties:");
			pos.y += ImagePack.fieldHeight * 0.75f;
		}
		
		DrawEditor (pos, false);
		
		pos.y -= ImagePack.fieldHeight;
		//pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
	}

	public override void CreateNewData ()
	{
		editingDisplay = new ItemData ();
		originalDisplay = new ItemData ();
		selectedDisplay = -1;
	}

	// Edit or Create
	public override void DrawEditor (Rect box, bool newItem)
	{
		// Setup the layout
		Rect pos = box;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;

		if (!linkedTablesLoaded) {	
			LoadAbilityOptions();
			LoadCurrencyOptions();
			LoadDamageOptions();
			LoadStatOptions();
			LoadBuildObjectOptions();
			LoadQuestOptions();
			LoadSkillOptions();
			LoadFactionOptions();
			//LoadRequirementOptions();
			itemTypeOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Item Type", false);
			weaponTypeOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Weapon Type", false);
			armorTypeOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Armor Type", false);
			itemEffectTypeOptions = ServerOptionChoices.LoadAtavismChoiceOptions("Item Effect Type", true);
			ServerOptionChoices.LoadAtavismChoiceOptions("Requirement", false, out requirementIds, out requirementOptions);
			ServerOptionChoices.LoadAtavismChoiceOptions("Race", false, out raceIds, out raceOptions);
			ServerOptionChoices.LoadAtavismChoiceOptions("Class", false, out classIds, out classOptions);
			ServerOptionChoices.LoadAtavismChoiceOptions("Ammo Type", false, out ammoIds, out ammoOptions);
			linkedTablesLoaded = true;
		}

		// Draw the content database info
		if (newItem) {
			ImagePack.DrawLabel (pos.x, pos.y, "Create a new item");
			pos.y += ImagePack.fieldHeight;
			
			if (editingDisplay.itemType == "") {
				foreach(string itemTypeOption in itemTypeOptions) {
					if (ImagePack.DrawButton (pos.x, pos.y, itemTypeOption)) {
						editingDisplay.itemType = itemTypeOption;
					}
					pos.y += ImagePack.fieldHeight;
				}
				
				pos.y += ImagePack.fieldHeight;
				//Build prefabs button
				if (ImagePack.DrawButton (pos.x, pos.y, "Generate Prefabs")) {
					GenerateAllPrefabs();
				}
				
				return;
			}
		}

		editingDisplay.name = ImagePack.DrawField (pos, "Name:", editingDisplay.name, 0.8f);
		pos.y += ImagePack.fieldHeight;
		pos.width /= 2;
		if (editingDisplay.itemType != "Weapon" && editingDisplay.itemType != "Armor") {
			string[] options = new string[itemTypeOptions.Length-2];
			int optionPos = 0;
			foreach (string option in itemTypeOptions) {
				if (option != "Weapon" && option != "Armor") {
					options[optionPos++] = option;
				}
			}
			editingDisplay.itemType = ImagePack.DrawSelector (pos, "Item Type:", editingDisplay.itemType, options);
		} else {
			ImagePack.DrawText (pos, "Item Type: " + editingDisplay.itemType);
		}
		pos.x += pos.width;
		editingDisplay.icon = ImagePack.DrawSpriteAsset (pos, "Icon:", editingDisplay.icon);		
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		if (editingDisplay.itemType == "Weapon")
			editingDisplay.subType = ImagePack.DrawSelector (pos, "Sub-Type:", editingDisplay.subType, weaponTypeOptions);
		else if (editingDisplay.itemType == "Armor")
			editingDisplay.subType = ImagePack.DrawSelector (pos, "Sub-Type:", editingDisplay.subType, armorTypeOptions);
		pos.width *= 2;
		pos.y += 1.3f*ImagePack.fieldHeight;
		if ((editingDisplay.itemType == "Weapon") || (editingDisplay.itemType == "Armor")) {
			pos.width *= 0.7f;
			editingDisplay.display = ImagePack.DrawGameObject(pos, "Equipment Display:", editingDisplay.display, 0.6f);
			pos.width /= 0.7f;
		}
		pos.width /= 2;
		pos.y += 1.5f*ImagePack.fieldHeight;
		/*editingDisplay.category = ImagePack.DrawField (pos, "Category:", editingDisplay.category);
		pos.x += pos.width;
		editingDisplay.subcategory = ImagePack.DrawField (pos, "Sub-category:", editingDisplay.subcategory);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;*/
		if ((editingDisplay.itemType == "Weapon") || (editingDisplay.itemType == "Armor")) {
			if (editingDisplay.itemType == "Weapon")
				editingDisplay.slot = ImagePack.DrawSelector (pos, "Slot:", editingDisplay.slot, editingDisplay.slotWeaponOptions);
			else
				editingDisplay.slot = ImagePack.DrawSelector (pos, "Slot:", editingDisplay.slot, editingDisplay.slotArmorOptions);

			if (editingDisplay.itemType == "Weapon") {
				pos.y += ImagePack.fieldHeight;
				editingDisplay.damageType = ImagePack.DrawSelector (pos, "Damage Type:", editingDisplay.damageType, damageOptions); 
				pos.x += pos.width;
				editingDisplay.damage = ImagePack.DrawField (pos, "Damage:", editingDisplay.damage);
				pos.x -= pos.width;
				pos.y += ImagePack.fieldHeight;				
				editingDisplay.delay = ImagePack.DrawField (pos, "Delay:", editingDisplay.delay);
			}
			pos.y += ImagePack.fieldHeight;
		} else if (editingDisplay.itemType == "Ammo") {
			int ammoID = 0;
			int.TryParse(editingDisplay.slot, out ammoID);
			int selectedAmmo = GetOptionPosition (ammoID, ammoIds);
			selectedAmmo = ImagePack.DrawSelector (pos, "Ammo Type:", selectedAmmo, ammoOptions);
			editingDisplay.slot = ammoIds [selectedAmmo].ToString();
			pos.x += pos.width;
			editingDisplay.damage = ImagePack.DrawField (pos, "Damage:", editingDisplay.damage);
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
			
		}
		editingDisplay.itemQuality = ImagePack.DrawSelector (pos, "Item Quality:", editingDisplay.itemQuality - 1, editingDisplay.itemQualityOptions) + 1;
		pos.x += pos.width;
		editingDisplay.binding = ImagePack.DrawSelector (pos, "Binding:", editingDisplay.binding, editingDisplay.bindingOptions);
		pos.x -= pos.width;
		//pos.x += pos.width;
		pos.y += ImagePack.fieldHeight;
		int selectedCurrency = GetOptionPosition(editingDisplay.purchaseCurrency, currencyIds);
		//selectedCurrency = ImagePack.DrawSelector (pos, "Currency:", selectedCurrency, currencyOptions);
		selectedCurrency = ImagePack.DrawDynamicFilteredListSelector(pos, "Currency: ", ref currencySearchInput, selectedCurrency, currencyOptions);
		editingDisplay.purchaseCurrency = currencyIds[selectedCurrency];
		//pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.purchaseCost = ImagePack.DrawField (pos, "Purchase Cost:", editingDisplay.purchaseCost);
		pos.y += ImagePack.fieldHeight;
		editingDisplay.sellable = ImagePack.DrawToggleBox (pos, "Is Item Sellable?", editingDisplay.sellable);
		pos.x += pos.width;
		editingDisplay.isUnique = ImagePack.DrawToggleBox (pos, "Is Item Unique?", editingDisplay.isUnique);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		if (editingDisplay.itemType == "Bag") {
			editingDisplay.stackLimit = ImagePack.DrawField (pos, "Slots:", editingDisplay.stackLimit);
		} else {
			editingDisplay.stackLimit = ImagePack.DrawField (pos, "Stack Limit:", editingDisplay.stackLimit);
		}
		
		pos.y += ImagePack.fieldHeight;
		pos.width *= 2;
		editingDisplay.toolTip = ImagePack.DrawField (pos, "Tool Tip:", editingDisplay.toolTip, 0.75f, 60);
		
		pos.y += 2.5f*ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos. x, pos.y, "Item Effects");
		pos.y += ImagePack.fieldHeight;
		
		for (int i = 0; i < editingDisplay.maxEffectEntries; i++) {
			if (editingDisplay.effects.Count > i) {
				pos.width = pos.width / 2;
				editingDisplay.effects[i].itemEffectType = ImagePack.DrawSelector (pos, "Effect " + (i+1) 
					+ " Type:", editingDisplay.effects[i].itemEffectType, itemEffectTypeOptions);
				
				// Generate search string if none exists
				if (effectSearchInput.Count <= i) {
					effectSearchInput.Add("");
				}
				string searchString = effectSearchInput[i];
				
				if (editingDisplay.effects[i].itemEffectType == "Stat") {
					//pos.x -= pos.width;
					pos.y += ImagePack.fieldHeight;
					//editingDisplay.effects[i].itemEffectName = ImagePack.DrawSelector (pos, "Stat:", editingDisplay.effects[i].itemEffectName, statOptions);
					editingDisplay.effects[i].itemEffectName = ImagePack.DrawDynamicFilteredListSelector(pos, "Stat: ", ref searchString, editingDisplay.effects[i].itemEffectName, statOptions);
					//pos.x += pos.width;
					pos.y += ImagePack.fieldHeight;
					if (editingDisplay.effects[i].itemEffectValue == "")
						editingDisplay.effects[i].itemEffectValue = "0";
					editingDisplay.effects[i].itemEffectValue = ImagePack.DrawField (pos, "Value:", editingDisplay.effects[i].itemEffectValue);
				} else if (editingDisplay.effects[i].itemEffectType == "UseAbility") {
					if (editingDisplay.effects[i].itemEffectValue == "")
						editingDisplay.effects[i].itemEffectValue = "0";
					//pos.x -= pos.width;
					pos.y += ImagePack.fieldHeight;
					int selectedAbility = GetOptionPosition(int.Parse(editingDisplay.effects[i].itemEffectValue), abilityIds);
					//selectedAbility = ImagePack.DrawSelector (pos, "Ability:", selectedAbility, abilityOptions);
					selectedAbility = ImagePack.DrawDynamicFilteredListSelector(pos, "Ability: ", ref searchString, selectedAbility, abilityOptions);
					editingDisplay.effects[i].itemEffectValue = abilityIds[selectedAbility].ToString();
				} else if (editingDisplay.effects[i].itemEffectType == "AutoAttack") {
					if (editingDisplay.effects[i].itemEffectValue == "")
						editingDisplay.effects[i].itemEffectValue = "0";
					pos.y += ImagePack.fieldHeight;
					int selectedAbility = GetOptionPosition(int.Parse(editingDisplay.effects[i].itemEffectValue), abilityIds);
					//selectedAbility = ImagePack.DrawSelector (pos, "Ability:", selectedAbility, abilityOptions);
					selectedAbility = ImagePack.DrawDynamicFilteredListSelector(pos, "Ability: ", ref searchString, selectedAbility, abilityOptions);
					editingDisplay.effects[i].itemEffectValue = abilityIds[selectedAbility].ToString();
				} else if (editingDisplay.effects[i].itemEffectType == "Currency") {
					if (editingDisplay.effects[i].itemEffectValue == "")
						editingDisplay.effects[i].itemEffectValue = "0";
					pos.y += ImagePack.fieldHeight;
					selectedCurrency = GetOptionPosition(int.Parse(editingDisplay.effects[i].itemEffectValue), currencyIds);
					//selectedCurrency = ImagePack.DrawSelector (pos, "Currency:", selectedCurrency, currencyOptions);
					selectedCurrency = ImagePack.DrawDynamicFilteredListSelector(pos, "Currency: ", ref searchString, selectedCurrency, currencyOptions);
					editingDisplay.effects[i].itemEffectValue = currencyIds[selectedCurrency].ToString();
				} else if (editingDisplay.effects[i].itemEffectType == "ClaimObject") {
					if (editingDisplay.effects[i].itemEffectValue == "")
						editingDisplay.effects[i].itemEffectValue = "0";
					pos.y += ImagePack.fieldHeight;
					int selectedBuildObject = GetOptionPosition (int.Parse(editingDisplay.effects[i].itemEffectValue), buildObjectIds);
					//selectedBuildObject = ImagePack.DrawSelector (pos, "Build Object:", selectedBuildObject, buildObjectOptions);
					selectedBuildObject = ImagePack.DrawDynamicFilteredListSelector(pos, "Build Object: ", ref searchString, selectedBuildObject, buildObjectOptions);
					editingDisplay.effects[i].itemEffectValue = "" + buildObjectIds [selectedBuildObject];
				} else if (editingDisplay.effects[i].itemEffectType == "CreateClaim") {
					pos.x += pos.width;
					editingDisplay.effects[i].itemEffectValue = ImagePack.DrawField (pos, "Size:", editingDisplay.effects[i].itemEffectValue);
					pos.x -= pos.width;
				} else if (editingDisplay.effects[i].itemEffectType == "StartQuest") {
					if (editingDisplay.effects[i].itemEffectValue == "")
						editingDisplay.effects[i].itemEffectValue = "0";
					pos.y += ImagePack.fieldHeight;
					int selectedQuest = GetOptionPosition(int.Parse(editingDisplay.effects[i].itemEffectValue), questIds);
					//selectedQuest = ImagePack.DrawSelector (pos, "Quest:", selectedQuest, questOptions);
					selectedQuest = ImagePack.DrawDynamicFilteredListSelector(pos, "Quest: ", ref searchString, selectedQuest, questOptions);
					editingDisplay.effects[i].itemEffectValue = questIds[selectedQuest].ToString();
				} else if (editingDisplay.effects[i].itemEffectType == "UseAmmo") {
					pos.y += ImagePack.fieldHeight;
					int ammoID = 0;
					int.TryParse(editingDisplay.effects [i].itemEffectName, out ammoID);
					int selectedAmmo = GetOptionPosition (ammoID, ammoIds);
					selectedAmmo = ImagePack.DrawSelector (pos, "Ammo Type:", selectedAmmo, ammoOptions);
					editingDisplay.effects [i].itemEffectName = ammoIds [selectedAmmo].ToString();
				} else {
					pos.x += pos.width;
					editingDisplay.effects[i].itemEffectValue = ImagePack.DrawField (pos, "Value:", editingDisplay.effects[i].itemEffectValue);
					pos.x -= pos.width;
				}
				
				// Save back the search string
				effectSearchInput[i] = searchString;
				
				pos.x += pos.width;
				pos.y += ImagePack.fieldHeight;
				if (ImagePack.DrawButton (pos.x, pos.y, "Remove Effect")) {
					editingDisplay.effects.RemoveAt(i);
				}
				pos.x -= pos.width;
				//pos.x -= pos.width;
				pos.y += ImagePack.fieldHeight;
				pos.width = pos.width * 2;
			}
		}
		if (editingDisplay.effects.Count < editingDisplay.maxEffectEntries) {
			if (ImagePack.DrawButton (pos.x, pos.y, "Add Item Effect")) {
				ItemEffectEntry itemEffectEntry = new ItemEffectEntry("", "", "");
				editingDisplay.effects.Add(itemEffectEntry);
			}
		}
		
		// Requirements area
		pos.y += 1.5f * ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Requirements");
		pos.y += 1.5f * ImagePack.fieldHeight;
		pos.width /= 2;
		for (int i = 0; i < editingDisplay.itemTemplateOptions.Count; i++) {
			int selectedRequirement = GetOptionPosition (editingDisplay.itemTemplateOptions [i].editor_option_type_id, requirementIds);
			selectedRequirement = ImagePack.DrawSelector (pos, "Type:", selectedRequirement, requirementOptions);
			editingDisplay.itemTemplateOptions[i].editor_option_type_id = requirementIds [selectedRequirement];
			pos.x += pos.width;
			if (requirementOptions[selectedRequirement] == "Race") {
				int raceID = 0;
				int.TryParse(editingDisplay.itemTemplateOptions [i].editor_option_choice_type_id, out raceID);
				int selectedRace = GetOptionPosition (raceID, raceIds);
				selectedRace = ImagePack.DrawSelector (pos, "Race:", selectedRace, raceOptions);
				editingDisplay.itemTemplateOptions[i].editor_option_choice_type_id = raceIds [selectedRace].ToString();
			} else if (requirementOptions[selectedRequirement] == "Class") {
				int classID = 0;
				int.TryParse(editingDisplay.itemTemplateOptions [i].editor_option_choice_type_id, out classID);
				int selectedClass = GetOptionPosition (classID, classIds);
				selectedClass = ImagePack.DrawSelector (pos, "Class:", selectedClass, classOptions);
				editingDisplay.itemTemplateOptions[i].editor_option_choice_type_id = classIds [selectedClass].ToString();
			} else if (requirementOptions[selectedRequirement] == "Level") {
				editingDisplay.itemTemplateOptions[i].required_value = ImagePack.DrawField (pos, "Level:", editingDisplay.itemTemplateOptions[i].required_value);
			} else if (requirementOptions[selectedRequirement] == "Skill Level") {
				int skillID = 0;
				int.TryParse(editingDisplay.itemTemplateOptions [i].editor_option_choice_type_id, out skillID);
				int selectedSkill = GetOptionPosition (skillID, skillIds);
				selectedSkill = ImagePack.DrawSelector (pos, "Skill:", selectedSkill, skillOptions);
				editingDisplay.itemTemplateOptions[i].editor_option_choice_type_id = skillIds [selectedSkill].ToString();
				pos.y += ImagePack.fieldHeight;
				editingDisplay.itemTemplateOptions[i].required_value = ImagePack.DrawField (pos, "Level:", editingDisplay.itemTemplateOptions[i].required_value);
			} else if (requirementOptions[selectedRequirement] == "Stat") {
				editingDisplay.itemTemplateOptions[i].editor_option_choice_type_id = ImagePack.DrawSelector (pos, "Stat:", editingDisplay.itemTemplateOptions[i].editor_option_choice_type_id, statOptions);
				pos.y += ImagePack.fieldHeight;
				editingDisplay.itemTemplateOptions[i].required_value = ImagePack.DrawField (pos, "Value:", editingDisplay.itemTemplateOptions[i].required_value);
			} else if (requirementOptions[selectedRequirement] == "Faction") {
				int factionID = 0;
				int.TryParse(editingDisplay.itemTemplateOptions [i].editor_option_choice_type_id, out factionID);
				int selectedFaction = GetOptionPosition (factionID, factionIds);
				selectedFaction = ImagePack.DrawSelector (pos, "Faction:", selectedFaction, factionOptions);
				editingDisplay.itemTemplateOptions[i].editor_option_choice_type_id = factionIds [selectedFaction].ToString();
				pos.y += ImagePack.fieldHeight;
				//editingDisplay.itemTemplateOptions[i].required_value = ImagePack.DrawField (pos, "Reputation:", editingDisplay.itemTemplateOptions[i].required_value);
				int selectedStance = FactionData.GetPositionOfStance(editingDisplay.itemTemplateOptions[i].required_value);
				selectedStance = ImagePack.DrawSelector (pos, "Stance:", selectedStance, FactionData.stanceOptions);
				editingDisplay.itemTemplateOptions[i].required_value = FactionData.stanceValues[selectedStance];
			}
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Remove Requirement")) {
				if (editingDisplay.itemTemplateOptions[i].id > 0)
					editingDisplay.itemOptionsToBeDeleted.Add(editingDisplay.itemTemplateOptions[i].id);
				editingDisplay.itemTemplateOptions.RemoveAt(i);
			}
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
		}
		if (ImagePack.DrawButton (pos.x, pos.y, "Add Requirement")) {
			editingDisplay.itemTemplateOptions.Add (new ItemTemplateOptionEntry (-1, -1, ""));
		}
		pos.width *= 2;
		
		// Save data		
		pos.x -= ImagePack.innerMargin;
		pos.y += 3f * ImagePack.fieldHeight;
		pos.width /= 3;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Data")) {
			if (newItem)
				InsertEntry ();
			else
				UpdateEntry ();
			
			state = State.Loaded;
		}
		
		// Delete data
		if (!newItem) {
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Data")) {
				DeleteEntry ();
				LoadSelectList ();
				newSelectedDisplay = 0;
				// Debug.Log ("(Delete)I:" + newSelectedDisplay);
				state = State.Loaded;
			}
		}
		
		// Cancel editing
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Cancel")) {
			editingDisplay = originalDisplay.Clone();
			if (newItem)
				state = State.New;
			else
				state = State.Loaded;
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}

		if (!newItem)
			EnableScrollBar(pos.y - box.y + ImagePack.fieldHeight + 100);
		else
			EnableScrollBar(pos.y - box.y + ImagePack.fieldHeight);

	}
	
	// Insert new entries into the table
	void InsertEntry ()
	{
		NewResult("Inserting...");
		// Setup the update query
		string query = "INSERT INTO " + tableName;
		query += " (" + editingDisplay.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + editingDisplay.FieldList ("?", ", ") + ") ";
		
		int itemID = -1;

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		
		// Update the database
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);

		// If the insert failed, don't insert the spawn marker
		if (itemID != -1) {          
			// Update online table to avoid access the database again			
			editingDisplay.id = itemID;
			// Insert the Requirements
			foreach (ItemTemplateOptionEntry entry in editingDisplay.itemTemplateOptions) {
				if (entry.editor_option_type_id != -1) {
					entry.itemID = itemID;
					InsertRequirement (entry);
				}
			}
			editingDisplay.isLoaded = true;
			//Debug.Log("ID:" + itemID + "ID2:" + editingDisplay.id);
			dataRegister.Add (editingDisplay.id, editingDisplay);
			displayKeys.Add (editingDisplay.id);
			newItemCreated = true;

			// Configure the correponding prefab
			CreatePrefab();
			NewResult("New entry inserted");
		} else {
			NewResult("Error occurred, please check the Console");
		}
	}
	
	void InsertRequirement(ItemTemplateOptionEntry entry)
	{
		string query = "INSERT INTO item_templates_options";		
		query += " (" + entry.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + entry.FieldList ("?", ", ") + ") ";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		
		int itemID = -1;
		itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);
		
		entry.id = itemID;
	}

	// Update existing entries in the table based on the iddemo_table
	void UpdateEntry ()
	{
		NewResult("Updating...");
		// Setup the update query
		string query = "UPDATE " + tableName;
		query += " SET ";
		query += editingDisplay.UpdateList ();
		query += " WHERE id=?id";

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int));
	
		// Update the database
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
		
		// Insert/Update the requirements
		foreach (ItemTemplateOptionEntry entry in editingDisplay.itemTemplateOptions) {
			if (entry.editor_option_type_id != -1) {
				if (entry.id < 1) {
					// This is a new entry, insert it
					entry.itemID = editingDisplay.id;
					InsertRequirement (entry);
				} else {
					// This is an existing entry, update it
					entry.itemID = editingDisplay.id;
					UpdateRequirement (entry);
				}
			}
		}
		
		// Delete any requirements that are tagged for deletion
		foreach (int requirementID in editingDisplay.itemOptionsToBeDeleted) {
			DeleteRequirement(requirementID);
		}
				
		// Update online table to avoid access the database again			
		dataRegister [displayKeys [selectedDisplay]] = editingDisplay;	
		
		// Configure the correponding prefab
		CreatePrefab();
		NewResult("Entry updated");	
	}
	
	void UpdateRequirement (ItemTemplateOptionEntry entry)
	{
		string query = "UPDATE item_templates_options";		
		query += " SET ";
		query += entry.UpdateList ();
		query += " WHERE id=?id";
		
		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in entry.fields.Keys) {
			update.Add (entry.fieldToRegister (field));       
		}
		update.Add (new Register ("id", "?id", MySqlDbType.Int32, entry.id.ToString (), Register.TypesOfField.Int));
		
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
	}
	
	void DeleteRequirement(int requirementID) {
		string query = "UPDATE item_templates_options SET isactive = 0 where id = " + requirementID;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
	}
	
	// Delete entries from the table
	void DeleteEntry ()
	{
		// Remove the prefab
		DeletePrefab();
		// Delete the database entry
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, tableName, delete);
		string query = "UPDATE " + tableName + " SET isactive = 0 where id = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Update online table to avoid access the database again		
		dataRegister.Remove (displayKeys [selectedDisplay]);
		displayKeys.Remove (displayKeys [selectedDisplay]);
		if (dataRegister.Count > 0)	{
			LoadSelectList();
			selectedDisplay = -1;
			newSelectedDisplay = 0;
		} else {
			displayList = null;
			dataLoaded = false;
		}
	}
	
	void GenerateAllPrefabs() {
		if (!dataLoaded) {
			Load ();
		}
		
		// Delete all existing prefabs?
		ItemPrefab.DeleteAllPrefabs();
		
		foreach (ItemData itemData in dataRegister.Values) {
			item = new ItemPrefab(itemData);
			item.Save(itemData);
		}
	}

	void CreatePrefab()
	{
		// Configure the correponding prefab
		item = new ItemPrefab(editingDisplay);
		item.Save(editingDisplay);
	}

	void DeletePrefab()
	{
		item = new ItemPrefab(editingDisplay);

		if (item.Load())
			item.Delete();
	}
	
}
