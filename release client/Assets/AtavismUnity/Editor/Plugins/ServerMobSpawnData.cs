using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Crafting Recipes Configuration
public class ServerMobSpawnData : AtavismDatabaseFunction 
{
	
	public Dictionary<int, MobSpawnData> dataRegister;
	public MobSpawnData editingDisplay;
	public MobSpawnData originalDisplay;
	
	public int[] itemIds = new int[] {-1};
	public string[] itemsList = new string[] {"~ none ~"};
	public int[] mobIds = new int[] {-1};
	public string[] mobOptions = new string[] {"~ none ~"};
	public int[] merchantIds = new int[] {-1};
	public string[] merchantOptions = new string[] {"~ none ~"};
	public int[] lootIds = new int[] {-1};
	public string[] lootOptions = new string[] {"~ none ~"};

	// Use this for initialization
	public ServerMobSpawnData ()
	{	
		functionName = "Mob Spawn Data";
		// Database tables name
	    tableName = "spawn_data";
		functionTitle =  "Mob Spawn Configuration";
		loadButtonLabel =  "Load Mob Spawn Data";
		notLoadedText =  "No Spawn Data loaded.";
		// Init
		dataRegister = new Dictionary<int, MobSpawnData> ();

		editingDisplay = new MobSpawnData ();	
		originalDisplay = new MobSpawnData ();	
	}

	public override void Activate()
	{
		linkedTablesLoaded = false;
	}

	private void LoadItemList ()
	{
		string query = "SELECT id, name FROM item_templates where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows!=null) && (rows.Count > 0)) {
			itemsList = new string[rows.Count + 1];
			itemsList [optionsId] = "~ none ~"; 
			itemIds = new int[rows.Count + 1];
			itemIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				itemsList [optionsId] = data ["id"] + ":" + data ["name"]; 
				itemIds[optionsId] = int.Parse(data ["id"]);
			}
		}
	}
	
	public void LoadMobOptions ()
	{
		string query = "SELECT id, name FROM mob_templates where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			mobOptions = new string[rows.Count + 1];
			mobOptions [optionsId] = "~ none ~"; 
			mobIds = new int[rows.Count + 1];
			mobIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				mobOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				mobIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadMerchantOptions ()
	{
		string query = "SELECT id, name FROM merchant_tables where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			merchantOptions = new string[rows.Count + 1];
			merchantOptions [optionsId] = "~ none ~"; 
			merchantIds = new int[rows.Count + 1];
			merchantIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				merchantOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				merchantIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	public void LoadLootOptions ()
	{
		string query = "SELECT id, name FROM loot_tables where isactive = 1";
		
		// If there is a row, clear it.
		if (rows != null)
			rows.Clear ();
		
		// Load data
		rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
		// Read data
		int optionsId = 0;
		if ((rows != null) && (rows.Count > 0)) {
			lootOptions = new string[rows.Count + 1];
			lootOptions [optionsId] = "~ none ~"; 
			lootIds = new int[rows.Count + 1];
			lootIds [optionsId] = -1;
			foreach (Dictionary<string,string> data in rows) {
				optionsId++;
				lootOptions [optionsId] = data ["id"] + ":" + data ["name"]; 
				lootIds [optionsId] = int.Parse (data ["id"]);
			}
		}
	}
	
	// Load Database Data
	public override void Load ()
	{
		if (!dataLoaded) {
			// Clean old data
			dataRegister.Clear ();
			displayKeys.Clear ();

			// Read all entries from the table
			string query = "SELECT " + originalDisplay.GetFieldsString() + " FROM " + tableName + " where isactive = 1 and instance is NULL";
			
			// If there is a row, clear it.
			if (rows != null)
				rows.Clear ();
		
			// Load data
			rows = DatabasePack.LoadData (DatabasePack.contentDatabasePrefix, query);
			//Debug.Log("#Rows:"+rows.Count);
			// Read all the data
			if ((rows!=null) && (rows.Count > 0)) {
				foreach (Dictionary<string,string> data in rows) {
					//foreach(string key in data.Keys)
					//	Debug.Log("Name[" + key + "]:" + data[key]);
					//return;
					MobSpawnData display = new MobSpawnData ();
					display.id = int.Parse (data ["id"]);
					display.name = data["name"];
					display.mobTemplate = int.Parse(data["mobTemplate"]);
					display.numSpawns = int.Parse(data["numSpawns"]);
					display.spawnRadius = int.Parse(data["spawnRadius"]);
					display.respawnTime = int.Parse(data["respawnTime"]);
					display.corpseDespawnTime = int.Parse(data["corpseDespawnTime"]);
					display.combat = bool.Parse(data["combat"]);
					display.roamRadius = int.Parse(data["roamRadius"]);
					display.startsQuests = data["startsQuests"];
					display.endsQuests = data["endsQuests"];
					display.startsDialogues = data["startsDialogues"];
					display.baseAction = data["baseAction"];
					display.weaponSheathed = bool.Parse(data["weaponSheathed"]);
					display.merchantTable = int.Parse(data["merchantTable"]);
					display.questOpenLootTable = int.Parse(data["questOpenLootTable"]);
					display.isChest = bool.Parse(data["isChest"]);
					display.pickupItem = int.Parse(data["pickupItem"]);
											
					display.isLoaded = true;
					//Debug.Log("Name:" + display.name  + "=[" +  display.id  + "]");
					dataRegister.Add (display.id, display);
					displayKeys.Add (display.id);
				}
				LoadSelectList();
			}
			dataLoaded = true;
		}
	}
	
	public void LoadSelectList() 
	{
			//string[] selectList = new string[dataRegister.Count];
			displayList =  new string[dataRegister.Count];
			int i = 0;
			foreach (int displayID in dataRegister.Keys) {
				//selectList [i] = displayID + ". " + dataRegister [displayID].name;
				displayList [i] = displayID + ". " + dataRegister [displayID].name;
				i++;
			}
			//displayList = new Combobox(selectList);
	}	
	
	// Draw the loaded list
	public override void DrawLoaded (Rect box)
	{	
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;
								
		if (dataRegister.Count <= 0) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawLabel (pos.x, pos.y, "You must create a Mob Spawn Data before editing it.");		
			return;
		}
		

		// Draw the content database info
		ImagePack.DrawLabel (pos.x, pos.y, "Edit Mob Spawn");
		
		if (newItemCreated) {
			newItemCreated = false;
			LoadSelectList();
			newSelectedDisplay = displayKeys.Count - 1;
		}
		
		// Draw data Editor
		if (newSelectedDisplay != selectedDisplay) {
			selectedDisplay = newSelectedDisplay;	
			int displayKey = displayKeys [selectedDisplay];
			editingDisplay = dataRegister [displayKey];		
			originalDisplay = editingDisplay.Clone();
		} 
		
		pos.y += ImagePack.fieldHeight * 1.5f;
		pos.x -= ImagePack.innerMargin;
		pos.y -= ImagePack.innerMargin;
		pos.width += ImagePack.innerMargin;
		
		if (state != State.Loaded) {
			pos.x += ImagePack.innerMargin;
			pos.width /= 2;
			//Draw super magical compound object.
			newSelectedDisplay = ImagePack.DrawDynamicPartialListSelector(pos, "Search Filter: ", ref entryFilterInput, selectedDisplay, displayList);
			
			pos.width *= 2;
			pos.y += ImagePack.fieldHeight * 1.5f;
			ImagePack.DrawLabel (pos.x, pos.y, "Mob Spawn Properties:");
			pos.y += ImagePack.fieldHeight * 0.75f;
		}
		
		DrawEditor (pos, false);
		
		pos.y -= ImagePack.fieldHeight;
		//pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
	}
	
	public override void CreateNewData()
	{
		editingDisplay = new MobSpawnData ();		
		originalDisplay = new MobSpawnData ();	
		selectedDisplay = -1;
	}
	// Edit or Create
	public override void DrawEditor (Rect box, bool newItem)
	{
		if (!linkedTablesLoaded) {
			// Load items
			LoadItemList();
			LoadMobOptions();
			LoadMerchantOptions();
			LoadLootOptions();
			linkedTablesLoaded = true;
		}
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;

		// Draw the content database info		
		if (newItem) {
			ImagePack.DrawLabel (pos.x, pos.y, "Create new mob spawn data");		
			pos.y += ImagePack.fieldHeight;
		}
		
		editingDisplay.name = ImagePack.DrawField (pos, "Name:", editingDisplay.name, 0.5f);
		pos.width /= 2;
		pos.y += ImagePack.fieldHeight;
		int selectedMob = GetPositionOfMob(editingDisplay.mobTemplate);
		selectedMob = ImagePack.DrawSelector (pos, "Mob Template:", selectedMob, mobOptions);
		editingDisplay.mobTemplate = mobIds[selectedMob];
		pos.y += ImagePack.fieldHeight;
		editingDisplay.numSpawns = ImagePack.DrawField (pos, "Num Spawns:", editingDisplay.numSpawns);
		pos.x += pos.width;
		editingDisplay.spawnRadius = ImagePack.DrawField (pos, "Spawn Radius:", editingDisplay.spawnRadius);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.respawnTime = ImagePack.DrawField (pos, "Respawn (ms):", editingDisplay.respawnTime);
		pos.x += pos.width;
		editingDisplay.corpseDespawnTime = ImagePack.DrawField (pos, "Corspe Despawn (ms):", editingDisplay.corpseDespawnTime);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.combat = ImagePack.DrawToggleBox (pos, "Has Combat?", editingDisplay.combat);
		pos.x += pos.width;
		editingDisplay.roamRadius = ImagePack.DrawField (pos, "Roam Radius (m):", editingDisplay.roamRadius);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		
		// TODO: Quest and Dialogue area
		
		int selectedMerchant = GetPositionOfMerchantTable(editingDisplay.merchantTable);
		selectedMerchant = ImagePack.DrawSelector (pos, "Merchant Table:", selectedMerchant, merchantOptions);
		editingDisplay.merchantTable = merchantIds[selectedMerchant];
		pos.y += ImagePack.fieldHeight;
		int selectedLoot = GetPositionOfLootTable(editingDisplay.questOpenLootTable);
		selectedLoot = ImagePack.DrawSelector (pos, "Loot Table:", selectedLoot, lootOptions);
		editingDisplay.questOpenLootTable = lootIds[selectedLoot];
		pos.y += ImagePack.fieldHeight;
		editingDisplay.isChest = ImagePack.DrawToggleBox (pos, "Is a Chest?", editingDisplay.isChest);
		pos.x += pos.width;
		int selectedItem = GetPositionOfItem(editingDisplay.pickupItem);
		selectedItem = ImagePack.DrawSelector (pos, "Pickup Item:", selectedItem, itemsList);
		editingDisplay.pickupItem = itemIds[selectedItem];
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		/*selectedItem = GetPositionOfItem(editingDisplay.resultItemID);
		selectedItem = ImagePack.DrawSelector (pos, "Creates Item:", selectedItem, itemsList);
		editingDisplay.resultItemID = itemIds[selectedItem];
		pos.x += pos.width;
		editingDisplay.resultItemCount = ImagePack.DrawField (pos, "Count:", editingDisplay.resultItemCount);
		pos.x -= pos.width;
		pos.y += 1.5f * ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Requirements");		
		pos.y += 1.5f * ImagePack.fieldHeight;
		int selectedSkill = GetPositionOfSkill (editingDisplay.skillID);
		selectedSkill = ImagePack.DrawSelector (pos, "Skill:", selectedSkill, skillOptions);
		editingDisplay.skillID = skillIds [selectedSkill];
		pos.x += pos.width;
		editingDisplay.skillLevelReq = ImagePack.DrawField (pos, "Skill Level:", editingDisplay.skillLevelReq);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.stationReq = ImagePack.DrawSelector (pos, "Station Req:", editingDisplay.stationReq, stationOptions);
		pos.x += pos.width;
		editingDisplay.qualityChangeable = ImagePack.DrawToggleBox (pos, "Changes Quality:", editingDisplay.qualityChangeable);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.allowDyes = ImagePack.DrawToggleBox (pos, "Allows Dyes:", editingDisplay.allowDyes);
		pos.x += pos.width;
		editingDisplay.allowEssences = ImagePack.DrawToggleBox (pos, "Allows Essences:", editingDisplay.allowEssences);
		pos.x -= pos.width;
		pos.y += ImagePack.fieldHeight;
		editingDisplay.layoutReq = ImagePack.DrawToggleBox (pos, "Must Match Layout:", editingDisplay.layoutReq);
		pos.y += 1.5f * ImagePack.fieldHeight;
		ImagePack.DrawLabel (pos.x, pos.y, "Items Required:");		
		pos.y += 1.5f * ImagePack.fieldHeight;
		for (int i = 0; i < editingDisplay.maxEntries; i++) {
			if (editingDisplay.entries.Count <= i)
				editingDisplay.entries.Add(new RecipeComponentEntry(-1, 1));
			if (i == 0) {
				ImagePack.DrawText(pos, "Row 1");
				pos.y += ImagePack.fieldHeight;
			} else if (i == 4) {
				ImagePack.DrawText(pos, "Row 2");
				pos.y += ImagePack.fieldHeight;
			} else if (i == 8) {
				ImagePack.DrawText(pos, "Row 3");
				pos.y += ImagePack.fieldHeight;
			} else if (i == 12) {
				ImagePack.DrawText(pos, "Row 4");
				pos.y += ImagePack.fieldHeight;
			}
			selectedItem = GetPositionOfItem(editingDisplay.entries[i].itemId);
			selectedItem = ImagePack.DrawSelector (pos, "Item " + (i+1) + ":", selectedItem, itemsList);
			editingDisplay.entries[i].itemId = itemIds[selectedItem];
			pos.x += pos.width;
			editingDisplay.entries[i].count = ImagePack.DrawField (pos, "Count:", editingDisplay.entries[i].count);
			pos.x -= pos.width;
			pos.y += ImagePack.fieldHeight;
		}*/
		pos.width = pos.width * 2;
		
		// Save data		
		pos.x -= ImagePack.innerMargin;
		pos.y += 1.4f * ImagePack.fieldHeight;
		pos.width /=3;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Data")) {
			if (newItem)
				InsertEntry ();
			else
				UpdateEntry ();
			
			state = State.Loaded;
		}
		
		// Delete data
		if (!newItem) {
			pos.x += pos.width;
			if (ImagePack.DrawButton (pos.x, pos.y, "Delete Data")) {
				DeleteEntry ();
				newSelectedDisplay = 0;
				state = State.Loaded;
			}
		}
		
		// Cancel editing
		pos.x += pos.width;
		if (ImagePack.DrawButton (pos.x, pos.y, "Cancel")) {
			editingDisplay = originalDisplay.Clone();
			if (newItem)
				state = State.New;
			else
				state = State.Loaded;
		}
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}
		
		if (!newItem)
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight + 100);
		else
			EnableScrollBar (pos.y - box.y + ImagePack.fieldHeight);
	}
	
	// Insert new entries into the table
	void InsertEntry ()
	{
		NewResult("Inserting...");
		// Setup the update query
		string query = "INSERT INTO " + tableName;		
		query += " (" + editingDisplay.FieldList ("", ", ") + ") ";
		query += "VALUES ";
		query += " (" + editingDisplay.FieldList ("?", ", ") + ") ";
		
		int mobID = -1;

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) {
			update.Add (editingDisplay.fieldToRegister (field));       
		}
		
		// Update the database
		mobID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, update);

		// If the insert failed, don't insert the spawn marker
		if (mobID != -1) {          
			// Update online table to avoid access the database again			
			editingDisplay.id = mobID;
			editingDisplay.isLoaded = true;
			//Debug.Log("ID:" + mobID + "ID2:" + editingDisplay.id);
			dataRegister.Add (editingDisplay.id, editingDisplay);
			displayKeys.Add (editingDisplay.id);
			newItemCreated = true;
			NewResult("New entry inserted");
		} else {
			NewResult("Error occurred, please check the Console");
		}
	}

	// Update existing entries in the table based on the iddemo_table
	void UpdateEntry ()
	{
		NewResult("Updating...");
		// Setup the update query
		string query = "UPDATE " + tableName;
		query += " SET ";
		query += editingDisplay.UpdateList ();
		query += " WHERE id=?id";

		// Setup the register data		
		List<Register> update = new List<Register> ();
		foreach (string field in editingDisplay.fields.Keys) { 
			update.Add (editingDisplay.fieldToRegister (field));       
		}
			update.Add (new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int));
	
		// Update the database
		DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, update);
				
		// Update online table to avoid access the database again			
		dataRegister [displayKeys [selectedDisplay]] = editingDisplay;	
		NewResult("Entry updated");			
	}
	
	// Delete entries from the table
	void DeleteEntry ()
	{
		//Register delete = new Register ("id", "?id", MySqlDbType.Int32, editingDisplay.id.ToString (), Register.TypesOfField.Int);
		//DatabasePack.Delete (DatabasePack.contentDatabasePrefix, tableName, delete);
		string query = "UPDATE " + tableName + " SET isactive = 0 where id = " + editingDisplay.id;
		DatabasePack.Update(DatabasePack.contentDatabasePrefix, query, new List<Register> ());
		
		// Update online table to avoid access the database again		
		dataRegister.Remove (displayKeys [selectedDisplay]);
		displayKeys.Remove (displayKeys [selectedDisplay]);
		if (dataRegister.Count > 0)	{
			LoadSelectList();
			selectedDisplay = -1;
			newSelectedDisplay = 0;
		} else {
			displayList = null;
			dataLoaded = false;
		}
	}
	
	private int GetPositionOfItem(int itemId) {
		for (int i = 0; i < itemIds.Length; i++) {
			if (itemIds[i] == itemId)
				return i;
		}
		return 0;
	}
	
	private int GetPositionOfMob (int mobID)
	{
		for (int i = 0; i < mobIds.Length; i++) {
			if (mobIds [i] == mobID)
				return i;
		}
		return 0;
	}
	
	private int GetPositionOfMerchantTable (int merchantTableID)
	{
		for (int i = 0; i < merchantIds.Length; i++) {
			if (merchantIds [i] == merchantTableID)
				return i;
		}
		return 0;
	}
	
	private int GetPositionOfLootTable (int lootTableID)
	{
		for (int i = 0; i < lootIds.Length; i++) {
			if (lootIds [i] == lootTableID)
				return i;
		}
		return 0;
	}
}
