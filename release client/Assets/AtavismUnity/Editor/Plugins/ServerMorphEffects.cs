﻿using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Effects Configuration
public class ServerMorphEffects : ServerEffectType
{

	public string effectType = "Morph";
	public string[] effectTypeOptions = new string[] {"MorphEffect"};

	// Use this for initialization
	public ServerMorphEffects ()
	{	
	}
	
	public override void LoadOptions(EffectsData editingDisplay, bool newItem) {
	}
	
	// Edit or Create
	public override Rect DrawEditor (Rect pos, bool newItem, EffectsData editingDisplay, out bool showTimeFields)
	{
		editingDisplay.stringValue1 = ImagePack.DrawGameObject (pos, "Model: ", editingDisplay.stringValue1, 0.75f);
		pos.y += ImagePack.fieldHeight;
		showTimeFields = true;
		return pos;
	}
	
	public override string EffectType {
		get {
			return effectType;
		}
	}
	
	public override string[] EffectTypeOptions {
		get {
			return effectTypeOptions;
		}
	}
}
