using UnityEngine;
using UnityEditor;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

// Handles the Effects Configuration
public class ServerResourceNodes : AtavismFunction
{
	// Result text
	public string result = "";
	public float resultTimeout = -1;
	public float resultDuration = 5;

	bool showConfirmDelete = false;
	int bulkEntryCount = 100;

	// Use this for initialization
	public ServerResourceNodes ()
	{	
		functionName = "Resource Nodes";		
		
		showConfirmDelete = false;		
	}

	public override void Activate()
	{
	}
	
	public void LoadSelectList() 
	{
			//string[] selectList = new string[dataRegister.Count];
			/*displayList =  new string[dataRegister.Count];
			int i = 0;
			foreach (int displayID in dataRegister.Keys) {
				//selectList [i] = displayID + ". " + dataRegister [displayID].name;
				displayList [i] = displayID + ". " + dataRegister [displayID].name;
				i++;
			}*/
			//displayList = new Combobox(selectList);
	}
	
	// Edit or Create
	public override void Draw(Rect box)
	{
		
		// Setup the layout
		Rect pos = box;
		pos.x += ImagePack.innerMargin;
		pos.y += ImagePack.innerMargin;
		pos.width -= ImagePack.innerMargin;
		pos.height = ImagePack.fieldHeight;

		// Draw the content database info
		//pos.y += ImagePack.fieldHeight;
		
		ImagePack.DrawLabel (pos.x, pos.y, "Save Resource Nodes");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "Save the Resource Nodes in the current scene by clicking below.");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "When the Resource Nodes have been saved, save your scene again.");
		pos.y += ImagePack.fieldHeight;
		bulkEntryCount = ImagePack.DrawField(pos, "Bulk Entry Count:", bulkEntryCount);
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Save Nodes")) {
			GetSceneResourceNodes();
			showConfirmDelete = false;
		}
		
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "To delete all node data from this Scene in the Database, click");
		pos.y += ImagePack.fieldHeight;
		ImagePack.DrawText(pos, "Delete Node Data.");
		pos.y += ImagePack.fieldHeight;
		if (ImagePack.DrawButton (pos.x, pos.y, "Delete Node Data")) {
			showConfirmDelete = true;
		}
		
		if (showConfirmDelete) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, "Are you sure?");
			pos.y += ImagePack.fieldHeight;
			if (ImagePack.DrawButton (pos.x, pos.y, "Yes, delete")) {
				ClearSavedInstanceNodeData();
				showConfirmDelete = false;
			}
		}
		
		
		if (resultTimeout != -1 && resultTimeout > Time.realtimeSinceStartup) {
			pos.y += ImagePack.fieldHeight;
			ImagePack.DrawText(pos, result);
		}

	}
	
	void GetSceneResourceNodes() {
		Debug.Log("Resource saving start time: " + System.DateTime.Now);
		NewResult ("Saving...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		List<ResourceNode> preexistingNodes = new List<ResourceNode>();
		List<ResourceNode> newNodes = new List<ResourceNode>();
		
		// Find all resource nodes in the scene
		int itemID = -1;
		UnityEngine.Object[] resourceNodes = FindObjectsOfType<ResourceNode>();
		string query = "";
		foreach (UnityEngine.Object resourceNodeObj in resourceNodes) {
			ResourceNode resourceNode = (ResourceNode)resourceNodeObj;
			if (resourceNode.id > 0) {
				preexistingNodes.Add(resourceNode);
			} else {
				newNodes.Add(resourceNode);
			}
		}
		
		string insertNodeQuery = "INSERT INTO resource_node_template (name, skill, skillLevel, skillLevelMax, weaponReq, equipped, gameObject, coordEffect" 
			+ ", instance, respawnTime, locX, locY, locZ, harvestCount, harvestTimeReq)" + " values ";
			
		query = insertNodeQuery;
		int insertCount = 0;
		
		Debug.Log("Before Insert time: " + System.DateTime.Now);
		foreach (ResourceNode resourceNode in newNodes) {
			// Insert the new resource node
			string coordEffect = "";
			if (resourceNode.harvestCoordEffect != null)
				coordEffect = resourceNode.harvestCoordEffect.name;
			/*query = "INSERT INTO resource_node_template (name, skill, skillLevel, skillLevelMax, weaponReq, equipped, gameObject, coordEffect" 
				+ ", instance, respawnTime, locX, locY, locZ, harvestCount, harvestTimeReq)" + " values ('" + resourceNode.name + "'," + resourceNode.skillType + "," 
				+ resourceNode.reqSkillLevel + "," + resourceNode.skillLevelMax + "," + "'" + resourceNode.harvestTool + "'," 
				+ resourceNode.ToolMustBeEquipped + ",'','" + coordEffect + "','" + instance + "',"
				+ resourceNode.refreshDuration + "," + resourceNode.transform.position.x + "," + resourceNode.transform.position.y + "," 
				+ resourceNode.transform.position.z + "," + resourceNode.resourceCount + "," + resourceNode.timeToHarvest + ")";*/
			if (insertCount > 0)
				query += ",";
			query += "('" + resourceNode.name + "'," + resourceNode.skillType + "," 
					+ resourceNode.reqSkillLevel + "," + resourceNode.skillLevelMax + "," + "'" + resourceNode.harvestTool + "'," 
					+ resourceNode.ToolMustBeEquipped + ",'','" + coordEffect + "'," + instanceID + ","
					+ resourceNode.refreshDuration + "," + resourceNode.transform.position.x + "," + resourceNode.transform.position.y + "," 
					+ resourceNode.transform.position.z + "," + resourceNode.resourceCount + "," + resourceNode.timeToHarvest + ")";	
					
			insertCount++;
			if (insertCount == bulkEntryCount) {
				DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
				insertCount = 0;
				query = insertNodeQuery;
			}
			/*itemID = DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, new List<Register>(), true);
			
			if (itemID == -1) {
				// Insert failed, set message
				return;
			}*/
			resourceNode.id = itemID;
			EditorUtility.SetDirty( resourceNode );
			
			/*foreach (ResourceDrop drop in resourceNode.resources) {
				// Insert the resource drops
				if (drop.item == null) {
					Debug.LogWarning("ResourceDrop skipped for resourceNode " + itemID + " as it has no item");
					continue;
				}
				query = "INSERT INTO resource_drop (resource_template, item, min, max, chance)"
					+ " values(" + itemID + "," + drop.item.TemplateId + "," + drop.min + "," + drop.max + "," + drop.chance + ")";
				DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, new List<Register>(), true);
			}*/
			Debug.Log("Finished inserting node at: " + System.DateTime.Now);
		}
		
		if (insertCount != 0) {
			DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
			insertCount = 0;
		}
		
		// Now run a select of all id's in the database for this scene
		query = "Select id, locX, locY, locZ from resource_node_template where instance = " + instanceID;
		try
		{
			List<int> dropsToDelete = new List<int>();
			// Open the connection
			DatabasePack.Connect(DatabasePack.contentDatabasePrefix);
			if (DatabasePack.con.State.ToString() != "Open")
				DatabasePack.con.Open();
			// Use the connections to fetch data
			using (DatabasePack.con)
			{
				using (MySqlCommand cmd = new MySqlCommand(query, DatabasePack.con))
				{
					// Execute the query
					MySqlDataReader data = cmd.ExecuteReader();
					// If there are columns
					if (data.HasRows) {
						int fieldsCount = data.FieldCount;
						while (data.Read()) {
							Vector3 loc = new Vector3(data.GetFloat("locX"), data.GetFloat("locY"), data.GetFloat("locZ"));
							foreach (ResourceNode resourceNode in newNodes) {
								if (Math.Abs(resourceNode.transform.position.x - loc.x) < 0.1f && Math.Abs(resourceNode.transform.position.y - loc.y) < 0.1f
								    && Math.Abs(resourceNode.transform.position.z - loc.z) < 0.1f) {
									resourceNode.id = data.GetInt32("id");
									break;
								}
							}
						}
					}
					data.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			NewResult ("Error occurred deleting old entries");
		}
		finally
		{
		}
		
		// Now go through each node again and save the drops
		insertNodeQuery = "INSERT INTO resource_drop (resource_template, item, min, max, chance) values ";
		query = insertNodeQuery;
		Debug.Log("Before drop insert time: " + System.DateTime.Now);
		foreach (ResourceNode resourceNode in newNodes) {
			foreach (ResourceDrop drop in resourceNode.resources) {
				// Insert the resource drops
				if (drop == null || drop.item == null) {
					Debug.LogWarning("ResourceDrop skipped for resourceNode " + itemID + " as it has no item");
					continue;
				}
				if (insertCount > 0)
					query += ",";
				query += "(" + resourceNode.id + "," + drop.item.TemplateId + "," + drop.min + "," + drop.max + "," + drop.chance + ")";
				insertCount++;
				if (insertCount == bulkEntryCount) {
					DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
					insertCount = 0;
					query = insertNodeQuery;
				}
			}
		}
		
		if (insertCount != 0) {
			DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
			insertCount = 0;
		}
		Debug.Log("After drop insert time: " + System.DateTime.Now);
		
		// Now deal with preexisting nodes
		foreach (ResourceNode resourceNode in preexistingNodes) {
			// Update the existing resource node
			string coordEffect = "";
			if (resourceNode.harvestCoordEffect != null)
				coordEffect = resourceNode.harvestCoordEffect.name;
			query = "UPDATE resource_node_template set name = '" + resourceNode.name + "', skill = " + resourceNode.skillType + ", skillLevel = " 
				+ resourceNode.reqSkillLevel + ", skillLevelMax = " + resourceNode.skillLevelMax + ", weaponReq = '" + resourceNode.harvestTool
				+ "', equipped = " + resourceNode.ToolMustBeEquipped + ", gameObject = '', coordEffect = '" + coordEffect + "', instance = " 
				+ instanceID + ", respawnTime = " + resourceNode.refreshDuration + ", locX = " + resourceNode.transform.position.x 
				+ ", locY = " + resourceNode.transform.position.y + ", locZ = " + resourceNode.transform.position.z + ", harvestCount = "
				+ resourceNode.resourceCount + ", harvestTimeReq = " + resourceNode.timeToHarvest + " where id = " + resourceNode.id;
			DatabasePack.Update (DatabasePack.contentDatabasePrefix, query, new List<Register>());
			
			// Delete the existing resource drops for this node - not the most efficient, but easier to do
			Register delete = new Register ("resource_template", "?resource_template", MySqlDbType.Int32, resourceNode.id.ToString (), Register.TypesOfField.Int);
			DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "resource_drop", delete, false);
			
			// And re-insert them
			foreach (ResourceDrop drop in resourceNode.resources) {
				// Insert the resource drops
				if (drop == null || drop.item == null) {
					Debug.LogWarning("ResourceDrop skipped for resourceNode " + itemID + " as it has no item");
					continue;
				}
				query = "INSERT INTO resource_drop (resource_template, item, min, max, chance)"
					+ " values(" + resourceNode.id + "," + drop.item.TemplateId + "," + drop.min + "," + drop.max + "," + drop.chance + ")";
				DatabasePack.Insert (DatabasePack.contentDatabasePrefix, query, new List<Register>());
			}
		}
		
		NewResult ("Nodes saved");
	}
	
	void ClearSavedInstanceNodeData() {
		NewResult ("Deleting...");
		string instance = EditorApplication.currentScene;
		string[] split = instance.Split(char.Parse("/"));
		instance = split[split.Length -1];
		split = instance.Split(char.Parse("."));
		instance = split[0];
		
		int instanceID = ServerInstances.GetInstanceID(instance);
		
		DatabasePack.con = DatabasePack.Connect(DatabasePack.contentDatabasePrefix);
		// Get all resource nodes that are to be deleted
		/*string query = "Select id from resource_node_template where instance = '" + instance + "'";
		
		try
		{
			List<int> dropsToDelete = new List<int>();
			// Open the connection
			if (DatabasePack.con.State.ToString() != "Open")
				DatabasePack.con.Open();
			// Use the connections to fetch data
			using (DatabasePack.con)
			{
				using (MySqlCommand cmd = new MySqlCommand(query, DatabasePack.con))
				{
					// Execute the query
					MySqlDataReader data = cmd.ExecuteReader();
					// If there are columns
					if (data.HasRows) {
						int fieldsCount = data.FieldCount;
						while (data.Read()) {
							dropsToDelete.Add(data.GetInt32("id"));
						}
					}
					data.Dispose();
				}
			}
			
			foreach (int id in dropsToDelete) {
				Register delete = new Register ("resource_template", "?resource_template", MySqlDbType.Int32, id.ToString (), Register.TypesOfField.Int);
				DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "resource_drop", delete, false);
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			NewResult ("Error occurred deleting old entries");
			return;
		}
		finally
		{
		}*/
		
		string query = "delete from resource_drop where resource_template IN (Select id from resource_node_template where instance = " + instanceID + ")";
		DatabasePack.ExecuteNonQuery(DatabasePack.contentDatabasePrefix, query);
		
		// Now delete all resource nodes in this instance
		Register delete2 = new Register ("instance", "?instance", MySqlDbType.Int32, instanceID.ToString(), Register.TypesOfField.Int);
		DatabasePack.Delete (DatabasePack.contentDatabasePrefix, "resource_node_template", delete2, true);
		
		// Finally, reset the id for each node in the scene
		UnityEngine.Object[] resourceNodes = FindObjectsOfType<ResourceNode>();
		foreach (UnityEngine.Object resourceNodeObj in resourceNodes) {
			ResourceNode resourceNode = (ResourceNode)resourceNodeObj;
			resourceNode.id = -1;
			EditorUtility.SetDirty(resourceNode);
		}
		NewResult ("Node data deleted");	
	}
	
	public void NewResult(string resultMessage) {
		result = resultMessage;
		resultTimeout = Time.realtimeSinceStartup + resultDuration;
	}
	
}
