﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CreateControlPoints))]
public class EditorCreateControlPoints : Editor
{
    bool showMapityNotLoadedMessage = false;

    public override void OnInspectorGUI() {
        CreateControlPoints myTarget = (CreateControlPoints)target;

        if (GUILayout.Button("Create Control Points")) {
            myTarget.CreatePoints();
        }
        DrawDefaultInspector();
    }
}