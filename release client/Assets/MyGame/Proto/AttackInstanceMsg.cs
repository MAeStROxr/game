// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class AttackInstanceMsg : Table {
  public static AttackInstanceMsg GetRootAsAttackInstanceMsg(ByteBuffer _bb) { return GetRootAsAttackInstanceMsg(_bb, new AttackInstanceMsg()); }
  public static AttackInstanceMsg GetRootAsAttackInstanceMsg(ByteBuffer _bb, AttackInstanceMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public AttackInstanceMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public bool Processed { get { int o = __offset(4); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateProcessed(bool processed) { int o = __offset(4); if (o != 0) { bb.Put(o + bb_pos, (byte)(processed ? 1 : 0)); return true; } else { return false; } }
  public bool IsAbility { get { int o = __offset(6); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsAbility(bool isAbility) { int o = __offset(6); if (o != 0) { bb.Put(o + bb_pos, (byte)(isAbility ? 1 : 0)); return true; } else { return false; } }
  public bool Calculated { get { int o = __offset(8); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateCalculated(bool calculated) { int o = __offset(8); if (o != 0) { bb.Put(o + bb_pos, (byte)(calculated ? 1 : 0)); return true; } else { return false; } }
  public UnitMsg SrcUnit { get { return GetSrcUnit(new UnitMsg()); } }
  public UnitMsg GetSrcUnit(UnitMsg obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public UnitMsg TgtUnit { get { return GetTgtUnit(new UnitMsg()); } }
  public UnitMsg GetTgtUnit(UnitMsg obj) { int o = __offset(12); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int UnitAbilityID { get { int o = __offset(14); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateUnitAbilityID(int unitAbilityID) { int o = __offset(14); if (o != 0) { bb.PutInt(o + bb_pos, unitAbilityID); return true; } else { return false; } }
  public bool IsMelee { get { int o = __offset(16); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsMelee(bool isMelee) { int o = __offset(16); if (o != 0) { bb.Put(o + bb_pos, (byte)(isMelee ? 1 : 0)); return true; } else { return false; } }
  public bool IsCounter { get { int o = __offset(18); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsCounter(bool isCounter) { int o = __offset(18); if (o != 0) { bb.Put(o + bb_pos, (byte)(isCounter ? 1 : 0)); return true; } else { return false; } }
  public bool Missed { get { int o = __offset(20); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateMissed(bool missed) { int o = __offset(20); if (o != 0) { bb.Put(o + bb_pos, (byte)(missed ? 1 : 0)); return true; } else { return false; } }
  public bool Critical { get { int o = __offset(22); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateCritical(bool critical) { int o = __offset(22); if (o != 0) { bb.Put(o + bb_pos, (byte)(critical ? 1 : 0)); return true; } else { return false; } }
  public bool Stunned { get { int o = __offset(24); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateStunned(bool Stunned) { int o = __offset(24); if (o != 0) { bb.Put(o + bb_pos, (byte)(Stunned ? 1 : 0)); return true; } else { return false; } }
  public bool Silenced { get { int o = __offset(26); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateSilenced(bool Silenced) { int o = __offset(26); if (o != 0) { bb.Put(o + bb_pos, (byte)(Silenced ? 1 : 0)); return true; } else { return false; } }
  public bool Flanked { get { int o = __offset(28); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateFlanked(bool flanked) { int o = __offset(28); if (o != 0) { bb.Put(o + bb_pos, (byte)(flanked ? 1 : 0)); return true; } else { return false; } }
  public bool Destroyed { get { int o = __offset(30); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateDestroyed(bool Destroyed) { int o = __offset(30); if (o != 0) { bb.Put(o + bb_pos, (byte)(Destroyed ? 1 : 0)); return true; } else { return false; } }
  public float Damage { get { int o = __offset(32); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDamage(float damage) { int o = __offset(32); if (o != 0) { bb.PutFloat(o + bb_pos, damage); return true; } else { return false; } }
  public int Stun { get { int o = __offset(34); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateStun(int stun) { int o = __offset(34); if (o != 0) { bb.PutInt(o + bb_pos, stun); return true; } else { return false; } }
  public int Silent { get { int o = __offset(36); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateSilent(int silent) { int o = __offset(36); if (o != 0) { bb.PutInt(o + bb_pos, silent); return true; } else { return false; } }
  public float DamageTableModifier { get { int o = __offset(38); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDamageTableModifier(float damageTableModifier) { int o = __offset(38); if (o != 0) { bb.PutFloat(o + bb_pos, damageTableModifier); return true; } else { return false; } }
  public float FlankingBonus { get { int o = __offset(40); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateFlankingBonus(float flankingBonus) { int o = __offset(40); if (o != 0) { bb.PutFloat(o + bb_pos, flankingBonus); return true; } else { return false; } }
  public UnitMsg GetUnitList(int j) { return GetUnitList(new UnitMsg(), j); }
  public UnitMsg GetUnitList(UnitMsg obj, int j) { int o = __offset(42); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int UnitListLength { get { int o = __offset(42); return o != 0 ? __vector_len(o) : 0; } }

  public static Offset<AttackInstanceMsg> CreateAttackInstanceMsg(FlatBufferBuilder builder,
      bool processed = false,
      bool isAbility = false,
      bool calculated = false,
      Offset<UnitMsg> srcUnitOffset = default(Offset<UnitMsg>),
      Offset<UnitMsg> tgtUnitOffset = default(Offset<UnitMsg>),
      int unitAbilityID = 0,
      bool isMelee = false,
      bool isCounter = false,
      bool missed = false,
      bool critical = false,
      bool Stunned = false,
      bool Silenced = false,
      bool flanked = false,
      bool Destroyed = false,
      float damage = 0,
      int stun = 0,
      int silent = 0,
      float damageTableModifier = 0,
      float flankingBonus = 0,
      VectorOffset unitListOffset = default(VectorOffset)) {
    builder.StartObject(20);
    AttackInstanceMsg.AddUnitList(builder, unitListOffset);
    AttackInstanceMsg.AddFlankingBonus(builder, flankingBonus);
    AttackInstanceMsg.AddDamageTableModifier(builder, damageTableModifier);
    AttackInstanceMsg.AddSilent(builder, silent);
    AttackInstanceMsg.AddStun(builder, stun);
    AttackInstanceMsg.AddDamage(builder, damage);
    AttackInstanceMsg.AddUnitAbilityID(builder, unitAbilityID);
    AttackInstanceMsg.AddTgtUnit(builder, tgtUnitOffset);
    AttackInstanceMsg.AddSrcUnit(builder, srcUnitOffset);
    AttackInstanceMsg.AddDestroyed(builder, Destroyed);
    AttackInstanceMsg.AddFlanked(builder, flanked);
    AttackInstanceMsg.AddSilenced(builder, Silenced);
    AttackInstanceMsg.AddStunned(builder, Stunned);
    AttackInstanceMsg.AddCritical(builder, critical);
    AttackInstanceMsg.AddMissed(builder, missed);
    AttackInstanceMsg.AddIsCounter(builder, isCounter);
    AttackInstanceMsg.AddIsMelee(builder, isMelee);
    AttackInstanceMsg.AddCalculated(builder, calculated);
    AttackInstanceMsg.AddIsAbility(builder, isAbility);
    AttackInstanceMsg.AddProcessed(builder, processed);
    return AttackInstanceMsg.EndAttackInstanceMsg(builder);
  }

  public static void StartAttackInstanceMsg(FlatBufferBuilder builder) { builder.StartObject(20); }
  public static void AddProcessed(FlatBufferBuilder builder, bool processed) { builder.AddBool(0, processed, false); }
  public static void AddIsAbility(FlatBufferBuilder builder, bool isAbility) { builder.AddBool(1, isAbility, false); }
  public static void AddCalculated(FlatBufferBuilder builder, bool calculated) { builder.AddBool(2, calculated, false); }
  public static void AddSrcUnit(FlatBufferBuilder builder, Offset<UnitMsg> srcUnitOffset) { builder.AddOffset(3, srcUnitOffset.Value, 0); }
  public static void AddTgtUnit(FlatBufferBuilder builder, Offset<UnitMsg> tgtUnitOffset) { builder.AddOffset(4, tgtUnitOffset.Value, 0); }
  public static void AddUnitAbilityID(FlatBufferBuilder builder, int unitAbilityID) { builder.AddInt(5, unitAbilityID, 0); }
  public static void AddIsMelee(FlatBufferBuilder builder, bool isMelee) { builder.AddBool(6, isMelee, false); }
  public static void AddIsCounter(FlatBufferBuilder builder, bool isCounter) { builder.AddBool(7, isCounter, false); }
  public static void AddMissed(FlatBufferBuilder builder, bool missed) { builder.AddBool(8, missed, false); }
  public static void AddCritical(FlatBufferBuilder builder, bool critical) { builder.AddBool(9, critical, false); }
  public static void AddStunned(FlatBufferBuilder builder, bool Stunned) { builder.AddBool(10, Stunned, false); }
  public static void AddSilenced(FlatBufferBuilder builder, bool Silenced) { builder.AddBool(11, Silenced, false); }
  public static void AddFlanked(FlatBufferBuilder builder, bool flanked) { builder.AddBool(12, flanked, false); }
  public static void AddDestroyed(FlatBufferBuilder builder, bool Destroyed) { builder.AddBool(13, Destroyed, false); }
  public static void AddDamage(FlatBufferBuilder builder, float damage) { builder.AddFloat(14, damage, 0); }
  public static void AddStun(FlatBufferBuilder builder, int stun) { builder.AddInt(15, stun, 0); }
  public static void AddSilent(FlatBufferBuilder builder, int silent) { builder.AddInt(16, silent, 0); }
  public static void AddDamageTableModifier(FlatBufferBuilder builder, float damageTableModifier) { builder.AddFloat(17, damageTableModifier, 0); }
  public static void AddFlankingBonus(FlatBufferBuilder builder, float flankingBonus) { builder.AddFloat(18, flankingBonus, 0); }
  public static void AddUnitList(FlatBufferBuilder builder, VectorOffset unitListOffset) { builder.AddOffset(19, unitListOffset.Value, 0); }
  public static VectorOffset CreateUnitListVector(FlatBufferBuilder builder, Offset<UnitMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartUnitListVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static Offset<AttackInstanceMsg> EndAttackInstanceMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<AttackInstanceMsg>(o);
  }
};


}
