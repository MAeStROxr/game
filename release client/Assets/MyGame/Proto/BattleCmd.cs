// automatically generated, do not modify

namespace mygame.proto
{

public enum BattleCmd : sbyte
{
 EXECUTE_FAILURE = 0,
 PLAYER_STARTED_BATTLE = 1,
 PLAYER_JOINED_BATTLE = 2,
 PLAYER_DEPLOYED_ARMY = 3,
 PLAYER_LEFT_BATTLE = 4,
 PLAYER_ENDED_TURN = 5,
 PLAYER_RETREAT_BATTLE = 6,
 BATTLE_OVER = 7,
 BATTLE_COMMENCED = 8,
 UNIT_MOVE = 9,
 UNIT_ATTACK = 10,
 UNIT_ABILITY = 11,
 BATTLE_INFO = 12,
};


}
