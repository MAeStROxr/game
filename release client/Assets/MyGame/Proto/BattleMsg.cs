// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class BattleMsg : Table {
  public static BattleMsg GetRootAsBattleMsg(ByteBuffer _bb) { return GetRootAsBattleMsg(_bb, new BattleMsg()); }
  public static BattleMsg GetRootAsBattleMsg(ByteBuffer _bb, BattleMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public BattleMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID { get { int o = __offset(4); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.PutLong(o + bb_pos, OID); return true; } else { return false; } }
  public BattleCmd Cmd { get { int o = __offset(6); return o != 0 ? (BattleCmd)bb.GetSbyte(o + bb_pos) : BattleCmd.EXECUTE_FAILURE; } }
  public bool MutateCmd(BattleCmd cmd) { int o = __offset(6); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)cmd); return true; } else { return false; } }
  public long SubjectOID { get { int o = __offset(8); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateSubjectOID(long subjectOID) { int o = __offset(8); if (o != 0) { bb.PutLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public ArmyMsg GetArmies(int j) { return GetArmies(new ArmyMsg(), j); }
  public ArmyMsg GetArmies(ArmyMsg obj, int j) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int ArmiesLength { get { int o = __offset(10); return o != 0 ? __vector_len(o) : 0; } }
  public int CurrentFactionID { get { int o = __offset(12); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateCurrentFactionID(int currentFactionID) { int o = __offset(12); if (o != 0) { bb.PutInt(o + bb_pos, currentFactionID); return true; } else { return false; } }
  public BattleActionMsg BattleActionType { get { int o = __offset(14); return o != 0 ? (BattleActionMsg)bb.Get(o + bb_pos) : BattleActionMsg.NONE; } }
  public bool MutateBattleActionType(BattleActionMsg battleAction_type) { int o = __offset(14); if (o != 0) { bb.Put(o + bb_pos, (byte)battleAction_type); return true; } else { return false; } }
  public TTable GetBattleAction<TTable>(TTable obj) where TTable : Table { int o = __offset(16); return o != 0 ? __union(obj, o) : null; }
  public Vec3 Position { get { return GetPosition(new Vec3()); } }
  public Vec3 GetPosition(Vec3 obj) { int o = __offset(18); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }
  public TileMsg GetGridTiles(int j) { return GetGridTiles(new TileMsg(), j); }
  public TileMsg GetGridTiles(TileMsg obj, int j) { int o = __offset(20); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int GridTilesLength { get { int o = __offset(20); return o != 0 ? __vector_len(o) : 0; } }
  public bool IsSiege { get { int o = __offset(22); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateIsSiege(bool isSiege) { int o = __offset(22); if (o != 0) { bb.Put(o + bb_pos, (byte)(isSiege ? 1 : 0)); return true; } else { return false; } }
  public long PointOID { get { int o = __offset(24); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutatePointOID(long pointOID) { int o = __offset(24); if (o != 0) { bb.PutLong(o + bb_pos, pointOID); return true; } else { return false; } }
  public string Text { get { int o = __offset(26); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetTextBytes() { return __vector_as_arraysegment(26); }
  public int BattlePhase { get { int o = __offset(28); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateBattlePhase(int battlePhase) { int o = __offset(28); if (o != 0) { bb.PutInt(o + bb_pos, battlePhase); return true; } else { return false; } }

  public static void StartBattleMsg(FlatBufferBuilder builder) { builder.StartObject(13); }
  public static void AddOID(FlatBufferBuilder builder, long OID) { builder.AddLong(0, OID, 0); }
  public static void AddCmd(FlatBufferBuilder builder, BattleCmd cmd) { builder.AddSbyte(1, (sbyte)cmd, 0); }
  public static void AddSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.AddLong(2, subjectOID, 0); }
  public static void AddArmies(FlatBufferBuilder builder, VectorOffset armiesOffset) { builder.AddOffset(3, armiesOffset.Value, 0); }
  public static VectorOffset CreateArmiesVector(FlatBufferBuilder builder, Offset<ArmyMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartArmiesVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddCurrentFactionID(FlatBufferBuilder builder, int currentFactionID) { builder.AddInt(4, currentFactionID, 0); }
  public static void AddBattleActionType(FlatBufferBuilder builder, BattleActionMsg battleActionType) { builder.AddByte(5, (byte)battleActionType, 0); }
  public static void AddBattleAction(FlatBufferBuilder builder, int battleActionOffset) { builder.AddOffset(6, battleActionOffset, 0); }
  public static void AddPosition(FlatBufferBuilder builder, Offset<Vec3> positionOffset) { builder.AddStruct(7, positionOffset.Value, 0); }
  public static void AddGridTiles(FlatBufferBuilder builder, VectorOffset gridTilesOffset) { builder.AddOffset(8, gridTilesOffset.Value, 0); }
  public static VectorOffset CreateGridTilesVector(FlatBufferBuilder builder, Offset<TileMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartGridTilesVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddIsSiege(FlatBufferBuilder builder, bool isSiege) { builder.AddBool(9, isSiege, false); }
  public static void AddPointOID(FlatBufferBuilder builder, long pointOID) { builder.AddLong(10, pointOID, 0); }
  public static void AddText(FlatBufferBuilder builder, StringOffset textOffset) { builder.AddOffset(11, textOffset.Value, 0); }
  public static void AddBattlePhase(FlatBufferBuilder builder, int battlePhase) { builder.AddInt(12, battlePhase, 0); }
  public static Offset<BattleMsg> EndBattleMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<BattleMsg>(o);
  }
};


}
