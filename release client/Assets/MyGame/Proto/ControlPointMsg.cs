// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class ControlPointMsg : Table {
  public static ControlPointMsg GetRootAsControlPointMsg(ByteBuffer _bb) { return GetRootAsControlPointMsg(_bb, new ControlPointMsg()); }
  public static ControlPointMsg GetRootAsControlPointMsg(ByteBuffer _bb, ControlPointMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public ControlPointMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID { get { int o = __offset(4); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.PutLong(o + bb_pos, OID); return true; } else { return false; } }
  public PointCmd Cmd { get { int o = __offset(6); return o != 0 ? (PointCmd)bb.GetSbyte(o + bb_pos) : PointCmd.POINT_INFO; } }
  public bool MutateCmd(PointCmd cmd) { int o = __offset(6); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)cmd); return true; } else { return false; } }
  public long SubjectOID { get { int o = __offset(8); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateSubjectOID(long subjectOID) { int o = __offset(8); if (o != 0) { bb.PutLong(o + bb_pos, subjectOID); return true; } else { return false; } }
  public ArmyMsg Garrison { get { return GetGarrison(new ArmyMsg()); } }
  public ArmyMsg GetGarrison(ArmyMsg obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public ArmyMsg GetGarrisonedArmies(int j) { return GetGarrisonedArmies(new ArmyMsg(), j); }
  public ArmyMsg GetGarrisonedArmies(ArmyMsg obj, int j) { int o = __offset(12); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int GarrisonedArmiesLength { get { int o = __offset(12); return o != 0 ? __vector_len(o) : 0; } }
  public ArmyMsg GetArmies(int j) { return GetArmies(new ArmyMsg(), j); }
  public ArmyMsg GetArmies(ArmyMsg obj, int j) { int o = __offset(14); return o != 0 ? obj.__init(__indirect(__vector(o) + j * 4), bb) : null; }
  public int ArmiesLength { get { int o = __offset(14); return o != 0 ? __vector_len(o) : 0; } }
  public DataAction Action { get { int o = __offset(16); return o != 0 ? (DataAction)bb.GetSbyte(o + bb_pos) : DataAction.NONE; } }
  public bool MutateAction(DataAction action) { int o = __offset(16); if (o != 0) { bb.PutSbyte(o + bb_pos, (sbyte)action); return true; } else { return false; } }
  public string SubjectName { get { int o = __offset(18); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetSubjectNameBytes() { return __vector_as_arraysegment(18); }
  public long ControllingOID { get { int o = __offset(20); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateControllingOID(long controllingOID) { int o = __offset(20); if (o != 0) { bb.PutLong(o + bb_pos, controllingOID); return true; } else { return false; } }
  public string ControllingName { get { int o = __offset(22); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetControllingNameBytes() { return __vector_as_arraysegment(22); }
  public long EnablingOID { get { int o = __offset(24); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateEnablingOID(long enablingOID) { int o = __offset(24); if (o != 0) { bb.PutLong(o + bb_pos, enablingOID); return true; } else { return false; } }
  public string EnablingName { get { int o = __offset(26); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetEnablingNameBytes() { return __vector_as_arraysegment(26); }
  public bool Capturable { get { int o = __offset(28); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateCapturable(bool capturable) { int o = __offset(28); if (o != 0) { bb.Put(o + bb_pos, (byte)(capturable ? 1 : 0)); return true; } else { return false; } }
  public bool Captured { get { int o = __offset(30); return o != 0 ? 0!=bb.Get(o + bb_pos) : (bool)false; } }
  public bool MutateCaptured(bool captured) { int o = __offset(30); if (o != 0) { bb.Put(o + bb_pos, (byte)(captured ? 1 : 0)); return true; } else { return false; } }
  public string Name { get { int o = __offset(32); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetNameBytes() { return __vector_as_arraysegment(32); }
  public Vec3 Position { get { return GetPosition(new Vec3()); } }
  public Vec3 GetPosition(Vec3 obj) { int o = __offset(34); return o != 0 ? obj.__init(o + bb_pos, bb) : null; }

  public static void StartControlPointMsg(FlatBufferBuilder builder) { builder.StartObject(16); }
  public static void AddOID(FlatBufferBuilder builder, long OID) { builder.AddLong(0, OID, 0); }
  public static void AddCmd(FlatBufferBuilder builder, PointCmd cmd) { builder.AddSbyte(1, (sbyte)cmd, 0); }
  public static void AddSubjectOID(FlatBufferBuilder builder, long subjectOID) { builder.AddLong(2, subjectOID, 0); }
  public static void AddGarrison(FlatBufferBuilder builder, Offset<ArmyMsg> garrisonOffset) { builder.AddOffset(3, garrisonOffset.Value, 0); }
  public static void AddGarrisonedArmies(FlatBufferBuilder builder, VectorOffset garrisonedArmiesOffset) { builder.AddOffset(4, garrisonedArmiesOffset.Value, 0); }
  public static VectorOffset CreateGarrisonedArmiesVector(FlatBufferBuilder builder, Offset<ArmyMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartGarrisonedArmiesVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddArmies(FlatBufferBuilder builder, VectorOffset armiesOffset) { builder.AddOffset(5, armiesOffset.Value, 0); }
  public static VectorOffset CreateArmiesVector(FlatBufferBuilder builder, Offset<ArmyMsg>[] data) { builder.StartVector(4, data.Length, 4); for (int i = data.Length - 1; i >= 0; i--) builder.AddOffset(data[i].Value); return builder.EndVector(); }
  public static void StartArmiesVector(FlatBufferBuilder builder, int numElems) { builder.StartVector(4, numElems, 4); }
  public static void AddAction(FlatBufferBuilder builder, DataAction action) { builder.AddSbyte(6, (sbyte)action, 0); }
  public static void AddSubjectName(FlatBufferBuilder builder, StringOffset subjectNameOffset) { builder.AddOffset(7, subjectNameOffset.Value, 0); }
  public static void AddControllingOID(FlatBufferBuilder builder, long controllingOID) { builder.AddLong(8, controllingOID, 0); }
  public static void AddControllingName(FlatBufferBuilder builder, StringOffset controllingNameOffset) { builder.AddOffset(9, controllingNameOffset.Value, 0); }
  public static void AddEnablingOID(FlatBufferBuilder builder, long enablingOID) { builder.AddLong(10, enablingOID, 0); }
  public static void AddEnablingName(FlatBufferBuilder builder, StringOffset enablingNameOffset) { builder.AddOffset(11, enablingNameOffset.Value, 0); }
  public static void AddCapturable(FlatBufferBuilder builder, bool capturable) { builder.AddBool(12, capturable, false); }
  public static void AddCaptured(FlatBufferBuilder builder, bool captured) { builder.AddBool(13, captured, false); }
  public static void AddName(FlatBufferBuilder builder, StringOffset nameOffset) { builder.AddOffset(14, nameOffset.Value, 0); }
  public static void AddPosition(FlatBufferBuilder builder, Offset<Vec3> positionOffset) { builder.AddStruct(15, positionOffset.Value, 0); }
  public static Offset<ControlPointMsg> EndControlPointMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<ControlPointMsg>(o);
  }
};


}
