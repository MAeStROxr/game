// automatically generated, do not modify

namespace mygame.proto
{

public enum DataAction : sbyte
{
 NONE = 0,
 REPLACE = 1,
 SET = 2,
 GET = 3,
 ADD = 4,
 REMOVE = 5,
};


}
