// automatically generated, do not modify

namespace mygame.proto
{

public enum PointCmd : sbyte
{
 POINT_INFO = 0,
 POINT_CAPTURE = 1,
 POINT_ENABLE = 2,
 POINT_ARMY_GARRISON = 3,
 POINT_UNIT_GARRISON = 4,
 POINT_SIEGE = 5,
 POINT_SIEGE_RESULT = 6,
};


}
