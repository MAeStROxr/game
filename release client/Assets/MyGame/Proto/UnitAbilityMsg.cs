// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class UnitAbilityMsg : Table {
  public static UnitAbilityMsg GetRootAsUnitAbilityMsg(ByteBuffer _bb) { return GetRootAsUnitAbilityMsg(_bb, new UnitAbilityMsg()); }
  public static UnitAbilityMsg GetRootAsUnitAbilityMsg(ByteBuffer _bb, UnitAbilityMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public UnitAbilityMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public UnitMsg Activator { get { return GetActivator(new UnitMsg()); } }
  public UnitMsg GetActivator(UnitMsg obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int AbilityID { get { int o = __offset(6); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateAbilityID(int abilityID) { int o = __offset(6); if (o != 0) { bb.PutInt(o + bb_pos, abilityID); return true; } else { return false; } }
  public TileMsg TargetedTile { get { return GetTargetedTile(new TileMsg()); } }
  public TileMsg GetTargetedTile(TileMsg obj) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public AttackInstanceMsg Att { get { return GetAtt(new AttackInstanceMsg()); } }
  public AttackInstanceMsg GetAtt(AttackInstanceMsg obj) { int o = __offset(10); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static Offset<UnitAbilityMsg> CreateUnitAbilityMsg(FlatBufferBuilder builder,
      Offset<UnitMsg> activatorOffset = default(Offset<UnitMsg>),
      int abilityID = 0,
      Offset<TileMsg> targetedTileOffset = default(Offset<TileMsg>),
      Offset<AttackInstanceMsg> attOffset = default(Offset<AttackInstanceMsg>)) {
    builder.StartObject(4);
    UnitAbilityMsg.AddAtt(builder, attOffset);
    UnitAbilityMsg.AddTargetedTile(builder, targetedTileOffset);
    UnitAbilityMsg.AddAbilityID(builder, abilityID);
    UnitAbilityMsg.AddActivator(builder, activatorOffset);
    return UnitAbilityMsg.EndUnitAbilityMsg(builder);
  }

  public static void StartUnitAbilityMsg(FlatBufferBuilder builder) { builder.StartObject(4); }
  public static void AddActivator(FlatBufferBuilder builder, Offset<UnitMsg> activatorOffset) { builder.AddOffset(0, activatorOffset.Value, 0); }
  public static void AddAbilityID(FlatBufferBuilder builder, int abilityID) { builder.AddInt(1, abilityID, 0); }
  public static void AddTargetedTile(FlatBufferBuilder builder, Offset<TileMsg> targetedTileOffset) { builder.AddOffset(2, targetedTileOffset.Value, 0); }
  public static void AddAtt(FlatBufferBuilder builder, Offset<AttackInstanceMsg> attOffset) { builder.AddOffset(3, attOffset.Value, 0); }
  public static Offset<UnitAbilityMsg> EndUnitAbilityMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<UnitAbilityMsg>(o);
  }
};


}
