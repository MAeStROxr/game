// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class UnitAttackMsg : Table {
  public static UnitAttackMsg GetRootAsUnitAttackMsg(ByteBuffer _bb) { return GetRootAsUnitAttackMsg(_bb, new UnitAttackMsg()); }
  public static UnitAttackMsg GetRootAsUnitAttackMsg(ByteBuffer _bb, UnitAttackMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public UnitAttackMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public UnitMsg Attacking { get { return GetAttacking(new UnitMsg()); } }
  public UnitMsg GetAttacking(UnitMsg obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public UnitMsg Attacked { get { return GetAttacked(new UnitMsg()); } }
  public UnitMsg GetAttacked(UnitMsg obj) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public AttackInstanceMsg Att { get { return GetAtt(new AttackInstanceMsg()); } }
  public AttackInstanceMsg GetAtt(AttackInstanceMsg obj) { int o = __offset(8); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public int ShotsFired { get { int o = __offset(10); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateShotsFired(int shotsFired) { int o = __offset(10); if (o != 0) { bb.PutInt(o + bb_pos, shotsFired); return true; } else { return false; } }

  public static Offset<UnitAttackMsg> CreateUnitAttackMsg(FlatBufferBuilder builder,
      Offset<UnitMsg> attackingOffset = default(Offset<UnitMsg>),
      Offset<UnitMsg> attackedOffset = default(Offset<UnitMsg>),
      Offset<AttackInstanceMsg> attOffset = default(Offset<AttackInstanceMsg>),
      int shotsFired = 0) {
    builder.StartObject(4);
    UnitAttackMsg.AddShotsFired(builder, shotsFired);
    UnitAttackMsg.AddAtt(builder, attOffset);
    UnitAttackMsg.AddAttacked(builder, attackedOffset);
    UnitAttackMsg.AddAttacking(builder, attackingOffset);
    return UnitAttackMsg.EndUnitAttackMsg(builder);
  }

  public static void StartUnitAttackMsg(FlatBufferBuilder builder) { builder.StartObject(4); }
  public static void AddAttacking(FlatBufferBuilder builder, Offset<UnitMsg> attackingOffset) { builder.AddOffset(0, attackingOffset.Value, 0); }
  public static void AddAttacked(FlatBufferBuilder builder, Offset<UnitMsg> attackedOffset) { builder.AddOffset(1, attackedOffset.Value, 0); }
  public static void AddAtt(FlatBufferBuilder builder, Offset<AttackInstanceMsg> attOffset) { builder.AddOffset(2, attOffset.Value, 0); }
  public static void AddShotsFired(FlatBufferBuilder builder, int shotsFired) { builder.AddInt(3, shotsFired, 0); }
  public static Offset<UnitAttackMsg> EndUnitAttackMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<UnitAttackMsg>(o);
  }
};


}
