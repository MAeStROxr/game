// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class UnitMoveMsg : Table {
  public static UnitMoveMsg GetRootAsUnitMoveMsg(ByteBuffer _bb) { return GetRootAsUnitMoveMsg(_bb, new UnitMoveMsg()); }
  public static UnitMoveMsg GetRootAsUnitMoveMsg(ByteBuffer _bb, UnitMoveMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public UnitMoveMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public UnitMsg Unit { get { return GetUnit(new UnitMsg()); } }
  public UnitMsg GetUnit(UnitMsg obj) { int o = __offset(4); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }
  public TileMsg TargetedTile { get { return GetTargetedTile(new TileMsg()); } }
  public TileMsg GetTargetedTile(TileMsg obj) { int o = __offset(6); return o != 0 ? obj.__init(__indirect(o + bb_pos), bb) : null; }

  public static Offset<UnitMoveMsg> CreateUnitMoveMsg(FlatBufferBuilder builder,
      Offset<UnitMsg> unitOffset = default(Offset<UnitMsg>),
      Offset<TileMsg> targetedTileOffset = default(Offset<TileMsg>)) {
    builder.StartObject(2);
    UnitMoveMsg.AddTargetedTile(builder, targetedTileOffset);
    UnitMoveMsg.AddUnit(builder, unitOffset);
    return UnitMoveMsg.EndUnitMoveMsg(builder);
  }

  public static void StartUnitMoveMsg(FlatBufferBuilder builder) { builder.StartObject(2); }
  public static void AddUnit(FlatBufferBuilder builder, Offset<UnitMsg> unitOffset) { builder.AddOffset(0, unitOffset.Value, 0); }
  public static void AddTargetedTile(FlatBufferBuilder builder, Offset<TileMsg> targetedTileOffset) { builder.AddOffset(1, targetedTileOffset.Value, 0); }
  public static Offset<UnitMoveMsg> EndUnitMoveMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<UnitMoveMsg>(o);
  }
};


}
