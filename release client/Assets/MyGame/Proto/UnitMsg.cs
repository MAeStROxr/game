// automatically generated, do not modify

namespace mygame.proto
{

using System;
using FlatBuffers;

public sealed class UnitMsg : Table {
  public static UnitMsg GetRootAsUnitMsg(ByteBuffer _bb) { return GetRootAsUnitMsg(_bb, new UnitMsg()); }
  public static UnitMsg GetRootAsUnitMsg(ByteBuffer _bb, UnitMsg obj) { return (obj.__init(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public UnitMsg __init(int _i, ByteBuffer _bb) { bb_pos = _i; bb = _bb; return this; }

  public long OID { get { int o = __offset(4); return o != 0 ? bb.GetLong(o + bb_pos) : (long)0; } }
  public bool MutateOID(long OID) { int o = __offset(4); if (o != 0) { bb.PutLong(o + bb_pos, OID); return true; } else { return false; } }
  public string UnitName { get { int o = __offset(6); return o != 0 ? __string(o + bb_pos) : null; } }
  public ArraySegment<byte>? GetUnitNameBytes() { return __vector_as_arraysegment(6); }
  public int PrefabId { get { int o = __offset(8); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutatePrefabId(int prefabId) { int o = __offset(8); if (o != 0) { bb.PutInt(o + bb_pos, prefabId); return true; } else { return false; } }
  public int DataId { get { int o = __offset(10); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateDataId(int dataId) { int o = __offset(10); if (o != 0) { bb.PutInt(o + bb_pos, dataId); return true; } else { return false; } }
  public int TileId { get { int o = __offset(12); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateTileId(int tileId) { int o = __offset(12); if (o != 0) { bb.PutInt(o + bb_pos, tileId); return true; } else { return false; } }
  public int FactionID { get { int o = __offset(14); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateFactionID(int factionID) { int o = __offset(14); if (o != 0) { bb.PutInt(o + bb_pos, factionID); return true; } else { return false; } }
  public int Level { get { int o = __offset(16); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateLevel(int level) { int o = __offset(16); if (o != 0) { bb.PutInt(o + bb_pos, level); return true; } else { return false; } }
  public float HP { get { int o = __offset(18); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateHP(float HP) { int o = __offset(18); if (o != 0) { bb.PutFloat(o + bb_pos, HP); return true; } else { return false; } }
  public float AP { get { int o = __offset(20); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateAP(float AP) { int o = __offset(20); if (o != 0) { bb.PutFloat(o + bb_pos, AP); return true; } else { return false; } }
  public float TurnPriority { get { int o = __offset(22); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateTurnPriority(float turnPriority) { int o = __offset(22); if (o != 0) { bb.PutFloat(o + bb_pos, turnPriority); return true; } else { return false; } }
  public int MoveRange { get { int o = __offset(24); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateMoveRange(int moveRange) { int o = __offset(24); if (o != 0) { bb.PutInt(o + bb_pos, moveRange); return true; } else { return false; } }
  public int AttackRange { get { int o = __offset(26); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateAttackRange(int attackRange) { int o = __offset(26); if (o != 0) { bb.PutInt(o + bb_pos, attackRange); return true; } else { return false; } }
  public float HitChance { get { int o = __offset(28); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateHitChance(float hitChance) { int o = __offset(28); if (o != 0) { bb.PutFloat(o + bb_pos, hitChance); return true; } else { return false; } }
  public float DodgeChance { get { int o = __offset(30); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDodgeChance(float dodgeChance) { int o = __offset(30); if (o != 0) { bb.PutFloat(o + bb_pos, dodgeChance); return true; } else { return false; } }
  public float DamageMin { get { int o = __offset(32); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDamageMin(float damageMin) { int o = __offset(32); if (o != 0) { bb.PutFloat(o + bb_pos, damageMin); return true; } else { return false; } }
  public float DamageMax { get { int o = __offset(34); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDamageMax(float damageMax) { int o = __offset(34); if (o != 0) { bb.PutFloat(o + bb_pos, damageMax); return true; } else { return false; } }
  public float CritChance { get { int o = __offset(36); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateCritChance(float critChance) { int o = __offset(36); if (o != 0) { bb.PutFloat(o + bb_pos, critChance); return true; } else { return false; } }
  public float CritAvoidance { get { int o = __offset(38); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateCritAvoidance(float critAvoidance) { int o = __offset(38); if (o != 0) { bb.PutFloat(o + bb_pos, critAvoidance); return true; } else { return false; } }
  public float CritMultiplier { get { int o = __offset(40); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateCritMultiplier(float critMultiplier) { int o = __offset(40); if (o != 0) { bb.PutFloat(o + bb_pos, critMultiplier); return true; } else { return false; } }
  public float StunChance { get { int o = __offset(42); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateStunChance(float stunChance) { int o = __offset(42); if (o != 0) { bb.PutFloat(o + bb_pos, stunChance); return true; } else { return false; } }
  public float StunAvoidance { get { int o = __offset(44); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateStunAvoidance(float stunAvoidance) { int o = __offset(44); if (o != 0) { bb.PutFloat(o + bb_pos, stunAvoidance); return true; } else { return false; } }
  public int StunDuration { get { int o = __offset(46); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateStunDuration(int stunDuration) { int o = __offset(46); if (o != 0) { bb.PutInt(o + bb_pos, stunDuration); return true; } else { return false; } }
  public float SilentChance { get { int o = __offset(48); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateSilentChance(float silentChance) { int o = __offset(48); if (o != 0) { bb.PutFloat(o + bb_pos, silentChance); return true; } else { return false; } }
  public float SilentAvoidance { get { int o = __offset(50); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateSilentAvoidance(float silentAvoidance) { int o = __offset(50); if (o != 0) { bb.PutFloat(o + bb_pos, silentAvoidance); return true; } else { return false; } }
  public int SilentDuration { get { int o = __offset(52); return o != 0 ? bb.GetInt(o + bb_pos) : (int)0; } }
  public bool MutateSilentDuration(int silentDuration) { int o = __offset(52); if (o != 0) { bb.PutInt(o + bb_pos, silentDuration); return true; } else { return false; } }
  public float HPPerTurn { get { int o = __offset(54); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateHPPerTurn(float HPPerTurn) { int o = __offset(54); if (o != 0) { bb.PutFloat(o + bb_pos, HPPerTurn); return true; } else { return false; } }
  public float APPerTurn { get { int o = __offset(56); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateAPPerTurn(float APPerTurn) { int o = __offset(56); if (o != 0) { bb.PutFloat(o + bb_pos, APPerTurn); return true; } else { return false; } }
  public float DefaultHP { get { int o = __offset(58); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDefaultHP(float defaultHP) { int o = __offset(58); if (o != 0) { bb.PutFloat(o + bb_pos, defaultHP); return true; } else { return false; } }
  public float DefaultAP { get { int o = __offset(60); return o != 0 ? bb.GetFloat(o + bb_pos) : (float)0; } }
  public bool MutateDefaultAP(float defaultAP) { int o = __offset(60); if (o != 0) { bb.PutFloat(o + bb_pos, defaultAP); return true; } else { return false; } }

  public static Offset<UnitMsg> CreateUnitMsg(FlatBufferBuilder builder,
      long OID = 0,
      StringOffset unitNameOffset = default(StringOffset),
      int prefabId = 0,
      int dataId = 0,
      int tileId = 0,
      int factionID = 0,
      int level = 0,
      float HP = 0,
      float AP = 0,
      float turnPriority = 0,
      int moveRange = 0,
      int attackRange = 0,
      float hitChance = 0,
      float dodgeChance = 0,
      float damageMin = 0,
      float damageMax = 0,
      float critChance = 0,
      float critAvoidance = 0,
      float critMultiplier = 0,
      float stunChance = 0,
      float stunAvoidance = 0,
      int stunDuration = 0,
      float silentChance = 0,
      float silentAvoidance = 0,
      int silentDuration = 0,
      float HPPerTurn = 0,
      float APPerTurn = 0,
      float defaultHP = 0,
      float defaultAP = 0) {
    builder.StartObject(29);
    UnitMsg.AddOID(builder, OID);
    UnitMsg.AddDefaultAP(builder, defaultAP);
    UnitMsg.AddDefaultHP(builder, defaultHP);
    UnitMsg.AddAPPerTurn(builder, APPerTurn);
    UnitMsg.AddHPPerTurn(builder, HPPerTurn);
    UnitMsg.AddSilentDuration(builder, silentDuration);
    UnitMsg.AddSilentAvoidance(builder, silentAvoidance);
    UnitMsg.AddSilentChance(builder, silentChance);
    UnitMsg.AddStunDuration(builder, stunDuration);
    UnitMsg.AddStunAvoidance(builder, stunAvoidance);
    UnitMsg.AddStunChance(builder, stunChance);
    UnitMsg.AddCritMultiplier(builder, critMultiplier);
    UnitMsg.AddCritAvoidance(builder, critAvoidance);
    UnitMsg.AddCritChance(builder, critChance);
    UnitMsg.AddDamageMax(builder, damageMax);
    UnitMsg.AddDamageMin(builder, damageMin);
    UnitMsg.AddDodgeChance(builder, dodgeChance);
    UnitMsg.AddHitChance(builder, hitChance);
    UnitMsg.AddAttackRange(builder, attackRange);
    UnitMsg.AddMoveRange(builder, moveRange);
    UnitMsg.AddTurnPriority(builder, turnPriority);
    UnitMsg.AddAP(builder, AP);
    UnitMsg.AddHP(builder, HP);
    UnitMsg.AddLevel(builder, level);
    UnitMsg.AddFactionID(builder, factionID);
    UnitMsg.AddTileId(builder, tileId);
    UnitMsg.AddDataId(builder, dataId);
    UnitMsg.AddPrefabId(builder, prefabId);
    UnitMsg.AddUnitName(builder, unitNameOffset);
    return UnitMsg.EndUnitMsg(builder);
  }

  public static void StartUnitMsg(FlatBufferBuilder builder) { builder.StartObject(29); }
  public static void AddOID(FlatBufferBuilder builder, long OID) { builder.AddLong(0, OID, 0); }
  public static void AddUnitName(FlatBufferBuilder builder, StringOffset unitNameOffset) { builder.AddOffset(1, unitNameOffset.Value, 0); }
  public static void AddPrefabId(FlatBufferBuilder builder, int prefabId) { builder.AddInt(2, prefabId, 0); }
  public static void AddDataId(FlatBufferBuilder builder, int dataId) { builder.AddInt(3, dataId, 0); }
  public static void AddTileId(FlatBufferBuilder builder, int tileId) { builder.AddInt(4, tileId, 0); }
  public static void AddFactionID(FlatBufferBuilder builder, int factionID) { builder.AddInt(5, factionID, 0); }
  public static void AddLevel(FlatBufferBuilder builder, int level) { builder.AddInt(6, level, 0); }
  public static void AddHP(FlatBufferBuilder builder, float HP) { builder.AddFloat(7, HP, 0); }
  public static void AddAP(FlatBufferBuilder builder, float AP) { builder.AddFloat(8, AP, 0); }
  public static void AddTurnPriority(FlatBufferBuilder builder, float turnPriority) { builder.AddFloat(9, turnPriority, 0); }
  public static void AddMoveRange(FlatBufferBuilder builder, int moveRange) { builder.AddInt(10, moveRange, 0); }
  public static void AddAttackRange(FlatBufferBuilder builder, int attackRange) { builder.AddInt(11, attackRange, 0); }
  public static void AddHitChance(FlatBufferBuilder builder, float hitChance) { builder.AddFloat(12, hitChance, 0); }
  public static void AddDodgeChance(FlatBufferBuilder builder, float dodgeChance) { builder.AddFloat(13, dodgeChance, 0); }
  public static void AddDamageMin(FlatBufferBuilder builder, float damageMin) { builder.AddFloat(14, damageMin, 0); }
  public static void AddDamageMax(FlatBufferBuilder builder, float damageMax) { builder.AddFloat(15, damageMax, 0); }
  public static void AddCritChance(FlatBufferBuilder builder, float critChance) { builder.AddFloat(16, critChance, 0); }
  public static void AddCritAvoidance(FlatBufferBuilder builder, float critAvoidance) { builder.AddFloat(17, critAvoidance, 0); }
  public static void AddCritMultiplier(FlatBufferBuilder builder, float critMultiplier) { builder.AddFloat(18, critMultiplier, 0); }
  public static void AddStunChance(FlatBufferBuilder builder, float stunChance) { builder.AddFloat(19, stunChance, 0); }
  public static void AddStunAvoidance(FlatBufferBuilder builder, float stunAvoidance) { builder.AddFloat(20, stunAvoidance, 0); }
  public static void AddStunDuration(FlatBufferBuilder builder, int stunDuration) { builder.AddInt(21, stunDuration, 0); }
  public static void AddSilentChance(FlatBufferBuilder builder, float silentChance) { builder.AddFloat(22, silentChance, 0); }
  public static void AddSilentAvoidance(FlatBufferBuilder builder, float silentAvoidance) { builder.AddFloat(23, silentAvoidance, 0); }
  public static void AddSilentDuration(FlatBufferBuilder builder, int silentDuration) { builder.AddInt(24, silentDuration, 0); }
  public static void AddHPPerTurn(FlatBufferBuilder builder, float HPPerTurn) { builder.AddFloat(25, HPPerTurn, 0); }
  public static void AddAPPerTurn(FlatBufferBuilder builder, float APPerTurn) { builder.AddFloat(26, APPerTurn, 0); }
  public static void AddDefaultHP(FlatBufferBuilder builder, float defaultHP) { builder.AddFloat(27, defaultHP, 0); }
  public static void AddDefaultAP(FlatBufferBuilder builder, float defaultAP) { builder.AddFloat(28, defaultAP, 0); }
  public static Offset<UnitMsg> EndUnitMsg(FlatBufferBuilder builder) {
    int o = builder.EndObject();
    return new Offset<UnitMsg>(o);
  }
};


}
