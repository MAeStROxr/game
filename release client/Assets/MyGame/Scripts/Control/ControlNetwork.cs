﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using TBTK;
using NT;
using System.IO;
using UnityEngine.UI;

using FlatBuffers;

namespace NT {

    public class ControlNetwork : MonoBehaviour {

        private static ControlNetwork instance;
        public ControlPointManager pointManager;
        public PlayerManager playerManager;
      
        public GameObject nodePrefab;
        public long clientOID=0;
        public String clientName;
        Proto proto = new Proto();
        void Awake() {
            
        }

        // Use this for initialization
        void Start() {
            instance = this;

            //RegisterControlListener();
            //ControlNode root = ControlUI.createDebugTree();
            //TreeGraph(root);
            //controlTree.gameObject.SetActive(false);
        }
        public void OnLevelWasLoaded(int level) {
            if (level == 1) {
                clientOID = ClientAPI.GetPlayerOid();
                clientName = ClientAPI.GetPlayerObject().Name;
                //SendAddPlayer();
                //SendUpdateTree();
            }
        }

        // Update is called once per frame
        void Update() {

        }
        /*
        public  void RegisterControlListener() {
            NetworkAPI.RegisterExtensionMessageHandler
               (Control.CONTROL_POINT.ToString(), RecievedControlMsg);
            NetworkAPI.RegisterExtensionMessageHandler
               (Control.PLAYER_CONTROL.ToString(), RecievedControlMsg);
           


        }

       

        public  static void SendControlMsg(ControlMsg controlMsg) {
            
            Dictionary<string, object> props = new Dictionary<string, object>();
            props.Add("ControlMsg", controlMsg.ByteBuffer.Data);
            NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "control." + controlMsg.Cmd.ToString(), props);
       
            //instance.RecievedPointMsg(controlMsg);

         }

        public  void RecievedPointMsg(ControlMsg controlMsg) {
            ControlPointMsg msg = controlMsg.PointMsg ;
            switch (msg.Cmd) {
                case PointCmd.POINT_INFO:
                    pointManager.PointInfo(msg);
                    break;
                case PointCmd.POINT_CAPTURE:
                    pointManager.PointCaptured(msg);
                    break;
                case PointCmd.POINT_ENABLE:
                    pointManager.PointEnabled(msg);
                    break;
                case PointCmd.POINT_ARMY_GARRISON:
                    pointManager.PointInfo(msg);
                    break;
                case PointCmd.POINT_UNIT_GARRISON:
                    pointManager.PointInfo(msg);
                    break;
            }
        }

        public  void RecievedControlMsg(Dictionary<string, object> props) {
            byte[] data = (byte[])props["ControlMsg"];
            ByteBuffer buffer = new ByteBuffer(data);
            ControlMsg msg=ControlMsg.GetRootAsControlMsg(buffer);
            switch (msg.Cmd) {
                case Control.CONTROL_POINT:
                    //pointManager.ProcessPointMsg(msg.PointMsg);
                    break;
                case Control.PLAYER_CONTROL:
                   // playerManager.PlayerControlInfo(msg);
                    break;
              
            }
            
        }
        
        public  void SendAddPlayer() { instance._SendAddPlayer(); }
        public void _SendAddPlayer() {
            Dictionary<string, object> props = new Dictionary<string, object>();
            props.Add("player_oid", OID.fromLong(ClientAPI.GetPlayerOid()));
            props.Add("player_name", ClientAPI.GetPlayerObject().Name);
            NetworkAPI.SendExtensionMessage(ClientAPI.GetPlayerOid(), false, "controlpoint.PLAYER_ADD", props);


        }
        public static void SendPointInfo(ControlPoint point, PointCmd command = PointCmd.POINT_INFO, ArmyMsg updateToUnits = null, DataAction action = DataAction.GET) { instance._SendPointInfo(point, command, updateToUnits, action); }
        public void _SendPointInfo(ControlPoint point, PointCmd command, ArmyMsg army = null, DataAction action = DataAction.GET) {
            
            FlatBufferBuilder fbb = new FlatBufferBuilder(0);
            VectorOffset armiesOffset=new VectorOffset(0);
            Offset<ArmyMsg> armyOffset = new Offset<ArmyMsg>(0);
            switch (command) {
                case PointCmd.POINT_ARMY_GARRISON:
                    List<ArmyMsg> armies = new List<ArmyMsg>();
                    armies.Add(army);
                    armiesOffset = proto.addPointExistingArmies(fbb, armies);
                   
                    break;
                case PointCmd.POINT_UNIT_GARRISON:
                    armyOffset=proto.addExistingArmyMsg(fbb, point.GetGarrison());
                    break;
            }
           

            Offset<ControlPointMsg> pointMsgOffset = proto.addPointMsg(true, fbb, point.pointOID, command, clientOID, armyOffset, armiesOffset, action, point.pointName);
            fbb.Finish(pointMsgOffset.Value);

            ControlPointMsg msg = ControlPointMsg.GetRootAsControlPointMsg(fbb.DataBuffer);
            SendControlMsg(msg);

        }
       


        public  void SendPointCapture(long pointOID) { instance._SendPointCapture(pointOID); }
        public void _SendPointCapture(long pointOID) {
            FlatBufferBuilder fbb = new FlatBufferBuilder(0);
            Offset<ControlPointMsg> capMsg = proto.addPointMsg(true, fbb, pointOID, PointCmd.POINT_CAPTURE,clientOID );
            ControlMsg msg = proto.createControlPoint(fbb,capMsg);
            SendControlMsg(msg);
        }
        public static void SendUpdateTree() {
            instance._SendUpdateTree();
        }

        public   void _SendUpdateTree() {
            FlatBufferBuilder fbb = new FlatBufferBuilder(0);
            Offset<ControlMsg> msgOffset = ControlMsg.CreateControlMsg(fbb, Control.PLAYER_CONTROL, clientOID);
            fbb.Finish(msgOffset.Value);
            ControlMsg msg = ControlMsg.GetRootAsControlMsg(fbb.DataBuffer);
            SendControlMsg(msg);
        }
       */
    }
}