﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TBTK;
using System;
using NT;
using UnityEngine.UI;
using System.Linq;
using mygame.proto;
using FlatBuffers;

public class ControlPoint : MonoBehaviour
{

    public long privateOID = 10;
    public long clientOID { get { return GameMainframe.GetInstance().clientOID; } }
    public long pointOID { get {
            return privateOID;
        }
        set {
            privateOID = value;
        } }
    private string privateName = "point";

    public string pointName {  get  {       return privateName;    }     set    { privateName = value;  }  }
    public long startingOID;
    public ArmyMsg garrison;
    
    public Player controller = null;
    public Player enabler = null;
    public Vector3 Pos=default(Vector3);
    public ControlPointManager pointManager;
    public bool capturable = false;
    public bool stationed = false;
    public bool hostile = true;
    public bool friendly;
    bool showGUI = false;
    bool mouseOver = false;
    public bool inSiege = false;
    public bool networkLinked = false;
    //Inventory inv;
    Dictionary<long, ArmyMsg> stationedGenerals = new Dictionary<long, ArmyMsg>();
    public ControlPointUI pointUI;
    public Texture2D cursorIcon;
    public static float maxInteractionDistance = 30;
    private static System.Random rand = new System.Random();
    public void UpdateControlPoint(Player controller, Player enabler, bool capturable) {
        friendly = false;
        hostile = true;
        this.capturable = capturable;
        Player clientPlayer = pointManager.playerManager.GetPlayer(clientOID);
        if (controller != null && controller.GetName() != "Root") {
            this.controller = controller;
            //|| clientPlayer.HasCommonAncestor(controller)
            if (controller.getOID() == clientPlayer.getOID() )  {
                friendly = true;
                capturable = false;
                hostile = false;
            }
        }
        //|| clientPlayer.HasCommonAncestor(enabler)
        if (enabler != null && enabler.GetName() != "Root" ) {
            this.enabler = enabler;
            if (enabler.getOID() == clientOID) {
                friendly = true;
                capturable = false;
                hostile = false;
            }
        }
        
      
    }

    public void updateCapturable(bool capturable) {
        this.capturable = capturable;
    }
    void Awake() {
        showGUI = false;
        //pointOID = startingOID;
        ResetUnitsGarrison();
    }

    public void ResetUnitsGarrison() {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<ArmyMsg> armyOffset = Proto.addArmy(fbb, pointOID, "ControlPoint:" + pointName, new List<DataUnit>());
        fbb.Finish(armyOffset.Value);
        garrison = ArmyMsg.GetRootAsArmyMsg(fbb.DataBuffer);
    }

    public void ResetTotalGarrison() {
        ResetUnitsGarrison();
        stationedGenerals.Values.ToList().ForEach(general => GeneralRemoved(general.OID));
        stationedGenerals = new Dictionary<long, ArmyMsg>();
        pointUI.clearGarrisonUI();
    }

    // Use this for initialization
    void Start() {
        
        pointManager = ControlPointManager.GetInstance();
        gameObject.AddComponent<AtavismNode>();
        
        // StartCoroutine(linkNetwork(5));
        //UIGarrison = transform.Find("UIGarrison").gameObject;
        //pointUI.gameObject.SetActive(showGUI);

    }
    IEnumerator NetworkCoroutine(float waitTime = 1f) {
        yield return new WaitForSeconds(waitTime);
        linkNetwork();
    }
    internal void SetControlling(Player player) {
        controller = player;
    }

    internal void SetEnabling(Player player) {
        enabler = player;
    }

    public void linkNetwork() {
        /*
         ClaimObject claim = gameObject.GetComponent<ClaimObject>();
         if (claim != null)
             pointOID = claim.ClaimID;*/
        //Debug.Log("claimID:" + claim.ClaimID + " ID:" + claim.ID + " TemplateID:" + claim.TemplateID + " pointOID:" + pointOID);
        //pointOID = claim.ID;
        
        ControlPointManager.RegisterControlPoint(this);
        networkLinked = true;
    }

    public void OnLevelWasLoaded(int level) {
        if (level == 1) {
            int wait = rand.Next(0, 20);
            StartCoroutine(NetworkCoroutine(wait));
            
        }
    }
    public void ShowGUI() {
        showGUI = !showGUI;
        pointUI.SetPoint(this);


        if (showGUI) {
            pointUI.Show();
            pointUI.updateGarrisonUI(GetGarrisonTotal());
        }
        else {
            pointUI.Hide();
            pointUI.clearGarrisonUI();
        }
    }

    public float PlayerDistance() {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Vector3 pointLoc = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        // Casts the ray and get the first game object hit
        if ( Physics.Raycast(ray, out hit, Mathf.Infinity, pointManager.GetPointMask())) {
            ControlPoint cp = hit.transform.GetComponent<ControlPoint>();
            if (cp != null && cp == this)
               
                Pos = hit.point;
        }
        
        float dist = (Pos - GameMainframe.GetInstance().GetClientObject().transform.position).magnitude;
        return dist;
    }

    // Update is called once per frame
    void Update() {
        //friendly = IsFriendly(GameMainframe.clientOID);
        if (showGUI || mouseOver) {
            float dist = PlayerDistance();
            if (dist > maxInteractionDistance) {
                if (showGUI) ShowGUI();
                return;
            }
            else if (mouseOver && Input.GetMouseButtonDown(1)) ShowGUI();
        }

    }
    //Texture temp = null;
    float distance = 10;
    void OnMouseOver() {
        mouseOver = true;
        AtavismCursor.Instance.SetMouseOverObject(GetComponent<AtavismNode>(), pointManager.GetPointIcon(), maxInteractionDistance);

        if (networkLinked) {
            
           // temp = AtavismCursor.Instance.defaultCursor;
           // AtavismCursor.Instance.defaultCursor = pointManager.GetPointIcon();
            
        }

    }

    void OnMouseExit() {
        mouseOver = false;
        AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
       
        //if (networkLinked) AtavismCursor.Instance.ClearMouseOverObject(GetComponent<AtavismNode>());
    }

    public void GarrisonGeneral() {
        pointManager.GarrisonGeneral(this, DataAction.ADD);
    }
    public void GeneralGarrisoned(ArmyMsg garrisonedArmy) {
        if (inSiege) return;
        stationedGenerals[garrisonedArmy.OID] = garrisonedArmy;

        if (garrisonedArmy.OID == clientOID)
            stationed = true;
        if (showGUI) {
            pointUI.updateGarrisonUI(GetGarrisonTotal());
        }
        //GarrisonCharacter();
    }
    
    public void GarrisonCharacter(bool enter, long playerOID) {
        if (enter) {
            AtavismPlayer player = (AtavismPlayer)ClientAPI.GetObjectNode(playerOID);

            Vector3 objPos = player.GetControllingTransform().position;
            Vec3 position = new Vec3();
            /*position.X = objPos.x;
            position.Y = objPos.y;
            position.Z = objPos.z;*/
            // garrisonedArmy.Pos = ;
            player.GetControllingTransform().position = gameObject.transform.position;
            //player.MobController.enabled = false;
            player.InputController.enabled = false;

        }
        else {
            ArmyMsg army = GetGarrisonedArmies().Find(a => a.OID == playerOID);
            AtavismPlayer player = (AtavismPlayer)ClientAPI.GetObjectNode(playerOID);

            player.GetControllingTransform().position = new Vector3(army.Position.X,army.Position.Y,army.Position.Z);
            player.MobController.enabled = true;
            //if (playerOID == clientOID)
            //    ClientAPI.GetPlayerObject().MobController.enabled = false;
        }
    }

    public void GetPaid() {

    }

    Vector3 getMsgVec(Vec3 vec) {
        return new Vector3();
    }
    public void RemoveGeneral() {
        pointManager.GarrisonGeneral(this, DataAction.REMOVE);
    }

    public void GeneralRemoved(long playerOID) {
        if (inSiege) return;
        ArmyMsg removed = stationedGenerals[playerOID];
        if (playerOID == clientOID)
            stationed = false;
        stationedGenerals.Remove(playerOID);
        if (showGUI) {
            pointUI.updateGarrisonUI(GetGarrisonTotal());
        }

        //GarrisonCharacter();

    }

    public void OnTransferComplete(List<UnitMsg> newInv, List<UnitMsg> newGarrison) {
        pointManager.UpdateUnitsGarrison(this, newInv, newGarrison);

    }
    public bool NeedSiege() {
        if (garrison == null) return false;
        return garrison.ArmyUnitsLength > 0 || stationedGenerals.Count != 0;
    }



    public void SetGarrison(ArmyMsg newGarrison) {
        garrison = newGarrison;

        if (showGUI) {
            pointUI.updateGarrisonUI(GetGarrisonTotal());
        }
    }
    public ArmyMsg GetGarrison() {
        return garrison;
    }
    public Player GetControllingPlayer() {
        return controller;
    }
    public Player GetEnablingPlayer() {
        return enabler;
    }




    public List<ArmyMsg> GetGarrisonTotal(bool garrisonUnitsToo = true) {
        List<ArmyMsg> armies = new List<ArmyMsg>();
        if (garrison != null && garrisonUnitsToo && garrison.ArmyUnitsLength > 0) armies.Add(garrison);
        foreach (KeyValuePair<long, ArmyMsg> entry in stationedGenerals) {
            ArmyMsg army = entry.Value;
            army.MutateDataID((short)armies.Count);
            army.MutateFactionID((short)armies.Count);
            armies.Add(army);
        }
        return armies;
    }

    public List<ArmyMsg> GetGarrisonedArmies() {
        List<ArmyMsg> armies = new List<ArmyMsg>();
        foreach (KeyValuePair<long, ArmyMsg> entry in stationedGenerals) {
            ArmyMsg army = entry.Value;
            army.MutateDataID((short)armies.Count);
            army.MutateFactionID((short)armies.Count);
            armies.Add(army);
        }
        return armies;
    }
    public void Capture() {
        if (!NeedSiege())
            pointManager.CapturePoint(this);
        else pointManager.SiegePoint(this);

    }

    public bool  IsFriendly(long playerOID) {
        return pointManager.IsFriendly(this , playerOID);
    }
    
}
