﻿using UnityEngine;
using System.Collections;
using mygame.proto;
using System.Collections.Generic;
using NT;
using System;
using FlatBuffers;
using TBTK;
#pragma warning disable 0114
public class ControlPointManager : MonoBehaviour
{
    static private Dictionary<long, ControlPoint> controlPoints = new Dictionary<long, ControlPoint>();
    private long clientOID { get { return GameMainframe.GetInstance().clientOID; } }
    private string clientName;
    static ControlPointManager instance;
    public PlayerManager playerManager;
    public Texture2D controlPointIcon;
    public LayerMask contorlPointMask;
    Proto proto = new Proto();
    bool isConnected = false;
    void Awake() {
        instance = this;
        foreach (KeyValuePair<long, ControlPoint> pair in controlPoints) {
            pair.Value.pointManager = this;
        }
        clientName = "player";
        
    }

    public void Start() {
        NetworkAPI.RegisterExtensionMessageHandler
              (Control.CONTROL_POINT.ToString(), RecievedPointMsg);
        
    }

    public void OnLevelWasLoaded(int level) {
        if (level == 1) {

            clientName = GameMainframe.GetInstance().clientName;
            isConnected = true;
        }
    }

    public Texture2D GetPointIcon() {
        return controlPointIcon;
    }
    public List<ArmyMsg> pointGarrisonedArmies(ControlPointMsg pointMsg) {
        List<ArmyMsg> armies = new List<ArmyMsg>();
        for (int i = 0; i < pointMsg.GarrisonedArmiesLength; i++)
            armies.Add(pointMsg.GetGarrisonedArmies(i));
        return armies;
    }
    public static ControlPointManager GetInstance() { return instance; }
    public void RecievedPointMsg(Dictionary<string, object> props) {
        byte[] data = (byte[])props["ControlPointMsg"];
        ByteBuffer buffer = new ByteBuffer(data);
        buffer.Position = (int)props["BufferPosition"];
        ControlPointMsg pointMsg = ControlPointMsg.GetRootAsControlPointMsg(buffer);
        //Debug.Log("Recieved ControlPointMsg: pointOID-" + pointMsg.OID + " subjectOID-" + pointMsg.SubjectOID + " pointCmd-" + pointMsg.Cmd.ToString());
        ProcessPointMsg(pointMsg);
    }

    public void ProcessPointMsg(ControlPointMsg pointMsg) {
        ControlPoint cp = controlPoints[pointMsg.OID];
        switch (pointMsg.Cmd) {
            case PointCmd.POINT_INFO:
            case PointCmd.POINT_ARMY_GARRISON:
            case PointCmd.POINT_UNIT_GARRISON:
                UpdatePoint(pointMsg,cp);
                break;
            case PointCmd.POINT_CAPTURE:
                PointCaptured(pointMsg, cp);
                break;
            case PointCmd.POINT_ENABLE:
                PointEnabled(pointMsg, cp);
                break;
            case PointCmd.POINT_SIEGE_RESULT:
                SiegeResult(pointMsg,cp);
                break;
        }
    }

    private void SiegeResult(ControlPointMsg pointMsg, ControlPoint cp) {
       
        List<ArmyMsg> newArmiesGarrison = proto.getPointArmiesGarrison(pointMsg);
        pointMsg.MutateCmd(PointCmd.POINT_INFO);
        UpdatePoint(pointMsg,cp);
        

    }

    public void UpdatePoint(ControlPointMsg pointMsg, ControlPoint cp) {
        
        switch (pointMsg.Cmd) {

            case PointCmd.POINT_INFO:
                addMsgPlayers(pointMsg);
                cp.ResetTotalGarrison();
                List<ArmyMsg> armies = pointGarrisonedArmies(pointMsg);
                armies.ForEach(a => cp.GeneralGarrisoned(a));
                cp.SetGarrison(pointMsg.Garrison);

                cp.UpdateControlPoint(playerManager.GetPlayer(pointMsg.ControllingOID),
                    playerManager.GetPlayer(pointMsg.EnablingOID), pointMsg.Capturable);
                
                break;

            case PointCmd.POINT_ARMY_GARRISON:
                if (pointMsg.Action == DataAction.REMOVE) {
                    List<ArmyMsg> garrisoned = cp.GetGarrisonedArmies(),msgArmies = pointGarrisonedArmies(pointMsg);
                    garrisoned.ForEach(garrisonedArmy => 
                    {
                        if (!msgArmies.Exists(msgArmy => msgArmy.OID == garrisonedArmy.OID))
                            cp.GeneralRemoved(garrisonedArmy.OID);
                    });

                }
                if (pointMsg.Action == DataAction.ADD) {
                    pointGarrisonedArmies(pointMsg).ForEach(army => cp.GeneralGarrisoned(army));
                }
                break;
            case PointCmd.POINT_UNIT_GARRISON:
                cp.SetGarrison(pointMsg.Garrison);
                break;
                

        }
        //cp.pointUI.updateGarrisonUI(cp.GetGarrisonTotal());
    }

    public LayerMask GetPointMask() {
        return contorlPointMask;
    }

    internal static void GetPointInfo(ControlPoint controlPoint) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        StringOffset pointName = fbb.CreateString(controlPoint.name);
        StringOffset subjectName = fbb.CreateString(GameMainframe.GetInstance().clientName);
        Proto.addControlPoint(fbb, PointCmd.POINT_INFO, controlPoint.pointOID, GameMainframe.GetInstance().clientOID);
        ControlPointMsg.AddName(fbb, pointName);
        ControlPointMsg.AddSubjectName(fbb, pointName);
        ControlPointMsg.AddAction(fbb, DataAction.GET);
        SendControlPointMsg(fbb);
    }
    public void UnitsGarrisonUpdated(ControlPointMsg pointMsg, ControlPoint cp) {
        cp.SetGarrison(pointMsg.Garrison);
    }
    public void UpdateUnitsGarrison(ControlPoint point, List<UnitMsg> newInv, List<UnitMsg> newGarrison) {
        List<UnitMsg> oldInv = BattleManager.getUnitsMsgsFromInv();

        List<UnitMsg> list = new List<UnitMsg>();
        foreach (UnitMsg u in newInv) {
            if (!oldInv.Exists(unit => unit.OID == u.OID))
                list.Add(u);
        }
        BattleManager.addUnitsToInventory(list);
        list.Clear();
        foreach (UnitMsg un in oldInv) {
            if (!newInv.Exists(unit => unit.OID == un.OID))
                list.Add(un);
        }
        BattleManager.DeleteUnitsFromInv(list);
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<ArmyMsg> myArmy = Proto.addArmy(fbb, clientOID,
                    clientName, newGarrison);
        Proto.addControlPoint(fbb, PointCmd.POINT_UNIT_GARRISON, point.pointOID, clientOID);
        ControlPointMsg.AddGarrison(fbb, myArmy);
        ControlPointMsg.AddAction(fbb, DataAction.REPLACE);
        SendControlPointMsg(fbb);

    }

    public void GarrisonGeneral(ControlPoint point, DataAction action) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);

        Proto.addArmy(fbb, clientOID,
                    clientName, BattleManager.getUnitsMsgsFromInv(), false);
        Vector3 p=GameMainframe.GetInstance().GetClientObject().transform.position;
        ArmyMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, p.x, p.y, p.z));
        ArmyMsg.AddIsMainArmy(fbb,true);
        Offset<ArmyMsg> myArmy = ArmyMsg.EndArmyMsg(fbb);
        VectorOffset vec = ControlPointMsg.CreateGarrisonedArmiesVector(fbb, new Offset<ArmyMsg>[] { myArmy });
        Proto.addControlPoint(fbb, PointCmd.POINT_ARMY_GARRISON, point.pointOID, clientOID);
        ControlPointMsg.AddGarrisonedArmies(fbb, vec);

        ControlPointMsg.AddAction(fbb, action);
        SendControlPointMsg(fbb);
    }

    static void SendControlPointMsg(FlatBufferBuilder fbb) {
        Offset<ControlPointMsg> cpMsgOffset = ControlPointMsg.EndControlPointMsg(fbb);
        fbb.Finish(cpMsgOffset.Value);
        ControlPointMsg pointMsg = ControlPointMsg.GetRootAsControlPointMsg(fbb.DataBuffer);
        if (GameMainframe.GetInstance().debug) {
            instance.ProcessPointMsg(pointMsg);
            return;
        }
        /*Debug.Log("Sending control point msg:" + pointMsg.Cmd.ToString() + " pointOID:" + pointMsg.OID + " subject:" + pointMsg.SubjectOID + " " + pointMsg.SubjectName);
        for (int i = 0; i < pointMsg.ArmiesLength; i++) {
            ArmyMsg a = pointMsg.GetArmies(i);
            Debug.Log("army oid:" + a.OID + " units size:" + a.ArmyUnitsLength);
        }*/
        Proto.SendExtensionMessage("ControlPointMsg", pointMsg.ByteBuffer.Data,
                 pointMsg.ByteBuffer.Position, "control." + Control.CONTROL_POINT.ToString());
    }

    public void CapturePoint(ControlPoint point) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Proto.addControlPoint(fbb, PointCmd.POINT_CAPTURE, point.pointOID, clientOID);
        ControlPointMsg.AddCaptured(fbb, true);
        ControlPointMsg.AddCapturable(fbb, false);
        SendControlPointMsg(fbb);
    }

    public void SiegePoint(ControlPoint point) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(100);
        BattleManager battleMngr = BattleManager.GetInstance();
        Dictionary<long, AtavismMobNode> targets = battleMngr.GetNearPlayers();
        List<DataUnit> invUnits = battleMngr.getUnitsDataFromInv();
        List<Offset<ArmyMsg>> armies = new List<Offset<ArmyMsg>>();
        int i = 0;
        armies.Add(proto.addArmyInfo(fbb, GameMainframe.GetInstance().clientOID, invUnits, null, -1, GameMainframe.GetInstance().GetClientObject().transform.position));
        foreach (long targetOid in targets.Keys) {
            if (targetOid == GameMainframe.GetInstance().clientOID) continue;
            AtavismMobNode mobNode = targets[targetOid];
            int targetType = (int)mobNode.GetProperty("targetType");
            if (targetType < 1) continue;
            armies.Add(proto.addArmyInfo(fbb, targetOid, null, null, i, mobNode.Position));
        }
        VectorOffset armiesOffset = ControlPointMsg.CreateArmiesVector(fbb, armies.ToArray());
        Proto.addControlPoint(fbb, PointCmd.POINT_SIEGE, point.pointOID, clientOID);
        Vector3 v = point.transform.position;
        ControlPointMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, v.x, v.y, v.z));
        ControlPointMsg.AddCaptured(fbb, true);
        ControlPointMsg.AddCapturable(fbb, false);
        ControlPointMsg.AddArmies(fbb, armiesOffset);


        SendControlPointMsg(fbb);
    }

    private void addMsgPlayers(ControlPointMsg msg) {
        if (msg.SubjectOID > 0) PlayerManager.AddPlayer(msg.SubjectOID, msg.SubjectName);
        if (msg.ControllingOID > 0) PlayerManager.AddPlayer(msg.ControllingOID, msg.ControllingName);
        if (msg.EnablingOID > 0) PlayerManager.AddPlayer(msg.EnablingOID, msg.EnablingName);
    }


    public void PointCaptured(ControlPointMsg msg, ControlPoint cp) {
        Debug.Log("Captured Point Received: captured-" + msg.Captured + " capturable-" + msg.Capturable);
        bool pointCaptured = msg.Captured;
        if (!pointCaptured) {
            Debug.Log("Unable to capture point " + msg.OID);
            return;
        }
        addMsgPlayers(msg);
        Player newController  = playerManager.GetPlayer(msg.ControllingOID), oldController = cp.controller == null? null : playerManager.GetPlayer(cp.controller.getOID()),
            newEnabler = playerManager.GetPlayer(msg.EnablingOID) , oldEnabler = cp.enabler == null ? null : playerManager.GetPlayer(cp.enabler.getOID());

        if (newController.PlayerNode == null) playerManager.AddPlayerToTree(newController);
        if (oldController != null) oldController.DiscapturePoint(cp); 
        newController.CapturePoint(cp);

        if (oldEnabler != null) oldEnabler.DisablePoint(cp);
        if (newEnabler != null) newEnabler.EnablePoint(cp);
        
        cp.UpdateControlPoint(newController, newEnabler, msg.Capturable);
        cp.ResetTotalGarrison();
        Debug.Log("point captured: " + msg.OID);
    }


    public static void RegisterControlPoint(ControlPoint point) {
        controlPoints.Remove(point.pointOID);
        controlPoints.Add(point.pointOID, point);
        if (instance != null)
            point.pointManager = instance;
        GetPointInfo(point);
    }


    internal void PointEnabled(ControlPointMsg msg, ControlPoint cp) {
        throw new NotImplementedException();
    }

    internal void SendPointInfo(ControlPoint controlPoint, Control cONTROL_POINT, ArmyMsg army, DataAction rEPLACE) {
        throw new NotImplementedException();
    }

    internal bool IsFriendly(ControlPoint point, long playerOID) {

        return playerManager.AreFriendly(point.GetControllingPlayer().getOID(), playerOID);
    }
}
