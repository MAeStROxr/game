﻿using UnityEngine;
using System.Collections;
using TBTK;
using System.Collections.Generic;
using NT;
using UnityEngine.UI;
using mygame.proto;

public class ControlUI : MonoBehaviour {
    public WMG_Hierarchical_Tree controlTree;
    public ScrollRect controlWindow;
    // Use this for initialization
    void Start () {
        showUI = false;
        //controlWindow.gameObject.SetActive(showUI);
    }
    bool showUI;
    public void ShowControlUI() {
        showUI = !showUI;
        controlWindow.gameObject.SetActive(showUI);
        if (showUI) {
            //ControlNode root = ControlUI.createDebugTree();
           // ControlPointNetwork.TreeGraph(root);
           // controlTree.Refresh();
        }

        

    }

    public static ControlNode createDebugTree() {
        ControlNode root = createNode("root", null);
        List<ControlNode> controlled = new List<ControlNode>();
        ControlNode node11 = createNode("node 11",root),node12=createNode("node 12", root);
        ControlNode[] arr = new ControlNode[] { node11, node12 };
        /*
        root.Controlled.AddRange(arr);
        ControlNode node21 = createNode("node 21", node11), node22 = createNode("node 22", node11), node23 = createNode("node 23", node11);
        arr = new ControlNode[] { node21, node22,node23 };
        node11.Controlled.AddRange(arr);
        ControlNode node24 = createNode("node 24", node12),  node25 = createNode("node 25", node12),
        node31 = createNode("node 31", node24);
        node12.Controlled.Add(node24);
        node12.Controlled.Add(node25);
        node24.Controlled.Add(node31);
        ControlNode node13 = createNode("node 13", root);
        root.Controlled.Add(node13);
        */
        return root;
    }
    public static ControlNode createNode(string name, ControlNode parent, long oid=-1,int tax=10,List<ControlNode> controlled=null,List<ControlPointMsg> points=null) {
        ControlNode node = new ControlNode();
        /*
        node.ControlName = name;
        node.ControlOID = oid;
        node.TaxRate = tax;
        node.Parent = parent;
        node.Controlled = controlled;
        if (node.Controlled == null)
            node.Controlled = new List<ControlNode>();
        node.ControlPoints = points;*/
        return node;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
