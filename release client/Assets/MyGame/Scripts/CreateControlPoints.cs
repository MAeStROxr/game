﻿using UnityEngine;
using System.Collections;
using TBTK;

[ExecuteInEditMode]
public class CreateControlPoints : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public GameObject controlPointUI;
#if UNITY_EDITOR
    public void CreatePoints() {
        long pointOids = 100;
        foreach (Transform childT in transform) {
            GameObject child = childT.gameObject;

            DestroyImmediate(child.transform.GetChild(2).gameObject);
            //CreatePoint(child, pointOids++);
        }
    }

    void CreatePoint(GameObject child, long pointOid) {
        GameObject collider = child.GetComponentInChildren<MeshCollider>().gameObject;
        collider.AddComponent<ControlPoint>();
        ControlPoint point = collider.GetComponent<ControlPoint>();
        point.pointOID = pointOid;
        point.name = child.name;
        GameObject obj = Instantiate(controlPointUI), canvasObj = obj.transform.GetChild(0).gameObject;
        canvasObj.SetActive(true);
        ControlPointUI newPointUI = obj.GetComponentInChildren<ControlPointUI>();
        point.pointUI = newPointUI;
        newPointUI.point = point;
        obj.transform.parent = collider.transform;
        canvasObj.SetActive(false);
    }

#endif
}
