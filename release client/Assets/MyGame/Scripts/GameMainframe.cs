﻿using UnityEngine;
using System.Collections;
using System;
using NT;

public class GameMainframe : MonoBehaviour {
    public  long clientOID = 0;
    public  string clientName = "Player";
    public string GetClientName() { return clientName; }
    public  long GetClientOID() { return clientOID; }
    public Proto proto = new Proto();
    public Proto GetProto() { return proto;  }
    public  bool debug { get { return Instance.internal_debug_flag; } }
    internal  GameObject GetClientObject() { return Instance.character;  }
    public GameObject character;
    public int MaxTacticalPlayers;
    static GameMainframe Instance;
    public static GameMainframe GetInstance() { return Instance; }
    public bool internal_debug_flag=false;
    public bool _inBattle = false;
    public bool inBattle { get { return _inBattle; } set { _inBattle = value; } }
    public long currentBattleOID = 0;
	// Use this for initialization
    void Awake() {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnLevelWasLoaded(int level) {
        if (level == 1) {
            linkAtavism();
        }
    }
    private void linkAtavism() {
        //AtavismPlayer player = (AtavismPlayer)ClientAPI.GetObjectNode(clientOID);
        //character = playereObject;.Gam
        clientOID = ClientAPI.GetPlayerOid();
        clientName = ClientAPI.GetPlayerObject().Name;
        character = ClientAPI.GetPlayerObject().GameObject;
    }

    
}
