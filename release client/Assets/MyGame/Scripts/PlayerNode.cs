﻿
using mygame.proto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace NT
{
    /*
    public class PlayerNode {
        Player player;
        PlayerNode root, parent;
        List<PlayerNode> children = new List<PlayerNode>();

        //public PlayerNode(Player player,PlayerNode root, PlayerNode parent, List<PlayerNode> children )

        internal bool isNodeAncestor(PlayerNode playerNode) {
            throw new NotImplementedException();
        }

        internal void setParent(PlayerNode playerNode) {
            throw new NotImplementedException();
        }

        internal Player getPlayer() {
            throw new NotImplementedException();
        }

        internal int getChildCount() {
            throw new NotImplementedException();
        }

        internal PlayerNode getChildAt(int i) {
            throw new NotImplementedException();
        }
    }
}*/


    public class PlayerNode<T>
    {
        private readonly T _value;
        private readonly List<PlayerNode<T>> _children = new List<PlayerNode<T>>();
        
        public PlayerNode(T value) {
            _value = value;
        }

        public  PlayerNode<T> this[int i] {
            get { return _children[i]; }
        }

        public virtual PlayerNode<T> Parent { get; private set; }
        public PlayerNode<T> Root { get; private set; }
        public bool IsRoot() { return this == Root; }
        public int TreeId { get;  set; }
        public T Value { get { return _value; } }

        public ReadOnlyCollection<PlayerNode<T>> Children {
            get { return _children.AsReadOnly(); }
        }

        public List<PlayerNode<T>> MutableChildren { get { return _children; } }
        public PlayerNode<T> MutateAddChild(PlayerNode<T> node) {
            if (node.Parent != null) node.Parent.RemoveChild(node);
            node.Parent = this;

            _children.Add(node);
            return node;
        }

        public PlayerNode<T> AddChild(T value) {
            var node = new PlayerNode<T>(value) { Parent = this };
            _children.Add(node);
            return node;
        }

        public PlayerNode<T>[] AddChildren(params T[] values) {
            return values.Select<T, PlayerNode<T>>(AddChild).ToArray();
        }

        public bool RemoveChild(PlayerNode<T> node) {
            return _children.Remove(node);
        }

        public void Traverse(Action<T> action) {
            action(Value);
            foreach (var child in _children)
                child.Traverse(action);
        }

        public IEnumerable<T> Flatten() {
            return new[] { Value }.Union(_children.SelectMany(x => x.Flatten()));
        }

        public bool IsAncestor(PlayerNode<T> node) {  
            PlayerNode<T> current = Parent;
            while (current!=null) {
                if (current == node) return true;
            }
            return false;
        }

        public PlayerNode<T> ExecuteTreeBFS(Action<PlayerNode<T>> f) {
            Queue<PlayerNode<T>> queue = new Queue<PlayerNode<T>>();
            queue.Enqueue(this);
            while (queue.Count > 0) {
                PlayerNode<T> tempNode = queue.Dequeue();
                Console.WriteLine("Node number: " + tempNode.TreeId);
                f(tempNode);
                foreach (var item in tempNode.Children) {
                    queue.Enqueue(item);
                }
            }
            return null;
        }

        public PlayerNode<T> SearchTreeBFS(Func<PlayerNode<T>,bool> f) {
            Queue<PlayerNode<T>> queue = new Queue<PlayerNode<T>>();
            queue.Enqueue(this);
            while (queue.Count > 0) {
                PlayerNode<T> tempNode = queue.Dequeue();
                Console.WriteLine("Node number: " + tempNode.TreeId);
                if (f(tempNode)) return tempNode;
                foreach (var item in tempNode.Children) {
                    queue.Enqueue(item);
                }
            }
            return null;
        }

        public string PrintSubTree() {
            StringBuilder sb = new StringBuilder();
            ExecuteTreeBFS(n => sb.Append(n.ToString() + "\n"));
            return sb.ToString();
        }
    }

    public class TreeControlNode : PlayerNode<ControlNode>
    {
        public override string ToString() {
            string s = "[" + Value.OID + "-" + Value.ControlName;
            s += "<";
            foreach (TreeControlNode child in Children)
                s += "[" + child.Value.OID + "-" + child.Value.ControlName + "]";
            s += ">]";
            return  s;
        }
       

      
        public TreeControlNode(ControlNode value) :base(value) {
            for (int i = 0; i < Value.ControlledLength; i++)
                MutateAddChild(new TreeControlNode(Value.GetControlled(i)));
              
        }
        /*
        private  List<PlayerNode<ControlNode>> _children { get {
                
                List < PlayerNode < ControlNode >>l= new List<PlayerNode<ControlNode>>();
                for (int i=0;i<Value.ControlledLength; i++)
                    l.Add()
                return l;
            } }
            */

    }
}