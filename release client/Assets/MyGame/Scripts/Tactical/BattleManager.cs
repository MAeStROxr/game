﻿using UnityEngine;
using System.Collections.Generic;
using TBTK;
using System.IO;
using System;
using System.Linq;
using mygame.proto;
using NT;
using FlatBuffers;
using System.Collections;

public class BattleException : Exception { }
public class BattleManager : MonoBehaviour
{
    public ITacticalController battleController
    {
        get
        {
            return controllerContainer.Result;
        }
        set
        {
            controllerContainer.Result = value;
        }
    }
    [SerializeField]
    private ITacticalControllerContainer controllerContainer;
    public TacticalNetworkController networkController;

    //Dictionary<long, Battle> battles = new Dictionary<long, Battle>();

    [Serializable]
    public class BattleDic : SerializableDic<long, Battle> { }
    public BattleDic battles = new BattleDic();
    Proto proto = new Proto();
    private static BattleManager instance;
    public Battle battlePrefab, emptyBattlePrefab;
    public List<Unit> allUnitList = new List<Unit>();
    public GameObject battleCollider;
    private long clientOID = 0;

    public Dictionary<int, Unit> unitsDic = new Dictionary<int, Unit>();
    Inventory inv;
    // Use this for initialization
    void Awake() {
        instance = this;
        List<Unit> clonedUnitList = new List<Unit>();
        allUnitList.ForEach(u => clonedUnitList.Add(Instantiate(u)));
        allUnitList = clonedUnitList;
        allUnitList.ForEach(u => {
            u.gameObject.SetActive(false);
            unitsDic[u.prefabId] = u;
            DontDestroyOnLoad(u);
            u.transform.parent = transform;
        });

      
            

        // foreach (Unit u in allUnitList) { Debug.Log(u.name + " " + u.prefabId); }

    }
    void OnLevelWasLoaded(int level) {
        if (level == 1) {
            if (!GameMainframe.GetInstance().debug) inv = ClientAPI.ScriptObject.GetComponent<Inventory>();

        }
    }

    void Start() {
        DontDestroyOnLoad(this);
        NetworkAPI.RegisterExtensionMessageHandler
               (Tactical.BATTLE_COMMAND.ToString(), RecievedBattleMsg);
        //if (debug) DebugStartBattle();
    }
    public static BattleManager GetInstance() { return instance; }

    public int debugPlayers = 2;

    public static void RegisterBattle(Battle battleData) {
        if (instance.battles.ContainsKey(battleData.battleOID)) return;
        instance.battles[battleData.battleOID] = battleData;
        battleData.controller = instance.battleController;
    }

    public delegate void SendBattleMessageDel(BattleMsg battleMsg);
    public event SendBattleMessageDel sendMsgEvent;

    public void SendBattleMessage(BattleMsg battleMsg) {
        Debug.Log("Sending Battle Message with command:" + battleMsg.Cmd.ToString()+", battle:"+battleMsg.OID);
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg a = battleMsg.GetArmies(i);
            Debug.Log("Army-" + i + " in battleMsg, armyOid-" + a.OID + " networkPlayer-" + a.IsNetworkPlayer + " factionId-"+a.FactionID+" units size:" + a.ArmyUnitsLength);
        }
        if (GameMainframe.GetInstance().debug) {
            sendMsgEvent(battleMsg);
            return;
        }
        Proto.SendExtensionMessage("BattleMsg", battleMsg.ByteBuffer.Data,
                   battleMsg.ByteBuffer.Position, "tactical." + Tactical.BATTLE_COMMAND.ToString());
       
    }
    public void RecievedBattleMsg(Dictionary<string, object> props) {
        byte[] data = (byte[])props["BattleMsg"];
        ByteBuffer buffer = new ByteBuffer(data);
        buffer.Position = (int)props["BufferPosition"];
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(buffer);
        
        ProcessBattleMsg(msg);
    }


    public void ProcessBattleMsg(BattleMsg msg) {
        Debug.Log("Recieved BattleMsg: battleOid-" + msg.OID +" Cmd-" + msg.Cmd.ToString());
        for (int i = 0; i < msg.ArmiesLength; i++) {
            ArmyMsg a = msg.GetArmies(i);
            Debug.Log("Army-" + i + " in battleMsg, armyOid-" + a.OID +" isHostile-"+a.IsHostile+ " networkPlayer-" + a.IsNetworkPlayer + " playerFaction-"+a.IsPlayerFaction+" factionId-" + a.FactionID + " units size:" + a.ArmyUnitsLength+" tile size:"+a.DeployableTilesLength);
        }
        switch (msg.Cmd) {
            case BattleCmd.BATTLE_INFO:
                BattleInfo(msg);
                break;
            case BattleCmd.PLAYER_STARTED_BATTLE:
                BattleStarted(msg);
                break;

            case BattleCmd.PLAYER_JOINED_BATTLE:
                BattleJoined(msg);
                break;
            case BattleCmd.PLAYER_DEPLOYED_ARMY:
                BattleDeployed(msg);
                break;
            case BattleCmd.BATTLE_OVER:
                BattleOver(msg);
                break;
            case BattleCmd.PLAYER_LEFT_BATTLE:
                BattleLeft(msg);
                break;
            case BattleCmd.PLAYER_ENDED_TURN:
                BattleTurnOver(msg);
                break;
            case BattleCmd.BATTLE_COMMENCED:
                BattleCommenced(msg);
                break;
            case BattleCmd.PLAYER_RETREAT_BATTLE:
                BattleRetreat(msg);
                break;
            case BattleCmd.UNIT_ABILITY:
            case BattleCmd.UNIT_ATTACK:
            case BattleCmd.UNIT_MOVE:
                BattleAction(msg);
                break;
            case BattleCmd.EXECUTE_FAILURE:
                Debug.Log("Battle command failed, cmd:" + msg.Cmd.ToString() + " error:" + msg.Text);
                break;
        }

    }

    public long GetClientOID() {
        return clientOID;
    }
    internal void BattleJoined(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.ArmiesJoined(battle, msg);
        // battle.battleController.ArmiesJoined(battle, msg);
    }

    internal void BattleDeployed(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.ArmiesDeployed(battle, msg);
    }

    internal void BattleLeft(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        //  battle.battleController.Ba(battle, msg);
    }

    internal void BattleTurnOver(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.TurnEnded(battle);
    }

    internal void BattleCommenced(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        networkController.BattleCommenced(battle, msg);
    }

    internal void BattleOver(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        GameMainframe mainframe = GameMainframe.GetInstance();
        if (msg.OID == mainframe.currentBattleOID) {
            mainframe.currentBattleOID = 0;
            mainframe.inBattle = false;
        }
        networkController.BattleEnded(battle);
    }


    internal Battle GetBattle(long battleOID) {
        if (battles.ContainsKey(battleOID))
            return battles[battleOID];
        else return null;
    }

    internal void BattleRetreat(BattleMsg msg) {
        throw new NotImplementedException();
    }



    internal void BattleAction(BattleMsg msg) {
        Battle battle = GetBattle(msg.OID);
        switch (msg.BattleActionType) {
            case BattleActionMsg.UnitMoveMsg:
                UnitMoveMsg moveMsg = msg.GetBattleAction(new UnitMoveMsg());
                networkController.UnitMoved(battle, moveMsg);
                break;
            case BattleActionMsg.UnitAttackMsg:
                UnitAttackMsg attackMsg = msg.GetBattleAction(new UnitAttackMsg());
                networkController.UnitAttacked(battle, attackMsg);
                break;
            case BattleActionMsg.UnitAbilityMsg:
                UnitAbilityMsg abilityMsg = msg.GetBattleAction(new UnitAbilityMsg());
                networkController.UnitAbilityUsed(battle, abilityMsg);
                break;
            default:
                throw new BattleException();
        }

    }

    private int unitCount = 0;

    public static void DeleteUnitsFromInv(List<UnitMsg> units) {
        if (GameMainframe.GetInstance().debug) {
            units.ForEach(u => instance.unitsData.RemoveAll(un => u.OID == un.OID));
            return;
        }
        //need to fix
        units.ForEach(u => instance.inv.DeleteItemWithOID(OID.fromLong(u.OID)));
    }


    public static void addUnitsToInventory(List<UnitMsg> units) {

        if (GameMainframe.GetInstance().debug) {
            instance.unitsData.AddRange(units);
            return;
        }
        units.ForEach(u => NetworkAPI.SendTargetedCommand(instance.clientOID, "/gi " + u.PrefabId));
    }

    public List<DataUnit> getUnitsDataFromInv() {
        return Proto.MsgsToData(getUnitsMsgsFromInv());
    }

    List<UnitMsg> unitsData = null;

    public static List<UnitMsg> GenerateRandomUnitList(int minUnits, int maxUnits) {
        List<UnitMsg> units = new List<UnitMsg>();
        Dictionary<int, Unit> dic = instance.unitsDic;
        KeyValuePair<int, Unit>[] arr = dic.ToArray();
        Unit u;

        int count = UnityEngine.Random.Range(minUnits, maxUnits);
        for (int i = 0; i < count; i++) {
            int index = UnityEngine.Random.Range(0, dic.Count);
            u = arr[index].Value;
            if (u != null) {
                DataUnit d = new DataUnit();
                d.Setup(u);
                d.OID = instance.unitCount++;
                d.PrefabID = u.prefabId;
                units.Add(d.GetMsg());
            }
        }
        return units;
    }

    public static List<UnitMsg> getUnitsMsgsFromInv() {
        Dictionary<int, Unit> dic = instance.unitsDic;

        Unit u;
        if (GameMainframe.GetInstance().debug) {
            KeyValuePair<int, Unit>[] arr = dic.ToArray();
            if (instance.unitsData == null)
                instance.unitsData = new List<UnitMsg>();
            else
                return instance.unitsData;
            instance.unitsData = GenerateRandomUnitList(2, 3);
            return instance.unitsData;

        }

        List<UnitMsg> unitsData = new List<UnitMsg>();
        foreach (Bag bag in instance.inv.Bags.Values) {
            foreach (AtavismInventoryItem item in bag.items.Values) {
                int tempId = item.templateId;
                dic.TryGetValue(tempId, out u);
                if (u != null) {
                    DataUnit d = new DataUnit();
                    d.Setup(u);
                    d.OID = item.ItemId.ToLong();
                    d.PrefabID = item.templateId;
                    d.name = item.name;
                    unitsData.Add(d.GetMsg());
                }
            }
        }
        return unitsData;
    }

    public static List<Unit> GetUnits(List<UnitMsg> unitsData, bool clone = false) {
        List<Unit> clones = new List<Unit>();
        unitsData.ForEach(d =>
        {
            //Debug.Log(d.PrefabId);
            Unit u = instance.unitsDic[d.PrefabId];
            if (clone) {
                u = Instantiate(u);
                u.name = d.UnitName;
                u.ModifyStatsToData(new DataUnit(d), d.DataId);
            }
            clones.Add(u);
            
        });
        return clones;
    }
    public static List<Unit> GetUnits(List<DataUnit> unitsData, bool instantiate) {
        List<Unit> prefabs = new List<Unit>();
        unitsData.ForEach(d =>
        {
            //Debug.Log(d.name + " " + d.PrefabID);
            Unit u = instance.unitsDic[d.PrefabID];
            prefabs.Add(u);
            d.unit = u;

        });
        if (instantiate) {
            List<Unit> instantiated = new List<Unit>();
            foreach (DataUnit d in unitsData) {
                d.unit = Instantiate(d.unit);
                d.unit.ModifyStatsToData(d, d.DataId);

                instantiated.Add(d.unit);
            }
            return instantiated;
        }
        return prefabs;
    }
    public List<GameObject> debugObjects = new List<GameObject>();
    public void DebugStartBattle() {
        Dictionary<long, GameObject> targets = new Dictionary<long, GameObject>();
        for (int i = 0; i < debugObjects.Count; i++) {
            targets.Add(i, debugObjects[i]);
        }
        //TacticalNetwork.SendStartBattle(targets);
    }

    public Dictionary<long,AtavismMobNode> GetNearPlayers() {
        Dictionary<long, AtavismMobNode> targets = new Dictionary<long, AtavismMobNode>();
        List<long> worldObjOIDs = ClientAPI.WorldManager.GetObjectOidList();
        foreach (AtavismMobNode mob in ClientAPI.WorldManager.GetMobNodes()) {
            float distance = Vector3.Distance(mob.Position, ClientAPI.GetPlayerObject().Position);
            if (mob.Oid == GameMainframe.GetInstance().clientOID) continue; 
            if (mob.PropertyExists("unitsCount") && (int)mob.GetProperty("unitsCount")>0 &&  distance < 16.0)
                targets.Add(mob.Oid, mob);
        }
        return targets;
    }

    public void StartBattle(AtavismMobNode mobNode) {
        
        Dictionary<long, AtavismMobNode> targets = GetNearPlayers();
        foreach (long playerOid in targets.Keys) {
            AtavismMobNode mob = targets[playerOid];
            
        }
        targets[mobNode.Oid]= mobNode;
        StartBattle(targets);
    }





    public void StartBattle(Dictionary<long, AtavismMobNode> targets) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(100);
        Vector3 battlePosition = new Vector3(0, 0, 0);
        foreach (AtavismObjectNode obj in targets.Values) {
            battlePosition += obj.Position;

        }
        battlePosition /= targets.Count;
        int height = 0, maxHeight = 100;
        for (height = 0; height < maxHeight; height++) {
            battlePosition.y++;
            if (!Physics.CheckBox(battlePosition, battleCollider.transform.localScale / 2)) break;


        }
        if (height == maxHeight) {
            battleCollider.transform.position = battlePosition;
            StartCoroutine(NewBattleCollides());
            ClientAPI.Write("Cannot start battle, not enough room. Try to find open space.");
            return;
        }
        int factionIDs = 0;
        List<DataUnit> invUnits = getUnitsDataFromInv();      
        Offset<ArmyMsg>[] armies = new Offset<ArmyMsg>[targets.Count + 1];
        int i = 0;
        Vector3 p = GameMainframe.GetInstance().GetClientObject().transform.position;       
        armies[i++] = proto.addArmyInfo(fbb, GameMainframe.GetInstance().clientOID, invUnits,null, factionIDs++, p);
        foreach (KeyValuePair<long, AtavismMobNode> player in targets) {
            p = player.Value.Position;
            armies[i++] = proto.addArmyInfo(fbb, player.Key, null, null,factionIDs++, p);
        }
        VectorOffset armiesVec = BattleMsg.CreateArmiesVector(fbb, armies);
        BattleMsg.StartBattleMsg(fbb);
        BattleMsg.AddSubjectOID(fbb, GameMainframe.GetInstance().GetClientOID());
        BattleMsg.AddOID(fbb, 666);
        BattleMsg.AddArmies(fbb, armiesVec);
        BattleMsg.AddCmd(fbb, BattleCmd.PLAYER_STARTED_BATTLE);
        BattleMsg.AddPosition(fbb, Vec3.CreateVec3(fbb, battlePosition.x, battlePosition.y, battlePosition.z));
        BattleMsg.AddSubjectOID(fbb, clientOID);
        Offset<BattleMsg> battleOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(battleOffset.Value);
        BattleMsg battleMsg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(battleMsg);
    }

    IEnumerator NewBattleCollides() {
        yield return null;
        battleCollider.SetActive(true);
        yield return new WaitForSeconds(4f);
        battleCollider.SetActive(false);
        yield return null;
    }



    public Battle InstantiateBattleTemplate(Vector3 battlePosition = default(Vector3)) {

        GameObject newBattle = (GameObject)Instantiate(emptyBattlePrefab.gameObject, battlePosition, Quaternion.identity);
        Battle battle = newBattle.GetComponent<Battle>();
        battle.controller = battleController;
        return battle;
    }


    public void BattleInfo(BattleMsg battleMsg) {
        Battle battle = GetBattle(battleMsg.OID);
        // Debug.Log("BattleManager started battle:  " + battleMsg.OID + " started");
        if (battle == null) {
            Vec3 pos = battleMsg.Position;
            Vector3 battlePosition = new Vector3(pos.X, pos.Y, pos.Z);
            battle = InstantiateBattleTemplate(battlePosition);
            battle.battleOID = battleMsg.OID;
            battle.gameObject.SetActive(true);
            DontDestroyOnLoad(battle);
        }

        networkController.BattleStarted(battle, battleMsg);
        networkController.ArmiesJoined(battle, battleMsg);
        networkController.ArmiesDeployed(battle, battleMsg);
        if (battleMsg.BattlePhase == _GamePhase.Play.GetHashCode())
            networkController.BattleCommenced(battle, battleMsg);

    }


    public void BattleStarted(BattleMsg battleMsg) {
        Battle battle = GetBattle(battleMsg.OID);
        // Debug.Log("BattleManager started battle:  " + battleMsg.OID + " started");
        if (battle == null) {
            Vec3 pos = battleMsg.Position;
            Vector3 battlePosition = new Vector3(pos.X, pos.Y, pos.Z);
            battle = InstantiateBattleTemplate(battlePosition);
            battle.battleOID = battleMsg.OID;
            battle.gameObject.SetActive(true);
        }
        networkController.BattleStarted(battle, battleMsg);
        

    }




    void Update() {

    }
}
