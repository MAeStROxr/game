﻿using UnityEngine;
using System.Collections;
using TBTK;
using System.Collections.Generic;
using System.IO;
using System;
using mygame.proto;
using FlatBuffers;
using NT;

public class TacticalNetworkController : TacticalLocalController
{


    // Update is called once per frame
    void Update() {
        //debug = battleData.debug;
    }



    new void Start() {
        //base.Start();
        battleManager = BattleManager.GetInstance();
        //BattleManager.RegisterBattle(battleData);
    }

    override public void InitBattle(Battle battle) {

    }

    public IEnumerator SendMessageRoutine(BattleMsg battleMsg, float delay = 0.5f) {
        yield return new WaitForSeconds(delay);
        SendBattleMessage(battleMsg);
    }


    public void SendBattleMessage(BattleMsg battleMsg) {
        BattleManager.GetInstance().SendBattleMessage(battleMsg);

        //else
        //BattleManager.GetInstance().SendBattleMessage(battleData.battleOID, subtype, props);

    }

    public long clientOID { get { return GameMainframe.GetInstance().clientOID; } }
    public void DirectMessageBySubtype(Battle battle, BattleMsg battleMsg) {
        //string prefix = "tactical.";
        //Tactical cmd = (Tactical)Enum.Parse(typeof(Tactical), subtype);

        switch (battleMsg.Cmd) {
            case BattleCmd.UNIT_MOVE:
                UnitMoveMsg moveMsg = new UnitMoveMsg();
                UnitMoved(battle, battleMsg.GetBattleAction(moveMsg));
                break;
            case BattleCmd.UNIT_ATTACK:
                UnitAttackMsg attackMsg = new UnitAttackMsg();
                UnitAttacked(battle, battleMsg.GetBattleAction(attackMsg));
                break;
            case BattleCmd.UNIT_ABILITY:
                UnitAbilityMsg abilityMsg = new UnitAbilityMsg();
                UnitAbilityUsed(battle, battleMsg.GetBattleAction(abilityMsg));
                break;
            case BattleCmd.PLAYER_JOINED_BATTLE:
                ArmiesJoined(battle, battleMsg);
                break;
            case BattleCmd.PLAYER_STARTED_BATTLE:
                BattleStarted(battle, battleMsg);
                break;
            case BattleCmd.PLAYER_DEPLOYED_ARMY:
                ArmiesDeployed(battle, battleMsg);
                break;
            case BattleCmd.PLAYER_ENDED_TURN:
                TurnEnded(battle);
                break;
            case BattleCmd.BATTLE_COMMENCED:
                BattleCommenced(battle, battleMsg);
                break;
        }

    }


    public BattleMsg GetBattleMsg(Battle battle, BattleCmd cmd) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        proto.addBattle(fbb, battle, cmd, clientOID);
        if (cmd == BattleCmd.PLAYER_STARTED_BATTLE || cmd == BattleCmd.PLAYER_ENDED_TURN)
            BattleMsg.AddCurrentFactionID(fbb, battle.factionManager.selectedFactionID);
        Offset<BattleMsg> offset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(offset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        return msg;
    }

    override public void StartBattle(Battle battle) {
        List<DataUnit> invUnits = BattleManager.GetInstance().getUnitsDataFromInv();
        BattleMsg battleMsg = GetBattleMsg(battle, BattleCmd.PLAYER_STARTED_BATTLE);
        SendBattleMessage(battleMsg);
    }



    virtual public void BattleStarted(Battle battle, BattleMsg battleMsg) {
        battle.waitingPlayers = battleMsg.ArmiesLength;
        battle.factionManager.ClearUnit(false);
        battle.factionManager.selectedFactionID = Math.Max(battleMsg.CurrentFactionID - 1, -1);
        //Debug.Log("Battle Started " + battle.battleOID);
        bool clientInBattle = false;
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg army = battleMsg.GetArmies(i);
            if (!army.IsNetworkPlayer) clientInBattle = true;
            AddArmy(battle, army.FactionID);
            SetArmy(battle, army);
            if (JoinArmy(battle, army)) {
                battle.waitingPlayers--;
            }

        }
        if (clientInBattle) {
           battle.UIObject.gameObject.SetActive(true);
            Vector3 moveToPos = new Vector3(battleMsg.Position.X, battleMsg.Position.Y + 2, battleMsg.Position.Z);
            GameMainframe.GetInstance().GetClientObject().transform.position = moveToPos;
        }
        if (battle.waitingPlayers == 0)
            base.StartBattle(battle);

        bool joinArmies = false;
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg a = battleMsg.GetArmies(i);
            Faction fac = battle.factionManager.factionList[a.FactionID];
            if (!fac.isJoined && !fac.isNetworkFaction && fac.isPlayerFaction) {
                GameMainframe.GetInstance().inBattle = true;
                GameMainframe.GetInstance().currentBattleOID = battleMsg.OID;
                joinArmies = true;
            }
        }
        if (joinArmies)
            JoinArmies(battle);
    }


    override public void JoinArmies(Battle battle) {
        BattleManager btlMngr = BattleManager.GetInstance();
        GameMainframe game = GameMainframe.GetInstance();
        List<DataUnit> invUnits = btlMngr.getUnitsDataFromInv();
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        
        List<Faction> facList = new List<Faction>(battle.factionManager.factionList);
        facList = facList.FindAll(f => !f.isJoined && f.isPlayerFaction && !f.isNetworkFaction);
        Offset<ArmyMsg>[] armiesOffset = new Offset<ArmyMsg>[facList.Count];
        Vector3 p = game.GetClientObject().transform.position;
        for (int i = 0; i < facList.Count; i++) armiesOffset[i] = proto.addArmyInfo(fbb, facList[i].armyOID, invUnits, null, facList[i].ID, p);


        VectorOffset armiesVec = BattleMsg.CreateArmiesVector(fbb, armiesOffset);
        BattleMsg.StartBattleMsg(fbb);
        BattleMsg.AddSubjectOID(fbb, game.GetClientOID());
        BattleMsg.AddOID(fbb, battle.battleOID);
        BattleMsg.AddArmies(fbb, armiesVec);
        BattleMsg.AddCmd(fbb, BattleCmd.PLAYER_JOINED_BATTLE);
        BattleMsg.AddSubjectOID(fbb, game.GetClientOID());
        Offset<BattleMsg> battleOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(battleOffset.Value);
        BattleMsg battleMsg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(battleMsg);

    }

    virtual public void ArmiesJoined(Battle battle, BattleMsg battleMsg) {
        List<ArmyMsg> armies = new List<ArmyMsg>();
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg army = battleMsg.GetArmies(i);
            //armies.Add(army);
            if (JoinArmy(battle, army))
                battle.waitingPlayers--;
            if (battle.gameControl.GetGamePhase() == _GamePhase.Play) {
                battle.factionManager.InitFaction(army.FactionID);
            }
        }
        base.JoinArmies(battle);
        if (battle.gameControl.GetGamePhase() == _GamePhase.Initialization &&
            battle.waitingPlayers <= 0) {
            base.StartBattle(battle);
        }
    }

    public bool JoinArmy(Battle battle, ArmyMsg army) {
        FactionManager facMngr = battle.factionManager;
        //AddArmy(battle, army.FactionID);


        Faction fac = facMngr.factionList[army.FactionID];
        if (army.ArmyUnitsLength == 0 || fac == null || fac.isJoined) return false;
        fac.isJoined = true;
        //SetArmy(battle, army);
        List<DataUnit> deployable = new List<DataUnit>();
        List<DataUnit> deployed = new List<DataUnit>();
        for (int i = 0; i < army.ArmyUnitsLength; i++) {
            DataUnit dataUnit = new DataUnit(army.GetArmyUnits(i));
            if (dataUnit.tile_id < 0)
                deployable.Add(dataUnit);
            else
                deployed.Add(dataUnit);
            //Debug.Log("unit " + army.GetArmyUnits(i).UnitName + " " + army.GetArmyUnits(i).PrefabId);
        }
        if (deployable.Count > 0) {
            fac.loadFromData = true;
            BattleManager.GetUnits(deployable, false);
            battle.data.SetLoadData(army.FactionID, deployable);
        }
        List<Tile> tileList = battle.gridManager.GetGrid().tileList;
        List<Unit> deployedUnits = BattleManager.GetUnits(deployed, true);
        facMngr.ModifyUnitsData(deployedUnits, deployed);
        fac.allUnitList = deployedUnits;
        return true;
    }

    private Color getFactionColor(int factionID) {
        switch (factionID) {
            case 0: return Color.red;
            case 1: return Color.yellow;
            case 2: return Color.blue;
            case 3: return Color.black;
            case 4: return Color.green;
            case 5: return Color.cyan;
            case 6: return Color.grey;
            default: return Color.white;
        }
    }

    private List<Tile> getFactionDeployableTiles(Battle battle, int factionID, int tilesAmount = 10) {
        GridManager gridMngr = battle.gridManager;
        Grid grid = gridMngr.GetGrid();
        float seedX = 0, seedY = 0;
        int offset = 0;
        bool vertical = false;
        switch (factionID) {

            case 0:
                seedX = 0.3f;
                seedY = 0;
                offset = -2;
                break;
            case 1:
                seedX = 0.3f;
                seedY = 1;
                offset = -2;
                break;
            case 2:
                seedX = 0;
                seedY = 0.3f;
                offset = 1;
                vertical = true;
                break;
            case 3:
                offset = 1;
                seedX = -1;
                seedY = 0.3f;
                
                vertical = true;
                break;
               
        }
        int startFromTile = (int)(seedX * (gridMngr.length-1) + seedY * (gridMngr.width-1) * (gridMngr.length));
        startFromTile += offset;
        int step = vertical ? gridMngr.length : 1;
        if (startFromTile < 0) startFromTile = 0;
        if (startFromTile >= grid.tileList.Count-tilesAmount) startFromTile = grid.tileList.Count - tilesAmount;
        List <Tile> deployableTiles = new List<Tile>();
        for (int i = 0; i < tilesAmount; i++) {
            startFromTile += step;
            deployableTiles.Add(grid.tileList[startFromTile]);
        }
        //deployableTiles.ForEach(t => t.deployAreaID = factionID);
        return deployableTiles;

    }

    public void AddArmy(Battle battle, int factionID = -1) {

        List<Faction> facList = battle.factionManager.factionList;
        if (factionID < 0)
            factionID = facList.Count;
        //Faction newFac = new Faction();
        if (factionID >= facList.Count) {
            while (facList.Count <= factionID) {
                Faction fac = new Faction();
                fac.isPlayerFaction = true;
                fac.isNetworkFaction = false;
                fac.requireDeployment = true;
                fac.isHostile = false;
                fac.loadFromData = false;
                fac.ID = facList.Count;
                fac.dataID = facList.Count;
                fac.color = getFactionColor(fac.ID);         
                facList.Add(fac);
            }
        }
        facList[factionID].deployableTileList = getFactionDeployableTiles(battle, factionID);
    }

    public void SetArmy(Battle battle, ArmyMsg army) {

        Faction fac = battle.factionManager.factionList[army.FactionID];
        if (fac.isSet) return;
        fac.isSet = true;
        fac.color = getFactionColor(fac.ID);
        fac.armyOID = army.OID;
        fac.isPlayerFaction = army.IsPlayerFaction;
        fac.isNetworkFaction = army.IsNetworkPlayer;
        fac.requireDeployment = army.RequireDeployment;
        fac.isHostile = army.IsHostile;
        fac.loadFromData = false;
        fac.ID = army.FactionID;
        fac.dataID = army.DataID;
        //fac.color = army.Color==null? getFactionColor(army.FactionID) : new Color(army.Color.X, army.Color.Y, army.Color.Z);
        //fac.color = getFactionColor(army.FactionID);
        List<Tile> deployableTiles = new List<Tile>();
        for (int i = 0; i < army.DeployableTilesLength; i++) {
            TileMsg tileMsg = army.GetDeployableTiles(i);
            Tile tile = getTile(battle, tileMsg);
            deployableTiles.Add(tile);
        }
        if (deployableTiles.Count > 0) {
            fac.deployableTileList = deployableTiles;
            
        }
        fac.deployableTileList.ForEach(t => t.deployAreaID = fac.ID);
    }




    /*
    public void ArmiesJoinedRecieved(Battle battle, BattleMsg battleMsg) {

        ArmiesJoined(battle, battleMsg);
    }*/
    public override void DeployArmies(Battle battle) {
        //battle.factionManager.CompleteDeployment();
        if (battle.factionManager.IsDeploymentComplete()) {
            battle.deploymentUI.Hide();
            BattleMsg battleDeployMsg = GetBattleMsg(battle, BattleCmd.PLAYER_DEPLOYED_ARMY);
            SendBattleMessage(battleDeployMsg);
        }
        

    }


    public void ArmiesDeployed(Battle battle, BattleMsg battleMsg) {
        for (int i = 0; i < battleMsg.ArmiesLength; i++) {
            ArmyMsg army = battleMsg.GetArmies(i);

            DeployArmy(battle, army);

            if (battle.gameControl.GetGamePhase() == _GamePhase.Play) {
                battle.factionManager.SetupFaction(army.FactionID);
            }
        }

        if (battle.factionManager.factionList.TrueForAll(f => f.isDeployed)) {
            BattleCommenced(battle, battleMsg);
        }


    }

    public void DeployArmy(Battle battle, ArmyMsg army) {
        FactionManager facMngr = battle.factionManager;
        Faction deployingFac = facMngr.GetFaction(army.FactionID);
        List<Tile> tileList = battle.gridManager.GetGrid().tileList;

        if (deployingFac.isDeployed) return;
        deployingFac.isDeployed = true;
        for (int i = 0; i < army.ArmyUnitsLength; i++) {

            UnitMsg unitMsg = army.GetArmyUnits(i);
            if (unitMsg.TileId < 0) continue;
            Unit foundUnit = deployingFac.startingUnitList.Find(u => u.dataID == unitMsg.DataId);
            List<Unit> removeFrom = deployingFac.startingUnitList;

            if (foundUnit == null) {
                foundUnit = battle.factionManager.deployedUnitList.Find(u => u.dataID == unitMsg.DataId);
                removeFrom = battle.factionManager.deployedUnitList;
            }
            if (foundUnit == null) {
                foundUnit = deployingFac.allUnitList.Find(u => u.dataID == unitMsg.DataId);
                removeFrom = deployingFac.allUnitList;
            }

            if (foundUnit == null)
                throw new BattleException();


            //if (battle.turnControl.GetTurnMode() == _TurnMode.UnitPerTurn)

            //else if (battle.turnControl.GetTurnMode() == _TurnMode.FactionUnitPerTurn)
            //   facMngr.allUnitList.Add(foundUnit);

            facMngr.DeployUnit(foundUnit, new DataUnit(unitMsg), tileList);
            removeFrom.Remove(foundUnit);
            deployingFac.allUnitList.Add(foundUnit);
            foundUnit.factionID = deployingFac.ID;

            foundUnit.gameObject.SetActive(true);



        }

        /*
            List<Unit> tempDeployed = facMngr.deployedUnitList;
            facMngr.deployedUnitList = new List<Unit>();
            facMngr.AutoDeployFaction(army.FactionID);
            facMngr.deployedUnitList.Clear();
            facMngr.deployedUnitList = tempDeployed;
            */
    }
    public override void CommenceBattle(Battle battle) {
        BattleMsg battleCommenceMsg = GetBattleMsg(battle, BattleCmd.BATTLE_COMMENCED);
        SendBattleMessage(battleCommenceMsg);
    }
    public void BattleCommenced(Battle battle, BattleMsg battleMsg) {
        if (battle.gameControl.GetGamePhase() == _GamePhase.UnitDeployment)
            base.CommenceBattle(battle);
    }
    public override void EndBattle(Battle battle) {
        BattleMsg battleEndMsg = GetBattleMsg(battle, BattleCmd.BATTLE_OVER);
        SendBattleMessage(battleEndMsg);
    }
    public void BattleEnded(Battle battle) {
        base.EndBattle(battle);
    }


    override public void MoveUnit(Battle battle, Unit unit, Tile tile) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<UnitMoveMsg> moveOffset = proto.addUnitMove(fbb, unit, tile);
        proto.addBattle(fbb, battle, BattleCmd.UNIT_MOVE, clientOID, moveOffset.Value);
        BattleMsg.AddBattleActionType(fbb, BattleActionMsg.UnitMoveMsg);
        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }



    public void UnitMoved(Battle battle, UnitMoveMsg moveMsg) {
        Debug.Log("entering network controller unit moved");
        Unit u = getUnit(battle, moveMsg.Unit);
        Tile t = getTile(battle, moveMsg.TargetedTile);
        base.MoveUnit(battle, u, t);
    }

    override public void AttackUnit(Battle battle, Unit unit, Unit attackedUnit, bool isCounter = false) {
        AttackInstance attInst = new AttackInstance(battle, unit, attackedUnit, isCounter);
        attInst.Process();
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<UnitAttackMsg> attackOffset = proto.addUnitAttack(fbb, unit, attackedUnit, attInst, unit.shootPointList.Count);
        proto.addBattle(fbb, battle, BattleCmd.UNIT_ATTACK, clientOID, attackOffset.Value);
        BattleMsg.AddBattleActionType(fbb, BattleActionMsg.UnitAttackMsg);
        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }

    virtual public void UnitAttacked(Battle battle, UnitAttackMsg attackMsg) {
        Unit attacking = getUnit(battle, attackMsg.Att.SrcUnit);
        Unit attacked = getUnit(battle, attackMsg.Att.TgtUnit);
        AttackInstance attInst = new AttackInstance(battle, attacking, attacked, attackMsg.Att.IsCounter);
        attInst.Process();
        attInst.SetData(attackMsg.Att);
        attInst.ShowAttack = true;
        base.UnitAttacked(battle, attacking, attacked, attInst);
    }



    override public void UnitAbility(Battle battle, Unit unit, UnitAbility ability, int abilityId, Tile tile, AttackInstance att) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        Offset<UnitAbilityMsg> abilityOffset = proto.addUnitAbility(fbb, unit, abilityId, tile, att);
        proto.addBattle(fbb, battle, BattleCmd.UNIT_ABILITY, clientOID, abilityOffset.Value);
        BattleMsg.AddBattleActionType(fbb, BattleActionMsg.UnitAbilityMsg);
        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }

    public void UnitAbilityUsed(Battle battle, UnitAbilityMsg abilityMsg) {
        Unit unit = getUnit(battle, abilityMsg.Activator);
        Tile t = getTile(battle, abilityMsg.TargetedTile);
        int abilityId = abilityMsg.AbilityID;
        UnitAbility ability = unit.abilityList[abilityId];
        AttackInstance attInst = new AttackInstance(battle, unit, abilityId);
        attInst.SetData(abilityMsg.Att);
        unit.abilityTargetedTile = t;
        base.UnitAbility(battle, unit, ability, abilityId, t, attInst);

    }

    public override void EndTurn(Battle battle) {
        FlatBufferBuilder fbb = new FlatBufferBuilder(10);
        proto.addBattle(fbb, battle, BattleCmd.PLAYER_ENDED_TURN, clientOID);

        Offset<BattleMsg> msgOffset = BattleMsg.EndBattleMsg(fbb);
        fbb.Finish(msgOffset.Value);
        BattleMsg msg = BattleMsg.GetRootAsBattleMsg(fbb.DataBuffer);
        SendBattleMessage(msg);
    }


    public void TurnEnded(Battle battle) {
        base.EndTurn(battle);

    }

    

    private Unit getUnit(Battle battle, UnitMsg msg) {
        List<Unit> units = battle.factionManager.GetAllUnitsOfFaction(msg.FactionID);
        return units.Find(u => msg.OID == u.oid);
    }

    private Tile getTile(Battle battle, TileMsg msg) {
        List<Tile> tiles = battle.gridManager.grid.tileList;
        return tiles[msg.TileId];
    }
    // Use this for initialization

}
