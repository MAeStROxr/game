﻿using UnityEngine;

[ExecuteInEditMode]
public class DebugNormalsTangents : MonoBehaviour 
{
    [SerializeField]
    private Mesh _target;

    [SerializeField, Range(0.1f,5)]
    private float _lineLength = 1.0f;

    public Mesh target
    {
        set {_target = value;}
    }

	void Update () 
    {
        if (_target == null)
            return;
	    Vector3[] verts = _target.vertices;
	    Vector3[] norms = _target.normals;
	    Vector4[] tangs = _target.tangents;
	    int vertCount = _target.vertexCount;

        Color green = new Color(0,1,0.2f, 0.3f);
        Color red = new Color(1,0,0.2f, 0.3f);

	    for(int v = 0; v < vertCount; v++)
	    {
	        Vector3 worldPos = transform.position + verts[v];
	        Vector3 normal = norms[v];
	        Vector4 tangent = tangs[v];
            Vector3 tanDir = tangent * tangent.w;

            Debug.DrawLine(worldPos, worldPos + normal * _lineLength, green, 0.01f, false);
            Debug.DrawLine(worldPos, worldPos + tanDir * _lineLength, red, 0.01f, false);
	    }
    }
}
