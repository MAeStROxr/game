﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(DebugNormalsTangents))]
public class DebugNormalTangentsEditor : Editor
{
    private Mesh _selectedMesh;

    public override void OnInspectorGUI()
    {
        DebugNormalsTangents _target = (DebugNormalsTangents)target;
        MeshFilter[] filters = _target.gameObject.GetComponentsInChildren<MeshFilter>();
        foreach(MeshFilter filter in filters)
        {
            if(filter.sharedMesh == null)
                continue;
            if(GUILayout.Button(filter.name + " " + filter.sharedMesh.name))
            {
                _selectedMesh = filter.sharedMesh;
                _target.target = _selectedMesh;
            }
        }

        DrawDefaultInspector();
    } 
}
