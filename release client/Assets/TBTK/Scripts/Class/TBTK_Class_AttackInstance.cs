﻿using UnityEngine;
using System.Collections;

using TBTK;
using System.Collections.Generic;
using mygame.proto;

namespace TBTK {

    //contains all the information of an attack (normal attack, counter attack, ability-shoot)
    //an instance of the class is generate each time an attack took place
    public class AttackInstance {
        AttackInstanceMsg msg;
        public List<DataUnit> UnitList;
        public Unit srcUnit;
        public Unit tgtUnit;
        public bool Processed;
        public bool ShowAttack = false;
        Battle battle;
        CoverSystem coverSystem { get { return battle.coverSystem; } }
        GameControl gameControl {  get { return battle.gameControl; } }
        public CoverSystem._CoverType coverType = CoverSystem._CoverType.None;

        public float HitChance = 1;
        public float CritChance = 0;
        public float StunChance = 0;
        public float SilentChance = 0;

        public bool IsAbility = false;
        public int UnitAbilityID = -1;

        public bool IsMelee = false;

        public bool IsCounter = false;
        
        

        public bool Missed = false;
        public bool Critical = false;
        public bool Stunned = false;
        public bool Silenced = false;
        public bool Flanked = false;
        public bool Destroyed = false;

        public float Damage = 0;
        public int Stun = 0;
        public int Silent = 0;

        public float DamageTableModifier = 1;
        public float FlankingBonus = 1;




        public AttackInstanceMsg GetData() {
            return msg;
        }

        public void SetData(AttackInstanceMsg attMsg) {
            
            this.IsCounter = attMsg.IsCounter;
            this.IsMelee = attMsg.IsMelee;
            this.Missed = attMsg.Missed;
            this.Processed = attMsg.Processed;
            //this.Calculated = attMsg.Calculated;

            this.Silenced = attMsg.Silenced;
            this.Silent = attMsg.Silent;

            this.Stun = attMsg.Stun;
            this.Stunned = attMsg.Stunned;
            this.UnitAbilityID = attMsg.UnitAbilityID;
            List<DataUnit> unitList = new List<DataUnit>();
            for (int i = 0; i < attMsg.UnitListLength; i++)
                unitList.Add(new DataUnit(attMsg.GetUnitList(i)));
            this.UnitList = unitList;

            this.Critical = attMsg.Critical;
            this.Damage = attMsg.Damage;
            this.Destroyed = attMsg.Destroyed;
            this.Flanked = attMsg.Flanked;
            this.IsAbility = attMsg.IsAbility;

            this.DamageTableModifier = attMsg.DamageTableModifier;
            this.FlankingBonus = attMsg.FlankingBonus;

            //this.FlankingBonus = attMsg.FlankingBonus;
            //this.HitChance = attMsg.HitChance;
            //this.SilentChance = attMsg.SilentChance;
            
        }
        //constructor for normal and counter attack
        public AttackInstance(Battle battleData, Unit sUnit, Unit tUnit, bool counter = false) {
            srcUnit = sUnit;
            tgtUnit = tUnit;
            //IsCounter = counter;
            battle = battleData;
            CalculateChance();
           // UnitList = new List<UnitMsg>();
        }


        
        //constructor for shooting ability
        public AttackInstance(Battle battleData, Unit sUnit, int abilityID, float hit = Mathf.Infinity) {
            //IsAbility = true;
            battle = battleData;
            srcUnit = sUnit;
            //UnitAbilityID = abilityID;
            HitChance = hit;
           
            //UnitList = new List<UnitMsg>();
           // if (Random.Range(0f, 1f) > HitChance) Missed = true;
        }


        public void SetTarget(Unit unit) {
            tgtUnit = unit;
            CalculateChance();
        }

        //calculate the chance and determine various condition
      
        

        private void CalculateChance() {
            //if (Calculated) return;
           // Calculated = true;

            float coverDodgeBonus = 0;
            float exposedCritBonus = 0;

            //if cover system is enabled, get the dodge and crit bonus
            if (gameControl.EnableCover()) {
                coverType = battle.coverSystem.GetCoverType(srcUnit.tile, tgtUnit.tile);
                if (coverType == CoverSystem._CoverType.Half) coverDodgeBonus = CoverSystem.GetHalfCoverDodgeBonus();
                else if (coverType == CoverSystem._CoverType.Full) coverDodgeBonus = CoverSystem.GetFullCoverDodgeBonus();
                else exposedCritBonus = CoverSystem.GetExposedCritChanceBonus();
            }

            //calculate the hit chance
            //float hit = !IsMelee ? srcUnit.GetHitChance() : srcUnit.GetHitChanceMelee();
            float dodge = tgtUnit.GetDodgeChance() + coverDodgeBonus;
           // HitChance = Mathf.Clamp(hit - dodge, 0f, 1f);

            //calculate the critical chance
           // float critHit = (!IsMelee ? srcUnit.GetCritChance() : srcUnit.GetCritChanceMelee()) + exposedCritBonus;
            float critAvoid = tgtUnit.GetCritAvoidance();
            //CritChance = Mathf.Clamp(critHit - critAvoid, 0f, 1f);

            //calculate stun chance
            float stunHit = srcUnit.GetStunChance();
            float stunAvoid = tgtUnit.GetStunAvoidance();
            StunChance = Mathf.Clamp(stunHit - stunAvoid, 0f, 1f);

            //calculate silent chance
            float silentHit = srcUnit.GetSilentChance();
            float silentAvoid = tgtUnit.GetSilentAvoidance();
            SilentChance = Mathf.Clamp(silentHit - silentAvoid, 0f, 1f);

            //check if flanking is enabled an applicable in this instance
            if (gameControl.EnableFlanking()) {
                //Vector2 dir=new Vector2(srcUnit.tile.pos.x-tgtUnit.tile.pos.x, srcUnit.tile.pos.z-tgtUnit.tile.pos.z);
                float angleTH = 180 - Mathf.Min(180, gameControl.GetFlankingAngle());
                Quaternion attackRotation = Quaternion.LookRotation(tgtUnit.tile.GetPos() - srcUnit.tile.GetPos());
                //Debug.Log(Quaternion.Angle(attackRotation, tgtUnit.thisT.rotation)+"    "+angleTH);
                //if (Quaternion.Angle(attackRotation, tgtUnit.thisT.rotation) < angleTH) Flanked = true;
            }
        }

        //do the stats processing
        public void Process() {
             if (Processed) return;

             if (IsAbility) {    //if this instance is for ability, then there's no need to calculate the rest of the stats
                 if (Random.Range(0f, 1f) > HitChance) {
                     Missed = true;


                 }
                 return;
             }

             Processed = true;
            Debug.Log("att instance processed");
             //first determine all the chances and condition
             CalculateChance();

             //if the attack missed, skip the rest of the calculation
             if (Random.Range(0f, 1f) > HitChance) {
                 Missed = true;
                 return;
             }

             //get the base damage
             if (!IsMelee) Damage = Random.Range(srcUnit.GetDamageMin(), srcUnit.GetDamageMax());
             else Damage = Random.Range(srcUnit.GetDamageMinMelee(), srcUnit.GetDamageMaxMelee());

             //modify the damage with damage to armor modifier
             int armorType = tgtUnit.armorType;
             int damageType = srcUnit.damageType;
             DamageTableModifier = DamageTable.GetModifier(armorType, damageType);
             Damage *= DamageTableModifier;

             //if this is a counter attack, modify the damage with counter modifier
             if (IsCounter) Damage *= gameControl.GetCounterDamageMultiplier();

             //this the target is flanked, apply the flanking bonus
             if (!IsCounter && Flanked) {
                 FlankingBonus = 1 + gameControl.GetFlankingBonus() + srcUnit.GetFlankingBonus() - tgtUnit.GetFlankedModifier();
                 Damage *= FlankingBonus;
             }

             //if the attack crits, add the critical multiplier
             if (Random.Range(0f, 1f) < CritChance) {
                 Critical = true;
                 Damage *= !IsMelee ? srcUnit.GetCritMultiplier() : srcUnit.GetCritMultiplierMelee();
             }

             //if the attack stuns the target, get the stun duration
             if (Random.Range(0f, 1f) < StunChance) {
                 Stunned = true;
                 Stun = srcUnit.GetStunDuration();
             }

             //if the attack stuns the target, get the stun duration
             if (Random.Range(0f, 1f) < SilentChance) {
                 Silenced = true;
                 Silent = srcUnit.GetSilentDuration();
             }

             //check if the unit is destroyed in this instance and make the destroyed flag according
             if (Damage > tgtUnit.HP) Destroyed = true;

             //Debug.Log("Damage: "+damage);
             //new TextOverlay(tgtUnit.GetTargetT().position, damage.ToString("f0"), Color.white);
         
        }




    }

    }