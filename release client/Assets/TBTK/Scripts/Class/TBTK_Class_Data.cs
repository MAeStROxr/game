﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


using TBTK;
using System;
using mygame.proto;
using FlatBuffers;

namespace TBTK{

	public class Data {//: MonoBehaviour {

		private  List<List<DataUnit>> factionLoadList=new List<List<DataUnit>>();
		private  List<List<DataUnit>> factionEndList=new List<List<DataUnit>>();
		
		//*******************************************************************//	
		//load
		public  void ClearLoadData(){
			factionLoadList=new List<List<DataUnit>>();
		}
		
		//on loadlist, the ID is always the index of the element of the list
		public  void SetLoadData(int ID, List<DataUnit> list){
			for(int i=0; i<list.Count; i++){
				if(list[i].unit==null){
					list.RemoveAt(i);
					i-=1;
				}
			}
            int idx = 0;
            list.ForEach(u => u.DataId = idx++);
            if (ID == factionLoadList.Count) factionLoadList.Add(list);
            else if (ID < factionLoadList.Count) {
                factionLoadList[ID]= list;
                //factionLoadList[ID] = list;
            }
            else {
                while (factionLoadList.Count < ID) factionLoadList.Add(null);
                factionLoadList.Add(list);
               
                
            }
        }
		
		public  List<DataUnit> GetLoadData(int ID){
			if(ID<0 || ID>=factionLoadList.Count) return null;
			return factionLoadList[ID];
		}
		
		
		//*******************************************************************//	
		//end
		public  bool EndDataExist(){
			return factionEndList.Count==0 ? false : true ;
		}
		
		public  void ClearEndData(){
			factionEndList=new List<List<DataUnit>>();
		}
		
		public  void SetEndData(int ID, List<DataUnit> list){
			if(ID==factionEndList.Count) factionEndList.Add(list);
			else if(ID<factionEndList.Count) factionEndList[ID]=list;
			else{
				while(factionEndList.Count<ID) factionEndList.Add(null);
				factionEndList.Add(list);
			}
		}
		
		public  List<DataUnit> GetEndData(int ID){
			if(ID<0 && ID>=factionEndList.Count) return null;
			return factionEndList[ID];
		}
		
		
		
	}



    [Serializable()]
    public class DataUnit  {
        UnitMsg unitMsg;
        public Unit unit;

        //these value are use to overwrite the default value of the unit spawned in game when set to >0
        //NT
        public int DataId = -1;
        public long OID = 0;
        public string name = "";
        public int tile_id = -1;
        public int FactionID;
        public int PrefabID = -1;
        public float DefaultHP = 0;
        public float DefaultAP = 0;
        //	
        //these value are use to overwrite the default value of the unit spawned in game when set to >0
        public int Level = 1;

        public float HP = -1;
        public float AP = -1;

        public float TurnPriority = -1;
        public int MoveRange = -1;
        public int AttackRange = -1;

        public float HitChance = -1;
        public float DodgeChance = -1;
        public float DamageMin = -1;
        public float DamageMax = -1;

        public float CritChance = -1;
        public float CritAvoidance = -1;
        public float CritMultiplier = -1;

        public float StunChance = -1;
        public float StunAvoidance = -1;
        public int StunDuration = -1;

        public float SilentChance = -1;
        public float SilentAvoidance = -1;
        public int SilentDuration = -1;

        public float HPPerTurn = -1;
        public float APPerTurn = -1;

        public override string ToString() {
            string s = "DataUnit:name-" + name + " prefabId-" + PrefabID + " dataId-" + DataId + " tileId-" + tile_id + " oid-" + OID;
            return s;
        }
        public void Setup(Unit unitInstance) {
            if (unitInstance == null) {
                Debug.LogWarning("Data's unit is not set", null);
                return;
            }

            unit = unitInstance;

            Level = unit.GetLevel();

            //nt
            HP = unit.HP;
            AP = unit.AP;
            DefaultHP = unit.defaultHP; //unit.GetFullHP();
            DefaultAP = unit.defaultAP; //unit.GetFullAP();

            TurnPriority = unit.turnPriority;
            MoveRange = unit.moveRange;
            AttackRange = unit.attackRange;
            HitChance = unit.hitChance;
            DodgeChance = unit.dodgeChance;
            DamageMin = unit.damageMin;
            DamageMax = unit.damageMax;
            CritChance = unit.critChance;
            CritAvoidance = unit.critAvoidance;
            CritMultiplier = unit.critMultiplier;
            StunChance = unit.stunChance;
            StunAvoidance = unit.stunAvoidance;
            StunDuration = unit.stunDuration;
            SilentChance = unit.silentChance;
            SilentAvoidance = unit.silentAvoidance;
            SilentDuration = unit.silentDuration;
            HPPerTurn = unit.HPPerTurn;
            APPerTurn = unit.APPerTurn;

            //nt
            OID = unit.oid;
            name = unit.name;
            PrefabID = unit.prefabId;
            if (unit.tileId >= 0) tile_id = unit.tileId;
            if (unit.GetDataID() >= 0) DataId = unit.GetDataID();
            FactionID = unit.factionID;
        }

        






        //these value are use to overwrite the default value of the unit spawned in game when set to >0
        //NT

        public UnitMsg GetMsg() {
            FlatBufferBuilder fbb = new FlatBufferBuilder(10);
            StringOffset nameOffset = fbb.CreateString(name);
            Offset<UnitMsg> offset = UnitMsg.CreateUnitMsg(fbb, OID, nameOffset, (short)PrefabID, (short)DataId, (short)tile_id, (short)FactionID,
                (short)Level, HP, AP, TurnPriority, (short)MoveRange, (short)AttackRange, HitChance, DodgeChance,
                DamageMin, DamageMax, CritChance, CritAvoidance, CritMultiplier, StunChance,
                StunAvoidance, StunDuration, SilentChance, SilentAvoidance, SilentDuration, HPPerTurn,
                APPerTurn, DefaultHP, DefaultAP);
            fbb.Finish(offset.Value);
            return UnitMsg.GetRootAsUnitMsg(fbb.DataBuffer);
        }
        public DataUnit(UnitMsg unit)  {
        unitMsg = unit;
        //data.Setup(unit);
       
           
            Level = unit.Level;
            //nt
            OID = unit.OID;
            name = unit.UnitName;
            PrefabID = unit.PrefabId;
            tile_id = unit.TileId;
            DataId = unit.DataId;
            FactionID = unit.FactionID;
            HP = unit.HP;
            AP = unit.AP;
            DefaultHP = unit.DefaultHP; //unit.GetFullHP();
            DefaultAP = unit.DefaultAP; //unit.GetFullAP();
             //nt


            TurnPriority = unit.TurnPriority;
            MoveRange = unit.MoveRange;
            AttackRange = unit.AttackRange;
            HitChance = unit.HitChance;
            DodgeChance = unit.DodgeChance;
            DamageMin = unit.DamageMin;
            DamageMax = unit.DamageMax;
            CritChance = unit.CritChance;
            CritAvoidance = unit.CritAvoidance;
            CritMultiplier = unit.CritMultiplier;
            StunChance = unit.StunChance;
            StunAvoidance = unit.StunAvoidance;
            StunDuration = unit.StunDuration;
            SilentChance = unit.SilentChance;
            SilentAvoidance = unit.SilentAvoidance;
            SilentDuration = unit.SilentDuration;
            HPPerTurn = unit.HPPerTurn;
            APPerTurn = unit.APPerTurn;
            
          
        }

        public DataUnit() {
        }

       
    }


    public class DataTile {
        public int tile_id;
        public bool walkable;
        public void Setup(Tile tile) {
            this.walkable = tile.walkable;
        }

    }
}