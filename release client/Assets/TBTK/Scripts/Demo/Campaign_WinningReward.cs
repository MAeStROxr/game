﻿using UnityEngine;
using System.Collections;

using TBTK;

namespace TBTK {

    public class Campaign_WinningReward : MonoBehaviour {
        public GameControl gameControl;
        void Awake()
        {
            Battle battleData = gameObject.GetComponentInParent<Battle>();
            gameControl = battleData.gameControl;
        }
        void OnEnable()
        {
            gameControl.onGameOverE += OnGameOver;
        }
        void OnDisable()
        {
            gameControl.onGameOverE -= OnGameOver;
        }

        //add currency when finishing a battle, for the sake of the demo, perk currency is used
        void OnGameOver(int facID)
        {
            PerkManager.SpendCurrency(-30); //use negative value on SpendCurrency() to add to the perk currency, otherwise it would subtract
        }

    }

}