﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TBTK;

public class SceneModifier_CoverSystem : MonoBehaviour {
    FactionManager factionManager
    {
        get
        {
            return battle.factionManager;
        }

    }
    public Battle battle;
    // Use this for initialization
    void Start () {
        Battle battle2 = gameObject.GetComponentInParent<Battle>();
        if (battle2 != null) battle = battle2;
        factionManager.onUnitDeploymentPhaseE += DeploymentPhaseChanged;
	}
	
	void DeploymentPhaseChanged(bool flag){
		if(!flag) StartCoroutine(DelayStart());
	}
	IEnumerator DelayStart(){
		yield return null;
		yield return null;
		yield return null;
		
		List<Unit> unitList=factionManager.GetAllUnit();
		for(int i=0; i<unitList.Count; i++){
			Unit unit=unitList[i];
			
			unit.movePerTurn=2;
			unit.moveRemain=2;
			//unit.sight+=1;
		}
	}
	
}
