﻿using UnityEngine;
using System.Collections;

using TBTK;

namespace TBTK {

    public enum _GamePhase {
        Initialization,         //the game is being initialized
        UnitDeployment,     //unit deployment is taking place
        Play,                   //the game is playing	
        Over,                   //game over
    }

    [RequireComponent(typeof(DamageTable))]
    [RequireComponent(typeof(TurnControl))]
    [RequireComponent(typeof(AIManager))]
    public class GameControl : MonoBehaviour {
        FactionManager factionManager {
            get {
                return battleData.factionManager;
            }

        }
        Data savedData {
            get {
                return battleData.data;
            }
        }

        public TurnControl turnControl {
            get {
                return battleData.turnControl;
            }
        }
        public GridManager gridManager {
            get {
                return battleData.gridManager;
            }
        }
        AbilityManagerFaction abilityManagerFaction {
            get {
                return battleData.abilityManagerFaction;
            }
        }
        AbilityManagerUnit abilityManagerUnit {
            get {
                return battleData.abilityManagerUnit;
            }
        }
        PerkManager perkManager {
            get {
                return battleData.perkManager;
            }
        }
        public Battle battleData;
        public delegate void GameMessageHandler(string msg);
        public event GameMessageHandler onGameMessageE; //listen by UI to display any game message

        public delegate void GameStartHandler();
        public event GameStartHandler onGameStartE; //called when game started (unit deployment finished) for UI overlays to show

        public delegate void IterateTurnHandler();
        public event IterateTurnHandler onIterateTurnE; //listen by EffectTracker and AbilityManager to keep track of the cd/duration and what not

        public delegate void GameOverHandler(int factionID);
        public event GameOverHandler onGameOverE;   //listen by AudioManager and UI


        public _GamePhase gamePhase = _GamePhase.Initialization;
        public _GamePhase GetGamePhase() { return gamePhase; }


        public Unit selectedUnit;   //the current selected unit, this is only for player's unit as AI unit dont get selected


        public bool useGlobalSetting = true;


        public bool enableManualUnitDeployment = true;
        public bool EnableManualUnitDeployment() { return instance.enableManualUnitDeployment; }

        public bool enableActionAfterAttack = false;
        public bool EnableActionAfterAttack() { return instance.enableActionAfterAttack; }

        public bool enableCounter = false;
        public bool EnableCounter() { return instance.enableCounter; }
        public float counterAPMultiplier = 1f;
        public float GetCounterAPMultiplier() { return !instance.useAPForAttack ? 0 : instance.counterAPMultiplier; }
        public float counterDamageMultiplier = 1f;
        public float GetCounterDamageMultiplier() { return instance.counterDamageMultiplier; }

        public bool restoreUnitAPOnTurn = true;
        public bool RestoreUnitAPOnTurn() { return instance != null ? instance.restoreUnitAPOnTurn : true; }

        public bool useAPForMove = true;
        public bool useAPForAttack = true;
        public bool UseAPForMove() { return instance.useAPForMove; }
        public bool UseAPForAttack() { return instance.useAPForAttack; }


        public bool attackThroughObstacle = false;
        public bool AttackThroughObstacle() { return instance.attackThroughObstacle; }


        public bool enableFogOfWar = false;
        public bool EnableFogOfWar() { return instance.enableFogOfWar; }
        public float peekFactor = 0.4f;
        public float GetPeekFactor() { return Mathf.Clamp(instance.peekFactor, 0, 0.5f); }


        public bool enableCover = false;
        public float exposedCritBonus = 0.3f;
        public float fullCoverBonus = 0.75f;
        public float halfCoverBonus = 0.25f;
        public bool EnableCover() { return instance.enableCover; }
        public float GetFullCoverBonus() { return instance.fullCoverBonus; }
        public float GetHalfCoverBonus() { return instance.halfCoverBonus; }
        public int effectiveCoverAngle = 45;
        public int GetEffectiveCoverAngle() { return instance.effectiveCoverAngle; }


        public bool enableFlanking = false;
        public float flankingAngle = 120;
        public float flankingBonus = .5f;
        public bool EnableFlanking() { return instance.enableFlanking; }
        public float GetFlankingAngle() { return instance.flankingAngle; }
        public float GetFlankingBonus() { return instance.flankingBonus; }


        public bool enableActionCam = false;
        public float actionCamFreqAttack = 0.25f;
        public float actionCamFreqAbility = 0.5f;
        public float GetAttackActionCamFreq() { return instance.enableActionCam ? instance.actionCamFreqAttack : 0; }
        public float GetAbilityActionCamFreq() { return instance.enableActionCam ? instance.actionCamFreqAbility : 0; }


        public string nextScene = "";
        public string mainMenu = "";
        public void LoadNextScene() { if (instance.nextScene != "") Load(instance.nextScene); }
        public void LoadMainMenu() { if (instance.mainMenu != "") Load(instance.mainMenu); }
        public void Load(string levelName) {
            //so that the factionData can be set even if the game is not over (lost unit remain lost)
            if (instance.gamePhase != _GamePhase.Over) factionManager.GameOver();
            Application.LoadLevel(levelName);
        }


        private GameObject defaultShootObject;  //the backup shootObject to use, in case a unit prefab has not shootObject
        public GameObject GetDefaultShootObject() { return instance.defaultShootObject; }


        public GameControl instance { get { return battleData.gameControl; } }





        void Awake() {
            if (battleData==null)
              battleData = gameObject.GetComponentInParent<Battle>();
            //Init();
        }
        public void Init() {

            savedData.ClearEndData();

            SettingDB settingDB = InitSetting();

            if (enableCover) {
                CoverSystem.SetFullCoverDodgeBonus(fullCoverBonus);
                CoverSystem.SetHalfCoverDodgeBonus(halfCoverBonus);
                CoverSystem.SetExposedCritChanceBonus(exposedCritBonus);
            }

            //get the instance of each component and initiate them, the order in which these component matters

            if (perkManager != null) perkManager.Init();

            abilityManagerUnit.Init();

            // turnControl = (TurnControl)FindObjectOfType(typeof(TurnControl));
            turnControl.Init();

            if (settingDB != null) {
                turnControl.turnMode = settingDB.turnMode;
                turnControl.moveOrder = settingDB.moveOrder;
            }

            if (settingDB != null) gridManager.generateGridOnStart = settingDB.generateGridOnStart;
            gridManager.Init();

            if (settingDB != null) factionManager.generateUnitOnStart = settingDB.generateUnitOnStart;
            factionManager.Init();

            gridManager.SetupGridForFogOfWar();

            defaultShootObject = Resources.Load("ScenePrefab/DefaultShootObject", typeof(GameObject)) as GameObject;

            gamePhase = _GamePhase.Initialization;
        }

        public void StartGamePhases() {
            if (!factionManager.RequireManualUnitDeployment()) StartCoroutine(DelayStartGame(0.5f));
            else StartCoroutine(DelayUnitDeployment(0.25f));
        }

        void Start() {
           // StartGamePhases();
        }
        bool gameStarted = false;
        //start the game, this is called after unit deployment is complete, or after initialization if no deployment is required
        public void StartGame() { instance.StartCoroutine(instance.DelayStartGame(0.5f)); }
        IEnumerator DelayStartGame(float delay = 0.5f) {
            yield return null;
            if (gameStarted) yield break;
            gameStarted = true;
            int factions = 0;
            factionManager.factionList.ForEach(f => {
                f.ID = factions++;
                factionManager.SetupFaction(f.ID);
                
            });
            gridManager.SetupGridForFogOfWar();
            yield return null;

            if (delay > 0) yield return new WaitForSeconds(delay);

            abilityManagerFaction.StartCounter();   //for ability energy to start charging

            if (onGameStartE != null) onGameStartE();

            gamePhase = _GamePhase.Play;
            turnControl.EndTurn();  //this will initiate unit selection and start the game
            yield return null;
        }

        //called if unit deployment is required
        IEnumerator DelayUnitDeployment(float delay = 0.5f) {
            if (delay > 0) yield return new WaitForSeconds(delay);
            gamePhase = _GamePhase.UnitDeployment;
            factionManager.StartUnitDeploymentPhase();
            yield return null;
        }

        


        private bool allowUnitSelect = true;    //lock unit select after a unit has been moved
        public bool AllowUnitSelect() { return instance.allowUnitSelect; }
        public void LockUnitSelect() {
            if (turnControl.GetTurnMode() == _TurnMode.FactionUnitPerTurn) instance.allowUnitSelect = false;
        }
        public void UnlockUnitSelect() { instance.allowUnitSelect = true; }


        //function to select unit, unit selection start here
        public void SelectUnit(Tile tile) { SelectUnit(tile.unit); }
        public void SelectUnit(Unit unit, bool reselect = false) {
            if (factionManager.IsPlayerFaction(unit.factionID) && !factionManager.IsNetworkFaction(unit.factionID)) {
                ClearSelectedUnit();
                selectedUnit = unit;
                gridManager.Select(unit);
                unit.Select(reselect);

            } 
            else if (!factionManager.IsPlayerFaction(unit.factionID) && !factionManager.IsNetworkFaction(unit.factionID)) {  //used in FactionUnitPerTurn & UnitPerTurn mode
                ClearSelectedUnit();
                battleData.aiManager.MoveUnit(unit);
            } else {
                ClearSelectedUnit();
            }


        }
        public void ClearSelectedUnit() {
            if (selectedUnit != null) {
                selectedUnit.ClearSelectedAbility();
                selectedUnit.Deselect();
            }
            gridManager.ClearAllTile();

            selectedUnit = null;

        }




        public void GameOver(int factionID) {
            if (factionManager.IsPlayerFaction(factionID)) {
                PerkManager.GainPerkCurrencyOnVictory();
            }

            onGameMessageE("GameOver");

            gamePhase = _GamePhase.Over;

            factionManager.GameOver();

            if (onGameOverE != null) onGameOverE(factionID);
        }




        void OnEnable() {
            battleData.unitListeners.onUnitDestroyedE += OnUnitDestroyed;
        }
        void OnDisable() {
            battleData.unitListeners.onUnitDestroyedE -= OnUnitDestroyed;
        }

        void OnUnitDestroyed(Unit unit) {
            if (turnControl.GetTurnMode() == _TurnMode.FactionPerTurn) return;
            if (turnControl.GetTurnMode() == _TurnMode.FactionUnitPerTurn) {
                if (turnControl.GetMoveOrder() == _MoveOrder.Free) return;
            }

            if (onIterateTurnE != null) onIterateTurnE();   //listen by EffectTracker and AbilityManager to iterate effect and cd duration
                                                            //listen by tile in for tracking forceVisible(scan)
        }

        //end the turn, called when EndTurn button are pressed or when a unit has used up all its move(in FactionUnitPerTurn & UnitPerTurn mode)
        public void EndTurn() {

            if (onIterateTurnE != null) onIterateTurnE();   //listen by EffectTracker and AbilityManager to iterate effect and cd duration
                                                            //listen by tile in for tracking forceVisible(scan)

            Debug.Log("end turn, selected faction:"+factionManager.selectedFactionID);

            ClearSelectedUnit();
            turnControl.EndTurn();
        }



        private SettingDB InitSetting() {
            //if set to use global setting, overwrite all the local setting with the one from DB
            if (!useGlobalSetting) return null;

            SettingDB db = SettingDB.LoadDB();
            if (db == null) return null;

            enableManualUnitDeployment = db.enableManualUnitDeployment;

            enableActionAfterAttack = db.enableActionAfterAttack;

            enableCounter = db.enableCounter;
            counterAPMultiplier = db.counterAPMultiplier;
            counterDamageMultiplier = db.counterDamageMultiplier;

            restoreUnitAPOnTurn = db.restoreUnitAPOnTurn;

            useAPForMove = db.useAPForMove;
            useAPForAttack = db.useAPForAttack;

            attackThroughObstacle = db.attackThroughObstacle;

            enableFogOfWar = db.enableFogOfWar;
            peekFactor = db.peekFactor;

            enableCover = db.enableCover;
            exposedCritBonus = db.exposedCritBonus;
            fullCoverBonus = db.fullCoverBonus;
            halfCoverBonus = db.halfCoverBonus;

            enableFlanking = db.enableFlanking;
            flankingAngle = db.flankingAngle;
            flankingBonus = db.flankingBonus;

            return db;
        }



        public void DisplayMessage(string msg) {
            if (onGameMessageE != null) onGameMessageE(msg);
        }

    }

}