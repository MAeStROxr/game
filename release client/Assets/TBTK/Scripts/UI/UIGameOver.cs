﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using TBTK;

namespace TBTK{

	public class UIGameOver : MonoBehaviour {
        GameControl gameControl
        {
            get
            {
                return battleData.gameControl;
            }
        }
        FactionManager factionManager
        {
            get
            {
                return battleData.factionManager;
            }

        }
        Battle battleData;
        public Text labelTitle;
		
		private static UIGameOver instance;
		
		private GameObject thisObj;
		
		void Start(){
            battleData = gameObject.GetComponentInParent<Battle>();

            instance = this;
			thisObj=gameObject;
			
			transform.localPosition=Vector3.zero;
			
			_Hide();
		}
		
		
		public void OnNextLevelButton(){
			Time.timeScale=1;
			gameControl.LoadNextScene();
		}
		public void OnRestartButton(){
			Time.timeScale=1;
			Application.LoadLevel(Application.loadedLevelName);
		}
		public void OnMainMenuButton(){
			Time.timeScale=1;
			gameControl.LoadMainMenu();
		}
		
		
		void UpdateDisplay(int factionID){
			Faction fac=factionManager.GetFaction(factionID);
			if(!fac.isPlayerFaction || fac.isNetworkFaction) labelTitle.text="Game Over";
			else{
				if(factionManager.GetPlayerFactionCount()==1) labelTitle.text="Victory!";
				else	labelTitle.text="Player "+factionID+" Wins!";
			}
		}
		
		
		public static bool isOn=true;
		public static void Show(int factionID){ instance._Show(factionID); }
		public void _Show(int factionID){
			UpdateDisplay(factionID);
			
			isOn=true;
			thisObj.SetActive(isOn);
		}
		public static void Hide(){ instance._Hide(); }
		public void _Hide(){
			isOn=false;
			thisObj.SetActive(isOn);
		}
		
		
	}

}