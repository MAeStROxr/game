﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TBTK;
using mygame.proto;

namespace TBTK {

    public class Unit : MonoBehaviour {
        GameControl gameControl {
            get {
                return battle.gameControl;
            }
        }
        GridManager gridManager {
            get {
                return battle.gridManager;
            }
        }
        FactionManager factionManager {
            get {
                return battle.factionManager;
            }

        }
        public Battle battle;
        TurnControl turnControl {
            get {
                return battle.turnControl;
            }
        }
        AbilityManagerFaction abilityManagerFaction {
            get {
                return battle.abilityManagerFaction;
            }
        }
        AbilityManagerUnit abilityManagerUnit {
            get {
                return battle.abilityManagerUnit;
            }
        }

        
        public event UnitListener.ActionCamHandler onActionCamE; 
        public event UnitListener.UnitDestroyedHandler onUnitDestroyedE;      //fire when the unit is destroyed
        public event UnitListener.UnitSelectedHandler onUnitSelectedE;            //listen by UI only  & UIOverlay(for display cover icon)
        public event UnitListener.UnitMoveDepletedHandler onMoveDepletedE;
        

        [HideInInspector]
        public bool isObjectUnit = false;


        public int prefabId;
        public int instanceID;
        public int factionID;
        /* NT */
        public int tileId = -1;
        public long oid = 1;
        /**/

        public int dataID = -1;//use to identiy unit in LoadData 
        public int GetDataID() { return dataID; }

        public string unitName = "Unit";
        public string desp = "";
        public Sprite iconSprite;

        [HideInInspector]
        public bool trigger = false;
        [HideInInspector]
        public bool isAIUnit = false;
        [HideInInspector]
        public Unit lastAttacker;
        [HideInInspector]
        public Unit lastTarget;

        public int damageType = 0;
        public int armorType = 0;

        public Tile tile;   //occupied tile

        public float hitThreshold = 0.25f;
        public Transform targetPoint;
        public Transform GetTargetT() { return targetPoint == null ? thisT : targetPoint; }

        public bool requireDirectLOSToAttack = true;

        public int value = 5;

        public float moveSpeed = 10;
        [HideInInspector]
        private float rotateSpeed = 3;

        public DurationCounter stunCounter;
        public int stunned = 0;         //if >0, unit is stunned
        private int disableAbilities = 0;   //if >0, unit cannot used ability
        public bool IsStunned() { return stunned > 0 ? true : false; }
        public bool DisableAbilities() { return disableAbilities > 0 ? true : false; }

        public DurationCounter silentCounter;
        public int silenced = 0;
        public bool IsSilenced() { return silenced > 0 ? true : false; }

        [Header("Basic Stats")]
        public float defaultHP = 10;
        public float defaultAP = 10;
        public float HP = 10;
        public float AP = 10;

        public float moveAPCost = 0;
        public float attackAPCost = 0;

        public float turnPriority = 1;

        public int moveRange = 3;
        public int attackRange = 3;
        public int sight = 6;

        public int movePerTurn = 1;
        public int attackPerTurn = 1;
        public int counterPerTurn = 1;  //counter attack
        [HideInInspector]
        public int moveRemain = 1;
        //[HideInInspector] 
        public int attackRemain = 1;
        [HideInInspector]
        public int counterRemain = 1;

        public float damageMin = 3;
        public float damageMax = 6;

        public float hitChance = .8f;
        public float dodgeChance = .15f;

        public float critChance = 0.1f;
        public float critAvoidance = 0;
        public float critMultiplier = 2;

        public float stunChance = 0;
        public float stunAvoidance = 0;
        public int stunDuration = 1;

        public float silentChance = 0;
        public float silentAvoidance = 0;
        public int silentDuration = 1;

        public float flankingBonus = 0;
        public float flankedModifier = 0;

        public float HPPerTurn = 0;
        public float APPerTurn = 0;


        [Header("Hybrid Unit")]
        public bool isHybridUnit;
        public int meleeRange;

        public float damageMinMelee = 3;
        public float damageMaxMelee = 6;

        public float hitChanceMelee = .8f;
        //public float dodgeChanceMelee=.15f;

        public float critChanceMelee = 0.1f;
        //public float critAvoidanceMelee=0;
        public float critMultiplierMelee = 2;

        public int GetMeleeRange() { return meleeRange; }


        //********************************************************************************************************************************
        //these section are functions that get active stats of unit

        public float GetFullHP() { return defaultHP * (1 + GetEffHPBuff() + PerkManager.GetUnitHPBuff(prefabId)); }
        public float GetFullAP() { return defaultAP * (1 + GetEffAPBuff() + PerkManager.GetUnitAPBuff(prefabId)); }

        public float GetMoveAPCost() { return (gameControl.UseAPForMove()) ? Mathf.Max(0, moveAPCost + GetEffMoveAPCost() + PerkManager.GetUnitMoveAPCost(prefabId)) : 0; }
        public float GetAttackAPCost() { return (gameControl.UseAPForAttack()) ? Mathf.Max(0, attackAPCost + GetEffAttackAPCost() + PerkManager.GetUnitAttackAPCost(prefabId)) : 0; }
        public float GetCounterAPCost() { return GetAttackAPCost() * gameControl.GetCounterAPMultiplier(); }

        public float GetTurnPriority() { return turnPriority + GetEffTurnPriority() + PerkManager.GetUnitTurnPriority(prefabId); }

        public int GetMoveRange() { return moveRange + GetEffMoveRange() + PerkManager.GetUnitMoveRange(prefabId); }
        public int GetAttackRange() { return attackRange + GetEffAttackRange() + tile.GetAttackRange() + PerkManager.GetUnitAttackRange(prefabId); }
        public int GetSight() { return sight + GetEffSight() + tile.GetSight() + PerkManager.GetUnitSight(prefabId); }

        public int GetMovePerTurn() { return movePerTurn + GetEffMovePerTurn() + PerkManager.GetUnitMovePerTurn(prefabId); }
        public int GetAttackPerTurn() { return attackPerTurn + GetEffAttackPerTurn() + PerkManager.GetUnitAttackPerTurn(prefabId); }
        public int GetCounterPerTurn() { return counterPerTurn + GetEffCounterPerTurn() + PerkManager.GetUnitCounterPerTurn(prefabId); }

        public float GetDamageMin() { return damageMin * (1 + GetEffDamage() + tile.GetDamage() + PerkManager.GetUnitDamage(prefabId)); }
        public float GetDamageMax() { return damageMax * (1 + GetEffDamage() + tile.GetDamage() + PerkManager.GetUnitDamage(prefabId)); }

        public float GetHitChance() { return hitChance + GetEffHitChance() + tile.GetHitChance() + PerkManager.GetUnitHitChance(prefabId); }
        public float GetDodgeChance() { return dodgeChance + GetEffDodgeChance() + tile.GetDodgeChance() + PerkManager.GetUnitDodgeChance(prefabId); }

        public float GetCritChance() { return critChance + GetEffCritChance() + tile.GetCritChance() + PerkManager.GetUnitCritChance(prefabId); }
        public float GetCritAvoidance() { return critAvoidance + GetEffCritAvoidance() + tile.GetCritAvoidance() + PerkManager.GetUnitCritChance(prefabId); }
        public float GetCritMultiplier() { return critMultiplier + GetEffCritMultiplier() + tile.GetCritMultiplier() + PerkManager.GetUnitCritChance(prefabId); }

        public float GetStunChance() { return stunChance + GetEffStunChance() + tile.GetStunChance() + PerkManager.GetUnitStunChance(prefabId); }
        public float GetStunAvoidance() { return stunAvoidance + GetEffStunAvoidance() + tile.GetStunAvoidance() + PerkManager.GetUnitStunAvoidance(prefabId); }
        public int GetStunDuration() { return stunDuration + GetEffStunDuration() + tile.GetStunDuration() + PerkManager.GetUnitStunDuration(prefabId); }

        public float GetSilentChance() { return silentChance + GetEffSilentChance() + tile.GetSilentChance() + PerkManager.GetUnitSilentChance(prefabId); }
        public float GetSilentAvoidance() { return silentAvoidance + GetEffSilentAvoidance() + tile.GetSilentAvoidance() + PerkManager.GetUnitSilentAvoidance(prefabId); }
        public int GetSilentDuration() { return silentDuration + GetEffSilentDuration() + tile.GetSilentDuration() + PerkManager.GetUnitSilentDuration(prefabId); }

        public float GetFlankingBonus() { return flankingBonus + GetEffFlankingBonus() + PerkManager.GetUnitFlankingBonus(prefabId); }
        public float GetFlankedModifier() { return flankedModifier + GetEffFlankedModifier() + PerkManager.GetUnitFlankedModifier(prefabId); }

        public float GetHPPerTurn() { return HPPerTurn * (1 + GetEffHPPerTurn() + tile.GetHPPerTurn() + PerkManager.GetUnitHPPerTurn(prefabId)); }
        public float GetAPPerTurn() { return APPerTurn * (1 + GetEffAPPerTurn() + tile.GetAPPerTurn() + PerkManager.GetUnitAPPerTurn(prefabId)); }

        public float GetHPRatio() { return HP / GetFullHP(); }
        public float GetAPRatio() { return AP / GetFullAP(); }

        public int GetEffectiveMoveRange() {
            if (movePerTurn == 0) return 0;
            float apCost = GetMoveAPCost();
            int apAllowance = apCost == 0 ? 999999 : (int)Mathf.Abs(AP / apCost);
            return Mathf.Min(GetMoveRange(), apAllowance);
        }

        public bool CanAttack() {
            bool apFlag = gameControl.UseAPForAttack() ? AP >= GetAttackAPCost() : true;
            return attackRemain > 0 & GetAttackRange() > 0 & stunned <= 0 & apFlag;
        }
        public bool CanMove() { return moveRemain > 0 & GetEffectiveMoveRange() > 0 & stunned <= 0; }
        public bool CanUseAbilities() { return disableAbilities < 0 & stunned <= 0 & silenced <= 0; }




        public float GetDamageMinMelee() { return damageMinMelee * (1 + GetEffDamage() + tile.GetDamage() + PerkManager.GetUnitDamage(prefabId)); }
        public float GetDamageMaxMelee() { return damageMaxMelee * (1 + GetEffDamage() + tile.GetDamage() + PerkManager.GetUnitDamage(prefabId)); }

        public float GetHitChanceMelee() { return hitChanceMelee + GetEffHitChance() + tile.GetHitChance() + PerkManager.GetUnitHitChance(prefabId); }
        //public float GetDodgeChanceMelee(){ return dodgeChanceMelee+GetEffDodgeChance()+tile.GetDodgeChance()+PerkManager.GetUnitDodgeChance(prefabID); }

        public float GetCritChanceMelee() { return critChanceMelee + GetEffCritChance() + tile.GetCritChance() + PerkManager.GetUnitCritChance(prefabId); }
        //public float GetCritAvoidanceMelee(){ return critAvoidanceMelee+GetEffCritAvoidance()+tile.GetCritAvoidance()+PerkManager.GetUnitCritChance(prefabID); }
        public float GetCritMultiplierMelee() { return critMultiplierMelee + GetEffCritMultiplier() + tile.GetCritMultiplier() + PerkManager.GetUnitCritChance(prefabId); }



        //end get stats section
        //********************************************************************************************************************************



        //********************************************************************************************************************************
        //these section are related to ability effects
        [Header("Misc")]
        public List<Effect> effectList = new List<Effect>();
        public List<Effect> GetEffectList() { return effectList; }
        public void ApplyEffect(Effect eff, AttackInstance att = null) {
            //new TextOverlay(GetTargetT().position, eff.name, Color.white);

            float HPVal = Random.Range(eff.HPMin, eff.HPMax);
            if (att != null) {
                if (att.Processed) {
                    // TODO:
                    DataUnit dataUnit = att.UnitList.Find
                        (u => u.DataId == dataID && u.FactionID == factionID);
                    HPVal = dataUnit.HP;
                    if (HPVal > 0) RestoreHP(HPVal);
                    else if (HPVal < 0) {
                        ApplyDamage(-HPVal * DamageTable.GetModifier(armorType, eff.damageType), false, att.ShowAttack);
                    }
                }
                else {
                    DataUnit u = GetData();
                    u.HP = HPVal;
                    att.UnitList.Add(u);
                    return;
                }
            } else {
                if (HPVal > 0) RestoreHP(HPVal);
                else if (HPVal < 0) {
                    ApplyDamage(-HPVal * DamageTable.GetModifier(armorType, eff.damageType), false, att.ShowAttack);
                }
            }


            if (eff.stun > 0 || eff.duration >= 0) AddUnitToEffectTracker();

            if (eff.duration >= 0) {
                effectList.Add(eff);
                eff.StartDurationCount();
            }

           

            AP = Mathf.Clamp(AP + Mathf.Min(eff.APMin, eff.APMax), 0, GetFullAP());

            if (eff.stun > 0 && stunned < eff.stun) {
                stunned = eff.stun;
                stunCounter.Count(eff.stun);
                VisualEffectManager.UnitStunned(this, stunned);
            }

            if (eff.unitStat.sight != 0 && !isAIUnit) SetupFogOfWar();

            if (RequireReselect(eff)) {
                moveRemain += eff.unitStat.movePerTurn;
                attackRemain += eff.unitStat.attackPerTurn;
                if (gameControl.selectedUnit == this) gameControl.SelectUnit(tile);
            }
        }
        public bool ProcessEffectList() {
            if (stunned > 0) {
                stunCounter.Iterate();
                stunned = (int)stunCounter.duration;
            }

            if (silenced > 0) {
                silentCounter.Iterate();
                silenced = (int)silentCounter.duration;
            }

            bool turnPriorityChanged = false;

            for (int i = 0; i < effectList.Count; i++) {
                effectList[i].IterateDuration();

                if (effectList[i].duration <= 0) {
                    if (effectList[i].unitStat.turnPriority != 0) turnPriorityChanged = true;
                    effectList.RemoveAt(i);
                    i -= 1;
                }
            }

            if (stunned == 0 && silenced == 0 && effectList.Count == 0) EffectTracker.RemoveUnitWithEffect(this);

            return turnPriorityChanged;
        }

        public bool RequireReselect(Effect eff) {
            if (eff.unitStat.movePerTurn != 0) return true;
            if (eff.unitStat.attackPerTurn != 0) return true;
            if (eff.unitStat.moveRange != 0) return true;
            if (eff.unitStat.attackRange != 0) return true;
            if (eff.unitStat.sight != 0) return true;
            return false;
        }

        private void AddUnitToEffectTracker() {
            if (stunned <= 0 || silenced <= 0 || effectList.Count == 0) {
                EffectTracker.AddUnitWithEffect(this);
            }
        }


        float GetEffHPBuff() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.HPBuff;
            return value;
        }
        float GetEffAPBuff() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.APBuff;
            return value;
        }

        float GetEffMoveAPCost() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.moveAPCost;
            return value;
        }
        float GetEffAttackAPCost() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.attackAPCost;
            return value;
        }

        float GetEffTurnPriority() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.turnPriority;
            return value;
        }

        int GetEffMoveRange() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.moveRange;
            return value;
        }
        int GetEffAttackRange() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.attackRange;
            return value;
        }
        int GetEffSight() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.sight;
            return value;
        }

        int GetEffAttackPerTurn() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.attackPerTurn;
            return value;
        }
        int GetEffMovePerTurn() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.movePerTurn;
            return value;
        }
        int GetEffCounterPerTurn() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.counterPerTurn;
            return value;
        }

        float GetEffDamage() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.damage;
            return value;
        }
        //float GetEffDamageMax(){
        //	float value=0;
        //	for(int i=0; i<effectList.Count; i++) value+=effectList[i].unitStat.damageMax;
        //	return value;
        //}

        float GetEffHitChance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.hitChance;
            return value;
        }
        float GetEffDodgeChance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.dodgeChance;
            return value;
        }

        float GetEffCritChance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.critChance;
            return value;
        }
        float GetEffCritAvoidance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.critAvoidance;
            return value;
        }
        float GetEffCritMultiplier() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.critMultiplier;
            return value;
        }

        float GetEffStunChance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.stunChance;
            return value;
        }
        float GetEffStunAvoidance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.stunAvoidance;
            return value;
        }
        int GetEffStunDuration() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.stunDuration;
            return value;
        }

        float GetEffSilentChance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.silentChance;
            return value;
        }
        float GetEffSilentAvoidance() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.silentAvoidance;
            return value;
        }
        int GetEffSilentDuration() {
            int value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.silentDuration;
            return value;
        }

        float GetEffFlankingBonus() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.flankingBonus;
            return value;
        }
        float GetEffFlankedModifier() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.flankedModifier;
            return value;
        }

        float GetEffHPPerTurn() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.HPPerTurn;
            return value;
        }
        float GetEffAPPerTurn() {
            float value = 0;
            for (int i = 0; i < effectList.Count; i++) value += effectList[i].unitStat.APPerTurn;
            return value;
        }

        //end ability effect section
        //********************************************************************************************************************************




        private int level = 1;  //for data only, has no effect on game
        public int GetLevel() { return level; }
        public void SetLevel(int lvl) { level = lvl; }

        public DataUnit GetData() {
            DataUnit data = new DataUnit();
            //data.unit=this;
            //data.level=level;

            data.Setup(this);

            return data;
        }
        //called by FactionManager in InitFaction to setup the stats of unit loaded from data
        public void ModifyStatsToData(DataUnit data, int ID) {
            dataID = ID;
            if (data.Level > 0)
                level = data.Level;
            if (data.DefaultHP > 0)
                defaultHP = data.DefaultHP;
            if (data.HP > 0)
                HP = data.HP;
            else HP = defaultHP;
            if (data.DefaultAP > 0)
                defaultAP = data.DefaultAP;
            if (data.AP > 0)
                AP = data.AP;
            else AP = defaultAP;


            if (data.TurnPriority > 0) turnPriority = data.TurnPriority;

            if (data.MoveRange > 0) moveRange = data.MoveRange;
            if (data.AttackRange > 0) attackRange = data.AttackRange;

            if (data.HitChance > 0) hitChance = data.HitChance;
            if (data.DodgeChance > 0) dodgeChance = data.DodgeChance;
            if (data.DamageMin > 0) damageMin = data.DamageMin;
            if (data.DamageMax > 0) damageMax = data.DamageMax;

            if (data.CritChance > 0) critChance = data.CritChance;
            if (data.CritAvoidance > 0) critAvoidance = data.CritAvoidance;
            if (data.CritMultiplier > 0) critMultiplier = data.CritMultiplier;

            if (data.StunChance > 0) stunChance = data.StunChance;
            if (data.StunAvoidance > 0) stunAvoidance = data.StunAvoidance;
            if (data.StunDuration > 0) stunDuration = data.StunDuration;

            if (data.SilentChance > 0) silentChance = data.SilentChance;
            if (data.SilentAvoidance > 0) silentAvoidance = data.SilentAvoidance;
            if (data.SilentDuration > 0) silentDuration = data.SilentDuration;

            if (data.HPPerTurn > 0) HPPerTurn = data.HPPerTurn;
            if (data.APPerTurn > 0) APPerTurn = data.APPerTurn;

            /*NT*/
            if (data.tile_id >= 0) tileId = data.tile_id;
            this.oid = data.OID;
            //prefabId = data.PrefabID;
            if (data.FactionID >= 0)
                factionID = data.FactionID;
            if (data.DataId > 0)
                dataID = data.DataId;
            /**/
        }



        //********************************************************************************************************************************
        //these section are related to UnitAbilities

        public List<int> abilityIDList = new List<int>();
        public List<UnitAbility> abilityList = new List<UnitAbility>(); //only used in runtime
        public List<UnitAbility> GetAbilityList() { return abilityList; }
        public void InitUnitAbility() {
            //get bonus abilityID from perk and add it to perkAbilityIDList
            List<int> perkAbilityIDList = PerkManager.GetUnitAbilityIDList(prefabId);
            for (int i = 0; i < perkAbilityIDList.Count; i++) abilityIDList.Add(perkAbilityIDList[i]);

            List<int> perkAbilityXIDList = PerkManager.GetUnitAbilityXIDList(prefabId);
            for (int i = 0; i < perkAbilityXIDList.Count; i++) abilityIDList.Remove(perkAbilityXIDList[i]);

            abilityList = abilityManagerUnit.GetAbilityListBasedOnIDList(abilityIDList);
            for (int i = 0; i < abilityList.Count; i++) abilityList[i].SetUnit(this);
        }
        public UnitAbility GetUnitAbility(int ID) { return abilityList[ID]; }

        public int selectedAbilityID = -1;
        public int GetSelectedAbilityID() { return selectedAbilityID; }
        public void ClearSelectedAbility() {
            if (selectedAbilityID < 0) return;
            selectedAbilityID = -1;
            gridManager.ClearTargetMode();
            battle.unitListeners.unitSelectedHandler(this);
        }
        private bool requireTargetSelection = false;        //indicate if current selected Ability require target selection

        public string SelectAbility(int ID) {   //called from UI to select an ability
            if (abilityManagerFaction.GetSelectedAbilityID() >= 0) abilityManagerFaction.ClearSelectedAbility();

            if (selectedAbilityID >= 0) {
                if (selectedAbilityID == ID) {
                    if (!requireTargetSelection)
                        battle.controller.SetUnitAndAbility(battle,this, selectedAbilityID)(tile);

                    //AbilityTargetSelected(tile);	
                    ClearSelectedAbility();
                    return "";
                }
                else ClearSelectedAbility();
            }

            selectedAbilityID = ID;
            UnitAbility ability = abilityList[ID];

            string exception = ability.IsAvailable();
            if (exception != "") return exception;

            selectedAbilityID = ID;

            requireTargetSelection = ability.requireTargetSelection;
            if (requireTargetSelection)

                gridManager.AbilityTargetMode(tile, ability.GetRange(), ability.GetAOERange(),
                    ability.normalAttack, ability.requireDirectLOS, ability.targetType,
                    battle.controller.SetUnitAndAbility(battle,this, selectedAbilityID));

            //if(!ability.requireTargetSelection) AbilityTargetSelected(tile);	//not target selection required, just cast the ability on occupied tile
            //else gridManager.AbilityTargetMode(tile, ability.GetRange(), ability.GetAOERange(), ability.targetType, this.AbilityTargetSelected);

            battle.unitListeners.unitSelectedHandler(this);

            return "";
        }

        //callback function for when a target tile has been selected for current active ability
        public Tile abilityTargetedTile;
        public void AbilityTargetSelected(Tile tile, AttackInstance remoteAttack = null) {
            if (tile == null) return;
            AttackInstance attInstance;
            if (remoteAttack != null) attInstance = remoteAttack;
            else attInstance = new AttackInstance(battle, this, selectedAbilityID);

            abilityTargetedTile = tile;

            UnitAbility ability = abilityList[selectedAbilityID];
            AP -= ability.GetCost();
            ability.Use();

            abilityManagerUnit.AbilityActivated();

            if (ability.shoot) {
                GameObject shootObj = ability.shootObject;
                if (shootObj == null) shootObj = gameControl.GetDefaultShootObject();

                //if (onActionCamE != null) onActionCamE(this, tile.GetPos(), gameControl.GetAbilityActionCamFreq());
                battle.unitListeners.actionCamHandler(this, tile.GetPos(), gameControl.GetAbilityActionCamFreq());

                StartCoroutine(AttackRoutine(tile, tile.unit, shootObj, attInstance));
            }
            else {
                AbilityShootObjectHit(attInstance);
            }

            if (!ability.enableMoveAfterCast) moveRemain = 0;
            if (!ability.enableAttackAfterCast) attackRemain = 0;
            if (!ability.enableAbilityAfterCast) disableAbilities = 1;
        }
        //callback function when the shootObject of an ability which require shoot hits it's target
        public void AbilityShootObjectHit(AttackInstance attInst) {
            if (attInst.Processed) {
                if (attInst.Missed) {
                    abilityTargetedTile.GetPos();
                    new TextOverlay(abilityTargetedTile.GetPos(), "missed", Color.white);
                    return;
                }
                else {
                    abilityManagerUnit.ApplyAbilityEffect(this, abilityTargetedTile, abilityList[attInst.UnitAbilityID], attInst);

                }
            }
            else {
                //attInst.Process();
                if (!attInst.Missed)
                    abilityManagerUnit.ApplyAbilityEffect(this, abilityTargetedTile, abilityList[attInst.UnitAbilityID], attInst);

            }

        }

        //this is for ability teleport and spawnNewUnit to settle in new unit
        public void SetNewTile(Tile targetTile) {
            if (tile != null) tile.unit = null;
            tile = targetTile;
            targetTile.unit = this;
            thisT.position = targetTile.GetPos();

            factionManager.UpdateHostileUnitTriggerStatus(this);
            SetupFogOfWar();
        }

        //end UnitAbilities section
        //********************************************************************************************************************************





        //[HideInInspector]
        public Transform thisT;
        [HideInInspector]
        public GameObject thisObj;

        void Awake() {
            thisT = transform;
            thisObj = gameObject;
           
            
            for (int i = 0; i < shootPointList.Count; i++) {
                if (shootPointList[i] == null) {
                    shootPointList.RemoveAt(i);
                    i -= 1;
                }
            }

            if (shootPointList.Count == 0) shootPointList.Add(thisT);

            if (turretObject == null) turretObject = thisT;
        }

        void Start() {
            if (battle == null) {
                battle = gameObject.transform.root.GetComponentInChildren<Battle>();
                if (battle == null) {
                    FactionManager facMngr = gameObject.transform.parent.GetComponent<FactionManager>();
                    battle = facMngr.battleData;
                }
            }
            silentCounter = new DurationCounter(battle);
            stunCounter = new DurationCounter(battle);
            HP = GetFullHP();
            if (gameControl.RestoreUnitAPOnTurn()) AP = GetFullAP();

        }

        void OnEnable() {
            if (battle != null)
                abilityManagerUnit.onIterateAbilityCooldownE += OnIterateAbilityCooldown;

        }
        void OnDisable() {

            if (battle!=null)
                abilityManagerUnit.onIterateAbilityCooldownE -= OnIterateAbilityCooldown;
        }

        void OnIterateAbilityCooldown() {
            for (int i = 0; i < abilityList.Count; i++) abilityList[i].IterateCooldown();
        }



        public void Select(bool reselect = false) {
            if (!reselect && unitAudio != null) unitAudio.Select();

            battle.unitListeners.unitSelectedHandler(this);

        }
        public void Deselect() {
            battle.unitListeners.unitSelectedHandler(null);
        }


        private bool rotating = false;
        private Quaternion facingRotation;
        public void Rotate(Quaternion rotation) {
            facingRotation = rotation;
            if (!rotating) StartCoroutine(_Rotate());
        }
        IEnumerator _Rotate() {
            rotating = true;
            while (Quaternion.Angle(facingRotation, thisT.rotation) > 0.5f) {
                if (!turnControl.ClearToProceed()) yield break;
                thisT.rotation = Quaternion.Lerp(thisT.rotation, facingRotation, Time.deltaTime * moveSpeed * 2);
                yield return null;
            }
            rotating = false;
        }

        public void Move(Tile targetTile) {
            Debug.Log("Move entered, move Remain:" +moveRemain);
            if (moveRemain <= 0) return;

            moveRemain -= 1;
            //AP-=GetMoveAPCost()*(int)Mathf.Min(GetEffectiveMoveRange(), gridManager.GetDistance(tile, targetTile));

            Debug.Log("moving " + name + " to " + targetTile);
            gameControl.LockUnitSelect();

            StartCoroutine(MoveRoutine(targetTile));
        }
        public IEnumerator MoveRoutine(Tile targetTile) {
            tile.unit = null;
            gridManager.ClearAllTile();

            List<Tile> path = AStar.SearchWalkableTile(tile, targetTile, gridManager);

            while (path.Count > GetEffectiveMoveRange()) path.RemoveAt(path.Count - 1);

            AP -= GetMoveAPCost() * path.Count;

            if (gridManager.GetTileType() == _TileType.Square) {    //smooth the path so the unit wont zig-zag along a diagonal direction
                path.Insert(0, tile);
                path = PathSmoothing.SmoothDiagonal(path, gridManager);
                path.RemoveAt(0);
            }

            while (!turnControl.ClearToProceed()) yield return null;

            if (gameControl.EnableFogOfWar() && isAIUnit && path.Count > 0) {
                bool pathVisible = false;
                for (int i = 0; i < path.Count; i++) {
                    if (path[i].IsVisible()) {
                        pathVisible = true;
                        break;
                    }
                }

                if (!pathVisible) {
                    thisT.position = path[path.Count - 1].GetPos();
                    tile = path[path.Count - 1];
                    tile.unit = this;
                    yield break;
                }
            }

            //path.Insert(0, tile);
            //PathSmoothing.Smooth(path);
            //path.RemoveAt(0);

            turnControl.ActionCommenced();

            if (path.Count != 0) {
                if (unitAnim != null) unitAnim.Move();
                if (unitAudio != null) unitAudio.Move();
            }

            while (path.Count > 0) {

                UpdateVisibility(path[0]);

                /*
				//for path smoothing with subpath witin tile
				List<Vector3> tilePath=path[0].GetPath();
				while(tilePath.Count>0){
					while(true){
						Quaternion wantedRot=Quaternion.LookRotation(tilePath[0]-thisT.position);
						thisT.rotation=Quaternion.Lerp(thisT.rotation, wantedRot, Time.deltaTime*moveSpeed*4);
						
						float dist=Vector3.Distance(thisT.position, tilePath[0]);
						if(dist<0.05f) break;
						
						Vector3 dir=(tilePath[0]-thisT.position).normalized;
						thisT.Translate(dir*Mathf.Min(moveSpeed*Time.deltaTime, dist), Space.World);
						yield return null;
					}
					
					tilePath.RemoveAt(0);
				}
				*/

                while (true) {
                    float dist = Vector3.Distance(thisT.position, path[0].GetPos());
                    if (dist < 0.05f) break;

                    Quaternion wantedRot = Quaternion.LookRotation(path[0].GetPos() - thisT.position);
                    thisT.rotation = Quaternion.Slerp(thisT.rotation, wantedRot, Time.deltaTime * moveSpeed * 3);

                    Vector3 dir = (path[0].GetPos() - thisT.position).normalized;
                    thisT.Translate(dir * Mathf.Min(moveSpeed * Time.deltaTime, dist), Space.World);
                    yield return null;
                }

                tile = path[0];

                factionManager.UpdateHostileUnitTriggerStatus(this);
                SetupFogOfWar();

                path.RemoveAt(0);
            }

            if (unitAnim != null) unitAnim.StopMove();

            tile.unit = this;
            tileId = tile.tileId;
            thisT.position = tile.GetPos();

            turnControl.ActionCompleted(0.15f);

            FinishAction();
        }




        public void Attack(Unit targetUnit, AttackInstance attInstance) {
            if (attInstance.tgtUnit == null) {
                Debug.Log("null target unit in attack instance");
            }

            if (attackRemain == 0) return;
            if (AP < GetAttackAPCost()) return;

            attackRemain -= 1;
            AP -= GetAttackAPCost();

            Debug.Log(this.unitName + " attacking " + targetUnit.unitName+" for damage:"+attInstance.Damage);//+"      "+gridManager.GetDistance(tile, targetUnit.tile, true)+"    "+GetAttackRange()+"     "+FogOfWar.InLOS(tile, targetUnit.tile)+"      attackRemain:"+attackRemain);//.unitName);
            gameControl.LockUnitSelect();

            GameObject shootObj = null;
            
            

            if (isHybridUnit && gridManager.GetDistance(tile, targetUnit.tile) <= meleeRange) {
                attInstance.IsMelee = true;
                shootObj = shootObjectMelee != null ? shootObjectMelee.gameObject : null;
            }
            else {
                shootObj = shootObject != null ? shootObject.gameObject : null;
            }

            attInstance.Process();

            if (shootObj == null) shootObj = gameControl.GetDefaultShootObject();

            battle.unitListeners.actionCamHandler(this, targetUnit.GetTargetT().position, gameControl.GetAbilityActionCamFreq());

            StartCoroutine(AttackRoutine(targetUnit.tile, targetUnit, shootObj, attInstance));

            if (!gameControl.EnableActionAfterAttack()) {
                moveRemain = 0;
                disableAbilities = 1;
            }
        }



        public bool rotateTurretOnly = false;
        public bool rotateTurretAimInXAxis = true;
        public Transform turretObject;
        public Transform barrelObject;
        public ShootObject shootObject;
        public ShootObject shootObjectMelee;
        public List<Transform> shootPointList = new List<Transform>();
        public float delayBetweenShootPoint = 0;

        private bool aiming = false;    //to avoid aiming and rotate back to origin at the same time
        bool Aiming(Tile tile, Unit targetUnit = null) {
            Quaternion wantedRotY = Quaternion.LookRotation(tile.GetPos() - thisT.position);
            Vector3 targetPos = (targetUnit == null) ? tile.GetPos() : targetUnit.GetTargetT().position;

            //rotate body
            if (!rotateTurretOnly && thisT != turretObject) {
                thisT.rotation = Quaternion.Slerp(thisT.rotation, wantedRotY, Time.deltaTime * rotateSpeed);
            }

            if (rotateTurretAimInXAxis) {
                if (barrelObject != null) {
                    turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRotY, Time.deltaTime * rotateSpeed);

                    Quaternion wantedRot = Quaternion.LookRotation(targetPos - barrelObject.position);
                    //if(targetUnit==null) wantedRot=Quaternion.LookRotation(tile.GetPos()-barrelObject.position);
                    //else wantedRot=Quaternion.LookRotation(targetUnit.GetTargetT().position-barrelObject.position);

                    Quaternion barrelRot = Quaternion.Euler(wantedRot.eulerAngles.x, 0, 0);
                    barrelObject.localRotation = Quaternion.Slerp(barrelObject.localRotation, barrelRot, Time.deltaTime * rotateSpeed);

                    float angle1 = Quaternion.Angle(turretObject.rotation, wantedRotY);
                    float angle2 = Quaternion.Angle(barrelObject.localRotation, barrelRot);
                    //float angle=Quaternion.Angle(barrelObject.rotation, wantedRot);
                    if (angle1 < 0.5f && angle2 < 0.5f) return true;
                }
                else {
                    Quaternion wantedRot = Quaternion.LookRotation(targetPos - turretObject.position);
                    turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRot, Time.deltaTime * rotateSpeed);
                    float angle = Quaternion.Angle(turretObject.rotation, wantedRot);
                    if (angle < .5f) return true;
                }
            }
            else {
                turretObject.rotation = Quaternion.Slerp(turretObject.rotation, wantedRotY, Time.deltaTime * rotateSpeed);
                if (Mathf.Abs(turretObject.rotation.eulerAngles.y - wantedRotY.eulerAngles.y) < 1f) return true;
                //float angle=Quaternion.Angle(turretObject.rotation, wantedRotY);
                //if(angle<1f) return true;
            }

            return false;
        }
        bool RotateTurretToOrigin() {
            turretObject.localRotation = Quaternion.Lerp(turretObject.localRotation, Quaternion.identity, Time.deltaTime * rotateSpeed);
            float angle = Quaternion.Angle(turretObject.localRotation, Quaternion.identity);

            if (barrelObject != null) {
                barrelObject.localRotation = Quaternion.Slerp(barrelObject.localRotation, Quaternion.identity, Time.deltaTime * rotateSpeed);
                float angleAlt = Quaternion.Angle(barrelObject.localRotation, Quaternion.identity);
                return (angle > 1 || angleAlt > 1) ? false : true;
            }
            else return angle > 1 ? false : true;
        }

        //void OnGUI(){
        //	Vector3 screenPos=Camera.main.WorldToScreenPoint(thisT.position);
        //	GUI.Label(new Rect(screenPos.x, Screen.height-screenPos.y, 60, 20), aiming.ToString());
        //}

        public IEnumerator AttackRoutine(Tile targetTile, Unit targetUnit, GameObject shootObject, AttackInstance attInstance) {
            while (!turnControl.ClearToProceed()) yield return null;
            turnControl.ActionCommenced();

            aiming = true; yield return null;
            while (!Aiming(targetTile, targetUnit)) yield return null;

            //play animation
            if (attInstance.ShowAttack && unitAnim != null) {
                if (!attInstance.IsMelee) unitAnim.Attack();
                else unitAnim.AttackMelee();
            }
            if (attInstance.ShowAttack && unitAudio != null) {
                if (!attInstance.IsMelee) unitAudio.Attack();
                else unitAudio.AttackMelee();
            }

            if (!attInstance.IsAbility && !attInstance.Stunned && !attInstance.Destroyed && targetUnit.CanCounter(this)) battle.controller.AttackUnit(battle,targetUnit, this, true);
            //targetUnit.Counter(this);

            //shoot
            for (int i = 0; i < shootPointList.Count; i++) {
                Shoot(shootObject, targetTile, shootPointList[i], attInstance, i == shootPointList.Count - 1);
                if (delayBetweenShootPoint > 0) yield return new WaitForSeconds(delayBetweenShootPoint);
               
            }

            turnControl.ActionCompleted(0.15f);
            while (!turnControl.ClearToProceed()) yield return null;

            aiming = false;

            FinishAction();

            if (turretObject != null && turretObject != thisT) { while (!RotateTurretToOrigin() && !aiming) yield return null; }
        }



        //counter attack routine
        public void Counter(Unit targetUnit, AttackInstance attInst = null) { StartCoroutine(CounterRoutine(targetUnit, attInst)); }
        public IEnumerator CounterRoutine(Unit targetUnit, AttackInstance remoteAttack = null) {
            turnControl.CounterCommenced();

            yield return null;  //wait for shot to be fired first
                                //turnControl.actionInProgress will be set to >2 when there's shootObject active
                                //thus the next line wont be skipped, refer toturnControl.ClearToCounter() 

            while (!turnControl.ClearToCounter()) yield return null;
            yield return new WaitForSeconds(0.5f);

            AP -= GetCounterAPCost();
            counterRemain -= 1;

            aiming = true; yield return null;
            while (!Aiming(targetUnit.tile, targetUnit)) yield return null;

            if (unitAnim != null) unitAnim.Attack();
            if (unitAudio != null) unitAudio.Attack();

            AttackInstance attInstance = new AttackInstance(battle, this, targetUnit, true);
            attInstance.Process();
            if (remoteAttack != null) attInstance = remoteAttack;
            GameObject shootObj = shootObject != null ? shootObject.gameObject : null;
            if (shootObj == null) shootObj = gameControl.GetDefaultShootObject();

            for (int i = 0; i < shootPointList.Count; i++) {
                Shoot(shootObj, targetUnit.tile, shootPointList[i], attInstance, i == shootPointList.Count - 1);
                if (delayBetweenShootPoint > 0) yield return new WaitForSeconds(delayBetweenShootPoint);
            }

            while (!turnControl.ClearToCounter()) yield return null;
            turnControl.CounterCompleted();

            yield return new WaitForSeconds(0.25f);
            aiming = false;

            if (turretObject != null && turretObject != thisT) { while (!RotateTurretToOrigin() && !aiming) yield return null; }
        }


        private void Shoot(GameObject shootObj, Tile targetTile, Transform sp, AttackInstance attInstance, bool lastShootPoint = false) {
            GameObject sObjInstance = (GameObject)Instantiate(shootObj, sp.position, sp.rotation);
            ShootObject soInstance = sObjInstance.GetComponent<ShootObject>();
            if (!attInstance.IsAbility) {                           //for normal attack
                if (!lastShootPoint && targetTile.unit!=null) {
                    Vector3 pos = targetTile.unit.GetTargetT().position;
                    soInstance.Shoot(battle, pos, attInstance);
                }
                else soInstance.Shoot(battle, attInstance);
            }
            else {                                                  //this is for UnitAbility
                Vector3 pos = targetTile.GetPos();
                if (targetTile.unit != null) pos = targetTile.unit.GetTargetT().position;
                if (!lastShootPoint && targetTile.unit != null) soInstance.Shoot(battle, pos, attInstance);
                else soInstance.Shoot(battle, attInstance);
            }
        }


        void FinishAction() {
            if (factionManager.factionList[factionID].isNetworkFaction || isAIUnit) return;

            if (!IsAllActionCompleted()) gameControl.SelectUnit(this, true);
            else {
                factionManager.UnitMoveDepleted(this);
                gameControl.UnlockUnitSelect();
                bool unitAvailable = battle.factionManager.factionList[factionID].SelectFirstAvailableUnit(gameControl);
                if (!unitAvailable)
                    battle.controller.EndTurn(battle);
                //gameControl.SelectUnit(this);

               // if (onMoveDepletedE != null) onMoveDepletedE(this);
                battle.unitListeners.unitMoveDepletedHandler(this);
            }
        }


        public void ApplyAttack(AttackInstance attInstance) {
            attInstance.srcUnit.lastTarget = this;
            lastAttacker = attInstance.srcUnit;
            if (isAIUnit && !trigger) trigger = true;

            if (attInstance.Missed) {
                new TextOverlay(GetTargetT().position, "missed", Color.white);
                return;
            }

            if (unitAudio != null) unitAudio.Hit();
            if (unitAnim != null) unitAnim.Hit();

            ApplyDamage(attInstance.Damage, attInstance.Critical, attInstance.ShowAttack);

            if (attInstance.Stunned && stunned < attInstance.Stun) {
                AddUnitToEffectTracker();
                stunned = attInstance.Stun;
                stunCounter.Count(attInstance.Stun);
                VisualEffectManager.UnitStunned(this, stunned);
            }

            if (attInstance.Silenced && silenced < attInstance.Silent) {
                AddUnitToEffectTracker();
                silenced = attInstance.Silent;
                silentCounter.Count(attInstance.Silent);
            }
        }
        public override string ToString() {
            string s = "Unit:name-" + name + " prefabId-" + prefabId + " dataId-" + dataID + " tileId-" + tileId + " oid-" + oid;
            return s;
        }


        public void ApplyDamage(float dmg, bool critical, bool showDamage) {
            
            if (showDamage) {
                if (!critical) new TextOverlay(GetTargetT().position, dmg.ToString("f0"), Color.white);
                else new TextOverlay(GetTargetT().position, dmg.ToString("f0") + " Critical!", new Color(1f, .6f, 0, 1f));
            }
            float beforeHp = HP;
            HP -= dmg;
            Debug.Log("Unit recieved damage, before HP:" + beforeHp + " after HP:" + HP + " dmg:" + dmg);
            if (HP <= 0) {
                HP = 0;

                StartCoroutine(Dead());

                ClearVisibleTile();
                tile.unit = null;
            }
        }

        public GameObject destroyEffectObj;
        IEnumerator Dead() {
            if (destroyEffectObj != null) Instantiate(destroyEffectObj, GetTargetT().position, Quaternion.identity);

            float delay = 0;
            if (unitAudio != null) delay = unitAudio.Destroy();
            if (unitAnim != null) delay = Mathf.Max(delay, unitAnim.Destroy());
           // if (battleData.unitListeners.onUnitDestroyedE != null)
                battle.unitListeners.unitDestroyedHandler(this);
           // if (onUnitDestroyedE != null) onUnitDestroyedE(this);

            yield return new WaitForSeconds(delay);
            Destroy(thisObj);
        }

        public void RestoreHP(float val) {
            HP = Mathf.Min(HP + val, GetFullHP());
            new TextOverlay(GetTargetT().position, val.ToString("f0"), Color.green);
        }

        //called when a unit just reach it's turn
        public void ResetUnitTurnData() {
            moveRemain = GetMovePerTurn();
            attackRemain = GetAttackPerTurn();
            counterRemain = GetCounterPerTurn();
            disableAbilities = 0;

            if (gameControl.RestoreUnitAPOnTurn()) AP = GetFullAP();
            else AP = Mathf.Min(AP + GetAPPerTurn(), GetFullAP());

            HP = Mathf.Min(HP + GetHPPerTurn(), GetFullHP());
        }

        public bool CanCounter(Unit unit) {
            if (!gameControl.EnableCounter()) return false;
            if (stunned > 0) return false;
            if (counterRemain <= 0) return false;
            if (GetCounterAPCost() > AP) return false;

            float dist = gridManager.GetDistance(unit.tile, tile);
            if (dist > GetAttackRange()) return false;

            if (gameControl.EnableFogOfWar()) {
                if (requireDirectLOSToAttack && !gridManager.fogOfWar.InLOS(unit.tile, tile)) return false;
                if (!gridManager.fogOfWar.IsTileVisibleToFaction(unit.tile, factionID)) return false;
            }

            return true;
        }

        public bool IsAllActionCompleted() {
            if (stunned > 0) return true;
            if (attackRemain > 0 && AP >= GetAttackAPCost()) return false;
            if (moveRemain > 0 && AP >= GetMoveAPCost()) return false;
            //if(CanUseAbilities()) return false;
            return true;
        }



        //********************************************************************************************************************************
        //these section are related to FogOfWar

        [HideInInspector]
        private List<Tile> visibleTileList = new List<Tile>();  //a list of tile visible to the unit

        //called whenever the unit moved into a new tile
        //when ignoreFaction is set to true,  even AI faction will run the function, this is for optimization since the function will be called in MoveRoutine very often
        public void SetupFogOfWar(bool ignoreFaction = false) {
            if (!gameControl.EnableFogOfWar()) return;
            if (!ignoreFaction && isAIUnit) return;
            StartCoroutine(FogOfWarNewTile(ignoreFaction));
        }

        //add new tiles within sight to visibleTileList and remove those that are out of sight
        public IEnumerator FogOfWarNewTile(bool ignoreFaction) {
            List<Tile> newList = gridManager.GetTilesWithinDistance(tile, sight, false);
            newList.Add(tile);
            for (int i = 0; i < newList.Count; i++) {
                if (gridManager.fogOfWar.CheckTileVisibility(newList[i])) {
                    newList[i].SetVisible(true);
                }
            }

            yield return new WaitForSeconds(0.1f);

            ClearVisibleTile();

            visibleTileList = newList;
        }
        //set visible tile that becomes invisible to invisible
        public void ClearVisibleTile() {
            for (int i = 0; i < visibleTileList.Count; i++) {
                if (!gridManager.fogOfWar.CheckTileVisibility(visibleTileList[i])) visibleTileList[i].SetVisible(false);
            }
        }

        //called just before a unit start moving into a new tile, for AI unit to show/hide itself as it move in/out of fog-of-war
        //also called when a unit is just been placed on a grid in mid-game
        public void UpdateVisibility(Tile newTile = null) {
            if (!gameControl.EnableFogOfWar()) return;
            //if (!isAIUnit) return;

            if (newTile == null) newTile = tile;
            int layer = -1;
            if (newTile.IsVisible() ) {
                layer = LayerManager.GetLayerUnit();     
            }
            else {
                if (!factionManager.factionList[factionID].isHostile)
                    layer = LayerManager.GetLayerUnit();
                else
                    layer = LayerManager.GetLayerUnitInvisible();
            }
            thisObj.layer = layer;
            Utilities.SetLayerRecursively(thisT, layer);
        }

        //end FogOfWar section
        //********************************************************************************************************************************


        [HideInInspector]
        private UnitAudio unitAudio;
        public void SetAudio(UnitAudio unitAudioInstance) { unitAudio = unitAudioInstance; }

        [HideInInspector]
        private UnitAnimation unitAnim;
        public void SetAnimation(UnitAnimation unitAnimInstance) { unitAnim = unitAnimInstance; }
        public void DisableAnimation() { unitAnim = null; }
    }

}